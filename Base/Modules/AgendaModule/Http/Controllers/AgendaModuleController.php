<?php

namespace Modules\AgendaModule\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use Alert;
use Storage;
use Validator;
use Carbon\Carbon;
use App\Models\User;
use Auth;
use App\Http\Controllers\FilterCtrl;
class AgendaModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

     static $pelaksana=[
        'TACT',
        'TACT-LG',
        'DSS',
        'MIS NUWSP',
        'SUBDIT PEKERJAAN UMUM',
        'PEKERJAAN UMUM',

     ];

    public function index($tahun)
    {
        $page_meta=[
            'title'=>'Kegiatan',
            'keterangan'=>'Kegiatan',
        ];
        return view('agendamodule::index')->with(['page_meta'=>$page_meta,'pelaksana_op'=>static::$pelaksana]);
    }

    public function load($tahun,Request $request){
        $paginate=20;
        $data=DB::table('dataset.agenda as ag')
        ->where('tahun',$tahun)
        ->orderBy('pel_awal','desc');
        $select=['ag.*'];
        

        $My=User::find(Auth::id());
        if(in_array('TACT-REGIONAL',$My->getRoleNames()->toArray())){
            $filter=FilterCtrl::buildFilterPemda($request);
            $select[]="(case when ag.pelaksana='TACT-REGIONAL ".$filter['regional']."' then true else false end ) as editable ";
            $list_kegiatan[]='TACT-REGIONAL '.$filter['regional'];
        }else if(in_array('TACT-LG',$My->getRoleNames()->toArray())){
            $select[]="(case when (ag.pelaksana='TACT-LG' OR (ag.pelaksana ilike 'TACT-REGIONAL%')) then true else false end ) as editable ";
            $list_kegiatan[]='TACT-REGIONAL I';
            $list_kegiatan[]='TACT-REGIONAL II';
            $list_kegiatan[]='TACT-REGIONAL III';
            $list_kegiatan[]='TACT-REGIONAL IV';
            $list_kegiatan[]='TACT-LG';
        }

        if($request->kpi){
            $data=$data->where('kpi_'.$request->kpi,1);
        }

        $data=$data->selectRaw(implode(',',$select))->paginate($paginate);
        $data_=[];

       
        foreach($data as $key=>$d){
            $d->pel_awal=Carbon::parse($d->pel_awal)->format('Y-m-d');
            $d->pel_ahir=Carbon::parse($d->pel_ahir)->format('Y-m-d');
            $d->format_pel=Carbon::parse($d->pel_awal)->format('d F Y').' - '.Carbon::parse($d->pel_ahir)->format('d F Y');
            $d->jumlah_pemda=count(DB::table('dataset.agenda_dokumen_pendukung')
            ->where('id_agenda',$d->id)
            ->groupBy('kodepemda')
            ->select('kodepemda')
            ->get()->toArray());
            $d->kpi=implode(',',array_filter([$d->kpi_1,($d->kpi_2==1 ? '2':null),($d->kpi_3==1 ? '3':null),($d->kpi_4==1 ? '4':null)]));
            if($d->kpi){
                $d->kpi=implode(', ',array_map(function($el){return 'KPI '.$el;},explode(',',$d->kpi)));
            }
            $data_[]=$d;

        }
        return [
            'data'=>$data_,
            'list_option'=>$list_kegiatan,
            'current_page'=>$data->currentPage(),
            'last_page'=>$data->lastPage(),
            'paginate_count'=>$paginate,

        ];

    }


  

  
    public function store($tahun,Request $request)
    {
        //
		
        $valid=Validator::make($request->all(),[
            'name'=>'required|string',
            'pel_awal'=>'required|date',
            'pel_ahir'=>'required|date',
            'keterangan'=>'nullable|string',
            'pelaksana'=>'required|string',
        ]);

        if($valid->fails()){
            Alert::error('',$valid->errors()->first());
            return back();
        }

        DB::table('dataset.agenda')->insert([
            'name'=>$request->name,
			'kpi_1'=>(isset($request->kpi_1) ? '1':''),
			'kpi_2'=>(isset($request->kpi_2) ? '1':''),
			'kpi_3'=>(isset($request->kpi_3) ? '1':''),
			'kpi_4'=>(isset($request->kpi_4) ? '1':''),
            'pel_awal'=>$request->pel_awal,
            'pel_ahir'=>$request->pel_ahir,
            'pelaksana'=>$request->pelaksana,
            'keterangan'=>$request->keterangan,
            'tahun'=>$tahun,
        ]);

        Alert::success('','Berhasil Menambah Agenda Kegiatan');

        return back();
    }

   
    public function update(Request $request, $tahun,$id)
    {
        //
		//dd($request);
        $valid=Validator::make($request->all(),[
            'name'=>'required|string',
            'pel_awal'=>'required|date',
            'pel_ahir'=>'required|date',
            'keterangan'=>'nullable|string',
            'pelaksana'=>'required|string',
        ]);

        if($valid->fails()){
            Alert::error('',$valid->errors()->first());
            return back();
        }

        DB::table('dataset.agenda')->where('id',$id)->update([
            'name'=>$request->name,
			'kpi_1'=>(isset($request->kpi_1) ? '1':''),
			'kpi_2'=>(isset($request->kpi_2) ? '1':''),
			'kpi_3'=>(isset($request->kpi_3) ? '1':''),
			'kpi_4'=>(isset($request->kpi_4) ? '1':''),
            'pel_awal'=>$request->pel_awal,
            'pel_ahir'=>$request->pel_ahir,
            'pelaksana'=>$request->pelaksana,
            'keterangan'=>$request->keterangan,
            'tahun'=>$tahun,
        ]);

        Alert::success('','Berhasil Merubah Agenda Kegiatan');

        return back();
    }

    public function destroy($tahun,$id)
    {
        //
       $del= DB::table('dataset.agenda')->where('id',$id)->delete();

       if($del){
           Alert::success('','Berhasil Menghapus Data');
       }

       return back();
    }


    // detail

    public function load_detail($tahun,$id,Request $request)
    {
        $paginate=20;
        $select=["d.id,d.kodepemda,mp.nama_pemda,mp.regional_1,mp.regional_2,mp.tahun_proyek,mp.tipe_bantuan,d.file,d.name"];
        $filter=FilterCtrl::buildFilterPemda($request);
        $pemdas=FilterCtrl::filter_pemda($filter['regional']??null,false,$filter['sortlist'],$filter['tipe_bantuan'],$filter['jenis_pemda'],$filter['pemda']);
        $select[]="(case when (d.kodepemda in ('".implode("','",$pemdas)."')) then true else false end) as editable";
        $data=DB::table('dataset.agenda_dokumen_pendukung as d')
        ->join('dataset.agenda as ag','ag.id','=','d.id_agenda')
        ->where('d.id_agenda',$id)
        ->selectRaw(implode(',',$select))
        ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda');
        $data=$data->paginate($paginate);

        return [
            'data'=>$data->items(),
            'last_page'=>$data->lastPage(),
            'current_page'=>$data->currentPage(),
            'paginate_count'=>$paginate

        ];

    }

    function index_file($tahun,$id,Request $request){
        $data=DB::table('dataset.agenda')->where('tahun',$tahun)->where('id',$id)->first();
        if($data){
            $data->title=$data->name;
	        $pusat=DB::table('master.master_pemda')->selectRaw("'0' as kodepemda,'PUSAT/NASIONAL' as name")->limit(1);
            $pemda=DB::table('master.master_pemda')->selectRaw('kodepemda,nama_pemda as name')->where('kodepemda','!=',0)->union($pusat)->get();
            $role=Auth::User()->getRoleNames()->toArray();
            
            if(in_array("TACT-LG",$role)){
                $pemda=DB::table('master.master_pemda')->selectRaw('kodepemda,nama_pemda as name')->where('kodepemda','!=',0)->get();
            }else if(in_array("TACT-REGIONAL",$role)){
                $pemda=DB::table('master.master_pemda')->selectRaw('kodepemda,nama_pemda as name')
                ->where('regional_1',Auth::user()->regional)
                ->get();
            }   

            return view('agendamodule::detail')->with(['basis'=>_g_basis($request),'page_meta'=>(array)$data,'list_pemda'=>$pemda]);
        }else{
            Alert::info('','Data Tidak Tersedia');
            return back();
        }
        
      
    }

    public function store_file($tahun,$id,Request $request){
        $request['kegiatan']=$id;
        $valid=Validator::make($request->all(),[
            'name'=>'required|string',
            'file'=>'required|file',
            'kodepemda'=>'required|string|exists:master_pemda,kodepemda',
            'kegiatan'=>'required|numeric|exists:agenda,id'
        ]);

        if($valid->fails()){
            Alert::error('',$valid->errors()->first());
            return back();
        }

        $path=Storage::put('public/dokumen-pendukung-kegiatan/tahun-'.$tahun.'/'.$request->kodepemda.'/kegiatan-'.$id,$request->file);
        if($path){
            $path=Storage::url($path);
        }

        DB::table('dataset.agenda_dokumen_pendukung')->insert([
            'name'=>$request->name,
            'file'=>$path,
            'kodepemda'=>$request->kodepemda,
            'id_agenda'=>$id,
            
        ]);

        Alert::success('','Berhasil Menambah Dokumen Kegiatan');

        return back();



    }

    public function destroy_file($tahun,$id,$id_sub,Request $request)
    {
        //
       $del= DB::table('dataset.agenda_dokumen_pendukung')->where('id_agenda',$id)->where('id',$id_sub)->delete();

       if($del){
           Alert::success('','Berhasil Menghapus Data');
       }

       return back();
    }


    public function update_file($tahun,$id,$id_sub,Request $request){
        $request['kegiatan']=$id;
        $request['id_dokumen']=$id_sub;

        $valid=Validator::make($request->all(),[
            'name'=>'required|string',
            'file'=>'nullable|file',
            'kodepemda'=>'required|string|exists:master_pemda,kodepemda',
            'kegiatan'=>'required|numeric|exists:agenda,id',
            'id_dokumen'=>'required|numeric|exists:agenda_dokumen_pendukung,id',

        ]);

        if($valid->fails()){
            Alert::error('',$valid->errors()->first());
            return back();
        }

        $data=DB::table('dataset.agenda_dokumen_pendukung')->where('id_agenda',$id)->where('id',$id_sub)->first();


        if($data){
            if($request->file){
                $path=Storage::put('public/dokumen-pendukung-kegiatan/tahun-'.$tahun.'/'.$request->kodepemda.'/kegiatan-'.$id,$request->file);
                if($path){
                    $path=Storage::url($path);
                }
            }else{
                $path=$data->file;
            }
    
            DB::table('dataset.agenda_dokumen_pendukung')->where('id',$id_sub)->update([
                'name'=>$request->name,
                'file'=>$path,
                'kodepemda'=>$request->kodepemda,
                'id_agenda'=>$id,
                
            ]);
    
            Alert::success('','Berhasil Merubah Dokumen Kegiatan');
    
            return back();
        }

    }
    public function dash($tahun,Request $request){
        if(!$request->basis){
            $basis='LONGLIST';
        }else{
            $basis=$request->basis;
        }
        $tema=[];
        $tema[]=['kpi'=>1,'name'=>'40 Pemda meningkatkan dukungan finansial kepada BUMDAM'];
        $tema[]=['kpi'=>2,'name'=>'20 Pemda/PDAM mendapatkan program pendamping'];
        $tema[]=['kpi'=>3,'name'=>'200 pemda berpartisipasi aktif dalam program pelatihan dan peningkatan kapasitas'];
        $tema[]=['kpi'=>4,'name'=>'mencapai tarif yang full cost recovery'];




            $page_meta=['title'=>'Pelaksanaan Kegiatan','keterangan'=>''];
            return view('agendamodule::dashboard')->with(['page_meta'=>$page_meta,'basis'=>$basis,'kpi'=>$request->kpi,'tema'=>$tema]);	
    }


         public function data($tahun,Request $request){
			$paginate=20;
            
			switch ($request->kpi){
			case '1' :
			$data=DB::table('dataset.agenda as ag')
			->where('kpi_1','1')
			->orderBy('pel_awal','desc')
			->paginate($paginate);
			break;
			case '2' :
			$data=DB::table('dataset.agenda as ag')
			->where('kpi_2','1')
			->orderBy('pel_awal','desc')
			->paginate($paginate);
			break;
			case '3' :
			$data=DB::table('dataset.agenda as ag')
			->where('kpi_3','1')
			->orderBy('pel_awal','desc')
			->paginate($paginate);
			break;
			case '4' :
			$data=DB::table('dataset.agenda as ag')
			->where('kpi_4','1')
			->orderBy('pel_awal','desc')
			->paginate($paginate);
			break;
		}
		$data_=[];
        foreach($data as $key=>$d){
            $d->pel_awal=Carbon::parse($d->pel_awal)->format('Y-m-d');
            $d->pel_ahir=Carbon::parse($d->pel_ahir)->format('Y-m-d');
            $d->format_pel=Carbon::parse($d->pel_awal)->format('d F Y').' - '.Carbon::parse($d->pel_ahir)->format('d F Y');
            $d->jumlah_pemda=DB::table('dataset.agenda_dokumen_pendukung')->where('id_agenda',$d->id)->groupBy('kodepemda')->count();
			$d->kpi=implode(',',array_filter([$d->kpi_1,($d->kpi_2==1 ? '2':null),($d->kpi_3==1 ? '3':null),($d->kpi_4==1 ? '4':null)]));
            $data_[]=$d;

            
        }
        return [
            'data'=>$data_,
            'current_page'=>$data->currentPage(),
            'last_page'=>$data->lastPage(),
            'paginate_count'=>$paginate,

        ];
	 return $data;
}
}
