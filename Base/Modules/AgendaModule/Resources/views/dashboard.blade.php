@extends('adminlte::page')


@section('content')
<script>
    var page_meta=<?=json_encode($page_meta)?>;
</script>
<div class="parallax pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
    <div class="container">
        <section >
            <h3><b>{{$page_meta['title']}}</b></h3>
            <p><b>{!!$page_meta['keterangan']!!}</b></p>
        </section>

    </div>
</div>

<div id="app">
    <div class="p-3">
        <div class="d-flex">
           <div class="flex-column p-2" v-for="item,i in tema">
            <h4><b>KPI @{{item.kpi}}</b></h4>
            <small class="text-wight-bold"> @{{item.name}}</small>
            <p>
            <a v-bind:href="('{{route('mod.agenda.dash',['tahun'=>$StahunRoute,'kpi'=>'xxxx'])}}').replace('xxxx',item.kpi)" class="btn btn-sm btn-success"><i class="fa fa-arrow-right"></i> Detail</a>
            </p>
            
            </div>
        </div>
    </div>
    <div class="p-3 bg-white">
        <div class="d-flex">
            <div class="d-flex p-3 bg-primary rounded">
                <h4><b>KPI @{{kpi}}</b></h4></div>
            <div class="d-flex pl-3">
                 <h5 class="text-uppercase align-self-center"><b>@{{current_tema}}</b></h5>    

            </div>
        </div>
        <hr>
        
        <div class="text-center" v-if="loading">
            <div class="spinner-border text-primary" role="status">
                <span class="sr-only">Loading...</span>
                </div>
                <div class="spinner-border text-secondary" role="status">
                <span class="sr-only">Loading...</span>
                </div>
                <div class="spinner-border text-success" role="status">
                <span class="sr-only">Loading...</span>
                </div>
                <div class="spinner-border text-danger" role="status">
                <span class="sr-only">Loading...</span>
                </div>
        </div>
        <div class="table-responsive" >
            <table class="table table-bordered table-striped" id="table-data">
                <thead class="thead-dark">
                    <tr>
                        <th>Aksi</th>
                        <th>Nama</th>
                        <th>Penangung Jawab</th>
                        <th>Tanggal Pelaksanaan</th>
                        <th>Jumlah Pemda Terdata</th>
                        <th>keterangan</th>
                    </tr>    
                </thead>
            </table>
        </div>
    </div>

    <div class="modal fade" id="modal-detail" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-full" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    @{{modal.title}} 
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
                <small>@{{modal.pelaksanaan}}</small>
            </div>
            <div class="modal-body">
                <div class="text-center" v-if="modal.loading">
                    <div class="spinner-border text-primary" role="status">
                        <span class="sr-only">Loading...</span>
                        </div>
                        <div class="spinner-border text-secondary" role="status">
                        <span class="sr-only">Loading...</span>
                        </div>
                        <div class="spinner-border text-success" role="status">
                        <span class="sr-only">Loading...</span>
                        </div>
                        <div class="spinner-border text-danger" role="status">
                        <span class="sr-only">Loading...</span>
                        </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="table-detail">
                        <thead class="thead-dark">
                            <th>KODEPEMDA</th>
                            <th>NAMAPEMDA</th>
                            <th>DOKUMEN</th>
                        </thead>
                    </table>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('js')


<script>
var app=new Vue({
    el:'#app',
    data:{
        data:[],
        loading:false,
        current_page:0,
        tema:<?=json_encode($tema)?>,
        kpi:{{$kpi}},
        ref_table:null,   
        modal:{
            loading:false,
            id:null,
            title:'',
            data:[],
            pelaksanaan:'',
            ref_table:null
        }    

    },
    computed:{
        current_tema:function(){
            var f=this.tema.filter(el=>{
                return el.kpi==this.kpi;
            });

            if(f.length){
                return f[0].name;
            }else{
                return '';
            }
        }
    },
    created:function(){
        var self=this;
        setTimeout((ref) => {
            ref.modal.ref_table=$('#table-detail').DataTable({
                    columns:[
                        {
                                data:'kodepemda'
                            },
                            {
                                data:'nama_pemda'
                            },
                            {
                                data:'name',
                                render:(data,type,row,index)=>{

                                    return '<a download href="'+'{{url('')}}'+row.file+'" class="btn btn-primary btn-sm">'+data+'</a>';
                                }
                            }
                    ]
                });
                ref.ref_table=$('#table-data').DataTable({
                    columns:[
                        {
                            type:'html',
                            sort:false,
                            render:(data,type,row,index)=>{
                                return '<div  class="btn-group"><a onclick="app.showModal('+index.row+')" class="btn btn-success btn-sm" href="javascript:void(0)"><i class="fa fa-arrow-right"></i></a>';
                                
                            }
                        }
                        ,{
                            data:'name'
                        },{
                            data:'pelaksana'
                        },
                        {
                            data:'format_pel'
                        },
                        {
                            data:'jumlah_pemda',
                            type:'number',
                            render:(data)=>{
                                return data+' PEMDA';
                            }
                        },
                        {
                            data:'keterangan'
                        }
                    ]
                });
            ref.init();

            }, 400,self);
    },
    methods:{
       
        init:function(){
            this.current_page=1;
            this.data=[];
            this.load();
            var self=this;
            
        },
        showModal:function(index){
            var data=this.data[index];
            this.modal.title=data.name;
            this.modal.current_page=1;
            this.modal.data=[];
            this.modal.ref_table.clear().draw();
            this.modal.pelaksanaan=data.format_pell
            this.modal.id=data.id;
            this.loadDetail();
            $('#modal-detail').modal();
            

        },
        loadDetail:function(){
            var data=this.modal;
            var self=this;
            this.modal.loading=true;
            req_ajax.post(('{{route('api-web-mod.agenda.load_detail',['tahun'=>$StahunRoute,'id'=>'xxxx'])}}').replace('xxxx',data.id),{
                page:this.modal.current_page,
                filter:'',
                basis:''
            }).then(res=>{
                 self.modal.loading=false;

                if(res.data.current_page==self.modal.current_page){
                    if(self.modal.ref_table){
                        res.data.data.forEach(el=>{
                            self.modal.data.push(el);
                        });

                        self.modal.ref_table.clear();
                        self.modal.ref_table.rows.add(self.modal.data).draw();
                   
                    }else{


                    }
                    if(res.data.last_page!=self.modal.current_page){
                        self.modal.current_page+=1;
                        self.loadDetail();
                    }

                    
                }

            });

        },
        load:function(){
            var self=this;
            this.loading=true;

            req_ajax.post('{{route('api-web-mod.agenda.load',['tahun'=>$StahunRoute])}}',{
                page:this.current_page,
                kpi:this.kpi
            }).then(function(res){
                self.loading=false;
                res.data.data.forEach(el => {
                    self.data.push(el);
                });

                if(self.ref_table){
                    self.ref_table.clear().draw();
                    self.ref_table.rows.add(self.data).draw();
                }

                if(res.data.last_page!=self.current_page){
                    self.current_page+=1;
                    self.load();
                }
            });
        }
    }
})    
</script>
    
    

@stop