@extends('adminlte::page')


@section('content')
<script>
    var page_meta=<?=json_encode($page_meta)?>;
</script>
<div class="parallax pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
    <div class="container">
        <section >
            <h3><b>{{$page_meta['title']}} - {{$page_meta['pelaksana']}}</b></h3>
            <p><b>{!!$page_meta['keterangan']!!}</b></p>
            <p>{{Carbon\Carbon::parse($page_meta['pel_awal'])->format('d F Y')}} - {{\Carbon\Carbon::parse($page_meta['pel_ahir'])->format('d F Y')}}</p>
           
        </section>
      
    </div>
   
</div>


<div id="app" class="p-3">
    <div class="btn-group">
        <button class="btn btn-success btn-xs" @click="add()"><i class="fa fa-plus"></i> Tambah Dokumen PEMDA</button>
    </div>
    <hr>
    <div class="text-center" v-if="loading">
        <div class="spinner-border text-primary" role="status">
                <span class="sr-only">Loading...</span>
                </div>
                <div class="spinner-border text-secondary" role="status">
                <span class="sr-only">Loading...</span>
                </div>
                <div class="spinner-border text-success" role="status">
                <span class="sr-only">Loading...</span>
                </div>
                <div class="spinner-border text-danger" role="status">
                <span class="sr-only">Loading...</span>
                </div>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-striped" id="table-data">
            <thead class="thead-dark">
                <tr>
                    <th>AKSI</th>
                    <th>REGIONAL</th>
                    <th>WILAYAH</th>
                    <th>TAHUN PROYEK</th>
                    <th>TIPE BANTUAN</th>
                    <th>KODEPEMDA</th>
                    <th>NAMA PEMDA</th>
                    <th>DOKUMEN</th>
                </tr>
            </thead>
        </table>
    </div>

    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form v-bind:action="modal.action_url" method="post" enctype='multipart/form-data'>
                    @csrf
                    <div class="modal-header">
                <h5 class="modal-title">@{{modal.title}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="">PEMDA</label>
                   <v-select name="kodepemda" required="" v-model="modal.kodepemda" :reduce="(option) => option.kodepemda" :label="'name'"  :options="option" ></v-select>
                    <input type="hidden" name="kodepemda" v-bind:value="modal.kodepemda">
                 </div>
                 <div class="form-group">
                    <label for="">Nama File</label>
                    <input type="text" name="name" class="form-control" required v-model="modal.name">
                 </div>
                 <a v-bind:href="modal.file_render_path" download="" v-if="modal.file" class="btn btn-success">Download File</a>
                 <div class="form-group">
                    <label for="">File</label>
                    <input type="file" name="file" class="form-control" required>
                 </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
                </form>
            </div>
        </div>
        </div>

        <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
       <form method="post" v-bind:action="('{{route('mod.agenda.detail.delete',['tahun'=>$StahunRoute,'id'=>$page_meta['id'],'id_sub'=>'xxxx'])}}').replace('xxxx',form_del.id)">
            @csrf     
            <div class="modal-header">
                <h5 class="modal-title">Menghapus Kegiatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah Anda Yakin Menghapus Dokumen "@{{form_del.name}}" ?</p>    
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger">Hapus</button>
            </div> 
        </form>   
    </div>
  </div>

</div>

@stop

@section('js')
<script>
    var app=new Vue({
        el:'#app',
        data:{
            data:[],
            loading:false,
            current_page:0,
            ref_table:null,
            filter:'provinsi && kota kab',
            basis:"{{$basis}}",
            option:<?=json_encode($list_pemda)?>,
            form_del:{
                id:null,
                name:'',
            },
            modal:{
                title:'',
                kodepemda:'',
                tahun:{{$StahunRoute}},
                file:'',
                file_render_path:'',
                name:'',
                action_url:''
            }
        },
        created:function(){
            this.current_page=1;
            var self=this;

            setTimeout((ref) => {
                self.ref_table=$('#table-data').DataTable({
                    columns:[
                        {   type:'html',
                            sort:false,
                            data:'editable',
                            render:(data,type,row,index)=>{
                                 return data?'<div class="btn-group">'+
                                    '<button onclick="app.edit('+index.row+')" type="button" class="btn btn-warning btn-sm"><i class="fa fa-pen"></i></button>'+
                                    '<button type="button" onclick="app.hapus('+index.row+')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button></div>'
                                    :"";
                            }
                        },
                        {
                            data:'regional_1',
                            render:function(data){
                                return parseInt(data)?data:'';
                            }
                        },
                        {
                            data:'regional_2',
                            render:function(data){
                                return parseInt(data)?data:'';
                            }
                        },
                        {
                            data:'tahun_proyek',
                            render:function(data){
                                return parseInt(data)?data:'';
                            }
                        },
                        {
                            data:'tipe_bantuan',
                            render:function(data){
                                return parseInt(data)?data:'';
                            }
                        },
                        {
                            data:'kodepemda',
                            render:function(data){
                                return parseInt(data)?data:'';
                            }
                        },
                        {
                            data:'nama_pemda'
                        },
                        {
                            data:'name',
                            render:(data,type,row,index)=>{

                                return '<a download href="'+'{{url('')}}'+row.file+'" class="btn btn-primary btn-sm">'+data+'</a>';
                            }
                        }
                    ]
                })
                ref.init();
            }, 300,self);

        },
        methods:{
            init:function(){
                this.current_page=1;
                this.data=[];
                this.load();
            },
            react:function(data){
                this.filter=data.data.filter;
                this.init();
            },
            load:function(){
                var self=this;
                self.loading=true;
                req_ajax.post('{{route('api-web-mod.agenda.load_detail',['tahun'=>$StahunRoute,'id'=>$page_meta['id']])}}',{
                    filter:this.filter,
                    basis:this.basis,
                    page:this.current_page
                }).then(res=>{
                     self.loading=false;

                    res.data.data.forEach(element => {
                        self.data.push(element);
                    });

                    if(self.ref_table){
                        self.ref_table.clear().draw();
                        self.ref_table.rows.add(self.data).draw();
                    }

                    if(res.data.last_page!=self.current_page){
                        self.current_page+=1;
                        self.load();
                    }
                });
            },
            add:function(){
                this.modal.title='Tambah Dokumen';
                this.modal.kodepemda=null;
                this.modal.file=null;
                this.modal.name=null;
                this.modal.file_render_path=null;
                this.modal.action_url='{{route('mod.agenda.detail.store',['tahun'=>$StahunRoute,'id'=>$page_meta['id']])}}';
                $('#modal-form').modal();
            },
            edit:function(index){
                if(this.data[index]!=undefined){
                    var data=this.data[index];
                    this.modal.title='Ubah Dokumen';
                    this.modal.kodepemda=data.kodepemda;
                    this.modal.file=data.file;
                    this.modal.name=data.name;
                    this.modal.file_render_path='{{url('')}}'+data.file;
                    this.modal.action_url=('{{route('mod.agenda.detail.update',['tahun'=>$StahunRoute,'id'=>$page_meta['id'],'id_sub'=>'xxxx'])}}').replace('xxxx',data.id);

                    $('#modal-form').modal();
                }
            },
            hapus:function(index){
                if(this.data[index]!=undefined){
                    var data=this.data[index];
                    this.form_del.id=data.id;
                    this.form_del.name=data.name;
                    $('#modal-delete').modal();



                }

            }
        }
    });


   
</script>

<script>


</script>

@stop