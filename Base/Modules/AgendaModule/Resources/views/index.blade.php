@extends('adminlte::page')


@section('content')
<script>
    var page_meta=<?=json_encode($page_meta)?>;
</script>
<div class="parallax pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
    <div class="container">
        <section >
            <h3><b>{{$page_meta['title']}}</b></h3>
            <p><b>{!!$page_meta['keterangan']!!}</b></p>
        </section>
      
    </div>
   
</div>
<div id="app">
    <div class="p-3">
        <button class="btn btn-success btn-sm" @click="create"><i class="fa fa-plus"></i> Tambah</button>
        <hr>
        <div class="text-center" v-if="loading">
            <div class="spinner-border text-primary" role="status">
                <span class="sr-only">Loading...</span>
                </div>
                <div class="spinner-border text-secondary" role="status">
                <span class="sr-only">Loading...</span>
                </div>
                <div class="spinner-border text-success" role="status">
                <span class="sr-only">Loading...</span>
                </div>
                <div class="spinner-border text-danger" role="status">
                <span class="sr-only">Loading...</span>
                </div>
            </div>
        <div class="table-responsive" >
            <table class="table table-bordered table-striped" id="table-data">
                <thead class="thead-dark">
                    <tr>
                        <th>Aksi</th>
						<th>KPI</th>
                        <th>Nama</th>
                        <th>Penangung Jawab</th>
                        <th>Tanggal Pelaksanaan</th>
                        <th>Jumlah Pemda Terdata</th>

                        <th>keterangan</th>
                    </tr>    
                </thead>
                </table>
        </div>
    </div>


    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
       <form method="post" v-bind:action="('{{route('mod.agenda.delete',['tahun'=>$StahunRoute,'id'=>'xxxx'])}}').replace('xxxx',form_del.id)">
            @csrf        
        <div class="modal-header">
                <h5 class="modal-title">Menghapus Kegiatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah Anda Yakin Menghapus Kagiatan "@{{form_del.name}}" ?</p>    
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger">Hapus</button>
            </div> 
        </form>   
    </div>
  </div>
    </div>
    <div class="modal fade" id="modal-add" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
     <form v-bind:action="form.action_url" method="post">
        @csrf
        <div class="modal-header">
        <h5 class="modal-title">@{{form.title}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		<div class="form-group">
           <label for="">Mendukung KPI</label>

			<div class="row">
			<div class="col-2">
			<input type="checkbox" value="kpi_1" name="kpi_1" id="kpi_1" v-model="form.kpi_1"  />
			<label for="kpi_1">KPI 1</label>
			</div>
			<div class="col-2">
            <input type="checkbox" value="kpi_2" name="kpi_2" id="kpi_2" v-model="form.kpi_2" />
			<label for="kpi_2">KPI 2</label>
			</div>
			<div class="col-2">
            <input type="checkbox" value="kpi_3" name="kpi_3" id="kpi_3" v-model="form.kpi_3" />            
			<label for="kpi_3">KPI 3</label>
			</div>
			<div class="col-2">
            <input type="checkbox" value="kpi_4" name="kpi_4" id="kpi_4" v-model="form.kpi_4" />
			<label for="kpi_4">KPI 4</label>
			</div>
			
			</div >
		</div>

        <div class="form-group">
            <label for="">Nama Kegiatan</label>
            <input type="text" name="name" required v-model="form.name" class="form-control">
        </div>

        <div class="form-group">
            <label for="">Pelaksana</label>
            <select name="pelaksana" required id="" class="form-control">
                <option v-bind:value="item" v-for="item,k in option_pelaksana">
                    @{{item}}
                </option>
            </select>
        </div>
        <div class="form-group">
            <label for="">Tanggal Pelaksanaan</label>
            <input type="date" name="pel_awal" required v-model="form.pel_awal" v-bind:max="form.pel_ahir" class="form-control">
        </div>
        <div class="form-group">
            <label for="">Tanggal Pelaksanaan Ahir</label>
            <input type="date" name="pel_ahir" required v-model="form.pel_ahir"  v-bind:min="form.pel_awal" class="form-control">
        </div>
        <div class="form-group">
            <label for="">Keterangan</label>
            <textarea name="keterangan" class="form-control" v-model="form.keterangan" id="" cols="30" rows="10"></textarea>
        </div>
        
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Kirim</button>
      </div>    
    </form>
    </div>
  </div>
</div>
</div>

@stop

@section('js')
<script>
var app=new Vue({
    el:'#app',
    data:{
        data:[],
        loading:false,
        current_page:0,
        ref_table:null,
        option_pelaksana:[],
        form_del:{
            id:null,
            name:null
        },
        form:{
            title:'Tambah Kegiatan',
            kpi:null,
			pel_awal:null,
            pel_ahir:null,
            pelaksana:null,
            keterangan:null,
            name:null,
            action_url:null,
        }

    },
    created:function(){
        var self=this;
        setTimeout((ref) => {
                ref.ref_table=$('#table-data').DataTable({
                    columns:[
                        {
                            type:'html',
                            sort:false,
                            data:'editable',
                            render:(data,type,row,index)=>{
                                var edit='';
                                if(data){
                                    edit='<button onclick="app.edit('+index.row+')" type="button" class="btn btn-warning btn-sm"><i class="fa fa-pen"></i></button>'+
                                    '<button type="button" onclick="app.hapus('+index.row+')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button></div>';
                                }
                                return '<div class="btn-group"><a class="btn btn-success btn-sm" href="'+('{{route('mod.agenda.detail.index',['tahun'=>$StahunRoute,'id'=>'xxxx'])}}').replace('xxxx',row.id)+'"><i class="fa fa-arrow-right"></i></a>'+edit;
                            }
                        },{
                            data:'kpi'
                        },{
                            data:'name'
                        },{
                            data:'pelaksana'
                        },
                        {
                            data:'format_pel'
                        },
                        {
                            data:'jumlah_pemda',
                            type:'number',
                            render:(data)=>{
                                console.log(data,'pp');
                                return data+' PEMDA';
                            }
                        },
                        {
                            data:'keterangan'
                        }
                    ]
                });
            ref.init();

            }, 400,self);
    },
    methods:{
        create:function(){
            this.form.action_url='{{route('mod.agenda.store',['tahun'=>$StahunRoute])}}';
            this.form.title='Tambah Kegiatan';
			this.form.kpi_1=null;
			this.form.kpi_2=null;
			this.form.kpi_3=null;
			this.form.kpi_4=null;
            this.form.pel_awal=null;
            this.form.pel_ahir=null;
            this.form.pelaksana=null;
            this.form.keterangan=null;
            this.form.name=null;

            $('#modal-add').modal();
        },

        hapus:function(index){

            if(this.data[index]!=undefined){
                var data=this.data[index];
                this.form_del.id=data.id;
                this.form_del.name=data.name;
                $('#modal-delete').modal();
             }
        },
        edit:function(index){

            if(this.data[index]!=undefined){
                var data=this.data[index];
                this.form.action_url=('{{route('mod.agenda.update',['tahun'=>$StahunRoute,'id'=>'xxxx'])}}').replace('xxxx',data.id);
                this.form.title='Ubah Kegiatan';
				this.form.kpi_1=(data.kpi_1 == 1 ? 'kpi_1':'');
				this.form.kpi_2=(data.kpi_2 == 1 ? 'kpi_2':'');
				this.form.kpi_3=(data.kpi_3 == 1 ? 'kpi_3':'');
				this.form.kpi_4=(data.kpi_4 == 1 ? 'kpi_4':'');
				this.form.pel_awal=data.pel_awal;
                this.form.pel_ahir=data.pel_ahir;
                this.form.pelaksana=data.pelaksana;
                this.form.keterangan=data.keterangan;
                this.form.name=data.name;
                $('#modal-add').modal();
            }
          
        },
        init:function(){
            this.current_page=1;
            this.load();
            var self=this;
            
        },
        load:function(){
            var self=this;
            this.loading=true;

            req_ajax.post('{{route('api-web-mod.agenda.load',['tahun'=>$StahunRoute])}}',{
                page:this.current_page
            }).then(function(res){
                self.loading=false;
                self.option_pelaksana=res.data.list_option,
                res.data.data.forEach(el => {
                    self.data.push(el);
                });

                if(self.ref_table){
                    self.ref_table.clear().draw();
                    self.ref_table.rows.add(self.data).draw();
                }

                if(res.data.last_page!=self.current_page){
                    self.current_page+=1;
                    self.load();
                }
            });
        }
    }
})    
</script>

@stop