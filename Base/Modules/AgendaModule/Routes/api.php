<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('mod/{tahun}/module-agenda')->name('api-web-mod.agenda')->middleware('auth:api')->group(function(){
    Route::post('/','AgendaModuleController@load')->name('.load');
    Route::post('/load/detail/{id}','AgendaModuleController@load_detail')->name('.load_detail');

    
});
Route::prefix('mod/{tahun}/module-agenda')->name('api-web-mod.agenda')->group(function(){
    Route::get('/{kpi}','AgendaModuleController@data')->name('.data');

});