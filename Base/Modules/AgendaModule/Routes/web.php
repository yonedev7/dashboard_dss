<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::prefix('session/{tahun}')->middleware('bindTahun')->name('mod.agenda')->group(function(){
        Route::prefix('module-agenda')->group(function() {
            Route::get('/view/kpi/{kpi}', 'AgendaModuleController@dash')->middleware('auth:web')->name('.dash');
            

        });
});
Route::prefix('dash/{tahun}/sistem-informasi/module-agenda')->name('mod.agenda')->middleware(['auth:web','bindTahun'])->group(function() {
    Route::get('/', 'AgendaModuleController@index')->name('.index');
    Route::post('store', 'AgendaModuleController@store')->name('.store');
    Route::post('update/{id}', 'AgendaModuleController@update')->name('.update');
    Route::post('delete/{id}', 'AgendaModuleController@destroy')->name('.delete');

    Route::prefix('/detail/{id}/document')->name('.detail')->group(function(){
        Route::get('/', 'AgendaModuleController@index_file')->name('.index');
        Route::post('/store', 'AgendaModuleController@store_file')->name('.store');
        Route::post('/update/{id_sub}', 'AgendaModuleController@update_file')->name('.update');
        Route::post('/delete/{id_sub}', 'AgendaModuleController@destroy_file')->name('.delete');

    });

    
});
