<?php

namespace Modules\DSS\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Modules\DSS\Http\Controllers\DatasetController;
use Illuminate\Support\Facades\Schema;
use DB;
class APBDController extends DatasetController
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

    
    function __construct($tahun=null){

        if($tahun){
            $this->tahun=$tahun;
        }
        $this->table='pemda_apbd_[TAHUN]';
        $this->table_status='dts_104_[TAHUN]';
        $this->area=[
            'namaprogram'=>[
                'data'=>'namaprogram',
                'aggre'=>'max(namaprogram)',
                'label'=>'Nama Program',
                'satuan'=>'-',
                'type'=>'String'
            ],
            'namakegiatan'=>[
                'data'=>'namakegiatan',
                'aggre'=>'max(namakegiatan)',
                'label'=>'Nama Program',
                'satuan'=>'-',
                'type'=>'String'
            ],
            'namasubkegiatan'=>[
                'data'=>'namasubkegiatan',
                'aggre'=>'max(namasubkegiatan)',
                'label'=>'Nama Program',
                'satuan'=>'-',
                'type'=>'String'
            ],
            'totalharga'=>[
                'data'=>'totalharga',
                'aggre'=>'max(totalharga)',
                'label'=>'Nama Program',
                'satuan'=>'-',
                'type'=>'String'
            ]
        ];
    }
    
    public function table_control($tahun)
    {
        $this->tahun=$tahun;
        $long=($this->_getPemda());
        $sort=($this->_getPemda('sortlist'));
        $data=($this->_getLastData(false,$sort['kodepemda']));

        dd($this->_getRekapTahun($data->tahun,$sort['kodepemda']));


        return view('dss::index');
    }

    public function keterisian(){


    }

    public function formInput($tahun,$kodepemda){



    }


    public function store($tahun,$kodepemda){

    }

    public function showcaseDashboard($tahun,Request $request){
        $title='DATA APBD Air Minum';
        $subtitle="";
        $data=[];
        foreach([$tahun-2,$tahun -1,$tahun ] as $th){
            $table=str_replace('[TAHUN]',$th,$this->table);
            $table_status=str_replace('[TAHUN]',$th,$this->table_status);

            
           if(Schema::hasTable('dataset.'.$table)){
            $c= DB::table('dataset.'.$table.' as d')
            ->selectRaw('sum(d.totalharga)::float8 as y,
            count(distinct(d.kodepemda)) as jumlah_pemda,
            count(distinct(d.kodepemda)) filter (where length(d.kodepemda)>3) as jumlah_pemda_kabkot,
            count(distinct(d.kodepemda)) filter (where length(d.kodepemda)<=3) as jumlah_pemda_provinsi,
            d.tahun as name')
            ->where('d.kodesubkegiatan','ilike','1.03.03%')
            ->where('d.tahun','=',$th)
            ->groupBy('d.tahun')
            ->orderBy('d.tahun','asc');
    
            if($request->status_data){
                $c=$c->join('status_data.'.$table_status.' as st',[['st.kodepemda','=','d.kodepemda'],['st.tahun','=','d.tahun']])
                ->where('st.status',$request->status_data);
            }
            $agre=false;
            if($request->kodepemda){
                $c=$c->where('d.kodepemda',$request->kodepemda);
            }else if($request->regional){
                $agre=true;
                $pemda=$this->_getPemda(null,$request->regional);
                $title.=' Regional '.$request->regional.' ('.count($pemda['kodepemda']).' Pemda)';
                $c=$c->whereIn('d.kodepemda',$pemda['kodepemda']);
            }else if($request->scope){
                $agre=true;
    
                $pemda=$this->_getPemda($request->scope);
                $title.=' '.$request->scope.' ('.count($pemda['kodepemda']).' Pemda)';
                $c=$c->whereIn('d.kodepemda',$pemda['kodepemda']);
            }else{
                $agre=true;
    
                $pemda=$this->_getPemda();
                $title.=' Nasional ('.count($pemda['kodepemda']).' Pemda)';
                $c=$c->whereIn('d.kodepemda',$pemda['kodepemda']);
            }        
            $c=$c->first();

            if($c){
                array_push($data,$c);
            }
           }
        }



        $series=[
            [
                'name'=>'Anggaran APBD',
                'data'=>[],
                'yAxis'=>0,
                'type'=>'area',

            ],
            [
                'name'=>'Jumlah Pemda',
                'data'=>[],
                'yAxis'=>1,
                'type'=>'column',


            ],
            [
                'name'=>'Jumlah Pemda Kota/Kab',
                'data'=>[],
                'yAxis'=>1,
                'type'=>'column',


            ],
            [
                'name'=>'Jumlah Pemda Provisi',
                'data'=>[],
                'type'=>'column',

                'yAxis'=>1,

            ]
        ];


        $yAxis=[
            [
                'title'=> [
                    'text'=>'Rp',
                ]
            ],
            [
                'title'=> [
                    'text'=>'Pemda',
                ],
                'min'=>0,
                'max'=>548,
                'opposite'=>true
            ]

           
        ];


        if(!$agre){
            $series=[$series[0]];
        }

       
        foreach($data as $k=>$d){
            $data[$k]->y=(float)$d->y;
            if($agre){
                $series[0]['data'][]=[
                    'y'=>(float)$d->y,
                    'name'=>$d->name,
                    
                ];
                $series[1]['data'][]=[
                    'y'=>(float)$d->jumlah_pemda,
                    'name'=>$d->name
                ];
                $series[2]['data'][]=[
                    'y'=>(float)$d->jumlah_pemda_kabkot,
                    'name'=>$d->name
                ];
                $series[3]['data'][]=[
                    'y'=>(float)$d->jumlah_pemda_provinsi,
                    'name'=>$d->name
                ];
            }else{
                $series[0]['data'][]=[
                    'y'=>(float)$d->y,
                    'name'=>$d->name
                ];

            }
        }

        if(count($data)){
            $capaian=[
                'tahun'=>$data[count($data)-1]->name??0,
                'value'=>$data[count($data)-1]->y??0,
                'jumlah_pemda'=>$data[count($data)-1]->jumlah_pemda,
                'jumlah_pemda_kabkot'=>$data[count($data)-1]->jumlah_pemda_kabkot,
                'jumlah_pemda_provinsi'=>$data[count($data)-1]->jumlah_pemda_provinsi
            ];
    
    
            if(count($data)>1){
            $deviasi=($data[count($data)-1]->y??0) - ($data[count($data)-2]->y??0);

            $persentase=$this->_percentage($deviasi,$data[count($data)-2]->y);
            }else{
            $deviasi=0;
            $persentase=0;
            }
        }else{
            $capaian=[
                'tahun'=>$tahun,
                'value'=>0,
                'jumlah_pemda'=>0,
                'jumlah_pemda_kabkot'=>0,
                'jumlah_pemda_provinsi'=>0
            ];
    
    
            $deviasi=0;
    
            $persentase=0;
    
        }

       return [
            'title'=>'DATA APBD Air Minum',
            'subtitle'=>'',
            'dev'=>$deviasi,
            'capaian'=>$capaian,
            'dev_satuan'=>'Rp.',
            'dev_satuan_pos'=>'prefix',
            'dev_percentage'=>$persentase,
            'charts'=>[
                [
                    'chart'=>[
                        'type'=>'area'
                    ],
                    'title'=>[
                        'text'=>$title
                    ],
                    'subtitle'=>[
                        'text'=>$subtitle
                    ],
                    'plotOptions'=>[
                        "series"=>[
                            "dataLabels"=>[
                               
                                    "enabled"=>true
                            ]
                        ]
                    ],
                    'xAxis'=>[
                        'type'=>'category'
                    ],
                    'yAxis'=>$yAxis,
                    'series'=>$series
                ]
               
            ]
        ];

      
    }






}
