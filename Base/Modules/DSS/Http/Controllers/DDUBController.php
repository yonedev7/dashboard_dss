<?php

namespace Modules\DSS\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use DB;

class DDUBController extends DatasetController
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

    function __construct(){
        $this->table='pemda_ddub';
        $this->table_status='dts_120';
    }

    public function showcaseDashboard($tahun,Request $request){
        $title='DATA DDUB Perencanaan ';
        $subtitle="";

        $data=  DB::table('dataset.'.$this->table.' as d')
        ->groupBy('d.tahun')
        ->selectRaw('sum(d.ddub_perencanaan)::float8 as y,
        count(distinct(d.kodepemda)) as jumlah_pemda,
        count(distinct(d.kodepemda)) filter (where  length(d.kodepemda)>3) as jumlah_pemda_kabkot,
        count(distinct(d.kodepemda)) filter (where  length(d.kodepemda)<=3) as jumlah_pemda_provinsi,
        d.tahun as name')
        ->orderBy('d.tahun','asc')
        ->where('d.tahun','<=',$tahun)
        ->where('d.tahun','>=',$tahun-($request->tahun_mundur??2));


        if($request->status_data){
            $data=$data->join('status_data.'.$this->table_status.' as st',[['st.kodepemda','=','d.kodepemda'],['st.tahun','=','d.tahun']])
            ->where('st.status',$request->status_data);
        }
        $agre=false;
        if($request->kodepemda){
            $data=$data->where('d.kodepemda',$request->kodepemda);
        }else if($request->regional){
            $agre=true;
            $pemda=$this->_getPemda(null,$request->regional,'STIMULAN');
            $title.=' Regional '.$request->regional.' ('.count($pemda['kodepemda']).' Pemda)';
            $data=$data->whereIn('d.kodepemda',$pemda['kodepemda']);
        }else if($request->scope){
            $agre=true;
            $pemda=$this->_getPemda($request->scope,null,'STIMULAN');
            $title.=' '.$request->scope.' ('.count($pemda['kodepemda']).' Pemda)';
            $data=$data->whereIn('d.kodepemda',$pemda['kodepemda']);
        }else{
            $agre=true;

            $pemda=$this->_getPemda(null,null,'STIMULAN');
            $title.=' Nasional ('.count($pemda['kodepemda']).' Pemda)';
            $data=$data->whereIn('d.kodepemda',$pemda['kodepemda']);
        }       
        $data=$data->get();

        $series=[
            [
                'name'=>'Realisasi Pemda',
                'data'=>[],
                'type'=>'area',

                'yAxis'=>0,
            ],
            [
                'name'=>'Jumlah Pemda',
                'data'=>[],
                'yAxis'=>1,
                'type'=>'column',


            ],
            [
                'name'=>'Jumlah Pemda Kota/Kab',
                'data'=>[],
                'yAxis'=>1,
                'type'=>'column',


            ],
            [
                'name'=>'Jumlah Pemda Provisi',
                'data'=>[],
                'yAxis'=>1,
                'type'=>'column',


            ]
        ];


        $yAxis=[
            [
                'title'=> [
                    'text'=>'Rp',
                ]
            ],
            [
                'title'=> [
                    'text'=>'Pemda',
                ],
                'min'=>0,
                'max'=>548,
                'opposite'=>true
            ]

           
        ];


        if(!$agre){
            $series=[$series[0]];
        }

       
        if(count($data)){
            foreach($data as $k=>$d){
                $data[$k]->y=(float)$d->y;
                if($agre){
                    $series[0]['data'][]=[
                        'y'=>(float)$d->y,
                        'name'=>$d->name,
                        
                    ];
                  
                    $series[1]['data'][]=[
                        'y'=>(float)$d->jumlah_pemda,
                        'name'=>$d->name
                    ];
                    $series[2]['data'][]=[
                        'y'=>(float)$d->jumlah_pemda_kabkot,
                        'name'=>$d->name
                    ];
                    $series[3]['data'][]=[
                        'y'=>(float)$d->jumlah_pemda_provinsi,
                        'name'=>$d->name
                    ];
                }else{
                    $series[0]['data'][]=[
                        'y'=>(float)$d->y,
                        'name'=>$d->name
                    ];
    
                }
            }
    
            $capaian=[
                'tahun'=>$data[count($data)-1]->name??0,
                'value'=>$data[count($data)-1]->y??0,
                'jumlah_pemda'=>$data[count($data)-1]->jumlah_pemda,
                'jumlah_pemda_kabkot'=>$data[count($data)-1]->jumlah_pemda_kabkot,
                'jumlah_pemda_provinsi'=>$data[count($data)-1]->jumlah_pemda_provinsi
            ];

            if(count($data)>1){
                $deviasi=($data[count($data)-1]->y??0) - ($data[count($data)-2]->y??0);
    
                $persentase=$this->_percentage($deviasi,$data[count($data)-2]->y);
               }else{
                $deviasi=0;
                $persentase=0;
               }

        }else{
            $capaian=[
                'tahun'=>$tahun,
                'value'=>0,
                'jumlah_pemda'=>0,
                'jumlah_pemda_kabkot'=>0,
                'jumlah_pemda_provinsi'=>0
            ];
    
    
            $deviasi=0;
    
            $persentase=0;

        }
       

       return [
            'title'=>'DATA DDUB Perencanaan ',
            'subtitle'=>'',
            'dev'=>$deviasi,
            'capaian'=>$capaian,
            'dev_satuan'=>'Rp.',
            'dev_satuan_pos'=>'prefix',
            'dev_percentage'=>$persentase,
            'charts'=>[
                [
                    'chart'=>[
                        'type'=>'area'
                    ],
                    'title'=>[
                        'text'=>$title
                    ],
                    'subtitle'=>[
                        'text'=>$subtitle
                    ],
                    'plotOptions'=>[
                        "series"=>[
                            "dataLabels"=>[
                               
                                    "enabled"=>true
                            ]
                        ]
                    ],
                    'xAxis'=>[
                        'type'=>'category'
                    ],
                    'yAxis'=>$yAxis,
                    'series'=>$series
                ]
               
            ]
        ];

      
    }

    public function showcaseDashboard2($tahun,Request $request){
        $title='DATA DDUB Realisasi ';
        $subtitle="";

        $data=  DB::table('dataset.'.$this->table.' as d')
        ->groupBy('d.tahun')
        ->selectRaw('sum(d.ddub_realisasi)::float8 as y,
        count(distinct(d.kodepemda)) as jumlah_pemda,
        count(distinct(d.kodepemda)) filter (where  length(d.kodepemda)>3) as jumlah_pemda_kabkot,
        count(distinct(d.kodepemda)) filter (where  length(d.kodepemda)<=3) as jumlah_pemda_provinsi,
        d.tahun as name')
        ->orderBy('d.tahun','asc')
        ->where('d.tahun','<=',$tahun)
        ->where('d.tahun','>=',$tahun-($request->tahun_mundur??2));


        if($request->status_data){
            $data=$data->join('status_data.'.$this->table_status.' as st',[['st.kodepemda','=','d.kodepemda'],['st.tahun','=','d.tahun']])
            ->where('st.status',$request->status_data);
        }
        $agre=false;
        if($request->kodepemda){
            $data=$data->where('d.kodepemda',$request->kodepemda);
        }else if($request->regional){
            $agre=true;
            $pemda=$this->_getPemda(null,$request->regional,'STIMULAN');
            $title.=' Regional '.$request->regional.' ('.count($pemda['kodepemda']).' Pemda)';
            $data=$data->whereIn('d.kodepemda',$pemda['kodepemda']);
        }else if($request->scope){
            $agre=true;
            $pemda=$this->_getPemda($request->scope,null,'STIMULAN');
            $title.=' '.$request->scope.' ('.count($pemda['kodepemda']).' Pemda)';
            $data=$data->whereIn('d.kodepemda',$pemda['kodepemda']);
        }else{
            $agre=true;

            $pemda=$this->_getPemda(null,null,'STIMULAN');
            $title.=' Nasional ('.count($pemda['kodepemda']).' Pemda)';
            $data=$data->whereIn('d.kodepemda',$pemda['kodepemda']);
        }       
        $data=$data->get();

        $series=[
            [
                'name'=>'Realisasi Pemda',
                'data'=>[],
                'type'=>'area',

                'yAxis'=>0,
            ],
            [
                'name'=>'Jumlah Pemda',
                'data'=>[],
                'yAxis'=>1,
                'type'=>'column',


            ],
            [
                'name'=>'Jumlah Pemda Kota/Kab',
                'data'=>[],
                'yAxis'=>1,
                'type'=>'column',


            ],
            [
                'name'=>'Jumlah Pemda Provisi',
                'data'=>[],
                'yAxis'=>1,
                'type'=>'column',


            ]
        ];


        $yAxis=[
            [
                'title'=> [
                    'text'=>'Rp',
                ]
            ],
            [
                'title'=> [
                    'text'=>'Pemda',
                ],
                'min'=>0,
                'max'=>548,
                'opposite'=>true
            ]

           
        ];


        if(!$agre){
            $series=[$series[0]];
        }

        if(count($data)){
            foreach($data as $k=>$d){
                $data[$k]->y=(float)$d->y;
                if($agre){
                    $series[0]['data'][]=[
                        'y'=>(float)$d->y,
                        'name'=>$d->name,
                        
                    ];
                  
                    $series[1]['data'][]=[
                        'y'=>(float)$d->jumlah_pemda,
                        'name'=>$d->name
                    ];
                    $series[2]['data'][]=[
                        'y'=>(float)$d->jumlah_pemda_kabkot,
                        'name'=>$d->name
                    ];
                    $series[3]['data'][]=[
                        'y'=>(float)$d->jumlah_pemda_provinsi,
                        'name'=>$d->name
                    ];
                }else{
                    $series[0]['data'][]=[
                        'y'=>(float)$d->y,
                        'name'=>$d->name
                    ];
    
                }
            }
    
            $capaian=[
                'tahun'=>$data[count($data)-1]->name??0,
                'value'=>$data[count($data)-1]->y??0,
                'jumlah_pemda'=>$data[count($data)-1]->jumlah_pemda,
                'jumlah_pemda_kabkot'=>$data[count($data)-1]->jumlah_pemda_kabkot,
                'jumlah_pemda_provinsi'=>$data[count($data)-1]->jumlah_pemda_provinsi
            ];

            if(count($data)>1){
                $deviasi=($data[count($data)-1]->y??0) - ($data[count($data)-2]->y??0);
    
                $persentase=$this->_percentage($deviasi,$data[count($data)-2]->y);
               }else{
                $deviasi=0;
                $persentase=0;
               }

        }else{
            $capaian=[
                'tahun'=>$tahun,
                'value'=>0,
                'jumlah_pemda'=>0,
                'jumlah_pemda_kabkot'=>0,
                'jumlah_pemda_provinsi'=>0
            ];
    
    
            $deviasi=0;
    
            $persentase=0;

        }
       

       return [
            'title'=>'DATA DDUB Realisasi',
            'subtitle'=>'',
            'dev'=>$deviasi,
            'capaian'=>$capaian,
            'dev_satuan'=>'Rp.',
            'dev_satuan_pos'=>'prefix',
            'dev_percentage'=>$persentase,
            'charts'=>[
                [
                    'chart'=>[
                        'type'=>'area'
                    ],
                    'title'=>[
                        'text'=>$title
                    ],
                    'subtitle'=>[
                        'text'=>$subtitle
                    ],
                    'plotOptions'=>[
                        "series"=>[
                            "dataLabels"=>[
                               
                                    "enabled"=>true
                            ]
                        ]
                    ],
                    'xAxis'=>[
                        'type'=>'category'
                    ],
                    'yAxis'=>$yAxis,
                    'series'=>$series
                ]
               
            ]
        ];

      
    }
    public function index()
    {
        return view('dss::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('dss::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('dss::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('dss::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
