<?php

namespace Modules\DSS\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use Auth;
use Illuminate\Support\Facades\Schema;

class DatasetController extends Controller
{
    public $id_datset=null;
    public $table='';
    public $schema='dataset';
    public $table_status='';
    public $schema_status='status_data';
    public $connection_db='live';
    public $tahun=2022;

    public $st_code=[0,1,2];

    public function _listedDataset($context){
        $data=DB::table('master.dataset as dts')->leftJoin('master.menus as mn','mn.id','=','dts.id_menu')
        ->leftJoin('master.kpi as kpi','mn.id','=','kpi.id')
        ->orderBy('mn.type','asc')
        ->orderBy('mn.index','asc')
        ->orderBy('kpi.id','asc')
        ->orderBy('dts.index','asc')
        ->selectRaw("dts.*, kpi.keterangan as target")
       ->get();
       

        
        return $data;


    }

    public function _viewTableControl($tahun,$kontext){
        $this->tahun=$tahun;

        $data=$this->_listedDataset($kontext);

        $sort=$this->_getPemda('sortlist');
        $long=$this->_getPemda('longlist');
        $nas=$this->_getPemda();



        foreach($data as $k=>$d){
            $this->table=$d->table;
            $this->table_status='dts_'.$d->id;
            if(str_contains($this->table,'[TAHUN]')){
                $this->table_status.='_[TAHUN]';
            }

            $data[$k]->last_data=$this->_getLastData();
            $str_pemda=$sort;
            $lg_pemda=$long;
            $nas_pemda=$nas;

            $data[$k]->rekap=[
                'sort_pemda'=>$str_pemda,
                'sort'=>$this->_getRekapTahun($data[$k]->last_data->tahun,$sort['kodepemda']),
                'long_pemda'=>$lg_pemda,
                'long'=>$this->_getRekapTahun($data[$k]->last_data->tahun,$long['kodepemda']),
                'all_pemda'=>$nas_pemda,
                'all'=>$this->_getRekapTahun($data[$k]->last_data->tahun,$nas['kodepemda']),

            ];

            $data[$k]->capaian=[];

            foreach([$tahun-4,$tahun-3,$tahun-2,$tahun-1,$tahun] as $th){
                $data[$k]->capaian[$th]=$this->_getRekapTahun($th,$nas['kodepemda']);
            }

            
        }




        return view('dss::admin.table-data.index',['data'=>$data,'Sadmin'=>true]);


    }

    public function _queryData($tahun,$status=null,$kodepemda=[]){
        if(str_contains($this->table,'[TAHUN]')){      
            $tb=str_replace('[TAHUN]',$tahun,$this->table);
            $tbs=str_replace('[TAHUN]',$tahun,$this->table_status);
        }else{
            $tb=$this->table;
            $tbs=$this->table_status;
        }

        $data=DB::table($this->schema.'.'.$tb.' as d')
        ->leftJoin('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
        ->leftJoin($this->schema_status.'.'.$tbs.' as st',[['st.kodepemda','=','d.kodepemda'],['st.tahun','=','st.tahun']]);

        if($status){
            $data=$data->where('st.status','=',$status);
        }
        if(count($kodepemda)){
            $data=$data->whereIn('d.kodepemda',$kodepemda);
        }
        return $data;
    
    }

    public function _getRekapTahun($tahun,$kodepemda=[]){
           
            $data=(Object)[
                'target'=>count($kodepemda),
                'total'=>0,
                'count_input'=>0,
                'count_verifikasi'=>0,
                'count_validasi'=>0,
                'tahun'=>$tahun,
                'exist_table'=>false,
                'percentage_total'=>0,
                'percentage_validasi'=>0,

            ];
            $x=null;


            $tb=$this->table;
            $tbs=$this->table_status;
            if(str_contains($this->table,'[TAHUN]')){
               
                $tb=str_replace('[TAHUN]',$tahun,$this->table);
                $tbs=str_replace('[TAHUN]',$tahun,$this->table_status);
                if(Schema::hasTable($this->schema.'.'.$tb)){
                    $data->exist_table=true;

                    $x= DB::connection($this->connection_db)->table($this->schema.'.'.$tb.' as d')
                    ->leftJoin($this->schema_status.'.'.$tbs.' as st',[['st.tahun','=','d.tahun'],['st.kodepemda','=','d.kodepemda']])
                    ->groupBy('d.tahun')
                    ->where('d.tahun',$tahun)
                    ->selectRaw("count(distinct(d.kodepemda)) as total, 
                    count(distinct(d.kodepemda)) filter (where (d.id is not null and st.status is null) or st.status={$this->st_code[0]} or d.kodepemda is not null) as count_input,
                    count(distinct(d.kodepemda))  as count_verifikasi,
                    count(distinct(d.kodepemda))  filter (where (st.status >= {$this->st_code[2]})) as count_validasi,
                     d.tahun");
                    if(count($kodepemda)){
                        $x=$x->whereIn('d.kodepemda',$kodepemda);
                    }
                    
                    $x=$x->first();
                }
                    
    
    
            }else{
                if(!Schema::hasTable($this->schema.'.'.$tb)){
                    return $data;
                }
                $data->exist_table=true;



                $x= DB::connection($this->connection_db)->table($this->schema.'.'.$tb.' as d')
                ->leftJoin($this->schema_status.'.'.$tbs.' as st',[['st.tahun','=','d.tahun'],['st.kodepemda','=','d.kodepemda']])
                ->selectRaw("count(distinct(d.kodepemda)) as total, 
                    count(distinct(d.kodepemda)) filter (where (d.id is not null and st.status is null) or st.status={$this->st_code[0]} or d.kodepemda is not null) as count_input,
                    count(distinct(d.kodepemda)) filter (where (st.status >= {$this->st_code[1]})) as count_verifikasi,
                    count(distinct(d.kodepemda))  filter (where (st.status = {$this->st_code[2]})) as count_validasi,
                     d.tahun")
                ->where('d.tahun',$tahun)
                ->groupBy('d.tahun')
                ->orderBy(DB::raw("count(d.id)"),'desc');
                if(count($kodepemda)){
                    $x=$x->whereIn('d.kodepemda',$kodepemda);
                }
                
                $x=$x->first();

            }

            if($x){
                $data=array_merge((array)$data,(array)$x);
            }

            $data=(object)$data;
            $data->percentage_total=$this->_percentage($data->total,$data->target);
            $data->percentage_validasi=$this->_percentage($data->count_validasi,$data->target);

            return (object)$data;

    }

    public function _percentage($val,$all){
        if($val!=0 AND $all!=0){
            return (float)number_format((($val/$all)*100),2,'.','. ');
        }else if($all==0){
            return 100;
        }else{
            return 0;

        }
    }


    public function _getPemda($lokus=null,$regional=null,$tipe_bantuan=null){

        if(Auth::guard('web')->check() OR Auth::guard('api')->check()){

        }

        $pemda= DB::table('master.master_pemda as mp')
            ->where('mp.kodepemda','!=','0')
         ->leftjoin('master.pemda_lokus as lk','lk.kodepemda','=','mp.kodepemda');
         if(($lokus)){
            switch(strtolower($lokus)){
                case 'sortlist':
                $pemda=$pemda->where('lk.tahun_proyek',$this->tahun);
                break;
                case 'longlist':
                    $pemda=$pemda->where('lk.id','>=',1);
                break;
            }
         }

         if($regional){
            $pemda=$pemda->where('mp.regional_1',$regional);
         }

         if($tipe_bantuan){
            if(is_array($tipe_bantuan)){
                $pemda=$pemda->whereIn('lk.tipe_bantuan',$tipe_bantuan);
            }else{
                $pemda=$pemda->where('lk.tipe_bantuan',$tipe_bantuan);
            }

         }

         $pemda=$pemda->selectRaw('mp.kodepemda,max(mp.nama_pemda) as nama_pemda')->groupBy('mp.kodepemda')->get();

         $data=[
            'kodepemda'=>[],
            'data'=>[]
         ];

         foreach($pemda as $k=>$p){
            $data['kodepemda'][]=$p->kodepemda;
            $data['data'][]=$p;
         }


         return $data;
    }

    

    public function _getLastData($public=true,$kodepemda=[]){
        $data=(Object)[
            'count'=>0,
            'tahun'=>$this->tahun,
            'exist_table'=>false,
        ];
        $x=null;


        if($public){
            $tb=$this->table;
            $tbs=$this->table_status;
            if(str_contains($this->table,'[TAHUN]')){

                foreach([$this->tahun,$this->tahun-1,$this->tahun-2,$this->tahun-3] as $th){
                    $tb=str_replace('[TAHUN]',$th,$this->table);
                    $tbs=str_replace('[TAHUN]',$th,$this->table_status);
                    if(Schema::hasTable($this->schema.'.'.$tb)){
                        $data->exist_table=true;
                        $x= DB::connection($this->connection_db)->table($this->schema.'.'.$tb.' as d')
                        ->join($this->schema_status.'.'.$tbs.' as st',[['st.tahun','=','d.tahun'],['st.kodepemda','=','d.kodepemda']])
                        ->where('st.status',$this->st_code[2])
                        ->where('st.tahun',$th)
                        ->groupBy('st.tahun')
                        ->selectRaw("count(distinct(d.kodepemda)) as count,st.tahun");
                        if(count($kodepemda)){
                            $x=$x->whereIn('d.kodepemda',$kodepemda);
                        }

                        $x=$x->first();
                        if($x){
                            if($x->count){
                                break; 
                            }
                        }
                    }
    
                }
    
            }else{
                if(!Schema::hasTable($this->schema.'.'.$tb)){
                    return $data;
                }
                $data->exist_table=true;

                
                $x= DB::connection($this->connection_db)->table($this->schema.'.'.$tb.' as d')
                ->join($this->schema_status.'.'.$tbs.' as st',[['st.tahun','=','d.tahun'],['st.kodepemda','=','d.kodepemda']])
                ->selectRaw("count(distinct(d.kodepemda)) as count,st.tahun")->where('st.status',$this->st_code[2])
                ->groupBy('st.tahun')->orderBy(DB::raw("count(d.id)"),'desc');
                if(count($kodepemda)){
                    $x=$x->whereIn('d.kodepemda',$kodepemda);
                }
                
                $x=$x->first();

            }
           
        }else{

            $tb=$this->table;
            $tbs=$this->table_status;
            if(str_contains($this->table,'[TAHUN]')){
                foreach([$this->tahun,$this->tahun-1,$this->tahun-2,$this->tahun-3] as $th){

                    $tb=str_replace('[TAHUN]',$th,$this->table);
                    $tbs=str_replace('[TAHUN]',$th,$this->table_status);
                    if(Schema::hasTable($this->schema.'.'.$tb)){
                        $data->exist_table=true;

                        $x= DB::connection($this->connection_db)->table($this->schema.'.'.$tb.' as d')
                        ->leftJoin($this->schema_status.'.'.$tbs.' as st',[['st.tahun','=','d.tahun'],['st.kodepemda','=','d.kodepemda']])
                        ->groupBy('d.tahun')
                        ->where('d.tahun',$th)
    
                        ->selectRaw("count(distinct(d.kodepemda)) as count,d.tahun");

                        if(count($kodepemda)){
                            $x=$x->whereIn('d.kodepemda',$kodepemda);
                        }
                        
                        $x=$x->first();
                        if($x){
                            if($x->count){
                                break; 
                            }
                        }
                    }
    
                }
    
            }else{
                if(!Schema::hasTable($this->schema.'.'.$tb)){
                    return $data;
                }
                $data->exist_table=true;


                $x= DB::connection($this->connection_db)->table($this->schema.'.'.$tb.' as d')
                ->leftJoin($this->schema_status.'.'.$tbs.' as st',[['st.tahun','=','d.tahun'],['st.kodepemda','=','d.kodepemda']])
                ->selectRaw("count(distinct(d.kodepemda)) as count,d.tahun")
                ->groupBy('d.tahun')
                ->orderBy(DB::raw("count(d.id)"),'desc');

                if(count($kodepemda)){
                    $x=$x->whereIn('d.kodepemda',$kodepemda);
                }
                
                $x=$x->first();

            }


        }

        if($x){
            $data=array_merge((array)$data,(array)$x);
        }

        return (object)$data;


    }
    
}
