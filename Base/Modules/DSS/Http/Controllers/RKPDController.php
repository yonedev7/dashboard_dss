<?php

namespace Modules\DSS\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\DSS\Http\Controllers\DatasetController;
use DB;
class RKPDController extends DatasetController
{
    function __construct(){
        $this->id_datset=101;
        $this->table='rkpd_sipd_air_minum';
        $this->table_status='dts_101';
    }

    public function rekapRegional($tahun){

        $pemda=$this->_getPemda();
        $pemda_l=$this->_getPemda('longlist');
        $pemda_s=$this->_getPemda('sortlist');
        $pemda_reg_1=$this->_getPemda(null,'I');
        $pemda_reg_2=$this->_getPemda(null,'II');
        $pemda_reg_3=$this->_getPemda(null,'III');
        $pemda_reg_4=$this->_getPemda(null,'IV');


        $last=$this->_getLastData(true,$pemda['kodepemda']);

        $rekap=[
            'nasional'=>$this->_getRekapTahun($tahun,$pemda['kodepemda']),
            'longlist'=>$this->_getRekapTahun($tahun,$pemda_l['kodepemda']),
            'sortlist'=>$this->_getRekapTahun($tahun,$pemda_s['kodepemda']),
        ];


        $data_regional=[
            'I'=>$this->_queryData($tahun,null,$pemda_reg_1['kodepemda'])->selectRaw('d.kodepemda::int,st.status')->get(),
            'II'=>$this->_queryData($tahun,null,$pemda_reg_2['kodepemda'])->selectRaw('d.kodepemda::int,st.status')->get(),
            'III'=>$this->_queryData($tahun,null,$pemda_reg_3['kodepemda'])->selectRaw('d.kodepemda::int,st.status')->get(),
            'IV'=>$this->_queryData($tahun,null,$pemda_reg_4['kodepemda'])->selectRaw('d.kodepemda::int,st.status')->get(),

        ];


       return view('dss::dash.rkpd')->with(['rekap'=>$rekap,'rekap_regional'=>$data_regional]);
        


    }

    public function table_control($tahun)
    {
        $this->tahun=$tahun;
        
        dd($this->_getLastData(false));


        return view('dss::index');
    }

    public function admin_keterisian(){

    }

    public function admin_data(){

    }



    public function showcaseDashboard($tahun,Request $request){
        $title='DATA RKPD Air Minum';
        $subtitle="";

        $data=  DB::table('dataset.'.$this->table.' as d')
        ->groupBy('d.tahun')
        ->where('d.kodesubkegiatan','ilike','1.03.03%')
        ->selectRaw('sum(pagu)::float8 as y,
        count(distinct(d.kodepemda)) as jumlah_pemda,
        count(distinct(d.kodepemda)) filter (where  length(d.kodepemda)>3) as jumlah_pemda_kabkot,
        count(distinct(d.kodepemda)) filter (where  length(d.kodepemda)<=3) as jumlah_pemda_provinsi,
        d.tahun as name')
        ->orderBy('d.tahun','asc')
        ->where('d.tahun','<=',$tahun)
        ->where('d.tahun','>=',$tahun-($request->tahun_mundur??2));


        if($request->status_data){
            $data=$data->join('status_data.'.$this->table_status.' as st',[['st.kodepemda','=','d.kodepemda'],['st.tahun','=','d.tahun']])
            ->where('st.status',$request->status_data);
        }
        $agre=false;
        if($request->kodepemda){
            $data=$data->where('d.kodepemda',$request->kodepemda);
        }else if($request->regional){
            $agre=true;
            $pemda=$this->_getPemda(null,$request->regional);
            $title.=' Regional '.$request->regional.' ('.count($pemda['kodepemda']).' Pemda)';
            $data=$data->whereIn('d.kodepemda',$pemda['kodepemda']);
        }else if($request->scope){
            $agre=true;
            $pemda=$this->_getPemda($request->scope);
            $title.=' '.$request->scope.' ('.count($pemda['kodepemda']).' Pemda)';
            $data=$data->whereIn('d.kodepemda',$pemda['kodepemda']);
        }else{
            $agre=true;

            $pemda=$this->_getPemda();
            $title.=' Nasional ('.count($pemda['kodepemda']).' Pemda)';
            $data=$data->whereIn('d.kodepemda',$pemda['kodepemda']);
        }       
        $data=$data->get();

        $series=[
            [
                'name'=>'Pagu RKPD',
                'data'=>[],
                'type'=>'area',

                'yAxis'=>0,
            ],
            [
                'name'=>'Jumlah Pemda',
                'data'=>[],
                'type'=>'column',

                'yAxis'=>1,

            ],
            [
                'name'=>'Jumlah Pemda Kota/Kab',
                'data'=>[],
                'yAxis'=>1,
                'type'=>'column',


            ],
            [
                'name'=>'Jumlah Pemda Provisi',
                'data'=>[],
                'yAxis'=>1,
                'type'=>'column',


            ]
        ];


        $yAxis=[
            [
                'title'=> [
                    'text'=>'Rp',
                ]
            ],
            [
                'title'=> [
                    'text'=>'Pemda',
                ],
                'min'=>0,
                'max'=>548,
                'opposite'=>true
            ]

           
        ];


        if(!$agre){
            $series=[$series[0]];
        }

       
        if(count($data)){
            foreach($data as $k=>$d){
                $data[$k]->y=(float)$d->y;
                if($agre){
                    $series[0]['data'][]=[
                        'y'=>(float)$d->y,
                        'name'=>$d->name,
                        
                    ];
                  
                    $series[1]['data'][]=[
                        'y'=>(float)$d->jumlah_pemda,
                        'name'=>$d->name
                    ];
                    $series[2]['data'][]=[
                        'y'=>(float)$d->jumlah_pemda_kabkot,
                        'name'=>$d->name
                    ];
                    $series[3]['data'][]=[
                        'y'=>(float)$d->jumlah_pemda_provinsi,
                        'name'=>$d->name
                    ];
                }else{
                    $series[0]['data'][]=[
                        'y'=>(float)$d->y,
                        'name'=>$d->name
                    ];
    
                }
            }
    
            $capaian=[
                'tahun'=>$data[count($data)-1]->name??0,
                'value'=>$data[count($data)-1]->y??0,
                'jumlah_pemda'=>$data[count($data)-1]->jumlah_pemda,
                'jumlah_pemda_kabkot'=>$data[count($data)-1]->jumlah_pemda_kabkot,
                'jumlah_pemda_provinsi'=>$data[count($data)-1]->jumlah_pemda_provinsi
            ];

            if(count($data)>1){
                $deviasi=($data[count($data)-1]->y??0) - ($data[count($data)-2]->y??0);
    
                $persentase=$this->_percentage($deviasi,$data[count($data)-2]->y);
               }else{
                $deviasi=0;
                $persentase=0;
               }
        }else{
            $capaian=[
                'tahun'=>$tahun,
                'value'=>0,
                'jumlah_pemda'=>0,
                'jumlah_pemda_kabkot'=>0,
                'jumlah_pemda_provinsi'=>0
            ];
    
    
            $deviasi=0;
    
            $persentase=0;

        }

       return [
            'title'=>'DATA RKPD Air Minum',
            'subtitle'=>'',
            'dev'=>$deviasi,
            'capaian'=>$capaian,
            'dev_satuan'=>'Rp.',
            'dev_satuan_pos'=>'prefix',
            'dev_percentage'=>$persentase,
            'charts'=>[
                [
                    'chart'=>[
                        'type'=>'area'
                    ],
                    'title'=>[
                        'text'=>$title
                    ],
                    'subtitle'=>[
                        'text'=>$subtitle
                    ],
                    'plotOptions'=>[
                        "series"=>[
                            "dataLabels"=>[
                               
                                    "enabled"=>true
                            ]
                        ]
                    ],
                    'xAxis'=>[
                        'type'=>'category'
                    ],
                    'yAxis'=>$yAxis,
                    'series'=>$series
                ]
               
            ]
        ];

      
    }

    public function showcaseData($tahun,Request $request){
        $data=  DB::table('dataset'.$this->table.' as d');
        if($request->scope){
          $pemda=$this->_getPemda();
        }else{
          $pemda=$this->_getPemda();
        }
      }
}
