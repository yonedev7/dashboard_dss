<?php

namespace Modules\DSS\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
class TematikKpiController extends DatasetController
{

   
    public function index($tahun)
    {
        $this->tahun=$tahun;
        $tahun_base=2019;
       
        $pemda_l=$this->_getPemda('longlist');

        $this->table='pemda_ddub';
        $this->table_status='dts_120';

        $ddub= $this->_queryData($tahun,2,$pemda_l['kodepemda'])
        ->where('d.tahun','<=',$tahun)
        ->groupBy('d.kodepemda')
        ->selectRaw('d.kodepemda,sum(d.ddub_realisasi) as realisasi')->where('d.ddub_komitmen','>',0)->get();

        $this->table='pemda_pmpd';
        $this->table_status='dts_119';

        $pmpd= $this->_queryData($tahun,2,$pemda_l['kodepemda'])
        ->where('d.tahun','<=',$tahun)
        ->groupBy('d.kodepemda')
        ->selectRaw('d.kodepemda,sum(d.modal_pemda) as realisasi')->where('d.modal_pemda','>',0)->get();

        $pmpd_jdb= $this->_queryData($tahun,2,$pemda_l['kodepemda'])
        ->where('d.tahun','<=',$tahun)
        ->groupBy('d.kodepemda')
        ->selectRaw('d.kodepemda,sum(d.modal_pemda_infra)')->where('d.modal_pemda_infra','>',0)->get();

        $this->table='pemda_pmpd';
        $this->table_status='dts_119';

        $ar=array_merge($ddub->pluck('kodepemda')->toArray(),$pmpd->pluck('kodepemda')->toArray(),$pmpd_jdb->pluck('kodepemda')->toArray());

        $pemda=DB::table('master.master_pemda')->whereIn('kodepemda',$ar)->selectRaw('kodepemda,nama_pemda')->get();
        dd($pemda);
        dd($ddub,$pmpd,$pmpd_jdb);

        
    }

   
}
