@extends('adminlte::page')

@section('content_header')
<style>
    .bg-hightlight-soft{
        background-color:rgb(181, 243, 190)!important;
        font-weight: 800!important;
}
</style>
<h4>Table Control</h4>

@stop
@section('content')


<div class="container-fluid">
    <table class="table  table-bordered table-striped">
        <thead class="bg-dark">
    
            <tr>
                <th >Nama Dataset</th>
                <th>Target</th>
                @foreach([$StahunRoute-4,$StahunRoute-3,$StahunRoute-2,$StahunRoute-1,$StahunRoute] as $th)
                <th>{{$th}} </th>
                @endforeach
            </tr>
    
        </thead>
    
       <tbody>
        @foreach($data as $d)
        <tr class="{{$d->target?'bg-secondary':''}}">
            <td >{{$d->name}}</td>
            <td></td>
            @foreach($d->capaian as $th=>$dt)
            <td>{!!$dt->total!!} Pemda</td>
            @endforeach
        </tr>
         @endforeach
       </tbody>
        
    </table>
</div>

{{-- <table class="table  table-bordered table-striped">
    <thead>
        <tr>
            <th rowspan="2">Nama Dataset</th>
            <th colspan="3"> Sort</th>
            <th colspan=3>Long</th>
        </tr>
        <tr>
            <th>Target</th>
            <th>Capaian</th>
            <th>Kebutuhan</th>
            <th>Target</th>
            <th>Capaian</th>
            <th>Kebutuhan</th>



        </tr>  
        <tr class="bg-dark">
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
            <th>5</th>
            <th>6</th>
            <th>7</th>


        </tr>
    </thead>   
@foreach($data as $d)
    <tr>
        <td rowspan="2">{{$d->name}} - {{$d->last_data->tahun}}</td>
        <td>{{number_format(count($d->rekap['sort_pemda']['kodepemda']))}}</td>
        <td>{{number_format($d->rekap['sort']->total)}}</td>
        <td rowspan="2" class="bg-success">{{number_format(count($d->rekap['sort_pemda']['kodepemda']) - $d->rekap['sort']->total)}}</td>
        <td>{{number_format(count($d->rekap['long_pemda']['kodepemda']))}}</td>
        <td>{{number_format($d->rekap['long']->total)}}</td>
        <td rowspan="2" class="bg-primary">
            <div id="dtd-{{$d->id}}">

            </div>

        


        </td>


    </tr>
    <tr>
        <td colspan="">
            <p><b>Verifikasi</b></p>
            {{number_format($d->rekap['sort']->count_verifikasi)}}
        </td>
        <td>
            <p><b>Tervalidasi</b></p>
            {{number_format($d->rekap['sort']->count_validasi)}}
        </td>
       
        <td>
            <p><b>Verifikasi</b></p>
            {{number_format($d->rekap['long']->count_verifikasi)}}
        </td>
        <td>
            <p><b>Tervalidasi</b></p>
            {{number_format($d->rekap['long']->count_validasi)}}
        </td>
       
    </tr>    
@endforeach
</table>    --}}

@stop