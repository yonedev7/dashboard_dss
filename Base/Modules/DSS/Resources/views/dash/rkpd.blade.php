@extends('adminlte::page')

@php
// dd($rekap);    

@endphp
@section('content')
   <div class="">

    <div class="bg-white p-2 rounded">
        <div class="row">
            <div class="col-md-12 text-center border-bottom mb-2">
                <h3>Rekap Keterisian Data RKPD tahun {{$StahunRoute}}</h3>
            </div>
            <div class="col-md-4 text-center border-right">
                <h3><b>Data Nasional</b></h3>
                <p><span class="badge badge-primary" >{{$rekap['nasional']->percentage_validasi}}%</span> {{$rekap['nasional']->count_validasi}} / {{$rekap['nasional']->target}}</p>

            </div>
            <div class="col-md-4 text-center border-right" >
                <h3><b>Data Longlist</b></h3>
                <p><span class="badge badge-primary" >{{$rekap['longlist']->percentage_validasi}}%</span> {{$rekap['longlist']->count_validasi}} / {{$rekap['longlist']->target}} </p>


            </div>
            <div class="col-md-4 text-center">
                <h3><b>Data Sortlist</b></h3>
                <p><span class="badge badge-primary" >{{$rekap['sortlist']->percentage_validasi}}%</span> {{$rekap['sortlist']->count_validasi}} / {{$rekap['sortlist']->target}}</p>
            </div>
        </div>
    </div>
   </div>
<div class="" id="app">
    <div class="d-flex">
        <charts :options="chart_1" ></charts>  
    </div>
</div>
@endsection


@section('js')
<script src="{{asset('js/dss-final.js')}}"></script>
<script>

    var DD=new Vue({
        el:'app',
        data:{
            chart_1:{
                chart:{
                    type:'map'
                },
                series:[
                    {
                        map
                    }
                ]
            }
        }
    })
</script>


@stop