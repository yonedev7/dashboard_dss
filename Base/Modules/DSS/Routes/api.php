<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/dss', function (Request $request) {
    return $request->user();
});


Route::prefix('dataset-mod-air-minum/{tahun}')->name('api.dss')->group(function(){
    Route::prefix('rkpd')->group(function(){
        Route::post('showcase-dashboard','RKPDController@showcaseDashboard')->name('.rkpd.showcase');
        
    });

    Route::prefix('apbd')->group(function(){
        Route::post('showcase-dashboard','APBDController@showcaseDashboard')->name('.apbd.showcase');
        
    });
    Route::prefix('pmpd')->group(function(){
        Route::post('showcase-dashboard','PMPDController@showcaseDashboard')->name('.pmpd.showcase');
        Route::post('showcase-dashboard-2','PMPDController@showcaseDashboard2')->name('.pmpd.showcase-2');

    });

    Route::prefix('ddub')->group(function(){
        Route::post('showcase-dashboard','DDUBController@showcaseDashboard')->name('.ddub.showcase');
        Route::post('showcase-dashboard-2','DDUBController@showcaseDashboard2')->name('.ddub.showcase-2');

        
    });
});
