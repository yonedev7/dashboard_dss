<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('dash/{tahun}/sistem-informasi/dss')->group(function() {

    Route::get('table-control/{context}','DatasetController@_viewTableControl')->name('dss.table-control');

    Route::prefix('rkpd')->group(function(){
        Route::prefix('admin')->group(function(){
            Route::get('table-control','RKPDController@table_control')->name('dss.rkpd.admin.table-ctrl');
        });
    });

    Route::prefix('apbd')->group(function(){
        Route::prefix('admin')->group(function(){
            Route::get('table-control','APBDController@table_control')->name('dss.apbd.admin.table-ctrl');
        });
    });
});

Route::prefix('session/{tahun}/dss')->group(function() {
    Route::prefix('rkpd')->group(function(){
        Route::get('rekap-regional','RKPDController@rekapRegional')->name('dss.rkpd.dash.r-regional');
    });
    Route::prefix('kpi')->group(function(){
        Route::get('rekap','TematikKpiController@index')->name('dss.kpi.dash');
    });
});;




