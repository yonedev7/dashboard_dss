<?php

return [
    'name' => 'DashboardAdmin',
    'dash'=>[
            
        ['ROW',0,[
               ['COL',0,[
               ['TITLE','','']

               ]],
            ]
        ]
    ],
    'component'=>[
        'ROW'=>[
            'title'=>'Row',
            'des'=>'Menambahkan Area Data Kesisi Bahwah Tampilan',
            'entity'=>[
                    [NULL,'ROW'],
                    [NULL,0],
                    [NULL,[]]
            ]
        ],
        'COL'=>[
            'title'=>'Column',
            'des'=>'Menambahkan Area Data Kesisi Kanan Tampilan',
            'entity'=>[
                    [NULL,'COL'],
                    [NULL,0],
                    [NULL,[]]
            ]
        ],
        'TITLE'=>[
            'title'=>'Mark Down',
            'des'=>'Menambahkan Area Data Kesisi Kanan Tampilan',
            'entity'=>[
                    [NULL,'TITLE',],
                    ['input-text','','Title'],
                    ['input-markdown','','Description']
            ]
        ],
        'RKPD_JP'=>[
            'title'=>'Rencana Kegiatan Dalam Pengembangan Jaringan Perpipaan',
            'des'=>'',
            'method'=>['per_sub'],
            'type'=>['line','column'],
            'tahun_type'=>'single_year',
            'entity'=>[
                    [NULL,'COMDATA'],
                    [NULL,12],
                    [NULL,'','Title'],
                    [NULL,'RKPD_JP',''],
                    [NULL,'per_sub',''],
                    [NULL,'column','Pilihan Chart'],



            ]
        ]
    ]
];
