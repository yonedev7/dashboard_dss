<?php

namespace Modules\DashboardAdmin\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use GuzzleHttp\Client;
use Auth;
use App\Models\User;
class DashboardAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

    public static function postWa($tahun,Request $request){
        $My=Auth::User();
        if($My){
            $My=User::find($My->id);
        }

        $phone=json_decode($My->phone_number??'[]',0);


        if(isset($phone[2])){
            $phone=str_replace('+','',$phone[2]);
            if(!str_contains($phone,'@')){
                $phone=$phone.'@c.us';
            }
        }

        if($request->number){
            $phone=$request->number;
        
        }
        if($request->user){
            $user=$request->user;
        }
        else{
            $user=$My->id;
        }

        if($phone AND $user){

            $clientX = new Client([
                // Base URI is used with relave requests
                'base_uri' => 'http://127.0.0.1:'.env('PORT'),
                // You can set any number of default request options.
                'timeout'  => 600.0,

            ]);


            try {
              $response = $clientX->request('POST', $request->method,[
                  'allow_redirects' => true,
                  'json'=>([
                      "type"=>"from-url",
                      "url"=>$request->url_dom,
                      "user"=>$user,
                      "number"=>$phone,
                      'dom_html'=>$request->dom_html,
                      "dom_selected"=>$request->dom_selected,
                      "name_file"=>$request->name_file
                  ])
              ]);
            } catch (\Exception $e) {
                return [
                    'code'=>500,
                    'error'=>$e,
                    'node'=>[
                        'host'=> 'http://127.0.0.1:'.env('PORT'),
                        'method'=>$request->method
                    ],
                    'payload'=>([
                        "type"=>"from-url",
                        "url"=>$request->url_dom,
                        "user"=>$user,
                        "number"=>$phone,
                        'dom_html'=>$request->dom_html,
                        "dom_selected"=>$request->dom_selected,
                        "name_file"=>$request->name_file
                    ])
                ];

            }




            if($response->getStatusCode()==200){
                    $json=json_decode($response->getBody(),true);
                    if($json['code']==200){
                        return [
                            'code'=>200,
                            'data'=>$json,
                            'target'=>$phone
                        ];
                    }
                }






        }
        return [
            'code'=>500
        ];


    }

    public function index($tahun,$id_dataset)
    {
        $dataset=DB::table('master.dataset')->find($id_dataset);
        if($dataset){
            $table=$dataset->table;
            $table_status='dts_'.$dataset->id;
            if(str_contains($table,'[TAHUN]')){
                $table=str_replace('[TAHUN]',$tahun,$table);
                $table_status.='_'.$tahun;
            }
            $exist_table=(int)DB::table('pg_tables')->whereIn('tablename',[$table,$table_status])->whereIn('schemaname',['dataset','status_data'])->count();
            if($exist_table>=2){

                return view('dashboardadmin::index')->with(['dataset'=>$dataset]);
            }else{
                return redirect()->route('mod.dash.admin.index',['tahun'=>$tahun-1,'id_dataset'=>$id_dataset]);
            }

        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('dashboardadmin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('dashboardadmin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('dashboardadmin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
