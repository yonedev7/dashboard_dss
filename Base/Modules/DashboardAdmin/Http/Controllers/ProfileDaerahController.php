<?php

namespace Modules\DashboardAdmin\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
class ProfileDaerahController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index($tahun,$kodepemda=null)
    {
        
        $pemdas=DB::table('master.master_pemda as mp')->join('master.pemda_lokus as lk','lk.kodepemda','=','mp.kodepemda')
        ->selectRaw("mp.kodepemda,max(mp.nama_pemda),string_agg(concat(lk.tipe_bantuan,'-',lk.tahun_proyek),', ') as nuwas")
        ->orderBy('mp.kodepemda')->groupBy('mp.kodepemda')->get();
        $pemda=(Object)[
            'kode'=>null,
            'name'=>'Nasional'
        ];

        $lis_com=[];
        if(file_exists(storage_path('app/skema_tampilan/'.$tahun.'/profile_pemda.json'))){
            $lis_com=json_decode(file_get_contents(storage_path('app/skema_tampilan/'.$tahun.'/profile_pemda.json')));
        }else{
            $lis_com=[['ROW',0,['x']]];
        }

        if($kodepemda){
            $pemda=DB::table('master.master_pemda')->where('kodepemda',$kodepemda)->selectRaw(
                "kodepemda as kode,nama_pemda as name"
            )->first();
            if(file_exists(storage_path('app/skema_tampilan/'.$tahun.'/profile_pemda_'.$kodepemda.'.json'))){
                $lis_com=json_decode(file_get_contents(storage_path('app/skema_tampilan/'.$tahun.'/profile_pemda_'.$kodepemda.'.json')));

            }

        }
        
        $components=json_decode(file_get_contents(storage_path('app/component_data/com_pemda.json')),true);

        return view('dashboardadmin::dash')->with(['pemda'=>$pemda,'list_pemda'=>$pemdas,'list_schema'=>$lis_com,'component_list'=>$components]);
    }


    

    
}
