<?php

namespace Modules\DashboardAdmin\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
class TableControlController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

    public function index($tahun,Request $request){
        return view('dashboardadmin::table_control');
    }

    public function load($tahun,Request $request )
    {
    

        $pemda_reg=[
            'NAS'=>[
                'PEMDA'=>DB::table('master.master_pemda as mp')
                ->groupBy('mp.kodepemda','mp.nama_pemda')
                ->where('mp.kodepemda','!=','0')
                ->leftJoin('master.pemda_lokus as lk','mp.kodepemda','=','lk.kodepemda')->selectRaw('null as status,mp.kodepemda,mp.nama_pemda'),
                'STATE'=>[],
                'SORTLIST'=>[]

            ],
            'LONG'=>[
                'PEMDA'=>DB::table('master.master_pemda as mp')->join('master.pemda_lokus as lk','mp.kodepemda','=','lk.kodepemda')->selectRaw('null as status,mp.kodepemda,mp.nama_pemda')->groupBy('mp.kodepemda','mp.nama_pemda'),
                'STATE'=>[],
                'SORTLIST'=>[]


            ],
            'I'=>[
                'PEMDA'=>DB::table('master.master_pemda as mp')->join('master.pemda_lokus as lk','mp.kodepemda','=','lk.kodepemda')->where('mp.regional_1','I')->selectRaw('null as status,mp.kodepemda,mp.nama_pemda')->groupBy('mp.kodepemda','mp.nama_pemda'),
                'STATE'=>[],
                'SORTLIST'=>[]

            ],
            'II'=>[
                'PEMDA'=>DB::table('master.master_pemda as mp')->join('master.pemda_lokus as lk','mp.kodepemda','=','lk.kodepemda')->where('mp.regional_1','II')->selectRaw('null as status,mp.kodepemda,mp.nama_pemda')->groupBy('mp.kodepemda','mp.nama_pemda'),

                'STATE'=>[],
                'SORTLIST'=>[]

            ],
            'III'=>[
                'PEMDA'=>DB::table('master.master_pemda as mp')->join('master.pemda_lokus as lk','mp.kodepemda','=','lk.kodepemda')->where('mp.regional_1','III')->selectRaw('null as status,mp.kodepemda,mp.nama_pemda')->groupBy('mp.kodepemda','mp.nama_pemda'),
                'STATE'=>[],
                'SORTLIST'=>[]

            ],
            'IV'=>[
                'PEMDA'=>DB::table('master.master_pemda as mp')->join('master.pemda_lokus as lk','mp.kodepemda','=','lk.kodepemda')->where('mp.regional_1','IV')->selectRaw('null as status,mp.kodepemda,mp.nama_pemda')->groupBy('mp.kodepemda','mp.nama_pemda'),
                'STATE'=>[],
                'SORTLIST'=>[]

            ],
            

        ];

        $return=[];
        $dataset=DB::table('master.dataset as d')
        ->leftJoin('kpi_data_monev as km','km.id_data','=','d.id')
        ->leftJoin('master.menus as menu','menu.id','=','d.id_menu')
        ->leftJoin('kpi as kpi','kpi.id','=','km.id_kpi')
        ->select('d.id','menu.title as menu',DB::raw("(case when kpi.name IS NOT NULL then kpi.name else 'LAINYA' end) as kpi"),'d.name','d.jenis_dataset','d.login','d.klasifikasi_1','d.klasifikasi_2','d.table')
        ->orderBy('kpi.name','asc')
        ->orderBy('d.index','asc')->get()->toArray();
        

        foreach($dataset as $k=>$d){
            $tb=$d->table;
            $st='dts_'.$d->id;
            if(str_contains($d->table,"[TAHUN]")){
                $tb=str_replace("[TAHUN]",$tahun,$d->table);
                $st.='_'.$tahun;
            }
            $exist_table=(int)DB::table('pg_tables')->whereIn('tablename',[$tb,$st])->whereIn('schemaname',['dataset','status_data'])->count();
            if($exist_table>=2){
                

                foreach($pemda_reg as $reg=>$rg){
                   
                    if(!isset($return[$reg])){
                        
                        $return[$reg]=[
                            'PEMDA'=>(clone $rg['PEMDA'])->get()->toArray(),
                            'STATE'=>[]
                        ];
                    }

              

                if(!isset($return[$reg]['STATE'][$tb])){
                    if($d->klasifikasi_2 AND $d->klasifikasi_2!='[]'){
                        $tph=json_decode(strtoupper(str_replace('Bantuan ','',$d->klasifikasi_2)),true);
                        
                        
                        





                        $pemda=(clone $rg['PEMDA'])->whereIn('lk.tipe_bantuan',$tph);
                        $lpemda=(clone $rg['PEMDA'])->whereIn('lk.tipe_bantuan',$tph);
                    }else{
                        $pemda=clone $rg['PEMDA'];
                        $lpemda=(clone $rg['PEMDA']);

                    }
                    $return[$reg]['STATE'][$tb]=[
                        'id'=>$d->id,
                        'name'=>$d->name,
                        'tahun'=>$tahun,
                        'PEMDA'=>$pemda->get()->toArray(),
                        'STATE'=>[],
                        'SORTLIST'=>[
                            'PEMDA'=>$lpemda->where('lk.tahun_proyek',$tahun)->get()->toArray(),
                            'STATE'=>[]
                        ]
                    ];
                }

                    $return[$reg]['STATE'][$tb]['STATE'] =(array)DB::table('dataset.'.$tb." as d")
                    ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
                    ->leftJoin('status_data.'.$st.' as std',[['std.kodepemda','=','d.kodepemda'],['std.tahun','=','d.tahun']])
                    ->whereIn('d.kodepemda',array_map(function($el){return $el->kodepemda;},$return[$reg]['STATE'][$tb]['PEMDA']))
                    ->where('d.tahun','=',$tahun)
                    ->selectRaw("string_agg(distinct(concat(d.kodepemda,'@',(case when std.status is not null then std.status else 0 end))),',') as kdpemdas,count(distinct(d.kodepemda)) as terdata,count(DISTINCT(case when (std.status =2) then d.kodepemda else null end)) as tervalidasi,
                    count(DISTINCT(case when (std.status in (1,2)) then d.kodepemda else null end)) as terverifikasi")->first();
                    
                   

                    $return[$reg]['STATE'][$tb]['SORTLIST']['STATE']=(array)DB::table('dataset.'.$tb." as d")
                    ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
                    ->leftJoin('status_data.'.$st.' as std',[['std.kodepemda','=','d.kodepemda'],['std.tahun','=','d.tahun']])
                    ->whereIn('d.kodepemda',array_map(function($el){return $el->kodepemda;},$return[$reg]['STATE'][$tb]['SORTLIST']['PEMDA']))
                    ->where('d.tahun','=',$tahun)
                    ->selectRaw("string_agg(distinct(concat(d.kodepemda,'@',(case when std.status is not null then std.status else 0 end))),',') as kdpemdas,count(distinct(d.kodepemda)) as terdata,count(DISTINCT(case when (std.status =2) then d.kodepemda else null end)) as tervalidasi,
                    count(DISTINCT(case when (std.status in (1,2)) then d.kodepemda else null end)) as terverifikasi")->first();
                    $status=explode(',',$return[$reg]['STATE'][$tb]['STATE']['kdpemdas']);

                    $status_sort=explode(',',$return[$reg]['STATE'][$tb]['SORTLIST']['STATE']['kdpemdas']);

                    
                    foreach($return[$reg]['STATE'][$tb]['SORTLIST']['PEMDA'] as  $i=>$p){
                       foreach($status as $stx){
                            if(str_contains($stx,$p->kodepemda.'@')){
                                $return[$reg]['STATE'][$tb]['SORTLIST']['PEMDA'][$i]->status=(int)explode('@',$stx)[1];
                            }
                       }
                    }
                    

                    $status=explode(',',$return[$reg]['STATE'][$tb]['STATE']['kdpemdas']);
                    foreach(  $return[$reg]['STATE'][$tb]['PEMDA'] as  $i=>$p){
                        if(!isset($p->kodepemda)){
                        }
                       foreach($status as $stx){
                        if(str_contains($stx,$p->kodepemda.'@')){
                            $return[$reg]['STATE'][$tb]['PEMDA'][$i]->status=(int)explode('@',$stx)[1];
                        }
                       }
                    }
                }
            
            }
                
        }

        return array_map(function($el){
            $old=$el;
            $old['STATE']=array_values($old['STATE']);
            $old['SORTLIST']=array_values($old['SORTLIST']??[]);

            return $old;
        },$return);


    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('dashboardadmin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('dashboardadmin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('dashboardadmin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
