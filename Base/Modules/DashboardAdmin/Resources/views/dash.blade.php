@extends('adminlte::page')
@section('css')
<style>
    .loading-wrapper{
        
    }
</style>

@stop
@section('content_header')
    <h4>Profil Pemda {{$pemda->name}}</h4>
    <small>Tahun {{$StahunRoute}}</small>
    <hr>
@endsection
@section('content')
    <div id="app" class="p-2">
        <div class="row bg-white mb-2 pt-2" v-if="obj.kode==null">
            <div class="col-2">
                <div class="form-group">
                    <label for="">Lokus</label>
                    <select name="" class="form-control" v-model="r.lokus" id="">
                        <option value="N">Nasional</option>
                        <option value="L">Longlist</option>
                        <option value="S">SortList</option>

                    </select>
                </div>
            </div>
            <div class="col-2">
                <div class="form-group">
                    <label for="">Status Data</label>
                    <select name="" class="form-control" v-model="r.status" id="">
                        <option value="N">Terinput</option>
                        <option value="2">Tervalidasi</option>

                    </select>
                </div>
            </div>
            <div class="col-2" v-if="['L','S'].includes(r.lokus)">
                <div class="form-group">
                    <label for="">Regional</label>
                    <select name="" class="form-control" v-model="r.regional" id="">
                        <option value="N">Semua</option>
                        <option value="I">Regional I</option>
                        <option value="II">Regional II</option>
                        <option value="III">Regional III</option>
                        <option value="IV">Regional IV</option>

                    </select>
                </div>
            </div>
            {{-- <div class="col-4" v-if="['L','S'].inludes(r.lokus)">
                <div class="form-group">
                    <label for="">Tipe Bantuan</label>
                    <select name="" class="form-control" v-model="r.regional" id="">
                        <option value="I">Regional I</option>
                        <option value="II">Regional II</option>
                        <option value="III">Regional III</option>
                        <option value="IV">Regional IV</option>
                    </select>
                </div>
            </div> --}}
        </div>
        <div class=" col-12 mb-2 bg-white rounded p-2 shadow">
            <button class="btn btn-warning " @click="editableToogle">@{{editable?'Preview Mode':'Edit Mode'}}</button>
            <button v-if="editable" class="btn btn-primary" @click="saveMode">Save</button>
        </div>
        <div :class="(editable?'row m-0':'') ">
             <div :class="(editable?'col-12 bg-white rounded border-white border shodow':'') ">
                <template  v-for="com,i in list_schema">
                    <dash-pre :editable="editable" v-on:remove-com="removeElement" v-on:emit-from-pre="editCom" :pointer="[i]"     :index-com.lazy="i"  :with-break-row="true" :filter.sync="filter" :com.sync="com" url_schema="{{route('api-web-public-mod.dataset.cluster.show',['tahun'=>'@TAHUN','context'=>'@CONTEXT'])}}"></dash-pre>
                </template> 
            </div>
       </div>
       <div ref="modal_save" v-if="obj.kode" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content" >
                <div class="modal-header">
                    <h5 class="modal-title" id="my-modal-title">Simpan Sekema Tampilan</h5>
                    <button class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="btn-group">
                        <button class="btn btn-primary" @click="save()">Sebagai Default Tampilan</button>
                        <button class="btn btn-warning text-capitalize"  @click="save(obj.kode)">Sebagai Tampilan Pada @{{for_tampilan.replaceAll('_',' ')}} @{{obj.name}}</button>

                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
       </div>
    </div>
@stop

@section('js')
    <script>
        let component_builder_dash_pemda_list=<?=json_encode($component_list)?>;

        var app=new Vue({
            el:'#app',
            data:{
                r:{
                    lokus:'N',
                    regional:'N',
                    status:'N'

                },
                obj:{
                    kode:null,
                    name:''
                },
                for_tampilan:'profile_pemda',
                list_schema:[],
                filter:{
                    filter_regional:[],
                    filter_kodepemdas:[],
                    filter_lokus:'',
                    tahun:<?=$StahunRoute?>
                },
                editable:false,
                old:<?=json_encode($list_schema)?>,
                component_builder_dash_pemda_list:<?=json_encode($component_list)?>
            }, 
            watch:{
                "editable":function(val){
                    this.$forceUpdate();
                    console.log('editable status',val);
                },
                "list_schema":{
                    handler:function(el){
                    console.log('444',this.list_schema);
            
                    },
                    immediate: true,
                    flush:'post'
                },
                
                r:{
                    deep:true,
                    handler:function(){
                        this.filterChange();
                    }
                }
                
            },
            created:function(){
                var self=this;
                this.filterChange();
                setTimeout((ref) => {
                    ref.list_schema=Object.assign([],ref.old);
                }, (10),self);

                this.obj=<?=json_encode($pemda)?>;
                if(this.obj.kode){
                    this.filter.filter_kodepemdas=[this.obj.kode];

                }

            },
            computed:{
                com_list:function(){
                    return this.list_schema;
                }
            },
            methods:{
                filterChange:function(){
                    if(this.r.regional!='N'){
                        this.filter.filter_regional=[this.r.regional];
                    }else{
                        this.filter.filter_regional=[];
                    }

                    if(this.r.status!='N'){
                        this.filter.filter_status=[this.r.status];
                    }else{
                        this.filter.filter_status=[];
                    }

                    if(this.r.lokus){
                        this.filter.filter_lokus=this.r.lokus;
                        if(this.r.lokus=='N'){
                            this.filter.filter_regional=[];

                        }
                    }
                },
                save:function(kode){
                    req_ajax.post('{{route('api-web.mod.dashboard.save_schema_tampilan',['tahun'=>$StahunRoute,'context'=>'@?','kode'=>'@?'])}}'.yoneReplaceParam('@?',[this.for_tampilan,this.obj.kode]),{
                        schema_key:this.for_tampilan,
                        schema_kode:kode,
                        shcema:this.list_schema
                    }).then(res=>{

                    }).finally(function(){

                    });
                },
                saveMode:function(){
                    if(this.obj.kode){
                        $(this.$refs.modal_save).modal();
                    }else{
                        this.save(this.obj.kode);
                    }
                },
                editableToogle:function(){
                    if(this.editable){
                        this.editable=false;
                    }else{
                        this.editable=true;
                    }
                },
                removeElement:function(index){
                    delete this.list_schema[2][index];
                    this.list_schema[2]=Object.values(this.list_schema[2]);
                },
                editCom:function(com,index){
                    var self=this;
                    this.list_schema[index]=Object.assign(this.list_schema[index],com);
                    this.$forceUpdate();
                    console.log(this.list_schema);


                  

                },
               
                

        
               
            }
        })
    </script>


@stop