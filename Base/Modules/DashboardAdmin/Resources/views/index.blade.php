@extends('adminlte::page')

@section('content_header')
    <h4>{{$dataset->name}}</h4>
    <hr>
@endsection
@section('content')
<div class="p-2" id="app">
    {{-- <button class="btn btn-primary" @click="jsondata.push({})"> <i class="fa fa-plus"></i></button>
    <div class="col-12">
            <vue-excel-editor v-model="jsondata" allow-add-col="true" page="20">
                <vue-excel-column field="birth"  label="Date Of Birth" type="date"   ></vue-excel-column>

                <vue-excel-column field="name"   label="Name"          type="string" ></vue-excel-column>
                <vue-excel-column field="user"   label="User ID"       type="string"  ></vue-excel-column>

                <vue-excel-column field="phone"  label="Contact"       type="string"  ></vue-excel-column>
                <vue-excel-column field="gender" label="Gender"        type="select" :options="['F','M','U']" ></vue-excel-column>
                <vue-excel-column field="age"    label="Age"           type="number"  ></vue-excel-column>
            </vue-excel-editor>
    </div> --}}

   
   <section class="container-fuild">
    <div class="row">
        <div class="col-6">
            <div class="bg-white rounded shadow p-3 mb-2 position-sticky sticky-top" style="top:55px;">
            <v-dash-dts-keterisian-chart    link="{{route('api-web-mod.dataset.data.series.column.keterisian',['tahun'=>'@?','id_dataset'=>'@?'])}}" :tahun="{{$StahunRoute}}" :dataset="dataset" ></v-dash-dts-keterisian-chart>

            </div>
        </div>
        <div class="col-6">
            <div class="row">
                <div class="bg-white rounded shadow p-3 col-12 mb-2">
                    <v-dash-dts-keterisian-chart :with-title="true" regional="REGIONAL 1"   link="{{route('api-web-mod.dataset.data.series.column.keterisian',['tahun'=>'@?','id_dataset'=>'@?'])}}" :tahun="{{$StahunRoute}}" :dataset="dataset" ></v-dash-dts-keterisian-chart>

                </div>
                <div class="bg-white rounded shadow p-3 col-12 mb-2">
                    <v-dash-dts-keterisian-chart :with-title="true" regional="REGIONAL 2"   link="{{route('api-web-mod.dataset.data.series.column.keterisian',['tahun'=>'@?','id_dataset'=>'@?'])}}" :tahun="{{$StahunRoute}}" :dataset="dataset" ></v-dash-dts-keterisian-chart>

                </div>
             
                <div class="bg-white rounded shadow p-3 col-12 mb-2">
                    <v-dash-dts-keterisian-chart :with-title="true" regional="REGIONAL 3"   link="{{route('api-web-mod.dataset.data.series.column.keterisian',['tahun'=>'@?','id_dataset'=>'@?'])}}" :tahun="{{$StahunRoute}}" :dataset="dataset" ></v-dash-dts-keterisian-chart>

                </div>
                <div class="bg-white rounded shadow p-3 col-12 mb-2">
                    <v-dash-dts-keterisian-chart :with-title="true" regional="REGIONAL 4"   link="{{route('api-web-mod.dataset.data.series.column.keterisian',['tahun'=>'@?','id_dataset'=>'@?'])}}" :tahun="{{$StahunRoute}}" :dataset="dataset" ></v-dash-dts-keterisian-chart>

                </div>
             
             
                
            </div>
        </div>
    </div>
</section>
</div>
    
@endsection


@section('js')
    <script>
        var app=new Vue({
            el:'#app',
            data:{
                dataset:<?=json_encode($dataset)?>
            }
        })
    </script>

@stop