@extends('adminlte::page')

@section('content_header')
    <h4>Table Control</h4>
    <hr>
@endsection
@section('content')
<div id="app">

   <div class="p-2">
    <div class="p-3 rounded shadow bg-white mb-2" v-if="print_mode==false">
        <div class="row">
            <div class="col-3">
                <div class="form-group">
                    <label for="">Lingkup</label>
                    <select name="" class="form-control" v-model="search.cat" id="">
                        <option value=""></option>
                        <option :value="op[1]" v-for="op in Object.entries(urutan)">@{{op[1]}}</option>
                    </select>
                </div>
                
            </div>
            <div class="col-3">
                <div class="form-group">
                    <label for="">Cari Dataset</label>
                    <input type="text" class="form-control" v-model="search.q" placeholder="...">
                </div>
            </div>
        </div>
    </div>
    <div id="print_content">
        <div class="p-3 rounded shadow bg-white mb-2 "  v-for="sc,i in com_urutan">
            <div v-if="sc[0]!='NAS'" class="pagebreak"></div>
            <table class="table table-bordered">
                <thead >
                    <tr v-if="print_mode">
                        <th colspan="9">
                            <div id="header-dss" class="text-uppercase align-self-center">
                                <span><img src="{{asset('assets/img/brand/logo.png')}}" height="20px" alt=""></span>    <b class="my-3">{{Carbon\Carbon::now()->format('d F Y h:i a')}}</b>
                            </div>
                        </th>

                    </tr>
                    <tr :class="print_mode?'':'bg-secondary'">
                        <th colspan="9"    > 
                            @{{sc[1]}} -
                        <template v-if="data[sc[0]]!=undefined">
                            <i class="fa fa-bullseye "></i> @{{data[sc[0]]['PEMDA'].length}} Pemda
                        </template>
                        </th>
                    </tr>
                    <tr :class="print_mode?'':'bg-primary'">
                        <th>Dataset</th>
                        <th>Target Keterisian (L) </th>
                        <th>Data Terinput (L)</th>
                        <th>Data Tervalidasi (L)</th>
                        <th>Data Terverifikasi (L)</th>
                        <th>Target Keterisian (S)</th>
                        <th>Data Terinput (S)</th>
                        <th>Data Terverifikasi (S)</th>
                        <th>Data Tervalidasi (S)</th>
    
                        
    
                    </tr>
                </thead>
                <tbody>
           <template v-if="data[sc[0]]!=undefined">
            <template class="ml-2 shadow rounded bg-white mt-2 p-3" v-for="item in filter_dts(data[sc[0]]['STATE'])">
                        
                   
                        <tr>
                            <td>@{{item.name}} Tahun @{{item.tahun}}</td>
                            <td :class="print_mode?'':'bg-primary '"> @{{item.PEMDA.length}} Pemda   </td>
                            <td :class="print_mode?'':'bg-primary disabled'">@{{item.STATE.terdata}} Pemda (@{{percentageValue(item.STATE.terdata,item.PEMDA.length)}}%)</td>
                            

                            <td :class="print_mode?'':'bg-primary disabled'">@{{item.STATE.terverifikasi}} Pemda (@{{percentageValue(item.STATE.terverifikasi,item.PEMDA.length)}}%)</td>
                            <td :class="print_mode?'':'bg-primary disabled'" >@{{item.STATE.tervalidasi}} Pemda (@{{percentageValue(item.STATE.tervalidasi,item.PEMDA.length)}}%)</td>
                            <td  :class="print_mode?'':'bg-info '">@{{item.SORTLIST.PEMDA.length}} Pemda</td>


                            <td :class="print_mode?'':'bg-info diabled'">@{{item.SORTLIST.STATE.terdata}} Pemda (@{{percentageValue(item.SORTLIST.STATE.terdata,item.SORTLIST.PEMDA.length)}}%)</td>
    
                            <td :class="print_mode?'':'bg-info diabled'">@{{item.SORTLIST.STATE.terverifikasi}} Pemda (@{{percentageValue(item.SORTLIST.STATE.terverifikasi,item.SORTLIST.PEMDA.length)}}%)</td>
    
                            <td :class="print_mode?'':'bg-info diabled'" >@{{item.SORTLIST.STATE.tervalidasi}} Pemda (@{{percentageValue(item.SORTLIST.STATE.tervalidasi,item.SORTLIST.PEMDA.length)}}%)</td>
    
                            
                            
    
    
                        </tr>
              
    
            </template>
           </template>
                </tbody>
            </table>
           
        </div>
    </div>
    
    
   </div>
</div>
@stop

@section('js')
<script>
    var app=new Vue({
        el:'#app',
        data:{
            print_mode:false,
            urutan:{
                'NAS':'NASIONAL',
                'LONG':'LONGLIST',
                'I':'REGIONAL I',
                'II':'REGIONAL II',
                'III':'REGIONAL III',
                'IV':'REGIONAL IV'
            },
            search:{
                q:'',
                cat:'',
            },
            data:[]
        },
        computed:{
            com_urutan:function(){
                return Object.entries(this.urutan).filter(el => {
                    console.log(el[1],this.search.cat);
                    return (el[1]==(this.search.cat) ||  this.search.cat=='' );
                });
            },
        
        },
        created:function(){
            this.loadData();
         
        },
        methods:{
            activePrintMode:function(dom_selector=null){
                this.print_mode=true;
                if(dom_selector==null){
                var dom=$('#print_content').html();

                }else{
                var dom=$(dom_selector).html();

                }
                // this.print_mode=false;
                return dom;
            },
            filter_dts:function(data){
                return data.filter(el=>{
                    return (el.name||'').toLowerCase().includes((this.search.q||'').toLowerCase());
                });
            },
            percentageValue:function(val,target){
               return (((parseInt(val)/parseInt(target)))*100).toFixed(2);
            },
            loadData:function(){
                var self=this;
                req_ajax.post(('{{route('mod.dash.admin.api.tb.load',['tahun'=>$StahunRoute])}}')).then(res=>{
                    self.data=res.data;
                    console.log(self.data);
                });
            }
        }
    })
</script>

@stop