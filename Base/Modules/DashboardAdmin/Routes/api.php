<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('public-api/{tahun}/mod-dash-admin')->name('mod.dash.admin.api')->middleware('auth:api')->group(function() {
    Route::post('/table-control/load', 'TableControlController@load')->name('.tb.load');

    Route::post('request-to-wa-send-file','DashboardAdminController@postWa')->name('.wa.send-content-web');
    
});

Route::middleware('auth:api')->get('/dashboardadmin', function (Request $request) {
    return $request->user();
});