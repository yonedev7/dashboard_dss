<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('dash/{tahun}/sistem-informasi/mod-dash-admin')->name('mod.dash.admin')->group(function() {
    Route::get('/show-frame/{id_dataset}', 'DashboardAdminController@index')->name('.index');
    Route::get('/table-control', 'TableControlController@index')->name('.tb.control.index');
    Route::get('/dash-pemda/{kodepemda?}', 'ProfileDaerahController@index')->name('.dash.pemda');


    

});
