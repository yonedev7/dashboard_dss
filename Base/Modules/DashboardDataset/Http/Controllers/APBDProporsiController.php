<?php

namespace Modules\DashboardDataset\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Http\Controllers\FilterCtrl;
use DB;
use Str;

class APBDProporsiController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    static $meta=[
        'title'=>'',
        'keterangan'=>'',
        'dataset'=>104,
        'table_master_nomen'=>'master.master_nomen_90',
        'filter_master_nomen'=>['level','=',6],
        'column_table'=>[
           [
              'name'=>'nama_pemda',
              'label'=>'Nama Pemda',
              'tipe_nilai'=>'string', 
              'cluster'=>false,
           ],
           
           
        ],
        'group_nomen'=>[
          
           
        ],
        'filter_nomen'=>[
           'jenis_pemda'=>[
              'provinsi'=>['1.03.03.1.01.01','1.03.03.1.01.02','1.03.03.1.01.03','1.03.03.1.01.04','1.03.03.1.01.05','1.03.03.1.01.06','1.03.03.1.01.07','1.03.03.1.01.08','1.03.03.1.01.09','1.03.03.1.01.10','1.03.03.1.01.11','1.03.03.1.01.12'],
              'kabkot'=>['1.03.03.2.01.01','1.03.03.2.01.02','1.03.03.2.01.03','1.03.03.2.01.04','1.03.03.2.01.05','1.03.03.2.01.06','1.03.03.2.01.07','1.03.03.2.01.08','1.03.03.2.01.09','1.03.03.2.01.10','1.03.03.2.01.11','1.03.03.2.01.12','1.03.03.2.01.13','1.03.03.2.01.14','1.03.03.2.01.15','1.03.03.2.01.16','1.03.03.2.01.17','1.03.03.2.01.18','1.03.03.2.01.19','1.03.03.2.01.20','1.03.03.2.01.21'],   
           ],
           'kabkot'=>[
              'desa'=>['1.03.03.2.01.04','1.03.03.2.01.06','1.03.03.2.01.08','1.03.03.2.01.16','1.03.03.2.01.17','1.03.03.2.01.18','1.03.03.2.01.19','1.03.03.2.01.21','1.03.03.2.01.01','1.03.03.2.01.02','1.03.03.2.01.09','1.03.03.2.01.10','1.03.03.2.01.11','1.03.03.2.01.12','1.03.03.2.01.13','1.03.03.2.01.14'],
              'kabkot'=>['1.03.03.2.01.01','1.03.03.2.01.02','1.03.03.2.01.03','1.03.03.2.01.05','1.03.03.2.01.07','1.03.03.2.01.09','1.03.03.2.01.10','1.03.03.2.01.11','1.03.03.2.01.12','1.03.03.2.01.13','1.03.03.2.01.14','1.03.03.2.01.15','1.03.03.2.01.20'],
           ],
           'jenis_sub'=>[
              'pengelolaan'=>['1.03.03.2.01.09','1.03.03.2.01.10','1.03.03.2.01.11','1.03.03.2.01.08','1.03.03.2.01.20','1.03.03.1.01.06','1.03.03.2.01.21','1.03.03.1.01.09','1.03.03.2.01.13','1.03.03.2.01.15','1.03.03.2.01.16','1.03.03.1.01.08'],
              'pengembangan'=>['1.03.03.2.01.17','1.03.03.2.01.18','1.03.03.1.01.10','1.03.03.1.01.11','1.03.03.2.01.19','1.03.03.2.01.03','1.03.03.2.01.04','1.03.03.1.01.03','1.03.03.1.01.04','1.03.03.1.01.05','1.03.03.2.01.05','1.03.03.2.01.06','1.03.03.2.01.07'],
              'pengelolaandanpengembangan'=>['1.03.03.2.01.12','1.03.03.2.01.01','1.03.03.1.01.02','1.03.03.2.01.02','1.03.03.1.01.12','1.03.03.1.01.01','1.03.03.2.01.14','1.03.03.1.01.07'],   
           ],
           'tematik'=>[
              'peningkatan'=>['1.03.03.2.01.20','1.03.03.2.01.15','1.03.03.2.01.14','1.03.03.2.01.07','1.03.03.2.01.05','1.03.03.2.01.03']
           ]
        ]
       ];

     function __construct($title=null,$keterangan=null) {
           $nomen_list=DB::table('master.master_nomen_90')
           ->where('kode','ilike','1.03.03%')
           ->orderBy('name','asc')
           ->get()->toArray();
           

           $nomen_list=array_map(function($el){
              return [
                 "name"=>$el->name.' - '.($el->tipe_pemda==1?'Provinsi':'Kota/Kabupaten'),
                 'nomen'=>[$el->kode]
              ];
           },$nomen_list);

           static::$meta['group_nomen']=array_merge(static::$meta['group_nomen'],$nomen_list);
      }

    public static function index($tahun,$header=true,$title=null,$keterangan=null,Request $request){
        return view('dashboarddataset::partials.104.view_proporsi')->with([
           'meta'=>[
              'title'=>$title,
              'keterangan'=>$keterangan
           ],
           'no_header'=>!$header,
           'req'=>$request,
           'dataset'=>DB::table('master.dataset')->find(static::$meta['dataset'])
        ]);
    }

    public function getDataPemdaPropAkun($tahun,$kodesub,$kodepemda,Request $request){
      $table=DB::table('master.dataset')->find(static::$meta['dataset']);
      if(!$table){
         return [];
      }else{
         $table_status='dts_'.$table->id;
      }
   

      if(str_contains($table->table,'[TAHUN]')){
         $table_status.='_'.$tahun;
         $table->table=str_replace('[TAHUN]',$tahun,$table->table);
      }

      $exist_table=DB::table('pg_tables')->where('schemaname','dataset')->where('tablename',$table->table)->first();

 
      if(!$exist_table){
         return [];
         
      }

      $pemdas=FilterCtrl::filter_pemda($request->filter_regional,$request->filter_lokus??false,$request->filter_sortlist,$request->filter_tipe_bantuan,$request->filter_jenis_pemda??3);
  
      $data=DB::table('dataset.'.$table->table.' as d')
      ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
      ->leftjoin('master.master_nomen_90 as nn','nn.kode','=','d.kodesubkegiatan')
      ->selectRaw("concat(namaakun1,' - ',namaakun2,' - ',namaakun3) as name,sum(totalharga) as y")
      ->where('d.kodesubkegiatan',DB::raw("'".$kodesub."'"))
      ->where('d.kodepemda',DB::raw("'".$kodepemda."'"))
      ->where([
            ['d.tw','=',4],
            ['d.tahun','=',$tahun]
      ])
      ->whereRaw("d.kodepemda in ('".implode("','",$pemdas)."')")
      ->groupBy(DB::raw("concat(namaakun1,' - ',namaakun2,' - ',namaakun3)"));

      $sql= Str::replaceArray('?', $data->getBindings(), $data->toSql());
      $x=DB::table(DB::raw("(".$sql.") as x"))
      ->orderBy('x.y','desc')
      ->get();
      $data=[];
      foreach($x as $k=>$d){
         if(!isset($series[$k])){
            $data[$k]=[
               'name'=>$d->name,
               'y'=>(int)$d->y,
            ];
         }
      }


      return array_values($data);
    }
    public function getDataProporsiAkun($tahun,$kodesubkegiatan,Request $request){
      $table=DB::table('master.dataset')->find(static::$meta['dataset']);
      if(!$table){
         return [];
      }else{
         $table_status='dts_'.$table->id;
      }
   

      if(str_contains($table->table,'[TAHUN]')){
         $table_status.='_'.$tahun;
         $table->table=str_replace('[TAHUN]',$tahun,$table->table);
      }

      $exist_table=DB::table('pg_tables')->where('schemaname','dataset')->where('tablename',$table->table)->first();

 
      if(!$exist_table){
         return [];
         
      }

      $pemdas=FilterCtrl::filter_pemda($request->filter_regional,$request->filter_lokus??false,$request->filter_sortlist,$request->filter_tipe_bantuan,$request->filter_jenis_pemda??3);
  
      $data=DB::table('dataset.'.$table->table.' as d')
      ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
      ->leftjoin('master.master_nomen_90 as nn','nn.kode','=','d.kodesubkegiatan')
      ->selectRaw("max(kodeakun3) as kode_akun,concat(namaakun1,' - ',namaakun2,' - ',namaakun3) as name,
         sum(d.totalharga) as y,count(distinct(d.kodepemda)) as jumlah_pemda")
         ->where('d.kodesubkegiatan',DB::raw("'".$request->kodesub."'"))
         ->where([
            ['d.tw','=',4],
            ['d.tahun','=',$tahun]
         ])
      ->whereRaw("d.kodepemda in ('".implode("','",$pemdas)."')")
      ->groupBy(DB::raw("concat(namaakun1,' - ',namaakun2,' - ',namaakun3)"));

      $sql= Str::replaceArray('?', $data->getBindings(), $data->toSql());
      $x=DB::table(DB::raw("(".$sql.") as x"))
      ->orderBy('x.y','desc')
      ->get();
      $data=[];
      foreach($x as $k=>$d){
         
         if(!isset($data[$k])){
            $data[$k]=[
               'name'=>$d->name,
               'y'=>(int)$d->y,
               'jumlah_pemda'=>(int)$d->jumlah_pemda,
               'kode_akun'=>''.$d->kode_akun
            ];
         }
      }

      return array_values($data);

    }

    public function getDataProporsiAkunPerPemda($tahun,$kode_akun,Request $request){
      $table=DB::table('master.dataset')->find(static::$meta['dataset']);
      if(!$table){
         return [];
      }else{
         $table_status='dts_'.$table->id;
      }
   

      if(str_contains($table->table,'[TAHUN]')){
         $table_status.='_'.$tahun;
         $table->table=str_replace('[TAHUN]',$tahun,$table->table);
      }

      $exist_table=DB::table('pg_tables')->where('schemaname','dataset')->where('tablename',$table->table)->first();

 
      if(!$exist_table){
         return [];
         
      }

      $pemdas=FilterCtrl::filter_pemda($request->filter_regional,$request->filter_lokus??false,$request->filter_sortlist,$request->filter_tipe_bantuan,$request->filter_jenis_pemda??3);
  
      $data=DB::table('dataset.'.$table->table.' as d')
      ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
      ->leftjoin('master.master_nomen_90 as nn','nn.kode','=','d.kodesubkegiatan')
      ->selectRaw("mp.kodepemda,max(mp.nama_pemda) as name,sum(d.totalharga) as y")
      ->where('d.kodeakun3',DB::raw("'".$kode_akun."'"))
      ->where('d.kodesubkegiatan',DB::raw("'".$request->kodesub."'"))
      ->where([
            ['d.tw','=',4],
            ['d.tahun','=',$tahun]
      ])
      ->whereRaw("d.kodepemda in ('".implode("','",$pemdas)."')")
      ->groupBy('mp.kodepemda');

      $sql= Str::replaceArray('?', $data->getBindings(), $data->toSql());
      $x=DB::table(DB::raw("(".$sql.") as x"))
      ->orderBy('x.y','desc')
      ->get();
      $data=[];
      foreach($x as $k=>$d){
         if(!isset($series[$k])){
            $data[$k]=[
               'name'=>$d->name,
               'y'=>(int)$d->y,
            ];
         }
      }

      return array_values($data);


    }


    public function getMeta($tahun,Request $request){
        $meta=static::$meta;
        unset($meta['table_master_nomen']);
        unset($meta['filter_master_nomen']);

        if($request->filter_jenis_pemda==2){

        }else if($request->filter_jenis_pemda==1){

        }

        return $meta;
     }

     public function getChartSeries($tahun,Request $request){

        $table=DB::table('master.dataset')->find(static::$meta['dataset']);
        if(!$table){
           return [];
        }else{
           $table_status='dts_'.$table->id;
        }
     
  
        if(str_contains($table->table,'[TAHUN]')){
           $table_status.='_'.$tahun;
           $table->table=str_replace('[TAHUN]',$tahun,$table->table);
        }
  
        $exist_table=DB::table('pg_tables')->where('schemaname','dataset')->where('tablename',$table->table)->first();
  
   
        if(!$exist_table){
           return [];
           
        }
  
  
        $pemdas=FilterCtrl::filter_pemda($request->filter_regional,$request->filter_lokus??false,$request->filter_sortlist,$request->filter_tipe_bantuan,$request->filter_jenis_pemda??3);
  
  
        $data=DB::table('dataset.'.$table->table.' as d')
        ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
        ->leftjoin('master.master_nomen_90 as nn','nn.kode','=','d.kodesubkegiatan')
        ->selectRaw("d.kodesubkegiatan,max(nn.name) as name,count(distinct(d.kodepemda::text)) as jumlah_pemda,
        sum(case when d.totalharga is null then 0 else d.totalharga end) as jumlah_pagu,
           string_agg(d.kodepemda,',') as kodepemda_list")
           ->where([
              ['d.tw','=',4],
              ['d.tahun','=',$tahun]
           ])
        ->whereRaw("d.kodepemda in ('".implode("','",$pemdas)."')")
        ->groupBy('d.kodesubkegiatan');
  
  
        if(true){
           $data=$data->join('status_data.'.$table_status.' as std', DB::raw("(std.kodepemda=d.kodepemda and std.tahun=".$tahun." and std.status=2)"),DB::raw("true"));
        }
  
        $nomen=[];
        foreach(static::$meta['group_nomen'] as $k=>$n){
           $nomen=array_merge($nomen,$n['nomen']);
        }
  
  
        if($request->nomen){
        $data=$data->whereRaw("d.kodesubkegiatan in ( '".implode("','",$request->nomen)."')");
        }else{
           $data=$data->whereRaw("d.kodesubkegiatan in ('".implode("','",$nomen)."')");
        }
  
        $length = $request->input('length');
        $orderBy = $request->input('sorted_by','kodesubkegiatan'); //Index
        $orderByDir = $request->input('dir', 'desc');
        $searchValue = $request->input('search');
  
        // dd($data->toSql());
  
       
        $sql= Str::replaceArray('?', $data->getBindings(), $data->toSql());
  
  
        $data=DB::table(DB::raw("(".$sql.") as d"));
        
        
  
        if($orderBy){
           $data=$data->orderBy($orderBy,$orderByDir);
        }
  
  
        $data = $data->get();
  
        $series=[
          
           [
              'name'=>'Total Anggaran',
              'yAxis'=>1,
              'type'=>'column',
              'data'=>[]
           ],
           [
              'name'=>'Jumlah Pemda',
              'yAxis'=>0,
              'type'=>'line',
              'data'=>[]
           ],
           
           // [
           //    'name'=>'Jumlah Pemda Provinsi',
           //    'yAxis'=>0,
           //    'type'=>'area',
           //    'data'=>[]
           // ],
           // [
           //    'name'=>'Jumlah Pemda Kab/Kota',
           //    'yAxis'=>0,
           //    'type'=>'area',
           //    'data'=>[]
           // ],
  
        ];
  
        $colors=["#005a59","#99c8c8","#fb5e00","#e40000","#fcc117","#173f62","#5b8f99","#faab5c","#bf3414","#851826","#4c6a8e","#25e1d4","#ffa742","#57ef7c","#2c51e1","#96ceb4","#ffeead","#ff6f69","#ffcc5c","#88d8b0","#d9534f","#f9f9f9","#5bc0de","#5cb85c","#428bca","ffe135","#318ce7","#0d98ba","#1b4d3e","#cc5500","#5f9ea0","#c95a49","#007aa5"];
        foreach($data as $k=>$d){
           $pemda=explode(',',$d->kodepemda_list);
           $pemdakota=array_filter($pemda,function($el){
              return strlen($el)>3;
           });
           $provinsi=array_filter($pemda,function($el){
              return strlen($el)==3;
           });
  
           $series[0]['data'][]=[
              'name'=>$d->name.' ('.$d->kodesubkegiatan.')',
              'kode'=>''.$d->kodesubkegiatan,
              'color'=>$colors[$k],
              'y'=>(int)$d->jumlah_pagu??0,
              'kodepemda'=>$d->kodepemda_list
           ];
           $series[1]['data'][]=[
              'name'=>$d->name.' ('.$d->kodesubkegiatan.')',
              'y'=>(int)$d->jumlah_pemda??0,
              'kode'=>''.$d->kodesubkegiatan,
              // 'color'=>$colors[$k],
              'kodepemda'=>$d->kodepemda_list
           ];
  
           // $series[2]['data'][]=[
           //    'name'=>$d->name.' ('.$d->kodesubkegiatan.')',
           //    'type'=>'area',
           //    'kode'=>''.$d->kodesubkegiatan,
           //    'y'=>(int)count($provinsi)??0,
           //    'kodepemda'=>$d->kodepemda_list
           // ];
  
           // $series[3]['data'][]=[
           //    'name'=>$d->name.' ('.$d->kodesubkegiatan.')',
           //    'type'=>'area',
           //    'kode'=>''.$d->kodesubkegiatan,
           //    'y'=>(int)count($pemdakota)??0,
           //    'kodepemda'=>$d->kodepemda_list
           // ];
        }
           
  
  
        
        return ($series);
  
  
       }

       public function getData($tahun,Request $request){
        $table=DB::table('master.dataset')->find(static::$meta['dataset']);
        if(!$table){
           return [];
        }else{
           $table_status='dts_'.$table->id;
        }
     
        if(str_contains($table->table,'[TAHUN]')){
           $table_status.='_'.$tahun;
           $table->table=str_replace('[TAHUN]',$tahun,$table->table);
        }

        $exist_table=DB::table('pg_tables')->where('schemaname','dataset')->where('tablename',$table->table)->first();

   
        if(!$exist_table){
           return [];
           
        }


        $pemdas=FilterCtrl::filter_pemda($request->filter_regional,$request->filter_lokus??false,$request->filter_sortlist,$request->filter_tipe_bantuan,$request->filter_jenis_pemda??3);


        $data=DB::table('dataset.'.$table->table.' as d')
        ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
        ->join('master.master_pemda as mpp','mpp.kodepemda','=',DB::raw("left(mp.kodepemda,3)"))

        ->selectRaw("max(mpp.nama_pemda) as nama_provinsi,d.kodepemda,max(mp.nama_pemda) as nama_pemda,count(distinct(d.kodesubkegiatan::text)) as jumlah_sub_kegiatan,sum(case when d.totalharga is null then 0 else d.totalharga end) as jumlah_pagu")
        ->whereRaw("d.kodepemda in ('".implode("','",$pemdas)."')")

        ->where( [['d.tw','=',4],
        ['d.tahun','=',$tahun]])
        ->groupBy('d.kodepemda');

        if(true){
           $data=$data->join('status_data.'.$table_status.' as std', DB::raw("(std.kodepemda=d.kodepemda and std.tahun=".$tahun." and std.status=2)"),DB::raw("true"));
        }

        $nomen=[];
        foreach(static::$meta['group_nomen'] as $k=>$n){
           $nomen=array_merge($nomen,$n['nomen']);
        }
        $data=$data->whereRaw("d.kodesubkegiatan ilike '".$request->kodesub."'");
       

     

        $length = $request->input('length');
        $kodepemda = $request->input('kodepemda');
        $orderBy = $request->input('column','d.kodepemda'); //Index
        $orderByDir = $request->input('dir', 'asc');
        $searchValue = $request->input('search');
        if($kodepemda){
           $data=$data->whereRaw("d.kodepemda in ('".implode("','",$kodepemda)."')");
        }


        
        $sql= Str::replaceArray('?', $data->getBindings(), $data->toSql());


        $data=DB::table(DB::raw("(".$sql.") as x"));
        
        if($searchValue){
           $where=[];
              $where[]="(x.nama_pemda::text ilike '%".$searchValue."%')"; 
              $data=$data->whereRaw(implode(' or ',$where));
           }

        if($orderBy=='id'){
           $orderBy='x.kodepemda';
        }else{
           $orderBy='x.'.$orderBy;
        }

        if($orderBy){
           
           $data=$data->orderBy($orderBy,$orderByDir);
        }
   

        $data = $data->paginate($length);
        $data=[
              'data'=>$data->items(),
              'links'=>$data->links(),
              'meta'=>$data,
              'payload'=>$request->all()
        ];
        
        return ($data);


    }
   
    
}
