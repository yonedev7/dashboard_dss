<?php

namespace Modules\DashboardDataset\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use Auth;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\FilterCtrl;
use App\Models\User;
use Str;
use JamesDordoy\LaravelVueDatatable\Http\Resources\DataTableCollectionResource;
class DashboardDatasetController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */


    public function partials_data($tahun,$id_partials,Request $request){
        $data=DB::table('analisa.partials_data')->find($id_partials);

        if($data->controller_path){
            return eval('return   (new '.(str_replace('/','\\',$data->controller_path)).'())->index($'.'tahun,true,$'.'data->title,$'.'data->keterangan,$request);');
              
        }

    }

    public function partials($tahun,$id_dataset,$sub,Request $request){
        $dataset=DB::table('master.dataset')->find($id_dataset);
        if($dataset){
            return view('dashboarddataset::partials.'.$id_dataset.'.'.$sub)->with(['dataset'=>$dataset,'req'=>$request,'status'=>2]);
        }
    }
    public function notValidate($tahun,$id,Request $request){
        $pemdas=FilterCtrl::filter_pemda($request->filter_regional,true,$request->filter_sortlist,$request->filter_tipe_bantuan,$request->filter_jenis_pemda??3);

        $table=DB::table('master.dataset')->find($id);
        if(!$table){
            return 0;
        }else{
            $table_status='dts_'.$table->id;
        }
        
        if(str_contains($table->table,'[TAHUN]')){
            $table_status.='_'.$tahun;
            $table->table=str_replace('[TAHUN]',$tahun,$table->table);
        }

        $exist_table=DB::table('pg_tables')->where('schemaname','dataset')->where('tablename',$table->table)->first();
        $data=null;
        if($exist_table){
            $data=DB::table('dataset.'.$table->table.' as d')
            ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
            ->leftJoin('status_data.'.$table_status.' as std', DB::raw("(std.kodepemda=d.kodepemda and std.tahun=".$tahun." and std.status=2)"),DB::raw("true"))
            ->where([
                'd.tahun'=>$tahun,
                'd.tw'=>4,
                'std.id'=>null
            ])
            ->whereIn('d.kodepemda',$pemdas)
            ->selectRaw("count(distinct(d.kodepemda)) as jumlah_pemda,string_agg(distinct(concat(mp.kodepemda::int,' - ',mp.nama_pemda)),'@') as list_pemda")->first();
        
            
        }
        if($data){
            if($data->jumlah_pemda!=0){
            $data->list_pemda=explode('@',$data->list_pemda);

            }else{
            $data->list_pemda=[];

            }
        }

        return $data;

    } 
    public function index($tahun,$id,Request $request)
    {
        if(is_numeric($id)){
            $dataset=DB::table('master.dataset')->where(['id'=>$id,'status'=>true])->first();
            if($dataset){
                $partials=DB::table('analisa.dataset_partials as dp')
                ->join('analisa.partials_data as p','p.id','=','dp.id_partial')
                ->selectRaw("p.*")
                ->where('dp.id_dataset',$dataset->id)
                ->orderBy('dp.id','desc')->get();

                $col=DB::table('master.dataset_data as sd')
                ->join('master.data as d','d.id','=','sd.id_data')
                ->orderby('sd.index','asc')
                ->where('column_dash_pemda',true)
                ->selectRaw('sd.aggregasi,sd.title_aggregate,sd.field_name,d.tipe_nilai,d.name,d.definisi_konsep,d.satuan,sd.group_pemda,sd.column_dash_pemda,sd.prioritas')
                ->where('sd.id_dataset',$id)->get()->toArray();
                $columns=[];
                foreach($col as $k=>$c){
                    $columns[]=[
                        'label'=>$c->title_aggregate??$c->name,
                        'name'=>$c->field_name,
                        // 'data'=>$c->key,
                        // 'columnName'=>$c->key,
                        'meta'=>$c
                    ];
                  
                }

                if(file_exists(__DIR__.'/../../Resources/views/dataset/dts_'.$id.'.blade.php')){
                    return view('dashboarddataset::dataset.dts_'.$id)->with(['dataset'=>$dataset,'req'=>$request,'columns'=>$columns]);
                }else{


                   
                    if($dataset->pusat){
                        return view('dashboarddataset::show_pusat')->with(['dataset'=>$dataset,'req'=>$request,'columns'=>$columns,'partials_data'=>$partials]);

                    }else{
                        return view('dashboarddataset::show')->with(['dataset'=>$dataset,'req'=>$request,'columns'=>$columns,'partials_data'=>$partials]);

                    }
                }
            }else{
                return abort(404);
            }
        }else if(file_exists(__DIR__.'/../../Resources/views/static/'.$id.'.blade.php')){
            
            return view('dashboarddataset::static.'.$id)->with(['req'=>$request]);
        }else{

        }

        return abort(404);
        
        
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('dashboarddataset::create');
    }

    public  function timeSeris($tahun,$id_dataset,$kodepemda){

             $table_data=DB::table('master.dataset_data as sd')
            ->join('master.data as d','d.id','=','sd.id_data')
            ->orderby('sd.index','asc')
            ->where('column_dash_pemda',true)
            ->where('prioritas',true)
            ->where('aggregasi_pusat','!=',null)
            ->selectRaw('sd.aggregasi_pusat,sd.title_aggregate_pusat,sd.aggregasi,sd.title_aggregate,sd.field_name,d.tipe_nilai,d.name,d.definisi_konsep,d.satuan,sd.group_pemda,sd.column_dash_pemda,sd.prioritas')
            ->where('sd.id_dataset',$id_dataset)->get()->toArray();
            $select=[];
            foreach($table_data as $kt=>$el){
                $select[]=$el->aggregasi_pusat.'('.$el->field_name.($el->aggregasi_pusat=='COUNT(DISTINCT'?'::text':'').')'.($el->aggregasi_pusat=='COUNT(DISTINCT'?')':'').' as '.$el->field_name.",'".$el->satuan."' as ".$el->field_name."_satuan";
            }

            $table=DB::table('master.dataset')->find($id_dataset);
            $table_status='dts_'.$table->id;
            $start_tahun=2019;
            $end_tahun=2023;
            if(str_contains($table->table,'[TAHUN]')){
                $sql_union=null;
                foreach([$tahun+4,$tahun+3,$tahun+2,$tahun+1,$tahun,$tahun-1,$tahun-2,$tahun-3,$tahun-4] as $th){
                    $table_status_x=$table_status.'_'.$tahun;
                        $tbth=str_replace('[TAHUN]',$th,$table->table);
                        $exist_table=DB::table('pg_tables')->where('schemaname','dataset')->where('tablename',$tbth)->first();
                        if($exist_table){
                            $x=DB::table('dataset.'.$tbth.' as tb')
                            ->join('status_data.'.$table_status_x.' as std', DB::raw("(std.kodepemda=tb.kodepemda and std.tahun=tb.tahun and std.status=2)"),DB::raw("true"))
                            ->selectRaw("tb.tahun,max(updated_at) as updated_at ".($select?','.implode(',',$select):''))
                            ->where([
                                ['tb.kodepemda','=',DB::raw("'".$kodepemda."'")],
                                ['tb.tw','=',4],
                            ])->where('tb.tahun','=',$tahun)
                            ->groupBy('tb.tahun')
                            ->orderBy('tb.tahun','asc');

                            if($sql_union){
                                $sql_union=$sql_union->union($x);
                            }else{
                                $sql_union=$x;
                            }
                        }

                }

                 $sqlBuilder=DB::table(DB::raw('('.(Str::replaceArray('?', $sql_union->getBindings(), $sql_union->toSql())).') as tbx '))
                    ->selectRaw("tahun,max(updated_at) as updated_at ".($select?','.implode(',',$select):''))
                    ->where('tbx.tahun','>=',$start_tahun)
                    ->where('tbx.tahun','<=',$end_tahun)
                    ->groupBy('tbx.tahun')
                    ->orderBy('tbx.tahun','asc');


            }else{
                $exist_table=DB::table('pg_tables')->where('schemaname','dataset')->where('tablename',$table->table)->first();

                if($exist_table){
                    $sqlBuilder=DB::table('dataset.'.$table->table.' as tb')
                    ->join('status_data.'.$table_status.' as std', DB::raw("(std.kodepemda=tb.kodepemda and std.tahun=tb.tahun and std.status=2)"),DB::raw("true"))
                    ->selectRaw("tb.tahun,max(updated_at) as updated_at ".($select?','.implode(',',$select):''))
                    ->where([
                        ['tb.kodepemda','=',$kodepemda],
                        ['tb.tw','=',4],
                    ])->where('tb.tahun','>=',$start_tahun)
                    ->where('tb.tahun','<=',$end_tahun)
                    ->groupBy('tb.tahun')
                    ->orderBy('tb.tahun','asc');
                }
                
            }


             if(isset($sqlBuilder)){
                $data=$sqlBuilder->get();

                $series=[];
                foreach($data as $k=>$d){
                    $d=(array)$d;
                    foreach($table_data as $kc=>$c){
                        if(!isset($series[$c->field_name])){
                            $series[$c->field_name]=[
                                'name'=>($c->title_aggregate_pusat??$c->name)." (".($c->satuan).')',
                                'data'=>[]
                            ];
                        }

                        $series[$c->field_name]['data'][]=[
                            'name'=>$d['tahun'],
                            'y'=>(float)$d[$c->field_name]
                        ];
                    }

                }

                return array_values($series);
                
             }else{
                 return [];
             }

    }

    public function timeSerisPusat($tahun,$id_dataset,Request $request){
        $pemdas=FilterCtrl::filter_pemda($request->filter_regional,true,$request->filter_sortlist,$request->filter_tipe_bantuan,$request->filter_jenis_pemda??3);

        $table_data=DB::table('master.dataset_data as sd')
       ->join('master.data as d','d.id','=','sd.id_data')
       ->orderby('sd.index','asc')
       ->where('column_dash_pemda',true)
       ->where('prioritas',true)
       ->where('aggregasi_pusat','!=',null)
       ->selectRaw('sd.aggregasi_pusat,sd.title_aggregate_pusat,sd.aggregasi,sd.title_aggregate,sd.field_name,d.tipe_nilai,d.name,d.definisi_konsep,d.satuan,sd.group_pemda,sd.column_dash_pemda,sd.prioritas')
       ->where('sd.id_dataset',$id_dataset)->get()->toArray();
       $select=[];
       foreach($table_data as $kt=>$el){
           $select[]=$el->aggregasi_pusat.'('.$el->field_name.($el->aggregasi_pusat=='COUNT(DISTINCT'?'::text':'').')'.($el->aggregasi_pusat=='COUNT(DISTINCT'?')':'').' as '.$el->field_name.",'".$el->satuan."' as ".$el->field_name."_satuan";
       }

       $table=DB::table('master.dataset')->find($id_dataset);
       $table_status='dts_'.$table->id;
       $start_tahun=$tahun-4;
       $end_tahun=$tahun+4;
       if(str_contains($table->table,'[TAHUN]')){
           $sql_union=null;
           foreach([$tahun+4,$tahun+3,$tahun+2,$tahun+1,$tahun,$tahun-1,$tahun-2,$tahun-3,$tahun-4] as $th){
                $table_status_x=$table_status.'_'.$tahun;
                   $tbth=str_replace('[TAHUN]',$th,$table->table);
                   $exist_table=DB::table('pg_tables')->where('schemaname','dataset')->where('tablename',$tbth)->first();
                   if($exist_table){
                       $x=DB::table('dataset.'.$tbth.' as tb')
                       ->join('status_data.'.$table_status_x.' as std', DB::raw("(std.kodepemda=tb.kodepemda and std.tahun=tb.tahun and std.status=2)"),DB::raw("true"))
                       ->selectRaw("tb.tahun,max(updated_at) as updated_at ".($select?','.implode(',',$select):''))
                       ->where([
                           ['tb.tw','=',4],
                       ])->where('tb.tahun','=',$tahun)
                       ->groupBy('tb.tahun')
                       ->whereRaw(DB::raw('tb.kodepemda in '.("('".implode("','",$pemdas)."')")))
                       ->orderBy('tb.tahun','asc');

                       if($sql_union){
                           $sql_union=$sql_union->union($x);
                       }else{
                           $sql_union=$x;
                       }
                   }

           }

            $sqlBuilder=DB::table(DB::raw('('.(Str::replaceArray('?', $sql_union->getBindings(), $sql_union->toSql())).') as tbx '))
               ->selectRaw("tahun,max(updated_at) as updated_at ".($select?','.implode(',',$select):''))
               ->where('tbx.tahun','>=',$start_tahun)
               ->where('tbx.tahun','<=',$end_tahun)
               ->groupBy('tbx.tahun')
               ->orderBy('tbx.tahun','asc');


       }else{
           $exist_table=DB::table('pg_tables')->where('schemaname','dataset')->where('tablename',$table->table)->first();

           if($exist_table){
               $sqlBuilder=DB::table('dataset.'.$table->table.' as tb')
               ->selectRaw("tb.tahun,max(updated_at) as updated_at ".($select?','.implode(',',$select):''))
               ->join('status_data.'.$table_status.' as std', DB::raw("(std.kodepemda=tb.kodepemda and std.tahun=tb.tahun and std.status=2)"),DB::raw("true"))
               ->where([
                   ['tb.tw','=',4],
               ])->where('tb.tahun','>=',$start_tahun)
               ->where('tb.tahun','<=',$end_tahun)
               ->whereIn('tb.kodepemda',$pemdas)
               ->groupBy('tb.tahun')
               ->orderBy('tb.tahun','asc');
           }
           
       }

        if(isset($sqlBuilder)){
           $data=$sqlBuilder->get();
           $series=[];
           foreach($data as $k=>$d){
               $d=(array)$d;
               foreach($table_data as $kc=>$c){
                   if(!isset($series[$c->field_name])){
                       $series[$c->field_name]=[
                           'name'=>($c->title_aggregate_pusat??$c->name)." (".($c->satuan).")",
                           'data'=>[]
                       ];
                   }

                   $series[$c->field_name]['data'][]=[
                       'name'=>$d['tahun'],
                       'y'=>(float)$d[$c->field_name]
                   ];
               }

           }

           return array_values($series);
           
        }else{
            return [];
        }

}

    public function dataPerPemda($tahun,$id_dataset,$kodepemda,DataTables $dataTables,Request $request){

            
                $table=DB::table('master.dataset')->find($id_dataset);
                if(!$table){
                    return [];
                }else{
                    $table_status='dts_'.$table->id;
                }
                
                if(str_contains($table->table,'[TAHUN]')){
                    $table_status.='_'.$tahun;
                    $table->table=str_replace('[TAHUN]',$tahun,$table->table);
                }

                $exist_table=DB::table('pg_tables')->where('schemaname','dataset')->where('tablename',$table->table)->first();

              
            if($exist_table){
                $data=DB::table('dataset.'.$table->table.' as d')
                ->join('status_data.'.$table_status.' as std', DB::raw("(std.kodepemda=d.kodepemda and std.tahun=d.tahun and std.status=2)"),DB::raw("true"))
                ->where(['d.kodepemda'=>$kodepemda,
                    'd.tahun'=>$tahun,
                    'd.tw'=>4,
                ]);
              
             
                $length = $request->input('length');
                $orderBy = $request->input('column'); //Index
                $orderByDir = $request->input('dir', 'asc');
                $searchValue = $request->input('search');
                $select=[];
                $group=[];
                $table_data=DB::table('master.dataset_data as sd')
                ->join('master.data as d','d.id','=','sd.id_data')
                ->orderby('sd.index','asc')
                ->where('column_dash_pemda',true)
                ->selectRaw('sd.aggregasi,sd.title_aggregate,sd.field_name,d.tipe_nilai,d.name,d.definisi_konsep,d.satuan,sd.group_pemda,sd.column_dash_pemda,sd.prioritas')
                ->where('sd.id_dataset',$id_dataset)->get()->toArray();
                
                foreach($table_data as $f=>$c){
                    if($c->group_pemda){
                        $group[]=$c->field_name;
                    }
                    $select[]=$c->aggregasi.'('.$c->field_name.($c->aggregasi=='COUNT(DISCTINCT'?'::text':'').')'.($c->aggregasi=='COUNT(DISCTINCT'?')':'').' as '.$c->field_name;
                }

                if(count($select)){
                 $data=$data->selectRaw(implode(' , ',$select));

                }else{
                    $data=$data->selectRaw("max(d.id)");
                }

                if(count($group)){
                    $data=$data->groupBy($group);

                }else{
                    $data=$data->groupBy('d.id');
                }

                if($searchValue){
                $where=[];
                    
                foreach($table_data as $f=>$c){
                    $where[]='('.$c->field_name."::text ilike '%".$searchValue."%')"; 
                }

                    
                    $data=$data->whereRaw(implode(' or ',$where));
                }

                if($orderBy=='id'){
                    if(isset($table_data[0])){
                     $orderBy=$table_data[0]->field_name;
                    }else{
                        $orderBy=null;
                    }
                }

                if($orderBy){
                    $data=$data->orderBy($orderBy,$orderByDir);
                }
               
                $data = $data->paginate($length);
                $data=[
                    'data'=>$data->items(),
                    'links'=>$data->links(),
                    'meta'=>$data,
                    'payload'=>$request->all()
                ];
                

                // $query = User::eloquentQuery($orderBy, $orderByDir, $searchValue);
                // $data = $query->paginate($length);
                
                // return new DataTableCollectionResource($data);
                
                return ($data);

            }else{
                return [];
            }




    }

    public function keterisian($tahun,$id_dataset,Request $request){
        $dataset=DB::table('master.dataset')->find($id_dataset);
       
        if($dataset->klasifikasi_2!='[]'){
            $request->filter_tipe_bantuan=json_decode(str_replace('BANTUAN ','',strtoupper($dataset->klasifikasi_2)),true);
        }


        $pemdas=FilterCtrl::filter_pemda($request->filter_regional,true,$request->filter_sortlist,$request->filter_tipe_bantuan,$request->filter_jenis_pemda??3);
       
        $data=DB::table('master.master_pemda as mp')
        ->whereIn('regional_1',['I','II','III','IV'])
        ->selectRaw("(case when length(mp.kodepemda)=3 then 2 else 1 end) as series,mp.regional_1 as reg, left(mp.kodepemda,3)::int as kode_provinsi,  mp.kodepemda::int as id,mp.kodepemda,max(mp.nama_pemda) as nama_pemda")
        ->groupBy('mp.regional_1','mp.kodepemda')
        ->whereIn('mp.kodepemda',$pemdas)
        ->get();

        $reg=[];
        foreach($data as $k=>$d){
            if(!isset($reg[$d->reg])){
                $reg[$d->reg]=[
                    'REGIONAL'=>$d->reg,
                    'pemdas'=>[],
                    'data'=>[
                        'kabkot'=>[],
                        'provinsi'=>[],

                    ]
                ];
            }

            $reg[$d->reg]['pemdas'][]=[
                'name'=>$d->nama_pemda,
                'nama_pemda'=>$d->nama_pemda,
                'kodepemda'=>$d->kodepemda,
                'kode_provinsi'=>$d->kode_provinsi,
                'id'=>$d->id,
            ];

            

        }

        foreach($reg as $k=>$r){
            $table_status='dts_'.$dataset->id;
            $name_table=$dataset->table;
            if(str_contains($dataset->table,'[TAHUN]')){
                 $name_table=str_replace('[TAHUN]',$tahun,$dataset->table);
                $table_status.='_'.$tahun;
            }
            $exist_table=false;
            $exist_table=(int)DB::table('pg_tables')->where('tablename','=',$name_table)->where('schemaname','=','dataset')->count();

            if($exist_table){
                $data=DB::table('dataset.'.$name_table.' as d')
                ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
                ->join('status_data.'.$table_status.' as std', DB::raw("(std.kodepemda=d.kodepemda and std.tahun=".$tahun." and std.status=2)"),DB::raw("true"))
                ->selectRaw("(case when (length(d.kodepemda)=3) then 'provinsi' else 'kabkot' end) as jenis,d.kodepemda::int as id,d.kodepemda ,max(mp.nama_pemda) as name, 1 as clickable")
                ->groupBy('d.kodepemda')
                ->where([
                    'd.tahun'=>$tahun,
                    'd.tw'=>4,
                ])
                ->whereIn('d.kodepemda',array_map(function($el){return $el['kodepemda'];},$r['pemdas']))
                ->get();
                foreach($data as $j=>$d){

                    $reg[$k]['data'][$d->jenis][]=$d;
                }
                

            }
        }


        return array_values($reg);


    }

    public function elementData($tahun,$id_dataset){
        $dataset=DB::table('master.dataset')->find($id_dataset);
        $table_status='dts_'.$dataset->id;
        $name_table=$dataset->table;
        if(str_contains($dataset->table,'[TAHUN]')){
             $name_table=str_replace('[TAHUN]',$tahun,$dataset->table);
            $table_status.='_'.$tahun;
        }
        $exist_table=false;
        $exist_table=(int)DB::table('pg_tables')->where('tablename','=',$name_table)->where('schemaname','=','dataset')->count();

        if($exist_table){
            $elemet=DB::table('master.dataset_data as sd')
            ->join('master.data as d','d.id','=','sd.id_data')
            ->selectRaw('d.*')
            ->where('sd.id_dataset',$id_dataset)
            ->where('sd.group_pemda',1)->orderBy('sd.index','asc')->get();

            return $elemet;
        }
        return [];
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function story($tahun,$id_dataset,Request $request)
    {

        $dataset=DB::table('master.dataset')->find($id_dataset);
        $table_status='dts_'.$dataset->id;
        $name_table=$dataset->table;
        if(str_contains($dataset->table,'[TAHUN]')){
             $name_table=str_replace('[TAHUN]',$tahun,$dataset->table);
            $table_status.='_'.$tahun;
        }
        $exist_table=false;
        $exist_table=(int)DB::table('pg_tables')->where('tablename','=',$name_table)->where('schemaname','=','dataset')->count();

        if($exist_table){
            $elemet=DB::table('story_data.story_kegiatan')
            ->where('id_dataset',$id_dataset)->orderBy('tanggal_pelaksanaan','desc')->paginate(10);

            return [
                'data'=>$elemet->items(),
                'perPage'=>$elemet->perPage(),
                'last_page'=>$elemet->lastPage(),
                'current_page'=>$elemet->currentPage()
            ];
        }
        return [
            'data'=>[],
            'perPage'=>10,
            'last_page'=>0,
            'current_page'=>1
        ];
        
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('dashboarddataset::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('dashboarddataset::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
