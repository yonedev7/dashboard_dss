<?php

namespace Modules\DashboardDataset\Http\Controllers\Partials\KPI1PROPORSI;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use App\Http\Controllers\FilterCtrl;
use Str;
use Rubix\ML\Clusterers\FuzzyCMeans;
use Rubix\ML\Kernels\Distance\Euclidean;
use Rubix\ML\Datasets\Labeled;
use Rubix\ML\Transformers\NumericStringConverter;
use Rubix\ML\Clusterers\Seeders\Random;
use Rubix\ML\Datasets\Generators\HalfMoon;
use Rubix\ML\Clusterers\DBSCAN;
use Rubix\ML\Graph\Trees\BallTree;
use Rubix\ML\Kernels\Distance\Diagonal;

use Rubix\ML\Clusterers\KMeans;
use Rubix\ML\Clusterers\Seeders\PlusPlus;


class KPI1PROPORSIController extends Controller
{
    static $meta=[
        
       ];

    function __construct($title=null,$keterangan=null) {
        static::$meta=require(__DIR__.'/config/meta.php');
        $nomen_list=DB::table('master.master_nomen_90')
        ->where('kode','ilike','1.03.03%')
        ->orderBy('name','asc')
        ->get()->toArray();
        

        $nomen_list=array_map(function($el){
           return [
              "name"=>$el->name.' - '.($el->tipe_pemda==1?'Provinsi':'Kota/Kabupaten'),
              'nomen'=>[$el->kode]
           ];
        },$nomen_list);

        static::$meta['group_nomen']=array_merge(static::$meta['group_nomen'],$nomen_list);
   }

   public static function index($tahun,$header=true,$title=null,$keterangan=null,Request $request){
        return view('partials_KPI1PROPORSI::index')->with([
        'meta'=>[
            'title'=>$title,
            'keterangan'=>$keterangan
        ],
        'no_header'=>!$header,
        'req'=>$request,
        'dataset'=>DB::table('master.dataset')->whereIn('id',static::$meta['dataset'])->orderBy('index','asc')->get(),
        'field_sort'=>DB::table('master.dataset_data as df')
        ->join('master.dataset as dts','dts.id','=','df.id_dataset')
        ->join('master.data as d','d.id','=','df.id_data')
        ->whereIn('df.id_dataset',static::$meta['dataset'])
        ->whereIn('dts.id',static::$meta['dataset'])
        ->where('df.ag_lev_pemda','!=',null)
        ->selectRaw('d.*,dts.table,df.field_name,df.ag_lev_pemda as ag,df.ag_lev_pemda_title as ag_title,df.ag_lev_pemda_satuan as ag_satuan')
        ->orderBy('df.index','asc')->get()->toArray()
        ]);
    }

    public function clustering($tahun,Request $request){
        // $samples = [[1, 1], [8, 7], [1, 2], [7, 8], [2, 1], [8, 9]];
       $maxCluster=0;
        $dataset = Labeled::fromIterator($request->dataset)
        ->apply(new NumericStringConverter());
        $estimator = new KMeans($request->k, $request->bz, $request->ep,$request->mc , $request->w, new Euclidean(), new PlusPlus());
        $estimator->train($dataset);
        $centroid=($estimator->centroids());
        $predictions = $estimator->predict($dataset);
        
        $re=[];
        $labels=$dataset->labels();
        $samples=$dataset->samples();

        foreach($predictions as $k=>$p){
            $def=[
                'label'=>$labels[$k],
                'cluster'=>$p,
                'data'=>[]
            ];
            if($maxCluster<$p){
                $maxCluster=$p;
            }
            foreach($samples[$k] as $sk=>$s ){
                $def['data'][]=[$s];
            }
            $re[]=$def;
        }

        $cluster=[];
        foreach($re as $d){
            if(!isset($cluster['clsuter_'.$d['cluster']])){
                $cluster['clsuter_'.$d['cluster']]=[];
            }
            $cluster['clsuter_'.$d['cluster']][]=$d['label'];
        }
        return [
            'data'=>[
                "cluster_count"=>$maxCluster,
                "cluster"=>$cluster
            ]
        ];


    }
 

   public function load($tahun,Request $request){
    $series=[];
        $pemdas=FilterCtrl::filter_pemda($request->filter_regional,$request->filter_lokus??false,$request->filter_sortlist,$request->filter_tipe_bantuan,$request->filter_jenis_pemda??3);

        $allDataset=DB::table('master.dataset')->whereIn('id',static::$meta['dataset'])->orderby('index','asc')->get();
        $list_series=[];
        $list_table=[];
        foreach($allDataset as $k=>$d){
            $tbstatus='dts_'.$d->id;
            if(str_contains($d->table,'[TAHUN]')){
                $d->table=str_replace('[TAHUN]',$tahun,$d->table);
                $tbstatus.='_'.$tahun;
            }

            $exist=DB::table('information_schema.tables')->where('table_schema','dataset')
            ->where('table_name',$d->table)
            ->first();
            if($exist){
                $sc_tb=[
                    'table'=>$d->table,
                    'status_data'=>$tbstatus,
                    'column'=>[],
                    'tahun'=>$tahun,
                ];
                $column=DB::table('master.dataset_data as df')
                ->join('master.data as d','d.id','=','df.id_data')
                ->where('df.id_dataset',$d->id)
                ->where('df.ag_lev_pemda','!=',null)
                ->selectRaw($d->id.' as id_dts, d.*,df.field_name,df.ag_lev_pemda as ag,df.ag_lev_pemda_title as ag_title,df.ag_lev_pemda_satuan as ag_satuan')
                ->orderBy('index','asc')->get()->toArray();

                $sc_tb['column']=$column;
                $list_series=array_merge($list_series,array_map(function($el) use ($d){
                    return [$d->table,$el->field_name,$el->ag_title??$el->name,$el->ag_satuan??$el->satuan,$d->name,$d->id];
                },$column));

                if(count($sc_tb['column'])){
                    $list_table[]=$sc_tb;
                }

            }
        }

        $query=null;
        foreach($list_table as $l){ 
                $select=['mp.kodepemda , max(mp.nama_pemda) as name'];
                foreach($list_series as $s){
                    $sdef=[];
                    if($s[0]!=$l['table']){
                        $sdef[]='0 as '.$s[1];

                    }else{
                        foreach($l['column'] as $c){
                            if($c->field_name==$s[1]){
                                $sdef[]=str_replace('@?',"d.".$s[1],$c->ag)." as ".$s[1];
                            }
                        }
                    }
                    if(count($sdef)){
                        $sdef[]="'".$s[2]."' as ".$s[1].'_title';
                        $sdef[]="'".$s[3]."' as ".$s[1].'_satuan';
                        $select=array_merge($select,$sdef);
                    }
                }

                if(count($select)>1){


                    $data= DB::table('master.master_pemda as mp')
                    ->join('dataset.'.$l['table'].' as d',[['d.kodepemda','=','mp.kodepemda'],['d.tahun','=',DB::raw($tahun)]])
                    ->join('status_data.'.$l['status_data'].' as st',[['st.kodepemda','=','d.kodepemda'],['st.tahun','=','d.tahun']])
                    ->selectRaw(implode(',',$select))
                    ->groupBy('mp.kodepemda')
                    ->whereIn('mp.kodepemda',$pemdas)
                    ->orderBy('mp.kodepemda','asc');

                    if($query==null){
                        $query=$data;
                    }else{
                        $query=$query->unionAll($data);
                    }
                   
                   

                }
               
                
              
        }

        $pemda_semua=DB::table('master.master_pemda as mp')->whereIn('kodepemda',$pemdas)->orderBy('kodepemda','asc')->get();
        $def_data=[];
        foreach($pemda_semua as $p){
            $def_data[$p->kodepemda]=[
                'name'=>$p->nama_pemda,
                'y'=>0,
                'kodepemda'=>$p->kodepemda

            ];
        }


        if($query!=null){
          
            $data=$query->orderBy($list_series[0][1],'desc')->get();
            
            foreach($data as $d){
                $d=(array)$d;
               foreach($list_series as $s){

                    if(!isset($series[$s[0].$s[1]])){
                        $series[$s[0].$s[1]]=[
                            'name'=>$s[2],
                            'id'=>$s[5].'_'.$s[1],
                            'dataset_name'=>$s[4],
                            'tahun'=>$tahun,
                            'satuan'=>$s[3],
                            'data'=>$def_data,
                            'type'=>'column'
                        ];
                    
                    }

                    if(!isset( $series[$s[0].$s[1]]['data'][$d['kodepemda']])){
                        $series[$s[0].$s[1]]['data'][$d['kodepemda']]=[
                            'name'=>$d['name'],
                            'kodepemda'=>$d['kodepemda'],
                            'y'=>(float)($d[$s[1]]??0)
                        ];
                    }else if($series[$s[0].$s[1]]['data'][$d['kodepemda']]['y']<(float)($d[$s[1]]??0)){
                            $series[$s[0].$s[1]]['data'][$d['kodepemda']]['y']=(float)($d[$s[1]]??0);
                    }
                   


               }
            }
        }


        if($request->sort_by){
            $sortby=$request->sort_by;
            if(str_contains( $sortby,'[TAHUN]')){
                $sortby=str_replace('[TAHUN]',$tahun, $sortby);
            }
            if(isset($series[$sortby])){
                $order=array_values($series[$sortby]['data']);
                usort($order, fn($a, $b) => ($a['y']??0)> ($b['y']??0));
                $order=array_reverse($order);
                $new_series=[];
                foreach($series as $k =>$s){
                    $data_s=$s['data'];
                    $s['data']=[];
                    $new_series[$k]=$s;
                    foreach($order as $ok=>$o){
                        $new_series[$k]['data'][$o['kodepemda']]=$data_s[$o['kodepemda']];
                    }

                }

                $series=$new_series;

            }else{
                dd(array_keys($series), $sortby);
            }
            
        }



        foreach($series as $k=> $s){
            $series[$k]['data']=array_values($s['data']);
        }





     
       return [
        'dataset'=>[$allDataset],
        'data'=>[
            'series'=>array_values($series)
        ]
       ];
   }


}
