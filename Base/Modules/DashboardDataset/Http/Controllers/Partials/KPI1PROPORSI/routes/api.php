<?php

Route::prefix('/mod/{tahun}/dataset-dashboard/partials/proporsi-kpi-1')->name('mod.partial.proporsi-kpi-1')->middleware(['bindTahun'])->group(function(){
    Route::post('/','KPI1PROPORSIController@load')->name('.load');
    Route::post('/cluster','KPI1PROPORSIController@clustering')->name('.cluster');

});