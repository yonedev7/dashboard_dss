@extends('adminlte::page')

@section('content')
<div id="app">
    <div class="parallax pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
        <div class="container">
            <section >
                <h3><b class="text-uppercase">{{$meta['title']}} Tahun {{$StahunRoute}}</b></h3>
                <p><b>{!!$meta['keterangan']!!}</b></p>
            </section>
        </div>
    </div>
    <div class="d-flex mt-1 p-1">
        <div class="card d-flex flex-grow-1 "  v-for="item,i in dataset" >
            <div class="card-body">
                <div class="d-flex mb-2">
                    <span class="badge mr-2 bg-navy">@{{item.klasifikasi_1}}</span>
                    <template v-for="tag in buildTag(item.klasifikasi_2,'bg-primary')">
                        <span :class="'badge mr-2  '+tag.color">@{{tag.name}}</span> 
                    </template>
                    <template v-for="tag in buildTag(item.kategori,'bg-success')">
                        <span :class="'badge mr-2 '+tag.color">@{{tag.name}}</span> 
                    </template>
                </div>
                <h5 class="text-uppercase">@{{item.name}} Tahun {{$StahunRoute}}</h5>
                <p class="" v-html="item.tujuan"></p>
                <a :href="('{{route('mod.dash.dataset.index',['tahun'=>'@?','id'=>'@?'])}}').yoneReplaceParam('@?',[{{$StahunRoute}},item.id])" class="btn btn-primary  btn-sm self-align-center text-white "><i class="fa fa-arrow-right"></i> Detail
                </a> 

            </div>
        </div>
    </div>
    <div class="p-1 bg-white mb-2">
        <div class="bg-white p-1 rounded ">
            <toggle-button v-model="filter.filter_lokus" :width="200" :height="30" :sync="true"
            :labels="{checked: 'NUWSP', unchecked: 'NASIONAL'}" :color="{checked: '#dd1f64', unchecked: '#136013', disabled: '#CCCCCC'}">
            </toggle-button>
            <hr>
            <div class="p-3">
                <div class="row d-flex" >
                    <div class="col-3" v-if="filter.filter_lokus">
                        <div class="">Regional</div>
                        <select name="filter_regional" class="form-control"  v-model="filter.filter_regional" id="">
                            <option value=""></option>
                            <option value="I">I</option>
                            <option value="II">II</option>
                            <option value="III">III</option>
                            <option value="IV">IV</option>
                        </select>
                    </div>
                    <div class="col-3"  v-if="filter.filter_lokus">
                        <div class="">Tahun Bantuan</div>
                        <select name="filter_sortlist" class="form-control"  v-model="filter.filter_sortlist" id="">
                            <option value=""></option>
                            <?php
                            for($i=$StahunRoute;$i>=($StahunRoute-3);$i--){
                                echo '<option value="'.$i.'" >'.$i.'</option>';
                            }
                            ?>
                           
                        </select>
                    </div>
                    <div class="col-3"  v-if="filter.filter_lokus">
                        <div class="">Tipe Bantuan</div>
                        <select name="filter_tipe_bantuan" class="form-control" v-model="filter.filter_tipe_bantuan" id="">
                            <option value=""></option>
                            <option value="STIMULAN">STIMULAN</option>
                            <option value="PENDAMPINGAN">PENDAMPINGAN</option>
                            <option value="BASIS KINERJA">BASIS KINERJA</option>
                        </select>
                    </div>
                    <div class="col-3">
                        <div class="">Jenis Pemda</div>
                        <select name="filter_jenis_pemda" v-model="filter.filter_jenis_pemda" id="" class="form-control"><option value="3">Provinsi &amp; Kab/Kot</option> <option value="2">Kab/Kot</option> <option value="1">Provinsi</option></select>
                    </div>
                </div>
            </div>

        </div>
        <hr>
        <div class="bg-dark p-2 ">
            <div class="d-flex align-items-stretch" >
                <div class="flex-grow-1 d-flex p-1" style="border-right:1px solid #fff;">
                   <div class="form-group col-12" >
                    <label for="">Sort</label>
                    <select name=""  v-model="filter.sort_by" class="form-control" id="" >
                        <option :value="item.table+item.field_name" v-for="item in column_sort">@{{item.ag_title||item.name}}</option>
                    </select>
                   </div>
                </div>
                <div class="col-8 d-flex ml-2 align-self-center" >
                    <h3><b class="text-uppercase">{{$meta['title']}} Tahun {{$StahunRoute}}</b></h3>
                </div>
            </div>
        </div>
        <div class="">
        <charts :options="chart_l1"></charts>  

        </div>
       
    </div>
    <div class="container">
        <div class="form-group">
            <label for="" class="text-center">Cari</label>
            <input type="text" class="form-control"  v-model="cari_l1">
        </div>
    </div>
    <div class="row no-gutters">
        <div class="col-4" v-for="col in lev_1">
            <ul class="list-group">
                <v-dropdown
                    :distance="6" :triggers="['hover']" v-for="item in col"
                    >
                    <!-- This will be the popover reference (for the events and position) -->
                    <li class="list-group-item" >
                        <p> <span v-if="item.name.toLowerCase().includes('provinsi')" class="badge badge-primary">Provinsi</span>
                            <span v-else class="badge badge-success">Kota/Kabupaten</span></p>
                        @{{item.name}}

                        <hr>
                        <p class="text-success m-0">@{{item.data.length}} Data</p>

                        
                    </li>

                    <!-- This will be the content of the popover -->
                    <template #popper>
                        <div class="p-2 bg-success">
                                <small>@{{item.name}}</small>
                                <p class="m-0" v-for="d in item.data">
                                @{{d[0]}} :  @{{d[2]=='-'?'':d[2]}} @{{ nformat(d[1])   }}
                                </p>
                                
        
                        </div>
                        <!-- You can put other components too -->
                    </template>
                </v-dropdown>
               
            </ul>

        </div>
    </div>
  
  
</div>
@stop

@section('js')
<script>
    var app=new Vue({
        el:'#app',
        data:{
            filter:{
                filter_lokus:null,
                filter_regional:null,
                filter_jenis_pemda:3,
                filter_sortlist:null,
                filter_tipe_bantuan:null,
                nama_sub:null,
                sort_by:null,
            },
            filter_nomen:{
                filter:{
                    prov:null,
                    kabkot:null
                }
               
            },
            dataset:<?=json_encode($dataset)?>,
            column_sort:<?=json_encode($field_sort)?>,
            cari_l1:'',
            cluster:{
                k:5,
                bz:128,
                ep:300,
                mc:10,
                w:10,
                index_cls:0,
                data:[],
                provinsi:{
                    chart:{
                        type:'map',
                        map:Highcharts.maps['ind'],

                    },
                    legend:{
                        enabled:false,
                    },
                    title:{
                        text:'Cluster Data Provinsi',
                    },
                    series:[

                    ],
                    plotOptions: {
                        map: {
                            dataLabels:{
                                enabled:true,
                            },
                            joinBy: ['id', 'kodepemda_2'],

                        }
                    }
                },
                kabkot:{
                    legend:{
                        enabled:false,
                    },
                    title:{
                        text:'Cluster Data Kab/Kota',
                    },
                    chart:{
                        type:'map',
                        map:Highcharts.maps['ind_kab'],


                    },
                
                    series:[
                        
                    ],
                    
                    plotOptions: {
                        dataLabels:{
                                enabled:true,
                            },
                        map: {
                            joinBy: ['id', 'kodepemda_2'],
                        }
                    }

                }
            },
            gap_l1:[],
            data_cluster_l1:[],
            colors:["#005a59","#99c8c8","#fb5e00","#e40000","#fcc117","#173f62","#5b8f99","#faab5c","#bf3414","#851826","#4c6a8e","#25e1d4","#ffa742","#57ef7c","#2c51e1","#96ceb4","#ffeead","#ff6f69","#ffcc5c","#88d8b0","#d9534f","#f9f9f9","#5bc0de","#5cb85c","#428bca","ffe135","#318ce7","#0d98ba","#1b4d3e","#cc5500","#5f9ea0","#c95a49","#007aa5"],
            chart_l1:{

                chart:{
                    type:'column',
                    height:500
                },
                title:{
                    text:''
                },
                subtitle:{
                    enabled:false,
                    text:''
                },
                xAxis:{
                    type:'category',
                    min:0,
                    showEmpty:true,
                    max:7,
                    scrollbar: {
                        enabled: true
                    },
                    // tickLength: 1,
                },
                tooltip:{
                    shared: true,
                    split: false,
                    enabled: true
                },
                plotOptions:{
                   
                    series:{
                        dataLabels:{
                            enabled:true
                        }
                    }
                },
                series:[]
            }
        },
        created:function(){
           var self=this;
           if(this.column_sort.length){
                this.filter.sort_by=this.column_sort[0].table+this.column_sort[0].field_name;
           }

           setTimeout((ref) => {
                ref.buildChart();
           }, 500,self);
        },
        watch:{
            "filter.filter_lokus":function(val){
                    if(val==false){
                        this.filter.filter_regional=null;
                        this.filter.filter_sortlist=null;
                        this.filter.filter_tipe_bantuan=null;
                        
                    }
                    this.filter.nama_sub=null;
                    this.buildChart();
                },
                "filter.filter_regional":function(val){
                    
                    this.buildChart();
                    this.filter.nama_sub=null;

                },
                "filter.filter_jenis_pemda":function(val){
                    this.buildChart();
                    this.filter.nama_sub=null;
                    if(val!=2){
                    // this.filter_nomen.filter.kabkot=null;

                    }

                },
                "filter.filter_tipe_bantuan":function(val){
                    
                    this.buildChart();
                    this.filter.nama_sub=null;

                },
                'filter.sort_by':function(val){
                    this.buildChart();
                },
                "filter.filter_sortlist":function(val){
                    
                    this.buildChart();
                    this.filter.nama_sub=null;

                },
               

        },
        methods:{
            showInfo:function(){
                $('#info_cluster').modal();
            },
            buildLegend:function(){
                var cl=[];
                for(var i=0;i<this.cluster.data;i++){
                  
                }
            },
            buildDataCluster:function(series){
                var data={};
                series.forEach(function(el,si){
                    
                    el.data.forEach(function(d){
                        if(data[d.kodepemda]==undefined){
                            data[d.kodepemda]={
                            };
                        }

                      
                        data[d.kodepemda][el.name+el.satuan]=d.y||0;
                        
                        if(si==(series.length-1)){
                            data[d.kodepemda]['label']=d.kodepemda;
                        }
                    });
                    
                });
                

                this.data_cluster_l1=data;
            },
            buildList:function(series){
                var data={};
                series.forEach(function(el){
                    el.data.forEach(d=>{
                        if(d.y>0){
                            if(data[d.kodepemda]==undefined){
                            data[d.kodepemda]={
                                'kodepemda':d.kodepemda,
                                'name':d.name,
                                 data:[]
                            };

                            }

                            data[d.kodepemda]['data'].push([el.name,d.y,el.satuan]);
                        }
                    });
                });
                this.gap_l1=Object.values(data);


            },
            buildChart:function(){
                var self=this;
                this.$isLoading(true);
                self.gap_l1=[];
                self.chart_l1.series=[];
                var k_del=[];
                req_ajax.post('{{route('mod.partial.proporsi-kpi-1.load',['tahun'=>$StahunRoute])}}',this.filter).then(res=>{
                    if(res.status==200){
                        self.chart_l1.series=res.data.data.series;

                        // series_id=res.data.data.series.map(el=>{
                        //   return  el.id;
                        // })

                        // if(self.chart_l1.series.length){
                        //     self.chart_l1.series.forEach((el,k)=>{
                        //         res.data.data.series.forEach(s=>{
                        //             if(s.id==el.id){
                        //                 self.chart_l1.series[k].data=s.data;
                        //             }else if(!series_id.includes(s.id)){
                        //                 k_del.push(k);
                        //             }
                        //         })
                        //     });
                            

                        //     k_del.sort();
                        //     k_del.reverse();
                        //     k_del.forEach(el=>{
                        //         self.chart_l1.series.splice(el);
                        //     })


                        // }else{

                        // }
                        self.buildList(self.chart_l1.series);
                        // self.buildDataCluster(self.chart_l1.series);


                    }
                }).finally(()=>{
                self.$isLoading(false);
                // self.$forceUpdate();
                
                // self.requestCluster();
                });
            },
            buildTag:function(data,color_class){
                data=JSON.parse(data||'[]');
                return data.map(el=>{
                    return {
                        name:el,
                        color:color_class
                    }
                });
            },
            nformat:function(val){
                return NumberFormat(val)
            },
            chuck:function(ar,chunkSize){
                var array_res=ar;
                var chucked=[];
                var arrnum=Math.ceil(array_res.length/chunkSize);
                for (let index = 0; index < chunkSize; index++) {
                    var first=(index*arrnum==0)?0:((index*arrnum)-1);
                    var last=first+(arrnum);
                    if(last>array_res.length){
                        last=array_res.length-1;
                    }
                    chucked.push(array_res.slice(first,last));
                }
               

                return chucked;
            },
            requestCluster:function(){
                var self=this;
                this.$isLoading(true);
                req_ajax.post('{{route('mod.partial.proporsi-kpi-1.cluster',['tahun'=>$StahunRoute])}}',{dataset:Object.values(Object.entries(this.data_cluster_l1).map(el=>{
                    return Object.values(el[1]);
                })),
                k:this.cluster.k,
                bz:this.cluster.bz,
                w:this.cluster.w,
                ep:this.cluster.ep,
                mc:this.cluster.mc,
                }).then(res=>{
                    if(res.status==200){
                        self.cluster.data=Object.values(res.data.data.cluster);
                        var series={
                            'name':'Cluster Pemda',
                            allAreas:false,
                            dataLabels: {
                                enabled: true,
                                format: '{point.name}:<br>{point.value} items'
                            },
                            'data':[]
                        };

                        var series_kab={
                            'name':'Cluster Pemda',
                            allAreas:false,
                            dataLabels: {
                                enabled: true,
                                format: '{point.name}:<br>{point.value} items'
                            },
                            'data':[]
                        };
                         self.cluster.index_cls=0;
                        Object.entries(res.data.data.cluster).forEach((lx,cluster_index)=>{
                            var el=lx[1];
                            el.forEach((d)=>{
                                var def={
                                    name:self.data_cluster_l1[d],
                                    kodepemda_2:parseInt(d),
                                    meta:{},
                                    color:self.colors[cluster_index]||'#000',

                                }

                                if(cluster_index>self.cluster.index_cls){
                                    self.cluster.index_cls=cluster_index;
                                }
                                Object.entries(self.data_cluster_l1[d]).forEach((dm)=>{
                                    if(dm[0]!='label'){
                                        def['meta'][dm[0].toLowerCase().replace(' ','_')]={
                                            name:dm[0],
                                            y:dm[1],
                                        }
                                    }
                                });
                                if(d.length==3){
                                series.data.push(def);

                                }else{
                                series_kab.data.push(def);

                                }
                            });
                        });

                        self.cluster.provinsi.series=[{allAreas:true,name:'Provinsi',data:[]},series];
                        self.cluster.kabkot.series=[{allAreas:true,name:'Kab',data:[]},series_kab];





                    }
                }).finally(()=>{
                     self.$isLoading(false);
                });

            },
            filter_l1:function(){
                var self=this;
                return (this.gap_l1||[]).filter(el=>{
                      
                        return (el.name.toLowerCase()).includes((self.cari_l1||'').toLowerCase());
                });
                   
            }
            
        },
        computed:{
            lev_1:function(){
                return this.chuck(this.filter_l1(),3);
            }
        }
    });
</script>

@stop