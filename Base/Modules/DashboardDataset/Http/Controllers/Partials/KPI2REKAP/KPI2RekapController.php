<?php

namespace Modules\DashboardDataset\Http\Controllers\Partials\RKPDAPBD;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use App\Http\Controllers\FilterCtrl;
use Str;


class RKPDAPBDController extends Controller
{
    static $meta=[
        
       ];

    function __construct($title=null,$keterangan=null) {
        static::$meta=require(__DIR__.'/config/meta.php');
        $nomen_list=DB::table('master.master_nomen_90')
        ->where('kode','ilike','1.03.03%')
        ->orderBy('name','asc')
        ->get()->toArray();
        

        $nomen_list=array_map(function($el){
           return [
              "name"=>$el->name.' - '.($el->tipe_pemda==1?'Provinsi':'Kota/Kabupaten'),
              'nomen'=>[$el->kode]
           ];
        },$nomen_list);

        static::$meta['group_nomen']=array_merge(static::$meta['group_nomen'],$nomen_list);
   }

   public static function index($tahun,$header=true,$title=null,$keterangan=null,Request $request){
        return view('partials_KPI2REKAP::index')->with([
        'meta'=>[
            'title'=>$title,
            'keterangan'=>$keterangan
        ],
        'no_header'=>!$header,
        'req'=>$request,
        'dataset'=>DB::table('master.dataset')
                ->whereIn('id',static::$meta['dataset'])
                ->orderBy('index','asc')
                ->get()
        ]);
    }
 

   public function load($tahun,Request $request){
    $series=[];
        $pemdas=FilterCtrl::filter_pemda($request->filter_regional,$request->filter_lokus??false,$request->filter_sortlist,$request->filter_tipe_bantuan,$request->filter_jenis_pemda??3);

       $data= DB::table('master.master_nomen_90 as nm')
       ->join('dataset.pemda_apbd_'.$tahun.' as d','d.kodesubkegiatan','=','nm.kode')
       ->join('status_data.dts_104_'.$tahun.' as std',[['std.kodepemda','=','d.kodepemda'],['std.tahun','=','d.tahun']])
       ->where('d.tahun','=',$tahun)
       ->selectRaw("count(distinct(d.kodepemda)) as jumlah_pemda, nm.kode,max(nm.name) as name,sum(d.totalharga) as anggaran,max(nm.tipe_pemda) as tipe_pemda")
       ->groupBy('nm.kode')
       ->whereIn('d.kodepemda',$pemdas);

       $datarkpd= DB::table('master.master_nomen_90 as nm')
       ->join('dataset.rkpd_sipd_air_minum as d','d.kodesubkegiatan','=','nm.kode')
       ->join('status_data.dts_101 as std',[['std.kodepemda','=','d.kodepemda'],['std.tahun','=','d.tahun']])
       ->where('d.tahun','=',$tahun)
       ->selectRaw("count(distinct(d.kodepemda)) as jumlah_pemda,nm.kode,max(nm.name) as name,sum(d.pagu) as pagu,max(nm.tipe_pemda) as tipe_pemda")
       ->groupBy('nm.kode')
       ->whereIn('d.kodepemda',$pemdas);


        switch($request->level){
            case 2:
            break;
            case 3:
            break;
            default:
            $data=$data->whereIn('nm.kode',array_map(function($el){ return $el['nomen'][0];},static::$meta['group_nomen']));
            $datarkpd=$datarkpd->whereIn('nm.kode',array_map(function($el){ return $el['nomen'][0];},static::$meta['group_nomen']));

            break;
        }
       
       $data=$data->get();
       $datarkpd=$datarkpd->get();
      
       $n=DB::table('master.master_nomen_90 as nm')->where('kode','ilike','1.03.03%')->get();
       $nomen=[];
       foreach($n as $nm){
        $nomen[$nm->kode]=[
            'name'=>$nm->name.' - '.($nm->tipe_pemda==1?'Provinsi':'Kota/Kabupaten'),
            'y'=>0,
            'kode'=>$nm->kode,
            'color'=>'orange',
            'arah_nilai'=>0,
            'nilai_max'=>0,
            'persentase'=>0,
            'jenis_daerah'=>$nm->tipe_pemda==1?'Provinsi':'Kota/Kabupaten',
            'anggaran'=>0,
            'pagu'=>0,
            'jumlah_pemda_rkpd'=>0,
            'jumlah_pemda_apbd'=>0,
            
        ];
       }

       $series=[
        [
            "name"=>'Pagu RKPD (Rp)',
            "data"=>$nomen,
            'yAxis'=>0,
            'type'=>'column',
            
        ],
        [
        "name"=>'Gap RKPD dan APBD',
        "data"=>$nomen,
        'yAxis'=>0,

        'type'=>'column',
        
        ],
        [
            "name"=>'Anggaran APBD (Rp)',
            "data"=>$nomen,
            'yAxis'=>0,
            'type'=>'column',
            
        ],
        [
            "name"=>'Jumlah Pemda Pada Data RKPD',
            "data"=>$nomen,
            'type'=>'line',
            'yAxis'=>1,
            
        ],
        [
            "name"=>'Jumlah Pemda Pada Data APBD',
            "data"=>$nomen,
            'yAxis'=>1,
            'type'=>'line',
           
        ]


        
    ];
    $series_final=$series;

       foreach($datarkpd as $d){
        $series[3]['data'][$d->kode]['y']=(int)$d->jumlah_pemda;
        if(!isset($series[0])){
            $series[0]=[
                "name"=>'Pagu RKPD (Rp)',
                "data"=>[],
                'type'=>'column',
                "dataSorting"=>[
                    "enabled"=>true,
                    "matchByName"=> true
                ] 

            ];
        }
        $series[0]['data'][$d->kode]=[
            'name'=>$d->name.' - '.($d->tipe_pemda==1?'Provinsi':'Kota/Kabupaten'),
            'y'=>(int)$d->pagu,
            'kode'=>$d->kode,
        ];

        }

        if(!isset($series[1])){
            $series[1]=[
                "name"=>'Gap RKPD dan APBD',
                "data"=>[],
                'type'=>'column',
                "dataSorting"=>[
                    "enabled"=>true,
                    "matchByName"=> true
                ] 

            ];
        }

       foreach($data as $d){
        $series[4]['data'][$d->kode]['y']=(int)$d->jumlah_pemda;

            if(!isset($series[2])){
                $series[2]=[
                    "name"=>'Anggaran APBD (Rp)',
                    "data"=>[],
                    'type'=>'column',
                    "dataSorting"=>[
                        "enabled"=>true,
                        "matchByName"=> true
                    ] 
                ];
            }
            $series[2]['data'][$d->kode]=[
                'name'=>$d->name.' - '.($d->tipe_pemda==1?'Provinsi':'Kota/Kabupaten'),
                'y'=>(int)$d->anggaran,
                'kode'=>$d->kode,
                // 'color'=>'blue'
            ];


       }

       foreach($data as $k=>$s){

            $gap=($series[2]['data'][$s->kode]['y']??0) - ($series[0]['data'][$s->kode]['y']??0);
            $max=max([($series[2]['data'][$s->kode]['y']??0),( $series[0]['data'][$s->kode]['y']??0)]);
            if(isset($series[1]['data'][$s->kode])){
                if($gap==0){
                    $series[1]['data'][$s->kode]=[
                        'name'=>$s->name.' - '.($s->tipe_pemda==1?'Provinsi':'Kota/Kabupaten'),
                        'y'=>$gap,
                        'kode'=>$s->kode,
                        'color'=>'green',
                        'arah_nilai'=>0,
                        'nilai_max'=>$max,
                        'persentase'=>0,
                        'jenis_daerah'=>$s->tipe_pemda==1?'Provinsi':'Kota/Kabupaten',
                        'anggaran'=>($series[2]['data'][$s->kode]['y']??0),
                        'pagu'=>($series[0]['data'][$s->kode]['y']??0),
                        'jumlah_pemda_rkpd'=>($series[3]['data'][$s->kode]['y']??0),
                        'jumlah_pemda_apbd'=>($series[4]['data'][$s->kode]['y']??0),

    
                    ];
                 }else if($gap<0){
                    $gap=$gap * -1;
                    $color="orange";
                    if($gap==$max){
                        $color="red";
                    }else{
                        $color="red";
    
                    }
    
                    $series[1]['data'][$s->kode]=[
                        'name'=>$s->name.' - '.($s->tipe_pemda==1?'Provinsi':'Kota/Kabupaten'),
                        'y'=>$gap,
                        'kode'=>$s->kode,
                        'color'=>$color,
                        'arah_nilai'=>-1,
                        'nilai_max'=>$max,
                        'persentase'=>($gap/$max)*100,
                        'jenis_daerah'=>$s->tipe_pemda==1?'Provinsi':'Kota/Kabupaten',
                        'anggaran'=>($series[2]['data'][$s->kode]['y']??0),
                        'pagu'=>($series[0]['data'][$s->kode]['y']??0),
                        'jumlah_pemda_rkpd'=>($series[3]['data'][$s->kode]['y']??0),
                        'jumlah_pemda_apbd'=>($series[4]['data'][$s->kode]['y']??0),
    
                    ];
    
                 }else{
    
                    $color="orange";
                    if($gap==$max){
                        $color="red";
                    }else{
                        $color="red";
    
                    }
    
                    $series[1]['data'][$s->kode]=[
                        'name'=>$s->name.' - '.($s->tipe_pemda==1?'Provinsi':'Kota/Kabupaten'),
                        'y'=>$gap,
                        'kode'=>$s->kode,
                        'color'=>$color,
                        'arah_nilai'=>1,
                        'nilai_max'=>$max,
                        'persentase'=>($gap/$max)*100,
                        'jenis_daerah'=>$s->tipe_pemda==1?'Provinsi':'Kota/Kabupaten',
                        'anggaran'=>($series[2]['data'][$s->kode]['y']??0),
                        'pagu'=>($series[0]['data'][$s->kode]['y']??0),
                        'jumlah_pemda_rkpd'=>($series[3]['data'][$s->kode]['y']??0),
                        'jumlah_pemda_apbd'=>($series[4]['data'][$s->kode]['y']??0),
    
                    ];
    
                 }
            }
           
       }

       foreach($datarkpd as $k=>$s){

        $gap=($series[2]['data'][$s->kode]['y']??0) - ($series[0]['data'][$s->kode]['y']??0);
        $max=max([($series[2]['data'][$s->kode]['y']??0),( $series[0]['data'][$s->kode]['y']??0)]);
        if( isset($series[1]['data'][$s->kode])){
            if($gap==0){
                $series[1]['data'][$s->kode]=[
                    'name'=>$s->name.' - '.($s->tipe_pemda==1?'Provinsi':'Kota/Kabupaten'),

                    'y'=>$gap,
                    'kode'=>$s->kode,
                    'color'=>'green',
                    'arah_nilai'=>0,
                    'nilai_max'=>$max,
                    'persentase'=>0,
                    'jenis_daerah'=>$s->tipe_pemda==1?'Provinsi':'Kota/Kabupaten',

                    'anggaran'=>($series[2]['data'][$s->kode]['y']??0),
                    'pagu'=>($series[0]['data'][$s->kode]['y']??0),
                    'jumlah_pemda_rkpd'=>($series[3]['data'][$s->kode]['y']??0),
                        'jumlah_pemda_apbd'=>($series[4]['data'][$s->kode]['y']??0),

                ];
             }else if($gap<0){
                $gap=$gap * -1;
                $color="orange";
                if($gap==$max){
                    $color="red";
                }else{
                    $color="red";

                }

                $series[1]['data'][$s->kode]=[
                    'name'=>$s->name.' - '.($s->tipe_pemda==1?'Provinsi':'Kota/Kabupaten'),

                    'y'=>$gap,
                    'kode'=>$s->kode,
                    'color'=>$color,
                    'arah_nilai'=>-1,
                    'nilai_max'=>$max,
                    'persentase'=>($gap/$max)*100,
                    'jenis_daerah'=>$s->tipe_pemda==1?'Provinsi':'Kota/Kabupaten',

                    'anggaran'=>($series[2]['data'][$s->kode]['y']??0),
                    'pagu'=>($series[0]['data'][$s->kode]['y']??0),
                    'jumlah_pemda_rkpd'=>($series[3]['data'][$s->kode]['y']??0),
                        'jumlah_pemda_apbd'=>($series[4]['data'][$s->kode]['y']??0),

                ];

             }else{

                $color="orange";
                if($gap==$max){
                    $color="red";
                }else{
                    $color="red";

                }

                $series[1]['data'][$s->kode]=[
                    'name'=>$s->name.' - '.($s->tipe_pemda==1?'Provinsi':'Kota/Kabupaten'),

                    'y'=>$gap,
                    'kode'=>$s->kode,
                    'color'=>$color,
                    'arah_nilai'=>1,
                    'nilai_max'=>$max,
                    'persentase'=>($gap/$max)*100,
                    'jenis_daerah'=>$s->tipe_pemda==1?'Provinsi':'Kota/Kabupaten',
                    'anggaran'=>($series[2]['data'][$s->kode]['y']??0),
                    'pagu'=>($series[0]['data'][$s->kode]['y']??0),
                    'jumlah_pemda_rkpd'=>($series[3]['data'][$s->kode]['y']??0),
                        'jumlah_pemda_apbd'=>($series[4]['data'][$s->kode]['y']??0),

                ];

             }
        }
       
        }

        $series_final[1]['data']=array_values($series[1]['data']);
        usort($series_final[1]['data'],function($a,$b){
            return ($a['y']< $b['y']?1:-1);
        });


        foreach($series as $k=>$s){
            if($k!=1){
                $series_final[$k]['data']=[];
                foreach($series_final[1]['data'] as $f){
                    $series_final[$k]['data'][]=$series[$k]['data'][$f['kode']];
                }

            }

            $series[$k]['data']=array_values($s['data']);
        }       

       return [
        'dataset'=>[],
        'data'=>[
            'series'=>array_values($series_final)
        ]
       ];
   }


}
