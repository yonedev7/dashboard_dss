<?php

Route::prefix('/mod/{tahun}/dataset-dashboard/partials/RKPD-APBD')->name('mod.partial.rkpd-apbd')->middleware(['bindTahun'])->group(function(){
    Route::post('/','RKPDAPBDController@load')->name('.load');
});