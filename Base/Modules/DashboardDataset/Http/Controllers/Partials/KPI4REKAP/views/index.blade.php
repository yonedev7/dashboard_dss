@extends('adminlte::page')

@section('content')
<div id="app">
    <div class="parallax pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
        <div class="container">
            <section >
                <h3><b class="text-uppercase">{{$meta['title']}} Tahun {{$StahunRoute}}</b></h3>
                <p><b>{!!$meta['keterangan']!!}</b></p>
            </section>
        </div>
    </div>
    <div class="d-flex mt-1 p-1">
        <div class="card d-flex flex-grow-1 "  v-for="item,i in dataset" style="background-image: url({{url('assets/img/bgcard.png')}}); transition: width 2s, height 4s; background-size:100% auto; position: relative;">
            <div class="card-body">
                <div class="d-flex mb-2">
                    <span class="badge mr-2 bg-navy">@{{item.klasifikasi_1}}</span>
                    <template v-for="tag in buildTag(item.klasifikasi_2,'bg-primary')">
                        <span :class="'badge mr-2  '+tag.color">@{{tag.name}}</span> 
                    </template>
                    <template v-for="tag in buildTag(item.kategori,'bg-success')">
                        <span :class="'badge mr-2 '+tag.color">@{{tag.name}}</span> 
                    </template>
                </div>
                <h5 class="text-uppercase">@{{item.name}} Tahun {{$StahunRoute}}</h5>
                <p class="" v-html="item.tujuan"></p>
                <a :href="('{{route('mod.dash.dataset.index',['tahun'=>'@?','id'=>'@?'])}}').yoneReplaceParam('@?',[{{$StahunRoute}},item.id])" class="btn btn-primary  btn-sm self-align-center text-white "><i class="fa fa-arrow-right"></i> Detail
                </a> 

            </div>
        </div>
    </div>
    <div class="p-1 bg-white mb-2">
        <div class="bg-white p-1 rounded ">
            <toggle-button v-model="filter.filter_lokus" :width="200" :height="30" :sync="true"
            :labels="{checked: 'NUWSP', unchecked: 'NASIONAL'}" :color="{checked: '#dd1f64', unchecked: '#136013', disabled: '#CCCCCC'}">
            </toggle-button>
            <div class="p-3">
                <div class="row d-flex" >
                    <div class="col-3" v-if="filter.filter_lokus">
                        <div class="">Regional</div>
                        <select name="filter_regional" class="form-control"  v-model="filter.filter_regional" id="">
                            <option value=""></option>
                            <option value="I">I</option>
                            <option value="II">II</option>
                            <option value="III">III</option>
                            <option value="IV">IV</option>
                        </select>
                    </div>
                    <div class="col-3"  v-if="filter.filter_lokus">
                        <div class="">Tahun Bantuan</div>
                        <select name="filter_sortlist" class="form-control"  v-model="filter.filter_sortlist" id="">
                            <option value=""></option>
                            <?php
                            for($i=$StahunRoute;$i>=($StahunRoute-3);$i--){
                                echo '<option value="'.$i.'" >'.$i.'</option>';
                            }
                            ?>
                           
                        </select>
                    </div>
                    <div class="col-3"  v-if="filter.filter_lokus">
                        <div class="">Tipe Bantuan</div>
                        <select name="filter_tipe_bantuan" class="form-control" v-model="filter.filter_tipe_bantuan" id="">
                            <option value=""></option>
                            <option value="STIMULAN">STIMULAN</option>
                            <option value="PENDAMPINGAN">PENDAMPINGAN</option>
                            <option value="BASIS KINERJA">BASIS KINERJA</option>
                        </select>
                    </div>
                    <div class="col-3">
                        <div class="">Jenis Pemda</div>
                        <select name="filter_jenis_pemda" v-model="filter.filter_jenis_pemda" id="" class="form-control"><option value="3">Provinsi &amp; Kab/Kot</option> <option value="2">Kab/Kot</option> <option value="1">Provinsi</option></select>
                    </div>
                </div>
            </div>

        </div>
        <hr>

        <charts :options="chart_l1"></charts>  
    </div>
    <div class="container">
        <div class="form-group">
            <label for="" class="text-center">Cari</label>
            <input type="text" class="form-control"  v-model="cari_l1">
        </div>
    </div>
    <div class="row no-gutters">
        <div class="col-4" v-for="col in lev_1">
            <ul class="list-group">
                <v-dropdown
                    :distance="6" :triggers="['hover']" v-for="item in col"
                    >
                    <!-- This will be the popover reference (for the events and position) -->
                    <li class="list-group-item" >
                       <p> <span v-if="item.jenis_daerah=='Provinsi'" class="badge badge-primary">Provinsi</span>
                        <span v-else class="badge badge-success">Kota/Kabupaten</span></p>
                        @{{item.jenis_daerah}}
                        @{{item.name}}
                        <hr>
                        <div class="d-flex">
                            <div class="text-orange" v-if="item.arah_nilai==-1">
                                <i class="fa fa-arrow-down" ></i> Menurun @{{item.persentase.toFixed(2)}} %
                            </div>
                            <div class="text-primary" v-else-if="item.arah_nilai==1">
                                <i class="fa fa-arrow-up" ></i> Meningkat @{{item.persentase.toFixed(2)}} %
                            </div>
                            <div class="text-success" v-else-if="item.arah_nilai==0">
                                <i class="fa fa-arrow-up" ></i> Konsisten @{{item.persentase.toFixed(2)}} %
                            </div>
                        </div>
                    </li>

                    <!-- This will be the content of the popover -->
                    <template #popper>
                        <div class="p-2 bg-success">
                                <small>@{{item.name}}</small>
                                <p class="m-0">
                                Pagu RKPD  Rp. @{{ nformat(item.pagu)   }}
                                </p>
                                <p class="m-0">
                                    Jumlah Pemda Dalam Data RKPD  @{{ nformat(item.jumlah_pemda_rkpd)   }} Pemda
                                </p>
                                <p class="m-0">
                                    Gap RKPD APBD @{{item.arah_nilai<0?'-':''}} Rp. @{{ nformat(item.y)   }}
                                    </p>
                                <p class="m-0">
                                    Anggaran APBD Rp. @{{ nformat(item.anggaran)   }}
                                </p>
                                <p class="m-0">
                                    Jumlah Pemda Dalam Data APBD @{{ nformat(item.jumlah_pemda_apbd)   }} Pemda
                                </p>
        
                        </div>
                        <!-- You can put other components too -->
                    </template>
                </v-dropdown>
               
            </ul>

        </div>
    </div>
</div>
@stop

@section('js')
<script>
    var app=new Vue({
        el:'#app',
        data:{
            filter:{
                filter_lokus:null,
                filter_regional:null,
                filter_jenis_pemda:3,
                filter_sortlist:null,
                filter_tipe_bantuan:null,
                nama_sub:null,
            },
            filter_nomen:{
                filter:{
                    prov:null,
                    kabkot:null
                }
               
            },
            dataset:<?=json_encode($dataset)?>,
            cari_l1:'',
            gap_l1:[],
            chart_l1:{
                chart:{
                    type:'column',
                    height:500
                },
                title:{
                    text:''
                },
                subtitle:{
                    text:'Tahun {{$StahunRoute}}'
                },
                xAxis:{
                    type:'category',
                    min:0,
                        max:9,
                        scrollbar: {
                            enabled: true
                        },
                        tickLength: 1,
                },
                yAxis:[
                    {
                        title:{
                            text:'Rp'
                        }
                    },
                    {
                        title:{
                            text:'Pemda'
                        }

                    }
                ],
                tooltip:{
                    shared: true,
                    split: false,
                    enabled: true
                },
                plotOptions:{
                    series:{
                        dataLabels:{
                        enabled:true
                    }
                    }
                },
                series:[]
            }
        },
        created:function(){
            this.buildChart()
        },
        watch:{
            "filter.filter_lokus":function(val){
                    if(val==false){
                        this.filter.filter_regional=null;
                        this.filter.filter_sortlist=null;
                        this.filter.filter_tipe_bantuan=null;
                        
                    }
                    this.filter.nama_sub=null;
                    this.buildChart();
                },
                "filter.filter_regional":function(val){
                    
                    this.buildChart();
                    this.filter.nama_sub=null;

                },
                "filter.filter_jenis_pemda":function(val){
                    this.buildChart();
                    this.filter.nama_sub=null;
                    if(val!=2){
                    // this.filter_nomen.filter.kabkot=null;

                    }

                },
                "filter.filter_tipe_bantuan":function(val){
                    
                    this.buildChart();
                    this.filter.nama_sub=null;

                },
                "filter.filter_sortlist":function(val){
                    
                    this.buildChart();
                    this.filter.nama_sub=null;

                },
               

        },
        methods:{
            buildChart:function(){
                var self=this;
                this.$isLoading(true);
                self.gap_l1=[];
                self.chart_l1.series=[];
                req_ajax.post('{{route('mod.partial.rkpd-apbd.load',['tahun'=>$StahunRoute])}}',this.filter).then(res=>{
                    if(res.status==200){
                        self.chart_l1.series=res.data.data.series;
                        self.gap_l1=res.data.data.series[1].data;
                        console.log(res.data.data.series[1].data);

                    }
                }).finally(()=>{
                this.$isLoading(false);
                self.$forceUpdate();


                });
            },
            buildTag:function(data,color_class){
                data=JSON.parse(data||'[]');
                return data.map(el=>{
                    return {
                        name:el,
                        color:color_class
                    }
                });
            },
            nformat:function(val){
                return NumberFormat(val+'')
            },
            chuck:function(ar,chunkSize){
                var array_res=ar;
                var chucked=[];
                var arrnum=Math.ceil(array_res.length/chunkSize);
                for (let index = 0; index < chunkSize; index++) {
                    var first=(index*arrnum==0)?0:((index*arrnum)-1);
                    var last=first+arrnum;
                    if(last>array_res.length){
                        last=array_res.length-1;
                    }
                    chucked.push(array_res.slice(first,last));
                }
                // while(array_res.length>0){

                //     if(array_res.length>chunkSize){
                //         chucked.push(array_res.slice(0,arrnum-1));
                //         array_res.splice(0,arrnum-1)


                //     }else{
                //         console.log('ar',array_res);
                //          chucked.push(array_res);
                //          console.log(chucked);
                //          array_res.splice(0,array_res.length);


                //     }
                // }


                return chucked;
            },
            filter_l1:function(){
                var self=this;
                return (this.gap_l1||[]).filter(el=>{
                        return ((el.name||'').toLowerCase()).includes((self.cari_l1||'').toLowerCase());
                    });
                   
            }
            
        },
        computed:{
            lev_1:function(){
                return this.chuck(this.filter_l1(),3);
            }
        }
    });
</script>

@stop