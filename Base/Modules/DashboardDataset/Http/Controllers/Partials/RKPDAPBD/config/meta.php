<?php
return [
        'title'=>'',
        'keterangan'=>'',
        'dataset'=>[101,104],
        'table_master_nomen'=>'master.master_nomen_90',
        'filter_master_nomen'=>['level','=',6],
        'column_table'=>[
           [
              'name'=>'nama_pemda',
              'label'=>'Nama Pemda',
              'tipe_nilai'=>'string', 
              'cluster'=>false,
           ],
           
           
        ],
        'group_nomen'=>[
          
           
        ],
        'filter_nomen'=>[
           'jenis_pemda'=>[
              'provinsi'=>['1.03.03.1.01.01','1.03.03.1.01.02','1.03.03.1.01.03','1.03.03.1.01.04','1.03.03.1.01.05','1.03.03.1.01.06','1.03.03.1.01.07','1.03.03.1.01.08','1.03.03.1.01.09','1.03.03.1.01.10','1.03.03.1.01.11','1.03.03.1.01.12'],
              'kabkot'=>['1.03.03.2.01.01','1.03.03.2.01.02','1.03.03.2.01.03','1.03.03.2.01.04','1.03.03.2.01.05','1.03.03.2.01.06','1.03.03.2.01.07','1.03.03.2.01.08','1.03.03.2.01.09','1.03.03.2.01.10','1.03.03.2.01.11','1.03.03.2.01.12','1.03.03.2.01.13','1.03.03.2.01.14','1.03.03.2.01.15','1.03.03.2.01.16','1.03.03.2.01.17','1.03.03.2.01.18','1.03.03.2.01.19','1.03.03.2.01.20','1.03.03.2.01.21'],   
           ],
           'kabkot'=>[
              'desa'=>['1.03.03.2.01.04','1.03.03.2.01.06','1.03.03.2.01.08','1.03.03.2.01.16','1.03.03.2.01.17','1.03.03.2.01.18','1.03.03.2.01.19','1.03.03.2.01.21','1.03.03.2.01.01','1.03.03.2.01.02','1.03.03.2.01.09','1.03.03.2.01.10','1.03.03.2.01.11','1.03.03.2.01.12','1.03.03.2.01.13','1.03.03.2.01.14'],
              'kabkot'=>['1.03.03.2.01.01','1.03.03.2.01.02','1.03.03.2.01.03','1.03.03.2.01.05','1.03.03.2.01.07','1.03.03.2.01.09','1.03.03.2.01.10','1.03.03.2.01.11','1.03.03.2.01.12','1.03.03.2.01.13','1.03.03.2.01.14','1.03.03.2.01.15','1.03.03.2.01.20'],
           ],
           'jenis_sub'=>[
              'pengelolaan'=>['1.03.03.2.01.09','1.03.03.2.01.10','1.03.03.2.01.11','1.03.03.2.01.08','1.03.03.2.01.20','1.03.03.1.01.06','1.03.03.2.01.21','1.03.03.1.01.09','1.03.03.2.01.13','1.03.03.2.01.15','1.03.03.2.01.16','1.03.03.1.01.08'],
              'pengembangan'=>['1.03.03.2.01.17','1.03.03.2.01.18','1.03.03.1.01.10','1.03.03.1.01.11','1.03.03.2.01.19','1.03.03.2.01.03','1.03.03.2.01.04','1.03.03.1.01.03','1.03.03.1.01.04','1.03.03.1.01.05','1.03.03.2.01.05','1.03.03.2.01.06','1.03.03.2.01.07'],
              'pengelolaandanpengembangan'=>['1.03.03.2.01.12','1.03.03.2.01.01','1.03.03.1.01.02','1.03.03.2.01.02','1.03.03.1.01.12','1.03.03.1.01.01','1.03.03.2.01.14','1.03.03.1.01.07'],   
           ],
           'tematik'=>[
              'peningkatan'=>['1.03.03.2.01.20','1.03.03.2.01.15','1.03.03.2.01.14','1.03.03.2.01.07','1.03.03.2.01.05','1.03.03.2.01.03']
           ]
        ]
];