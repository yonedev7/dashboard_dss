<?php

namespace Modules\DashboardDataset\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The module namespace to assume when generating URLs to actions.
     *
     * @var string
     */
    protected $moduleNamespace = 'Modules\DashboardDataset\Http\Controllers';

    /**
     * Called before routes are registered.
     *
     * Register any model bindings or pattern based filters.
     *
     * @return void
     */
    public function boot()
    {
      

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        $path_parrtial=glob(module_path('DashboardDataset','Http/Controllers/Partials/*/routes/web.php'));

        foreach($path_parrtial as $path){
           $arph= explode($this->moduleNamespace,str_replace('/',"\\",$path))[1];
           $new_namespace=($this->moduleNamespace.str_replace('\routes\web.php','',$arph));
            Route::middleware('web')
            ->namespace($new_namespace)
            ->group($path);
    
        }
        Route::middleware('web')
            ->namespace($this->moduleNamespace)
            ->group(module_path('DashboardDataset', '/Routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        $path_parrtial=glob(module_path('DashboardDataset','Http/Controllers/Partials/*/routes/api.php'));

        foreach($path_parrtial as $path){
           $arph= explode($this->moduleNamespace,str_replace('/',"\\",$path))[1];
           $new_namespace=($this->moduleNamespace.str_replace('\routes\api.php','',$arph));
           Route::prefix('api')
            ->middleware('api')
            ->namespace($new_namespace)
            ->group($path);
    
        }

        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->moduleNamespace)
            ->group(module_path('DashboardDataset', '/Routes/api.php'));
    }
}
