@extends('adminlte::page')
@php
    $scope_bantuan=json_decode($dataset->klasifikasi_2??'[]',true);
    if(count($scope_bantuan)){
        $scope_bantuan=array_map(function($el){
            return str_replace('BANTUAN ','',strtoupper($el));
        },$scope_bantuan);
        $lokus=true;
    }else{
        $scope_bantuan=$req->filter_tipe_bantuan;
        $lokus=false;
    }

    $regional=$req->filter_regional;
   

    $filter=['regional'=>$regional,'sortlist'=>$req->filter_sortlist,'lokus'=>$lokus,'tipe_bantuan'=>$scope_bantuan,'tipe_pemda'=>$req->tipe_pemda??3];
    $pemdas=\App\Http\Controllers\FilterCtrl::filter_pemda($filter['regional'],$filter['lokus'],$filter['sortlist'],$filter['tipe_bantuan'],$filter['tipe_pemda']);
    $data=[];
    if(strpos($dataset->table,'[TAHUN]')){
        $dataset->table=str_replace('[TAHUN]',$StahunRoute,$dataset->table);
    }
    $exist=DB::table('information_schema.tables')->where('table_schema','dataset')
            ->where('table_name',$dataset->table)
            ->first();

    if($exist){

        $data= DB::table('dataset.'.$dataset->table.' as d')
        ->join('master.master_pemda as mp','mp.kodepemda','=','mp.kodepemda')
        ->selectRaw("(case when (length(max(d.kodepemda))=3) then 'PROVINSI' else 'KAB/KOTA' end) as scope ,max(d.kodebidang) as kodebidang,max(d.namabidang) as namabidang,max(d.kodeprogram) as kodeprogram,max(d.namaprogram) as namaprogram,d.kodekegiatan,max(d.namakegiatan) as namakegiatan,sum(d.totalharga) as anggaran")
        ->groupBy('d.kodekegiatan')
        ->whereIn('d.kodepemda',$pemdas)
        ->where('d.status',1)
        ->where('d.tw',4)
        ->get();

    }




@endphp

@section('content')
<div id="app">
    <div class="parallax pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
        <div class="container">
            <section >
                <h3><b>{{$dataset->name}}</b></h3>
                <p><b>{!!$dataset->tujuan!!}</b></p>
            </section>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <charts :options="com_chart_program"></charts>
        </div>
    </div>
</div>

@stop


@section('js')
<script>
    var app=new Vue({
        el:'#app',
        data:{
            data_1:<?=json_encode($data)?>,
            chart_urusan:{
                chart:{
                    type:'pie',
                },
                series:[
                    {
                        name:'PROVINSI',
                        data:[]
                    },
                    {
                        name:'KAB/KOTA',
                        data:[]
                    }
                ]
            },
            chart_program:{
                chart: {
        type: 'column',
        inverted: true,
        polar: true
    },
    title: {
        text: 'Winter Olympic medals per existing country (TOP 5)'
    },
    tooltip: {
        outside: true
    },
    pane: {
        size: '85%',
        innerSize: '20%',
        endAngle: 270
    },
    xAxis: {
        tickInterval: 1,
        labels: {
            align: 'right',
            useHTML: true,
            allowOverlap: true,
            step: 1,
            y: 3,
            style: {
                fontSize: '13px'
            }
        },
        lineWidth: 0,
        categories: [
            'Norway <span class="f16"><span id="flag" class="flag no">' +
            '</span></span>',
            'United States <span class="f16"><span id="flag" class="flag us">' +
            '</span></span>',
            'Germany <span class="f16"><span id="flag" class="flag de">' +
            '</span></span>',
            'Canada <span class="f16"><span id="flag" class="flag ca">' +
            '</span></span>',
            'Austria <span class="f16"><span id="flag" class="flag at">' +
            '</span></span>'
        ]
    },
    yAxis: {
        crosshair: {
            enabled: true,
            color: '#333'
        },
        lineWidth: 0,
        tickInterval: 25,
        reversedStacks: false,
        endOnTick: true,
        showLastLabel: true
    },
    plotOptions: {
        column: {
            stacking: 'normal',
            borderWidth: 0,
            pointPadding: 0,
            groupPadding: 0.15
        }
    },
    series: [{
        name: 'Gold medals',
        data: [132, 105, 92, 73, 64]
    }, {
        name: 'Silver medals',
        data: [125, 110, 86, 64, 81]
    }, {
        name: 'Bronze medals',
        data: [111, 90, 60, 62, 87]
    }]
                
            },
            chart_kegiatan:{
                chart:{
                    type:'pie',
                },
                series:[
                    {
                        name:'PROVINSI',
                        data:[]
                    },
                    {
                        name:'KAB/KOTA',
                        data:[]
                    }
                ]
            },
            chart_subkegiatan:{
                chart:{
                    type:'pie',
                },
                series:[
                    {
                        name:'PROVINSI',
                        data:[]
                    },
                    {
                        name:'KAB/KOTA',
                        data:[]
                    }
                ]
            }
        },
        created:function(){
            var data_program={
                'PROVINSI':{},
                'KAB/KOTA':{}
            };
            var data_kegiatan={
                'PROVINSI':{},
                'KAB/KOTA':{}
            };

            

            
        },
        computed:{
            com_chart_program:function(){
                var chart=this.chart_program;
                // var series={
                //     'PROVINSI':{},
                //     'KAB/KOTA':{}

                // };
                // this.data_1.forEach(el => {
                //     console.log(el.kodeprogram.replace(/\./g,'_'));
                //     if(series[el.scope][el.kodeprogram.replace(/\./g,'_')]==undefined){
                //         series[el.scope][el.kodeprogram.replace(/\./g,'_')]={
                //             name:el.namaprogram,
                //             y:0
                //         };
                //     }
                //     series[el.scope][el.kodeprogram.replace(/\./g,'_')].y+=parseInt(el.anggaran);
                // });

                // if(series['PROVINSI']!=undefined){
                //     chart.series[0].data=Object.values(series['PROVINSI']);
                // }

                // if(series['KAB/KOTA']!=undefined){
                //     chart.series[1].data=Object.values(series['KAB/KOTA']);
                // }

                return chart;
            }
        }
    });
</script>

@stop