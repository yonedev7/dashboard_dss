@extends('adminlte::page')

@section('content')
@php
    $req->filter_lokus=true;
    $req['filter_lokus']=true;

@endphp
<div id="app_info">
    <div class="card bg-warning m-0" v-if="jumlah_pemda">
        <div class="card-body">
          <p class="m-0 p-0"><b><span class="badge badge-primary"><i class="fa fa-info"></i></span> @{{jumlah_pemda}} Pemda Belum Tervalidasi</b></p>
        </div>
    </div>
    <div class="parallax pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
        <div class="container">
            <section >
                <h3><b>@{{page.name}} {{$StahunRoute}}</b></h3>
                <p><b v-html="page.keterangan"></b></p>
            </section>
        </div>
    </div>
    <div class="card">
    
        <div class="card-body">
             
            <toggle-button v-model="filter.filter_lokus" :width="200" :height="30" :sync="true"
            :labels="{checked: 'NUWSP', unchecked: 'NASIONAL'}" :color="{checked: '#dd1f64', unchecked: '#136013', disabled: '#CCCCCC'}">
            </toggle-button>
    
            <div class="row d-flex" >
                <div class="col-3" v-if="filter.filter_lokus">
                    <div class="">Regional</div>
                    <select name="filter_regional" class="form-control"  v-model="filter.filter_regional" id="">
                        <option value=""></option>
                        <option value="I">I</option>
                        <option value="II">II</option>
                        <option value="III">III</option>
                        <option value="IV">IV</option>
                    </select>
                </div>
                <div class="col-3"  v-if="filter.filter_lokus">
                    <div class="">Tahun Bantuan</div>
                    <select name="filter_sortlist" class="form-control"  v-model="filter.filter_sortlist" id="">
                        <option value=""></option>
                        <?php
                        for($i=$StahunRoute;$i>=($StahunRoute-3);$i--){
                            echo '<option value="'.$i.'" >'.$i.'</option>';
                        }
                        ?>
                       
                    </select>
                </div>
                <div class="col-3"  v-if="filter.filter_lokus">
                    <div class="">Tipe Bantuan</div>
                    <select name="filter_tipe_bantuan" class="form-control" v-model="filter.filter_tipe_bantuan" id="">
                        <option value=""></option>
                        <option value="STIMULAN">STIMULAN</option>
                        <option value="PENDAMPINGAN">PENDAMPINGAN</option>
                        <option value="BASIS KINERJA">BASIS KINERJA</option>
                    </select>
                </div>
                <div class="col-3">
                    <div class="">Jenis Pemda</div>
                    <select name="filter_jenis_pemda" v-model="filter.filter_jenis_pemda" id="" class="form-control"><option value="3">Provinsi &amp; Kab/Kot</option> <option value="2">Kab/Kot</option> <option value="1">Provinsi</option></select>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="px-3 bg-white pt-0" id="app">
    <div class="row bg-secondary">
        <p class="m-0 p-3 col-12 text-uppercase text-center"><b>@{{filter.filter_lokus?"DATA NUWSP -":'NASIOANAL -'}} @{{filter.filter_regional?'Regional '+filter.filter_regional+',':''}} @{{filter.filter_jenis_pemda==3?'Pemda Provinsi & kabupaten Kota':(filter.filter_jenis_pemda==2?'Pemda Kota/Kabupaten':(filter.filter_jenis_pemda==1?'Pemda Provinsi':''))}}  @{{filter.filter_tipe_bantuan?'Tipe Bantuan '+filter.filter_tipe_bantuan+',':''}} @{{filter.filter_sortlist?'Tahun Bantuan '+filter.filter_sortlist:''}}</b></p>
    </div>
   <div class="row bg-dark pt-3">
    <div class="col-3">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1">Tipe Chart</span>
            </div>
            <select name="" class="form-control" v-model="chart_data.chart.type" id="" aria-describedby="basic-addon1">
                <option value="column">Column</option>
                <option value="bar">Bar</option>
            </select>
          </div>
        
       </div>
       <div class="col-3">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1">Frame Limit</span>
            </div>
            <div class="btn-group">
                <button :class="'btn btn-sm '+(chart_data.xAxis.max+1==5?'btn-info':'btn-primary')" @click="frame_limit=5">5</button>
                <button :class="'btn btn-sm '+(chart_data.xAxis.max+1==7?'btn-info':'btn-primary')" @click="frame_limit=7">7</button>
                <button :class="'btn btn-sm '+(chart_data.xAxis.max+1==10?'btn-info':'btn-primary')" @click="frame_limit=10">10</button>
                <button :class="'btn btn-sm '+(chart_data.xAxis.max+1==14?'btn-info':'btn-primary')"@click="frame_limit=14">14</button>
                <button :class="'btn btn-sm '+(chart_data.xAxis.max+1==21?'btn-info':'btn-primary')" @click="frame_limit=21">21</button>
                <button :class="'btn btn-sm '+(chart_data.xAxis.max+1==33?'btn-info':'btn-primary')" @click="frame_limit=33">33</button>
                <button class="btn btn-sm btn-warning" v-if="frame_limit_timeout!=null">
                    <div class="spinner-border" style="width:12px; height:12px;" role="status">
                        <span class="sr-only">Loading...</span>
                      </div>
                </button>

            </div>
          </div>
      
       </div>
       <div class="col-3">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1">Sort</span>
            </div>
            <select name="" class="form-control" v-model="filter.sorted_by" id="" aria-describedby="basic-addon1">
                <option value="kodesubkegiatan">Nomenklatur</option>
                <option value="jumlah_pagu">Nilai Pagu</option>
                <option value="jumlah_pemda">Jumlah Pemda</option>
            </select>
          </div>
        
       </div>
       <div class="col-3 bg-dark">
           <p class="pt-2 mb-0 pb-0"><b>@{{chart_data.series[0]!=undefined?chart_data.series[0].data.length:0}} / @{{filter.nomen.length}} Nomenklatur </b></p>
       </div>
   </div>
    <div class="d-flex" >
        <div class="col-3" v-if="chart_data.chart.type=='column'">
            <charts  :options="chart_prop" ></charts>
        </div>
        <div class="flex-grow-1">
            <charts ref="ref_chart_data" :callback="callback_chart" :deep-copy-on-update="chart_update" :options="chart_data" ></charts>
        </div>
    </div>
    <div class="row">
        <div class="col-12 d-flex">
            <div class="form-group mr-2" style="max-width:30%" v-if="parseInt(filter.filter_jenis_pemda)==2">
                <label for="">Klaster Nomenkaltur Kota/Kab</label>
                <select name="" class="form-control" v-model="filter_nomen.filter.kabkot" id="">
                    <option value=""></option>
                    <option value="kabkot">SPAM Perkotaan</option>
                    <option value="desa">SPAM Pedesaan</option>


                </select>
            </div>

            <div class="form-group mr-2" style="max-width:30%" >
                <label for="">Jenis Sub Kegiatan</label>
                <select name="" class="form-control" v-model="filter_nomen.filter.jenis_sub" id="">
                    <option value=""></option>
                    <option value="pengembangan">Pengembangan</option>
                    <option value="pengelolaan">Pengelolaan</option>
                    <option value="pengelolaandanpengembangan">Pengelolaan & Pengembangan</option>
                </select>
            </div>

            <div class="form-group mr-2" style="max-width:30%" >
                <label for="">Jenis Tematik</label>
                <select name="" class="form-control" v-model="filter_nomen.filter.tematik" id="">
                    <option value=""></option>
                    <option value="peningkatan">Peningkatan Qualitas SPAM Kab/Kota</option>
                </select>
            </div>
        </div>
        <div class="col-12">
            <div class="form-group">
                <label for="">Pilih Nomenklatur Sub Kegiatan</label>
                <select name="" class="form-control" multiple id="" v-model="filter.nomen">
                    <option :value="item.nomen[0]" v-for="item in filter_nomen.list.def_list">@{{item.name}}</option>
                </select>
            </div>
        </div>
         
        {{-- <div class="col-2">
            <div class="form-group">
                <label for="">Operator</label>
                <select name="" class="form-control" id="" v-model="filter.method_filter_nomen">
                    <option value="AND">AND</option>
                    <option value="OR">OR</option>

                </select>
            </div>
        </div> --}}
    </div>
    <hr id="table-data" >
    <div class="" v-if="filter.nama_sub" >
        <div class="text-center">
            <span class="mb-1 p-2 rounded" v-bind:style="'border-width:2px; border-style:groove; border-color:'+table_color" ><b>@{{filter.nama_sub}} </b></span>
        </div>
        <hr>
        <div class="table-responsive">
            <data-table  ref="ref_tabledata" :filters="filter" :data="data" class="table-bordered table" :columns="com_columns" :url="com_url_data">
                <template slot="body" slot-scope="{ data }">
                    <tbody v-if="tbdata=data">
                        <tr
                            :key="datakey"
                           
                            v-for="(item,datakey) in data">
                            <td 
                                :key="column.name"
                                v-for="column,key in com_columns"  >
                                <span v-if="column.tipe_nilai=='numeric' && column.cluster==false">@{{NumberFormat(item[column.name])}}</span>
                                <span v-else>@{{item[column.name]}}</span>
    
                                
                            </td>
                        </tr>
                    </tbody>
                </template>
            </data-table>
        </div>
    </div>
   
</div>


@stop


@section('js')
<script>
    var app_info=new Vue({
        el:"#app_info",
        data:{
            page:{
                name:'{{isset($meta)?$meta['title']:''}}',
                keterangan:'{!!isset($meta)?$meta['keterangan']:''!!}'
            },
            filter:{
                filter_lokus:{{$req['filter_lokus']?'true':'false'}},
                filter_regional:'{{$req['filter_regional']}}',
                filter_sortlist:'{{$req['filter_sortlist']}}',
                filter_tipe_bantuan:'{{$req['filter_tipe_bantuan']}}',
                filter_jenis_pemda:{{$req['filter_jenis_pemda']??3}},
            },
            jumlah_pemda:0,
        },
        watch:{
            filter:{
                deep:true,
                handler:function(){
                    window.app.filter.filter_lokus=this.filter.filter_lokus;
                    window.app.filter.filter_regional=this.filter.filter_regional;
                    window.app.filter.filter_sortlist=this.filter.filter_sortlist;
                    window.app.filter.filter_tipe_bantuan=this.filter.filter_tipe_bantuan;
                    window.app.filter.filter_jenis_pemda=this.filter.filter_jenis_pemda;



                }
            }
        },
        methods:{
            change_meta:function(data){
                if(!this.page.name){
                    this.page.name=data.title;
                    this.page.keterangan=data.keterangan;

                }
            }
        },
        created:function(){
            var self=this;
            req_ajax.post('{{route('api-web.mod-dataset-dash.count-not-validate',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id])}}',this.filter).then(res=>{
                if(res.status==200){
                    self.jumlah_pemda=parseInt(res.data);
                }
            });
        }
    })
    </script>

    <script>
        var app =new Vue({
            el:'#app',
            created:function(){
                this.init();
            },
            computed:{
              
                lingkup_data:function(){
                    if(this.filter.filter_regional || this.filter.filter_sortlist || this.filter.filter_tipe_bantuan){
                        return 'Lokus NUWSP';
                    }else{
                        return 'Data Nasional'
                    }
                },
                com_columns:function(){
                    var c=[{
                        name:'nama_provinsi',
                        label:'Nama Provinsi',
                        satuan:'-',
                        claster:false
                    }];
                    if(this.filter.filter_jenis_pemda!=1){
                        this.columns.forEach(el=>{
                            c.push(el);
                        });

                        return c;
                    }else{
                        return this.columns;
                    }
                },
                
                com_url_data:function(){
                    var url='{{route('api-web.mod-dataset-dash.par.rkpdpen.data',['tahun'=>$StahunRoute])}}?';
                        url+=(new URLSearchParams(this.filter));

                        return url;


                },
                com_select_nomen:function(){
                    return this.chunk(this.filter_nomen.list,4);
                },
                
            },
            data:{
                tbdata:[],
                chart_update:true,
                chart_prop:{
                    chart:{
                        type:'pie',
                        style:{
                            backgoundColor:"#ddd"
                        }
                       
                    },
                    title:{
                        text:'Proporsi Pagu'
                    },
                    subtitle:{
                        text:''
                    },
                    xAxis:{
                        type:'category'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}% / Rp. {point.y:,0f}</b>'
                    },
                    plotOptions:{
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '{point.percentage:.1f} %',
                                distance: -50,
                                filter: {
                                    property: 'percentage',
                                    operator: '>',
                                    value: 4
                                }
                            }
                        },
                        series:{
                          
                            point:{
                                events:{
                                    click:function(){
                                        window.app.filter.nama_sub=this.name;
                                        window.app.filter.kodesub=this.kode;
                                        window.app.filter.kodepemda=this.kodepemda.split(',');
                                        window.app.table_color=this.color;

                                        ScrollTo('#table-data',100,100);
                                    }
                                }
                            }
                            
                        },
                        
                    },
                    series:[]
                },
                chart_data:{
                    chart:{
                        type:'column',
                        height:500
                    },
                    title:{
                        text:''
                    },
                    subtitle:{
                        text:''
                    },
                    xAxis:{
                        type:'category',
                        min:0,
                        max:9,
                        scrollbar: {
                            enabled: true
                        },
                        tickLength: 1,
                        labels: {
                            rotation: -30
                        }
                    },
                    plotOptions:{
                        series:{
                            dataLabels:{
                                enabled:true,

                            },
                            lineWidth: 1,
                            point:{
                                events:{
                                    click:function(){
                                        window.app.filter.nama_sub=this.name;
                                        window.app.filter.kodesub=this.kode;
                                        window.app.filter.kodepemda=this.kodepemda.split(',');
                                        window.app.table_color=this.color;
                                        ScrollTo('#table-data',100,100);
                                    }
                                }
                            }
                            
                        },
                        
                    },
                    yAxis:[
                        {
                            title:{
                                text:'Jumlah Pemda'
                            }
                        },
                        {
                            opposite:true,
                            title:{
                                text:'Pagu (Rp.)'
                            }
                        }
                    ],
                    series:[]
                },
                rekap:{
                    jumlah_pemda:0,
                    jumlah_pagu:0
                },
                table_color:'#fff',
                frame_limit:10,
                frame_limit_timeout:null,
                filter:{
                    nomen:[],
                    method_filter_nomen:'OR',
                    filter_lokus:{{$req['filter_lokus']?'true':'false'}},
                    filter_regional:'{{$req['filter_regional']}}',
                    filter_sortlist:'{{$req['filter_sortlist']}}',
                    filter_tipe_bantuan:'{{$req['filter_tipe_bantuan']}}',
                    filter_jenis_pemda:{{$req['filter_jenis_pemda']??3}},
                    sorted_by:'kodesubkegiatan',
                    kodepemda:[],
                    kodesub:'',
                    nama_sub:''
                },
                filter_nomen:{

                    list:{
                        def_list:[],
                        def:[],
                        semua:[],
                        jenis_pemda:{
                            provinsi:[],
                            kabkot:[]
                        },
                        kabkot:{
                            kabkot:[],
                            desa:[]
                        },
                        jenis_sub:{
                            pengelolaan:[],
                            pengembangan:[],
                            pengelolaandanpengembangan:[]
                        },
                        tematik:{
                            peningkatan:[]
                        }
                    },
                    filter:{
                        kabkot:'',
                        jenis_sub:'',
                        tematik:''
                    }
                },
                columns:[
                   
                ]

            },
            watch:{
                "filter.filter_lokus":function(val){
                    if(val==false){
                        this.filter.filter_regional=null;
                        this.filter.filter_sortlist=null;
                        this.filter.filter_tipe_bantuan=null;
                        
                    }
                    this.filter.nama_sub=null;
                    this.getDataSeries();
                },
                "filter.filter_regional":function(val){
                    
                    this.getDataSeries();
                    this.filter.nama_sub=null;

                },
                "filter.filter_jenis_pemda":function(val){
                    this.init();
                    this.getDataSeries();
                    this.filter.nama_sub=null;
                    if(val!=2){
                    this.filter_nomen.filter.kabkot=null;

                    }

                },
                "filter.filter_tipe_bantuan":function(val){
                    
                    this.getDataSeries();
                    this.filter.nama_sub=null;

                },
                "filter.filter_sortlist":function(val){
                    
                    this.getDataSeries();
                    this.filter.nama_sub=null;

                },
               "filter_nomen.filter":{
                    deep:true,
                    handler:function(){
                        this.selectNomen();
                    }
               },
                "chart_data.chart.type":function(val){
                    if(val=='bar'){
                        this.chart_data.chart.height=700;
                    }else{
                        this.chart_data.chart.height=500;
                    }
                },
                "frame_limit":function(val){
                    val=parseInt(val);
                    if(isNaN(val)){
                        val=1;
                    }
                    if(this.frame_limit_timeout){
                        // clearTimeout(this.frame_limit_timeout);
                        // this.frame_limit_timeout=null;
                    }else{
                        var self=this;
                        this.frame_limit_timeout=setTimeout(function(ref){
                            ref.chart_data.xAxis.max=(val<=2?1:val)-1;
                            ref.frame_limit_timeout=null;
                        },1000,self);
                    }
                  
                  
                },
                "filter.nomen":function(val){
                    var self=this;
                    this.getDataSeries();
                   
                    
                },
                'filter.sorted_by':function(){
                    this.getDataSeries();
                },
                "filter_nomen.select":function(val){
                   var f=[];
                    val.map(el=>{
                        f=f.concat(el);
                    });

                    this.filter.nomen=f;
                },
                "filter.nama_sub":function(val){
                    if(val){
                        setTimeout(() => {
                            $('thead.laravel-vue-datatable-thead').addClass('thead-dark');
                        }, 400);
                    }
                }
            },
            methods:{
                callback_chart:function(a,b){
                    console.log(a,b);
                },
                init:function(){
                    var self=this;
                req_ajax.get('{{route('api-web.mod-dataset-dash.par.rkpdpen.meta',['tahun'=>$StahunRoute])}}').then(function(res){
                        if(res.status==200){
                            
                            self.columns=res.data.column_table;
                            app_info.change_meta({
                                title:res.data.title,
                                keterangan:res.data.keterangan
                            });

                            self.filter_nomen.list.def=res.data.group_nomen;
                            
                            var nn=[];
                            res.data.group_nomen.forEach(el=>{
                                if(el.nomen[0]==undefined){
                                    console.log(el);
                                }else{
                                    nn.push(el.nomen[0]);

                                }
                            });

                            self.filter_nomen.list.semua=nn;
                            
                            self.filter_nomen.list.jenis_pemda.provinsi=res.data.filter_nomen.jenis_pemda.provinsi;
                            self.filter_nomen.list.jenis_pemda.kabkot=res.data.filter_nomen.jenis_pemda.kabkot;
                            self.filter_nomen.list.kabkot.kabkot=res.data.filter_nomen.kabkot.kabkot;
                            self.filter_nomen.list.kabkot.desa=res.data.filter_nomen.kabkot.desa;
                            self.filter_nomen.list.jenis_sub.pengelolaan=res.data.filter_nomen.jenis_sub.pengelolaan;
                            self.filter_nomen.list.jenis_sub.pengembangan=res.data.filter_nomen.jenis_sub.pengembangan;
                            self.filter_nomen.list.jenis_sub.pengelolaandanpengembangan=res.data.filter_nomen.jenis_sub.pengelolaandanpengembangan;
                            self.filter_nomen.list.tematik.peningkatan=res.data.filter_nomen.tematik.peningkatan;
                            self.filter_nomen.list.def=self.filter_nomen.list.def.filter(el=>{
                               var  jj=parseInt(self.filter.filter_jenis_pemda||3);
                                if(jj!=3){
                                    if(jj==2){
                                        return self.filter_nomen.list.jenis_pemda.kabkot.includes(el.nomen[0]);
                                    }else if(jj==1){
                                        return self.filter_nomen.list.jenis_pemda.provinsi.includes(el.nomen[0]);
                                    }
                                }else{
                                    return true;
                                }
                            });
                            self.selectNomen();
                        }   
                    });
            
                },
                selectNomen:function(){
                    var nomen=this.filter_nomen.list.semua;

                    if(this.filter.filter_jenis_pemda!=3){
                        if(this.filter.filter_jenis_pemda==2){
                            nomen=this.filter_nomen.list.jenis_pemda.kabkot;
                        }else if(this.filter.filter_jenis_pemda==1){
                             nomen=this.filter_nomen.list.jenis_pemda.provinsi; 
                        }

                    }
                
                    
                    
                    if(this.filter_nomen.filter.kabkot=='kabkot'){
                        nomen=nomen.filter(el=>{
                            return this.filter_nomen.list.kabkot.kabkot.includes(el);
                        });
                    }else if(this.filter_nomen.filter.kabkot=='desa'){
                        nomen=nomen.filter(el=>{
                            return this.filter_nomen.list.kabkot.desa.includes(el);
                        });

                    }


                    if(this.filter_nomen.filter.jenis_sub=='pengembangan'){
                       
                       nomen=nomen.filter(el=>{
                           return this.filter_nomen.list.jenis_sub.pengembangan.includes(el);
                           });

                   }else if(this.filter_nomen.filter.jenis_sub=='pengelolaan'){
                       console.log('pengelolaan');
                       nomen=nomen.filter(el=>{
                           return this.filter_nomen.list.jenis_sub.pengelolaan.includes(el);
                   });
                       
                   }else if(this.filter_nomen.filter.jenis_sub=='pengelolaandanpengembangan'){
                       nomen=nomen.filter(el=>{
                           return this.filter_nomen.list.jenis_sub.pengelolaandanpengembangan.includes(el);
                       });
                       
                   }
                   
                    

                    if(this.filter_nomen.filter.tematik=='peningkatan'){
                        nomen=nomen.filter(el=>{
                            return this.filter_nomen.list.tematik.peningkatan.includes(el);
                        });
                    }

                    this.filter.nomen=nomen;

                    this.filter_nomen.list.def_list=this.filter_nomen.list.def.filter(el=>{
                         return nomen.includes(el.nomen[0]);
                    });

                    this.getDataSeries();
                    return nomen;

                },
                getDataSeries:function(){
                   
                    var self=this;
                    this.chart_data.xAxis.scrollbar.enabled=false;
                    this.chart_data.xAxis.max=this.filter.nomen.length?this.filter.nomen.length-1:1;
                    req_ajax.post('{{route('api-web.mod-dataset-dash.par.rkpdpen.chart-series',["tahun"=>$StahunRoute])}}',this.filter).then(res=>{
                        if(res.status==200){
                            self.chart_data.xAxis.max=self.frame_limit-1;

                            self.chart_data.series=res.data;
                            var pie={
                                    type: 'pie',
                                    name: 'Proporsi Pagu',
                                    data: res.data[0].data,
                                  
                                    showInLegend: false,
                                   
                                };
                            self.chart_prop.series=[pie];
                            var pagu=0;
                            var pemda=[];
                            var pemdakota=[];
                            var provinsi=[];

                            pie.data.forEach(el=>{
                                pagu+=el.y;
                            });
                            res.data[1].data.forEach(el=>{
                               pemda= pemda.concat(el.kodepemda.split(',')).unique();
                            });

                            pemdakota=pemda.filter(el=>{
                                return el.length>3;
                            });
                            provinsi=pemda.filter(el=>{
                                return el.length<=3;
                            });
                            self.chart_data.subtitle.text='Data Akumulasi '+pemda.length+' Pemda ('+pemdakota.length+' Kota/Kab, '+provinsi.length+' Provinsi )';
                            self.chart_prop.subtitle.text='Total Pagu '+self.NumberFormat(pagu);
                            self.chart_data.xAxis.scrollbar.enabled=true;


                        }
                    });
                    
                },
                NumberFormat:function(number){
                    return window.NumberFormat(number);
                },
                chunk:function(input,size){
                    var chunked=[];
                    
                    Array.from({length: Math.ceil(input.length / size)}, (val, i) => {
                    chunked.push(input.slice(i * size, i * size + size))
                    });
                    return chunked;
                },
            }
        });

    </script>
@stop
