<div id="meta-dataset-{{$dataset->id}}">
<div class="modal fade" id="modal-element-{{$dataset->id}}"tabindex="-1" role="dialog">
  <div class="modal-dialog modal-full" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-uppercase">Element Data {{$dataset->name}} </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="table table-responsive">
            <table class="table table-bordered">
                <thead class="thead-dark">
                    <th>No</th>
                    <th>Element</th>
                    <th>Satuan</th>
                    <th>Penangung Jawab</th>
                    <th>Definisi/Konsep</th>
                    <th>Cara Hitung</th>
                </thead>
                <tbody>
                    <tr v-for="item,i in element">
                      <td>@{{i+1}}</td>

                        <td>@{{item.name}}</td>
                        <td>@{{item.satuan}}</td>
                        <td></td>
                        <td>@{{item.definisi_konsep}}</td>
                        <td>@{{item.cara_hitung}}</td>

                    </tr>
                </tbody>
            </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-story-{{$dataset->id}}"tabindex="-1" role="dialog">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-uppercase">Story Data {{$dataset->name}}</h5>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <v-timeline
        :timeline-items="com_story"
        :message-when-no-items="messageWhenNoItemsSory">
        </v-timeline>
    </template>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="bg-secondary">
  <div class="d-fex p-2">
      <button class="btn bg-pink btn-sm"  @click="showStory()">Story Data</button>
      <button class="btn bg-teal btn-sm"  @click="showElement()">Detail Element Data (@{{element.length}})</button>
  </div>
</div>

</div>

@push('js_push')
<script>

  var app_meta_{{$dataset->id}}=new Vue({
    el:"#meta-dataset-{{$dataset->id}}",
    data:{
      element:[],
      story:[],
      current_page:1,
      last_page:1,
      messageWhenNoItemsSory:'Tidak data  history dari dataset ini!',


    },
    computed:{
      com_story:function(){
          return this.story.map(el=>{
            return {
              from:new Date(el.tanggal_pelaksanaan),
              title:el.kegiatan+' ('+moment(el.tanggal_pelaksanaan).format('ll')+')',
              description:el.keterangan
            }
          });
      }
    },
    created:function(){
      this.loadEelemnt();
    },
    methods:{
      loadStory:function(){
        var self=this;
        this.$isLoading(true);
        req_ajax.post('{{route('api-web.mod-dataset-dash.story-data',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id])}}',{
          page:this.current_page
        }).then((res)=>{
          console.log(res.status,res);
          self.current_page=res.data.current_page;
          self.last_page=res.data.last_page;
          self.story=res.data.data;

        }).finally(()=>{  
          $('#modal-story-{{$dataset->id}}').modal();

          self.$isLoading(false);

        })
      },
      loadEelemnt:function(){
        var self=this;

        req_ajax.post('{{route('api-web.mod-dataset-dash.element-data',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id])}}',{
          page:this.current_page
        }).then((res)=>{
          self.element=res.data;
    

        }).finally(()=>{  
        })
      },
      showElement:function(){
        $('#modal-element-{{$dataset->id}}').modal();
      },
      showStory:function(){
        if(this.story.length){
          console.log('exist');
          $('#modal-story-{{$dataset->id}}').modal();
        }else{
          this.loadStory();
        }
      }
    },
  
  })
</script>

@endpush