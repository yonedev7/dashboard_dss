@extends('adminlte::page')
@section('content')
@include('dashboarddataset::partials.dataset_partials.meta',['dataset'=>$dataset])
    <div id="app_info">
        
        <div class="card bg-warning m-0" v-if="jumlah_pemda">
            <div class="card-body">
              
              <p class="m-0 p-0"><b><span class="badge badge-primary"><i class="fa fa-info"></i></span> @{{jumlah_pemda}} Pemda Belum Tervalidasi</b> <span><button class="btn btn-primary btn-sm p-1" @click="showDetail()">Detail</button></span></p>
            </div>
        </div>
        <div class="modal fade" tabindex="-1" role="dialog" id="modal_non_validate">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">Data Pemda Belum Tervalidasi</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>NO</th>
                                <th>PEMDA</th>
                            </tr>

                        </thead>
                        <tbody>
                            <tr v-for="item,i in list_pemda">
                                <td>@{{i+1}}</td>

                                <td>@{{item}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>

          <div class="parallax pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
            <div class="container">
                <section >
                    <h3><b>{{$dataset->name}} Tahun {{$StahunRoute}}</b></h3>
                    <p><b>{!!$dataset->tujuan!!}</b></p>
                </section>
            </div>
        </div>
        <p class="bg-dark text-center mb-0 p-3" v-if="partials_data.length"><b>Tampilan Data Lainya :</b></p>
        <div class="d-flex bg-white align-items-stretch" style="border-bottom:1px solid #000;" >
            <div class="m-0 " v-for="item,i in partials_data">
                    <div class="card  mb-0" style=" background-image: url('{{url('assets/img/back-t.png')}}')">
                        <div class="card-body">
                            <h6><b>@{{item.title}}</b></h6>
                            <a class="btn btn-success" :href="('{{route('mod.dash.dataset.partials_data',['tahun'=>$StahunRoute,'id'=>'xxxx'])}}').replace('xxxx',item.id)">
                                <i class="fa fa-arrow-right"></i> Detail
                            </a>
                        </div>
                    </div>
            </div>
            
        </div>
       
          
    </div>
    
    
<div id="app">
    <div class="modal fade" id="modal-detail" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-full">
          <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@{{detailModal.nama_pemda}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <div class="modal-body">
                <charts v-if="detailModal.chart.series.length" :options="detailModal.chart"></charts>
                <div class="" v-if="detailModal.kodepemda">
                    <p class="bg-dark p-2 text-uppercase" ><b>Data @{{detailModal.nama_pemda}} Tahun @{{detailModal.tahun}}</b></p>
                    <data-table ref="table_detail" :debounce-delay="1000"
                    :columns="detailModal.columns"
                    :url="com_url_detail_data_table">
                    </data-table>
                </div>
            </div>
            <div class="modal-footer">
              </div>
          </div>
        </div>
      </div>
    <div class="p-3 bg-dark text-center">
        <small>Keterisian</small>
        <p>@{{rekap.melapor.provinsi+rekap.melapor.kabkot}} (@{{rekap.melapor.provinsi}} Provinsi @{{rekap.melapor.kabkot}} Kab/Kota ) / @{{rekap.jumlah_pemda}} PEMDA</p>
        <div class="progress">
            <div class="progress-bar" role="progressbar" v-bind:style="'width: '+com_percentage_rekap+'%'" v-bind:aria-valuenow="com_percentage_rekap" aria-valuemin="0" aria-valuemax="100">@{{com_percentage_rekap}}%</div>
          </div>
    </div>
    
    <div class="d-flex p-3 " style="width:100vw; overflow:scroll; background-color:#fff">
        <div class="d-flex">
            <charts :constructor-type="'mapChart'" :options="ChartRegI"></charts>
        </div>
        <div class="d-flex">
            <charts :constructor-type="'mapChart'" :options="ChartRegII"></charts>
        </div>
        <div class="d-flex">
            <charts :constructor-type="'mapChart'" :options="ChartRegIII"></charts>
        </div>
        <div class="d-flex">
            <charts :constructor-type="'mapChart'" :options="ChartRegIV"></charts>
        </div>
    </div> 
    <div class="p-3">
        <p class="p-0 m-0"><small>Keterangan Pewarnaan Peta</small></p>
        <span class="badge badge-danger"> Tidak Memiliki Data</span>
        <span class="badge bg-info"> Data Provinsi</span>
        <span class="badge bg-orange text-white"> Data Kota/Kab</span>


    </div>

    <div class="bg-white mt-1">
        <charts :constructor-type="'mapChart'" :options="series_pusat.chart"></charts>
        
    </div>
  

    
</div>

@stop

@section('js')
<script>
    var app_info=new Vue({
        el:"#app_info",
        data:{
            partials_data:<?=json_encode($partials_data)?>,
            filter:{
                filter_regional:'{{$req['filter_regional']}}',
                filter_sortlist:'{{$req['filter_sortlist']}}',
                filter_tipe_bantuan:'{{$req['filter_tipe_bantuan']}}',
                filter_jenis_pemda:'{{$req['filter_jenis_pemda']}}',
            },
            jumlah_pemda:0,
            list_pemda:[]
        },
        methods:{
            showDetail:function(){
                $('#modal_non_validate').modal();
            }
        },
        created:function(){
            var self=this;
            req_ajax.post('{{route('api-web.mod-dataset-dash.count-not-validate',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id])}}',this.filter).then(res=>{
                if(res.status==200){
                    self.jumlah_pemda=parseInt(res.data.jumlah_pemda);
                    self.list_pemda=(res.data.list_pemda);
                }
            });
        }
    })
    var app=new Vue({
        el:'#app',
        created:function(){
           this.init();
        },
        watch:{
            "detailModal.tahun":function(){
                if(this.$refs.table_detail!=undefined){
                this.$refs.table_detail.tableData.data=[];

                }
            }
        },
        computed:{
            com_percentage_rekap:function(){
                var agre=this.rekap.melapor.kabkot+this.rekap.melapor.provinsi;
                if(agre!=0 && this.rekap.jumlah_pemda!=0){
                    return ((agre/this.rekap.jumlah_pemda)*100).toFixed(2);
                }else{
                    return 0;
                }
            },
            com_url_detail_data_table:function(){
                return ('{{route('api-web.mod-dataset-dash.load-data',['tahun'=>'@?','id_dataset'=>$dataset->id,'kodepemda'=>'@?'])}}').yoneReplaceParam('@?',[this.detailModal.tahun,this.detailModal.kodepemda]);
            }
        },
        methods:{
            LegendClick:function(visible,reg,index){
                var self=this;
                if(this['ChartReg'+reg]!=undefined){
                    var chart=this['ChartReg'+reg].series;
                    if(visible){
                    
                    //     // false
                    //     if(index==2){
                    //         // provinsi true
                    //         chart[3].visible=true;
                    //         chart[1].visible=true;
                    //         chart[2].visible=false;
                    //         // chart[2].name='Provinsi';

                    //         // chart[2].index=3;
                    //         // chart[3].index=2;
                    //     }else{
                    //           // kabkot true
                    //         chart[1].visible=false;
                    //         chart[2].visible=true;
                    //         chart[3].visible=false;
                    //         // chart[2].name='Kab/Kota';
                    //         // chart[2].index=2;
                    //         // chart[3].index=3;
                    //     }
                        
                    // }else{
                    //     // true
                    //     if(index==2){
                    //         // provinsi
                    //         chart[1].visible=false;
                    //         chart[2].visible=true;
                    //         chart[3].visible=false;
                    //         // chart[2].name='Kab/Kota';

                    //         // chart[2].index=2;
                    //         // chart[3].index=3;

                    //     }else{
                    //         // kabkot
                           
                    //         chart[3].visible=true;
                    //         chart[1].visible=true;
                    //         chart[2].visible=false;
                    //         // chart[2].name='Provinsi';

                    //         // chart[2].index=3;
                    //         // chart[3].index=2;
                    //     }

                    }
                    // eval("self.ChartReg"+reg+'.series=chart;');
                    // this['ChartReg'+reg].series=chart;
                    // console.log(reg,chart);

                }
            },
            showDetail:function(kodepemda,nama_pemda){
                this.detailModal.nama_pemda=nama_pemda;
                this.detailModal.kodepemda=kodepemda;
                this.detailModal.tahun={{$StahunRoute}};
                var self=this;
                this.detailModal.chart.series=[];
                this.detailModal.chart.subtitle.text=nama_pemda;
                this.detailModal.chart.title.text='{{$dataset->name}}';
                this.$isLoading(true);
                req_ajax.post(('{{route('api-web-mod.dataset.public.data.series.pemda',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id,'kodepemda'=>'@?'])}}').yoneReplaceParam('@?',[this.detailModal.kodepemda])).then(res=>{
                    if(res.status==200){
                        if(res.data.data[0]!=undefined){
                            self.detailModal.chart.series=res.data.data[0].series;
                        }else{
                            self.detailModal.chart.series=[];
                        }
                    }
                }).finally(function(){
                    self.$isLoading(false);
                     $('#modal-detail').modal();

                });

            },
            init:function(){
                var self=this;
                this.$isLoading(true);
                req_ajax.post(('{{route('api-web-mod.dataset.public.data.series',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id])}}'),this.filter).then(res=>{
                    if(res.status==200){
                        var ser=[];
                        if(res.data.data[0]!=undefined){
                            self.series_pusat.chart.series=res.data.data[0].series;
                            self.series_pusat.chart.series.push({
                                'name':'Jumlah Pemda',
                                'type':'line',
                                'yAxis':1,
                                'data':res.data.data[0].series[0].data.map(el=>{
                                    return {name:el.name,y:el.jumlah_pemda};
                                })
                            })

                        }else{
                            self.series_pusat.chart.series=[];
                        }
                        
                    }
                });
                req_ajax.post('{{route('api-web.mod-dataset-dash.keterisian',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id])}}',self.filter).then(function(res){
                    if(res.status==200){
                        res.data.forEach(el=>{
                         
                            if(self['ChartReg'+el['REGIONAL']]!=undefined){
                                self['ChartReg'+el['REGIONAL']].subtitle.text='Tahun {{$StahunRoute}} - '+(el.data.kabkot.length+el.data.provinsi.length)+" ("+(el.data.provinsi.length +' Provinsi, '+el.data.kabkot.length+' Kab/Kota')+') / '+el.pemdas.length+' Pemda'

                                self['ChartReg'+el['REGIONAL']]['series'][0].data=el.pemdas;
                                self['ChartReg'+el['REGIONAL']]['series'][1].data=el.pemdas;
                                self['ChartReg'+el['REGIONAL']]['series'][2].showInLegend=true;
                                 
                                if(self['ChartReg'+el['REGIONAL']]['series'][3]!=undefined){
                                    self['ChartReg'+el['REGIONAL']]['series'][3]['data']=el.data.kabkot;
                                }

                                if(self['ChartReg'+el['REGIONAL']]['series'][2]!=undefined){
                                    self['ChartReg'+el['REGIONAL']]['series'][2]['data']=el.data.provinsi;
                                }

                            }
                    
                            switch(el['REGIONAL']){
                                case 'IV':
                                center=[350,50];
                                break;
                                case 'I':
                                center=[100,100];
                                break;
                                case 'II':
                                center=[100,150];
                                break;

                                case 'III':
                                center=[100,150];
                                break;
                                default:
                                center=[100,80];

                                break;
                            }

                            self.rekap.melapor.kabkot+=el.data.kabkot.length;
                            self.rekap.jumlah_pemda+=el.pemdas.length;
                            self.rekap.melapor.provinsi+=el.data.provinsi.length;


                            self['ChartReg'+el['REGIONAL']]['series'][5]=({
                                name:'Jumlah Pemda',
                                type:'pie',
                                linkedTo:'persentase',
                                showInLegend:false,
                                visible:true,
                                size:50,
                                index:5,
                                center:center,
                                xAxis:{
                                    type:'category'
                                },
                                data:[
                                   
                                    {
                                        name:'Kab/Kota',
                                        y:el.data.kabkot.length
                                    },
                                    {
                                        name:'Provinsi',
                                        y:el.data.provinsi.length
                                    },
                                    {   
                                        name:'Belumn Memiliki Data',
                                        y:el.pemdas.length-(el.data.kabkot.length+el.data.provinsi.length)
                                        
                                    }
                                ]
                            });
                           
                        });
                        self.series_pusat.chart.subtitle.text='Lokus NUWSP '+(self.rekap.melapor.provinsi+self.rekap.melapor.kabkot) +' ('+(self.rekap.melapor.provinsi)+' Provinsi,'+(self.rekap.melapor.kabkot)+' Kab/Kota) / '+(self.rekap.jumlah_pemda)+' PEMDA';
                    }
                }).finally(()=>{
                    self.$isLoading(false);
                });
            }
        },
        data:{
           
            series_pusat:{
                chart:{
                    chart:{
                        type:'column',
                        height:300,
                    },
                    plotOptions:{
                        series: {
                                stacking: 'normal'
                            },
                        line:{
                            dataLabels:{
                                enabled:true,
                                format:'{point.y} Pemda'

                            },
                            point:{
                                events:{
                                    click:function(){
                                        if(this.name=='{{$StahunRoute}}'){

                                        }else{
                                            var url=('{{url()->current()}}').yoneReplaceParam('session/{{$StahunRoute}}',['session/'+this.name]);
                                            window.open(url,'{{md5(url()->current())}}')
                                        }
                                    }
                                }
                            }
                        },
                        column:{
                            dataLabels:{
                                enabled:true,

                            },
                            point:{
                                events:{
                                    click:function(){
                                        if(this.name=='{{$StahunRoute}}'){

                                        }else{
                                            var url=('{{url()->current()}}').yoneReplaceParam('session/{{$StahunRoute}}',['session/'+this.name]);
                                            window.open(url,'{{md5(url()->current())}}')
                                        }
                                    }
                                }
                            }
                        }
                    },
                    title:{
                        text:'Pergerakan Data {{$dataset->name}}'
                    },
                    subtitle:{
                        text:'Nasional'
                    },
                    yAxis:[
                        {
                            name:''
                        },
                        {
                            title:{
                                text:'Jumlah Pemda'
                            }
                        }
                    ],
                    xAxis:{
                        type:'category'
                    },
                    series:[]
                }
            },
            detailModal:{
                kodepemda:'',
                columns:<?=json_encode($columns)?>,
                nama_pemda:'',
                tahun:'',
                chart:{
                    chart:{
                        type:'line',
                        height:300
                    },
                    plotOptions:{
                        line:{
                            dataLabels:{
                                enabled:true
                            },
                            point:{
                                events:{
                                    click:function(){
                                        console.log(this.name);
                                    
                                        window.app.detailModal.tahun=parseInt(this.name);
                                        
                                    }
                                }
                            }
                        },
                        
                    },
                    title:{
                        text:''
                    },
                    subtitle:{
                        text:''
                    },
                    xAxis:{
                        type:'category'
                    },
                    series:[]
                }
            },
            rekap:{
                melapor:{
                    provinsi:0,
                    kabkot:0,
                },
                jumlah_pemda:0

            },
            filter:{
                filter_regional:'{{$req['filter_regional']}}',
                filter_sortlist:'{{$req['filter_sortlist']}}',
                filter_tipe_bantuan:'{{$req['filter_tipe_bantuan']}}',
                filter_jenis_pemda:'{{$req['filter_jenis_pemda']}}',
            },
            ChartRegI:{
                chart:{
                    type:'map',
                    map:Highcharts.maps['ind_kab']
                },
                title:{
                    text:'keterisian Regional I',
                },
                plotOptions: {
                    map:{
                        point:{
                            events:{
                                click:function(){
                                    if(this.options['clickable']!=undefined){
                                    window.app.showDetail(this.kodepemda,this.options.name);
                                    }
                                }
                            }
                        }
                    },
                    pie: {
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>:<br>{point.percentage:.1f} %<br>{point.y} Pemda',
                        }
                    }
                },
                subtitle:{
                    text:'Tahun {{$StahunRoute}}'
                },
                series:[
                    {
                        name:'Pemda',
                        data:[],
                        index:0,
                        color:'#fff',
                        joinBy:['id','kode_provinsi'],
                        mapData:Highcharts.maps['ind'],
                        showInLegend:false,
                        visible:true,
                        allAreas:false,
                       
                    },
                    {
                        name:'PemdaKb',
                        data:[],
                        index:1,
                        color:'red',
                        joinBy:['id','id'],
                        showInLegend:false,
                        visible:true,
                        allAreas:false,
                       
                    },
                    
                    {
                        index:2,
                        mapData:Highcharts.maps['ind'],
                        name:'Provinsi',
                        id:'provinsi',
                        data:[],
                        joinBy:['id','id'],
                        allAreas:false,
                        visible:true,
                        events:{
                            legendItemClick:function(){
                                window.app.LegendClick(this.visible,'I',2);
                               
                            }
                        }

                    },
                    {
                        index:3,
                        name:'Kab/Kota',
                        data:[],
                        id:'kabkot',
                        joinBy:['id','id'],
                        allAreas:false,
                        visible:true,
                        events:{
                            legendItemClick:function(){
                                window.app.LegendClick(this.visible,'I',3);
                            }
                        }

                    },
                    {
                        name:'Persentase',
                        data:[],
                        index:4,
                        id:'persentase',
                        joinBy:['id','id'],
                        allAreas:false,
                        visible:true,
                        
                    },

                ]
            },
            ChartRegII:{
                chart:{
                    type:'map',
                    map:Highcharts.maps['ind_kab']
                },
                title:{
                    text:'keterisian Regional II',
                },
                plotOptions: {
                    map:{
                        point:{
                            events:{
                                click:function(){
                                    if(this.options['clickable']!=undefined){
                                    window.app.showDetail(this.kodepemda,this.options.name);
                                    }
                                }
                            }
                        }
                    },
                    pie: {
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>:<br>{point.percentage:.1f} %<br>{point.y} Pemda',
                        }
                    }
                },
                subtitle:{
                    text:'Tahun {{$StahunRoute}}'
                },
                series:[
                    {
                        name:'Pemda',
                        data:[],
                        index:0,
                        color:'#fff',
                        joinBy:['id','kode_provinsi'],
                        mapData:Highcharts.maps['ind'],
                        showInLegend:false,
                        visible:true,
                        allAreas:false,
                       
                    },
                    {
                        name:'PemdaKb',
                        data:[],
                        index:1,
                        color:'red',
                        joinBy:['id','id'],
                        showInLegend:false,
                        visible:true,
                        allAreas:false,
                       
                    },
                    
                    {
                        index:2,
                        mapData:Highcharts.maps['ind'],
                        name:'Provinsi',
                        data:[],
                        joinBy:['id','id'],
                        allAreas:false,
                        visible:false,
                        events:{
                            legendItemClick:function(){
                                window.app.LegendClick(this.visible,'II',2);
                                
                            }
                        }

                    },
                    {
                        index:3,
                        name:'Kab/Kota',
                        data:[],
                        id:'kabkot',
                        joinBy:['id','id'],
                        allAreas:false,
                        visible:true,
                        events:{
                            legendItemClick:function(){
                                window.app.LegendClick(this.visible,'II',3);
                             
                            }
                        }

                    },
                    {
                        name:'Persentase',
                        data:[],
                        id:'persentase',
                        joinBy:['id','id'],
                        allAreas:false,
                        visible:true,
                        
                    },

                ]
            },
            ChartRegIII:{
                chart:{
                    type:'map',
                    map:Highcharts.maps['ind_kab']
                },
                title:{
                    text:'keterisian Regional III',
                },
                plotOptions: {
                    map:{
                        point:{
                            events:{
                                click:function(){
                                    if(this.options['clickable']!=undefined){
                                    window.app.showDetail(this.kodepemda,this.options.name);
                                    }
                                }
                            }
                        }
                    },
                    pie: {
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>:<br>{point.percentage:.1f} %<br>{point.y} Pemda',
                        }
                    }
                },
                subtitle:{
                    text:'Tahun {{$StahunRoute}}'
                },
                series:[
                    {
                        name:'Pemda',
                        data:[],
                        index:0,
                        color:'#fff',
                        joinBy:['id','kode_provinsi'],
                        mapData:Highcharts.maps['ind'],
                        showInLegend:false,
                        visible:true,

                        allAreas:false,
                       
                    },
                    {
                        name:'PemdaKb',
                        data:[],
                        index:1,
                        
                        color:'red',
                        joinBy:['id','id'],
                        showInLegend:false,
                        visible:true,
                        allAreas:false,
                       
                    },
                    
                    {
                        index:2,
                        mapData:Highcharts.maps['ind'],
                        name:'Provinsi',
                        data:[],
                        joinBy:['id','id'],
                        allAreas:false,
                        visible:false,
                        events:{
                            legendItemClick:function(){
                                window.app.LegendClick(this.visible,'III',2);
                                
                            }
                        }

                    },
                    {
                        index:3,
                        name:'Kab/Kota',
                        data:[],
                        id:'kabkot',
                        joinBy:['id','id'],
                        allAreas:false,
                        visible:true,
                        events:{
                            legendItemClick:function(){
                                window.app.LegendClick(this.visible,'III',3);   
                            }
                        }

                    },
                    {
                        name:'Persentase',
                        data:[],
                        id:'persentase',
                        joinBy:['id','id'],
                        allAreas:false,
                        visible:true,
                        
                    },

                ]
            },
            ChartRegIV:{
                chart:{
                    type:'map',
                    map:Highcharts.maps['ind_kab']
                },
                title:{
                    text:'keterisian Regional IV',
                },
                plotOptions: {
                    map:{
                        point:{
                            events:{
                                click:function(){
                                    if(this.options['clickable']!=undefined){
                                    window.app.showDetail(this.kodepemda,this.options.name);
                                    }
                                }
                            }
                        }
                    },
                    pie: {
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>:<br>{point.percentage:.1f} %<br>{point.y} Pemda',
                        }
                    }
                },
                subtitle:{
                    text:'Tahun {{$StahunRoute}}'
                },
                series:[
                    {
                        name:'Pemda',
                        data:[],
                        index:0,
                        color:'#fff',
                        joinBy:['id','kode_provinsi'],
                        mapData:Highcharts.maps['ind'],
                        showInLegend:false,
                        visible:true,

                        allAreas:false,
                       
                    },
                    {
                        name:'PemdaKb',
                        data:[],
                        index:1,
                        
                        color:'red',
                        joinBy:['id','id'],
                        showInLegend:false,
                        visible:true,
                        allAreas:false,
                       
                    },
                    
                    {
                        index:2,
                        mapData:Highcharts.maps['ind'],
                        name:'Provinsi',
                        data:[],
                        joinBy:['id','id'],
                        allAreas:false,
                        visible:false,
                        events:{
                            legendItemClick:function(){
                                window.app.LegendClick(this.visible,'IV',2);
                                
                            }
                        }

                    },
                    {
                        index:3,
                        name:'Kab/Kota',
                        data:[],
                        id:'kabkot',
                        joinBy:['id','id'],
                        allAreas:false,
                        visible:true,
                        events:{
                            legendItemClick:function(){
                                window.app.LegendClick(this.visible,'IV',3);
                                
                            }
                        }

                    },
                    {
                        name:'Persentase',
                        data:[],
                        id:'persentase',
                        joinBy:['id','id'],
                        allAreas:false,
                        visible:true,
                        
                    },

                ]
            },
        }
    });

</script>

@stop

