@extends('adminlte::page')
@section('content')

    <div class="parallax pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
        <div class="container">
            <section >
                <h3><b>{{$dataset->name}}</b></h3>
                <p><b>{!!$dataset->tujuan!!}</b></p>
            </section>
        </div>
    </div>
    <div class="p-3 bg-white" id="app">
        <charts v-if="detailModal.chart.series.length" :options="detailModal.chart"></charts>
        <div class="" v-if="detailModal.kodepemda">
            <p class="bg-dark p-2"><b>Data @{{detailModal.nama_pemda}} Tahun @{{detailModal.tahun}}</b></p>
            <data-table
            :columns="detailModal.columns"
            :url="('{{route('api-web.mod-dataset-dash.load-data',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id,'kodepemda'=>'xxxx'])}}').replace('xxxx',detailModal.kodepemda)">
        </data-table>
        </div>
    </div>
   
@stop

@section('js')
<script>
    var app=new Vue({
        el:'#app',
        created:function(){
           this.showDetail('0','NASIONAL');
        },
        computed:{
            com_percentage_rekap:function(){
                var agre=this.rekap.melapor.kabkot+this.rekap.melapor.provinsi;
                if(agre!=0 && this.rekap.jumlah_pemda!=0){
                    return ((agre/this.rekap.jumlah_pemda)*100).toFixed(2);
                }else{
                    return 0;
                }
            }
        },
        methods:{
            showDetail:function(kodepemda,nama_pemda){
                this.detailModal.nama_pemda=nama_pemda;
                this.detailModal.kodepemda=kodepemda;
                this.detailModal.tahun={{$StahunRoute}};
                var self=this;
                this.detailModal.chart.series=[];
                this.detailModal.chart.subtitle.text=nama_pemda;
                this.detailModal.chart.title.text='{{$dataset->name}}';
                req_ajax.post(('{{route('api-web.mod-dataset-dash.time-series',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id,'kodepemda'=>'xxxx'])}}').replace('xxxx',kodepemda)).then(res=>{
                    if(res.status==200){
                        self.detailModal.chart.series=res.data;
                    }
                });
                $('#modal-detail').modal();

            },
          
        },
        data:{
            detailModal:{
                kodepemda:'',
                columns:<?=json_encode($columns)?>,
                nama_pemda:'',
                tahun:'',
                chart:{
                    chart:{
                        type:'line',
                        height:300
                    },
                    plotOptions:{
                        line:{
                            dataLabels:{
                                enabled:true
                            }
                        }
                    },
                    title:{
                        text:''
                    },
                    subtitle:{
                        text:''
                    },
                    xAxis:{
                        type:'category'
                    },
                    series:[]
                }
            },
            rekap:{
                melapor:{
                    provinsi:0,
                    kabkot:0,
                },
                jumlah_pemda:0

            },
            filter:{
                filter_regional:'{{$req['filter_regional']}}',
                filter_sortlist:'{{$req['filter_sortlist']}}',
                filter_tipe_bantuan:'{{$req['filter_tipe_bantuan']}}',
                filter_jenis_pemda:'{{$req['filter_jenis_pemda']}}',
            },
           
        }
    });

</script>

@stop