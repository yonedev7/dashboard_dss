@extends('adminlte::page')
@php
    $regional=$req->filter_regional;
    $kodepemda=['=',null];

    if($req->kodepemda){
        $kodepemda[1]=$req->kodepemda;
    }
   
   $filter=['regional'=>$regional,'sortlist'=>$req->filter_sortlist,'lokus'=>true,'tipe_bantuan'=>$req->filter_tipe_bantuan,'tipe_pemda'=>$req->filter_jenis_pemda??3,$kodepemda];
   $pemdas=\App\Http\Controllers\FilterCtrl::filter_pemda($filter['regional'],$filter['lokus'],$filter['sortlist'],$filter['tipe_bantuan'],$filter['tipe_pemda'],$kodepemda);
   if($kodepemda[1]){

    }else{
        $data=DB::table('dataset.monev_rkpd_apbd_2022')
        ->selectRaw("kode,max(subkegiatan) as subkegiatan,sum(case when (pagu_rkpd_2021 is null) then 0 else pagu_rkpd_2021 end) as pagu_rkpd_2021,sum(case when (pagu_rkpd_2022 is null) then 0 else pagu_rkpd_2022 end) as pagu_rkpd_2022,sum(case when (anggaran_2021 is null) then 0 else anggaran_2021 end) as anggaran_2021, sum(case when (anggaran_2022 is null) then 0 else anggaran_2022 end) as anggaran_2022")
        ->groupBy('kode')
        ->whereIn('kodepemda',$pemdas);
        if($req->filter_jenis_subkegiatan){
            $data=$data->whereIn('kode',['1.03.03.2.01.04','1.03.03.2.01.06','1.03.03.2.01.19','1.03.03.2.01.08'  ,'1.03.03.2.01.03','1.03.03.2.01.05','1.03.03.2.01.07','1.03.03.2.01.20','1.03.03.1.01.07','1.03.03.1.01.09','1.03.03.1.01.04']);
        }
        if($req->sorted){
            $data=$data->orderBy(DB::raw("sum(case when (".$req->sorted." is null) then 0 else ".$req->sorted." end)"),'desc');

        }else{
            $data=$data->orderBy('kode','asc');
        }
        
        $data=$data->get();
        $series=[
            
           
            'anggaran_2021'=>[
                'name'=>'Anggaran 2021',
                'color'=>'#2a78f5',
                'xAxis'=>1,

                'data'=>[]
            ],
            'pagu_2021'=>[
                'name'=>'RKPD 2021',
                'color'=>'#2af54b',
                'xAxis'=>0,
                'data'=>[]
            ],
           
            'anggaran_2022'=>[
                'name'=>'Anggaran 2022',
                'color'=>'#2a78f5',
                'xAxis'=>1,

                'data'=>[]
            ],
            'pagu_2022'=>[
                'name'=>'RKPD 2022',
                'color'=>'#2af54b',
                'xAxis'=>0,

                'data'=>[]
            ],

        ];
        foreach ($data as $key => $d) {
            if(!isset($series['pagu_2021']['data'][$d->kode])){
            $series['pagu_2021']['data'][$d->kode]=[
                'name'=>$d->subkegiatan,
                'xAxis'=>$series['anggaran_2022']['xAxis'],
                'color'=>$series['pagu_2021']['color'],
                'y'=>0,
                'value'=>0

            ];   
            }
            if(!isset($series['anggran_2021']['data'][$d->kode])){
            $series['anggaran_2021']['data'][$d->kode]=[
                'name'=>$d->subkegiatan,
                'xAxis'=>$series['anggaran_2022']['xAxis'],
                'color'=>$series['anggaran_2021']['color'],
                'y'=>0,
                'value'=>0

            ];   
            }
            if(!isset($series['pagu_2022']['data'][$d->kode])){
            $series['pagu_2022']['data'][$d->kode]=[
                'name'=>$d->subkegiatan,
                'xAxis'=>$series['anggaran_2022']['xAxis'],

                'color'=>$series['pagu_2022']['color'],
                'y'=>0,
                'value'=>0

            ];   
            }
            if(!isset($series['anggran_2022']['data'][$d->kode])){
            $series['anggaran_2022']['data'][$d->kode]=[
                'name'=>$d->subkegiatan,
                'xAxis'=>$series['anggaran_2022']['xAxis'],
                'color'=>$series['anggaran_2022']['color'],
                'y'=>0,
                'value'=>0
            ];   
            }
            $series['pagu_2021']['data'][$d->kode]['y']+=(int)$d->pagu_rkpd_2021??0;
            $series['pagu_2022']['data'][$d->kode]['y']+=(int)$d->pagu_rkpd_2022??0;
            $series['anggaran_2021']['data'][$d->kode]['y']+=(int)$d->anggaran_2021??0;
            $series['anggaran_2022']['data'][$d->kode]['y']+=(int)$d->anggaran_2022??0;
            

            $series['pagu_2021']['data'][$d->kode]['value']+=(int)$d->pagu_rkpd_2021??0;
            $series['pagu_2022']['data'][$d->kode]['value']+=(int)$d->pagu_rkpd_2022??0;
            $series['anggaran_2021']['data'][$d->kode]['value']+=(int)$d->anggaran_2021??0;
            $series['anggaran_2022']['data'][$d->kode]['value']+=(int)$d->anggaran_2022??0;

        }

        $series['anggaran_2022']['data']=array_values( $series['anggaran_2022']['data']);
        $series['pagu_2022']['data']=array_values( $series['pagu_2022']['data']);
        $series['pagu_2021']['data']=array_values( $series['pagu_2021']['data']);
        $series['anggaran_2021']['data']=array_values( $series['anggaran_2021']['data']);




        $pagu_2021=array_map(function($el){
            $el['y']=-1*$el['y'];
            return $el;
        },array_values($series['pagu_2021']['data']));
        $series['pagu_2021']['data']=$pagu_2021;

        $anggaran_2021=array_map(function($el){
                $el['y']=-1*$el['y'];
                return $el;
            },array_values($series['anggaran_2021']['data']));

        $series['anggaran_2021']['data']=$anggaran_2021;

        $series=array_values($series);
    }
    

@endphp



@section('content')
<div class="card">
    <div class="card-body">
        @include('sistem_informasi.partials.filter',['req'=>$req])
      
    </div>
</div>
    <div class="" id="app">
        <div class="p-3">
            <form action="{{url()->current()}}" method="get"  id="form-filter-partial">
                <input type="hidden" name="filter_regional" value="{{$req->filter_regional}}">
                <input type="hidden" name="filter_sortlist" value="{{$req->filter_sortlist}}">
                <input type="hidden" name="filter_tipe_bantuan" value="{{$req->filter_tipe_bantuan}}">
                <div class="row">
                    <div class="col-3">
                    
                    
                                <div class="form-group">
                                    <label for="">Jenis Sub Kegiatan</label>
                                    <select name="filter_jenis_subkegiatan" class="form-control" id="" v-model="filter_partials.jenis_subkegiatan">
                                        <option value=""></option>
                                        <option  value="PENINGKATAN">PENINGKATAN</option>
                                    </select>
                    
                                </div>
                    </div>
                    <div class="col-3">
                    
                    
                        <div class="form-group">
                            <label for="">Sorted By</label>
                            <select name="sorted" class="form-control" id="" v-model="filter_partials.sorted">
                                <option value=""></option>
                                <option value="pagu_rkpd_2021">RKPD 2021</option>
                                <option  value="pagu_rkpd_2022">RKPD 2022</option>
                                <option  value="anggaran_2021">Anggaran 2021</option>
                                <option  value="anggaran_2022">Anggaran 2022</option>

                            </select>
            
                        </div>
            </div>
            </form>

            </div>
        </div>
     <div class="card">
         <div class="card-body">
            <charts :options="chart"></charts>
         </div>
     </div>
      <div class="p-3">
        <div class="table-responsive">
            <table class="table table-bordered" id="table-data">
                <thead class="thead-dark">
                    <tr>
                        <th>AKSI</th>
                        <th>KODE SUB KEGIATAN</th>
                        <th>NAMA SUB KEGIATAN</th>
                        <th>PAGU 2021</th>
                        <th>ANGGARAN 2021</th>
                        <th>PAGU 2022</th>
                        <th>ANGGARAN 2022</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $k=>$d)
                    <tr>
                        <td>
                            <button onclick="app.detail('{{$d->kode}}')" class="btn btn-primary btn-sm">Detail</button>
                        </td>
                        <td>{{$d->kode}}</td>
                        <td>{{$d->subkegiatan}}</td>
                        <td>{{number_format($d->pagu_rkpd_2021??0,0)}}</td>
                        <td>{{number_format($d->anggaran_2021??0,0)}}</td>
                        <td>{{number_format($d->pagu_rkpd_2022??0,0)}}</td>
                        <td>{{number_format($d->anggaran_2022??0,0)}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
      </div>
    </div>
    
@stop

@section('js')
<script>

    var app=new Vue({
        el:'#app',
        watch:{
            filter_partials:{
                deep:true,
                handler:function(){
                    $('#form-filter-partial').submit();
                    }
            }

            },
        data:{
            filter_partials:{
                jenis_subkegiatan:'{{$req->filter_jenis_subkegiatan}}',
                filter_jenis_pemda:'{{$req->filter_jenis_pemda}}',
                filter_sortlist:'{{$req->filter_sortlist}}',
                filter_regional:'{{$req->filter_regional}}',
                filter_tipe_bantuan:'{{$req->filter_tipe_bantuan}}',
                sorted:'{{$req->sorted}}'
            },
            chart:{
                chart:{
                    type:'bar',
                    height:750,
                    min:0,
                    max:10,
                    scrollbar:{
                        enabled:true
                    }

                },
                plotOptions: {
                    series: {
                        // stacking: 'normal',
                        dataLabels: {
                            // formatter:'{(y<0?y*-1:y)}',
                            // enabled: true
                        }
                    },
                    
                },
                tooltip: {
                    headerFormat: '<b>{series.name}</b><br>',
                    pointFormat:'{point.name} : {point.value:,.0f}'
                },
                title:{
                    text:'Sinkronisasi RKPD APBD'
                },
                xAxis:[
                {
                    type:'category',
                    reversed: false,
                    title:{
                        text:'Tahun 2021'
                    },
                    labels: {
                        step: 1
                    },
                    accessibility: {
                        description: 'Age (male)'
                    },
                    
                    }, 
                    { // mirror axis on right side
                    type:'category',
                    title:{
                        text:'Tahun 2022'
                    },
                    opposite: true,
                    reversed: false,

                    labels: {
                        step: 1
                    },
                    accessibility: {
                        description: 'Age (female)'
                    },
                    
                }],
                yAxis: {
                    title:{
                        text:'Rp.'
                    },
                   labels:{
                       enabled:true,
                       formatter:function(){
                           return Math.abs(this.value);
                       }
                   }
                },
                series:<?=json_encode($series)?>
            }
        }
    })

    var table_data=$('#table-data').DataTable();

</script>

@stop