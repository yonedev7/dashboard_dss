<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/




Route::prefix('/mod/{tahun}/dataset-dashboard')->name('api-web.mod-dataset-dash')->group(function () {
    Route::post('/keterisian/{id_dataset}','DashboardDatasetController@keterisian')->name('.keterisian');
    Route::post('/element-data/{id_dataset}','DashboardDatasetController@elementData')->name('.element-data');
    Route::post('/story/{id_dataset}','DashboardDatasetController@story')->name('.story-data');


    Route::get('/data/{id_dataset}/{kodepemda}','DashboardDatasetController@dataPerPemda')->name('.load-data');
    Route::post('/time-series/{id_dataset}/{kodepemda}','DashboardDatasetController@timeSeris')->name('.time-series');
    Route::post('/time-series-pusat/{id_dataset}/','DashboardDatasetController@timeSerisPusat')->name('.time-series-pusat');
    Route::post('/keterisian-not-validate/{id_dataset}/','DashboardDatasetController@notValidate')->name('.count-not-validate');
});

include __DIR__.'/api_rkpdpeningkatan.php';
include __DIR__.'/api_apbd.php';
