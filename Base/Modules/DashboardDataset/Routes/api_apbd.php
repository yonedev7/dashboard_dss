<?php

Route::prefix('/mod/{tahun}/dataset-dashboard/partials/apbd-proporsi')->name('api-web.mod-dataset-dash.par.apbdprop')->group(function () {
    Route::get('/meta','APBDProporsiController@getMeta')->name('.meta');
    Route::get('/get-data','APBDProporsiController@getData')->name('.data');
    Route::post('/proporsi-data-akun-persub/{kodesubkegiatan}','APBDProporsiController@getDataProporsiAkun')->name('.data-proporsi-akun');
    Route::post('/proporsi-data-akun-perpemda/{kode_akun}','APBDProporsiController@getDataProporsiAkunPerPemda')->name('.data-proporsi-akun-pemda');
    Route::post('/pemda-proporsi-akun/{kodesub}/{kodepemda}','APBDProporsiController@getDataPemdaPropAkun')->name('.data-pemda-prop-akun');
    Route::post('/get-chart-series','APBDProporsiController@getChartSeries')->name('.chart-series');
});