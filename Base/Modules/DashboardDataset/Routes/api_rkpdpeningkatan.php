<?php

Route::prefix('/mod/{tahun}/dataset-dashboard/partials/rkpd-peningkatan')->name('api-web.mod-dataset-dash.par.rkpdpen')->group(function () {
    Route::get('/meta','RKPDPeningkatanController@getMeta')->name('.meta');
    Route::get('/get-data','RKPDPeningkatanController@getData')->name('.data');
    Route::post('/get-chart-series','RKPDPeningkatanController@getChartSeries')->name('.chart-series');


});