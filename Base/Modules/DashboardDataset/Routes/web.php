<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('dash/{tahun}/sistem-informasi/module-dashboard-dataset')->name('mod.dash.dataset-admin')->middleware(['auth:web','bindTahun'])->group(function() {

});


Route::prefix('session/{tahun}/module-dashboard-dataset')->name('mod.dash.dataset')->middleware(['bindTahun'])->group(function() {
    Route::get('/show/{id}','DashboardDatasetController@index')->name('.index');
    // Route::get('/partials/{id}/{sub}','DashboardDatasetController@partials')->name('.partials');
    Route::get('/partials-data/{id}/','DashboardDatasetController@partials_data')->name('.partials_data');

});

