<?php

return [
    'dataset_form'=>[
        '138'=>[
            'component'=>'vi-pelatihan',
            'controller'=>'Modules\DatasetModul\Http\Controllers\Form\PelatihanController',
            'scope'=>'pemda',
            'methods'=>[
                'get'=>'getter',
                'update'=>'update',
                'delete'=>'delete',
            ]

            
        ]
    ],
    'dataset_bind_form_controller'=>[
        'pelatihan'=>[
            'controller'=>'Modules\DatasetModul\Http\Controllers\Form\PelatihanController',
            'scope'=>'pemda',
            'methods'=>[
                'get'=>'getter',
                'update'=>'update',
                'delete'=>'delete',
            ]
        ],
    ],
    'name' => 'DatasetModul',
    'datasets_cluster'=>[
        'RKPD'=>[
            'id'=>[101],
            'match'=>[],
            'focus'=>[]
        ],
        'PMPD'=>[
            'id'=>[119],
            'match'=>[],
            'focus'=>[]
        ],
        'APBD'=>[
            'id'=>[104],
            'match'=>[],
            'focus'=>[]
        ],
        'RKPDAPBD'=>[
            'id'=>[101,104],
            'match'=>[['total','pagu']],
            'focus'=>[]
        ],
        
    ],
    'filter_tematik'=>[
        'nomen_peningkatan'=>[
            "1.03.03.2.01.15",
            "1.03.03.2.01.03",
            "1.03.03.2.01.14",
            "1.03.03.2.01.05",
            "1.03.03.2.01.20",
            "1.03.03.2.01.07"
        ],
        'nomen_jp'=>['1.03.03.2.01.03','1.03.03.2.01.04','1.03.03.2.01.07','1.03.03.2.01.19','1.03.03.2.01.20','1.03.03.2.01.08','1.03.03.2.01.05','1.03.03.2.01.06'],

    ]
];
