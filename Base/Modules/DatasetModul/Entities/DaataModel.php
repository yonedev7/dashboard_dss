<?php

namespace Modules\DatasetModul\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class DaataModel extends Model
{
    use HasFactory;
    protected $table="master.dataset";
    
    protected $fillable = [
        'id','name','jenis_dataset','table'
    ];
    
    protected static function newFactory()
    {
        return \Modules\DatasetModul\Database\factories\DaataModelFactory::new();
    }

    
}
