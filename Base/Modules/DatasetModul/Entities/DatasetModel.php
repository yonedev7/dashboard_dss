<?php

namespace Modules\DatasetModul\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class DatasetModel extends Model
{
    use HasFactory;

    protected $fillable = [];
    
    protected static function newFactory()
    {
        return \Modules\DatasetModul\Database\factories\DatasetModelFactory::new();
    }
}
