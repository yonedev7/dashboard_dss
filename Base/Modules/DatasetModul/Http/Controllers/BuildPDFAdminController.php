<?php

namespace Modules\DatasetModul\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Auth;
use App\Http\Controllers\FilterCtrl;
use DB;
class BuildPDFAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index($tahun,Request $request)
    {   
        $permission_dts=FilterCtrl::getPemissionsDataset($request);
        $dataset=DB::table('master.dataset')->orderBy('index','asc')->orderBy('admin_menu','asc')
        ->whereIn('id',$permission_dts['dataset_id']??[1])
        ->get()->toArray();
        $dataset_utama=array_filter($dataset,function($el){
            return $el->admin_menu==0;
        });
        $dataset_pendukung=array_filter($dataset,function($el){
            return $el->admin_menu!=0;
        });

        return view('datasetmodul::pdf.admin.rekap')->with(['dataset_utama'=>array_values($dataset_utama),'dataset_pendukung'=>array_values($dataset_pendukung)]);
        
    }

    public function dataset($tahun,$id_dataset){

    }
   
}
