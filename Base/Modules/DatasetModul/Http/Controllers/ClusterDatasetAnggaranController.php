<?php

namespace Modules\DatasetModul\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
class ClusterDatasetAnggaranController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    static public $filter=[];
    
    public function getTemaPelatihan($tahun){
        return DB::table('master.master_pelatihan')->orderBy('tanggal_mulai','desc')->get();
    }

    public static function buildChartMulty($type,$title,$subtitle,$data){
        $chart=[
            'chart'=>[
                'type'=>$type,
                'height'=>400
            ],
            'title'=>[
                'text'=>$title
            ],
            'subtitle'=>[
                'text'=>$subtitle
            ],
            'plotOptions'=>[
                'series'=>[
                    'dataLabels'=>[
                            'enabled'=>true,
                    ],
                ],
                'pie'=>[
                    'dataLabels'=>[
                        'enabled'=>true,
                        'format'=>'<b>{point.name}</b>: {point.percentage:.1f} %'
                    ]
                ]
            ],
            'xAxis'=>[
                'type'=>'category',
            ],
            'yAxis'=>[

            ],
            'series'=>[

            ]
        ];

        $EXIST_=[];
        foreach($data as $k=>$d){
            $chart['series'][$k]=$d;
            if($chart['yAxis']){

            }
            $yAxis=array_map(function($el){
                return [$el['title']['text']];
            },$chart['yAxis']);

           
            foreach($yAxis as $kl=>$y){
                $yAxis[$kl][1]=$kl;
            }
            

            $EXIST_=array_merge($EXIST_,array_filter($yAxis,function($el) use ($d){
                return $el[0]==$d['satuan'];
            }));
            
            if(count($EXIST_)){
                $chart['series'][$k]['yAxis']=$EXIST_[0][1];
            }else{
                $chart['yAxis'][]=[
                    'title'=>[
                        'text'=>$d['satuan'],
                    ]
                ];
                $chart['series'][$k]['yAxis']=count($chart['yAxis'])-1;

            }
        }

        return $chart;
    }

    public function buildChartPie($title,$subtitle,$data){

    }



     public static function RKPD_PENINGKATAN($tahun,Request $request){
        $schema=config('datasetmodul.datasets_cluster.RKPD',[]);
        if(count($schema)){

        } 
     }

     public static function storeFilter(Request $request){
    
        static::$filter=[
            'status'=>['in',$request->filter_status??[]],
            'id'=>['is not ',$request->filter_entry??null],
            'regional_1'=>['in',$request->filter_regional??[]],
            'kodepemda'=>['in',$request->filter_kodepemdas??[]],
            'lokus'=>['=',$request->filter_lokus??'N'],

        ];

     }
     public static function getComponent($schema,$tahun){
        $enabled=true;
        if(count($schema)){
            $dataset=[];
            foreach($schema['id'] as $k=> $id){
                $dts=DB::table('master.dataset')->find($id);
                $ex=false;

                if($dts){
                    $ex=true;
                    $tb=$dts->table;
                    $tb_status='dts_'.$dts->id;

                    if(str_contains($tb,"[TAHUN]")){
                        $tb=str_replace('[TAHUN]',$tahun,$tb);
                        $tb_status.="_".$tahun;
                        $ex=(int)DB::table('pg_tables')->where('tablename','=',$tb)->where('schemaname','=','dataset')->count();

                    }
                }
                if($ex){
                    $exist=(int)DB::table('pg_tables')->where('tablename','=',$tb_status)->where('schemaname','=','status_data')->count();
                    if(($exist)){
                        $dataset[$k]=[
                            $dts->id,
                            $tb,
                            $tb_status,
                            $dts->name,
                            $dts->tujuan
                        ];
                    }

                }

                if(!isset($dataset[$k])){
                    $dataset[$k]=false;
                    $enabled=false;
                }
            }
        }

        if($enabled){
            return $dataset;
        }else{
            return false;
        }
     }

     public static function filterProccess($data,$tahun){
        $subtitle=[];
       if(static::$filter['status'][1]){
            if(count(static::$filter['status'][1])){
                $data=$data->whereIn('st.status',static::$filter['status'][1]);
                if(static::$filter['status'][1][0]==2){
                    $subtitle[]='Data Validasi';
                }
            }
       }

       if(static::$filter['lokus'][1]){
            if(static::$filter['lokus'][1]=='S'){

                $pemdas=DB::table('master.pemda_lokus')->where('tahun_proyek',$tahun)->select('kodepemda')->get()->pluck('kodepemda');
                $data=$data->whereIn('d.kodepemda',(array)$pemdas->toArray());
                $subtitle[]='Sortlist NUWSP';

            }else if(static::$filter['lokus'][1]=='L'){

                $pemdas=DB::table('master.pemda_lokus')->select('kodepemda')->get()->pluck('kodepemda');
                
                $data=$data->whereIn('d.kodepemda',(array)$pemdas->toArray() );

                $subtitle[]='Longlist NUWSP';



            }
       }

        if(static::$filter['id'][1]){
            $data=$data->where('d.id','!=',null);
        }

        if(static::$filter['regional_1'][1]){
            if(count(static::$filter['regional_1'][1])){
                $data=$data->whereIn('mp.regional_1',static::$filter['regional_1'][1]);
                $subtitle[]='Regional '.static::$filter['regional_1'][1][0];

            }
        }

        if(static::$filter['kodepemda'][1]){
            if(count(static::$filter['kodepemda'][1])){
                $data=$data->whereIn('mp.kodepemda',static::$filter['kodepemda'][1]);
            }
        }


        return [
            'subtitle'=>count($subtitle)?' ('.implode(',',$subtitle).') ':'',
            'data'=>$data
        ];

     }

     public function RA_PMPD($tahun,Request $request){
        $title='Pendanaan Permodalan BUMDAM (PMPD)';
        $subtitle='Tahun '.$tahun;
        $satuan='Rp.';
        $P='prefix';


        $schema=config('datasetmodul.datasets_cluster.PMPD',[]);
        $component=static::getComponent($schema,$tahun);
        $PG='sum(d.modal_pemda)';


        $data=DB::table('dataset.'.$component[0][1].' as d')
        ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
        ->leftJoin('status_data.'.$component[0][2].' as st',[['st.kodepemda','=','d.kodepemda'],['st.tahun','=','d.tahun']])
        ->leftJoin(DB::raw("(select x.kodepemda,string_agg(x.tipe_bantuan,',') as tipe_bantuan,string_agg(x.tahun_proyek::text,'@') from master.pemda_lokus as x group by x.kodepemda ) as lk"),'lk.kodepemda','=','d.kodepemda')
        ->selectRaw("count(distinct(mp.kodepemda)) as jumlah_pemda,count(distinct(mp.kodepemda)) filter (where length(mp.kodepemda)>3) as jumlah_pemda_kabkot,count(distinct(mp.kodepemda)) filter (where length(mp.kodepemda)<=3) as jumlah_pemda_prov,".$PG." as jumlah_pagu,".$PG." filter (where length(mp.kodepemda)>3) as jumlah_pagu_kabkot,".$PG." filter (where length(mp.kodepemda)<=3) as jumlah_pagu_prov")

        ->selectRaw('sum(modal_pemda) as val');

        $x=static::filterProccess($data,$tahun);

        $data=$x['data']->first();
        $subtitle=$x['subtitle'].$subtitle; 

        if($data){
            $data=(float)$data->jumlah_pagu;
        }

        return [
            'code'=>200,
            'data'=>[
               'val'=> (float)$data,
                'satuan'=>$satuan,
                'title'=>$title,
                'subtitle'=>$subtitle,
                'satuan_posisi'=>$P
            ]
        ];
     }

     public static function RA_TEMATIK_JP($tahun,Request $request){
        $title='Rencana Dan Anggaran Air Minum Tematik Jaringan Perpipaan';
        $subtitle='';
        $subtitle.='TAHUN '.$tahun;
        $schema=config('datasetmodul.datasets_cluster.RKPDAPBD',[]);
        $nomen=config('datasetmodul.filter_tematik.nomen_jp',[]);
        return static::MATCHRKPDAPBD($tahun,$schema,$nomen,$title,$subtitle,$request);
     }

     public static function RA_RKPD_JP($tahun,Request $request){
        $title='Rencana Anggaran Air Minum Tematik Jaringan Perpipaan';
        $subtitle='';
        $subtitle.='TAHUN '.$tahun;
        $schema=config('datasetmodul.datasets_cluster.RKPD',[]);
        $nomen=config('datasetmodul.filter_tematik.nomen_jp',[]);
        return static::GET_RKPD_OR_APBD($tahun,$schema,$nomen,$title,$subtitle,$request);
     }
     public static function RA_APBD_JP($tahun,Request $request){
        $title='Anggaran Air Minum Tematik Jaringan Perpipaan';

        $subtitle='';
        $subtitle.='TAHUN '.$tahun;
        $schema=config('datasetmodul.datasets_cluster.APBD',[]);
        $nomen=config('datasetmodul.filter_tematik.nomen_jp',[]);
        return static::GET_RKPD_OR_APBD($tahun,$schema,$nomen,$title,$subtitle,$request,'APBD');
     }

     public static function RA_RKPD_PENINGKATAN($tahun,Request $request){
        $title='Rencana dan Anggaran Kegiatan  Air Minum Tematik Peningkatan Jaringan Perpipaan';

        $subtitle='';
        $subtitle.='TAHUN '.$tahun;
        $schema=config('datasetmodul.datasets_cluster.RKPD',[]);
        $nomen=config('datasetmodul.filter_tematik.nomen_peningkatan',[]);
        return static::GET_RKPD_OR_APBD($tahun,$schema,$nomen,$title,$subtitle,$request);
     }
     public static function RA_APBD_PENINGKATAN($tahun,Request $request){
        $title='Anggaran Kegiatan Air Minum Tematik Peningkatan SPAM Jaringan Perpipaan';

        $subtitle='';
        $subtitle.='TAHUN '.$tahun;
        $schema=config('datasetmodul.datasets_cluster.APBD',[]);
        $nomen=config('datasetmodul.filter_tematik.nomen_peningkatan',[]);
        return static::GET_RKPD_OR_APBD($tahun,$schema,$nomen,$title,$subtitle,$request,'APBD');
     }

     public static function RA_TEMATIK_PENINGKATAN($tahun,Request $request){
        $title=$request->title??'Data RKPD Tematik Jaringan Perpipaan';
        $title='Rencana Kegiatan Tematik Peningkatan  Air Minum';

        $subtitle='';
        $subtitle.='TAHUN '.$tahun;
        $schema=config('datasetmodul.datasets_cluster.RKPDAPBD',[]);
        $nomen=config('datasetmodul.filter_tematik.nomen_peningkatan',[]);
        return static::MATCHRKPDAPBD($tahun,$schema,$nomen,$title,$subtitle,$request);
     }


     public static function GET_RKPD_OR_APBD($tahun,$schema,$nomen,$title,$subtitle,Request $request,$cx=null,$satuan=''){

        if($cx=='APBD'){
            $PG='SUM(d.totalharga)';
            $series_name='Anggaran';

        }else{
            $PG='SUM(d.pagu)';
            $series_name='Pagu';
        }


        
        $component=static::getComponent($schema,$tahun);
        if($component){
            $data=DB::table('dataset.'.$component[0][1].' as d')
            ->leftJoin('master.master_nomen_90 as nm','nm.kode','=','d.kodesubkegiatan')
            ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
            ->leftJoin('status_data.'.$component[0][2].' as st',[['st.kodepemda','=','d.kodepemda'],['st.tahun','=','d.tahun']])
            ->leftJoin(DB::raw("(select x.kodepemda,string_agg(x.tipe_bantuan,',') as tipe_bantuan,string_agg(x.tahun_proyek::text,'@') from master.pemda_lokus as x group by x.kodepemda ) as lk"),'lk.kodepemda','=','d.kodepemda')
            ->selectRaw("count(distinct(mp.kodepemda)) as jumlah_pemda,count(distinct(mp.kodepemda)) filter (where length(mp.kodepemda)>3) as jumlah_pemda_kabkot,count(distinct(mp.kodepemda)) filter (where length(mp.kodepemda)<=3) as jumlah_pemda_prov,".$PG." as jumlah_pagu,".$PG." filter (where length(mp.kodepemda)>3) as jumlah_pagu_kabkot,".$PG." filter (where length(mp.kodepemda)<=3) as jumlah_pagu_prov")
            ->whereIn('d.kodesubkegiatan',$nomen)->orderBy(DB::raw($PG),'desc');
            $x=static::filterProccess($data,$tahun);
            $data=$x['data'];
            $subtitle=$x['subtitle'].$subtitle;


            
          
            if($request->sub_kegiatan){
                $data=$data->whereIn('d.kodesub_kegiatan',$request->sub_kegiatan);

            }
            $varian_display=[];
            switch($request->method){
                case 'per_sub':
                    $data= $data->groupBy('d.kodesubkegiatan');
                    $data= $data->selectRaw('max(nm.name) as name,d.kodesubkegiatan as kode');
                    $data=$data->get();

                 
                    $varian_display=['column','line','pie','area','table'];

                break;
                default:
                    $data=$data->first();
                    $data2=$data2->first();

                break;
            }




            if(in_array($request->type_chart??'x',$varian_display)){
                $display=$request->type_chart;
            }else{
                if(isset($varian_display[0])){
                    $display=$varian_display[0];
                }else{
                    return false;
                }
            }

           
            $sum=0;

        
           if(in_array($display,['column','line','area','pie'])){
            $data_series=[];
            foreach($data as $d){
                $d=(array)$d;
                if(!isset($data_series[0])){
                    $data_series[0]=[
                        'name'=>'Nilai '.$series_name,
                        'data'=>[],
                      
                        'satuan'=>'Rupiah',
                        'tooltip'=>[
                            'pointFormat'=>'Rp. {point.y} ({point.custome.jumlah_pemda} Pemda) <br>Pemda Kab/Kota : Rp. {point.custome.y_kabkot:,2f} ({point.custome.jumlah_pemda_kabkot} Pemda)<br>Pemda Provinsi : Rp. {point.custome.y_prov:,2f} ({point.custome.jumlah_pemda_provinsi} Pemda)
                           
                            '

                        ]
                    ];
                }
                $sum+=(float)$d['jumlah_pagu'];
                $data_series[0]['data'][]=[
                    'name'=>$d['name'].' ('.$d['kode'].')',
                    'kode'=>$d['kode'],
                    'y'=>(float)$d['jumlah_pagu'],

                    'custome'=>[
                        'jumlah_pemda'=>(float)$d['jumlah_pemda'],
                        'jumlah_pemda_kabkot'=>(float)$d['jumlah_pemda_kabkot'],
                        'jumlah_pemda_provinsi'=>(float)$d['jumlah_pemda_prov'],
                        'y_kabkot'=>(float)$d['jumlah_pagu_kabkot'],
                        'y_prov'=>(float)$d['jumlah_pagu_prov'],
                    ]
                ];
            }

            if($request->type_chart=='pie'){
                $subtitle='Rp. '.number_format($sum,0,'.',',').' '.$subtitle;
            }
       
            $chart=static::buildChartMulty($request->type_chart??'column',$title,$subtitle,$data_series);
            $dts=array_map(function($el){
                return [
                    'id'=>$el[0],
                    'name'=>$el[3],
                    'tujuan'=>$el[4],
                ];
            },$component);
            return [
                'chart_varian'=>$varian_display,
                'dataset'=>$dts,
                'chart'=>$chart,
                'payload'=>$request->all()

            ];
           }
   

            
        }else{
            return false;
        }
     }

     public static function MATCHRKPDAPBD($tahun,$schema,$nomen,$title,$subtitle,Request $request){
        $component=static::getComponent($schema,$tahun);
        if($component){
            $data=DB::table('dataset.'.$component[0][1].' as d')
            ->leftJoin('master.master_nomen_90 as nm','nm.kode','=','d.kodesubkegiatan')
            ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
            ->leftJoin('status_data.'.$component[0][2].' as st',[['st.kodepemda','=','d.kodepemda'],['st.tahun','=','d.tahun']])
            ->leftJoin(DB::raw("(select x.kodepemda,string_agg(x.tipe_bantuan,',') as tipe_bantuan,string_agg(x.tahun_proyek::text,'@') from master.pemda_lokus as x group by x.kodepemda ) as lk"),'lk.kodepemda','=','d.kodepemda')
            ->selectRaw("count(distinct(mp.kodepemda)) as jumlah_pemda,count(distinct(mp.kodepemda)) filter (where length(mp.kodepemda)>3) as jumlah_pemda_kabkot,count(distinct(mp.kodepemda)) filter (where length(mp.kodepemda)<=3) as jumlah_pemda_prov,sum(d.pagu) as jumlah_pagu,sum(d.pagu) filter (where length(mp.kodepemda)>3) as jumlah_pagu_kabkot,sum(d.pagu) filter (where length(mp.kodepemda)<=3) as jumlah_pagu_prov")
            ->whereIn('d.kodesubkegiatan',$nomen)->orderBy(DB::raw('sum(d.pagu)'),'desc');
            $x=static::filterProccess($data,$tahun);
            $data=$x['data'];


            $data2=DB::table('dataset.'.$component[1][1].' as d')
            ->leftJoin('master.master_nomen_90 as nm','nm.kode','=','d.kodesubkegiatan')
            ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
            ->leftJoin('status_data.'.$component[1][2].' as st',[['st.kodepemda','=','d.kodepemda'],['st.tahun','=','d.tahun']])
            ->leftJoin(DB::raw("(select x.kodepemda,string_agg(x.tipe_bantuan,',') as tipe_bantuan,string_agg(x.tahun_proyek::text,'@') from master.pemda_lokus as x group by x.kodepemda ) as lk"),'lk.kodepemda','=','d.kodepemda')
            ->selectRaw("count(distinct(mp.kodepemda)) as jumlah_pemda,count(distinct(mp.kodepemda)) filter (where length(mp.kodepemda)>3) as jumlah_pemda_kabkot,count(distinct(mp.kodepemda)) filter (where length(mp.kodepemda)<=3) as jumlah_pemda_prov,sum(d.totalharga) as jumlah_pagu,sum(d.totalharga) filter (where length(mp.kodepemda)>3) as jumlah_pagu_kabkot,sum(d.totalharga) filter (where length(mp.kodepemda)<=3) as jumlah_pagu_prov")
            ->whereIn('d.kodesubkegiatan',$nomen);

            $x=static::filterProccess($data2,$tahun);
            $data2=$x['data'];

            $subtitle=$x['subtitle'].$subtitle;

            

          
            if($request->sub_kegiatan){
                $data=$data->whereIn('d.kodesub_kegiatan',$request->sub_kegiatan);
                $data2=$data2->whereIn('d.kodesub_kegiatan',$request->sub_kegiatan);

            }
            $varian_display=[];
            switch($request->method){
                case 'per_sub':
                    $data= $data->groupBy('d.kodesubkegiatan');
                    $data= $data->selectRaw('max(nm.name) as name,d.kodesubkegiatan as kode');
                    $data=$data->get();

                    $data2=$data2->groupBy('d.kodesubkegiatan');
                    $data2=$data2->selectRaw('max(nm.name) as name,d.kodesubkegiatan as kode');
                    $data2= $data2->get();
                    $varian_display=['column','line','pie','area','table'];

                break;
                default:
                    $data=$data->first();
                    $data2=$data2->first();

                break;
            }




            if(in_array($request->type_chart??'x',$varian_display)){
                $display=$request->type_chart;
            }else{
                if(isset($varian_display[0])){
                    $display=$varian_display[0];
                }else{
                    return false;
                }
            }

        
           if(in_array($display,['column','line','area','pie'])){
            $data_series=[];
            foreach($data as $d){
                $d=(array)$d;
                if(!isset($data_series[0])){
                    $data_series[0]=[
                        'name'=>'Nilai Pagu',
                        'data'=>[],
                      
                        'satuan'=>'Rupiah',
                        'tooltip'=>[
                            'pointFormat'=>'Rp. {point.y} ({point.custome.jumlah_pemda} Pemda) <br>Pemda Kab/Kota : Rp. {point.custome.y_kabkot:,2f} ({point.custome.jumlah_pemda_kabkot} Pemda)<br>Pemda Provinsi : Rp. {point.custome.y_prov:,2f} ({point.custome.jumlah_pemda_provinsi} Pemda)
                           
                            '

                        ]
                    ];
                }
                $data_series[0]['data'][]=[
                    'name'=>$d['name'].' ('.$d['kode'].')',
                    'kode'=>$d['kode'],
                    'y'=>(float)$d['jumlah_pagu'],

                    'custome'=>[
                        'jumlah_pemda'=>(float)$d['jumlah_pemda'],
                        'jumlah_pemda_kabkot'=>(float)$d['jumlah_pemda_kabkot'],
                        'jumlah_pemda_provinsi'=>(float)$d['jumlah_pemda_prov'],
                        'y_kabkot'=>(float)$d['jumlah_pagu_kabkot'],
                        'y_prov'=>(float)$d['jumlah_pagu_prov'],
                    ]
                ];
            }
            foreach($data2 as $d){
                $d=(array)$d;
                if(!isset($data_series[1])){
                    $data_series[1]=[
                        'name'=>'Nilai Anggaran',
                        'data'=>[],
                       
                        'satuan'=>'Rupiah',
                        'tooltip'=>[
                            'pointFormat'=>'Rp. {point.y} ({point.custome.jumlah_pemda} Pemda) <br>Pemda Kab/Kota : Rp. {point.custome.y_kabkot:,2f} ({point.custome.jumlah_pemda_kabkot} Pemda)<br>Pemda Provinsi : Rp. {point.custome.y_prov:,2f} ({point.custome.jumlah_pemda_provinsi} Pemda)
                           
                            '

                        ]
                    ];
                }
                $data_series[1]['data'][]=[
                    'name'=>$d['name'].' ('.$d['kode'].')',
                    'kode'=>$d['kode'],

                    'y'=>(float)$d['jumlah_pagu'],
                    'custome'=>[
                        'jumlah_pemda'=>(float)$d['jumlah_pemda'],
                        'jumlah_pemda_kabkot'=>(float)$d['jumlah_pemda_kabkot'],
                        'jumlah_pemda_provinsi'=>(float)$d['jumlah_pemda_prov'],
                        'y_kabkot'=>(float)$d['jumlah_pagu_kabkot'],
                        'y_prov'=>(float)$d['jumlah_pagu_prov'],
                    ]
                ];
            }
            $chart=static::buildChartMulty($request->type_chart??'column',$title,$subtitle,$data_series);
            $dts=array_map(function($el){
                return [
                    'id'=>$el[0],
                    'name'=>$el[3],
                    'tujuan'=>$el[4],
                ];
            },$component);
            return [
                'chart_varian'=>$varian_display,
                'dataset'=>$dts,
                'chart'=>$chart,
                'payload'=>$request->all()

            ];
           }
   

            
        }else{
            return false;
        }
     }

     public function get($tahun,$contex,Request $request){
        static::storeFilter($request);
        return eval(' return $this->'.$contex.'($tahun,$request);');
        
        try{
            static::storeFilter($request);
            return eval('$this->'.$contex.'($tahun,$request);');


        }catch(\Exception $e){
            return [
                'code'=>500,
                'data'=>[],
                'error'=>$e
            ];
        }
     }
   

}
