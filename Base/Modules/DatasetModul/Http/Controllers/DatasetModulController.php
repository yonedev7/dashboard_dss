<?php

namespace Modules\DatasetModul\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use Auth;
use Alert;
use Carbon\Carbon;
use Storage;
use App\Http\Controllers\FilterCtrl;

class DatasetModulController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

     public function print($tahun,$id_dataset,$kodepemda=null,Request $request){
        return view('datasetmodul::form.pdf_data');
     }

     

     public function upload_file($tahun,$id_dataset,$tw,Request $request ){
        $re=[];
        foreach($request->name??[] as $key=>$d){
            if(isset($request->file[$key])){
               $pt= Storage::put('public/dataset/'.$id_dataset.'/'.$tw,$request->file[$key]);
               $pt=Storage::url($pt);
               $re[]=[
                   'name'=>$d,
                   'path'=>$pt
               ];
            }
        }

        return $re;
     }
    public function index($tahun,Request $request)
    {
        $MyUser=Auth::User();
        $fil=$request->meta?true:false;
        $page_meta=[
            'title'=>'Dataset '.($fil?'NASIONAL':'PEMDA'),
            'keterangan'=>'',
            'nasional'=>$fil
        ];

        $scope_bantuan=json_decode($dataset->klasifikasi_2??'[]',true);
        if(count($scope_bantuan)){
            $scope_bantuan=array_map(function($el){
                return str_replace('BANTUAN ','',strtoupper($el));
            },$scope_bantuan);
            $lokus=true;
        }else{
            $scope_bantuan=$request->filter_tipe_bantuan;
            $lokus=false;
        }

        $regional=$request->filter_regional;
        if($MyUser->roleCheck('TACT REGIONAL')){
            $lokus=true;
            $regional=$MyUser->regional;
        }

        $filter=['regional'=>$regional,'sortlist'=>$request->filter_sortlist,'lokus'=>$lokus,'tipe_bantuan'=>$scope_bantuan];
        $pemdas=FilterCtrl::filter_pemda($filter['regional'],$filter['lokus'],$filter['sortlist'],$filter['tipe_bantuan']);

       $data= DB::table('master.dataset as dt')
        ->where('pusat',$fil)
        ->get();

        foreach($data as $key=>$d){
            if(str_contains($d->table,'[TAHUN]')){
                $d->table=str_replace('[TAHUN]',$tahun,$d->table);
            }
            $rekap=[
                'terverifikasi'=>0,
                'belum_terverifikasi'=>0
            ];

            $exist=DB::table('information_schema.tables')->where('table_schema','dataset')
            ->where('table_name',$d->table)
            ->first();
            if($exist){
                if($d->jenis_dataset){
                    // multy row
                    $rekapx=DB::table('dataset.'.$d->table.' as d')
                    ->selectRaw("d.kodepemda,count(distinct(case when d.status=1 then d.kodepemda else null end)) as terverifikasi,count(distinct(case when d.status=1 then null else d.kodepemda  end)) as belum_terverifikasi")
                    ->whereIn('d.kodepemda',$pemdas)
                    ->groupBy('d.kodepemda')
                    ->where('d.tw',4)
                    ->where('d.tahun',$tahun)

                    ->get();

                   

                    foreach($rekapx as $k=>$dd){
                        if($dd->belum_terverifikasi){
                            $rekap['belum_terverifikasi']+=1;
                        }else{
                            $rekap['terverifikasi']+=1;
                        }
                    }
                    
                
                }else{
                    // single row
                   if(strtoupper($d->klasifikasi_1)=='Data BUMDAM'){
                    $rekapx=DB::table('dataset.'.$d->table.' as d')
                    ->selectRaw("d.kodepemda,count(distinct(case when d.status=1 then d.kodepemda else null end)) as terverifikasi,count(distinct(case when d.status=1 then null else d.kodepemda  end)) as belum_terverifikasi")
                    ->whereIn('d.kodepemda',$pemdas)
                    ->groupBy('d.kodepemda')
                    ->where('d.tw',4)
                    ->where('d.tahun',$tahun)


                    ->get();

                   
                    foreach($rekapx as $k=>$dd){
                        if($dd->belum_terverifikasi){
                            $rekap['belum_terverifikasi']+=1;
                        }else{
                            $rekap['terverifikasi']+=1;
                        }
                    }
                   }else{
                    $rekap=DB::table('dataset.'.$d->table.' as d')
                    ->selectRaw("count(distinct(case when d.status=1 then d.kodepemda else null end)) as terverifikasi,count(distinct(case when d.status=1 then null else d.kodepemda  end)) as belum_terverifikasi")
                    ->whereIn('d.kodepemda',$pemdas)
                    ->where('d.tw',4)
                    ->where('d.tahun',$tahun)

                    ->first();
                   }
                }

                $data[$key]->rekap=(Array) $rekap;
            }else{
                $data[$key]->rekap=(Array) $rekap;
                
            }

        }


        return view('datasetmodul::index')->with(['basis'=>\_g_basis($request),'page_meta'=>$page_meta,'req'=>$request->all(),'data'=>$data]);
    }

    public function ajax_kodepemda($tahun,$kodepemda,Request $request){
      if($request->like){
        $data= DB::table('master.master_pemda')->where('kodepemda','!=','0')
        ->selectRaw("kodepemda,nama_pemda,regional_1,regional_2,tahun_proyek,tipe_bantuan")->get();
      }else{
        $data= DB::table('master.master_pemda as mp')->where('kodepemda',$kodepemda);
        $where=_filter_l1($tahun,'provinsi & kab.kota',$request->basis);
        unset($where[0]);
        $data=$data->where($where);
        $data=$data->selectRaw("kodepemda,nama_pemda,regional_1,regional_2,tahun_proyek,tipe_bantuan")->first();

        if(!$data){
            $data=[
                'kodepemda'=>null,
                'nama_pemda'=>null,
                'regional_1'=>null,
                'regional_2'=>null,
                'tahun_proyek'=>null,
                'tipe_bantuan'=>null,
            ];
        }
      }
       return $data;
    }

    static function get_data($multi,$basis,$filter,$table,$tahun,$tw,$status){
        
        if($multi){
           if(count($status)>=2){
            $data=DB::table('master.master_pemda as mp')
            ->join($table.' as d','mp.kodepemda','=','d.kodepemda')
            ->selectRaw('count(distinct(mp.kodepemda)) as jumlah_pemda');
            $where=\_filter_l1($tahun,$filter,$basis,[['d.tw','=',$tw]]);
            if(count($where)){
                $data=$data->where($where);
            }
            $ret= $data->whereIn('d.status',$status)->first();
            return $ret?$ret->jumlah_pemda:0;
           }else{

            $data=DB::table('master.master_pemda as mp')
            ->join($table.' as d','mp.kodepemda','=','d.kodepemda')
            ->selectRaw('mp.kodepemda as kodepemda,count(d.id) jumlah_data,count(case when d.status=1 then d.id else null end) jumlah_data_ver')
            ->groupBy('mp.kodepemda');
            $where=\_filter_l1($tahun,$filter,$basis,[['d.tw','=',$tw]]);
            if(count($where)){
                $data=$data->where($where);
            }
            $data=$data->whereIn('d.status',$status);
            $ret=DB::table(($data),'xdata')->whereRaw('jumlah_data=jumlah_data_ver')->selectRaw('count(xdata.kodepemda) as jumlah_pemda')->first();
            return $ret?$ret->jumlah_pemda:0;

           }

        }else{
            $data=DB::table($table.' as d')
            ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
            ->selectRaw('count(distinct(mp.kodepemda)) as jumlah_pemda');
            $where=\_filter_l1($tahun,$filter,$basis,[['d.tw','=',$tw]]);
            if(count($where)){
                $data=$data->where($where);
            }
            $ret= $data->whereIn('d.status',$status)->first();
            return $ret?$ret->jumlah_pemda:0;
    
        }

    }

    public static function list_pemda_multy($tahun,$dataset,$tw,$request,$monev=false){
        $fil=$request->meta?true:false;
        $page_meta=[
            'title'=>$dataset->name,
            'keterangan'=>$dataset->tujuan,
            'tw'=>$tw,
            'id'=>$dataset->id,
            'tahun'=>$tahun
        ];
        if($monev){
            $page_meta['monev']=$tahun;
           if($request->basis){
               if(!in_array(strtoupper($request->basis),['LONGLIST','SORTLIST'])){
                   $request['basis']='LONGLIST';
               }
           }
        }
        return  view('datasetmodul::index_multy')->with(['basis'=>\_g_basis($request),'page_meta'=>$page_meta]);
    }

    public static function ajax_load_multy($tahun,$id_dataset,$tw,Request $request){
        $MyUser=Auth::User();
        $dataset=DB::table('master.dataset')->find($id_dataset);
        if(!$dataset){
            return [
                'items'=>[],
                'current_page'=>1,
                'last_page'=>1,
            ];
        }

        $table=$dataset->table;
        $table=str_replace('[TAHUN]',$tahun,$table);
        $exist_table=(int)DB::table('pg_tables')->where('tablename','=',$table)->where('schemaname','=','dataset')->count();
        if(!$exist_table){
            return [
                'items'=>[],
                'current_page'=>1,
                'last_page'=>1,
            ];
        }


        $data=DB::table('master.master_pemda as mp')
        ->join('dataset.'.$table.' as d','d.kodepemda','=','mp.kodepemda')
        ->groupBy('mp.kodepemda')
        ->selectRaw("mp.kodepemda,
            max(mp.nama_pemda) as name,
            max(mp.tahun_proyek) as tahun_proyek,
            max(mp.tipe_bantuan) as tipe_bantuan,
            max(mp.regional_1) as regional_1,
            max(mp.regional_2) as regional_2,
            count(case when d.status=1 then d.id else null end) as terverifikasi,
            count(d.*) as terdata
        ")->where('tahun',$tahun)
        ->where('tw',$tw);
      
        

        return $data->paginate(200);



    }

    public static function detail($tahun,$id_dataset,$tw,$kodepemda=null,Request $request){
        $MyUser=Auth::User();

        $dataset=DB::table('master.dataset')->find($id_dataset);
        $request['basis']=\_g_basis($request);
        $monev=false;
        if($request->monev){
             $monev=true;
             

             if($request->basis){
                if(!in_array(strtoupper($request->basis),['LONGLIST','SORTLIST'])){
                    $request['basis']='LONGLIST';
                }
            }

            if($id_dataset==95){
                return redirect()->route('mod.monev-admin.form.ddub',['tahun'=>$tahun,'id_dataset'=>$id_dataset,'tw'=>$tw,'basis'=>$request->basis]);
            }
        }
        
        if($dataset){
            $page_meta=[
                'title'=>$dataset->name??'',
                'keterangan'=>$dataset->tujuan,
                'tw'=>$tw,
                'id'=>$dataset->id,
                'tahun'=>$tahun
            ];

            if($monev){
                $page_meta['monev']=$tahun;
            }

            if(str_contains($dataset->table,'[TAHUN]')){
                $dataset->table=str_replace('[TAHUN]',$tahun,$dataset->table);
            }
           
            $exist_table=DB::table('information_schema.tables')->where('table_schema','dataset')
            ->where('table_name',$dataset->table)
            ->first();


            if($dataset->jenis_dataset){

                if($kodepemda==null){
                    if($dataset->pusat){
                            return redirect()->route($request->route()->getName(),['tahun'=>$tahun,'id_dataset'=>$id_dataset,'tw'=>4,'kodepemda'=>0]);
                    }

                   return static::list_pemda_multy($tahun,$dataset,$tw,$request,$monev);
                }else{
                    $pemda=DB::table('master_pemda')->where('kodepemda',$kodepemda)->first();
                    if($pemda){
                        $page_meta['pemda']=(array)$pemda;
                        $page_meta['title'].=' '.$pemda->nama_pemda;
                    }else{
                        Alert::info('','Kode Pemda Tidak tersedia');
                        return abort('404');
                    }
                    
                }

            }

         
            $col=DB::table('master.dataset_data as d')
            ->join('master.data as md','md.id','=','d.id_data')
            ->join('master.validator_data as v','v.id','=','md.id_validator')
            ->where('id_dataset',$dataset->id)
            ->selectRaw("
                d.index,
                d.field_name,
                v.name as validator,
                md.*
            ")
            ->orderBy('d.index','asc')
            ->get();

            if($dataset->pusat){
                $columns=[
                    [
                        'key'=>'status',
                        'label'=>'Verifikasi',
                        'editable'=>true,
                        'tipe_nilai'=>'numeric',
                        'tipe_nilai'=>'string',
                        'type'=>'options',
                        'options'=>[
                          [
                            'tag'=>'Terverifikasi',
                            'value'=>1
                          ],
                          [
                            'tag'=>'Belum Terverifikasi',
                            'value'=>0
                          ]
                        ],
                        'bind_view'=>[
                            [
                              'logic'=>'#val == 0',
                              'tag'=>'Belum Teverifikasi'
                            ],
                            [
                                'logic'=>'#val == 1',
                                'tag'=>'Teverifikasi'
                                ],
                          ]
    
    
                    ],
                ];
            }else{
                $columns=[
                    [
                        'key'=>'status',
                        'label'=>'Verifikasi',
                        'editable'=>true,
                        'tipe_nilai'=>'numeric',
                        'tipe_nilai'=>'string',
                        'type'=>'options',
                        'options'=>[
                          [
                            'tag'=>'Terverifikasi',
                            'value'=>1
                          ],
                          [
                            'tag'=>'Belum Terverifikasi',
                            'value'=>0
                          ]
                        ],
                        'bind_view'=>[
                            [
                              'logic'=>'#val == 0',
                              'tag'=>'Belum Teverifikasi'
                            ],
                            [
                                'logic'=>'#val == 1',
                                'tag'=>'Teverifikasi'
                                ],
                          ]
    
    
                    ],
                    [
                        'key'=>'regional_1',
                        'label'=>'Regional',
                        'editable'=>false,
                        'tipe_nilai'=>'string',
                        'type'=>'input-value',
                        'bind_view'=>[],
                    ],
                  
                    [
                        'key'=>'regional_2',
                        'label'=>'Wilayah',
                        'editable'=>false,
                        'tipe_nilai'=>'string',
                        'type'=>'input-value',
                        'bind_view'=>[],
                    ],
                    
                    [
                        'key'=>'tipe_bantuan',
                        'label'=>'Tipe Bantuan',
                        'editable'=>false,
                        'tipe_nilai'=>'string',
                        'type'=>'input-value',
                        'bind_view'=>[],

                    ],
                    // [
                    //     'key'=>'tahun_proyek',
                    //     'label'=>'Tahun Proyek',
                    //     'editable'=>false,
                    //     'tipe_nilai'=>'string',
                    //     'type'=>'input-value',
                    //     'bind_view'=>[
                    //         [
                    //           'logic'=>'#val == 1',
                    //           'tag'=>'LONGLIST'
                    //         ],
                    //         [
                    //             'logic'=>'#val > 200',
                    //             'tag'=>'[val]'
                    //             ],
                    //             [
                    //             'logic'=>'(#val == 0) || (#val==null)',
                    //             'tag'=>''
                    //         ],
                    //       ]
    
    
                    // ],
                    [
                        'key'=>'kodepemda',
                        'label'=>'Kodepemda',
                        'editable'=>$kodepemda!=null?false:true,
                        'tipe_nilai'=>'string',
                        'type'=>'input-value',
                        'bind_view'=>[],
    
    
    
                    ],
                    [
                        'key'=>'name',
                        'label'=>'Nama Pemda',
                        'editable'=>false,
                        'tipe_nilai'=>'string',
                        'type'=>'input-value',
                        'bind_view'=>[],
    
                    ],
                    
                    
                ];

                if($kodepemda){
                    $columns=[];
                }

            }

            if(strtoupper($dataset->klasifikasi_1)!='DATA PEMDA'){
                $columns[]=[
                    'key'=>'kodebu',
                    'label'=>'Kode BUMDAM',
                    'editable'=>true,
                    'tipe_nilai'=>'numeric',
                    'type'=>'input-value',
                    'bind_view'=>[],

                ];
               
                $columns[]=[
                    'key'=>'nama_bu',
                    'label'=>'Nama BUMDAM',
                    'editable'=>false,
                    'tipe_nilai'=>'string',
                    'type'=>'input-value',
                    'bind_view'=>[],
                ]; 
                $columns[4]['editable']=false;
            }

            foreach($col as $k=>$c){
                $columns[]=[
                    'id'=>$c->id,
                    'key'=>$c->field_name,
                    'label'=>$c->name,
                    'editable'=>true,
                    'tipe_nilai'=>$c->tipe_nilai,
                    'type'=>$c->type,
                    'definsisi_konsep'=>$c->definisi_konsep,
                    'classification'=>$c->classification,
                    'produsent_data'=>$c->produsent_data,
                    'validator'=>$c->validator,
                    'satuan'=>$c->satuan,
                    'file_accept'=>json_decode($c->file_accept),
                    'options'=>json_decode($c->options),
                    'bind_view'=>json_decode($c->bind_view),
                ];
            }

            $columns[]=[
                'key'=>'keterangan',
                'label'=>'Catatan',
                'editable'=>true,
                'tipe_nilai'=>'string',
                'type'=>'input-value',
                'bind_view'=>[],
            ];

            $scope_bantuan=json_decode($dataset->klasifikasi_2??'[]',true);
            if(count($scope_bantuan)){
                $scope_bantuan=array_map(function($el){
                    return str_replace('BANTUAN ','',strtoupper($el));
                },$scope_bantuan);
                $lokus=true;
            }else{
                $scope_bantuan=$request->filter_tipe_bantuan;
                $lokus=false;
            }

            $regional=$request->filter_regional;
            if($MyUser->roleCheck('TACT REGIONAL')){
                $lokus=true;
                $regional=$MyUser->regional;
            }


            $filter=['regional'=>$regional,'sortlist'=>$request->filter_sortlist,'lokus'=>$lokus,'tipe_bantuan'=>$scope_bantuan];
            $pemdas=FilterCtrl::filter_pemda($filter['regional'],$filter['lokus'],$filter['sortlist'],$filter['tipe_bantuan'],3);

            if(strtoupper($dataset->klasifikasi_1)=='DATA PEMDA'){
                $pemda_list=DB::table('master.master_pemda as pd')
                ->leftJoin('master.pemda_lokus as lk','lk.kodepemda','=','pd.kodepemda')
                ->selectRaw("pd.kodepemda,max(pd.nama_pemda) as name,
                max(pd.regional_1) as regional_1,
                max(pd.regional_2) as regional_2,
                ( string_agg(distinct(concat(lk.tipe_bantuan,' - ',lk.tahun_bantuan)),'@')) as tipe_bantuan
                ")
                ->orderBy('pd.kodepemda','ASC')
                ->whereIn('pd.kodepemda',$pemdas)
                ->groupBy('pd.kodepemda')->get()->toArray();
                $page_meta['pemda_list']=$pemda_list;

            }else{
                $pemda_list=DB::table('master.master_pemda as pd')
                ->leftJoin('master.pemda_lokus as lk','lk.kodepemda','=','pd.kodepemda')
                ->join('master.master_bu_pemda as bub','bub.kodepemda','=','pd.kodepemda')
                ->leftJoin('master.master_bu as bu','bu.id','=','bub.id_bu')
                ->selectRaw("
                max(bub.kodepemda) as kodepemda,
                max(pd.nama_pemda) as nama_pemda,
                max(bub.id_bu) as kodebu,
                concat(max(bub.id_bu),'@',max(bub.kodepemda)) as kodebu_integrate,
                max(bu.nama_bu) as nama_bu,
                max(pd.regional_1) as regional_1,
                max(pd.regional_2) as regional_2,
               concat( max(pd.nama_pemda),' - ',max(bu.nama_bu)) as name,
                ( string_agg(distinct(concat(lk.tipe_bantuan,' - ',lk.tahun_bantuan)),'@')) as tipe_bantuan
                ")
                ->orderBy(DB::raw('max(bub.kodepemda)'),'ASC')
                ->whereIn('pd.kodepemda',$pemdas)
                ->groupBy('bub.id')->get()->toArray();
                $page_meta['pemda_list']=$pemda_list;

            }
            // $pemda_list=DB::table('master.master_pemda as mp')
            
            // // ->
            // ->selectRaw('kodepemda, nama_pemda as name,tahun_proyek,tipe_bantuan,regional_1,regional_2')->orderBy('kodepemda');
            // $where=_filter_l1($tahun,'provinsi kab/kota',$request->basis,[['mp.kodepemda','!=','0']]);
            // unset($where[0]);
            // $pemda_list=$pemda_list->where($where)->get();

            

            return view('datasetmodul::listing')->with(['dataset'=>$dataset,'exist_table'=>$exist_table?true:false,'basis'=>$request->basis,'page_meta'=>$page_meta,'columns'=>$columns]);

        }
    

    }



    public function load($tahun,Request $request){
        $fil=$request->meta?true:false;
        $paginate=2;
        $monev=false;
        $data=DB::table('master.dataset')->orderBy('name','asc');
        if($request->monev){
            $monev=true;
        $data=$data->whereIn('id',[95,99,96,97]);

        }else{
            $data=$data->where('pusat',$fil??false);
        }
        $data=$data->paginate($paginate);
        $datanew=[];
        $datadelete=[];

        
        foreach($data as $k=>$d){
            $name_table='dataset.'.str_replace('[tahun]',$tahun,$d->table);
            $tb=explode('.',$name_table);
            $exist_table=false;
            if(count($tb)>1){
                $exist_table=(int)DB::table('pg_tables')->where('tablename','=',$tb[1])->where('schemaname','=',$tb[0])->count();
            }
          
            if($exist_table){
                $datanew['k'.$k]=$d;
                $datanew['k'.$k]->keterisian=[
                    'tw_1'=>[
                        'terdata'=>static::get_data($d->jenis_dataset,$request->basis,$request->filter,$name_table,$tahun,1,[0,1]),
                        'terverifikasi'=>static::get_data($d->jenis_dataset,$request->basis,$request->filter,$name_table,$tahun,1,[1]),
                        'link'=>route('mod.dataset.detail',['tahun'=>$tahun,'id_dataset'=>$d->id,'tw'=>1,'kodepemda'=>$fil?0:null,'monev'=>$monev?$tahun:null])
                    ],
                    'tw_2'=>[
                        'terdata'=>static::get_data($d->jenis_dataset,$request->basis,$request->filter,$name_table,$tahun,2,[0,1]),
                        'terverifikasi'=>static::get_data($d->jenis_dataset,$request->basis,$request->filter,$name_table,$tahun,2,[1]),
                        'link'=>route('mod.dataset.detail',['tahun'=>$tahun,'id_dataset'=>$d->id,'tw'=>2,'kodepemda'=>$fil?0:null,'monev'=>$monev?$tahun:null])
                    ],
                    'tw_3'=>[
                        'terdata'=>static::get_data($d->jenis_dataset,$request->basis,$request->filter,$name_table,$tahun,3,[0,1]),
                        'terverifikasi'=>static::get_data($d->jenis_dataset,$request->basis,$request->filter,$name_table,$tahun,3,[1]),
                        'link'=>route('mod.dataset.detail',['tahun'=>$tahun,'id_dataset'=>$d->id,'tw'=>3,'kodepemda'=>$fil?0:null,'monev'=>$monev?$tahun:null])
                    ],
                    'tw_4'=>[
                        'terdata'=>static::get_data($d->jenis_dataset,$request->basis,$request->filter,$name_table,$tahun,4,[0,1]),
                        'terverifikasi'=>static::get_data($d->jenis_dataset,$request->basis,$request->filter,$name_table,$tahun,4,[1]),
                        'link'=>route('mod.dataset.detail',['tahun'=>$tahun,'id_dataset'=>$d->id,'tw'=>4,'kodepemda'=>$fil?0:null,'monev'=>$monev?$tahun:null])
                    ],
    
                ];
            }

        }

        return ['data'=>array_values($datanew),
                'last_page'=>$data->lastPage(),
                'curent_page'=>$data->currentPage(),
                'total'=>$data->total(),
                'paginate_count'=>$paginate
        ];

        
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function ajax_load_single($tahun,$id_dataset,$tw,Request $request)
    {      
            $dataset=DB::table('master.dataset')->find($id_dataset);
            $name_table='dataset.'.str_replace('[TAHUN]',$tahun,$dataset->table);
            $tb=explode('.',$name_table);
            $exist_table=false;
            $data=[];
            $paginate=200;
            if(count($tb)>1){
                $exist_table=(int)DB::table('pg_tables')->where('tablename','=',$tb[1])->where('schemaname','=',$tb[0])->count();
            }
            if($exist_table){
                $lokussql=DB::table('master.pemda_lokus as lkg')
                ->selectRaw("lkg.kodepemda,(string_agg(distinct(concat(lkg.tipe_bantuan,' - ',lkg.tahun_bantuan)),'@')) as tipe_bantuan")
                ->groupBy('lkg.kodepemda')->toSql();

                if(strtoupper($dataset->klasifikasi_1)=='DATA PEMDA'){
                    $data=DB::table($name_table.' as d')
                    ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
                    ->leftJoin(DB::raw("(".$lokussql.") as lk"),'lk.kodepemda','=','d.kodepemda')
                    ->selectRaw('mp.regional_1,mp.regional_2,lk.tipe_bantuan,mp.tahun_proyek,mp.nama_pemda as name,d.*')
                    ->where('tw',$tw)
                    ->where('tahun',$tahun);

                }else{
                    $data=DB::table($name_table.' as d')
                    ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
                    ->leftJoin(DB::raw("(".$lokussql.") as lk"),'lk.kodepemda','=','d.kodepemda')
                    ->join('master.master_bu_pemda as bup',[['d.kodepemda','=','bup.kodepemda'],['d.kodebu','=','bup.id_bu']])
                    ->join('master.master_bu as bu','bu.id','=','bup.id_bu')
                    ->selectRaw('bu.nama_bu,mp.regional_1,mp.regional_2,lk.tipe_bantuan,mp.tahun_proyek,mp.nama_pemda as name,d.*')
                    ->orderBy('d.index','asc')
                    ->where('tw',$tw)
                    ->where('tahun',$tahun);
                }

                if($request->kodepemda){
                   $data=$data->where('d.kodepemda','=',$request->kodepemda);
                }
                
                try{
                    $data= $data->whereIn('d.status',$request->status??[0,1])->paginate($paginate);
                    return ['data'=>($data->items()),
                        'last_page'=>$data->lastPage(),
                        'curent_page'=>$data->currentPage(),
                        'paginate_count'=>$paginate,
                        'total'=>$data->total()
                    ];
                    
                }catch(Exception $e){

                }

                 
            }

            return [
                'data'=>[],
                'last_page'=>0,
                'curent_page'=>0,
                'paginate_count'=>$paginate,
                'total'=>0
             ];

           
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
   


    public function update($tahun,$id_dataset,$tw,Request $request)
    {
       $dataset= DB::table('master.dataset')->where('id',$id_dataset)->first();
       
       if($dataset){
           $uid=Auth::User()->id;
           $now=Carbon::now();
           $table=str_replace('[TAHUN]',$tahun,$dataset->table);
           $check_exist=DB::table('pg_tables')->where('schemaname','dataset')->where('tablename',$table)->count();
           if($check_exist){
               $report=[
                   'update'=>0,
                   'insert'=>0,
                   'delete'=>0,

               ];
               $insert=[];
               $datadelete=[];
               $listkodepemda=(DB::table('master.master_pemda')->select('kodepemda')->get()->pluck('kodepemda'))->toArray();
               DB::beginTransaction();
               try{
                foreach($request->data??[] as $key=>$d){
                    if(!isset($d['id'])){
                        $d['id']=null;
                    }

                 
                    if(!isset($d['method'])){
                        $d['method']='none';
                    }
                   if($d['method']!='delete'){
                       unset($d['method']);
                       unset($d['display']);
                       unset($d['name']);
                       unset($d['tahun_proyek']);
                       unset($d['uuid_row']);
                       unset($d['regional_1']);
                       unset($d['regional_2']);
                       unset($d['tipe_bantuan']);
                       unset($d['nama_bu']);
                       if(strtoupper($dataset->klasifikasi_1)=='DATA PEMDA'){
                           unset($d['kodebu']);
                       }
                      
                    //    if($key>0){
                    //     dd($d);
    
                    // }
                        $valid_kode=in_array($d['kodepemda'],$listkodepemda);
                         
                       if($valid_kode){
                           
                        if($d['id']){
                            $d['user_pengisi']=$uid;
                            $d['updated_at']=$now;
                            $check=[
                                'id'=>$d['id'],
                            ];
                            unset($d['id']);
                            if($dataset->jenis_dataset==false){

                            }else{
                               
                            }
                            


                            $up=DB::table('dataset.'.$table)->updateOrinsert($check,$d);
                            // dd($up);
                            
                            if($up){
                                $report['update']+=1;
                            }else{
                                $report['insert']+=1;
                            }
    
                        }else{
                            unset($d['id']);
                            $d['user_pengisi']=$uid;
                            $d['tanggal_pengisian']=$now;
                            $d['updated_at']=$now;
                            $insert[]=$d;
                        }
                       }
                   }else{
                       if($d['id']){
                           $datadelete[]=$d['id'];
                       }
                   }
                } 

                if(count($insert)){
                    $insert_report=DB::table('dataset.'.$table)->insert($insert);
                }

                if(count($datadelete)){
                    $report['delete']=DB::table('dataset.'.$table)->whereIn('id',$datadelete)->delete();
                }

                $report['insert']+=count($insert);

                DB::commit();
                Alert::success('Berhasil','Data Insert :'.$report['insert'].', Data Update :'.$report['update'].', Data Delete :'.$report['delete']);

                }catch(\Exception $e){
                    DB::rollback();
                    Alert::error('Error',$e->getMessage());

                    return back()->withInput();

                }
                return back();
           }else{
               Alert::error('Table tidak Tersedia','mohon menghubungi administrator');
               return back();
           }
       }

      
    }

  
}
