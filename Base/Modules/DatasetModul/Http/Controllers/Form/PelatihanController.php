<?php

namespace Modules\DatasetModul\Http\Controllers\Form;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use Auth;
use Carbon\Carbon;
use App\Http\Controllers\FilterCtrl;

class PelatihanController extends Controller
{
    private $tahun;
    private $id;
    private $table;
    private $table_status;

    private $exist=false;

    public function getOptions(){
        return [
            'pelatihan_op'=>DB::table('dataset.tema_pelatihan as tm')->where('tm.tahun',$this->tahun)->get()
        ];
    }

    public function __construct($tahun=0,$iddataset=0){
        $this->tahun=$tahun;
        $this->id=$iddataset;
        $dataset=DB::table('master.dataset')->find($this->id);
        if($dataset){
            $this->table=$dataset->table;
            $this->table_status='dts_'.$this->id;
            if(str_contains($dataset->table,'[TAHUN]')){
                $this->table=str_replace('[TAHUN]',$this->tahun,$this->table);
                $this->table_status.='_'.$this->tahun;
            }
            $exist=DB::table('information_schema.tables')->where('table_schema','dataset')
            ->where('table_name',$this->table)
            ->first();

            if($exist){
                $this->exist=true;
            }

        }

        $this->middleware('auth');
    }

    public function getter($tahun,Request $request){
       if($this->exist){
            $filter=FilterCtrl::buildFilterPemda($request);
            $pemdas=FilterCtrl::filter_pemda($filter['regional']??null,$filter['lokus'],$filter['sortlist'],$filter['tipe_bantuan'],$filter['jenis_pemda'],$filter['pemda']);

           $data=  DB::table('dataset.'.$this->table.' as d')
           ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
           ->leftJoin('status_data.'.$this->table_status.' as st',[['st.kodepemda','=','d.kodepemda'],['st.tahun','=','d.tahun']])
           ->selectRaw("d.id_ref,d.keterangan,d.tema_pelatihan as tema,d.id as exist_data,mp.nama_pemda,mp.kodepemda,st.status as status_data,d.lampiran_pelatihan as lampiran,d.jumlah_peserta as laki_laki,d.jumlah_peserta_perempuan as perempuan,d.peserta_disable as laki_laki_disable,d.peserta_perempuan_disable as perempuan_disable,d.tanggal_pengisian as update_content,(case when d.status=1 then d.tanggal_pengesahan else d.tanggal_pengesahan_2  end) as update_status")
           ->orderBy('d.kodepemda','asc')
           ->orderBy('d.tema_pelatihan','asc');

            if($request->options['kegiatan_id']){
                $data=$data->where('d.id_ref','=',$request->options['kegiatan_id']);
            }else{
               

            }

            if($request->options['tema']){
                $k=DB::table('dataset.tema_pelatihan')->where('temas','ilike','%'.$request->options['tema'].'%')->get()->pluck('id');
                if($k){
                    $data=$data->whereIn('d.id_ref',$k);
                }
            }

            
           
        
           return $data->get();
           
       }

    }

    public function update($tahun,Request $request){
        $date=Carbon::now();
        $pemda=[];
        $My=Auth::guard('api')->User();
     
        if($request->data_request['mode_single']){
           
            foreach($request->data_request['data'] as $k=>$d){
                if(isset($pemda[$d['kode']])){
                    $pemda[$d['kode']]=[];
                }
            
                $exist=DB::table($this->table)->where(
                    [
                        'kodepemda'=>$d['kode'],
                        'id'=>$d['id'],
                        'tahun'=>$tahun
                    ])->first();
    
                    if($exist){
                        $pemda[$d['kode']][]=$exist->id;
                        DB::table($this->table)->where(
                            [
                                'kodepemda'=>$d['kode'],
                                'id'=>$d['id'],
                                'tahun'=>$tahun
                            ]
                        )->update([
                            'user_pengisi'=>$My->id,
                            'jumlah_peserta_perempuan'=>$d['perempuan'],
                            'jumlah_peserta'=>$d['laki_laki'],
                            'peserta_disable'=>$d['laki_laki_disable'],
                            'peserta_perempuan_disable'=>$d['perempuan_disable'],
                            'tema_pelatihan'=>$d['tema'],
                            'tanggal_pengisian'=>$date,

                        ]);
                    }else{
                        $pemda[$d['kode']][]=DB::table($this->table)->insertGetId([
                            'kodepemda'=>$d['kode'],
                            'tw'=>4,
                            'status'=>true,
                            'point'=>0,
                            'user_pengisi'=>$My->id,
                            'jumlah_peserta_perempuan'=>$d['perempuan'],
                            'jumlah_peserta'=>$d['laki_laki'],
                            'peserta_disable'=>$d['laki_laki_disable'],
                            'peserta_perempuan_disable'=>$d['perempuan_disable'],
                            'tema_pelatihan'=>$d['tema'],
                            'id_ref'=>$d['id_ref'],
                            'tanggal_pengisian'=>$date,

                        ]);
                    }

                    DB::table($this->table_status)->updateOrInsert(
                        [
                            'kodepemda'=>$d['kode'],
                            'tahun'=>$tahun,
                        ],
                        [
                        'kodepemda'=>$d['kode'],
                        'tahun'=>$tahun,
                        'status'=>2,
                        'user_verified'=>$My->id,
                        'user_validate'=>$My->id,
                        'validate_date'=>$date,
                        
                    ]);
    
        
            }
    
    
            foreach($request->data_request['data_delete'] as $k=>$d){
                DB::table($this->table)->where([
                    'kodepemda'=>$d['kode'],
                    ''
                ])->delete();
            }
    
           if($request->data_request['mode_single']){
    
           }else{
                
    
           }
    
           return ($pemda);
    
        }else{

            $kegiatan=DB::table('tema_pelatihan')->where('id',$request->data_request['meta']['id'])->first();

           if($kegiatan){
            foreach($request->data_request['data'] as $k=>$d){
                if(isset($pemda[$d['kode']])){
                    $pemda[$d['kode']]=[];
                }
            
                $exist=DB::table('dataset.'.$this->table)->where(
                    [
                        'kodepemda'=>$d['kode'],
                        'id'=>$d['id'],
                        'tahun'=>$tahun
                    ])->first();

                    if($exist){
                        $pemda[$d['kode']][]=$exist->id;
                        DB::table('dataset.'.$this->table)->where(
                            [
                                'kodepemda'=>$d['kode'],
                                'id'=>$d['id'],
                                'tahun'=>$tahun
                            ]
                        )->update([
                            'user_pengisi'=>$My->id,
                            'jumlah_peserta_perempuan'=>$d['perempuan'],
                            'jumlah_peserta'=>$d['laki_laki'],
                            'peserta_disable'=>$d['laki_laki_disable'],
                            'peserta_perempuan_disable'=>$d['perempuan_disable'],
                            'tanggal_pengisian'=>$date,
                        ]);
                    }else{
                        $pemda[$d['kode']][]=DB::table('dataset.'.$this->table)->insertGetId([
                            'kodepemda'=>$d['kode'],
                            'tw'=>4,
                            'status'=>true,
                            'point'=>0,
                            'tahun'=>$tahun,
                            'user_pengisi'=>$My->id,
                            'jumlah_peserta_perempuan'=>$d['perempuan'],
                            'jumlah_peserta'=>$d['laki_laki'],
                            'peserta_disable'=>$d['laki_laki_disable'],
                            'peserta_perempuan_disable'=>$d['perempuan_disable'],
                            'id_ref'=>$d['id_ref'],
                            'tanggal_pengisian'=>$date,

                        ]);
                    }
                    DB::table('status_data.'.$this->table_status)->updateOrInsert(
                        [
                            'kodepemda'=>$d['kode'],
                            'tahun'=>$tahun,
                        ],
                        [
                        'kodepemda'=>$d['kode'],
                        'tahun'=>$tahun,
                        'status'=>2,
                        'user_verified'=>$My->id,
                        'user_validate'=>$My->id,
                        'validate_date'=>$date,
                        
                    ]);

                   

                    

            }

            foreach($request->data_request['data_delete'] as $k=>$d){
                DB::table($this->table)->where([
                    'id'=>$d['id'],
                    'kodepemda'=>$d['kode']
                ])->delete();
                DB::table($this->table_status)->where([
                    'kodepemda'=>$kodepemda,
                    'tahun'=>$tahun,
                    
                ])->delete();
            }


            DB::table($this->table)->where([
                'tahun'=>$tahun,
                'id_ref'=>$request->data_request['meta']['id']
            ])
            ->update([
                'tema_pelatihan'=>$kegiatan->nama_pelatihan,
                'date_mulai'=>$kegiatan->tanggal_mulai,
                'date_selesai'=>$kegiatan->tanggal_selesai,
                'pelaksana'=>$kegiatan->pelaksana_pelatihan
            ]);
          

            return [
                'code'=>200,
                'message'=>'Berhasil Memperbarui Data'
            ];
           }

        }

      
   }
   
}
