<?php

namespace Modules\DatasetModul\Http\Controllers\Form;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Symfony\Component\HttpFoundation\Response;

class RouteController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index($tahun,$iddataset,Request $request)
    {
        $conf=config('datasetmodul.dataset_bind_form_controller.'.$request->formname,['methods'=>[]]);


        if(in_array($request->method,array_keys($conf['methods']))){
            return eval(' return ( new '.$conf['controller'].'($tahun,$iddataset))->'.$conf['methods'][$request->method]."($"."tahun,$"."request);");
        }else{
            return response()->json(array(
                'code'      =>  500,
                'message'   =>  'no schema'
            ), 500);        

        }
    }

   
}
