<?php

namespace Modules\DatasetModul\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Auth;
use App\Http\Controllers\FilterCtrl;
use DB;
class FormActionController extends Controller
{

    function list_data_pemda($tahun){

    }

    function list_data_bumdam($tahun,$id_dataset,Request $request){

        $dataset_meta=DB::table('master.dataset')->where('id',$id_dataset)->first();

        if($dataset_meta){
            $table=str_replace('[TAHUN]',$tahun,'dataset.'.$dataset_meta->table);
            if(str_contains($dataset_meta->table,'[TAHUN]')){
                $table_status=str_replace('[TAHUN]',$tahun,'status_data.dts_'.$dataset_meta->id.'_[TAHUN]');
            }
            else{
                $table_status='status_data.dts_'.$dataset_meta->id;
            }
            $filter=FilterCtrl::buildFilterPemda($request);

            $pemisions=FilterCtrl::getPemissionsDataset($request,$id_dataset);
            
            if($dataset_meta->klasifikasi_2!='[]'){
                $filter['tipe_bantuan']=json_decode(str_replace('BANTUAN ','',strtoupper($dataset_meta->klasifikasi_2)),true);
            }
            $pemdas=FilterCtrl::filter_pemda($filter['regional']??null,$filter['lokus'],$filter['sortlist'],$filter['tipe_bantuan'],$filter['jenis_pemda'],$filter['pemda']);
            if($dataset_meta->pusat){

            }else{

                $data=DB::table('master.master_pemda as mp')
                ->selectRaw("mp.nama_pemda as title,mp.kodepemda, st.status as status_data")
                ->leftJoin($table_status.' as st','st.kodepemda','=','mp.kodepemda')
                ->whereIn('mp.kodepemda',$pemdas)->get();

                if(strtoupper($dataset_meta->klasifikasi_1)=='DATA BUMDAM'){

                    foreach($data as $k=>$d){
                        $data[$k]->bumdam=DB::table('master.master_bu_pemda as lbu')
                        ->selectRaw("bu.*")
                        ->join('master.master_bu as bu','bu.id','=','lbu.id_bu')
                        ->where('lbu.kodepemda',$d->kodepemda)
                        ->get();
                    }

                }else{

                }
            }
           
            return $data;



            // $column=DB::table('master.dataset_data as sc')
            // ->join('master.data as d','d.id','=','sc.id_data')
            // ->selectRaw("d.*,sc.field_name as field_name")
            // ->where('sc.id_dataset',$id_dataset)->get();

            // $data=DB::table($table.' as d')->get();

        }
        

    }

}