<?php

namespace Modules\DatasetModul\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use DB;
use Storage;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use App\Http\Controllers\FilterCtrl;

class FormExcelController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

    public function getExcel(Request $request,$tahun,$id_dataset,$kodepemda=null){
ini_set('memory_limit', '2048M');
        $dataset=DB::table('master.dataset')->find($id_dataset);
        
       
        
        
       if($dataset){
DB::enableQueryLog();
        $pemda=NULL;
        $title=$dataset->name.' Tahun '.$tahun." ";
            if($kodepemda){
                $pemda=DB::table('master.master_pemda')->where('kodepemda',$kodepemda)->first();
                
                if(!$pemda){
                    return null;
                }

                $title.=$pemda->nama_pemda;
            }
            $filter=FilterCtrl::buildFilterPemda($request);
            $pemdas=FilterCtrl::filter_pemda($filter['regional']??null,$filter['lokus'],$filter['sortlist'],$filter['tipe_bantuan'],$filter['jenis_pemda'],$filter['pemda']);

            $table=$dataset->table;
            $status='dts_'.$dataset->id;
            if(str_contains($dataset->table,"[TAHUN]")){
                $table=str_replace("[TAHUN]",$tahun,$dataset->table);
                $status.='_'.$tahun;
            }

            $exist_table=(int)DB::table('pg_tables')->whereIn('tablename',[$table,$status])->whereIn('schemaname',['dataset','status_data'])->count();
            if($exist_table){
                $columns=DB::table('master.data as d')->join('master.dataset_data as sd','sd.id_data','=','d.id')
                ->where('sd.id_dataset',$id_dataset)
                ->selectRaw('sd.field_name, d.*')
                ->orderBy('index')->get();

                $select=array_map(function($el){
                    return $el->field_name;
                },$columns->toArray());

                $BU_MODE=FALSE;
                if(strtoupper($dataset->klasifikasi_1)=='DATA BUMDAM'){
                    $column_head=array_merge([
                       [
                        'name'=>'Kode Pemda',
                        'field_name'=>'kodepemda',
                        'satuan'=>''
                       ],
                       [
                        'name'=>'Nama Pemda',
                        'field_name'=>'nama_pemda',
                        'satuan'=>''
                       ],
                       [
                        'name'=>'Nama BUMDAM',
                        'field_name'=>'nama_bu',
                        'satuan'=>''
                       ]
                    ],$columns->toArray());
                    $select=array_merge(['mp.kodepemda','mp.nama_pemda','bu.nama_bu as nama_bu'],$select);
                    $order=['mp.kodepemda','d.kodebu','d.tahun','d.index'];
                $BU_MODE=TRUE;

                }else{
                    $select=array_merge(['mp.kodepemda','mp.nama_pemda'],$select);
                    $order=['mp.kodepemda','d.tahun','d.index'];
                    $column_head=array_merge([
                        [
                         'name'=>'Kode Pemda',
                         'field_name'=>'kodepemda',
                         'satuan'=>''
                        ],
                        [
                         'name'=>'Nama Pemda',
                         'field_name'=>'nama_pemda',
                         'satuan'=>''
                        ],
                       
                     ],$columns->toArray());

                }

                $data=DB::table('dataset.'.$table." as d")
                ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
                ->leftJoin('status_data.'.$status.' as st',[['st.kodepemda','=','d.kodepemda'],['st.tahun','=','d.tahun']])
                ->selectRaw(implode(",",$select))->whereIn('d.kodepemda',$pemdas)
                ->where('d.tahun',$tahun)->where('d.tw',4);
//dd(DB::getQueryLog());

                if($BU_MODE){
                    $data=$data->join('master.master_bu as bu','bu.id','=','d.kodebu');
                }
                

                foreach($order as $o){
                    $data=$data->orderBy($o,'ASC');
                }
                if( $pemda){
                    $data=$data->where('d.kodepemda','=',$pemda->kodepemda);
                }

                $data=$data->get();

                $spreadsheet = new Spreadsheet();
                $sheet = $spreadsheet->getActiveSheet();
                $sheet->setCellValue('A1', $title);
                $sheet->mergeCells("A1:".static::cellA(count($column_head) -1,1));
                foreach($column_head as $c=>$k){
                    $k=(array)$k;
                    $sheet->setCellValue(static::cellA($c,3),$k['name']);
                }

                foreach($data as $r=>$d){
                    $d=(array)$d;
                    foreach($column_head as $c=>$k){
                        $k=(array)$k;
                        if(isset($k['tipe_nilai'])){
                            if($k['tipe_nilai']=='numeric'){
                                $d[$k['field_name']]=(float)$d[$k['field_name']];
                            }
                        }
                        $sheet->setCellValue(static::cellA($c,$r+4),$d[$k['field_name']]);
                    }
                    $sheet->setAutoFilter('A3:'.static::cellA($c,$r+4));
                }

                $writer = new Xlsx($spreadsheet);
                // header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment; filename="'. ($title).'.xlsx"');
                $writer->save('php://output');


               


    


            }else{
                return null;
            }
       }
    }

    static function cellA($col,$row){
        $ABJ=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
        return $ABJ[$col].$row;
    }

    public function loadDataPemda($tahun,$id_dataset,$kodepemda,Request $request){
       $dataset= DB::table('master.dataset')->find($id_dataset);
       $select=['std.status as status_data'];
       $length = $request->input('length');
       $orderBy = $request->input('column'); //Index
       $columnSerach=$request->input('columns_serach')??null;
       $orderByDir = $request->input('dir', 'asc');
       $searchValue = $request->input('search');

       if($dataset){
           $table_status='dts_'.$dataset->id;
           $columns=DB::table('master.dataset_data as sd')
           ->join('master.data as d','d.id','=','sd.id_data')
           ->selectRaw('d.*,sd.field_name')
           ->where('id_dataset',$id_dataset)->orderBy('index','asc')->get();
            foreach($columns as $k=>$c){
                $select[]="d.".$c->field_name;
            }
            if(str_contains($dataset->table,'[TAHUN]')){
                $dataset->table=str_replace('[TAHUN]',$tahun,$dataset->table);
                $table_status.='_'.$tahun;
            }
           
            $exist_table=(int)DB::table('pg_tables')->where('tablename','=',$dataset->table)->where('schemaname','=','dataset')->count();

            if($exist_table){
                $data=DB::table('dataset.'.$dataset->table.' as d')
                ->selectRaw(implode(',',$select))
                ->leftJoin('status_data.'.$table_status.' as std', DB::raw("(std.kodepemda=d.kodepemda and std.tahun=".$tahun.")"),DB::raw("true"))
                ->where([
                    ['d.kodepemda','=',$kodepemda],
                    ['d.tw','=',4],
                    ['d.tahun','=',$tahun],
                ]);
                if($searchValue){
                    $where=[];
                    if($columnSerach){
                        $where[]='('.$columnSerach."::text ilike '%".$searchValue."%')"; 
                    }else{
                        foreach($columns as $f=>$c){
                            $where[]='('.$c->field_name."::text ilike '%".$searchValue."%')"; 
                        }
                    }
                    
                    
                    $data=$data->whereRaw(implode(' or ',$where));
                }


                if($orderBy=='id'){
                    $orderBy='d.index';
                   
                }

                if($orderBy){
                    $data=$data->orderBy($orderBy,$orderByDir);
                }
               
                $data = $data->paginate($length);
                $data=[
                    'data'=>$data->items(),
                    'links'=>$data->links(),
                    'meta'=>$data,
                    'payload'=>$request->all()
                ];

                return $data;

                
                
            }
       }
    }

    public function createForm($tahun,$id_dataset,$kodepemda=null){
        $dataset=DB::table('master.dataset as dts')->find($id_dataset);
     

        if($dataset){
            $meta=[
                'title'=>$dataset->name,
                'pemda'=>null,
                'tahun'=>$tahun,
                'jenis_dataset'=>[
                    'kode'=>0,
                    'name'=>'Single Row',
                    'deskripsi'=>'Hanya Dapat Mengakomondir Satu Data Pemda / (Satu Data Pemda Dengan Multy Lokus NUWSP) Dalam Row Data'
                ],
                
            ];

            if($dataset->jenis_dataset){
                $pemda=DB::table('master.master_pemda')->where('kodepemda',$kodepemda)->first();
                if($pemda){

                    $meta['jenis_dataset']=[
                        'kode'=>1,
                        'name'=>'Multy Row',
                        'deskripsi'=>'Dapat Mengakomondir Multy Data Pemda Dengan Multy Lokus NUWSP) Dalam Row Data'
                    ];
                    $meta['title'].=' '.$pemda->nama_pemda;
                    $meta['pemda']=[
                        'nama_pemda'=>$pemda->nama_pemda,
                        'kodepemda'=>$pemda->kodepemda,
                        'lokus'=>[]
                    ];

                    $data=DB::table('master.master_pemda as mp')
                    ->leftJoin('dataset.'.$dataset->table.' as d','mp.kodepemda','=','d.kodepemda')
                    ->leftJoin('master.pemda_lokus as lk',[['mp.kodepemda','=','lk.kodepemda'],['lk.tahun'=>DB::raw($tahun)],['lk.id','=','d.id_lokus']])
                    ->selectRaw("
                        dd
                    ")->get();



                    
                }else{
                    Alert::error('','Kode Pemda Tidak Terdata');
                    return abort(404);
                }
            }
            $columns=DB::table('master.data as ds')->where('id_dataset',$id_dataset)->get();


            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();



            $sheet->setCellValue('A1', $dataset->name);


        }


    } 

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('datasetmodul::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('datasetmodul::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('datasetmodul::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
