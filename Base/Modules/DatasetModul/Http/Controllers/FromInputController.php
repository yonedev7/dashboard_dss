<?php

namespace Modules\DatasetModul\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use App\Http\Controllers\FilterCtrl;
use Auth;
use Carbon\Carbon;
use Storage;
class FromInputController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

    public function saveKomitmen($tahun,$id_dataset,$kodepemda,Request $request){
        $dataset=DB::table('master.dataset as dts')->find($id_dataset);

        $h=[];
        if($dataset){
           DB::beginTransaction();
           try{
            for($var=(int)$request->tahun_mulai;$var<=(int)$request->tahun_selesai;$var++){
                $status_data='dts_'.$dataset->id;
                $table=$dataset->table;

                if(str_contains($dataset->table,'[TAHUN]')){
                    $status_data.='_'.$var;

                    $table=str_replace('[TAHUN]',$var,$dataset->table);
                }
                
                $exist=DB::table('information_schema.tables')->where('table_schema','dataset')
                ->where('table_name',$table)
                ->first();
        
                if($exist){
                   $h[$var]=DB::table('dataset.'.$table)->updateOrInsert([
                        'tahun'=>$var,
                        'kodepemda'=>$kodepemda
                    ],
                    [
                        $request->column['field_name']=>$request->val,
                        'tw'=>4,
                        'user_pengisi'=>Auth::id(),
                        'kodepemda'=>$kodepemda,
                        'tahun'=>$var
                    ]);
                }
           }

           DB::table('dataset.'.$table)->where(['kodepemda'=>$kodepemda])
           ->where('tahun','<',$request->tahun_mulai)->delete();

           DB::table('dataset.'.$table)->where(['kodepemda'=>$kodepemda])
           ->where('tahun','>',$request->tahun_selesai)->delete();
           DB::commit();

           }catch(\Exception $e){
                DB::rollback();
           }

        }

        return ['code'=>200,'d'=>$h];

    }

    public function getKomitmen($tahun,$id_dataset,$kodepemda,Request $request){
        $dataset=DB::table('master.dataset as dts')->find($id_dataset);
        $column=DB::table('master.dataset_data')->where(['id_dataset'=>$id_dataset,'id_data'=>$request->id_data])->first();
        $komitmen=[
            'komitmen'=>0,
            'tahun'=>[
                'min'=>(int)date('Y'),
                'max'=>2018
            ]
            ];
        if($dataset){
            for($var=2018;$var<=2028;$var++){
                 $status_data='dts_'.$dataset->id;
                 $table=$dataset->table;
                 if(str_contains($dataset->table,'[TAHUN]')){
                     $status_data.='_'.$var;
 
                     $table=str_replace('[TAHUN]',$var,$dataset->table);
                 }
                 
                 $exist=DB::table('information_schema.tables')->where('table_schema','dataset')
                 ->where('table_name',$table)
                 ->first();
         
                 if($exist){
                     $d=DB::table('dataset.'.$table)->where(['tahun'=>$var,'kodepemda'=>$kodepemda])->first();
                     if($d){
                        $d=(array)$d;
                        if($komitmen['komitmen']<$d[$column->field_name]){
                            $komitmen['komitmen']=$d[$column->field_name];
                        }

                        if($var<$komitmen['tahun']['min']){
                            $komitmen['tahun']['min']=$var;
                        }

                        if($var>$komitmen['tahun']['max']){
                            $komitmen['tahun']['max']=$var;
                        }

                     }
                 }
            }
 
         }
 
         return ['code'=>200,
         'data'=>$komitmen
        ];


    }

    public function index($tahun,$id_dataset,Request $request)
    {
        $data=DB::table('master.master_pemda as  mp');
        $dataset=DB::table('master.dataset as dts')->find($id_dataset);
        if($dataset){
            $dataset_sandingan=DB::table('master.dataset_sandingan as sd')
            ->join('master.dataset as dt','sd.id_dataset_sandingan','=','dt.id')
            ->selectRaw('dt.*')
            ->where('id_dataset',$id_dataset)
            ->first();

            $page_meta=[
                'title'=>$dataset->name,
                'keterangan'=>$dataset->tujuan,
            ];
            $status_data='dts_'.$dataset->id;
            if(str_contains($dataset->table,'[TAHUN]')){
                 $status_data.='_'.$tahun;

                $dataset->table=str_replace('[TAHUN]',$tahun,$dataset->table);
            }
            
            $exist=DB::table('information_schema.tables')->where('table_schema','dataset')
            ->where('table_name',$dataset->table)
            ->first();
    
            if(!$exist){
                return ('ruang data belum tersedia');
            }


            $bind_form=config('datasetmodul.dataset_form');
            if(isset($bind_form[$dataset->id])){
                $clModul=eval('return new '.$bind_form[$dataset->id]['controller'].'('.$tahun.','.$dataset->id.');');
                $options=$clModul->getOptions();


                $filter=FilterCtrl::buildFilterPemda($request);
                $pemisions=FilterCtrl::getPemissionsDataset($request,$id_dataset);
                if($dataset->klasifikasi_2!='[]'){
                    $filter['tipe_bantuan']=json_decode(str_replace('BANTUAN ','',strtoupper($dataset->klasifikasi_2)),true);
                }
                $pemdas=FilterCtrl::filter_pemda($filter['regional']??null,$filter['lokus'],$filter['sortlist'],$filter['tipe_bantuan'],$filter['jenis_pemda'],$filter['pemda']);

                return view('datasetmodul::form-bind.index')
                ->with([
                'dataset'=>$dataset,
                'options'=>$options,
                'permisions'=>$pemisions['permissions'],
                'component'=>$bind_form[$dataset->id]['component'],
                'list_pemda'=>DB::table('master.master_pemda')
                    ->whereIn('kodepemda',$pemdas)
                    ->selectRaw('kodepemda as kode,nama_pemda as label_name')->get()
                ]);
            }



           $component_load=static::componentLoad($dataset);
            // return view('datasetmodul::form.new_form.data_bumdam')->with([
            //     'page_meta'=>$page_meta,
            //     'dataset'=>$dataset
            // ]);

            
            return view('datasetmodul::form.listing-pemda')->with(['fingerprint'=>$request->fingerprint(),'page_meta'=>$page_meta,'dataset'=>$dataset,'component_load'=>$component_load,'datasetsandingan'=>$dataset_sandingan]);
          
        }
        
        
    }


    public function upload_file($tahun,$id_dataset,$kodepemda,Request $request){
        try{
            if($request->file('file')){
                try{
                    $file= Storage::put('public/dataset-files/'.$tahun.'/'.$id_dataset.'/'.$kodepemda,$request->file);
                    if($file){
                        return Storage::url($file);
                    }
                }catch(\Exeption $e){
                    return null;
                }
            
            }
        }catch(\Exeption $e){
                return null;
        }
   

        return null;
    }
    public static function componentLoad($dataset){
        $component_load=[];
        if(strtoupper($dataset->klasifikasi_1)=='DATA PEMDA'){
            // pemda
            if($dataset->jenis_dataset){

                if(file_exists(__DIR__.'/../../Resources/views/form/partial_multy/chose_'.$dataset->id.'_pemda.blade.php')){
                    $component_load[]='datasetmodul::form/partial_multy/chose_'.$dataset->id.'_pemda';
                }else if(file_exists(__DIR__.'/../../Resources/views/form/partial_multy/chose_pemda.blade.php')){
                    $component_load[]='datasetmodul::form/partial_multy/chose_pemda';
                }
                // result

                if(file_exists(__DIR__.'/../../Resources/views/form/partial_multy/show_'.$dataset->id.'_iu.blade.php')){
                    $component_load[]='datasetmodul::form/partial_multy/show_'.$dataset->id.'_iu';
                }else if(file_exists(__DIR__.'/../../Resources/views/form/partial_multy/show_pemda.blade.php')){
                    $component_load[]='datasetmodul::form/partial_multy/show_pemda';
                }

            }else{
                if(file_exists(__DIR__.'/../../Resources/views/form/partial_single/sd_'.$dataset->id.'_iu.blade.php')){
                    $component_load[]='datasetmodul::form/partial_single/sd_'.$dataset->id.'_iu';
                }else if(file_exists(__DIR__.'/../../Resources/views/form/partial_single/single_pemda.blade.php')){
                    $component_load[]='datasetmodul::form/partial_single/single_pemda';
                }

                // result
                if(file_exists(__DIR__.'/../../Resources/views/form/partial_single/show_'.$dataset->id.'_iu.blade.php')){
                    $component_load[]='datasetmodul::form/partial_single/show_'.$dataset->id.'_iu';
                }else if(file_exists(__DIR__.'/../../Resources/views/form/partial_single/show_pemda.blade.php')){
                    $component_load[]='datasetmodul::form/partial_single/show_pemda';
                }
            }
        }else{
            // bumdam

            if($dataset->jenis_dataset){

                if(file_exists(__DIR__.'/../../Resources/views/form/partial_multy/chose_'.$dataset->id.'_bu.blade.php')){
                    $component_load[]='datasetmodul::form/partial_multy/chose_'.$dataset->id.'_bu';
                }else if(file_exists(__DIR__.'/../../Resources/views/form/partial_multy/chose_bu.blade.php')){
                    $component_load[]='datasetmodul::form/partial_multy/chose_bu';
                }

            }else{
                if(file_exists(__DIR__.'/../../Resources/views/form/partial_single/sd_'.$dataset->id.'_iu.blade.php')){
                    $component_load[]='datasetmodul::form/partial_single/sd_'.$dataset->id.'_iu';
                }else if(file_exists(__DIR__.'/../../Resources/views/form/partial_single/single_bu.blade.php')){
                    $component_load[]='datasetmodul::form/partial_single/single_bu';
                }

                
            }

        }

        return $component_load;
    }

   
    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function list_columns($tahun,$id_dataset,Request $request){
        return DB::table('master.dataset_data as sd')
        ->join('master.data as d','d.id','=','sd.id_data')
        ->where('sd.id_dataset',$id_dataset)
        ->selectRaw("d.*,sd.field_name,d.id as id_data_master")
        ->orderBy('sd.index','asc')
        ->get();

    }

    public function load_exist_bu_data($tahun,$id_dataset,$kodepemda,Request $request){
        $dataset=DB::table('master.dataset as dts')->find($id_dataset);
        if($dataset){
            $select=($request->columns??[]);
            if($request->guide){
                $select=[];
                $c=DB::table('master.dataset_data as dd')
                ->join('master.data as d','d.id','=','dd.id_data')
                ->selectRaw("dd.field_name")
                ->where([
                    'dd.id_dataset'=>$id_dataset,
                ])->whereIn('dd.id_data',$request->columns??[])
                ->get();

                foreach($c as $cc){
                    $select[]=$cc->field_name;
                }
            }
           
           
            $status_data='dts_'.$dataset->id;
            if(str_contains($dataset->table,'[TAHUN]')){
                $status_data.='_'.$tahun;
                $dataset->table=str_replace('[TAHUN]',$tahun,$dataset->table);
            }
            
            $exist=DB::table('information_schema.tables'
            )->whereIn('table_schema',['dataset','status_data'])
            ->whereIn('table_name',[$dataset->table,$status_data])
            ->count();

            if($exist!=2){
                return [];
            }
            if($exist==2){
                $data=DB::table('master.master_pemda as mp')
                ->join('master.master_bu_pemda as bu','bu.kodepemda','=','mp.kodepemda')
                ->join('master.master_bu as mbu','bu.id_bu','=','mbu.id')
                ->leftJoin('dataset.'.$dataset->table.' as d',[['d.kodepemda','=','mp.kodepemda'],['d.kodebu','=','mbu.id'],['d.tahun','=',DB::raw($tahun)],['d.tw','=',DB::raw(4)]])
                ->selectRaw("d.id,mp.kodepemda,mbu.nama_bu,d.keterangan,mbu.id as kodebu ".(count($select)?(',d.'.implode(",d.",$select)):''))
                ->where('mp.kodepemda',$kodepemda);
                if($request->guide){
                    $dataset->name= $dataset->name.' Tahun '.$tahun;
                    return [
                        'dataset'=>$dataset,
                        'data'=>$data->get(),
                    ];
                }else{
                    return $data->get();
                }
            }
                
        }
    }

    public function save($tahun,$id_dataset,$kodepemda,Request $request){

        $dataset=DB::table('master.dataset as dts')->find($id_dataset);
        $insert=[];
        $update=[];
        $delete=[];

        if($dataset){
            $select=($request->columns??[]);
           
            $status_data='dts_'.$dataset->id;
            if(str_contains($dataset->table,'[TAHUN]')){
                $status_data.='_'.$tahun;
                $dataset->table=str_replace('[TAHUN]',$tahun,$dataset->table);
            }
            
            $exist=DB::table('information_schema.tables'
            )->whereIn('table_schema',['dataset','status_data'])
            ->whereIn('table_name',[$dataset->table,$status_data])
            ->count();

            if($exist!=2){
                return [
                    'code'=>500,
                    'message'=>'Ruang Penyimpanan Dataset Tidak Tersedia'
                ];
            }
            if($exist==2){
               $now=Carbon::now();
               $myId=Auth::guard('api')->User()->id;
                foreach($request->data??[] as $key=>$d){
                    if(in_array($request->method,['edit','new'])){
                       
                        if($d['id']){
                            $dd=$d;
                            unset($dd['id']);
                            unset($dd['kodebu']);
                            unset($dd['kodepemda']);
                            $dd['tahun']=$tahun;
                            $dd['tw']=4;
                            $dd['user_pengisi']=$myId;
                            $dd['tanggal_pengisian']=$now;

                           
                            if(strtoupper($dataset->klasifikasi_1)=='DATA PEMDA'){
                                $up_s=[
                                    'id'=>$d['id'],
                                    'kodepemda'=>$kodepemda,
                                    'tw'=>4,
                                    'tahun'=>$tahun,
                                ];
                            }else{
                                $up_s=[
                                    'id'=>$d['id'],
                                    'kodepemda'=>$kodepemda,
                                    'tw'=>4,
                                    'tahun'=>$tahun,
                                    'kodebu'=>$d['kodebu']
                                ];

                            }
                            
                            $dum=[
                                $up_s,
                                $dd
                            ];
                            $update[]=$dum;
                        }else{

                            $dd=$d;
                            unset($dd['id']);
                            $dd['user_pengisi']=$myId;
                            $dd['tahun']=$tahun;
                            $dd['tw']=4;
                            $dd['tanggal_pengisian']=$now;
                            $insert[]=$dd;
                        }
                    }else if($request->method=='delete'){
                        if($d['id']){
                            
                            $delete[]=[
                                'id'=>$d['id'],
                                'kodepemda'=>$kodepemda
                            ];
                        }
                    }
                }
                DB::beginTransaction();
                try{
                    foreach($update as $k=> $up){
                        $x=DB::table('dataset.'.$dataset->table)->where($up[0])->update($up[1]);
                        if($x){
                            DB::table('status_data.'.$status_data)->where([
                                'kodepemda'=>$kodepemda,
                                'tahun'=>$tahun
                            ])->delete();
                        }
                        if($x){
                            $update[$k]=true;
    
                        }
                    }

                    foreach($delete as $k=> $up){

                        $x=DB::table('dataset.'.$dataset->table)->where('id','=',$up['id'])->delete();
                        if($x){
                            DB::table('status_data.'.$status_data)->where([
                                'kodepemda'=>$kodepemda,
                                'tahun'=>$tahun
                            ])->delete();
                        }
                        if($x){
                            $delete[$k]=true;
    
                        }
                    }
    
                    foreach($insert as $k=>$up){
                        $id=DB::table('dataset.'.$dataset->table)->insertGetId($up);
                        DB::table('dataset.'.$dataset->table)->where('id',$id)->update(['index'=>$id]);

                        DB::table('status_data.'.$status_data)->where([
                            'kodepemda'=>$kodepemda,
                            'tahun'=>$tahun
                        ])->delete(); 
                        if($id){
                            $insert[$k]=true;
                        }
                    }
                    DB::commit();
                }catch(\Exception $err){
                    DB::rollback();
                    return [
                        'code'=>500,
                        'message'=>'Server Error',
                        'message_sistem'=>$err->getMessage()
                    ];
                }

            }
        }else{
            return [
                'code'=>500,
                'message'=>'Ruang Penyimpanan Dataset Tidak Tersedia'
            ];
        }
        $rekap=[
            'in'=>0,
            'up'=>0,
            'del'=>0
        ];

        foreach($insert as $d){
            if($d==true){
                $rekap['in']+=1;
            }
        }

        foreach($update as $d){
            if($d==true){
                $rekap['up']+=1;
            }
        }
        foreach($delete as $d){
            if($d==true){
                $rekap['del']+=1;
            }
        }

        return [
            'code'=>200,
            'message'=>$rekap['up'].' Data Dirubah, '.$rekap['in'].' Data Ditambahkan, '.$rekap['del'].' Data Dihapus',
            'payload'=>$request->all()
        ];
    }

    public function load_exist_data($tahun,$id_dataset,$kodepemda,Request $request){
        $dataset=DB::table('master.dataset as dts')->find($id_dataset);
        if($dataset){
             $select=($request->columns??[]);
            $status_data='dts_'.$dataset->id;
            if(str_contains($dataset->table,'[TAHUN]')){
                 $status_data.='_'.$tahun;

                $dataset->table=str_replace('[TAHUN]',$tahun,$dataset->table);
            }
            
            $exist=DB::table('information_schema.tables'
            )->whereIn('table_schema',['dataset','status_data'])
            ->whereIn('table_name',[$dataset->table,$status_data])
            ->count();

            if($exist!=2){
                return [];
            }
            if($exist==2){
                $data=DB::table('master.master_pemda as mp')
                ->leftJoin('dataset.'.$dataset->table.' as d',[['d.kodepemda','=','mp.kodepemda'],['d.tahun','=',DB::raw($tahun)],['d.tw','=',DB::raw(4)]])
                ->selectRaw("d.id,mp.kodepemda,mp.nama_pemda,d.keterangan ".(count($select)?(',d.'.implode(",d.",$select)):''))
                ->where('mp.kodepemda',$kodepemda);
                if($request->guide){
                    $dataset->name= $dataset->name.' Tahun '.$tahun;
                    return [
                        'dataset'=>$dataset,
                        'data'=>$data->get(),
                    ];
                }else{
                    return $data->get();
                }
            }
        }

        return [];

    }

    public function rollback($tahun,$id_dataset,$kodepemda,Request $request){
        $dataset=DB::table('master.dataset as dts')->find($id_dataset);
        if($dataset){
             $select=($request->columns??[]);
            $status_data='dts_'.$dataset->id;
            if(str_contains($dataset->table,'[TAHUN]')){
                 $status_data.='_'.$tahun;

                $dataset->table=str_replace('[TAHUN]',$tahun,$dataset->table);
            }
            
            $exist=DB::table('information_schema.tables'
            )->whereIn('table_schema',['dataset','status_data'])
            ->whereIn('table_name',[$dataset->table,$status_data])
            ->count();

            if($exist!=2){
                return [
                    'code'=>'500',
                    'message'=>'Ruang Penyimpanan Tidak Tersedia'
                ];
            }

            $data=DB::table('status_data.'.$status_data)->where([
                'kodepemda'=>$kodepemda,
                'tahun'=>$tahun
            ])->first();
            $now=Carbon::now();
            $myId=Auth::guard('api')->User()->id;
            if($data){
                if($request->state==1){
                    $data=DB::table('status_data.'.$status_data)->where([
                        'kodepemda'=>$kodepemda,
                        'tahun'=>$tahun,
                    ])->update([
                        'status'=>1,
                        'user_validate'=>null,
                        'validate_date'=>null,
                    ]);
                    return [
                        'code'=>200,
                        'message'=>'Status Data Berhasil di Rubah Tahap Verifikasi'
                    ];
                }else{
                    $data=DB::table('status_data.'.$status_data)->where([
                        'kodepemda'=>$kodepemda,
                        'tahun'=>$tahun
                    ])->delete();
                    return [
                        'code'=>200,
                        'message'=>'Status Data Berhasil di Rubah Tahap Input Data'
                    ];
                }
                
            
            }else{
                
                return [
                    'code'=>500,
                    'message'=>'Data Belum Memiliki Status '.$kodepemda.' '.$status_data.' '.json_encode($request->all())
                ];
            
            }
        }else{
            return [
                'code'=>500,
                'message'=>'Terdapat Kegagalan Pada Server',
                'message_sistem'=>'db not found'
            ];
        }

    }

    public function verified($tahun,$id_dataset,$kodepemda){
        $dataset=DB::table('master.dataset as dts')->find($id_dataset);
        if($dataset){
             $select=($request->columns??[]);
            $status_data='dts_'.$dataset->id;
            if(str_contains($dataset->table,'[TAHUN]')){
                 $status_data.='_'.$tahun;

                $dataset->table=str_replace('[TAHUN]',$tahun,$dataset->table);
            }
            
            $exist=DB::table('information_schema.tables'
            )->whereIn('table_schema',['dataset','status_data'])
            ->whereIn('table_name',[$dataset->table,$status_data])
            ->count();

            if($exist!=2){
                return [
                    'code'=>'500',
                    'message'=>'Ruang Penyimpanan Tidak Tersedia'
                ];
            }

            $data=DB::table('status_data.'.$status_data)->where([
                'kodepemda'=>$kodepemda,
                'tahun'=>$tahun
            ])->first();
            $now=Carbon::now();
            $myId=Auth::guard('api')->User()->id;
            if($data){
                if($data->status==2){
                    return [
                        'code'=>500,
                        'message'=>'Data Dalam Kondisi Sudah Tervalidasi Pada '.Carbon::parse($data->validate_date)->format('Y/m/d')
                    ];
                }else if($data->status==1){
                    return [
                        'code'=>500,
                        'message'=>'Data Dalam Kondisi Sudah Terverifikasi Pada '.Carbon::parse($data->verified_date)->format('Y/m/d')
                    ];
                }
            }else{
                DB::beginTransaction();
                try{
                    $x=DB::table('status_data.'.$status_data)->insert([
                        'kodepemda'=>$kodepemda,
                        'tahun'=>$tahun,
                        'status'=>1,
                        'user_verified'=>$myId,
                        'verified_date'=>$now
                    ]);

                    DB::commit();

                }catch(\Exception $e){
                    DB::rollback();
                    return [
                        'code'=>500,
                        'message'=>'Terdapat Kegagalan Pada Server',
                        'message_sistem'=>$e->getMessage()
                    ];
                }

                return [
                    'code'=>200,
                    'message'=>'Berhasil Melakukan Validasi Data'
                ];
            
            }
        }else{
            return [
                'code'=>500,
                'message'=>'Terdapat Kegagalan Pada Server',
                'message_sistem'=>'db not found'
            ];
        }

    }

    public function validate($tahun,$id_dataset,$kodepemda){
        $dataset=DB::table('master.dataset as dts')->find($id_dataset);
        if($dataset){
             $select=($request->columns??[]);
            $status_data='dts_'.$dataset->id;
            if(str_contains($dataset->table,'[TAHUN]')){
                 $status_data.='_'.$tahun;
                $dataset->table=str_replace('[TAHUN]',$tahun,$dataset->table);
            }
            
            $exist=DB::table('information_schema.tables'
            )->whereIn('table_schema',['dataset','status_data'])
            ->whereIn('table_name',[$dataset->table,$status_data])
            ->count();

            if($exist!=2){
                return [
                    'code'=>'500',
                    'message'=>'Ruang Penyimpanan Tidak Tersedia'
                ];
            }

            $data=DB::table('status_data.'.$status_data)->where([
                'kodepemda'=>$kodepemda,
                'tahun'=>$tahun
            ])->first();

            $now=Carbon::now();
            $myId=Auth::guard('api')->User()->id;
            if($data){
                if($data->status==2){
                    return [
                        'code'=>500,
                        'message'=>'Data Dalam Kondisi Sudah Tervalidasi Pada '.Carbon::parse($data->validate_date)->format('Y/m/d')
                    ];
                }else if($data->status==1){

                    DB::beginTransaction();
                    try{
                        $x=DB::table('status_data.'.$status_data)->where('id',$data->id)->update(
                              
                            [
                                'kodepemda'=>$kodepemda,
                                'tahun'=>$tahun,
                                'status'=>2,
                                'user_validate'=>$myId,
                                'validate_date'=>$now
                            ]);
    
                        DB::commit();
    
                    }catch(\Exception $e){
                        DB::rollback();
                        return [
                            'code'=>500,
                            'message'=>'Terdapat Kegagalan Pada Server',
                            'message_sistem'=>$e->getMessage()
                        ];
                    }
    
                    return [
                        'code'=>200,
                        'message'=>'Berhasil Melakukan Validasi Data'
                    ];
                
                    
                }
            }else{
                return [
                    'code'=>500,
                    'message'=>'Data Belum Dilakukan Verifikasi'
                ];
               
            }
        }else{
            return [
                'code'=>500,
                'message'=>'Terdapat Kegagalan Pada Server',
                'message_sistem'=>'db not found'
            ];
        }

    }

    public function list_data($tahun,$id_dataset,Request $request){

        $dataset=DB::table('master.dataset as dts')->find($id_dataset);
        if($dataset){
            $status_data='dts_'.$dataset->id;
            if(str_contains($dataset->table,'[TAHUN]')){
                 $status_data.='_'.$tahun;

                $dataset->table=str_replace('[TAHUN]',$tahun,$dataset->table);
            }
            
            $exist=DB::table('information_schema.tables'
            )->whereIn('table_schema',['dataset','status_data'])
            ->whereIn('table_name',[$dataset->table,$status_data])
            ->count();

            if($exist!=2){
                return [];
            }
            if($exist==2){
                $filter=FilterCtrl::buildFilterPemda($request);
                $pemisions=FilterCtrl::getPemissionsDataset($request,$id_dataset);

                if($dataset->klasifikasi_2!='[]'){
                    $filter['tipe_bantuan']=json_decode(str_replace('BANTUAN ','',strtoupper($dataset->klasifikasi_2)),true);
                }

                
                $pemdas=FilterCtrl::filter_pemda($filter['regional']??null,false,$filter['sortlist'],$filter['tipe_bantuan'],$filter['jenis_pemda'],$filter['pemda']);
                $data=DB::table('master.master_pemda as mp')
                ->leftJoin('dataset.'.$dataset->table.' as d',DB::raw("(d.kodepemda=mp.kodepemda and d.tahun=".$tahun." and d.tw=4)"),'=',DB::raw("true"))
                ->leftJoin('status_data.'.$status_data.' as std',DB::raw("(std.kodepemda=d.kodepemda and std.tahun=".$tahun.")"),'=',DB::raw("true"))
                ->whereRaw("mp.kodepemda in ('".implode("','",$pemdas)."')")
                ->groupBy('mp.kodepemda');

                if($dataset->jenis_dataset){
                    $select="max(d.tanggal_pengisian) as updated_content,max(case when (std.status=1) then std.verified_date else std.validate_date end)  as updated_status,max(mp.nama_pemda) as nama_pemda,mp.kodepemda,max(std.status) as status_data,count(d.id) as exist_data";

                    // single
                    if(strtoupper($dataset->klasifikasi_1)=='DATA PEMDA'){
                        // pemda

                    }else{
                        // bu

                    }
                }else{ 

                    $select="max(d.tanggal_pengisian) as updated_content,max(case when (std.status=1) then std.verified_date else std.validate_date end) as updated_status,max(mp.nama_pemda) as nama_pemda,mp.kodepemda,max(std.status) as status_data,count(d.id) as exist_data";

                    if(strtoupper($dataset->klasifikasi_1)=='DATA PEMDA'){
                        // pemda
                        $columns=DB::table('master.dataset_data')->where('id_dataset',$dataset->id)->select('field_name')->get();

                        foreach($columns as $c){
                            $select.=',d.'.$c->field_name;
                            $data=$data->groupBy('d.'.$c->field_name);
                        }

                    }else{
                        // bu
                        $columns=DB::table('master.dataset_data')->where('id_dataset',$dataset->id)->select('field_name')->get();
                        $select.=",max(bu.nama_bu) as nama_bu,max(bu.id) as kodebu";
                        $data=$data->groupBy('bup.id_bu')
                        ->join('master.master_bu_pemda as bup',[['bup.kodepemda','=','mp.kodepemda']])
                        ->join('master.master_bu as bu','bup.id_bu','=','bu.id');

                


                        foreach($columns as $c){
                            $select.=',max(d.'.$c->field_name.") as ".$c->field_name;
                        }
                    }

                }

                
                $data=$data->selectRaw($select)
                ->orderBy('mp.kodepemda','asc')
                ->get();

                if(!str_contains($dataset->klasifikasi_1,'BUMDAM')){
                $data_lst=$data;
                foreach($data_lst as $k=>$d){
                    $data_lst[$k]->permissions=$pemisions['permissions'];
                }
                

                }else{
                    $data_lst=[];
                    foreach($data as $k=>$d){
                        if(!isset($data_lst[$d->kodepemda])){
                            $data_lst[$d->kodepemda]=[
                                'nama_pemda'=>$d->nama_pemda,
                                'status_data'=>$d->status_data,
                                'updated_content'=>$d->updated_content,
                                'updated_status'=>$d->updated_status,
                                'exist_data'=>$d->exist_data,
                                'kodepemda'=>$d->kodepemda,
                                'data_bu'=>[]
                            ];
                        }

                        $data_lst[$d->kodepemda]['data_bu'][$d->kodebu]=$d;

                    }

                    foreach($data_lst as $k=>$d){
                        $data_lst[$k]['data_bu']=array_values($d['data_bu']);
                    }
                    $data_lst=array_values($data_lst);
                    foreach($data_lst as $k=>$d){
                        $data_lst[$k]['permissions']=$pemisions['permissions'];
                    }
                }

              
                return $data_lst;
            }
        }

        return [];


    }
   
}
