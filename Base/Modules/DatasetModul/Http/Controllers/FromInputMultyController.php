<?php

namespace Modules\DatasetModul\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use App\Http\Controllers\FilterCtrl;
use Auth;
use Carbon\Carbon;
class FromInputMultyController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

    public function loadDataPemda($tahun,$id_dataset,$kodepemda,Request $request){
        $dataset=DB::table('master.dataset as dts')->find($id_dataset);
        if($dataset){
            $select=($request->columns??[]);
            if($request->guide){
                $select=[];
                $c=DB::table('master.dataset_data as dd')
                ->join('master.data as d','d.id','=','dd.id_data')
                ->selectRaw("dd.field_name")
                ->where([
                    'dd.id_dataset'=>$id_dataset,
                ])->whereIn('dd.id_data',$request->columns??[])
                ->get();
    
                foreach($c as $cc){
                    $select[]=$cc->field_name;
                }
            }
           
           
            $status_data='dts_'.$dataset->id;
            if(str_contains($dataset->table,'[TAHUN]')){
                $status_data.='_'.$tahun;
                $dataset->table=str_replace('[TAHUN]',$tahun,$dataset->table);
            }
            
            $exist=DB::table('information_schema.tables'
            )->whereIn('table_schema',['dataset','status_data'])
            ->whereIn('table_name',[$dataset->table,$status_data])
            ->count();
    
            if($exist!=2){
                return [];
            }
            if($exist==2){
                if(strtoupper($dataset->klasifikasi_2)=='DATA PEMDA'){

                }else{
                    $data=DB::table('dataset.'.$dataset->table.' as d')
                            ->where([
                                'kodepemda'=>DB::raw("'".$kodepemda."'"),
                                'tw'=>4,
                                'tahun'=>$tahun
                            ])->orderBy('d.index','asc');

                        $length = $request->input('length');
                        $kodepemda = $request->input('kodepemda');
                        $orderBy = $request->input('column','d.index'); //Index
                        $orderByDir = $request->input('dir', 'asc');
                        $searchValue = $request->input('search');
                       

                        if($searchValue){
                        // $where=[];
                        //     $where[]="(x.nama_pemda::text ilike '%".$searchValue."%')"; 
                        //     $data=$data->whereRaw(implode(' or ',$where));
                        }

                        if($orderBy=='id'){
                        $orderBy='d.index';
                        }else{
                        $orderBy='d.'.$orderBy;
                        }

                        if($orderBy){
                        
                        $data=$data->orderBy($orderBy,$orderByDir);
                        }
                

                        $data = $data->paginate($length);
                        $data=[
                            'data'=>$data->items(),
                            'links'=>$data->links(),
                            'meta'=>$data,
                            'payload'=>$request->all()
                        ];
                        
                        return ($data);

                }
                
            }
        }

    }
    
    public function loadData($tahun,$id_dataset,$kodepemda,$kodebu,Request $request){
        $dataset=DB::table('master.dataset as dts')->find($id_dataset);
        if($dataset){
            $select=($request->columns??[]);
            if($request->guide){
                $select=[];
                $c=DB::table('master.dataset_data as dd')
                ->join('master.data as d','d.id','=','dd.id_data')
                ->selectRaw("dd.field_name")
                ->where([
                    'dd.id_dataset'=>$id_dataset,
                ])->whereIn('dd.id_data',$request->columns??[])
                ->get();
    
                foreach($c as $cc){
                    $select[]=$cc->field_name;
                }
            }
           
           
            $status_data='dts_'.$dataset->id;
            if(str_contains($dataset->table,'[TAHUN]')){
                $status_data.='_'.$tahun;
                $dataset->table=str_replace('[TAHUN]',$tahun,$dataset->table);
            }
            
            $exist=DB::table('information_schema.tables'
            )->whereIn('table_schema',['dataset','status_data'])
            ->whereIn('table_name',[$dataset->table,$status_data])
            ->count();
    
            if($exist!=2){
                return [];
            }
            if($exist==2){
                if(strtoupper($dataset->klasifikasi_2)=='DATA PEMDA'){

                }else{
                    $data=DB::table('dataset.'.$dataset->table.' as d')
                            ->where([
                                'kodepemda'=>DB::raw("'".$kodepemda."'"),
                                'kodebu'=>$kodebu,
                                'tw'=>4,
                                'tahun'=>$tahun
                            ])->orderBy('d.index','asc');

                        $length = $request->input('length');
                        $kodepemda = $request->input('kodepemda');
                        $orderBy = $request->input('column','d.index'); //Index
                        $orderByDir = $request->input('dir', 'asc');
                        $searchValue = $request->input('search');
                       

                        if($searchValue){
                        // $where=[];
                        //     $where[]="(x.nama_pemda::text ilike '%".$searchValue."%')"; 
                        //     $data=$data->whereRaw(implode(' or ',$where));
                        }

                        if($orderBy=='id'){
                        $orderBy='d.index';
                        }else{
                        $orderBy='d.'.$orderBy;
                        }

                        if($orderBy){
                        
                        $data=$data->orderBy($orderBy,$orderByDir);
                        }
                

                        $data = $data->paginate($length);
                        $data=[
                            'data'=>$data->items(),
                            'links'=>$data->links(),
                            'meta'=>$data,
                            'payload'=>$request->all()
                        ];
                        
                        return ($data);

                }
                
            }
        }

    }
    

    public function LoadBu($tahun,$id_dataset,$kodepemda,$kodebu=null,Request $request){
        $dataset=DB::table('master.dataset as dts')->find($id_dataset);
        if($dataset){
            $select=($request->columns??[]);
            if($request->guide){
                $select=[];
                $c=DB::table('master.dataset_data as dd')
                ->join('master.data as d','d.id','=','dd.id_data')
                ->selectRaw("dd.field_name")
                ->where([
                    'dd.id_dataset'=>$id_dataset,
                ])->whereIn('dd.id_data',$request->columns??[])
                ->get();
    
                foreach($c as $cc){
                    $select[]=$cc->field_name;
                }
            }
           
           
            $status_data='dts_'.$dataset->id;
            if(str_contains($dataset->table,'[TAHUN]')){
                $status_data.='_'.$tahun;
                $dataset->table=str_replace('[TAHUN]',$tahun,$dataset->table);
            }
            
            $exist=DB::table('information_schema.tables'
            )->whereIn('table_schema',['dataset','status_data'])
            ->whereIn('table_name',[$dataset->table,$status_data])
            ->count();
    
            if($exist!=2){
                return [];
            }
            if($exist==2){
                if(strtoupper($dataset->klasifikasi_2)=='DATA PEMDA'){

                }else{
                    $bu=DB::table('master.master_bu as bu')
                    ->join('master.master_bu_pemda as bup','bup.id_bu','=','bu.id')
                    ->where([
                        'bup.kodepemda'=>$kodepemda,
                    
                    ])->orderBy('bu.id','asc')->get();
                    $data=[];
                    foreach($bu as $k=>$b){
                        $data[]=[
                            'kodebu'=>$b->id,
                            'nama_bu'=>$b->nama_bu,
                            'url'=>route('api-web-mod.dataset.form-handle.multy.get-exist-data',['tahun'=>$tahun,'id_dataset'=>$id_dataset,'kodepemda'=>$kodepemda,'kodebu'=>$b->id]),
                        ];    
                        
                    }
                    return $data;
                }
                
            }
        }
    }
   public function detail($tahun,$id_dataset,$kodepemda,Request $request){

    $dataset=DB::table('master.dataset as dts')->find($id_dataset);
    if($dataset){
        $select=($request->columns??[]);
        if($request->guide){
            $select=[];
            $c=DB::table('master.dataset_data as dd')
            ->join('master.data as d','d.id','=','dd.id_data')
            ->selectRaw("dd.field_name")
            ->where([
                'dd.id_dataset'=>$id_dataset,
            ])->whereIn('dd.id_data',$request->columns??[])
            ->get();

            foreach($c as $cc){
                $select[]=$cc->field_name;
            }
        }
       
       
        $status_data='dts_'.$dataset->id;
        if(str_contains($dataset->table,'[TAHUN]')){
            $status_data.='_'.$tahun;
            $dataset->table=str_replace('[TAHUN]',$tahun,$dataset->table);
        }
        
        $exist=DB::table('information_schema.tables'
        )->whereIn('table_schema',['dataset','status_data'])
        ->whereIn('table_name',[$dataset->table,$status_data])
        ->count();

        if($exist!=2){
            return [];
        }
        if($exist==2){
            $pemda=DB::table('master.master_pemda')->where('kodepemda',$kodepemda)->first();
            $columns=[];
            $bu=DB::table('master.master_bu as bu')
            ->selectRaw("bu.*,bu.id as kodebu")
            ->join('master.master_bu_pemda as bup','bup.id_bu','=','bu.id')
            ->where('bup.kodepemda',$kodepemda)
            ->orderBy('bu.id','asc')
            ->get();
            foreach($bu as $k=>$b){
                $bu[$k]->data=[];
            }

            if(strtoupper($dataset->klasifikasi_1)=='DATA PEMDA'){
                return view('datasetmodul::form.partial_multy.multy_pemda',['pemda'=>$pemda,'dataset'=>$dataset,'columns'=>$columns,'bu'=>$bu]);

            }else{
                return view('datasetmodul::form.partial_multy.multy_bu',['pemda'=>$pemda,'dataset'=>$dataset,'columns'=>$columns,'bu'=>$bu]);
            }

        }
    }

   }
   
}
