<?php

namespace Modules\DatasetModul\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use Auth;
use Alert;
use Carbon\Carbon;
use Validator;
use Storage;
use Artisan;
use App\Models\User;
use App\Http\Controllers\FilterCtrl;



class MasterController extends Controller
{

    static $type=0;
    static $meta=[
        'title'=>'Dataset',
        'keterangan'=>''
    ];

    function __construct($type=null) {
        if($type){
            static::$type=$type;

        }
    }

    public function list_column_dataset($id_dataset,Request $request){
        $data=DB::table('master.data as d')
        ->join('master.dataset_data as sd','sd.id_data','=','d.id')
        ->selectRaw('d.name,d.type,d.satuan,d.definisi_konsep,sd.*')
        ->where('sd.id_dataset',$id_dataset)
        ->orderBy('sd.index')
        ->get();

        return $data;

    }

    public function updateDtasetSub($tahun,Request $request){
        $My=Auth::User();
        $dts_access=FilterCtrl::getPemissionsDataset($request);
        if(in_array($request->id_dataset,$dts_access['dataset_id'])){
            if($request->status){
                DB::table('analisa.dataset_subs')->updateOrInsert([
                    'dataset_id'=>$request->id_dataset,
                    'user_id'=>$My->id
                ],
                [
                    'dataset_id'=>$request->id_dataset,
                    'user_id'=>$My->id
                ]
                );
            }else{
                DB::table('analisa.dataset_subs')->where([
                    'dataset_id'=>$request->id_dataset,
                    'user_id'=>$My->id
                ]
                )->delete();
            }
        }
    }

    public function getDtasetSub($tahun,Request $request){
        $My=Auth::User();
        $regional=null;
        if($My->can('is_admin')){
            if($request->user){
                $My=DB::table('public.users')->find($request->user);

            }
        }else{
            if($My->regional){
                $regional=$My->regional;
            }

        }

        $dts_access=FilterCtrl::getPemissionsDataset($request);
        $subcribe=DB::table('master.dataset as dts')
        ->leftJoin('analisa.dataset_subs as sub',[['sub.dataset_id','=','dts.id'],['sub.user_id','=',DB::raw($My->id)]])
        ->selectRaw("dts.id,dts.name,dts.tujuan,(case when (sub.id is not null) then true else false end) as subscribe_status")
        ->orderBy('dts.admin_menu','asc')
        ->whereIn('dts.id',$dts_access['dataset_id']??[0])
        ->orderBy('dts.index','asc')
        ->get();

        DB::table('analisa.dataset_subs')->where('user_id',$My->id)->whereNotIn('dataset_id',$dts_access['dataset_id']??[0])->delete();
        foreach($subcribe as $k=>$s){
            $subcribe[$k]->regional_only=$regional;
        }

        return $subcribe;

    }

    public function list_access($tahun,Request $request){
        if(static::$type){
            static::$meta['title']='Dataset Dokumen Pendukung';
        }
        $page_meta=[
            'title'=>static::$meta['title'],
            'keterangan'=>static::$meta['keterangan'],
        ];


        $dts_access=FilterCtrl::getPemissionsDataset($request);


        $dataset=DB::table('master.dataset as d')
        ->leftJoin('kpi_data_monev as km','km.id_data','=','d.id')
        ->leftJoin('master.menus as menu','menu.id','=','d.id_menu')
        ->leftJoin('kpi as kpi','kpi.id','=','km.id_kpi')
        ->select('d.id','menu.title as menu',DB::raw("(case when kpi.name IS NOT NULL then kpi.name else 'LAINYA' end) as kpi"),'d.name','d.jenis_dataset','d.login','d.klasifikasi_1','d.klasifikasi_2','d.table')
        ->whereIn('d.id',$dts_access['dataset_id'])
        ->where('d.admin_menu',static::$type)
        ->orderBy('kpi.name','asc')
        ->orderBy('d.index','asc')
        ->get();




        $filter=FilterCtrl::buildFilterPemda($request);
        $pemdas=FilterCtrl::filter_pemda($filter['regional']??null,$filter['lokus'],$filter['sortlist'],$filter['tipe_bantuan'],$filter['jenis_pemda'],$filter['pemda']);


        $dataset_l=[];

        $component_load=[];
        foreach($dataset as $k=>$d){
            $filter_x=$filter;
            $pemdasx=$pemdas;
            if($d->klasifikasi_2!='[]'){
                $filter_x['tipe_bantuan']=json_decode(str_replace('BANTUAN ','',strtoupper($d->klasifikasi_2)),true);
                $pemdasx=FilterCtrl::filter_pemda($filter_x['regional']??null,$filter_x['lokus'],$filter_x['sortlist'],$filter_x['tipe_bantuan'],$filter_x['jenis_pemda'],$filter_x['pemda']);
            }
            $pemition=FilterCtrl::getPemissionsDataset($request,$d->id)['permissions'];

            $tb=$d->table;
            $st='dts_'.$d->id;
            if(str_contains($d->table,"[TAHUN]")){
                $tb=str_replace("[TAHUN]",$tahun,$d->table);
                $st.='_'.$tahun;
            }

            $exist_table=(int)DB::table('pg_tables')->whereIn('tablename',[$tb,$st])->whereIn('schemaname',['dataset','status_data'])->count();
            if($exist_table>=2){
                $state=DB::table('dataset.'.$tb." as d")
                ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
                ->leftJoin('status_data.'.$st.' as std',[['std.kodepemda','=','d.kodepemda'],['std.tahun','=','d.tahun']])
                ->whereIn('d.kodepemda',$pemdasx)
                ->where('d.tahun','=',$tahun)
                ->selectRaw("count(distinct(d.kodepemda)) as terdata,count(DISTINCT(case when (std.status =2) then d.kodepemda else null end)) as tervalidasi,
                count(DISTINCT(case when (std.status in (1,2)) then d.kodepemda else null end)) as terverifikasi");

                // if($dataset[$k]->id==121){
                //     dd($state->first());
                // }else{
                    $state=$state->first();
                // }


                $dataset[$k]->status_data=[
                    'terdata'=>$state?$state->terdata:0,
                    'terverifikasi'=>$state?$state->terverifikasi:0,
                    'tervalidasi'=>$state?$state->tervalidasi:0,
                    'jumlah_pemda'=>count($pemdasx),
                    'tahun'=>$tahun
                ];

                $dataset[$k]->permissions=$pemition;
                $com=\Modules\DatasetModul\Http\Controllers\FromInputController::componentLoad($d);
                $dataset[$k]->components_load=$com;
                $dataset[$k]->roles=User::find(Auth::id())->getRoleNames();
                foreach($com as $c){
                    $component_load[]=$c;
                }
                $dataset_l[]=$dataset[$k];

            }



        }
        return view('datasetmodul::form_access.index')->with(
            [
                'page_meta'=>$page_meta,
                'data'=>array_values($dataset_l),
                'pemdas'=>$pemdas,
                're'=>$request,
                'component_load'=>$component_load,
                'direct_pemda'=>$filter['direct_pemda']
            ]
        );


    }


    public function delete($tahun,$id){
        DB::table('master.dataset')->where('id',$id)->delete();
        Alert::success('','Berhasil Menghapus dataset');
        return redirect()->route('mod.dataset.schema.index',['tahun'=>$tahun]);
    }

    public static function nameVar($var,$m=1){
        if($m==0){
            $var=  preg_replace('/_+/', '_',(preg_replace('/[^\p{L}\p{N}\s]/u', '_',str_replace(' ','_', $var))));

        }else if($m==1){
            $var=  preg_replace('/\s+/', ' ',(preg_replace('/[^\p{L}\p{N}\s]/u', ' ', $var)));
            $var=str_replace(' ','',ucwords(strtolower(str_replace('_',' ',$var))));
        }

        return str_replace('[TAHUN]','multy_years',$var);

    }

    public static function migrate_dataset($file){
        Artisan::call('migrate --path=database/dataset_migrations/'.$file.' ');
        DB::table('public.migrations')->where('migration',str_replace('.php','',$file))->delete();

    }

    static function build_schema_status_data($name_index){
        $class_name=static::nameVar($name_index);
        $file='<?php
        use Illuminate\Database\Migrations\Migration;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Support\Facades\Schema;
        use Doctrine\DBAL\Types\FloatType;
        use Doctrine\DBAL\Types\Type;

        class '.$class_name." extends Migration
        {
            public function up(){
                if(!Schema::hasTable('status_data.".$name_index."')){
                    Schema::create('status_data.".$name_index."',function(Blueprint $"."table){
                        $"."table->id();
                        $"."table->string('kodepemda',5);
                        $"."table->integer('tahun');
                        $"."table->integer('status')->default(0);
                        $"."table->bigInteger('user_validate')->unsigned()->nullable();
                        $"."table->bigInteger('user_verified')->unsigned()->nullable();
                        $"."table->dateTime('validate_date')->nullable();
                        $"."table->dateTime('verified_date')->nullable();
                        $"."table->unique(['kodepemda','tahun'],'".$name_index."_uniques');

                        $"."table->foreign('user_validate')
                        ->references('id')
                        ->on('public.users')
                        ->onDelete('cascade')
                        ->onUpdate('cascade');

                        $"."table->foreign('user_verified')
                        ->references('id')
                        ->on('public.users')
                        ->onDelete('cascade')
                        ->onUpdate('cascade');

                        $"."table->foreign('kodepemda')
                        ->references('kodepemda')
                        ->on('master.master_pemda')
                        ->onDelete('cascade')
                        ->onUpdate('cascade');

                    });
                }

            }

            public function down(){

            }
        }
        ";

        $file_schema_named=date('Y_m_d_h0000').'_'.strtolower(str_replace('@_','',preg_replace('/[A-Z]/D', '_$0', '@'.$class_name))).'.php';

        Storage::disk('dataset')->put($file_schema_named,$file);
        return [
            'name'=>$file_schema_named,
            'path'=>Storage::disk('dataset')->url($file_schema_named)

        ];



    }

    static function build_schema($name_index,$tahun,$table,$datum,$jenis=0,$list_data_recorded=[]){
        $index_aggre=array_filter($datum,function($el){  return ($el['aggregasi']!=null and $el['aggregasi']!='0');});
        $index_aggre=array_keys($index_aggre);
        $file='<?php


        use Illuminate\Database\Migrations\Migration;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Support\Facades\Schema;
        use Doctrine\DBAL\Types\FloatType;
        use Doctrine\DBAL\Types\Type;

        class '.static::nameVar($table).' extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {';

                $file.= "
                    if (!Type::hasType('double')) {
                        Type::addType('double', FloatType::class);
                    }
                    if(!Schema::hasTable('dataset.".$table."')){
                        Schema::create('dataset.".$table."',function(Blueprint $"."table){
                        $"."table->id();
                        $"."table->integer('index')->nullable()->default(0)->index();
                        $"."table->string('kodepemda',5)->index('".$name_index."_idx_kodepemda');
                        $"."table->integer('tahun')->index('".$name_index."_idx_tahun');
                        $"."table->integer('tw')->index('".$name_index."_idx_tw');
                        $"."table->integer('status')->default(0)->index('".$name_index."_idx_status');
                        $"."table->float('point')->default(0);
                        ";

                        $file.="
                        $"."table->mediumText('keterangan')->nullable();
                        ".
                        "$"."table->bigInteger('user_pengisi')->unsigned();
                        ".
                        "$"."table->dateTime('tanggal_pengisian')->nullable();
                        ".
                        "$"."table->bigInteger('user_pengesah')->unsigned()->nullable();
                        ".
                        "$"."table->dateTime('tanggal_pengesahan')->nullable();
                        ".
                        "$"."table->bigInteger('user_pengesah_2')->unsigned()->nullable();
                        ".
                        "$"."table->dateTime('tanggal_pengesahan_2')->nullable();
                        ".
                        "$"."table->bigInteger('user_pengesah_3')->unsigned()->nullable();
                        ".
                        "$"."table->dateTime('tanggal_pengesahan_3')->nullable();
                        ".
                        "$"."table->dateTime('updated_at')->nullable();
                        ;";
                        foreach($datum as $d){
                            if($d['tipe_nilai']=='numeric'){
                                $file.="$"."table->double('".$d['new_field_name']."',25,3)->nullable()->comment('".$d['id_data'].'-'.substr($d['name'],0,25).'-'.$d['satuan']."');
                            ";
                            }elseif($d['tipe_nilai']=='string' AND ($d['type']=='single-file')){
                                $file.="$"."table->string('".$d['new_field_name']."')->nullable()->comment('".$d['id_data'].'-'.substr($d['name'],0,25).'-'.$d['satuan']."');
                            ";
                            }elseif($d['tipe_nilai']=='string' OR ($d['type']=='multy-file')){
                                $file.="$"."table->mediumText('".$d['new_field_name']."')->nullable()->comment('".$d['id_data'].'-'.substr($d['name'],0,25).'-'.$d['satuan']."');
                            ";
                            }

                        }
                        if($jenis==0){
                            $file.="
                            $"."table->unique(['kodepemda','tahun','tw'],'".$name_index."_unique_single');
                            $"."table->index(['kodepemda','tahun','tw','status'],'".$name_index."_idx_composit');
                            $"."table->index(['kodepemda','tahun','tw','status'],'".$name_index."_idx_composit_pemda');
                            ";
                        }else{
                            $file.="
                            $"."table->index(['kodepemda','tahun','tw','status'],'".$name_index."_idx_composit');
                            $"."table->index(['kodepemda','tahun','tw','status'],'".$name_index."_idx_composit_pemda');

                            ";

                        }



                        $file.="});

                    }";



                    if(count($list_data_recorded)){
                    $file.="
                    if(Schema::hasTable('dataset.".$table."')){
                        Schema::table('dataset.".$table."',function (Blueprint $"."table){
                            ";

                            foreach($list_data_recorded as $d){
                                $file.="if(Schema::hasColumn('dataset.".$table."', '".$d['field_name']."')) {
                                            $"."table->renameColumn('".$d['field_name']."', '".$d['new_field_name']."');
                                            ";
                                $file.="}";
                            }

                    $file.="
                        });
                    }";
                    }

                    $file.="

                    if(Schema::hasTable('dataset.".$table."')){
                        Schema::table('dataset.".$table."',function (Blueprint $"."table){
                            ";
                            $file.="if(!Schema::hasColumn('dataset.".$table."', 'updated_at')) {
                                $"."table->dateTime('updated_at')->nullable();
                            }
                            ";
                            foreach($datum as $d){
                                $file.="if(!Schema::hasColumn('dataset.".$table."', '".$d['new_field_name']."')) {
                                ";
                               
                                    if($d['tipe_nilai']=='numeric'){
                                        $file.="$"."table->double('".$d['new_field_name']."',25,3)->nullable()->comment('".$d['id_data'].'-'.substr($d['name'],0,25).'-'.$d['satuan']."');
                                    ";
                                    }elseif($d['tipe_nilai']=='string' AND ($d['type']=='single-file')){
                                        $file.="$"."table->string('".$d['new_field_name']."')->nullable()->comment('".$d['id_data'].'-'.substr($d['name'],0,25).'-'.$d['satuan']."');
                                    ";
                                    }elseif($d['tipe_nilai']=='string' OR ($d['type']=='multy-file')){
                                        $file.="$"."table->mediumText('".$d['new_field_name']."')->nullable()->comment('".$d['id_data'].'-'.substr($d['name'],0,25).'-'.$d['satuan']."');
                                    ";
                                    }elseif($d['tipe_nilai']=='single-file'){
                                        $file.="$"."table->string('".$d['new_field_name']."')->nullable()->comment('".$d['id_data'].'-'.substr($d['name'],0,25).'-'.$d['satuan']."');";

                                    }elseif(($d['tipe_nilai']=='multy-file')){
                                        $file.="$"."table->mediumText('".$d['new_field_name']."')->nullable()->comment('".$d['id_data'].'-'.substr($d['name'],0,25).'-'.$d['satuan']."');";
                                    }
                                $file.="
                                    }else{";

                                        if($d['tipe_nilai']=='numeric'){
                                            $file.="$"."table->double('".$d['new_field_name']."',25,3)->nullable()->comment('".$d['id_data'].'-'.substr($d['name'],0,25).'-'.$d['satuan']."')->change();
                                        ";
                                        }elseif($d['tipe_nilai']=='string' AND ($d['type']=='single-file')){
                                            $file.="$"."table->string('".$d['new_field_name']."')->nullable()->comment('".$d['id_data'].'-'.substr($d['name'],0,25).'-'.$d['satuan']."')->change();
                                        ";
                                        }elseif($d['tipe_nilai']=='string' OR ($d['type']=='multy-file')){
                                            $file.="$"."table->mediumText('".$d['new_field_name']."')->nullable()->comment('".$d['id_data'].'-'.substr($d['name'],0,25).'-'.$d['satuan']."')->change();
                                        ";
                                        } elseif($d['tipe_nilai']=='single-file'){
                                            $file.="$"."table->string('".$d['new_field_name']."')->nullable()->comment('".$d['id_data'].'-'.substr($d['name'],0,25).'-'.$d['satuan']."')->change();";
                                           
                                        }elseif(($d['tipe_nilai']=='multy-file')){
                                           $file.="$"."table->mediumText('".$d['new_field_name']."')->nullable()->comment('".$d['id_data'].'-'.substr($d['name'],0,25).'-'.$d['satuan']."')->change();
                                        "; 
                                        }
                                $file.="
                                }
                                ";
                            }
                            $file.="
                        });
                        ";

                        $file.="
                        if(Schema::hasTable('dataset.".$table."')){
                            Schema::table('dataset.".$table."',function (Blueprint $"."table){
                                $"."sm = Schema::getConnection()->getDoctrineSchemaManager();
                                $"."indexesFound = $"."sm->listTableIndexes('dataset.".$table."');
                                ";

                                $file.="if(!array_key_exists('".$name_index."_idx_kodepemda', $"."indexesFound)){";
                                    $file.="$"."table->index(['kodepemda'],'".$name_index."_idx_kodepemda');";
                                $file.=" }";
                                $file.="if(!array_key_exists('".$name_index."_idx_tahun', $"."indexesFound)){";
                                    $file.="$"."table->index(['tahun'],'".$name_index."_idx_tahun');";
                                $file.=" }";
                                $file.="if(!array_key_exists('".$name_index."_idx_tw', $"."indexesFound)){";
                                    $file.="$"."table->index(['tw'],'".$name_index."_idx_tw');";
                                $file.=" }";
                                $file.="if(!array_key_exists('".$name_index."_idx_status', $"."indexesFound)){";
                                    $file.="$"."table->index(['status'],'".$name_index."_idx_status');";
                                $file.=" }";
                                $file.="if(!array_key_exists('".$name_index."_idx_composit', $"."indexesFound)){";
                                    $file.="$"."table->index(['kodepemda','tahun','tw','status'],'".$name_index."_idx_composit');";
                                $file.=" }";
                                if($jenis==0){
                                    // check not exist

                                    $file.="if(array_key_exists('".$name_index."_unique_multy_extend', $"."indexesFound)){";
                                        $file.="$"."table->dropUnique('".$name_index."_unique_multy_extend');";
                                    $file.=" }";

                                    $file.="if(!array_key_exists('".$name_index."_unique_single', $"."indexesFound)){";
                                        $file.="$"."table->unique(['kodepemda','tahun','tw'],'".$name_index."_unique_single');";
                                    $file.=" }";


                                }else{

                                    // $file.="if(array_key_exists('".$name_index."_unique_multy_extend', $"."indexesFound)){";
                                    //     $file.="$"."table->dropUnique('".$name_index."_unique_multy_extend');";
                                    // $file.=" }";

                                    $file.="if(array_key_exists('".$name_index."_unique_single', $"."indexesFound)){";
                                        $file.="$"."table->dropUnique('".$name_index."_unique_single');";
                                    $file.=" }";


                                }

                        $file.="
                            });
                        }";

                        foreach($index_aggre as $i){
                            $file.="DB::statement('CREATE INDEX IF NOT EXISTS ".$i.'_idx'." ON dataset.".'"'.$table.'"'." USING btree (".$i.")');
                            ";
                        }
                        $file.="
                    }

            }



            public function down(){



            }

        }";

        $file_schema_named=date('Y_m_d_h0000').'_'.strtolower(str_replace('@_','',preg_replace('/[A-Z]/D', '_$0', '@'.static::nameVar($table)))).'.php';

        Storage::disk('dataset')->put($file_schema_named,$file);
        return [
            'name'=>$file_schema_named,
            'path'=>Storage::disk('dataset')->url($file_schema_named)

        ];
    }

    static function build_schema_bu($name_index,$tahun,$table,$datum,$jenis=0,$list_data_recorded=[]){
        $index_aggre=array_filter($datum,function($el){  return ($el['aggregasi']!=null and $el['aggregasi']!='0');});

        $index_aggre=array_keys($index_aggre);
        $file='<?php


        use Illuminate\Database\Migrations\Migration;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Support\Facades\Schema;
        use Doctrine\DBAL\Types\FloatType;
        use Doctrine\DBAL\Types\Type;

        class '.static::nameVar($table).' extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {';

                $file.= "
                    if (!Type::hasType('double')) {
                        Type::addType('double', FloatType::class);
                    }
                    if(!Schema::hasTable('dataset.".$table."')){
                        Schema::create('dataset.".$table."',function(Blueprint $"."table){
                        $"."table->id();
                        $"."table->integer('index')->nullable()->default(0)->index();
                        $"."table->string('kodepemda',5)->index('".$name_index."_idx_kodepemda');
                        $"."table->bigInteger('kodebu')->index('".$name_index."_idx_kodebu');
                        $"."table->integer('tahun')->index('".$name_index."_idx_tahun');
                        $"."table->integer('tw')->index('".$name_index."_idx_tw');
                        $"."table->integer('status')->default(0)->index('".$name_index."_idx_status');
                        $"."table->float('point')->default(0);
                        ";

                        $file.="
                        $"."table->mediumText('keterangan')->nullable();
                        ".
                        "$"."table->bigInteger('user_pengisi')->unsigned();
                        ".
                        "$"."table->dateTime('tanggal_pengisian')->nullable();
                        ".
                        "$"."table->bigInteger('user_pengesah')->unsigned()->nullable();
                        ".
                        "$"."table->dateTime('tanggal_pengesahan')->nullable();
                        ".
                        "$"."table->bigInteger('user_pengesah_2')->unsigned()->nullable();
                        ".
                        "$"."table->dateTime('tanggal_pengesahan_2')->nullable();
                        ".
                        "$"."table->bigInteger('user_pengesah_3')->unsigned()->nullable();
                        ".
                        "$"."table->dateTime('tanggal_pengesahan_3')->nullable();
                        ".
                        "$"."table->dateTime('updated_at')->nullable();
                        ;";
                        foreach($datum as $d){
                            if($d['tipe_nilai']=='numeric'){
                                $file.="$"."table->double('".$d['new_field_name']."',25,3)->nullable()->comment('".$d['id_data'].'-'.substr($d['name'],0,25).'-'.$d['satuan']."');
                            ";
                            }elseif($d['tipe_nilai']=='string' AND ($d['type']=='single-file')){
                                $file.="$"."table->string('".$d['new_field_name']."')->nullable()->comment('".$d['id_data'].'-'.substr($d['name'],0,25).'-'.$d['satuan']."');
                            ";
                            }elseif($d['tipe_nilai']=='string' OR ($d['type']=='multy-file')){
                                $file.="$"."table->mediumText('".$d['new_field_name']."')->nullable()->comment('".$d['id_data'].'-'.substr($d['name'],0,25).'-'.$d['satuan']."');
                            ";
                            }

                        }
                        if($jenis==0){
                            $file.="
                            $"."table->unique(['kodepemda','tahun','tw','kodebu'],'".$name_index."_unique_single');
                            $"."table->index(['kodepemda','tahun','tw','status','kodebu'],'".$name_index."_idx_composit');
                            $"."table->index(['kodepemda','tahun','tw','status'],'".$name_index."_idx_composit_pemda');
                            ";
                        }else{
                            $file.="
                            $"."table->index(['kodepemda','tahun','tw','status','kodebu'],'".$name_index."_idx_composit');
                            $"."table->index(['kodepemda','tahun','tw','status'],'".$name_index."_idx_composit_pemda');

                            ";

                        }



                        $file.="});

                    }";



                    if(count($list_data_recorded)){
                    $file.="
                    if(Schema::hasTable('dataset.".$table."')){
                        Schema::table('dataset.".$table."',function (Blueprint $"."table){
                            if(!Schema::hasColumn('dataset".$table."','kodebu')){
                                $"."table->bigInteger('kodebu')->index('".$name_index."_idx_kodebu');
                            }
                            ";


                            foreach($list_data_recorded as $d){
                                $file.="if(Schema::hasColumn('dataset.".$table."', '".$d['field_name']."')) {
                                            $"."table->renameColumn('".$d['field_name']."', '".$d['new_field_name']."');
                                            ";
                                $file.="}";
                            }

                    $file.="
                        });
                    }";
                    }

                    $file.="

                    if(Schema::hasTable('dataset.".$table."')){
                        Schema::table('dataset.".$table."',function (Blueprint $"."table){
                            ";
                            $file.="if(!Schema::hasColumn('dataset.".$table."', 'updated_at')) {
                                $"."table->dateTime('updated_at')->nullable();
                            }
                            ";
                            foreach($datum as $d){
                                $file.="if(!Schema::hasColumn('dataset.".$table."', '".$d['new_field_name']."')) {
                                ";
                               
                                    if($d['tipe_nilai']=='numeric'){
                                        $file.="$"."table->double('".$d['new_field_name']."',25,3)->nullable()->comment('".$d['id_data'].'-'.substr($d['name'],0,25).'-'.$d['satuan']."');
                                    ";
                                    }elseif($d['tipe_nilai']=='string' AND ($d['type']=='single-file')){
                                        $file.="$"."table->string('".$d['new_field_name']."')->nullable()->comment('".$d['id_data'].'-'.substr($d['name'],0,25).'-'.$d['satuan']."');
                                    ";
                                    }elseif($d['tipe_nilai']=='string' OR ($d['type']=='multy-file')){
                                        $file.="$"."table->mediumText('".$d['new_field_name']."')->nullable()->comment('".$d['id_data'].'-'.substr($d['name'],0,25).'-'.$d['satuan']."');
                                    ";
                                    }elseif($d['tipe_nilai']=='single-file'){
                                        $file.="$"."table->string('".$d['new_field_name']."')->nullable()->comment('".$d['id_data'].'-'.substr($d['name'],0,25).'-'.$d['satuan']."');";

                                    }elseif(($d['tipe_nilai']=='multy-file')){
                                        $file.="$"."table->mediumText('".$d['new_field_name']."')->nullable()->comment('".$d['id_data'].'-'.substr($d['name'],0,25).'-'.$d['satuan']."');";
                                    }
                                $file.="
                                    }else{";

                                        if($d['tipe_nilai']=='numeric'){
                                            $file.="$"."table->double('".$d['new_field_name']."',25,3)->nullable()->comment('".$d['id_data'].'-'.substr($d['name'],0,25).'-'.$d['satuan']."')->change();
                                        ";
                                        }elseif($d['tipe_nilai']=='string' AND ($d['type']=='single-file')){
                                            $file.="$"."table->string('".$d['new_field_name']."')->nullable()->comment('".$d['id_data'].'-'.substr($d['name'],0,25).'-'.$d['satuan']."')->change();
                                        ";
                                        }elseif($d['tipe_nilai']=='string' OR ($d['type']=='multy-file')){
                                            $file.="$"."table->mediumText('".$d['new_field_name']."')->nullable()->comment('".$d['id_data'].'-'.substr($d['name'],0,25).'-'.$d['satuan']."')->change();
                                        ";
                                        } elseif($d['tipe_nilai']=='single-file'){
                                            $file.="$"."table->string('".$d['new_field_name']."')->nullable()->comment('".$d['id_data'].'-'.substr($d['name'],0,25).'-'.$d['satuan']."')->change();";
                                           
                                        }elseif(($d['tipe_nilai']=='multy-file')){
                                           $file.="$"."table->mediumText('".$d['new_field_name']."')->nullable()->comment('".$d['id_data'].'-'.substr($d['name'],0,25).'-'.$d['satuan']."')->change();
                                        "; 
                                        }
                                $file.="
                                }
                                ";
                            }
                            $file.="
                        });
                        ";

                        $file.="
                        if(Schema::hasTable('dataset.".$table."')){
                            Schema::table('dataset.".$table."',function (Blueprint $"."table){
                                $"."sm = Schema::getConnection()->getDoctrineSchemaManager();
                                $"."indexesFound = $"."sm->listTableIndexes('dataset.".$table."');
                                ";

                                $file.="if(!array_key_exists('".$name_index."_idx_kodepemda', $"."indexesFound)){";
                                    $file.="$"."table->index(['kodepemda'],'".$name_index."_idx_kodepemda');";
                                $file.=" }";
                                $file.="if(!array_key_exists('".$name_index."_idx_tahun', $"."indexesFound)){";
                                    $file.="$"."table->index(['tahun'],'".$name_index."_idx_tahun');";
                                $file.=" }";
                                $file.="if(!array_key_exists('".$name_index."_idx_tw', $"."indexesFound)){";
                                    $file.="$"."table->index(['tw'],'".$name_index."_idx_tw');";
                                $file.=" }";
                                $file.="if(!array_key_exists('".$name_index."_idx_status', $"."indexesFound)){";
                                    $file.="$"."table->index(['status'],'".$name_index."_idx_status');";
                                $file.=" }";
                                $file.="if(!array_key_exists('".$name_index."_idx_composit', $"."indexesFound)){";
                                    $file.="$"."table->index(['kodepemda','tahun','tw','status'],'".$name_index."_idx_composit');";
                                $file.=" }";
                                if($jenis==0){
                                    // check not exist

                                    $file.="if(array_key_exists('".$name_index."_unique_multy_extend', $"."indexesFound)){";
                                        $file.="$"."table->dropUnique('".$name_index."_unique_multy_extend');";
                                    $file.=" }";

                                    $file.="if(!array_key_exists('".$name_index."_unique_single', $"."indexesFound)){";
                                        $file.="$"."table->unique(['kodepemda','tahun','tw'],'".$name_index."_unique_single');";
                                    $file.=" }";


                                }else{

                                    // $file.="if(array_key_exists('".$name_index."_unique_multy_extend', $"."indexesFound)){";
                                    //     $file.="$"."table->dropUnique('".$name_index."_unique_multy_extend');";
                                    // $file.=" }";

                                    $file.="if(array_key_exists('".$name_index."_unique_single', $"."indexesFound)){";
                                        $file.="$"."table->dropUnique('".$name_index."_unique_single');";
                                    $file.=" }";


                                }

                        $file.="
                            });
                        }";

                        foreach($index_aggre as $i){
                            $file.="DB::statement('CREATE INDEX IF NOT EXISTS ".$i.'_idx'." ON dataset.".'"'.$table.'"'." USING btree (".$i.")');
                            ";
                        }
                        $file.="
                    }

            }



            public function down(){



            }

        }";

        $file_schema_named=date('Y_m_d_h0000').'_'.strtolower(str_replace('@_','',preg_replace('/[A-Z]/D', '_$0', '@'.static::nameVar($table)))).'.php';

        Storage::disk('dataset')->put($file_schema_named,$file);
        return [
            'name'=>$file_schema_named,
            'path'=>Storage::disk('dataset')->url($file_schema_named)

        ];
    }



    public function load_list_data($tahun,Request $request){
        $data=DB::table('master.data as d')
            ->join('master.validator_data as v','v.id','=','d.id_validator')
            ->where('d.name','ilike','%'.$request->search.'%')
            ->Orwhere('d.definisi_konsep','ilike','%'.$request->search.'%')
            ->selectRaw("'' as id,d.name,v.name as validator,0 as index ,d.produsent_data,d.id as id_data, d.file_accept,d.definisi_konsep,d.options,d.bind_view,d.satuan,d.tipe_nilai,d.type,concat('data_',d.id) as field_name")
            ->limit(15)->get();
        return $data;
    }

    public function load($tahun,Request $request){
        $paginate=20;
        $data=DB::table('master.dataset')->paginate($paginate);
        return [
            'data'=>$data->items(),
            'last_page'=>$data->lastPage(),
            'current_page'=>$data->currentPage(),
            'paginate_count'=>$paginate

        ];
    }

    public function index($tahun,Request $request)
    {
        $page_meta=[
            'title'=>'Master Dataset',
            'keterangan'=>''
        ];
        return view('datasetmodul::schema.index')->with(['page_meta'=>$page_meta]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create($tahun,Request $request)
    {

        $page_meta=[
            'name'=>old('name')??null,
            'jenis_dataset'=>old('jenis_dataset')??0,
            'login'=>old('login')??0,
            'tujuan'=>old('tujuan')??null,
            'kode'=>old('kode')??null,
            'pusat'=>old('pusat')??0,
            'table'=>old('table')??null,
            'status'=>old('status')??0,
            'penangung_jawab'=>old('penangung_jawab')??null,
            'definisi_konsep'=>old('definisi_konsep')??null,
            'tanggal_publish'=>old('tanggal_publish')??null,
        ];
        $data= old('list_data')?json_decode(old('list_data')):[];
        $page_meta['title']='Tambah Dataset';

        $page_meta['keterangan']='';
        $permissions=[];

        $re_pemis=$request->permissions?json_decode($request->permissions,true):[];
        $roles=DB::table('public.roles')->orderBy('id','asc')->get();
        foreach ($roles as $key => $r) {
            $pr=array_filter($re_pemis,function($el){
                return $el['id']==$r->id;
            });
            $id="xx";
            $schema_permit=[
                'name'=>$r->name,
                'id'=>$r->id,
                'permissions'=>[
                    'dts.'.$id.':view-data'=>[
                        'name'=>'View Data',
                        'key'=>'dts.'.$id.':view-data',
                        'state'=>false
                    ],
                    'dts.'.$id.':edit-data'=>[
                        'name'=>'Edit Data',
                        'key'=>'dts.'.$id.':edit-data',
                        'state'=>false
                    ],
                    'dts.'.$id.':verified-data'=>[
                        'name'=>'Verified Data',
                        'key'=>'dts.'.$id.':verified-data',
                        'state'=>false
                    ],
                    'dts.'.$id.':validate-data'=>[
                        'name'=>'Validate Data',
                        'key'=>'dts.'.$id.':validate-data',

                        'state'=>false
                    ],
                    'dts.'.$id.':rollback-data'=>[
                        'name'=>'Rollback Data',
                        'key'=>'dts.'.$id.':rollback-data',
                        'state'=>false
                    ],
                ]
            ];
            foreach($schema_permit['permissions'] as $k=>$perm){
                if(count($pr)){
                    $schema_permit['permissions'][$perm['key']]['state']=id_array($perm['key'],$pr[0]['permissions'])?true:false;
                }

            }

            $permissions[]=$schema_permit;
        }




        return view('datasetmodul::schema.show')->with(['roles'=>$permissions,'permitions'=>$request->permitions??[],'page_meta'=>$page_meta,'data'=>$data,'url'=>route('mod.dataset.schema.store',['tahun'=>$tahun])]);

    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store($tahun,Request $request)
    {
        define('STDIN',fopen("php://stdin","r"));
        $uid=Auth::User()->id;
        $now=Carbon::now();


        $validate=[
            'name'=>'required|string',
            'penangung_jawab'=>'required|string',
            'tujuan'=>'required|string',
            'pusat'=>'required|numeric|in:0,1',
            'klasifikasi_1'=>'required|string',
            'status'=>'required|numeric|in:0,1',
            'jenis_dataset'=>'required|numeric|in:0,1',
        ];
        if($request->table){
            // $validate['table']='required|string|unique:pg_tables,tablename';
        }
        $valid=Validator::make($request->all(),$validate);

        if($valid->fails()){
            Alert::error('',$valid->errors()->first());
            $re_pemis=$request->permission?json_decode($request->permission,true):[];
            $pr=array_map(function($el){
                return $el['id'];
            },$re_pemis);


            $roles=DB::table('public.roles')->orderBy('id','asc')->whereIn('id',$pr)->get();

            $pemis_accept=[];

            foreach($roles as $k=>$r){
                $filter=array_values(array_filter($re_pemis,function($el) use ($r){
                    return $el['id']==$r->id;
                    }));
                if(count($filter)){
                    if(!isset($filter[0])){
                        dd($filter);
                    }
                    $pemis_accept[$r->name]=$filter[0]['permissions'];
                }
            }

            foreach ($roles as $key => $r) {

                $id="xx";
                $schema_permit=[
                    'name'=>$r->name,
                    'id'=>$r->id,
                    'permissions'=>[
                        'dts.'.$id.':view-data'=>[
                            'name'=>'View Data',
                            'key'=>'dts.'.$id.':view-data',
                            'state'=>false
                        ],
                        'dts.'.$id.':edit-data'=>[
                            'name'=>'Edit Data',
                            'key'=>'dts.'.$id.':edit-data',
                            'state'=>false
                        ],
                        'dts.'.$id.':verified-data'=>[
                            'name'=>'Verified Data',
                            'key'=>'dts.'.$id.':verified-data',
                            'state'=>false
                        ],
                        'dts.'.$id.':validate-data'=>[
                            'name'=>'Validate Data',
                            'key'=>'dts.'.$id.':validate-data',
    
                            'state'=>false
                        ],
                        'dts.'.$id.':rollback-data'=>[
                            'name'=>'Rollback Data',
                            'key'=>'dts.'.$id.':rollback-data',
                            'state'=>false
                        ],
                    ]
                ];
               

                foreach($schema_permit['permissions'] as $k=>$perm){
                    if(count($pr)){
                        $schema_permit['permissions'][$perm['key']]['state']=in_array($perm['key'],$pemis_accept[$schema_permit['name']])?true:false;
                    }
    
                }
    
                $permissions[]=$schema_permit;
            }
            $request['permission']=$permissions;
            return back()->withInput();
        }
        DB::beginTransaction();
        try{
           $id_dataset= DB::table('master.dataset')->insertGetId([
                'name'=>$request->name,
                'tujuan'=>$request->tujuan,
                'jenis_distribusi'=>$request->jenis_distribusi,
                'jenis_dataset'=>$request->jenis_dataset,
                'pusat'=>$request->pusat,
                'status'=>$request->status,
                'admin_menu'=>$request->admin_menu,
                'login'=>(boolean)((int)$request->login??0),
                'tanggal_publish'=>$request->tanggal_publish,
                'penangung_jawab'=>$request->penangung_jawab,
                'kode'=>$request->kode,
                'kategori'=>$request->kategori,
                'klasifikasi_1'=>$request->klasifikasi_1??'Data Pemda',
                'klasifikasi_2'=>$request->klasifikasi_2,
                'table'=>$request->table??'name_table_input',
                'id_u_created'=>$uid,
                'id_u_updated'=>$uid,
                'created_at'=>$now,
                'updated_at'=>$now,
            ]);


            if($id_dataset && !isset($request['table'])){
                $name_table='dts_'.strtolower(str_replace(' ','_',$request->name));
                $table_name=strtolower(static::nameVar($name_table));
                $table_name_= substr($table_name, 0, 20);

                while(DB::table('pg_tables')->where('tablename',$table_name)->first()){
                    $table_name.='_'.(int)rand(0,100);
                }

                DB::table('master.dataset')->where('id',$id_dataset)->update(['table'=>$table_name]);
            }else{
                $table_name=$request->table;
            }

            $dataset=DB::table('master.dataset')->where('id',$id_dataset)->first();

            if($dataset){
                $sc_permit=[
                    'dts.'.$dataset->id.':verified-data',
                    'dts.'.$dataset->id.':rollback-data',
                    'dts.'.$dataset->id.':validate-data',
                    'dts.'.$dataset->id.':edit-data',
                ];

                foreach($sc_permit as $p=>$permis){
                    DB::table('public.permissions')->updateOrInsert([
                        'name'=>$permis,
                        'guard_name'=>'web'
                    ],[
                        'name'=>$permis,
                        'guard_name'=>'web'
                    ]);
                }
               



                $roles=DB::table('public.roles')->orderBy('id','asc')->get();
                $permissions=[];

                $re_pemis=$request->permission?json_decode($request->permission,true):[];
                $pemit_rekap=DB::table('public.permissions')->where('guard_name','web')->whereIn('name',$sc_permit)->get();
                $pemit_db=[];
                foreach($pemit_rekap as $p){
                    $pemit_db[$p->name]=$p->id;
                }

                foreach($re_pemis as $r=>$role){
                    $permis_acc=array_map(function($el) use ($pemit_db,$dataset){
                        return $pemit_db[str_replace('xx',$dataset->id,$el)];
                    },$role['permissions']);

                    foreach($permis_acc as $c){
                        DB::table('public.role_has_permissions')->updateOrInsert([
                            'permission_id'=>$c,
                            'role_id'=>$role['id']

                        ],[
                            'permission_id'=>$c,
                            'role_id'=>$role['id']
                        ]);


                    }
                    $rp=DB::table('public.role_has_permissions as rp')
                    ->join('public.permissions as p','p.id','=','rp.permission_id')
                    ->where('rp.role_id',$role['id'])
                    ->selectRaw('rp.*')
                    ->where('p.name','ilike','dts.'.$id_dataset.'%')
                    ->whereNotIn('rp.permission_id',$permis_acc)->get();

                    foreach($rp as $r){
                        DB::table('public.role_has_permissions as rp')
                        ->where([
                            'role_id'=>$r->role_id,
                            'permission_id'=>$r->permission_id
                        ])->delete();
                    }


                }



            }

            if($id_dataset and $request->list_data){
                $new=[];
                $old=[];
                $merge_id_data=[];


                $list_data=json_decode($request->list_data,true);
                foreach($list_data??[] as $k=>$d){
                    if($d['id']){
                        $old[]=$d['id'];
                    }else{

                        $idx=DB::table('master.dataset_data')->insertGetId([
                            'id_data'=>$d['id_data'],
                            'id_dataset'=>$id_dataset,
                            'field_name'=>$d['field_name'],
                            'id_u_created'=>$uid,
                            'index'=>$k,
                            'login'=>false,
                            'id_u_updated'=>$uid,
                            'created_at'=>$now,
                            'updated_at'=>$now,
                        ]);
                        if($idx){
                            $list_data[$k]['id']=$idx;
                            $new[]=$idx;
                        }
                    }
                }

            }




            if($dataset){
                $list_build_data=array_map(function($el){

                    return [
                        'id_data'=>$el['id_data'],
                        'name'=>$el['name'],
                        'satuan'=>$el['satuan'],
                        'new_field_name'=>$el['field_name'],
                        'tipe_nilai'=>$el['tipe_nilai'],
                        'type'=>$el['type'],
                        'aggregasi'=>0
                    ];
                 },(Array)($list_data));

                 $name_index='dts_'.$id_dataset;

                 if(str_contains($dataset->table,'[TAHUN]')){
                    $name_index.='_'.$tahun;
                    $table_name=str_replace('[TAHUN]',$tahun,$table_name);
                 }


                 if(strtoupper($request->klasifikasi_1)=='DATA PEMDA'){
                     $schema_table=static::build_schema($name_index,$tahun,$table_name,$list_build_data,$request->jenis_dataset?1:0);

                 }else{
                    $schema_table=static::build_schema_bu($name_index,$tahun,$table_name,$list_build_data,$request->jenis_dataset?1:0);

                 }

                static::migrate_dataset($schema_table['name']);

                $konsensus_table=static::build_schema_status_data($name_index);


            }else{
                Alert::error('','Server Error');
                $re_pemis=$request->permission?json_decode($request->permission,true):[];
                $pr=array_map(function($el){
                    return $el['id'];
                },$re_pemis);
    
    
                $roles=DB::table('public.roles')->orderBy('id','asc')->whereIn('id',$pr)->get();
    
                $pemis_accept=[];
    
                foreach($roles as $k=>$r){
                    $filter=array_values(array_filter($re_pemis,function($el) use ($r){
                        return $el['id']==$r->id;
                        }));
                    if(count($filter)){
                        if(!isset($filter[0])){
                            dd($filter);
                        }
                        $pemis_accept[$r->name]=$filter[0]['permissions'];
                    }
                }
    
    
    
                foreach ($roles as $key => $r) {
    
                    $id="xx";
                    $schema_permit=[
                        'name'=>$r->name,
                        'id'=>$r->id,
                        'permissions'=>[
                            'dts.'.$id.':view-data'=>[
                                'name'=>'View Data',
                                'key'=>'dts.'.$id.':view-data',
                                'state'=>false
                            ],
                            'dts.'.$id.':edit-data'=>[
                                'name'=>'Edit Data',
                                'key'=>'dts.'.$id.':edit-data',
                                'state'=>false
                            ],
                            'dts.'.$id.':verified-data'=>[
                                'name'=>'Verified Data',
                                'key'=>'dts.'.$id.':verified-data',
                                'state'=>false
                            ],
                            'dts.'.$id.':validate-data'=>[
                                'name'=>'Validate Data',
                                'key'=>'dts.'.$id.':validate-data',
        
                                'state'=>false
                            ],
                            'dts.'.$id.':rollback-data'=>[
                                'name'=>'Rollback Data',
                                'key'=>'dts.'.$id.':rollback-data',
                                'state'=>false
                            ],
                        ]
                    ];
                   
    
                    foreach($schema_permit['permissions'] as $k=>$perm){
                        if(count($pr)){
                            $schema_permit['permissions'][$perm['key']]['state']=in_array($perm['key'],$pemis_accept[$schema_permit['name']])?true:false;
                        }
        
                    }
        
                    $permissions[]=$schema_permit;
                }
                $request['permission']=$permissions;
                return back()->withInput();
            }

            DB::commit();
        }catch(\Exception $err){
            DB::rollback();
            Alert::error('',$err->getMessage());
            return back()->withInput();
        }


        Alert::success('','Berhasil Menambahkan Dataset');
        return back();


    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($tahun,$id,Request $request)
    {
        $dataset=DB::table('master.dataset')->find($id);
        if($dataset){

            $data=DB::table('master.dataset_data as sd')
            ->join('master.data as d','d.id','=','sd.id_data')
            ->join('master.validator_data as v','v.id','=','d.id_validator')
            ->where('sd.id_dataset',$id)
            ->orderBy('sd.index','asc')
            ->selectRaw("sd.id,d.name,v.name as validator,d.produsent_data,sd.index,d.id as id_data,d.file_accept,d.definisi_konsep,d.options,d.bind_view,d.satuan,d.tipe_nilai,d.type,sd.field_name as field_name")
            ->get();

            $page_meta=(array)$dataset;
            $page_meta['title']=$dataset->name;
            $page_meta['keterangan']=$dataset->definisi_konsep;
            $page_meta['tanggal_publish']=Carbon::parse($dataset->tanggal_publish)->format('Y-m-d');
            $permissions=[];
            $re_pemis=$request->permissions?json_decode($request->permissions,true):[];
            $roles=DB::table('public.roles')->orderBy('id','asc')->get();
            foreach ($roles as $key => $r) {
                $pr=array_filter($re_pemis,function($el){
                    return $el['id']==$r->id;
                });
                $schema_permit=[
                    'name'=>$r->name,
                    'id'=>$r->id,
                    'permissions'=>[
                        'dts.'.$id.':view-data'=>[
                            'name'=>'View Data',
                            'key'=>'dts.'.$id.':view-data',
                            'state'=>false
                        ],
                        'dts.'.$id.':edit-data'=>[
                            'name'=>'Edit Data',
                            'key'=>'dts.'.$id.':edit-data',
                            'state'=>false
                        ],
                        'dts.'.$id.':verified-data'=>[
                            'name'=>'Verified Data',
                            'key'=>'dts.'.$id.':verified-data',

                            'state'=>false
                        ],
                        'dts.'.$id.':validate-data'=>[
                            'name'=>'Validate Data',
                            'key'=>'dts.'.$id.':validate-data',

                            'state'=>false
                        ],
                        'dts.'.$id.':rollback-data'=>[
                            'name'=>'Rollback Data',
                            'key'=>'dts.'.$id.':rollback-data',
                            'state'=>false
                        ],
                    ]
                ];
                foreach($schema_permit['permissions'] as $k=>$perm){
                    if(count($pr)){
                        $schema_permit['permissions'][$perm['key']]['state']=id_array($perm['key'],$pr[0]['permissions'])?true:false;
                    }

                }

                $permit=DB::table('public.role_has_permissions as rp')
                ->join('public.permissions as perm','perm.id','=','rp.permission_id')
                ->select('perm.name','perm.guard_name',DB::raw("rp.role_id as id"))
                ->where('rp.role_id',$r->id)
                ->where('perm.guard_name','web')
                ->whereIn('perm.name',array_keys($schema_permit['permissions']))->get();

                foreach($permit as $p=>$perm){
                    if(isset($schema_permit['permissions'][$perm->name])){
                        $pr=array_filter($re_pemis,function($el){
                            return $el['id']==$r->id;
                        });
                        if(count($pr)){
                         $schema_permit['permissions'][$perm->name]['state']=id_array($perm->name,$pe[0]['permissions'])?true:false;

                        }else{
                         $schema_permit['permissions'][$perm->name]['state']=true;
                        }
                    }
                }



                $permissions[]=$schema_permit;

            }

            return view('datasetmodul::schema.show')->with(['roles'=>$permissions,'page_meta'=>$page_meta,'data'=>$data,'url'=>route('mod.dataset.schema.update',['tahun'=>$tahun,'id'=>$id])]);

        }
    }


    public function update($tahun,$id,Request $request)
    {
        define('STDIN',fopen("php://stdin","r"));
        $uid=Auth::User()->id;
        $now=Carbon::now();
        $dataset=DB::table('master.dataset')->find($id);
        if(!$dataset){
            Alert::error('','Data Tidak Tersedia Kembali');
            return back();
        }

        $validate=[
            'name'=>'required|string',
            'penangung_jawab'=>'required|string',
            'tujuan'=>'required|string',
            'klasifikasi_1'=>'required|string',
            'pusat'=>'required|numeric|in:0,1',
            'status'=>'required|numeric|in:0,1',
            'jenis_dataset'=>'required|numeric|in:0,1',
        ];


        $valid=Validator::make($request->all(),$validate);

        if($valid->fails()){
            Alert::error('',$valid->errors()->first());
            unset($request['permission']);
            return back()->withInput();

        }
        $sc_permit=[
            'dts.'.$id.':verified-data',
            'dts.'.$id.':rollback-data',
            'dts.'.$id.':validate-data',
            'dts.'.$id.':edit-data',
            'dts.'.$id.':view-data',

        ];
        foreach($sc_permit as $p=>$permis){
            DB::table('public.permissions')->updateOrInsert([
                'name'=>$permis,
                'guard_name'=>'web'
            ],[
                'name'=>$permis,
                'guard_name'=>'web'
            ]);
        }

        DB::beginTransaction();
        try{

            $id_dataset=$id;
            DB::table('master.dataset')->where('id',$id)->update([
                'name'=>$request->name,
                'admin_menu'=>$request->admin_menu,
                'tujuan'=>$request->tujuan,
                'jenis_dataset'=>$request->jenis_dataset,
                'jenis_distribusi'=>$request->jenis_distribusi,
                'pusat'=>$request->pusat,
                'table'=>$request->table,
                'kategori'=>$request->kategori??'[]',
                'klasifikasi_1'=>$request->klasifikasi_1??'Data Pemda',
                'klasifikasi_2'=>$request->klasifikasi_2,
                'status'=>$request->status,
                'tanggal_publish'=>$request->tanggal_publish,
                'penangung_jawab'=>$request->penangung_jawab,
                'kode'=>$request->kode,
                'id_u_updated'=>$uid,
                'updated_at'=>$now,
            ]);
            $dataset=DB::table('master.dataset')->find($id);

            $table_name=str_replace('[TAHUN]',$tahun,$dataset->table);

            if($id_dataset and $request->list_data){
                $new=[];
                $old=[];
                $merge_id_data=[];

                $list_data=array_map(function($el){
                    $el['new_field_name']=$el['field_name'];
                    return $el;
                },json_decode($request->list_data,true));

                $list_data_recorded=json_decode($request->list_data_recorded??'[]',true);


                $list_data_recorded=array_filter($list_data_recorded,function($el){
                    return $el['new_field_name']!=null;
                });
               
                foreach($list_data??[] as $k=>$d){
                    if($d['id']){
                        $old[]=$d['id'];
                        DB::table('master.dataset_data')->where([
                            'id_dataset'=>$id_dataset,
                            'id'=>$d['id']
                        ])->update([
                            'field_name'=>$d['new_field_name'],
                            'id_u_updated'=>$uid,
                            'updated_at'=>$now,
                            'index'=>$k
                        ]);

                    }else{

                        $idx=DB::table('master.dataset_data')->insertGetId([
                            'id_data'=>$d['id_data'],
                            'id_dataset'=>$id_dataset,
                            'index'=>$k,
                            'field_name'=>$d['new_field_name'],
                            'id_u_created'=>$uid,
                            'id_u_updated'=>$uid,
                            'created_at'=>$now,
                            'updated_at'=>$now,
                        ]);
                        if($idx){
                            $list_data[$k]['id']=$idx;
                            $new[]=$idx;
                        }
                    }
                }

                $id_data_list=array_merge($new,$old);
                $id_data_list=array_unique($id_data_list);

                DB::table('master.dataset_data')->where('id_dataset',$id)->whereNotIn('id',$id_data_list)->delete();

            }

            $list_build_data=array_map(function($el){
                return [
                    'id_data'=>$el['id_data'],
                    'name'=>$el['name'],
                    'satuan'=>$el['satuan'],
                    'field_name'=>$el['field_name'],
                    'new_field_name'=>$el['field_name'],
                    'tipe_nilai'=>$el['tipe_nilai'],
                    'type'=>$el['type'],
                    'aggregasi'=>0
                ];
             },(Array)($list_data));
            $name_index='dts_'.$id_dataset;

            if(str_contains($dataset->table,'[TAHUN]')){
               $name_index.='_'.$tahun;
            }


            if(strtoupper($request->klasifikasi_1)=='DATA PEMDA'){
                $schema_table=static::build_schema($name_index,$tahun,$table_name,$list_build_data,$request->jenis_dataset?1:0);
            }else{
               $schema_table=static::build_schema_bu($name_index,$tahun,$table_name,$list_build_data,$request->jenis_dataset?1:0);
            }

            static::migrate_dataset($schema_table['name']);

            $konsensus_table=static::build_schema_status_data($name_index);
            static::migrate_dataset($konsensus_table['name']);

            $schema_table_builded=[true,''];


            $roles=DB::table('public.roles')->orderBy('id','asc')->get();
            $permissions=[];
            $re_pemis=$request->permission?json_decode($request->permission,true):[];
            $pemit_rekap=DB::table('public.permissions')->where('guard_name','web')->whereIn('name',$sc_permit)->get();
            $pemit_db=[];
            foreach($pemit_rekap as $p){
                $pemit_db[$p->name]=$p->id;
            }

            foreach($re_pemis as $r=>$role){
                $permis_acc=array_map(function($el) use ($pemit_db){
                    return $pemit_db[$el];
                },$role['permissions']);

                foreach($permis_acc as $c){
                    DB::table('public.role_has_permissions')->updateOrInsert([
                        'permission_id'=>$c,
                        'role_id'=>$role['id']

                    ],[
                        'permission_id'=>$c,
                        'role_id'=>$role['id']
                    ]);


                }
                $rp=DB::table('public.role_has_permissions as rp')
                ->join('public.permissions as p','p.id','=','rp.permission_id')
                ->where('rp.role_id',$role['id'])
                ->selectRaw('rp.*')
                ->where('p.name','ilike','dts.'.$id_dataset.'%')
                ->whereNotIn('rp.permission_id',$permis_acc)->get();

                foreach($rp as $r){
                    DB::table('public.role_has_permissions as rp')
                    ->where([
                        'role_id'=>$r->role_id,
                        'permission_id'=>$r->permission_id
                    ])->delete();
                }


            }

            foreach ($roles as $key => $r) {
                $pr=array_filter($re_pemis,function($el) use ($r){
                    return $el['id']==$r->id;
                });
                $schema_permit=[
                    'name'=>$r->name,
                    'id'=>$r->id,
                    'permissions'=>[
                        'dts.'.$id.':view-data'=>[
                            'name'=>'View Data',
                            'key'=>'dts.'.$id.':view-data',
                            'state'=>false
                        ],
                        'dts.'.$id.':edit-data'=>[
                            'name'=>'Edit Data',
                            'key'=>'dts.'.$id.':edit-data',
                            'state'=>false
                        ],
                        'dts.'.$id.':verified-data'=>[
                            'name'=>'Verified Data',
                            'key'=>'dts.'.$id.':verified-data',

                            'state'=>false
                        ],
                        'dts.'.$id.':validate-data'=>[
                            'name'=>'Validate Data',
                            'key'=>'dts.'.$id.':validate-data',

                            'state'=>false
                        ],
                        'dts.'.$id.':rollback-data'=>[
                            'name'=>'Rollback Data',
                            'key'=>'dts.'.$id.':rollback-data',
                            'state'=>false
                        ],
                    ]
                ];
                foreach($schema_permit['permissions'] as $k=>$perm){
                    if(isset($pr[0])){
                        $schema_permit['permissions'][$perm['key']]['state']=in_array($perm['key'],$pr[0]['permissions'])?true:false;
                    }
                }

                $permissions[]=$schema_permit;


            }

            $request->permission=$permissions;


            if($dataset){
                $list_build_data=array_map(function($el){
                    return [
                        'id_data'=>$el['id_data'],
                        'name'=>$el['name'],
                        'satuan'=>$el['satuan'],
                        'new_field_name'=>$el['field_name'],
                        'tipe_nilai'=>$el['tipe_nilai'],
                        'type'=>$el['type'],
                        'aggregasi'=>0
                    ];
                 },(Array)($list_data));

                 $name_index='dts_'.$id_dataset;

                 if(str_contains($dataset->table,'[TAHUN]')){
                    $name_index.='_'.$tahun;
                    $table_name=str_replace('[TAHUN]',$tahun,$table_name);
                 }


              

                if(strtoupper($request->klasifikasi_1)=='DATA PEMDA'){
                    $schema_table=static::build_schema($name_index,$tahun,$table_name,$list_build_data,$request->jenis_dataset?1:0);
                }else{
                    $schema_table=static::build_schema_bu($name_index,$tahun,$table_name,$list_build_data,$request->jenis_dataset?1:0);
                }


               

                static::migrate_dataset($schema_table['name']);
                $konsensus_table=static::build_schema_status_data($name_index);
            }

            DB::commit();
        }catch(\Exception $err){
            DB::rollback();
            Alert::error('',$err->getMessage());
            unset($request['permission']);
           
            return back();
        }

        Alert::success('','Berhasil Memperbarui Dataset');
        return back();






        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
