<?php

namespace Modules\DatasetModul\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use Auth;
use Alert;
use Carbon\Carbon;
use Validator;
class MasterDataController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

     public function load($tahun,Request $request){
        $paginate=20;
        $data=DB::table('master.data as d')->join('master.validator_data as v','v.id','=','d.id_validator')
        ->selectRaw("
        d.id,
        d.name,
        v.name as validator,
        0 as index ,
        d.arah_nilai,
        d.produsent_data,
         d.file_accept,
         d.definisi_konsep,
         d.options,
         d.bind_view,
         d.satuan,
         d.tipe_nilai
         ,d.type
         ,concat('data_',d.id) as field_name
         ")
        ->orderBy('d.id_validator','asc')
        ->orderBy('d.name','asc')
        ->paginate($paginate);

        $data_re=[];
        foreach($data as $key=>$d){
            $d->dataset_list=DB::table('master.dataset_data as dd')->join('master.dataset as dts','dts.id','=','dd.id_dataset')
            ->selectRaw(" string_agg(dts.name::character varying, '#//#' order by dts.id asc) as name")->where('dd.id_data',$d->id)->first();
            if($d->dataset_list){
                $d->dataset_list=$d->dataset_list->name;
            }
            $data_re[]=$d;

        }

        return [
            'data'=>$data_re,
            'last_page'=>$data->lastPage(),
            'current_page'=>$data->currentPage(),
            'paginate_count'=>$paginate
        ];



        
     }
    public function index($tahun)
    {
        $page_meta=[
            'title'=>'Schema Data',
            'keterangan'=>''
        ];
        return view('datasetmodul::schema-data.index')->with('page_meta',$page_meta);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create($tahun)
    {
        $page_meta=[
            'title'=>'Tambah Schema Data',
            'keterangan'=>'',
            'id'=>null,
            'name'=>old('name')??null,
            'definsi_konsep'=>old('definsi_konsep')??null,
            'type'=>old('type')??'input-value',
            'tipe_nilai'=>old('tipe_nilai')??'numeric',
            'satuan'=>old('satuan')??null,
            'classification'=>old('classification')??0,
            'produsent_data'=>old('produsent_data')??null,
            'arah_nilai'=>old('arah_nilai')??0,
            'penangung_jawab'=>old('penangung_jawab')??null,
            'id_validator'=>old('id_validator')??null,
            'options'=>old('options')??'[]',
            'bind_view'=>old('bind_view')??'[]',
            'file_accept'=>old('file_accept')??'[]',

        ];

        if($page_meta){
            $validator=DB::table('master.validator_data')->get();
             return view('datasetmodul::schema-data.show')->with(['page_meta'=>$page_meta,'validator_list'=>$validator,'url'=>route('mod.dataset.schema-data.store',['tahun'=>$tahun])]);
        }
        return view('datasetmodul::schema-data.show');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request,$tahun)
    {
        //

        $request['classification']=$request->classification=='true'?true:false;
        $valid=Validator::make($request->all(),[
         'name'=>'required|string',
         'definisi_konsep'=>'required|string',
         'satuan'=>'required|string',
         'tipe_nilai'=>'required|string|in:numeric,string',
         'type'=>'required|string|in:input-value,options,single_file,multy_file,input-date',
         'id_validator'=>'required|numeric|exists:validator_data,id',
         'produsent_data'=>'required|string',
         'classification'=>'required|boolean'
        ]);
 
        if($valid->fails()){
            Alert::error('',$valid->errors()->first());
            return back();
        }
        $data=[
            'name'=>$request->name,
            'definisi_konsep'=>$request->definisi_konsep,
            'satuan'=>$request->satuan,
            'tipe_nilai'=>$request->tipe_nilai,
            'type'=>$request->type,
            'arah_nilai'=>$request->arah_nilai,
            'id_validator'=>$request->id_validator,
            'produsent_data'=>$request->produsent_data,
            'cara_hitung'=>$request->cara_hitung,
            'classification'=>$request->classification,
            'options'=>json_encode($request->options??[]),
            'bind_view'=>json_encode($request->bind_view??[]),
            'file_accept'=>json_encode($request->file_accept??[]),
            'updated_at'=>Carbon::now(),
            'table'=>'xxxxxx'
        ];

        $id=DB::table('master.data')->insertGetId($data);
        if($id){

            DB::table('master.data')->where('id',$id)->update(['table'=>'data_'.$id]);

            Alert::success('','Berhasil Menambah Data'); 
            return back();
    
           }

        
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($tahun,$id)
    {

        $page_meta=(array)DB::table('master.data as d')->join('master.validator_data as v','v.id','=','d.id_validator')
        ->selectRaw("d.*,v.name as validator,v.id as id_validator")
        ->where('d.id',$id)->first();

        if($page_meta){
            $validator=DB::table('master.validator_data')->get();
            $page_meta['title']= $page_meta['name'];
            $page_meta['keterangan']= $page_meta['definisi_konsep'];
             return view('datasetmodul::schema-data.show')->with(['page_meta'=>$page_meta,'validator_list'=>$validator,'url'=>route('mod.dataset.schema-data.update',['tahun'=>$tahun,'id'=>$id])]);

        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('datasetmodul::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $tahun,$id)
    {
        $request['classification']=$request->classification=='true'?true:false;

        $valid=Validator::make($request->all(),[
            'name'=>'required|string',
            'definisi_konsep'=>'required|string',
            'satuan'=>'required|string',
            'type'=>'required|string|in:input-value,options,single_file,multy_file,input-date',
            'id_validator'=>'required|numeric|exists:validator_data,id',
            'produsent_data'=>'required|string',
            'classification'=>'required|boolean'
        ]);

       if($valid->fails()){
           Alert::error('',$valid->errors()->first());
           return back();
       }
       $data=[
           'name'=>$request->name,
           'definisi_konsep'=>$request->definisi_konsep,
           'satuan'=>$request->satuan,
           'type'=>$request->type,
           'id_validator'=>$request->id_validator,
           'arah_nilai'=>$request->arah_nilai,
           'cara_hitung'=>$request->cara_hitung,
           'produsent_data'=>$request->produsent_data,
           'classification'=>$request->classification,
           'options'=>json_encode($request->options??[]),
           'bind_view'=>json_encode($request->bind_view??[]),
           'file_accept'=>json_encode($request->file_accept??[]),
           'updated_at'=>Carbon::now()
       ];


       $data=DB::table('master.data')->where('id',$id)->update($data);

       if($data){
        Alert::success('','Berhasil Merubah Data'); 
        return back();

       }

        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function delete($tahun,$id)
    {
        DB::table('master.data')->where('id',$id)->delete();
        Alert::success('','Berhasil Mengahapus Data');
        return back();

        //
    }
}
