<?php

namespace Modules\DatasetModul\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use App\Http\Controllers\FilterCtrl;

class QueryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

    public  static function lev_data_column($tahun,$id_dataset,Request $request){
        return DB::table('master.dataset_data as sd')
        ->join('master.data as d','d.id','=','sd.id_data')
        ->where([
            ['sd.id_dataset','=',$id_dataset],
            ['sd.ag_lev_data','!=',null]
        ])
        ->Orwhere([
            ['sd.id_dataset','=',$id_dataset],
            ['sd.ag_lev_data_group','=',true],

        ])
        ->selectRaw('sd.prioritas,sd.ag_lev_data_group as is_group,sd.field_name,sd.ag_lev_data as ag,sd.ag_lev_data_title as ag_title,d.satuan as ag_satuan,d.*')
        ->orderBy('sd.index','asc')
        ->get();
    }

   public static function lev_data($tahun,$id_dataset,$type,$public=true,$optional_where='',Request $request){
        
        $dataset=DB::table('master.dataset')->find($id_dataset);
        $field=static::lev_data_column($tahun,$id_dataset,$request);
        $status_diminta=[null,''];


        if($field AND  $dataset){
            if(strtoupper($dataset->klasifikasi_1)=='DATA PEMDA'){
                $select=['max(d.id) as id_data','d.kodepemda','MAX(mp.nama_pemda) as nama_pemda,max(std.status) as status_data'];
                $group=['d.kodepemda','d.tahun'];

            }else  if(strtoupper($dataset->klasifikasi_1)=='DATA BUMDAM'){
                $select=['max(d.id) as id_data','d.kodepemda','MAX(mp.nama_pemda) AS nama_pemda','d.kodebu','MAX(mbu.nama_bu) as nama_bu,max(std.status) as status_data'];
                $group=['d.kodepemda','d.tahun','d.kodebu'];

            }

            $priority=[];
            foreach($field as $kf=>$f){
                if($f->ag){
                    
                    $select[]=str_replace('@?','d.'.$f->field_name,$f->ag).' as '.$f->field_name.", '".$f->ag_satuan."' as ".$f->field_name.'_satuan';
                }
                if($f->is_group){
                    $group[]='d.'.$f->field_name;
                    if($f->ag==null){
                        $select[]=str_replace('@?','d.'.$f->field_name,'(@?)').' as '.$f->field_name.", '".$f->ag_satuan."' as ".$f->field_name.'_satuan';
                    }
                }

                if($f->prioritas){
                    $priority[]=str_replace('@?','d.'.$f->field_name,$f->ag).' as '.$f->field_name.", '".$f->ag_satuan."' as ".$f->field_name.'_satuan';
                }
            }
            
            if($type=='table'){
                $table_status='dts_'.$dataset->id;
                if(str_contains($dataset->table,'[TAHUN]')){
                    $dataset->table=str_replace('[TAHUN]',$tahun,$dataset->table);
                    $table_status='dts_'.$dataset->id.'_'.$tahun;
                }

               $exist_table=(int)DB::table('pg_tables')->where('tablename','=',$dataset->table)->where('schemaname','=','dataset')->count();
               if($exist_table){
                    $data=DB::table('dataset.'.$dataset->table.' as d')
                    ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
                    ->leftJoin('status_data.'.$table_status.' as std','std.kodepemda','=','d.kodepemda')
                    ->groupBy($group)
                    ->where([
                        ['d.tahun','=',$tahun],
                        ['d.tw','=',4],
                    ]);
                    if($optional_where){
                        $data=$data->whereRaw($optional_where);
                    }
                   
                    if(strtoupper($dataset->klasifikasi_1)=='DATA PEMDA'){

                    }else{
                        $data=$data->join('master.master_bu as mbu','mbu.id','=','d.kodebu');
                    }
                    $status_diminta=[null,'TTERINPUT, TERVERIFIKASI, TERVALIDASI'];
                    if($public){
                        $data=$data
                        ->where('std.status',2)
                        ->selectRaw(implode(',',$select))
                        ->get();
                        $status_diminta='TERVALIDASI';

                    }else{
                        if($request->status_data){
                            switch($request->status_data){
                                case 1:
                                    $data->where('std.status',1);
                                    $status_diminta=[1,'TERVERIFIKASI'];

                                break;
                                case 2:
                                    $data->where('std.status',2);
                                    $status_diminta=[2,'TERVALIDASI'];


                                break;
                                case 0:
                                    $data->where('std.status','=',null);
                                    $data->where('d.id','!=',null);
                                    $status_diminta=[0,'TERINPUT'];


                                break;
                                case 'x':


                                break;
                                default:


                                break;
                            }
                        }
                        // dd($data->toSql());
                        $data=$data->selectRaw(implode(',',$select))->orderBy(DB::raw('max(mp.kodepemda)'),'asc')->orderBy(DB::raw('max(d.index)'),'asc')->get();
                    }

                    return [
                        'columns'=>$field,
                        'data'=>$data,
                        'status_data'=>$status_diminta
                    
                    ];
                    
               }else{
                return [];
               }
            }

            if($type=='series'){
                $series=[

                ];
                $arth=[$tahun+2,$tahun+1,$tahun,$tahun-1,$tahun-2,$tahun-3,$tahun-4,$tahun-5];
                if(str_contains($dataset->table,'[TAHUN]')){
                    foreach($arth as $th){
                        $tbd=str_replace('[TAHUN]',$th,$dataset->table);
                        $tbst='dts_'.$dataset->id.'_'.$th;
                        $exist_table=(int)DB::table('pg_tables')->where('tablename','=',$tbd)->where('schemaname','=','dataset')->count();
                        if($exist_table){
                            $data=DB::table('dataset.'.$tbd.' as d')
                            ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
                            ->leftJoin(DB::raw('status_data.'.$tbst.' as std'),'std.kodepemda','=','d.kodepemda')
                            ->groupBy(implode(',',$group));
                            if(strtoupper($dataset->klasifikasi_1)=='DATA PEMDA'){
                                
                            }else{
                                $data=$data->join('master.master_bu as bu','bu.id','=','d.kodebu');
                            }
        
                            if($public){
                                $data=$data->where('std.status',2)
                                ->selectRaw(implode(',',$priority))
                                ->get();
                            }else{
                                $data=$data->selectRaw(implode(',',$priority))->get();
                            }

                        }
                    }
                }else{
                    $exist_table=(int)DB::table('pg_tables')->where('tablename','=',$dataset->table)->where('schemaname','=','dataset')->count();
                    if($exist_table){


                    }
                    
                }
                
            }

        }else{
            return [];
        }
   }

    public static function state_data($tahun,$id_dataset,$optional_where,Request $request){
        $dataset=DB::table('master.dataset')->find($id_dataset);
        if($dataset){
                $table_status='dts_'.$id_dataset;

            if(str_contains($dataset->table,"[TAHUN]")){
                $dataset->table=str_replace('[TAHUN]',$tahun,$dataset->table);
                $table_status='dts_'.$id_dataset.'_'.$tahun;
                
            }
            $exist_table=(int)DB::table('pg_tables')->where('tablename','=',$dataset->table)->where('schemaname','=','dataset')->count();
            $pemdas=0;
            if(($exist_table)){
                $last_update=DB::table('status_data.'.$table_status)->where('tahun',$tahun)
                ->select('validate_date')
                ->orderBy('validate_date','desc')->first();

                $data=DB::table('master.master_pemda as mp')
                ->leftJoin(( "dataset.".$dataset->table.' as d'),[['d.kodepemda','=','mp.kodepemda'],['d.tahun','=',DB::raw($tahun)]])
                ->leftJoin('status_data.'.$table_status.' as std',[
                    ['mp.kodepemda','=','std.kodepemda'],
                    ['std.tahun','=','d.tahun']
                ])
                ->groupBy('std.status',DB::raw('case when d.id is not null then 1 else 0 end'));

                if($optional_where){
                    $data=$data->whereRaw($optional_where);
                }
                
                $data=$data->selectRaw("max(d.id) as id_data,std.status as status,count(distinct(mp.kodepemda)) as jumlah_pemda")
                ->get();
                $series=[];
                $series[0]=[
                    'name'=>'STATUS DATA ',
                    'data'=>[]
                ];
                foreach($data as $k=>$d){
                    $d=(Array)$d;
                    $pemdas+=(int)$d['jumlah_pemda'];

                    switch($d['status']){
                        case 1:
                            $d['status']='TERVERIFIKASI';
                            $d['color']='#198754';

                        break;
                        case 2:
                            $d['status']='TERVALIDASI';
                            $d['color']='#5190ee';

                        break;
                        case NULL:
                            if($d['id_data']){
                                $d['status']='INPUT BARU';
                            $d['color']='#ffc107';


                            }else{
                                $d['status']='BELUM TERDAPAT STATUS';
                            $d['color']='#6c757d';


                            }
                        break;
                    }

                    if(!isset($series[0])){
                        $series[0]=[
                            'name'=>'STATUS DATA ',
                            'data'=>[]
                        ];
                    }

                    $series[0]['data'][]=[
                        'name'=>$d['status'],
                        'y'=>(int)$d['jumlah_pemda'],
                        'color'=>$d['color']
                    ];


                }
                if(isset($series[0])){
                    $series[0]['name'].='('.$pemdas.') PEMDA';

                }
                return [
                    'dataset'=>$dataset,
                    'series'=>$series,
                    'last_validate'=>$last_update?$last_update->validate_date:null
                ];
            }else{
                return [];
            }
        }else{
            return [];
        }

    }

    public static function getKeterisianAdmin($tahun,$id_dataset,Request $request){
        $status_data=null;
        switch($request->ftr['status_data']){
            case 'ONLY INPUT':
                $status_data= null;
            break;
            case 'ONLY VERIFICATION':
                $status_data= [1,2];

            break;
            case 'ONLY VALIDATION':
                $status_data= [2];

            break;
            default:
            $status_data= null;
            break;
        }

        
        $regional=null;

        
        $filter=[
            'regional'=>array_map(function($el){
                switch($el){
                    case 'REGIONAL 1':
                        return 'I';
                    break;
                    case 'REGIONAL 2':
                        return 'II';
                    break;
                    case 'REGIONAL 3':
                        return 'III';
                    break;
                    case 'REGIONAL 4':
                        return 'IV';
                    break;
                    
                    default:
                        return null;
                    break;
                }

            },$request->ftr['regional'])??[],
            'status_data'=>$status_data,
            'tipe_bantuan'=>$request->ftr['tipe_bantuan']??[]
        ];



        $dataset=DB::table('master.dataset')->find($id_dataset);
        if($dataset){
            $table=$dataset->table;
            $table_status='dts_'.$dataset->id;
            if(str_contains($table,"[TAHUN]")){
                $table=str_replace("[TAHUN]",$tahun,$table);
                $table_status.='_'.$tahun;
            }

            $exist_table=(int)DB::table('pg_tables')->whereIn('tablename',[$table,$table_status])->whereIn('schemaname',['status_data','dataset'])->count();
            if($exist_table>=2){
                $field=DB::table('master.dataset_data as sd')
                ->join('master.data as d','d.id','=','sd.id_data')
                ->where([[
                    'sd.ag_lev_nas','!=',null],
                    ['sd.id_dataset',$id_dataset]])
                ->Orwhere([
                    ['sd.ag_lev_nas_group','=',true],
                    ['sd.id_dataset','=',$id_dataset]
                ])
                ->selectRaw('sd.prioritas,sd.ag_lev_nas_group as is_group,sd.field_name,sd.ag_lev_nas as ag,sd.ag_lev_nas_title as ag_title,sd.ag_lev_nas_satuan as ag_satuan,d.*')
                ->orderBy('sd.index','asc')->get();

                $select=['count(DISTINCT(d.kodepemda)) as jumlah_pemda'];
                $group=[];
                $where_pemda=[];
                $where=[];
                $where_nas=[];

                if($filter['status_data']!==null){
                    if($filter['status_data']==null){
                
                    }else{
                        $where[]='st.status in ('.implode(',',$filter['status_data']).')';
                        $where_nas[]='st.status in ('.implode(',',$filter['status_data']).')';
                        
                    }
                }

                if($filter['tipe_bantuan']!=[]){
                    $where[]="lk.tipe_bantuan in ('".implode("','",$filter['tipe_bantuan'])."')";
                    // $where_nas[]="lk.tipe_bantuan not in ('".implode("','",$filter['tipe_bantuan'])."')";

                }

                if($filter['regional']!=[]){
                    $where[]="mp.regional_1 in ('".implode("','",$filter['regional'])."')";
                    // $where_nas[]="mp.regional_1 in ('".implode("','",$filter['regional'])."')";

                }

                foreach($field as $f){
                    if($f->is_group){
                        $group[]='d.'.$f->field_name;
                    }
                    if($f->ag){
                        if($f->prioritas){
                             $select[]=str_replace('@?',$f->field_name,$f->ag).' as '.'d'.$id_dataset.'_'.$f->field_name;

                        }
                    }
                }


                $nasional_pemda=DB::table('master.master_pemda as mp')
                ->leftJoin('status_data.'.$table_status.' as st',[['st.tahun','=',DB::raw($tahun)],['st.kodepemda','=','mp.kodepemda']])
                ->selectRaw("mp.kodepemda,mp.nama_pemda")->groupBy('mp.kodepemda','mp.nama_pemda');
                if(count($where_nas)){
                    $nasional_pemda=$nasional_pemda->whereRaw(implode(' and ',$where_nas));
                }
                $nasional_pemda=$nasional_pemda->get()->toArray();

                // nasional
                $nasional=(array) DB::table('dataset.'.$table.' as d')
                ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
                ->leftJoin('master.pemda_lokus as lk','lk.kodepemda','=','d.kodepemda')
                ->selectRaw(implode(',',$select))->where('d.tahun',$tahun)
                ->where('d.tw',4)->whereIn('d.kodepemda',array_map(function($el){
                    return $el->kodepemda;
                },$nasional_pemda))->first();

                $lokus=DB::table('master.pemda_lokus')->select('kodepemda')->groupBy('kodepemda')->get()->pluck('kodepemda');
                // non numas
               
                
                $non_nuwas_pemda=DB::table('master.master_pemda as mp')
                ->leftJoin('master.pemda_lokus as lk','lk.kodepemda','=','mp.kodepemda')
                ->leftJoin('status_data.'.$table_status.' as st',[['st.tahun','=',DB::raw($tahun)],['st.kodepemda','=','mp.kodepemda']])
                ->selectRaw("mp.kodepemda,mp.nama_pemda")->groupBy('mp.kodepemda','mp.nama_pemda')->whereNotIn('mp.kodepemda',$lokus);
                if(count($where_nas)){
                    $non_nuwas_pemda=$non_nuwas_pemda->whereRaw(implode(' and ',$where_nas));
                }
                $non_nuwas_pemda=$non_nuwas_pemda->get()->toArray();


            

                $nasional_non_nuwas=(array) DB::table('dataset.'.$table.' as d')
                ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
                ->leftJoin('master.pemda_lokus as lk','lk.kodepemda','=','d.kodepemda')
                ->selectRaw(implode(',',$select))->where('d.tahun',$tahun)
                ->where('d.tw',4)->whereIn('d.kodepemda',array_map(function($el){
                    return $el->kodepemda;
                },$non_nuwas_pemda))->first();


                $longlist_pemda=DB::table('master.master_pemda as mp')
                ->leftJoin('status_data.'.$table_status.' as st',[['st.kodepemda','=','mp.kodepemda']])
                ->join('master.pemda_lokus as lk','lk.kodepemda','=','mp.kodepemda')
                ->selectRaw("mp.kodepemda,mp.nama_pemda")
                ->groupBy('mp.kodepemda','mp.nama_pemda');
                if(count($where)){
                    $longlist_pemda=$longlist_pemda->whereRaw(implode(' and ',$where));
                }
                $longlist_pemda=$longlist_pemda->get()->toArray();



                $longlist=(array) DB::table('dataset.'.$table.' as d')
                ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
                ->join('master.pemda_lokus as lk','lk.kodepemda','=','d.kodepemda')
                ->selectRaw(implode(',',$select))->where('d.tahun',$tahun)
                ->where('d.tw',4)->whereIn('d.kodepemda',array_map(function($el){
                    return $el->kodepemda;
                },$longlist_pemda))->first();

                


                $sortlist_pemda=DB::table('master.master_pemda as mp')
                ->join('master.pemda_lokus as lk',[['lk.kodepemda','=','mp.kodepemda'],['lk.tahun_proyek','=',DB::raw($tahun)]])
                ->leftJoin('status_data.'.$table_status.' as st',[['st.tahun','=',DB::raw($tahun)],['st.kodepemda','=','mp.kodepemda']])
                ->selectRaw("mp.kodepemda,mp.nama_pemda")->groupBy('mp.kodepemda','mp.nama_pemda')->whereNotIn('mp.kodepemda',$lokus);
                if(count($where)){
                    $sortlist_pemda=$sortlist_pemda->whereRaw(implode(' and ',$where));
                }
                $sortlist_pemda=$sortlist_pemda->get()->toArray();


                $sortlist=(array) DB::table('dataset.'.$table.' as d')
                ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
                ->selectRaw(implode(',',$select))->where('d.tahun',$tahun)
                ->where('d.tw',4)->whereIn('d.kodepemda',array_map(function($el){
                    return $el->kodepemda;
                },$sortlist_pemda))->first();

           
                $series=[];
                $Yaxis=[];

                $fs=array_filter($field->toArray(),function($el){
                    return $el->prioritas;
                });
                foreach($fs as $f){
                

                    if(!isset($series['d'.$id_dataset.'_'.$f->field_name])){
                        if(!isset($Yaxis[($f->ag_satuan??$f->satuan)??'-'])){
                            $Yaxis[($f->ag_satuan??$f->satuan)??'-']=[
                                'title'=>[
                                    'text'=>($f->ag_satuan??$f->satuan)
                                ],
                                'opposite'=>count($Yaxis)==0?false:((count($Yaxis)==1?true:((count($Yaxis)%2)==0?false:true)))
                            ];
                            $keyAxisY=array_keys($Yaxis);
                            $indexYaxis=array_search(($f->ag_satuan??$f->satuan)??'-',$keyAxisY);
                        }else{
                            $keyAxisY=array_keys($Yaxis);
                            $indexYaxis=array_search(($f->ag_satuan??$f->satuan)??'-',$keyAxisY);
                        }

                        $series['d'.$id_dataset.'_'.$f->field_name]=[
                            'name'=>$f->ag_title??$f->name,
                            'satuan'=>$f->ag_satuan??$f->satuan,
                            'btn_active'=>'L',
                            'data'=>[],
                            'yAxis'=>$indexYaxis,
                            'dataLabels'=>[
                                'format'=>'{point.y:,.0f} ('.($f->ag_satuan??$f->satuan).') <br> {point.jumlah_pemda} Pemda',

                            ],
                            "tooltip"=>[
                                "valueSuffix"=>' ('.($f->ag_satuan??$f->satuan).')',
                                'valuePrefix'=>"({point.jumlah_pemda} Pemda) "
                            ]
                        ];
                        
                    }

                   

                    $series['d'.$id_dataset.'_'.$f->field_name]['data'][]=[
                        'name'=>'Nasional',
                        'key_lokus'=>'N',
                        'jumlah_pemda'=>(float)$nasional['jumlah_pemda'],
                        // 'yAxis'=>$indexYaxis,
                        'y'=>(float)$nasional['d'.$id_dataset.'_'.$f->field_name]??0
                    ];
                    $series['d'.$id_dataset.'_'.$f->field_name]['data'][]=[
                        'name'=>'Nasional Non NUWAS',
                        'key_lokus'=>'N-N',
                        'jumlah_pemda'=>(float)$nasional_non_nuwas['jumlah_pemda'],
                        // 'yAxis'=>$indexYaxis,
                        'y'=>(float)$nasional_non_nuwas['d'.$id_dataset.'_'.$f->field_name]??0
                    ];

                    $series['d'.$id_dataset.'_'.$f->field_name]['data'][]=[
                        'name'=>'Longlist NUWSP',
                        'key_lokus'=>'L',
                        // 'yAxis'=>$indexYaxis,
                        'jumlah_pemda'=>(float)$longlist['jumlah_pemda'],


                        'y'=>(float)$longlist['d'.$id_dataset.'_'.$f->field_name]??0
                    ];
                    $series['d'.$id_dataset.'_'.$f->field_name]['data'][]=[
                        'name'=>'Sortlist NUWSP',
                        'key_lokus'=>'S',
                        // 'yAxis'=>$indexYaxis,
                        'jumlah_pemda'=>(float)$sortlist['jumlah_pemda'],
                        'y'=>(float)$sortlist['d'.$id_dataset.'_'.$f->field_name]??0
                    ];

                }

                $series=array_values($series);

                return [
                    'series'=>$series,
                    'yAxis'=>array_values($Yaxis),
                    'columns'=>$fs,
                    'where'=>$where,

                ];
                
            }

        }else{
            return false;
        }
        // 


    }

    public  static function lev_nas($tahun,$id_dataset,$type,$public=true,$optional_where='',Request $request){

        $dataset=DB::table('master.dataset')->find($id_dataset);
        $field=DB::table('master.dataset_data as sd')
        ->join('master.data as d','d.id','=','sd.id_data')
        ->where([[
            'sd.ag_lev_nas','!=',null],
            ['sd.id_dataset',$id_dataset]])
        ->Orwhere([
            ['sd.ag_lev_nas_group','=',true],
            ['sd.id_dataset','=',$id_dataset]
        ])
        ->selectRaw('sd.prioritas,sd.ag_lev_nas_group as is_group,sd.field_name,sd.ag_lev_nas as ag,sd.ag_lev_nas_title as ag_title,d.satuan as ag_satuan,d.*')
        ->orderBy('sd.index','asc')->get();

        if($field AND  $dataset){
            if(strtoupper($dataset->klasifikasi_1)=='DATA PEMDA'){
                $select=['d.tahun',"string_agg(std.status::text,',') as status_data"];
                $group=['d.tahun'];
                $order=['d.tahun'];
            }else{
                $select=['d.tahun',"string_agg(std.status::text,',') as status_data"];
                $group=['d.tahun'];
                $order=['d.tahun'];
            }

            $priority=['d.tahun'];
            foreach($field as $kf=>$f){
                if($f->ag){
                    $select[]=str_replace('@?','d.'.$f->field_name,$f->ag).' as '.$f->field_name.", '".$f->ag_satuan."' as ".$f->field_name.'_satuan';
                }
                if($f->is_group){
                if($f->ag){
                    $group[]=$f->field_name;
                }else{
                    $group[]=$f->field_name;
                }
                }
                if($f->prioritas){
                    $priority[]=str_replace('@?','d.'.$f->field_name,$f->ag).' as '.$f->field_name.", '".$f->ag_satuan."' as ".$f->field_name.'_satuan';
                }
            }
            
            // dd($group,$select,$priority);

            if($type=='table'){
                $table_status='dts_'.$dataset->id;
                if(str_contains($dataset->table,'[TAHUN]')){
                    $dataset->table=str_replace('[TAHUN]',$tahun,$dataset->table);
                    $table_status='dts_'.$dataset->id.'_'.$tahun;
                }


           $exist_table=(int)DB::table('pg_tables')->where('tablename','=',$dataset->table)->where('schemaname','=','dataset')->count();
           if($exist_table){
                $data=DB::table('dataset.'.$dataset->table.' as d')
                ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
                ->groupByRaw(implode(',',$group));
                $data=$data->leftjoin(DB::raw('status_data.'.$table_status.' as std'),[['std.kodepemda','=','d.kodepemda'],['std.tahun','=','d.tahun']]);

                if($optional_where){
                    $data->whereRaw($optional_where);
                }

                if(strtoupper($dataset->klasifikasi_1)=='DATA PEMDA'){

                }else{
                    $data=$data->join('master.master_bu as bu','bu.id','=','d.kodebu');
                }

                if($public){
                    $data=$data->where('std.status',2)
                    ->selectRaw(implode(',',$select));
                    foreach($order??[] as $or){
                        $data=$data->orderBy($or,'asc');
                    }

                    $data=$data->get();
                    
                }else{
                    $data=$data->selectRaw(implode(',',$select));
                    foreach($order??[] as $or){
                        $data=$data->orderBy($or,'asc');
                    }

                    $data=$data->get();
                }
                return [
                    'columns'=>$field,
                    'data'=>$data,
                ];
                
           }else{
            return [];
           }
        }

        if($type=='series'){
            $group=['d.tahun'];
            $priority[]='count(distinct(mp.kodepemda)) as jumlah_pemda';
            $series=[

            ];

            $arth=[$tahun-5,$tahun-4,$tahun-3,$tahun-2,$tahun-1,$tahun,$tahun+1,$tahun+2,$tahun+3];
            if(str_contains($dataset->table,'[TAHUN]')){
                foreach($arth as $th){
                    $tbd=str_replace('[TAHUN]',$th,$dataset->table);
                    $tbst='dts_'.$dataset->id.'_'.$th;
                    
                    $exist_table=(int)DB::table('pg_tables')->where('tablename','=',$tbd)->where('schemaname','=','dataset')->count();
                    

                    if($exist_table){
                        $data=DB::table('dataset.'.$tbd.' as d')
                        ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
                        ->leftjoin(DB::raw('status_data.'.$tbst.' as std'),[['std.kodepemda','=','d.kodepemda'],['std.tahun','=','d.tahun']])
                        ->groupByRaw(implode(',',$group))
                        ->orderBy('d.tahun','asc');

                        if($optional_where){
                            $data->whereRaw($optional_where);
                        }
                       
                        if(strtoupper($dataset->klasifikasi_1)=='DATA PEMDA'){
                            
                        }else{
                            $data=$data->join('master.master_bu as bu','bu.id','=','d.kodebu');
                        }


    
                        if((Boolean)$public==true){

                            $data=$data->where('std.status',2)
                            ->selectRaw(implode(',',$priority))
                            ->get();
                        }else{
                            $data=$data->selectRaw(implode(',',$priority))->get();
                        }
                      

                        foreach($field as $p){
                            if($p->prioritas){
                               
                               foreach($data as $d){
                                if(!isset($series[$tahun])){
                                    $series[$tahun]=[
                                     
                                        'series'=>[]
                                    ];
                                    
                                  
                                }

                                if(!isset($series[$tahun]['series'][$p->field_name])){
                                    $series[$tahun]['series'][$p->field_name]=[
                                        'name'=>$p->ag_title??$d->name.' ('.($p->ag_satuan??$p->satuan).')',
                                        'satuan'=>($p->ag_satuan??$p->satuan),  
                                        'id_data'=>$p->id,
                                        'data'=>[],
                                    ];

                                }
                                    
                                   
                                $d=(Array)$d;
                                $series[$tahun]['series'][$p->field_name]['data'][]=[
                                    'name'=>$d['tahun'],
                                    'y'=>(float)$d[$p->field_name],
                                    'jumlah_pemda'=>(float)$d['jumlah_pemda']

                                ];
                               }
                                
                                
                            }
                            
                        }

                        
                    }
                }

                foreach($series as $ks=>$s){
                    $series[$ks]['series']=array_values( $series[$ks]['series']);
                }

                return [
                    'dataset'=>[
                        'name'=>$dataset->name,
                        'keterangan'=>$dataset->tujuan
                    ],
                    'columns'=>array_filter($field->toArray(),function($el){return $el->prioritas;}),
                    'data'=>array_values($series),
                ];

            }else{
                    $exist_table=(int)DB::table('pg_tables')->where('tablename','=',$dataset->table)->where('schemaname','=','dataset')->count();
                    if($exist_table){
                        $tbd=$dataset->table;
                        $tbst='dts_'.$dataset->id;

                        $data=DB::table('dataset.'.$tbd.' as d')
                        ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
                        ->where([
                            [
                                'd.tahun','<=',max($arth)
                            ],
                            [
                                'd.tahun','>=',min($arth)
                            ]
                        ])
                        ->groupByRaw(implode(',',$group))
                        ->orderBy('d.tahun','asc');
                        $data=$data->leftjoin(DB::raw('status_data.'.$tbst.' as std'),[['std.kodepemda','=','d.kodepemda'],['std.tahun','=','d.tahun']]);


                        if($optional_where){
                            $data->whereRaw($optional_where);
                        }
                    
                        if(strtoupper($dataset->klasifikasi_1)=='DATA PEMDA'){
                            
                        }else{
                            $data=$data->join('master.master_bu as bu','bu.id','=','d.kodebu');
                        }



                        if((Boolean)$public==true){

                            $data=$data->where('std.status',2)
                            ->selectRaw(implode(',',$priority))
                            ->get();
                        }else{
                            $data=$data->selectRaw(implode(',',$priority))->get();
                        }
                    

                        foreach($field as $p){
                            if($p->prioritas){
                            
                            foreach($data as $d){
                                if(!isset($series[$tahun])){
                                    $series[$tahun]=[
                                   
                                        'series'=>[]
                                    ];
                                    
                                
                                }

                                if(!isset($series[$tahun]['series'][$p->field_name])){
                                    $series[$tahun]['series'][$p->field_name]=[
                                        'name'=>$p->ag_title??$d->name.' ('.($p->ag_satuan??$p->satuan).')',
                                        'satuan'=>($p->ag_satuan??$p->satuan),  
                                        'id_data'=>$p->id,
                                        'data'=>[],
                                    ];

                                }
                                    
                                
                                $d=(Array)$d;
                                $series[$tahun]['series'][$p->field_name]['data'][]=[
                                    'name'=>$d['tahun'],
                                    'y'=>(float)$d[$p->field_name],
                                    'jumlah_pemda'=>(float)$d['jumlah_pemda']
                                ];
                            }
                                
                                
                            }
                            
                        }

                        
                    }
                

                    foreach($series as $ks=>$s){
                        $series[$ks]['series']=array_values( $series[$ks]['series']);
                    }

                    return [
                        'dataset'=>[
                            'name'=>$dataset->name,
                            'keterangan'=>$dataset->tujuan
                        ],
                        'columns'=>array_filter($field->toArray(),function($el){return $el->prioritas;}),
                        'data'=>array_values($series),
                    ];

                
                
            }
            
        }

    }else{
        return [];
    }
    } 
    public  static function lev_pemda($tahun,$id_dataset,$type,$public=true,$optional_where='',Request $request){
        $dataset=DB::table('master.dataset')->find($id_dataset);
        $field=DB::table('master.dataset_data as sd')
        ->join('master.data as d','d.id','=','sd.id_data')
        ->where([[
            'sd.ag_lev_pemda','!=',null],
            ['sd.id_dataset',$id_dataset]])
        ->Orwhere([
            ['sd.ag_lev_pemda_group','=',true],
            ['sd.id_dataset','=',$id_dataset]
        ])
        ->selectRaw('sd.prioritas,sd.ag_lev_pemda_group as is_group,sd.field_name,sd.ag_lev_pemda as ag,sd.ag_lev_pemda_title as ag_title,d.satuan as ag_satuan,d.*')
        ->orderBy('sd.index','asc')->get();

        if($field AND  $dataset){
            if(strtoupper($dataset->klasifikasi_1)=='DATA PEMDA'){
                $select=['d.kodepemda','MAX(mp.nama_pemda) as nama_pemda','d.tahun',"string_agg(std.status::text,',') as status_data"];
                $group=['d.kodepemda','d.tahun'];
                $order=['d.kodepemda','d.tahun'];


            }else if(strtoupper($dataset->klasifikasi_1)=='DATA BUMDAM'){
                $select=['d.kodepemda','MAX(mp.nama_pemda) as nama_pemda','d.kodebu','MAX(mbu.nama) as nama_bu','d.tahun',"string_agg(std.status::text,',') as status_data" ];
                $group=['d.kodepemda','d.tahun','d.kodebu'];
                $order=['d.kodepemda','d.tahun','d.kodebu'];

            }else{
            }

            $priority=['d.tahun'];
            foreach($field as $kf=>$f){
                if($f->ag){
                    $select[]=str_replace('@?','d.'.$f->field_name,$f->ag).' as '.$f->field_name.", '".$f->ag_satuan."' as ".$f->field_name.'_satuan';
                }
                if($f->is_group){
                    if($f->ag){
                        $group[]=$f->field_name;
                    }else{
                        $select[]=$f->field_name;
                    }
                }
                if($f->prioritas){
                    $priority[]=str_replace('@?','d.'.$f->field_name,$f->ag).' as '.$f->field_name.", '".$f->ag_satuan."' as ".$f->field_name.'_satuan';
                }
            }
            

            if($type=='table'){
                $table_status='dts_'.$dataset->id;
                if(str_contains($dataset->table,'[TAHUN]')){
                    $dataset->table=str_replace('[TAHUN]',$tahun,$dataset->table);
                    $table_status='dts_'.$dataset->id.'_'.$tahun;
                }


            $exist_table=(int)DB::table('pg_tables')->where('tablename','=',$dataset->table)->where('schemaname','=','dataset')->count();
            if($exist_table){
                    $data=DB::table('dataset.'.$dataset->table.' as d')
                    ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
                    ->groupByRaw(implode(',',$group));
                    $data=$data->leftJoin(DB::raw('status_data.'.$table_status.' as std'),[['std.kodepemda','=','d.kodepemda'],['std.tahun','=','d.tahun']]);


                    if($optional_where){
                        $data->whereRaw($optional_where);
                    }

                    if(strtoupper($dataset->klasifikasi_1)=='DATA PEMDA'){

                    }else{
                        $data=$data->join('master.master_bu as mbu','mbu.id','=','d.kodebu');
                        return $dataset->name;
                    }

                    if($public){
                        $data=$data->where('std.status',2)
                        ->selectRaw(implode(',',$select));
                        foreach($order??[] as $or){
                            $data=$data->orderBy($or,'asc');
                        }

                        $data=$data->get();
                        
                    }else{
                        $data=$data->selectRaw(implode(',',$select));
                        foreach($order??[] as $or){
                            $data=$data->orderBy($or,'asc');
                        }

                        $data=$data->get();
                    }
                    return [
                        'columns'=>$field,
                        'data'=>$data,
                    ];
                    
            }else{
                return [];
            }
            }

            if($type=='series'){
                $group=['d.tahun'];
                $group=['d.kodepemda','d.tahun'];
                $priority[]='max(mp.nama_pemda) as nama_pemda,d.kodepemda';

                $series=[

                ];

                $arth=[$tahun-5,$tahun-4,$tahun-3,$tahun-2,$tahun-1,$tahun,$tahun+1,$tahun+2,$tahun+3];
                if(str_contains($dataset->table,'[TAHUN]')){
                    foreach($arth as $th){
                        $tbd=str_replace('[TAHUN]',$th,$dataset->table);
                        $tbst='dts_'.$dataset->id.'_'.$th;
                        
                        $exist_table=(int)DB::table('pg_tables')->where('tablename','=',$tbd)->where('schemaname','=','dataset')->count();
                        

                        if($exist_table){
                            $data=DB::table('dataset.'.$tbd.' as d')
                            ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
                            ->groupByRaw(implode(',',$group));
                            if($optional_where){
                                $data->whereRaw($optional_where);
                            }
                        
                            if(strtoupper($dataset->klasifikasi_1)=='DATA PEMDA'){
                                
                            }else{
                                $data=$data->join('master.master_bu as bu','bu.id','=','d.kodebu');
                            }


        
                            if((Boolean)$public==true){

                                $data=$data->join(DB::raw('status_data.'.$tbst.' as std'),[['std.kodepemda','=','d.kodepemda'],['std.tahun','=','d.tahun']])
                                ->where('std.status',2)
                                ->selectRaw(implode(',',$priority))
                                ->get();
                            }else{
                                $data=$data->leftjoin(DB::raw('status_data.'.$tbst.' as std'),[['std.kodepemda','=','d.kodepemda'],['std.tahun','=','d.tahun']]);
                                $data=$data->selectRaw(implode(',',$priority))->get();
                            }
                        

                            foreach($field as $p){
                                if($p->prioritas){
                                
                                foreach($data as $d){
                                    if(!isset($series[$d->kodepemda])){
                                        $series[$d->kodepemda]=[
                                            'nama_pemda'=>$d->nama_pemda,
                                            'kodepemda'=>$d->kodepemda,
                                            'series'=>[]
                                        ];
                                        
                                    
                                    }

                                    if(!isset($series[$d->kodepemda]['series'][$p->field_name])){
                                        $series[$d->kodepemda]['series'][$p->field_name]=[
                                            'name'=>$p->ag_title??$d->name.' ('.($p->ag_satuan??$p->satuan).')',
                                            'satuan'=>($p->ag_satuan??$p->satuan),  
                                            'id_data'=>$p->id,
                                            'data'=>[],
                                        ];

                                    }
                                        
                                    $dx=(Array)$d;
                                    $series[$dx['kodepemda']]['series'][$p->field_name]['data'][]=[
                                        'name'=>$dx['tahun'],
                                        
                                        'y'=>(float)$dx[$p->field_name],
                                    ];
                                }
                                    
                                    
                                }
                                
                            }

                            
                        }
                    }

                    foreach($series as $ks=>$s){
                        $series[$ks]['series']=array_values( $series[$ks]['series']);
                    }

                    return [
                        'dataset'=>[
                            'name'=>$dataset->name,
                            'keterangan'=>$dataset->tujuan
                        ],
                        'columns'=>array_filter($field->toArray(),function($el){return $el->prioritas;}),
                        'data'=>array_values($series),
                    ];

                }else{
                        $exist_table=(int)DB::table('pg_tables')->where('tablename','=',$dataset->table)->where('schemaname','=','dataset')->count();
                        if($exist_table){
                            $tbd=$dataset->table;
                            $tbst='dts_'.$dataset->id;

                            $data=DB::table('dataset.'.$tbd.' as d')
                            ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
                            ->where([
                                [
                                    'd.tahun','<=',max($arth)
                                ],
                                [
                                    'd.tahun','>=',min($arth)
                                ]
                            ])
                            ->groupByRaw(implode(',',$group));
                            $data=$data->leftjoin(DB::raw('status_data.'.$tbst.' as std'),[['std.kodepemda','=','d.kodepemda'],['std.tahun','=','d.tahun']]);

                            if($optional_where){
                                $data->whereRaw($optional_where);
                            }
                        
                            if(strtoupper($dataset->klasifikasi_1)=='DATA PEMDA'){
                                
                            }else{
                                $data=$data->join('master.master_bu as bu','bu.id','=','d.kodebu');
                            }



                            if((Boolean)$public==true){

                                 $data=$data->where('std.status',2)
                                ->selectRaw(implode(',',$priority))
                                ->get();
                            }else{
                                $data=$data->selectRaw(implode(',',$priority))->get();
                            }
                        

                            foreach($field as $p){
                                if($p->prioritas){
                                
                                foreach($data as $d){
                                    if(!isset($series[$d->kodepemda])){
                                        $series[$d->kodepemda]=[
                                            'nama_pemda'=>$d->nama_pemda,
                                            'kodepemda'=>$d->kodepemda,
                                            'series'=>[]
                                        ];
                                        
                                    
                                    }

                                    if(!isset($series[$d->kodepemda]['series'][$p->field_name])){
                                        $series[$d->kodepemda]['series'][$p->field_name]=[
                                            'name'=>$p->ag_title??$p->name.' ('.($p->ag_satuan??$p->satuan).')',
                                            'satuan'=>($p->ag_satuan??$p->satuan),  
                                            'id_data'=>$p->id,
                                            'data'=>[],
                                        ];

                                    }
                                        
                                    
                                    $d=(Array)$d;
                                    $series[$d['kodepemda']]['series'][$p->field_name]['data'][]=[
                                        'name'=>$d['tahun'],
                                        'y'=>(float)$d[$p->field_name],
                                    ];
                                }
                                    
                                    
                                }
                                
                            }

                            
                        }
                    

                        foreach($series as $ks=>$s){
                            $series[$ks]['series']=array_values( $series[$ks]['series']);
                        }

                        return [
                            'dataset'=>[
                                'name'=>$dataset->name,
                                'keterangan'=>$dataset->tujuan
                            ],
                            'columns'=>array_filter($field->toArray(),function($el){return $el->prioritas;}),
                            'data'=>array_values($series),
                        ];

                    
                    
                }
                
            }

        }else{
            return [];
        }


    }
    
   
    public function getSeriesPemdaPublic($tahun,$id_dataset,$kodepemda,Request $request){
        $optional_where="d.kodepemda='".$kodepemda."'";
        return static::lev_pemda($tahun,$id_dataset,'series',true,$optional_where,$request);
    }

    public function getSeriesPemdaPenilaian($tahun,$id_dataset,$kodepemda,Request $request){
        $optional_where="d.kodepemda='".$kodepemda."' and std.status>=".$request->status;
        return static::lev_pemda($tahun,$id_dataset,'series',false,$optional_where,$request);
    }

    public function getTablePemdaPublic($tahun,$id_dataset,$kodepemda=null,Request $request){
        $optional_where='';
        if($kodepemda){
           $filter_pemda= ['=',$kodepemda];
            $optional_where="d.kodepemda = '".$kodepemda."'";

        }else{
           $filter_pemda= ['=',null];
        }
        $pemdas=FilterCtrl::filter_pemda($request->filter_regional??null,$request->filter_lokus==null?true:$request->filter_lokus,$request->filter_sortlist,$request->filter_tipe_bantuan,$request->filter_jenis_pemda??3,$filter_pemda);


        if(count($pemdas)){
            if($optional_where){
                $optional_where.=' and '.$optional_where="d.kodepemda in ('".implode("','",$pemdas)."')";
            }else{
                $optional_where=$optional_where="d.kodepemda in ('".implode("','",$pemdas)."')";
            }
        }
        return static::lev_data($tahun,$id_dataset,'table',true,$optional_where,$request);
    }

    public function getTablePemda($tahun,$id_dataset,$kodepemda=null,Request $request){
        $optional_where='';
        if($kodepemda){
           $filter_pemda= ['=',$kodepemda];
            $optional_where="d.kodepemda = '".$kodepemda."'";

        }else{
           $filter_pemda= ['=',null];
        }
        $pemdas=FilterCtrl::filter_pemda($request->filter_regional??null,$request->filter_lokus==null?true:$request->filter_lokus,$request->filter_sortlist,$request->filter_tipe_bantuan,$request->filter_jenis_pemda??3,$filter_pemda);

        if(count($pemdas)){
            if($optional_where){
                $optional_where.=' and '.$optional_where="d.kodepemda in ('".implode("','",$pemdas)."')";
            }else{
                $optional_where=$optional_where="d.kodepemda in ('".implode("','",$pemdas)."')";
            }
        }
        return static::lev_data($tahun,$id_dataset,'table',false,$optional_where,$request);
    }

    public function getSeriesPemda($tahun,$id_dataset,$kodepemda,Request $request){
        $optional_where="d.kodepemda='".$kodepemda."'";
        return static::lev_pemda($tahun,$id_dataset,'series',false,$optional_where,$request);
    }

    public function getSeries($tahun,$id_dataset){
        
        $dataset=DB::table('master.dataset')->find($id_dataset);

        $filter=FilterCtrl::buildFilterPemda($request);

        if($dataset->klasifikasi_2!='[]'){
            $filter['tipe_bantuan']=json_decode(str_replace('BANTUAN ','',strtoupper($dataset->klasifikasi_2)),true);
        }

        $pemdas=FilterCtrl::filter_pemda($filter['regional']??null,$filter['lokus'],$filter['sortlist'],$filter['tipe_bantuan'],$filter['jenis_pemda'],$filter['pemda']);
        $optional_where="mp.kodepemda in ('".implode("','",$pemdas)."')";

        return static::lev_nas($tahun,$id_dataset,'series',false,$optional_where);
    }

    public function getSeriesPublic($tahun,$id_dataset,Request $request){
        $dataset=DB::table('master.dataset')->find($id_dataset);

        if($dataset->klasifikasi_2!='[]'){
            $request->filter_tipe_bantuan=json_decode(str_replace('BANTUAN ','',strtoupper($dataset->klasifikasi_2)),true);
        }

        $pemdas=FilterCtrl::filter_pemda($request->filter_regional??null,$request->filter_lokus==null?true:$request->filter_lokus,$request->filter_sortlist,$request->filter_tipe_bantuan,$request->filter_jenis_pemda??3,['=',null]);
       
        $optional_where="mp.kodepemda in ('".implode("','",$pemdas)."')";


        $a=static::lev_nas($tahun,$id_dataset,'series',true,$optional_where,$request);
        $a['pemda_list']=$pemdas;
        return $a;
    }

    public function getStatePemda($tahun,$id_dataset,Request $request){
        $dataset=DB::table('master.dataset')->find($id_dataset);

        $filter=FilterCtrl::buildFilterPemda($request);

        if($dataset->klasifikasi_2!='[]'){
            $filter['tipe_bantuan']=json_decode(str_replace('BANTUAN ','',strtoupper($dataset->klasifikasi_2)),true);
        }

        $pemdas=FilterCtrl::filter_pemda($filter['regional']??null,$filter['lokus'],$filter['sortlist'],$filter['tipe_bantuan'],$filter['jenis_pemda'],$filter['pemda']);
        $optional_where="mp.kodepemda in ('".implode("','",$pemdas)."')";
        return static::state_data($tahun,$id_dataset,$optional_where,$request);

        
    }

    public function getStatePemdaPublic($tahun,$id_dataset,Request $request){
        $dataset=DB::table('master.dataset')->find($id_dataset);

        if($dataset->klasifikasi_2!='[]'){
            $request->filter_tipe_bantuan=json_decode(str_replace('BANTUAN ','',strtoupper($dataset->klasifikasi_2)),true);
        }

        $pemdas=FilterCtrl::filter_pemda($request->filter_regional??null,$request->filter_lokus==null?true:$request->filter_lokus,$request->filter_sortlist,$request->filter_tipe_bantuan,$request->filter_jenis_pemda??3,['=',null]);
       
       
        $optional_where="mp.kodepemda in ('".implode("','",$pemdas)."')";
        return static::state_data($tahun,$id_dataset,$optional_where,$request);
        
    }


    
    



}
