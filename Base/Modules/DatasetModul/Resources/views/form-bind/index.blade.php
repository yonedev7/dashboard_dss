@extends('adminlte::page')

@section('content_header')
    <h4>{{$dataset->name}} Tahun {{$StahunRoute}}</h4>
    <p></p>
@stop

@section('content')
    <div id="app" class="px-2">
        <{!!$component!!} ref="root" :options="options" :permissions="permissions" :tahun-def="{{$StahunRoute}}" :dataset="dataset" :url-load-def="'{{route('api-web-mod.dataset.form-handle.load',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id])}}'" :list-pemda="list_pemda"></{!!$component!!}>
    </div>
@stop


@section('js')
<script>
Vue.prototype.$url_base_form='{{url('/')}}';
Vue.prototype.$url_base_form_api='/mod/(TAHUN)/module-dataset/bind-form/{{$dataset->id}}'

var app=new Vue({
    el:'#app',
    data:{
        dataset:<?=json_encode($dataset)?>,
        list_pemda:<?=json_encode($list_pemda)?>,
        permissions:<?=json_encode($permisions)?>,
        options:<?=json_encode($options)?>   
    }
});
</script>
@stop