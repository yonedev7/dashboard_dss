<div id="app_show_file_{{$dataset->id}}" class="fade ">
    <div class="p-3 rounded-left shadow-lg loading-wrapper" style="position:fixed; width:60vw; min-height:60vh; border:1px solid rgb(70, 70, 215); right:0; top:100px; z-index:999999; background:#fff;" v-if="show" >
        <div class="btn-group mb-2">
            <button @click="close()" class="btn bg-secondary btn-sm"><i class="fa fa-times"></i> </button>
            <button @click="openNewTab()" class="btn btn-success btn-sm "><i class="fa fa-arrow-right"></i> Buka Di Tab Baru</button>
            <a  :href="link_def" download="" class="btn btn-primary btn-sm"><i class="fa fa-download"></i> Download</a>
        </div>
        <hr>
        <iframe v-if="mode=='iframe'" allowfullscreen class="rounded" :src="link" frameborder="1" style="width:calc(100% - 30px); height:60vh; background-color:#dddd;"></iframe>
        <div v-else-if="mode=='img'"  class="rounded "  style="width:calc(100% - 30px); border:1px solid rgb(108, 50, 169); height:60vh; background-color:#dddd;">
            <img  :src="link" class="img-responsive" style="max-width: 100%;" alt="">
        </div>

    </div>
</div>


@push('js_push')
<script>
    var app_show_file_{{$dataset->id}}=new Vue({
        el:'#app_show_file_{{$dataset->id}}',
        data:{
            dataset:<?=json_encode($dataset)?>,
            link:null,
            link_def:null,
            mode:'',
            show:false,
        },
        created:function(){
            $('#app_show_file_{{$dataset->id}}').removeClass('fade');
            
        },
        methods:{
            lodedLink:function(link){
                this.link_def=link;
                var extensi=link.split('.');
                extensi=extensi[extensi.length-1];
                if(['pdf'].includes(extensi)){
                    this.mode='iframe';
                    this.link=link;
                }else if(['doc','docx','xls','xlsx','ppt','pptx',].includes(extensi)){
                    this.mode='iframe';
                    this.link='https://docs.google.com/gview?url='+link+'&embedded=true';

                }else if(['jpg','jpeg','png','gif'].includes(extensi)){
                    this.mode='img';

                    this.link=link;

                }else{
                    this.mode='iframe';
                    this.link=link;
                }
                this.show=true;
function endMove() {
    $(".loading-wrapper.p-3").removeClass('movable');
}
function startMove() {
    $('.movable').on('mousemove', function(event) {
        var thisX = event.pageX  - $(this).width() / 2,
            thisY = event.pageY - $(this).height() / 2 *0.1;

        $('.movable').offset({
            left: thisX,
            top: thisY
        });
    });
}
$(document).ready(function() {
    $(".loading-wrapper.p-3").on('mousedown', function() {
        $(this).addClass('movable');
        startMove();
    }).on('mouseup', function(e) {
        $(".loading-wrapper.p-3").removeClass('movable');
        endMove();
    });

});
document.addEventListener('mouseup', function() {
        $(".loading-wrapper.p-3").removeClass('movable');
      });
                
            },
            openNewTab(){
                window.open(this.link,'file-view-'+this.dataset.id);
            },
           
            close(){
                this.show=false;
            }
        }
    });
</script>


@endpush