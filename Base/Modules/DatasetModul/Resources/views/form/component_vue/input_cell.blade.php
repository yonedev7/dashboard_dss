@push('js')

<script>
    Vue.component('input-cell-yone',{
        props:{
            col:Object,
            item:Object,
            actual:Boolean,
            dataset:Object,
            showSubmit:Function
        },
        
        created:function(){
            if(this.actual){
                this.show_actual=this.actual;
            }
            var com={};
            
        },
        watch:{
            
        },
        data:function(){
            return {
                show_actual:true
            }
        },
        methods:{
            updateF:function(){
                this.$forceUpdate();
                if(this.showSubmit){
                    this.showSubmit();
                }
                
            },
            deleteFile:function(field_name,index){
                var file=this.item[field_name]||'[]';
                file=JSON.parse(file);
                file=file.splice(index,file.length>1?1:0);
                console.log(file,index);
                this.item[field_name]=JSON.stringify(file);
                this.$forceUpdate();
            },
            actual_value:function(val){
               return NumberFormat(val||0);
            },
            file_json:function(item='[]'){
                var file=JSON.parse(item);
                return file;
            },
            updateData(data,field_name){
                this.item[field_name]=data;  

            },
            uploadFile:function(ev,type,field_name){
                var self=this;
                if(ev.target.files[0]!=undefined){
                    this.$isLoading(true);
                    var ex=JSON.parse(this.item[field_name]||'[]');

                     var formData = new FormData();
                    const name = ev.target.files[0].name;
                    const lastDot = name.lastIndexOf('.');
                    const fileName = name.substring(0, lastDot);
                    formData.append("file", ev.target.files[0]);
                    req_ajax.post(('{{route('api-web-mod.dataset.form-handle.up_file',['tahun'=>$StahunRoute,'id_dataset'=>'@?','kodepemda'=>'@?'])}}').yoneReplaceParam('@?',[this.dataset.id,this.item.kodepemda]), formData, {
                        headers: {
                        'Content-Type': 'multipart/form-data'
                        }
                    }).then((res)=>{
                        
                        if(res.data){
                            if(type=='single'){
                            ex=(JSON.stringify([res.data]));
                            }else{
                                ex.push(res.data);
                                ex=JSON.stringify(ex);
                                
                            }
                        }
                    }).finally(()=>{
                        this.$isLoading(false);
                        var c=this.item;
                        c[field_name]=ex;
                        Object.assign(this.item,c);
                        this.updateF();
                        ev.target.value=null;
                    });
                }
                
            },
            showFile:function(link){
                var url='{{url('')}}/'+link;
                if( window['app_show_file_'+this.dataset.id]!=undefined){
                window['app_show_file_'+this.dataset.id].lodedLink(url);
                    
                }else{
                    alert( 'app_show_file_'+this.dataset.id+' component no found');
                }
            }
        },
        computed:{
            com_item(){
                return this.item;
            },
            com_options:function(){
                return JSON.parse(this.col.options||'[]');
            },
           
        },
        template:`
            <div v-if="col.type=='input-value'">
                <div v-if="col.tipe_nilai=='numeric'">
                    <div class="input-group ">
                        <input type="number" class="form-control" step="0.01" v-model.number="item[col.field_name]">
                        <div class="input-group-append">
                        <span class="input-group-text" id="basic-addon2">@{{col.satuan}}</span>
                        </div>
                    </div>
                    <span  v-if="show_actual" class="text-small text-primary"><small>Actual Value : @{{actual_value(item[col.field_name])}}</small></span>
                </div>
                <div v-else-if="col.tipe_nilai=='string'">
                    <div class="input-group ">
                        <input type="text" class="form-control" step="0.01" v-model.trim="item[col.field_name]">
                        <div class="input-group-append">
                        <span class="input-group-text" id="basic-addon2">@{{col.satuan}}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div v-else-if="col.type=='options'">
               <div v-if="col.classification">
                    <div class="input-group ">
                        <select class="form-control" v-model="item[col.field_name]">
                            <option :value="op.value" v-for="op in com_options">@{{op.tag}}</option>    
                        </select>
                        <div class="input-group-append">
                        <span class="input-group-text" id="basic-addon2">@{{col.satuan}}</span>
                        </div>
                    </div>
                </div>
                <div v-else>
                    <div class="input-group ">
                        <select class="form-control" v-model="item[col.field_name]">
                            <option :value="op.value" v-for="op in com_options">@{{op.tag}}</option>    
                        </select>
                        <div class="input-group-append">
                        <span class="input-group-text" id="basic-addon2">@{{col.satuan}}</span>
                        </div>
                    </div>
                </div>  
            </div>
            <div v-else-if="col.type=='single_file'">
               <input type="file" class="form-control" @change="uploadFile($event,'single',col.field_name)"></input> 
                <div class="d-flex mt-3">
                    <div class="btn-group mr-2" v-for="file,fi in file_json(item[col.field_name])">
                        <button type="button" @click="showFile(file)" class="btn btn-sm btn-primary ">File @{{fi+1}}</button>
                        <button type="button" class="btn btn-sm btn-danger" @click="deleteFile(col.field_name,fi)"><i class="fa fa-times"></i> </button>

                    </div> 
                 
                </div> 
            </div>
            <div v-else-if="col.type=='multy_file'" >
                <input type="file" class="form-control" @change="uploadFile($event,'multy',col.field_name)"></input>      
                <div class="d-flex mt-3">
                    <div class="btn-group mr-2" v-for="file,fi in file_json(item[col.field_name])">
                        <button type="button" @click="showFile(file)" class="btn btn-sm btn-primary ">File @{{fi+1}}</button>
                        <button type="button" class="btn btn-sm btn-danger" @click="deleteFile(col.field_name,fi)"><i class="fa fa-times"></i> </button>

                    </div> 
                 
                </div> 
            </div>
            <div v-else-if="col.type=='input-date'">
                <input type="date" class="form-control" v-model="item[col.field_name]" ></input>  
               
            </div>



       
        `
    });
</script>

@endpush