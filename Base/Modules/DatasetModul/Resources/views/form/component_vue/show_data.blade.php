@include('datasetmodul::form.component_vue.file_show',['dataset'=>(Object)['id'=>null]])

@push('js')
<script>
    Vue.component('row-cell-yone',{
        props:{
            col:Object,
            item:Object,
            actual:Boolean,
            dataset:Object,
            withSatuan:{
                type:Boolean,
                default:true
            },
            colorValue:{
                type:String,
                default:''
            },
            classExtends:{
                type:String,
                default:''
            },
        },
       
        created:function(){
            if(this.withSatuan==undefined){
                this.withSatuan=true;
            }
            if(this.colorValue==undefined){
                this.colorValue='';
            }

            if(this.classExtends==undefined){
                this.classExtends='';
            }

            console.log(this.classExtends);
            if(this.actual){
                this.show_actual=this.actual;
            }
            var com={};
            if(this.col.options){
                this.op=JSON.parse(this.col.options);
            }
            if(this.col.bind_view){
                this.bind=JSON.parse(this.col.bind_view);
            }
           
        },
        watch:{
            item:{
                deep:true,
                handler:function(el){
                }
            }
        },
        data:function(){
            return {
                op:[],
                bind:[],
                show_actual:true
            }
        },
        methods:{
            showSatuan:function(val){
                if(val=='-'){
                    return '';
                }else{
                    return '('+val+')';
                }
            },
            fileRender:function(val){
                var f=JSON.parse(val||'[]');
                f=f.map((el)=>{
                    if(el){
                        var ur=el.split('.');
                        var extensi=ur[ur.length-1];
                        var name=ur.splice(ur.length-1,1).join('');
                        return [el,name,extensi];
                    }else{
                        return [el,el,''];
                    }
                });
                return f;
            },
            valueOption:function(valueData){
                if(this.op.length){
                    var p=this.op.filter(el=>{
                            return el.value==valueData;
                    });

                    if(this.bind.length){
                        var bind=this.bind.filter(el=>{
                            var lj=el.logic.replaceAll('val',this.col.tipe_nilai!='numeric'?("'"+valueData+"'"):valueData);
                            var res=false;
                            try {
                                res=eval(lj);
                                if(res){
                                    return true;
                                }else{
                                    return false;
                                }
                            } catch (error) {
                                return false;
                            }

                        });

                        if(bind.length){
                            return bind[0].tag;
                        }else{

                            if(p.length){
                                return p[0].tag;
                            }else{
                                return valueData;
                            }
                        }

                    }else{
                       
                        if(p.length){
                            return p[0].tag;
                        }else{
                            return valueData;
                        }
                    }
                }else{
                    return '-';
                }
            },
            
            actual_value:function(val){
               return NumberFormat(val||0);
            },
            nFormat:function(VAL){
                if(!isNaN(parseFloat(VAL))){
                    return NumberFormat(parseFloat(VAL)||0);
                }
                
            },
            showFile:function(val){
                if(window['app_show_file_']!=undefined){
                    window['app_show_file_'].lodedLink("{{url('')}}/"+(val||''));
                }else{
                    alert('app_show_file_ not found');
                }
            }
            
        },
        computed:{
            classVal:function(){
                return (this.colorValue?'text-'+this.colorValue:'')+' '+this.classExtends;
            },
           
        },
        template:`
           <div>
                <div v-if="col.type=='input-value'">
                    <span :class="classVal" v-if="col.tipe_nilai=='numeric'">@{{nFormat(item[col.field_name])}} </span>
                    <span  :class="classVal" v-else-if="col.tipe_nilai=='string'" v-html="item[col.field_name||'-']"></span>  
                    <span v-if="withSatuan"> @{{showSatuan(col.satuan)}}</span>

                </div>
                <div v-else-if="col.type=='options'">
                    <span  :class="classVal" >@{{valueOption(item[col.field_name])}}</span>
                    <span v-if="withSatuan"> @{{showSatuan(col.satuan)}}</span>

                </div>
                <div v-else-if="col.type=='single_file'">
                    <button @click="showFile(file[0])" type="button" v-for="file,i in fileRender(item[col.field_name]) " class="btn btn-sm btn-primary">File @{{i+1}}. (@{{file[2]}})</button>
                    <span v-if="fileRender(item[col.field_name]).length==0" class="text-danger">Belum Terdapat File Yang Diupload</span>  
                </div>
                <div v-else-if="col.type=='multy_file'">
                    <button @click="showFile(file[0])" type="button" v-for="file,i in fileRender(item[col.field_name]) " class="btn btn-sm btn-primary mr-1">File @{{i+1}}. (@{{file[2]}})</button>  
                    <span v-if="fileRender(item[col.field_name]).length==0" class="text-danger">Belum Terdapat File Yang Diupload</span>  
                    
                </div>
            </div>
            
        `
    });

</script>

@endpush