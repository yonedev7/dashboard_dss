@extends('adminlte::page')
@section('content_header')
<div>
    <div class="d-flex" id="app_header">
        <section  class="flex-grow-1 d-flex" v-if="dataset.id">
           <div class="align-self-center">
                <h3><b>{{$page_meta['title']}} Tahun {{$StahunRoute}}</b></h3>
                <p><b>{!!$page_meta['keterangan']!!}</b></p>
                <v-show-meta-data-dataset :class-btn="'btn-success'" :dataset="dataset" :path-resources="com_path_kolom"></v-show-meta-data-dataset>
            </div>
           
        </section>
        <div class="d-flex flex-column rounded p-2 shadow" style=" background-color:#ffffff9e; " >
              
         <charts  :options="series_state_data"></charts>
         <small class="bg-warning p-1">*Data Dapat Berubah Sesuai Filter</small>
    
        </div>
    
       
    </div>
    
</div>
@stop

@section('content')
<script>
    var page_meta=<?=json_encode($page_meta)?>;
    var def_link='{{route('mod.dataset.detail',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id,'tw'=>4,'kodepemda'=>'@?'])}}';
</script>
@php
$com_share=['dataset'=>$dataset,'fingerprint_parent'=>$fingerprint];

if($datasetsandingan){
    $com_share['datasetsandingan']=$datasetsandingan;
}

@endphp

@include('datasetmodul::form.component_vue.input_cell',$com_share)
@include('datasetmodul::form.component_vue.show_data',$com_share)
@include('datasetmodul::form.status_data.verified',$com_share)
@include('datasetmodul::form.status_data.validate',$com_share)
@include('datasetmodul::form.status_data.rollback_state',$com_share)
@include('datasetmodul::form.component_vue.file_show',$com_share)

@if($component_load)
    
    @foreach($component_load as $c)
        @include($c,$com_share)
    @endforeach
@endif


<div id="app">

<div class="p-3">
   <div class="bg-white p-3 shadow rounded">
    <div class="row d-flex " >
        @php
        @endphp
        @if(count(array_intersect(['TACT-REGIONAL','PEMDA','BUMDAM'], Auth::User()->getRoleNames()->toArray()))==0)
        <div class="col-3" >
            <div class="">Regional</div>
            <select name="filter_regional" class="form-control"  v-model="filter.filter_regional" id="">
                <option value=""></option>
                <option value="I">I</option>
                <option value="II">II</option>
                <option value="III">III</option>
                <option value="IV">IV</option>
            </select>
        </div>
        @endif
        <div class="col-3"  >
            <div class="">Tahun Proyek</div>
            <select name="filter_sortlist" class="form-control"  v-model="filter.filter_sortlist" id="">
                <option value=""></option>
                <?php
                for($i=$StahunRoute+3;$i>=($StahunRoute-3);$i--){
                    echo '<option value="'.$i.'" >'.$i.'</option>';
                }
                ?>
                
            </select>
        </div>
        <div class="col-3"  >
            <div class="">Tipe Bantuan</div>
            <select name="filter_tipe_bantuan" class="form-control" v-model="filter.filter_tipe_bantuan" id="">
                <option value=""></option>
                <option value="STIMULAN">STIMULAN</option>
                <option value="PENDAMPINGAN">PENDAMPINGAN</option>
                <option value="BASIS KINERJA">BASIS KINERJA</option>
            </select>
        </div>
        
    </div>
    </div>
</div>
<div class="bg-primary p-3" >
    <h4><b>Rekap dari @{{data.length}} Pemda</b></h4>
    <hr>
    <div class="d-flex">
        <div class="col-4">
            <p class="m-0"><b>Jumlah Ketersian Pemda</b></p>
            <small>@{{(com_rekap.terdata.prop - ((com_rekap.terverifikasi.prop||0) + (com_rekap.tervalidasi.prop||0))) +  (com_rekap.terdata.kota - ((com_rekap.terverifikasi.kota||0) + (com_rekap.tervalidasi.kota||0)))}} Pemda (@{{com_rekap.terdata.prop - ((com_rekap.terverifikasi.prop||0) + (com_rekap.tervalidasi.prop||0))}} Provinsi, @{{com_rekap.terdata.kota - ((com_rekap.terverifikasi.kota||0) + (com_rekap.tervalidasi.kota||0))}} Kota/Kab)</small>
        </div>
        <div class="col-4">
            <p class="m-0"><b>Pemda Ter Verifikasi</b></p>
            <small>@{{com_rekap.terverifikasi.prop +com_rekap.terverifikasi.kota}} Pemda (@{{com_rekap.terverifikasi.prop}} Provinsi, @{{com_rekap.terverifikasi.kota}} Kota/Kab)</small>
        </div>
        <div class="col-4">
            <p class="m-0"><b>Pemda Ter Validasi</b></p>
            <small>@{{com_rekap.tervalidasi.prop+com_rekap.tervalidasi.kota  }} Pemda (@{{com_rekap.tervalidasi.prop}} Provinsi, @{{com_rekap.tervalidasi.kota}} Kota/Kab)</small>

        </div>
    </div>
</div>

<div class="p-3">
    <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <label for="">Status Data</label>
                    <select name="" id="" class="form-control" v-model="filter.status_data">
                        <option value=""></option>
                        <option value="1">TERVERIFIKASI</option>
                        <option value="2">TERVALIDASI</option>
                        <option value="3">TERINPUT</option>
                        <option value="4">BELUM TERDAPAT STATUS</option>

                    </select>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="">Cari Pemda</label>
                    <input type="text" class="form-control" v-model="filter.search">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="">Jenis Pemda</label>
                    <select name="" id="" class="form-control" v-model="filter.jenis_pemda">
                        <option value=""></option>
                        <option value="3">Provinsi & Kota/Kabupaten</option>
                        <option value="2">Kota/Kabupaten</option>
                        <option value="1">Provinsi</option>

                    </select>
                </div>
            </div>
        </div>
    <p><span class="badge bg-secondary">Jumlah Pemda Dalam Table : @{{list_pemda.length}}</span></p>
        <template v-if="data.length>0">
            <dts-edit-pelatihan ref="table_input_buld_pelatihan" class="mb-2" :list-pemda="bulkListPemda" v-if="dataset.id==138"  :dataset="dataset" :columns="columns"></dts-edit-pelatihan>

        </template>
    <div class="table-responsive">
        <table class="table-bordered table bg-white" id="table-list-pemda" v-if="data_load">
            <thead class="thead-dark">
                <tr>
                    <th>Aksi</th>
                    <th>Nama Pemda</th>
                    <th>Status Data</th>
                   <template v-if="dataset.klasifikasi_1.includes('PEMDA')">
                    <th class="text-uppercase" v-for="col,i in columns" >
                        @{{col.name}} @{{col.satuan!='-'?'('+col.satuan+')':''}}   
                    </th>
                   </template>

                   <template v-if="dataset.klasifikasi_1.includes('BUMDAM')">
                    <template v-for="cc in ['I','II','III']">
                        <th  class="text-uppercase" v-for="col,i in columns" >
                            <p>@{{'BUMDAM '+cc}}</p>
                            @{{col.name}} @{{col.satuan!='-'?'('+col.satuan+')':''}}   
                        </th>
                    </template>
                   </template>
                   

                  

                </tr>
               
               
                </thead>
                <tbody>
                    <template  v-for="item,i in list_pemda">
                   <tr :class="diffDay(item)?'bg-secondary':''">
                        <td :rowspan="dataset.klasifikasi_1.includes('BUMDAM')?2:1">
                            <div class="btn btn-group">
                               
                                <button class="btn btn-info btn-sm" @click="direct_component(item,'show')" ><i class="fa fa-eye"></i> Detail</button>
                                <button class="btn btn-warning btn-sm" v-if="item.permissions.includes('dts.'+{{$dataset->id}}+':edit-data')  &&  (item.status_data==null) " @click="direct_component(item,'edit')"><i class="fa fa-pen"></i> Edit</button>
                                <button  @click="ver_form(item)"  class="btn btn-success btn-sm" v-if="item.permissions.includes('dts.'+{{$dataset->id}}+':verified-data') &&  (item.status_data==null && item.exist_data)">Verifikasi</button>
                                <button  @click="val_form(item)" class="btn btn-primary btn-sm" v-if="item.permissions.includes('dts.'+{{$dataset->id}}+':validate-data') && (item.status_data==1)">Validasi</button>
                                <button  @click="rollback_form(item)" class="btn btn-danger btn-sm" v-if="item.permissions.includes('dts.'+{{$dataset->id}}+':rollback-data') && (item.status_data>=1)">Rollback</button>
                                <a  download :href="('{{route('mod.dataset.download.excel',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id,'kodepemda'=>'@?'])}}').yoneReplaceParam('@?',[item.kodepemda])" class="btn  btn-success"><i class=" fa fa-download"></i> Excel</a>

                                <button v-if="item.status_data==2" class="btn bg-teal">Lembar Pengesahan</button>

                            </div>
                        </td>
                        <td :rowspan="dataset.klasifikasi_1.includes('BUMDAM')?2:1" >@{{item.nama_pemda}}</td>
                        <td :rowspan="dataset.klasifikasi_1.includes('BUMDAM')?2:1">
                            <div  class="bg-primary rounded p-1" v-if="item.status_data==2">
                                <p class="mb-0"><b>Tervalidasi</b></p>
                            </div>
                            <div  class="bg-success rounded p-1"  v-else-if="item.status_data==1">
                                <p class="mb-0"><b>Terverifikasi</b></p>
                            </div>
                            <div  class="bg-warning rounded p-1"  v-else-if="item.exist_data">
                                <p class="mb-0"><b>Terdata</b></p>
                            </div>
                            <div v-else>
                                <p class="mb-0"><b>Belum Terdapat Status</b></p>

                            </div>
                            <span class=""><small>@{{item.status_data!=null?'Update Content : '+parseTime(item.updtaed_status):('Update Content : '+parseTime(item.updated_content))}}</small></span>


                        </td>
                        <template v-if="dataset.klasifikasi_1.includes('PEMDA')">
                            <td v-for="col,i in columns" v-if="dataset.klasifikasi_1.includes('PEMDA')">
                                <span class="bg-white badge"><row-cell-yone :with-satuan="false" :item="item" :col="col" :dataset="dataset"></row-cell-yone></span>
                            </td>
                        </template>

                        <template v-if="dataset.klasifikasi_1.includes('BUMDAM') && columns.length">

                        <template v-for="cc,kcc in ['bg-primary','bg-success','bg-warning']">
                            <td :colspan="columns.length" :class="cc">
                                <span v-if="item.data_bu[kcc]!=undefined"> @{{item.data_bu[kcc].nama_bu}}</span>
                            </td>
                        </template>
                        </template>
                       


                    </tr>
                    <tr v-if="dataset.klasifikasi_1.includes('BUMDAM') && columns.length">
                        <template v-for="cc,kcc in ['bg-primary','bg-success','bg-warning']">
                            <td v-for="ccc in columns" :class="cc">
                                <span v-if="item.data_bu[kcc]!=undefined">
                                    <row-cell-yone :with-satuan="false" :item="item.data_bu[kcc]" :col="ccc" :dataset="dataset"></row-cell-yone>
                                </span>
                                
                            </td>
                        </template>
                        
                        </tr>    
                    
                    </template>
                </tbody>
        </table>    
    </div>
</div>    
</div>



@stop

@section('js')
<script>

var app_header=new Vue({
    el:'#app_header',
    methods:{
        loadStateData:function(){
            var self=this;
            self.$isLoading(true);
            req_ajax.post(('{{route('api-web-mod.dataset.data.state',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id])}}'),this.filter).then(res=>{
               if(res.status==200){
                if(res.data['series']!=undefined){
                 self.series_state_data.series=res.data['series'];
                 self.series_state_data.subtitle.text=res.data['series'][0].name;
                }else{
                self.series_state_data.series=[];

                }
               }else{
                self.series_state_data.series=[];
               }

            }).finally(function(){
                self.$isLoading(false);
            });
        },
    },
    created:function(){
        this.loadStateData();
    },
    computed:{
        com_path_kolom:function(){
           return ('{{route("api.kolum_dataset",["id_dataset"=>'@?'])}}').yoneReplaceParam('@?',[this.dataset.id]);
        },
    },
    data:{
        dataset:<?=json_encode($dataset)?>,
        series_state_data:{
                chart:{
                    type:'pie',
                    height:200,
                    width:300,
                    style:{
                    
                    }
                },
                title:{
                    text:'',
                    enabled:false,
                },
                subtitle:{
                    text:'',
                    enabled:true
                },
                tooltip: {
                    pointFormat: '{point.percentage:.1f} %<br>Total: {point.y:.0f} PEMDA'
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                        enabled: true,
                        format: '{point.percentage:.1f} %<br>Total: {point.y:.0f} PEMDA',
                        distance: -10,
                        }
                    }
                },
                series:[]
        },

    }
});

var app=new Vue({
    el:'#app',
    created:function(){
        var self=this;
        if(!this.dataset.jenis_dataset){
            if(((this.dataset.klasifikasi_1||'').toUpperCase()=='DATA PEMDA')){
                self.$isLoading(true);

                req_ajax.post('{{route('api-web-mod.dataset.form-handle.load_columns',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id])}}').then(res=>{
                    if(res.status==200){
                        
                        self.columns=res.data;
                        

                    }

                }).finally(()=>{
                    self.$isLoading(false);
                    self.requestData();

                });
            }else{
                // self.$isLoading(true);
                self.requestData();


                req_ajax.post('{{route('api-web-mod.dataset.form-handle.load_columns',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id])}}').then(res=>{
                    if(res.status==200){
                        self.columns=res.data;
                    }

                }).finally(()=>{
                    self.$isLoading(false);

                });

            }
        }else{
            self.requestData();

        }

       self.loadStateData();
    },
    data:{
        data_load:false,
        components:<?=json_encode(array_map(function($el){return explode('/',$el);},$component_load))?>,
        dataset:<?=json_encode($dataset)?>,
        columns:[],
        dt_table:null,
        filter:{
            search:'',
            filter_tipe_bantuan:'',
            filter_regional:'',
            filter_sortlist:null,
            jenis_pemda:3,
            status_data:null,
        },
        rekap:{
            terverifikasi:{
                prop:0,
                kota:0,
            },
            tervalidasi:{
                prop:0,
                kota:0,
            },
            terdata:{
                prop:0,
                kota:0,
            }
        },
        data:[]
    },
    methods:{
        loadStateData:function(){
            window.app_header.loadStateData();
        },
        showDate:function(date){
            return  moment(date).format('YYYY-MM-DD');
        },
        
        ver_form:function(item){
            eval("window.app_verified_{{$dataset->id}}.loadData(item,'app');");
        },
        val_form:function(item){
            eval("window.app_validate_{{$dataset->id}}.loadData(item,'app');");
        },
        rollback_form:function(item){
            eval("window.app_rollback_{{$dataset->id}}.loadData(item,'app');");
        },
        diffDay:function(item){
            var tm=item.updated_content;
            if(item.status_data==1){
                tm=item.updated_status;
            }else if(item.status_data==2){
                tm=item.updated_status;
            }

            if(tm){
                tm=moment(tm);
                var n=moment();
               if(tm.isValid()){
                if(tm.diff(n,'days')==0){
                    return true;
                }else{
                    return false;
                }
               }else{
                   return false;
               }
                
            }else{
                return false;
            }

        },
        parseTime:function(time){
           var i= moment(time);

           if(i.isValid()){
               return i.format('Y/MM/DD');
           }else{
               return '-';
           }
        },
        init:function(){
            this.requestData();
            return true;
        },
        direct_component:function(item,method='edit'){
           if(method=='edit'){
                var name_colmponents=(this.components).filter(el=>{
                    return ['partial_single','partial_multy'].includes(el[1])
                });
           }else if(method=='show'){

                var name_colmponents=this.components.filter(el=>{
                    return ['show_pemda','show_bu'].includes(el[2])
                });


           }

            if(name_colmponents.length){

               address=(name_colmponents[0].join().replace(/,/g,'_').replaceAll('datasetmodul::form_',''));
                if(address){

                    if(!address.includes({{$dataset->id}})){
                        address+='_{{$dataset->id}}';
                    }
                    
                    if( window[address]!=undefined){
                        window[address].loadData(item,method,'app');
                    }else{
                        alert('component '+address+' tidak terdefinisi');
                    }

                
                }else{
                    if(method=='edit'){
                         window.location.href=window.def_link.yoneReplaceParam('@?',[item.kodepemda]);

                    }

                }

            }else{
                if(method=='edit'){
                         window.location.href=window.def_link.yoneReplaceParam('@?',[item.kodepemda]);

                    }

            }

        },
        requestData:function(){
            var self=this;
            req_ajax.post('{{route('api-web-mod.dataset.form-handle.load',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id])}}',this.filter).then(res=>{
                if(res.status==200){
                    self.data=res.data;
                    // setTimeout((ref) => {
                    //     if(ref.dt_table){
                    //         ref.dt_table.destroy();
                    //     }
                        
                    //     ref.dt_table=$('#table-list-pemda').DataTable();
                    // }, 800,self);
                }
            }).finally(function(){
                setTimeout((ref) => {
                ref.data_load=true;
                    
                }, 500,self);

            });
        }
    },
    watch:{
       
        com_pemda:function(){
        //     setTimeout((ref) => {
        //     if(ref.dt_table){
        //         ref.dt_table.destroy();
        //         ref.dt_table=null;
        //     }
            
        //     ref.dt_table=$('#table-list-pemda').DataTable();
        // }, 500,this);

        },
        "filter.filter_regional":function(){
            this.requestData()
            this.loadStateData();
        },
        "filter.filter_sortlist":function(){
            this.requestData()
            this.loadStateData();

        },
        "filter.filter_tipe_bantuan":function(){
            this.requestData()
            this.loadStateData();

        }
    },
    computed:{
        com_path_kolom:function(){
           return ('{{route("api.kolum_dataset",["id_dataset"=>'@?'])}}').yoneReplaceParam('@?',[this.dataset.id]);
        },
        com_rekap:function(){
            return {
                terverifikasi:{
                    prop:this.list_pemda.filter(el=>{
                        return el.status_data==1&&el.kodepemda.length==3;
                    }).length,
                    kota:this.list_pemda.filter(el=>{
                        return el.status_data==1&&el.kodepemda.length>3;
                    }).length
                },
                tervalidasi:{
                    prop:this.list_pemda.filter(el=>{
                        return el.status_data==2 && el.kodepemda.length==3;
                    }).length,
                    kota:this.list_pemda.filter(el=>{
                        return el.status_data==2 && el.kodepemda.length>3;
                    }).length
                },
                terdata:{
                    prop:this.list_pemda.filter(el=>{
                        return el.exist_data&&el.kodepemda.length==3;
                    }).length,
                    kota:this.list_pemda.filter(el=>{
                        return el.exist_data&&el.kodepemda.length>3;
                    }).length
                }
            }
        },
        list_pemda:function(){
            var pemda=this.data;
            if(this.filter.search){
                pemda=pemda.filter(el=>{
                    return el.nama_pemda.toLowerCase().includes(this.filter.search.toLowerCase());
                });
            }

            if(this.filter.jenis_pemda){
                pemda=pemda.filter(el=>{
                    if(this.filter.jenis_pemda==2){
                        return el.kodepemda.length>3;

                    }else if(this.filter.jenis_pemda==1){
                        return el.kodepemda.length==3;

                    }else{
                        return true;
                    }
                });
            }

            if(this.filter.status_data){
                pemda=pemda.filter(el=>{
                    if(this.filter.status_data<3){
                       return el.status_data==this.filter.status_data;
                    }else if(this.filter.status_data==3){
                        return el.exist_data && el.status_data==null;
                    }else{
                        return el.exist_data==0;
                    }
                });
                
            }

            if(this.dt_table){
            }else{
              
            }

            

            return pemda;
        },
        bulkListPemda:function(){
            var x= this.data.map((el)=>{
                return [el.kodepemda,el.nama_pemda];
            });
            return x;
        }
    }
});   
 
</script>

@stop