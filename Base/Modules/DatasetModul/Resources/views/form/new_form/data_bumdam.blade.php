@extends('adminlte::page')
@section('content_header')

<h4><b>{{$page_meta['title']}} Tahun {{$StahunRoute}}</b></h4>
@stop
@section('content')
<div id="app">
   
    <list-data :id_dataset="{{$dataset->id}}" name="{{$dataset->name}}" :tahun="{{$StahunRoute}}"></list-data>
</div>


@stop

@section('js')
<script src="{{url('js/form_dataset.js')}}"></script>

<script>
    Vue.component('list-data',window.FORM_BUMDAM);
    var app=new Vue({
        el:'#app',
       
    })

</script>
@stop