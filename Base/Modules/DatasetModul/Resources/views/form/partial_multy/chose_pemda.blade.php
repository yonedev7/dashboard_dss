<div id="partial_multy_chose_pemda_{{$dataset->id}}">
    <div class="modal fade" id="modal_partial_multy_chose_pemda_{{$dataset->id}}" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">@{{meta.dataset.name}}</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <p><b>@{{meta.nama_pemda}}</b></p>
             <div class="btn-group">
                 <button @click="onTab()" class="btn btn-primary">Buka Pada Tab Ini</button>
                 <button  @click="newTab()" class="btn btn-success">Buka Pada Tab Baru</button>

             </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
</div>

@push('js_push')
    <script>
        var partial_multy_chose_pemda_{{$dataset->id}}=new Vue({
            el:'#partial_multy_chose_pemda_{{$dataset->id}}',
            data:{
                meta:{
                    kodepemda:'',
                    nama_pemda:'',
                    component_parent:'',
                    tab_id:null,
                    fingerprint:'{{$fingerprint_parent}}',
                    dataset:<?=json_encode($dataset)?>

                }
            },
            methods:{
                newTab:function(){
                    
                },
                onTab:function(){
                    this.meta.tab_id= window.open(("{{route('mod.dataset.form.detail_multy',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id,'kodepemda'=>'@?'])}}").yoneReplaceParam('@?',[this.meta.kodepemda]),'_parent');
                },
                loadData:function(item,method,component_parent){
                    this.meta.kodepemda=item.kodepemda;
                    this.meta.nama_pemda=item.nama_pemda;
                    this.meta.component_parent=component_parent;
                    $('#modal_partial_multy_chose_pemda_{{$dataset->id}}').modal();
                    // this.meta.tab_id=window.open(("{{route('mod.dataset.form.detail_multy',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id,'kodepemda'=>'@?'])}}").yoneReplaceParam('@?',[this.meta.kodepemda])+'?parent_id={{$fingerprint_parent}}','input_multy_bu');

                }
            }
        })
    </script>
@endpush