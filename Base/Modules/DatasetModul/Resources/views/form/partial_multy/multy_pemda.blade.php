@extends('adminlte::page')


@section('content')
@include('datasetmodul::form.component_vue.input_cell',['dataset'=>$dataset])
@include('datasetmodul::form.component_vue.show_data',['dataset'=>$dataset])
@include('datasetmodul::form.component_vue.file_show',['dataset'=>$dataset])



<div id="app" >
    <div class="parallax pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
        <div class="container">
            <section >
                <h3><b>@{{meta.dataset.name}} Tahun {{$StahunRoute}}</b></h3>
                <p><b v-html="meta.dataset.tujuan"></b></p>
                <h5><b>@{{meta.pemda.nama_pemda}}</b></h5>
               
            </section>
          
        </div>
       
    </div>
  
    <div class="p-3">
       
          
        <button @click="addNew()" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Tambah Data</button>
        <data-table :headers="header" :ref="'pemda_data_'+meta.pemda.kodepemda"  class="table bg-white table-bordered p-2 mt-2" :columns="com_columns" :url="url" >
           <template slot="header" slot-scope="{ tableProps }">
            <thead class="thead-dark">
                <tr>
                    <th class="text-uppercase" v-for="col in com_columns">
                        <span class="text-uppercase">@{{col.label}} @{{col.satuan!='-'?('('+col.satuan+')'):''}}</span>
                        <span v-if="!(['xxx','xxxAksi'].includes(col.name))" class="badge badge-primary p-1 rounded-circle"> <i class="fa fa-info"></i></span>
                    </th>
                </tr>
            </thead>
           </template>
            <template slot="body" slot-scope="{ data }">
                <tbody >
                    <tr
                      :key="item.id"
                      v-for="item,index_data in data">
                      <td 
                        :key="column.name"
                        v-for="column in com_columns">
                            <span v-if="column.name!='xxxAksi'">
                                <row-cell-yone :item="item" :col="column" :dataset="meta.dataset"></row-cell-yone>
                            </span>
                            <span v-else>
                                <div class="btn-group">
                                    <button @click="editForm(item)" class="btn btn-sm btn-warning"><i class="fa fa-pen"></i> Edit</button>
                                    <button @click="hapusForm(item)" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i>    Hapus</button>

                                </div>
                            </span>
                            

                      </td>
                    </tr>
                  </tbody>
            </template>
        </data-table>
            
    

    </div>

    <div v-bind:class="'modal fade '+(form.method=='delete'?'modal-danger':'')"  v-if="form.data.length" id="form-data"  tabindex="-1" role="dialog">
        <div class="modal-dialog modal-full" role="document">
          <div class="modal-content">
            <div v-bind:class="'modal-header '+(form.method=='delete'?'bg-danger':'')" >
              <h5 class="modal-title">@{{form.method=='edit'?'Rubah Data':(form.method=='new'?'Tambah Data':'Hapus Data')}}</h5>
              <button @click="visible_modal=false" type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="d-flex align-items-stretch">
                    <div class="flex-grow-1">
                      <div style="max-height:70vh; overflow-x:scroll;">
                        <div class="tab-content" >
                            <div class="row" v-for="d,index in form.data">
                              <div class="col-4" v-for="colRow,c in chuckSplit(meta.columns,3)">
                                <div class="form-group"  v-for="col,f in colRow" @mouseover="meta.sandingan.autoguide?meta.sandingan.search=col.name:null">
                                    <template v-if="form.method!='delete'">
                                        <template v-if="!meta.sandingan.autoguide">
                                            <label class="text-uppercase" for="">@{{col.name}}</label>
                                            <input-cell-yone key="col.field_name" :dataset="meta.dataset" :item="form.data[index]" :col="col" ></input-cell-yone>
                                        </template>
        
                                        <template v-else>
                                            <v-dropdown :distance="6" :triggers="['hover', 'focus','click']">
                                                <label  class="text-uppercase" for="">@{{col.name}}</label>
                                                <input-cell-yone :dataset="meta.dataset"  :item="form.data[index]" :col="col"></input-cell-yone>
                                                
                                                <template #popper>
                                                pop
                                                </template>
                                                
                                            </v-dropdown>
                                        </template>
                                    </template>
                                    <template v-else>
                                        <label class="text-uppercase" for="">@{{col.name}}</label>
                                            <row-cell-yone  :class-extends="'text-success text-bold'" :with-satuan="false" class="text-success" :item="d" :col="col" :dataset="meta.dataset"></row-cell-yone>
                                    </template>
                                
                                </div>
                              </div>
                              <div class="col-12">
                               <template v-if="form.method!='delete'">
                                <div class="form-group" v-if="d">
                                    <div class="d-flex bg-dark rounded p-3">
                                      <p class="mb-0 flex-grow-1">Catatan</p>
                                      <span v-if="recognition!=null" class="text-danger mr-3">Start .... </span>
                                      <button :class="'btn btn-sm rounded-circle '+(recognition!=null?'bg-danger':'bg-primary')" @click="recognice_keterangan(0)"> <i class="fa fa-microphone"></i></button>
                                    </div>
                                    <textarea-autosize
                                    placeholder="Type something here..."
                                    ref="myTextarea"
                                    v-model="form.data[0].keterangan"
                                    :min-height="100"
                                    :max-height="500"
                                    class="form-control"
                                    >
                                      </textarea-autosize>
                                  </div>
                               </template>
                               <template v-else>
                                <hr>
                                <p class="mb-1"><b>Catatan</b></p>
                                <p class="px-3" style="background-color:aliceblue" v-html="form.data[0].keterangan">

                                </p>
                               </template>
    
                              </div>
                            </div>
                  
                        </div>
                      </div>
                    </div>
                   
                  </div>
                 
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" @click="submit">Kirim</button>
              <button type="button" class="btn btn-secondary" @click="visible_modal=false" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
   
    

    
</div>
@stop
@section('js')
<script>

    var app=new Vue({
        el:'#app',
        created:function(){
            this.header.authorization='Bearer '+window.xdss{{md5(url()->current())}}.token;
            var self=this;
            this.$isLoading(true);
            req_ajax.post('{{route('api-web-mod.dataset.form-handle.load_columns',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id])}}').then(res=>{
                if(res.status==200){
                self.meta.columns=res.data;
                    var def_data=new Object({});
                    res.data.forEach(el=>{
                        def_data[el.field_name]=null;
                    });
                    Object.assign(self.def_form.data,def_data);
                    self.ready=true;

                }

            }).finally(()=>{
                self.$isLoading(false);
            });
            this.url='{{route('api-web-mod.dataset.form-handle.multy.get-exist-data-pemda',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id,'kodepemda'=>$pemda->kodepemda])}}';

           
        },  
        computed:{
            com_data:function(){
                return this.form.data;
            },
            com_columns:function(){
                var columns=[
                    {
                        name:'xxxAksi',
                        label:'Aksi',
                        field_name:'xxx',
                        satuan:'-'
                        
                    }
                ];
                 this.meta.columns.forEach(el=>{
                    columns.push( {
                        'name':el.field_name,
                        'label':el.name,
                        'type':el.type,
                        'tipe_nilai':el.tipe_nilai,
                        'field_name':el.field_name,

                        'options':el.options||'[]',
                        'bind_view':el.bind_view||'[]',
                        'satuan':el.satuan,
                        'orderable':el.tipe_nilai=='numeric' && el.type=='input-value'?true:false


                    });

                    

                   
                });

                columns.push({
                        name:'xxx',
                        label:'Catatan',
                        field_name:'keterangan',
                        satuan:'-'

                })

                return columns;
            }
        },
        watch:{
            "meta.bu_index":function(){
                this.def_form.data.kodebu=this.data[this.meta.bu_index].kodebu;
            },
            "form.data":{
                immediate: true,
                handler(){
                    console.log('i',this.form.data);
                }
            
            }
        },
        methods:{
            check_permition_service:async function(){
            var permit=false;
             await navigator.permissions.query(
                  { name: 'microphone' }
              ).then(function(permissionStatus){
                  if(permissionStatus.state!='granted'){
                    permit=false;
                    return false;

                    alert('Microphone tidak diizinkan');
                  }else{
                    permit=true;
                    return true;
                  }
              })
              return permit;
          },
            recognice_keterangan:function(index){
            this.check_permition_service();
             if( true){
                var self=this;
                window.SpeeechRecognition=window.SpeeechRecognition||window.webkitSpeechRecognition||null;
                if(SpeeechRecognition){
                  const recognition= new SpeeechRecognition();
                  this.recognition=recognition;
                  recognition.interimResult=true;
                  recognition.lang = 'id-ID';
                  recognition.addEventListener("result",(e)=>{
                      if(self.form.data[index].keterangan==null || self.form.data[index].keterangan==''){
                        self.form.data[index].keterangan='';
                        prefix='';
                      }else{
                        prefix=', ';
                      }
                      self.recognition=null;
                      self.form.data[index].keterangan+=prefix+e.results[0][0].transcript;
                  });
                  recognition.start();
                }else{
                  alert('Service Pembuatan Catatan Tidak Tersedia, Silahkan Mengunakan Chrome / Mozilla  Terbaru');
                }
             
              }
            },
            nFormat:function(num){
              return NumberFormat(num||0);
            },
            chuckSplit:function(input,size){
                var chunked=[];
                var x=Math.ceil(input.length / size);
                if(x){
                    return this.chunk(input,x);
                }else{
                    return [input];
                }
              },  
              chunk:function(input,size){
                  var chunked=[];
                  Array.from({length: Math.ceil(input.length / size)}, (val, i) => {
                      chunked.push(input.slice(i * size, i * size + size))
                  });
  
                  return chunked;
              },
              hapusForm:function(val){
                this.form.method='delete';
                    this.form.data=[val];
                    this.$forceUpdate();

                    setTimeout(() => {
                    $('#form-data').modal({backdrop: 'static', keyboard: false});
                        
                    }, 300);
              },
              addNew:function(){
                  var d=this.def_form.data;
                    d.id=null;
                    this.form.method='new';
                    this.form.data=[d];
                    this.$forceUpdate();

                setTimeout(() => {
                  $('#form-data').modal({backdrop: 'static', keyboard: false});
                    
                }, 300);
              },
              editForm:function(val){
               
                    this.form.method='edit';
                    this.form.data=[val];
                    this.$forceUpdate();

                    setTimeout(() => {
                    $('#form-data').modal({backdrop: 'static', keyboard: false});
                        
                    }, 300);
                },
              close:function(){
                $('#form-data').modal('hide');
              },
              submit:function(){
              this.$isLoading(true);
              var self=this;
              var url=('{{route('api-web-mod.dataset.form-handle.save',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id,'kodepemda'=>'@?'])}}').yoneReplaceParam('@?',[this.meta.pemda.kodepemda]);
              req_ajax.post(url,{
                kodepemda:this.meta.pemda.kodepemda,
                method:this.form.method,
                data:this.form.data
              }).then(res=>{
                if(res.status==200){
                  if(res.data.code==200){
                    window.Swal.fire({
                      title: 'Berhasil!',
                      text: res.data.message,
                      icon: 'success',
                      confirmButtonText: 'Close'
                    });
                  }else{
                    window.Swal.fire({
                      title: 'Error!',
                      text: res.data.message,
                      icon: 'error',
                      confirmButtonText: 'Close'
                    });
  
                  }
  
                }else{
                  alert('Terjadi Kegagalan Sistem');
                }
              }).finally(function(){
                self.$isLoading(false);
                 self.close();
                self.$refs['pemda_data_'+self.meta.pemda.kodepemda].getData();
              });
            },
        },
        data:{
            url:'',  
            header:{
                authorization:'Bearer '
            },
            visible_modal:false,
            ready:false,
            recognition:null,
            def_form:{
                method:'edit',
                data:{
                    'id':null,
                    'kodepemda':'{{$pemda->kodepemda}}',
                    'keterangan':''
                },
            },
            form:{
                kodebu:null,
                kodepemda:null,
                data:[]
            },
            meta:{
                sandingan:{
                    autoguide:false,
                },
                pemda:<?=json_encode($pemda)?>,
                dataset:<?=json_encode($dataset)?>,
                bu_index:0,
                columns:[{
                    name:'a',
                    label:'a',
                    satuan:'a'
                }]
            },
            data:[]

        }
    })
</script>


@stop