<div id="partial_multy_show_pemda_{{$dataset->id}}">
    <div class="modal fade " ref="modal_partial_multy_show_pemda_{{$dataset->id}}" id="modal_partial_multy_show_pemda_{{$dataset->id}}" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-full" role="document">
        <div class="modal-content">
          <div :class="'modal-header '+(meta.status_data==2?'bg-primary':(meta.status_data==1?'bg-success':''))">
            
            <h5 class="modal-title float-left text-upperca" >{{$dataset->name}} {{$StahunRoute}}</h5>
            <h5 class="m-0 text-uppercase text-left">
              @{{meta.nama_pemda}} <small> @{{(meta.status_data==2?'- Tervalidasi':(meta.status_data==1?'- Terverifikasi':('')))}}</small> <span><button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal"><i class="fa fa-times"></i> </button></span></h5>
          </div>
          <div class="modal-body" v-if="meta.columns">
           <template>
            <div class="d-flex">
              <charts :options="series_option" v-if="series_option.series.length"></charts>
              <charts :options="series_option_public" v-if="series_option_public.series.length"></charts>


            </div>
            <hr>
           </template>
            <data-table :headers="header"  :columns="com_columns" :url="com_url">
                <template slot="header" slot-scope="{ tableProps }">
                    <thead class="thead-dark">
                        <tr>
                            <th class="text-uppercase" v-for="col in com_columns">
                                <span class="text-uppercase">@{{col.label}} @{{col.satuan!='-'?('('+col.satuan+')'):''}}</span>
                                <span v-if="!(['xxx','xxxAksi'].includes(col.name))" class="badge badge-primary p-1 rounded-circle"> <i class="fa fa-info"></i></span>
                            </th>
                        </tr>
                    </thead>
                   </template>
                    <template slot="body" slot-scope="{ data }">
                        <tbody >
                            <tr
                              :key="item.id"
                              v-for="item,index_data in data">
                              <td 
                                :key="column.name"
                                v-for="column in com_columns">
                                    <span v-if="column.name!='xxxAksi'">
                                        <row-cell-yone :item="item" :col="column" :dataset="meta.dataset"></row-cell-yone>
                                    </span>
                                    
                              </td>
                            </tr>
                          </tbody>
                    </template>
            </data-table>
                
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  @push('js_push')
  
  
  <script >
      var partial_multy_show_pemda_{{$dataset->id}}=new Vue({
          el:'#partial_multy_show_pemda_{{$dataset->id}}',
          created:function(){
              var self=this;
              this.header.authorization='Bearer '+window.xdss{{md5(url()->current())}}.token;
              req_ajax.post('{{route('api-web-mod.dataset.form-handle.load_columns',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id])}}').then(res=>{
                if(res.status==200){
                  self.meta.columns=res.data;
                }
              })
          },
          data:{
            series_option:{
              chart:{
                type:'area'
              },
              title:{
                text:'DATA PROYEKSI'
              },
              subtitle:{
                text:''
              },
              plotOptions:{
                series:{
                  dataLabels:{
                    enabled:true
                  }
                }
              },
              xAxis:{
                type:'category',

              },
              series:[]
            },
            series_option_public:{
              chart:{
                type:'area'
              },
              plotOptions:{
                series:{
                  dataLabels:{
                    enabled:true
                  }
                }
              },
              title:{
                text:'DATA REALISASI'
              },
              subtitle:{
                text:''
              },
              xAxis:{
                type:'category',

              },
              series:[]
            },
            recognition:null,
            header:{
                authorization:'Bearer '
            },
            meta:{
                
              dataset:<?=json_encode($dataset)?>,
              dataset_sandingan:<?=isset($datasetsandingan)?$datasetsandingan->id:'null'?>,
              parent_component:'',
              changed:false,
              kodepemda:null,
              method:null,
              nama_pemda:null,
              status_data:null,
              form_active_index:-1,
              sandingan:{
                active:false,
                search:'',
                dataset:{
                  name:'',
                  id:0
                },
                kodepemda:'',
                autoguide:false,
                active_index:-1
              }
            },
            selected_field:['nilai_fcr'],
            edited_edited:['nilai_fcr'],
            columns:[],
            exist_data:[],
            exist_data_sandingan:[],
            data:[],
            data_sandingan:[]
          },
          methods:{
            loadData:function(item,method,parent_component){
                this.meta.method=method;
                this.meta.parent_component=parent_component;
                this.meta.kodepemda=item.kodepemda;
                this.meta.nama_pemda=item.nama_pemda;
                this.meta.status_data=item.status_data;
                this.series_option.subtitle.text=item.nama_pemda;
                this.series_option_public.subtitle.text=item.nama_pemda;
                this.loadSeries();
                this.show();
  
            },
            loadSeries:function(){
              this.$isLoading(true);
              var self=this;
              req_ajax.post(("{{route('api-web-mod.dataset.public.data.series.pemda',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id,'kodepemda'=>'@?'])}}").yoneReplaceParam('@?',[this.meta.kodepemda])).then(res=>{
                if(res.status==200){
                  if(res.data.data.length){
                    
                    self.series_option_public.series=res.data.data[0].series;
                  }else{
                    self.series_option_public.series=[];
                  }
                }else{
                  self.series_option_public.series=[];

                }

              }).finally(function(){
                self.$isLoading(false);
              });

              this.$isLoading(true);
              var self=this;
              req_ajax.post(("{{route('api-web-mod.dataset.data.series.pemda',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id,'kodepemda'=>'@?'])}}").yoneReplaceParam('@?',[this.meta.kodepemda])).then(res=>{
                if(res.status==200){
                  if(res.data.data.length){
                    
                    self.series_option.series=res.data.data[0].series;
                  }else{
                    self.series_option.series=[];

                  }
                }else{
                    self.series_option.series=[];

                  }

              }).finally(function(){
                self.$isLoading(false);
              });
            },
            close:function(){
            $('#modal_partial_multy_show_pemda_{{$dataset->id}}').modal('hide');
            },
        
          
              show:function(){
                $('#modal_partial_multy_show_pemda_{{$dataset->id}}').modal({backdrop: 'static', keyboard: false});
              },
          
              chuckSplit:function(input,size){
                var chunked=[];
                var x=Math.ceil(input.length / size);
                if(x){
                    return this.chunk(input,x);
                }else{
                    return [input];
                }
              },  
              chunk:function(input,size){
                  var chunked=[];
                  Array.from({length: Math.ceil(input.length / size)}, (val, i) => {
                      chunked.push(input.slice(i * size, i * size + size))
                  });
  
                  return chunked;
              },
           
              
          },
          computed:{
            com_url:function(){
                console.log(this.meta,'com_meta','{{route('api-web-mod.dataset.form-handle.multy.get-exist-data-pemda',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id,'kodepemda'=>'@?'])}}');
                return ('{{route('api-web-mod.dataset.form-handle.multy.get-exist-data-pemda',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id,'kodepemda'=>'@?'])}}').yoneReplaceParam('@?',[this.meta.kodepemda]);
            },
            com_columns:function(){
              var columns=[
                   
                ];

                 this.meta.columns.forEach(el=>{
                    columns.push( {
                      'name':el.field_name,
                        'label':el.name,
                        'type':el.type,
                        'tipe_nilai':el.tipe_nilai,
                        'field_name':el.field_name,
                        'options':el.options||'[]',
                        'bind_view':el.bind_view||'[]',
                        'satuan':el.satuan,
                        'orderable':el.tipe_nilai=='numeric' && el.type=='input-value'?true:false


                    });
                });
                columns.push({
                        name:'xxx',
                        label:'Catatan',
                        field_name:'keterangan',
                        satuan:'-'

                })

                return columns;
            }
          
          },
          watch:{
           
          }
          
      })
  
  </script>
  
  @endpush
  