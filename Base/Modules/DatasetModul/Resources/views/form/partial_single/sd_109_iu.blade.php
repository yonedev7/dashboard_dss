<div id="partial_single_sd_109_iu">
  <div class="modal fade " @click="cancleCloseBg($event)" id="modal_partial_single_sd_109_iu" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-full" role="document">
      <div class="modal-content">
        <div :class="'modal-header '+(meta.status_data==2?'bg-primary':(meta.status_data==1?'bg-success':''))">
          
        
          <h5 class="modal-title float-left" >From Perubahan {{$dataset->name}} {{$StahunRoute}}</h5>
          <h5 class="m-0 text-capitalize text-left">
            @{{meta.nama_pemda}} - <small> @{{(meta.status_data==2?'Tervalidasi':(meta.status_data==1?'Terverifikasi':''))}}</small></h5>
        </div>
        <div class="modal-body">
          <div class="d-flex align-items-stretch">
            <div style="width:30%; height: inherit;"  class=" mr-1 fade in show flex-column " v-if="meta.sandingan.active && meta.dataset_sandingan!=null">
              <h5 class="bg-dark rounded p-3 d-flex"><b class="flex-grow-1" >Sandingan Data</b>
              <button class="rounded-circle bg-primary btn btn-sm" @click="getSandinganIndex()"><span class="mdi mdi-reload"></span></button>
              </h5>
              <p class="m-0">@{{meta.sandingan.dataset.name}}</p>
              <p :class="'mb-2 '+(exist_data_sandingan[meta.sandingan.active_index]==undefined?'text-danger':'text-success')"><small>@{{exist_data_sandingan[meta.sandingan.active_index]!=undefined?exist_data_sandingan[meta.sandingan.active_index].nama_bu:'Data Bumdam Tidak Terdetekasi, Lakukan Refresh Sandingan Data !'}}</small></p>

              <div class="input-group mb-3">
                <input type="text" :disabled="meta.sandingan.autoguide" class="form-control" placeholder="Cari Data" v-model="meta.sandingan.search">
                <div class="input-group-append">
                  <button @click="meta.sandingan.autoguide?meta.sandingan.autoguide=false:meta.sandingan.autoguide=true" :class="'btn '+(meta.sandingan.autoguide?'btn-primary':'btn-secondary')  " type="button"><i v-if="meta.sandingan.autoguide" class="fa fa-check"></i>   Auto Guide</button>
                  <button class="btn btn-info"><i class="fa fa-info"></i></button>
                </div>
              </div>
              <div class="flex-grow-1 p-2" style="max-height:calc(60vh - 40px); overflow-x:scroll; background-color:rgb(206, 205, 205);">
                <div class="" v-for="col,i in com_column_sandingan">
                  
                  <template v-if="meta.sandingan.autoguide && meta.sandingan.search.length>0">
                    <div class="small-box bg-success position-sticky">
                      <div class="inner">
                      <h3>
                        <template v-if="meta.sandingan.active_index!=-1">
                          <template v-if="col.tipe_nilai=='numeric'">@{{nFormat(data_sandingan[meta.sandingan.active_index][col.field_name]||0)}}</template>
                          <template v-else=>@{{(data_sandingan[meta.sandingan.active_index][col.field_name]||'')}}</template>
      
                        </template>
                        <template v-else>
                          -
                        </template>

                        <sup style="font-size: 20px">@{{col.satuan}}</sup></h3>
                      <p>@{{col.name}} </p>
                      </div>
                      <div class="icon">
                      <i class="ion ion-stats-bars"></i>
                      </div>
                    </div>
                </template>
                <template v-else>
                  <div class="form-group">
                    <label for="">@{{col.name}} : </label>
                    <template v-if="meta.sandingan.active_index!=-1">
                      <template v-if="col.tipe_nilai=='numeric'">@{{nFormat(data_sandingan[meta.sandingan.active_index][col.field_name]||0)}}</template>
                      <template v-else=>@{{(data_sandingan[meta.sandingan.active_index][col.field_name]||'')}}</template>
                    </template>
                    <span>@{{col.satuan}}</span>
                    <hr>
  
                  </div>
                </template>
                </div>
              </div>
            </div>
            <div class="flex-grow-1">
              <template v-if="meta.sandingan.autoguide==true && meta.sandingan.active==false && meta.dataset_sandingan!=null">
                <h5 class="bg-dark rounded p-3 d-flex"><b class="flex-grow-1" >Sandingan Data</b>
                  <button class="rounded-circle bg-primary btn btn-sm" @click="getSandinganIndex()"><span class="mdi mdi-reload"></span></button>
                  </h5>
                <p class="m-0">@{{meta.sandingan.dataset.name}}</p>
                <p :class="'mb-2 '+(exist_data_sandingan[meta.sandingan.active_index]==undefined?'text-danger':'text-success')"><small>@{{exist_data_sandingan[meta.sandingan.active_index]!=undefined?exist_data_sandingan[meta.sandingan.active_index].nama_bu:'Data Bumdam Tidak Terdetekasi, Lakukan Refresh Sandingan Data !'}}</small></p>

  
              </template>
              <ul class="nav nav-tabs"  role="tablist">
                <li class="nav-item" role="presentation">
                  <button class="btn btn-primary" v-if="meta.dataset_sandingan!=null" @click="meta.sandingan.active?meta.sandingan.active=false:meta.sandingan.active=true" > <i class="fa fa-arrow-right"></i> Data Sandingan @{{meta.sandingan.autoguide?'- Auto':''}}</button>
                </li>
                <li class="nav-item" role="presentation" v-for="nav,i in exist_data">
                  <button :class="'nav-link '+(meta.form_active_index==i?'active':'') " @click="meta.form_active_index==i" >@{{nav.nama_bu}}</button>
                </li>
                
              </ul>
              <div style="max-height:70vh; overflow-x:scroll;">
                
                <div class="tab-content" >
                  <div :ll="i" v-if="meta.form_active_index==i" v-for="item,i in data" :class="'tab-pane pt-3 fade  in show active'"  >
                    <div class="row">
                      <div class="col-4" v-for="colRow,c in chuckSplit(columns,3)">
                        <div class="form-group"   v-for="form,f in colRow" @mouseover="meta.sandingan.autoguide?meta.sandingan.search=form.name:null">
                          <template v-if="!meta.sandingan.autoguide">
                            <label for="">@{{form.name}}</label>
                            <template v-if="form.tipe_nilai=='numeric'">
                              <div class="input-group ">
                                <input type="number" class="form-control" step="0.01" v-model="data[i][form.field_name]">

                                <div class="input-group-append">
                                  <span class="input-group-text" id="basic-addon2">@{{form.satuan}}</span>
                                </div>
                              </div>
                            <span class="text-small text-primary"><small>Actual Value : @{{nFormat(data[i][form.field_name])}}</small></span>


                            </template>
                          </template>
                          <template v-else>
                            <v-dropdown
                            :distance="6" :triggers="['hover', 'focus','click']"
                          >
                          <label for="">@{{form.name}}</label>
                          <template v-if="form.tipe_nilai=='numeric'">
                            <div class="input-group ">
                              <input type="number" class="form-control" step="0.01" v-model="data[i][form.field_name]">
                              <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon2">@{{form.satuan}}</span>
                              </div>
                              
                            </div>
                            <span class="text-small text-primary"><small>Actual Value : @{{nFormat(data[i][form.field_name])}}</small></span>


                          </template>
                          
                            <template #popper>
                              <template v-if="meta.sandingan.autoguide && meta.sandingan.search.length>0"  v-for="col,i in com_column_sandingan">
                                <div class="small-box bg-success position-sticky mb-0">
                                  <div class="inner">
                                  <h3>
                                    <template v-if="meta.sandingan.active_index!=-1">
                                      <template v-if="col.tipe_nilai=='numeric'">@{{nFormat(data_sandingan[meta.sandingan.active_index][col.field_name]||0)}}</template>
                                      <template v-else=>@{{(data_sandingan[meta.sandingan.active_index][col.field_name]||'')}}</template>
                                    </template>
                                    <template v-else>
                                      -
                                    </template>
            
                                    <sup style="font-size: 20px">@{{col.satuan}}</sup></h3>
                                    <p class="m-0">@{{col.name}} </p>
                                    <p class="m-0"><small>@{{meta.sandingan.dataset.name}}</small></p>
                                    <p class="mb-1"><small>@{{exist_data_sandingan[meta.sandingan.active_index]!=undefined?exist_data_sandingan[meta.sandingan.active_index].nama_bu:''}}</small></p>
                                  </div>
                                 
                                </div>
                              </template>
                            </template>
                            
                            </template>
                        </v-dropdown>
                          </template>
                          
                        </div>
                      </div>
                    </div>
                  </div>
          
                </div>
              </div>
            </div>
           
          </div>
          <div class="form-group" v-if="data[meta.form_active_index]!=undefined">
            <div class="d-flex bg-dark rounded p-3">
              <p class="mb-0 flex-grow-1">Catatan</p>
              <span v-if="recognition!=null" class="text-danger mr-3">Start .... </span>
              <button :class="'btn btn-sm rounded-circle '+(recognition!=null?'bg-danger':'bg-primary')" @click="recognice_keterangan()"> <i class="fa fa-microphone"></i></button>
            </div>
              
              <textarea name="" class="form-control" v-model="data[meta.form_active_index].keterangan" id="" ></textarea>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" @click="submit()" v-if="exist_data.length>0 && meta.changed==true" class="btn btn-primary">Kirim</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup & Hapus Perubahan Data</button>
        </div>
      </div>
    </div>
  </div>
</div>
@push('js_push')


<script >
    var partial_single_sd_109_iu=new Vue({
        el:'#partial_single_sd_109_iu',
        created:function(){
            var self=this;
          req_ajax.post('{{route('api-web-mod.dataset.form-handle.load_columns',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id])}}').then(res=>{
            if(res.status==200){
              self.columns=res.data;
            }
          })
        },
        data:{
          recognition:null,
          
          meta:{
            dataset:<?=json_encode($dataset)?>,
            dataset_sandingan:<?=isset($datasetsandingan)?$datasetsandingan->id:'null'?>,
            parent_component:'',
            changed:false,
            kodepemda:null,
            method:null,
            nama_pemda:null,
            status_data:null,
            form_active_index:-1,
            sandingan:{
              active:false,
              search:'',
              dataset:{
                name:'',
                id:0
              },
              kodebu:'',
              autoguide:false,
              active_index:-1
            }
          },
          selected_field:['nilai_fcr'],
          edited_edited:['nilai_fcr'],
          columns:[],
          exist_data:[],
          exist_data_sandingan:[],
          data:[],
          data_sandingan:[]
        },
        methods:{
          cancleCloseBg:function(ev){
            if(ev.cancelable){
              console.log('cancelable');
                ev.preventDefault();

            }else{
              console.log('not canclelable');

            }

          },
          close:function(){
            console.log('close');
            $('#modal_partial_single_sd_109_iu').modal('hide');
          },
          submit:function(){
            this.$isLoading(true);
            var self=this;
            var url=('{{route('api-web-mod.dataset.form-handle.save',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id,'kodepemda'=>'@?'])}}').yoneReplaceParam('@?',[this.meta.kodepemda]);
            req_ajax.post(url,{
              kodepemda:this.meta.kodepemda,
              method:this.meta.method,
              data:this.data
            }).then(res=>{
              if(res.status==200){
                if(res.data.code==200){
                  window.Swal.fire({
                    title: 'Berhasil!',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'Close'
                  });
                }else{
                  window.Swal.fire({
                    title: 'Error!',
                    text: res.data.message,
                    icon: 'error',
                    confirmButtonText: 'Close'
                  });

                }

              }else{
                alert('Terjadi Kegagalan Sistem');
              }
            }).finally(function(){
              self.$isLoading(false);
              var c=eval(self.meta.parent_component+'.init();');
              if(c){
                self.close();
              }

            });
          },
          check_permition_service:async function(){
            var permit=false;
             await navigator.permissions.query(
                  { name: 'microphone' }
              ).then(function(permissionStatus){
                  if(permissionStatus.state!='granted'){
                    permit=false;
                    return false;

                    alert('Microphone tidak diizinkan');
                  }else{
                    permit=true;
                    return true;
                  }
              })
              return permit;
          },
          recognice_keterangan:function(){
          this.check_permition_service();
           if( true){
              var self=this;
              window.SpeeechRecognition=window.SpeeechRecognition||window.webkitSpeechRecognition||null;
              if(SpeeechRecognition){
                const recognition= new SpeeechRecognition();
                this.recognition=recognition;
                recognition.interimResult=true;
                recognition.lang = 'id-ID';
                recognition.addEventListener("result",(e)=>{
                    if(self.data[self.meta.form_active_index].keterangan==null || self.data[self.meta.form_active_index].keterangan==''){
                      self.data[self.meta.form_active_index].keterangan='';
                      prefix='';
                    }else{
                      prefix=', ';
                    }
                    self.recognition=null;
                    self.data[self.meta.form_active_index].keterangan+=prefix+e.results[0][0].transcript;
                });
                recognition.start();
              }else{
                alert('Service Pembuatan Catatan Tidak Tersedia, Silahkan Mengunakan Chrome / Mozilla  Terbaru');
              }
           
            }
          },
          nFormat:function(num){
            return NumberFormat(num||0);
          },
          buildDataForm:function(context=null){
                var form=[];
                var self=this;
                var sources=[];
                this.meta.form_active_index=-1;
                if(context=='sandingan'){
                 sources= this.exist_data_sandingan;
                }else{
                  sources=this.exist_data;
                }
                sources.forEach(el=>{
                    var schema={
                      id:el['id'],
                      kodebu:el['kodebu'],
                      kodepemda:el['kodepemda'],
                      keterangan:el['keterangan'],
                    }
                    
                    this.columns.forEach(item=>{
                      if(el[item.field_name]!=undefined){
                        if(item.tipe_nilai=='numeric'){
                          schema[item.field_name]=parseFloat(el[item.field_name]);
                        }else{
                          schema[item.field_name]=(el[item.field_name]);
                        }
                      }
                    });

                    form.push(schema);
                });
                if(context=='sandingan'){
                  this.data_sandingan=form;
                }else{
                  this.data=form;
                }

                if(form.length){
                    this.meta.form_active_index=0;
                }

                setTimeout((ref) => {
                  ref.meta.changed=false;
                  
                }, 200,this);


            },
            getExistData:function(){
              var self=this;
              this.$isLoading(true);
              req_ajax.post(('{{route('api-web-mod.dataset.form-handle.get-exist-bu',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id,'kodepemda'=>'@?'])}}').yoneReplaceParam('@?',[this.meta.kodepemda]),{columns:this.columns.map(el=>el.field_name)}).then(res=>{
                if(res.status==200){
                  self.exist_data=res.data;
                }
              }).finally(()=>{
                this.buildDataForm();

              });
          
              // sansingan
              if(this.meta.dataset_sandingan){
                  req_ajax.post(('{{route('api-web-mod.dataset.form-handle.get-exist-bu',['tahun'=>$StahunRoute,'id_dataset'=>'@?','kodepemda'=>'@?'])}}').yoneReplaceParam('@?',[this.meta.dataset_sandingan,this.meta.kodepemda]),{columns:this.columns.map(el=>el.field_name),guide:true}).then(res=>{
                if(res.status==200){
                  Object.assign(self.meta.sandingan.dataset,res.data.dataset);
                  self.exist_data_sandingan=res.data.data;
                }
              }).finally(()=>{
                this.$isLoading(false);
                this.buildDataForm('sandingan');
                this.getSandinganIndex();
                this.show();  
              });

              }else{
                  this.$isLoading(false);
                  this.getSandinganIndex();
                  this.show();  
              }
              

            },
            loadData:function(item,method,parent_component){
              this.meta.method=method;
              this.meta.parent_component=parent_component;
              this.meta.kodepemda=item.kodepemda;
              this.meta.nama_pemda=item.nama_pemda;
              this.meta.status_data=item.status_data;


              this.getExistData();
            },
            show:function(){
              $('#modal_partial_single_sd_109_iu').modal({backdrop: 'static', keyboard: false});
            },
        
            chuckSplit:function(input,size){
              var chunked=[];
              var x=Math.ceil(input.length / size);
              if(x){
                  return this.chunk(input,x);
              }else{
                  return [input];
              }
            },  
            chunk:function(input,size){
                var chunked=[];
                Array.from({length: Math.ceil(input.length / size)}, (val, i) => {
                    chunked.push(input.slice(i * size, i * size + size))
                });

                return chunked;
            },
            getSandinganIndex:function(){
              var index=-1;
               var val=this.meta.form_active_index;
              if(val!=-1){
                if(this.data[val]!=undefined){
                  var sources=(this.data[val]!=undefined?this.data[val].kodebu:null)+'';
                  if(sources){
                    this.exist_data_sandingan.forEach((el,i)=>{
                      if((''+el.kodebu)==sources){
                        index=i;

                      }
                    });

                  }
              }
              this.meta.sandingan.active_index=index;

                }
            }
            
        },
        computed:{
          com_column_sandingan:function(){
            var column=this.columns;
            if(this.meta.sandingan.search){
                column=column.filter(el=>{
                return el.name.toLowerCase().includes(this.meta.sandingan.search.toLowerCase());
              });
            }
            return column;
          
          }
        },
        watch:{
          "meta.sandingan.autoguide":function(val){
            if(val){
              this.meta.sandingan.active=false;
            }

          },
          
          "meta.form_active_index":function(val){
            this.getSandinganIndex();
            
          },
          "exist_data":function(){
            this.getSandinganIndex();
          },
          "data_sandingan":function(){
            this.getSandinganIndex();
          },
          'data':{
            deep:true,
            handler:function(){
              this.meta.changed=true;
            }
          }
        }
        
    })

</script>

@endpush
