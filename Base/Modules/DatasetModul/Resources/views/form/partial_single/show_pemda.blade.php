<div id="partial_single_show_pemda_{{$dataset->id}}">
    <div class="modal fade " id="modal_partial_single_show_pemda_{{$dataset->id}}" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-full" role="document">
        <div class="modal-content">
          <div :class="'modal-header '+(meta.status_data==2?'bg-primary':(meta.status_data==1?'bg-success':''))">
            
            <h5 class="modal-title float-left" >{{$dataset->name}} {{$StahunRoute}}</h5>
            <h5 class="m-0 text-capitalize text-left">
              @{{meta.nama_pemda}} - <small> @{{(meta.status_data==2?'Tervalidasi':(meta.status_data==1?'Terverifikasi':''))}}</small></h5>
          </div>
          <div class="modal-body">
            <template>
              <div class="d-flex">
                <charts :options="series_option" class="col-6" v-if="series_option.series.length"></charts>
                <charts :options="series_option_public" class="col-6" v-if="series_option_public.series.length"></charts>
  
              </div>
              <hr>
             </template>
            <ul class="nav nav-tabs"  role="tablist">
                
              <li class="nav-item" role="presentation" v-for="nav,i in exist_data">
                <button :class="'nav-link '+(meta.form_active_index==i?'active':'') " @click="meta.form_active_index=i" >@{{nav.nama_pemda}}</button>
              </li>
              
            </ul>
            <div class="">
            
              <div style="max-height:70vh; overflow-x:scroll;">
                  
              <div class="row">
                <div class="col-12">
                  <div class="tab-content" >
                    <div :ll="i" v-if="meta.form_active_index==i" v-for="item,i in data" :class="'tab-pane pt-3 fade  in show active'"  >
                      <div class="row">
                        <div class="col-4" v-for="colRow,c in chuckSplit(columns,3)">
                          <div class="form-group"   v-for="form,f in colRow" >
                              <label for="">@{{form.name}} </label>
                              <row-cell-yone :class-extends="'text-success text-bold'" :item="item" :dataset="meta.dataset" :col="form"></row-cell-yone>
                          </div>
                            
                          </div>
                        </div>
                      </div>
                    </div>
            
                  </div>
                </div>
              </div>
              </div>
             
            <div class="form-group" v-if="data[meta.form_active_index]!=undefined">
              <div class="d-flex bg-dark rounded p-3">
                <p class="mb-0 flex-grow-1">Catatan</p>
                
              </div>
              <p>@{{data[meta.form_active_index].keterangan}}</p>
                
            </div>
                
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  @push('js_push')
  
  
  <script >
      var partial_single_show_pemda_{{$dataset->id}}=new Vue({
          el:'#partial_single_show_pemda_{{$dataset->id}}',
          created:function(){
              var self=this;
              req_ajax.post('{{route('api-web-mod.dataset.form-handle.load_columns',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id])}}').then(res=>{
                if(res.status==200){
                  self.columns=res.data;
                }
              })
          },
          data:{
            series_option:{
              chart:{
                type:'area'
              },
              title:{
                text:'PROYEKSI DATA'
              },
              subtitle:{
                text:''
              },
              plotOptions:{
                series:{
                  dataLabels:{
                    enabled:true
                  }
                }
              },
              xAxis:{
                type:'category',

              },
              series:[]
            },
            series_option_public:{
              chart:{
                type:'area'
              },
              plotOptions:{
                series:{
                  dataLabels:{
                    enabled:true
                  }
                }
              },
              title:{
                text:'DATA REALISASI'
              },
              subtitle:{
                text:''
              },
              xAxis:{
                type:'category',

              },
              series:[]
            },
            recognition:null,
            meta:{
              dataset:<?=json_encode($dataset)?>,

            dataset_sandingan:<?=isset($datasetsandingan)?$datasetsandingan->id:'null'?>,
              parent_component:'',
              changed:false,
              kodepemda:null,
              method:null,
              nama_pemda:null,
              status_data:null,
              form_active_index:-1,
              sandingan:{
                active:false,
                search:'',
                dataset:{
                  name:'',
                  id:0
                },
                kodepemda:'',
                autoguide:false,
                active_index:-1
              }
            },
            selected_field:['nilai_fcr'],
            edited_edited:['nilai_fcr'],
            columns:[],
            exist_data:[],
            exist_data_sandingan:[],
            data:[],
            data_sandingan:[]
          },
          methods:{
            loadSeries:function(){
              this.$isLoading(true);
              var self=this;
              req_ajax.post(("{{route('api-web-mod.dataset.public.data.series.pemda',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id,'kodepemda'=>'@?'])}}").yoneReplaceParam('@?',[this.meta.kodepemda])).then(res=>{
                if(res.status==200){
                  if(res.data.data.length){
                    
                    self.series_option_public.series=res.data.data[0].series;
                  }else{
                    self.series_option_public.series=[];
                  }
                }else{
                  self.series_option_public.series=[];

                }

              }).finally(function(){
                self.$isLoading(false);
              });

              this.$isLoading(true);
              var self=this;
              req_ajax.post(("{{route('api-web-mod.dataset.data.series.pemda',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id,'kodepemda'=>'@?'])}}").yoneReplaceParam('@?',[this.meta.kodepemda])).then(res=>{
                if(res.status==200){
                  if(res.data.data.length){
                    
                    self.series_option.series=res.data.data[0].series;
                  }else{
                    self.series_option.series=[];

                  }
                }else{
                    self.series_option.series=[];

                  }

              }).finally(function(){
                self.$isLoading(false);
              });
            },
            getExistData:function(){
                var self=this;
                this.$isLoading(true);
                req_ajax.post(('{{route('api-web-mod.dataset.form-handle.get-exist',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id,'kodepemda'=>'@?'])}}').yoneReplaceParam('@?',[this.meta.kodepemda]),{columns:this.columns.map(el=>el.field_name)}).then(res=>{
                  if(res.status==200){
                    self.exist_data=res.data;
                  }
                }).finally(()=>{
                  this.$isLoading(false);
                  this.buildDataForm();
                  this.show();
  
                });
            },
            close:function(){
              $('#modal_partial_single_show_pemda_{{$dataset->id}}').modal('hide');
            },
        
            buildDataForm:function(context=null){
              console.log(this.columns);
                  var form=[];
                  var self=this;
                  var sources=[];
                  this.meta.form_active_index=-1;
                  if(context=='sandingan'){
                   sources= this.exist_data_sandingan;
                  }else{
                    sources=this.exist_data;
                  }
                  sources.forEach(el=>{
                      var schema={
                        id:el['id'],
                        kodepemda:el['kodepemda'],
                        keterangan:el['keterangan'],
                      }
                      
                      this.columns.forEach(item=>{
                        if(el[item.field_name]!=undefined){
                          if(item.tipe_nilai=='numeric'){
                            schema[item.field_name]=parseFloat(el[item.field_name]);
                          }else{
                            schema[item.field_name]=(el[item.field_name]);
                          }
                        }
                      });
  
                      form.push(schema);
                  });
                  if(context=='sandingan'){
                    this.data_sandingan=form;
                  }else{
                    this.data=form;
                  }
  
                  if(form.length){
                      this.meta.form_active_index=0;
                  }
  
                  setTimeout((ref) => {
                    ref.meta.changed=false;
                    
                  }, 200,this);
  
  
              },
           
              loadData:function(item,method,parent_component){
                this.meta.method=method;
                this.meta.parent_component=parent_component;
                this.meta.kodepemda=item.kodepemda;
                this.meta.nama_pemda=item.nama_pemda;
                this.meta.status_data=item.status_data;
                this.series_option.subtitle.text=item.nama_pemda;
                this.series_option_public.subtitle.text=item.nama_pemda;
                this.loadSeries();
                this.getExistData();
  
              },
              show:function(){
                $('#modal_partial_single_show_pemda_{{$dataset->id}}').modal({backdrop: 'static', keyboard: false});
              },
          
              chuckSplit:function(input,size){
                var chunked=[];
                var x=Math.ceil(input.length / size);
                if(x){
                    return this.chunk(input,x);
                }else{
                    return [input];
                }
              },  
              chunk:function(input,size){
                  var chunked=[];
                  Array.from({length: Math.ceil(input.length / size)}, (val, i) => {
                      chunked.push(input.slice(i * size, i * size + size))
                  });
  
                  return chunked;
              },
           
              
          },
          computed:{
          
          },
          watch:{
           
          }
          
      })
  
  </script>
  
  @endpush
  