<html>

    <header>
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
    </header>
    <body>
       <div class="pl-2 pr-1">
            @yield('content_header')
            @yield('content')
       </div>
    </body>
    <script src="{{asset('js/app.js')}}"></script>
    @yield('js')
    @stack('js')
    @stack('js_push')
</html>