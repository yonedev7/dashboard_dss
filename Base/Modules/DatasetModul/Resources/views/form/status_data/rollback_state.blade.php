<div id="app_rollback_{{$dataset->id}}">
    <div class="modal fade bg-primary" id="modal_app_rollback_{{$dataset->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content text-dark">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Rollback Status Data</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
             <h5 class="text-uppercase">Rollback Status @{{meta.dataset.name}} Tahun {{$StahunRoute}}</h5>
             <p class="mb-0"><b>@{{meta.nama_pemda}}</b></p>
            <div class="form-group">
                <label for="">Status Data</label>
                <select name="" id="" class="form-control" v-model="state">
                    <option value="1" v-if="state_last>1">Verifikasi</option>
                    <option value="0">Input Data</option>
                </select>
            </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" @click="sumbit()" class="btn btn-primary">Kirim</button>
            </div>
          </div>
        </div>
    </div>

</div>

@push('js')

<script>
    var app_rollback_{{$dataset->id}}=new Vue({
        el:'#app_rollback_{{$dataset->id}}',
        data:{
            state_last:0,
            state:0,
            meta:{
                component_parent:null,
                dataset:{
                    name:'',
                    keterangan:''
                },
                kodepemda:'',
                nama_pemda:'',
               
            }
        },
        methods:{
            show:function(){
                $('#modal_app_rollback_{{$dataset->id}}').modal();
            },
            close:function(){
                $('#modal_app_rollback_{{$dataset->id}}').modal('hide');

            },
            loadData:function(item,component_parent){
                this.meta.kodepemda=item.kodepemda;
                this.meta.nama_pemda=item.nama_pemda;
                this.state_last=parseInt(item.status_data);
                this.meta.component_parent=component_parent;
                this.meta.dataset=<?=json_encode($dataset)?>;
                this.show();
            },
            sumbit:function(){
                this.$isLoading(true);
                var self=this;
                req_ajax.post(('{{route('api-web-mod.dataset.form-handle.rollback',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id,'kodepemda'=>'@?'])}}').yoneReplaceParam('@?',[this.meta.kodepemda]),{
                    state:this.state
                }).then(function(res){
                    if(res.status==200){
                        if(res.data.code==200){
                            window.Swal.fire({
                            title: 'Berhasil!',
                            text: res.data.message,
                            icon: 'success',
                            confirmButtonText: 'Close'
                            });
                        }else{
                            window.Swal.fire({
                            title: 'Error!',
                            text: res.data.message,
                            icon: 'error',
                            confirmButtonText: 'Close'
                            });
        
                        }
                    }
                }).finally(function(){
                    self.$isLoading(false);
                    eval('window.'+self.meta.component_parent+'.init()');
                    self.close();

                });

            }
        }
    })

</script>
  


@endpush