<div id="app_verified_{{$dataset->id}}">
    <div class="modal fade" id="modal_app_verified_{{$dataset->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Verifikasi Data</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
             <h5>Verifikasi @{{meta.dataset.name}} Tahun {{$StahunRoute}}</h5>
             <p class="mb-0"><b>@{{meta.nama_pemda}}</b></p>
             <p class="rounded text-capitalize p-3 bg-navy mt-3">
                 1. Data Telah Diperiksan dan Telah sesuai dengan Definisi dan konsep data
                 <br>
                 2. Data Akan Dilanjutkan pada proses Validasi oleh Pemerintah Pusat
             </p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" @click="sumbit()" class="btn btn-primary">Kirim Verifikasi</button>
            </div>
          </div>
        </div>
    </div>

</div>

@push('js')

<script>
    var app_verified_{{$dataset->id}}=new Vue({
        el:'#app_verified_{{$dataset->id}}',
        data:{
            meta:{
                component_parent:null,
                dataset:{
                    name:'',
                    keterangan:''
                },
                kodepemda:'',
                nama_pemda:'',
            }
        },
        methods:{
            show:function(){
                $('#modal_app_verified_{{$dataset->id}}').modal();
            },
            close:function(){
                $('#modal_app_verified_{{$dataset->id}}').modal('hide');

            },
            loadData:function(item,component_parent){
                this.meta.kodepemda=item.kodepemda;
                this.meta.nama_pemda=item.nama_pemda;
                this.meta.component_parent=component_parent;
                this.meta.dataset=<?=json_encode($dataset)?>;
                this.show();
            },
            sumbit:function(){
                this.$isLoading(true);
                var self=this;
                req_ajax.post(('{{route('api-web-mod.dataset.form-handle.verified',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id,'kodepemda'=>'@?'])}}').yoneReplaceParam('@?',[this.meta.kodepemda])).then(function(res){
                    if(res.status==200){
                        if(res.data.code==200){
                            window.Swal.fire({
                            title: 'Berhasil!',
                            text: res.data.message,
                            icon: 'success',
                            confirmButtonText: 'Close'
                            });
                        }else{
                            window.Swal.fire({
                            title: 'Error!',
                            text: res.data.message,
                            icon: 'error',
                            confirmButtonText: 'Close'
                            });
        
                        }
                    }
                }).finally(function(){
                    self.$isLoading(false);
                    eval('window.'+self.meta.component_parent+'.init()');
                    self.close();

                });

            }
        }
    })

</script>
  


@endpush