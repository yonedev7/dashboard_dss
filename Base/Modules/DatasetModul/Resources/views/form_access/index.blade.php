@extends('adminlte::page')
@section('content_header')

<h4><b>{{$page_meta['title']}} Tahun {{$StahunRoute}}</b></h4>
@stop

@section('content')
@if($direct_pemda)
@foreach($data as $d)
@include('datasetmodul::form.component_vue.input_cell')

@php
    $com_share=['dataset'=>$d,'fingerprint'=>$re->fingerprint(),'fingerprint_parent'=>$re->fingerprint()]
@endphp
    @include('datasetmodul::form.status_data.verified',$com_share)
    @include('datasetmodul::form.status_data.validate',$com_share)

    @if($component_load)
        @foreach($component_load as $c)
            @include($c,$com_share)
        @endforeach
    @endif
@endforeach




@endif






<div class="px-3 " id="app">
   <div class="bg-white rounded shadow  table-responsive ">
    <table class="table table-bordered">
        <thead class="thead-success">
            <tr>
                <th>Aksi</th>
                <th>Role</th>

                <th>Menu</th>
                <th>Nama Dataset</th>

                <th>Induk Data</th>
                <th>Terdata</th>
                <th>Terverifikasi</th>
                <th>Tervalidasi</th>
            </tr>
        </thead>
        <tbody>
            <template v-for="item,i in data">
                <tr v-if="kpi_last!=item.kpi" >
                    <td colspan="8" v-if="kpi_last=item.kpi" class=""><h3 class="text-center"><b>@{{item.kpi}}</b></h3></td>
                </tr>
            <tr >
                <td>
                    <div class="btn-group">
                        <template v-if="direct_pemda=='x'">
                            <button class="btn bg-teal btn-sm"  @click="directDetail(item)" >Detail</button>
                            <button class="btn btn-warning btn-sm" v-if="item.permissions.includes('dts.'+item.id+':edit-data') && !(item.status_data.tervalidasi||item.status_data.terverifikasi)" @click="editForm(item,'edit')"><i class="fa fa-pen"></i> Edit</button>
                            <button class="btn btn-success btn-sm" v-if="item.permissions.includes('dts.'+item.id+':validate-data') && !(item.status_data.tervalidasi||item.status_data.terverifikasi) && (item.status_data.terdata)">Verifikasi</button>
                            <button class="btn btn-primary btn-sm" v-if="item.permissions.includes('dts.'+item.id+':verified-data')&& !(item.status_data.tervalidasi) && (item.status_data.terverifikasi) &&  (item.status_data.terdata)">Validasi</button>
                            <button class="btn btn-danger btn-sm" v-if="item.permissions.includes('dts.'+item.id+':rollback-data')&& (item.status_data.terverifikasi)">Rollback</button>
                        </template>
                        <template v-else>
                            <button class="btn bg-primary btn-sm" @click="directDetail(item)" > <i class="fa fa-arrow-right"></i>  Detail</button>

                        </template>
                            <a  download :href="('{{route('mod.dataset.download.excel',['tahun'=>$StahunRoute,'id_dataset'=>'@?','kodepemda'=>null])}}').yoneReplaceParam('@?',[item.id])" class="btn  btn-success"><i class=" fa fa-download"></i> Excel</a>

                    </div>

                </td>
                <td>
                    <span v-if="((item.permissions.includes('dts.'+item.id+':edit-data') || item.permissions.includes('dts.'+item.id+':validate-data') || item.permissions.includes('dts.'+item.id+':verified-data') || item.permissions.includes('dts.'+item.id+':rollback-data')  ))" class="badge badge-primary">Proccesing</span>
                    <span v-else class="badge badge-warning">View Only</span>
                </td>
                <td class="text-uppercase">@{{item.menu}}</td>
                <td>@{{item.name}} Tahun @{{item.status_data.tahun}}</td>
                <td>@{{item.klasifikasi_1}}</td>
                <td>@{{item.status_data.terdata}} Pemda / @{{item.status_data.jumlah_pemda}} Pemda</td>
                <td>@{{item.status_data.terverifikasi}} Pemda / @{{item.status_data.jumlah_pemda  }} Pemda</td>
                <td>@{{item.status_data.tervalidasi}} Pemda / @{{item.status_data.jumlah_pemda}} Pemda</td>

            </tr>


            <tr v-if="item.roles.includes('TACT-LG') && buildTask(item.status_data,'TACT-LG')">
                <td colspan="8" class="p-0" >
                    <div class="d-flex align-items-stretch">
                        <div class="mr-2 flex-column d-flex px-3">
                            <p class="p-0 m-0">
                                <small>@{{item.name}}</small>
                            </p>
                            <p class="p-0 m-0"><b class="text-primary"><b>@{{buildTask(item.status_data,'TACT-LG')}} PEMDA</b></b>  LAGI HARUS TERVALIDASI</p>
                        </div>
                        <div style="background-color:#ddd;"  class="flex-grow-1 d-flex p-3 align-self-center ">
                            <div class="progress d-flex " style="width:100%;">

                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" v-bind:style="'width:'+percentage(item.status_data.jumlah_pemda,item.status_data.jumlah_pemda - buildTask(item.status_data,'TACT-LG'))+'%'">@{{percentage(item.status_data.jumlah_pemda,item.status_data.jumlah_pemda - buildTask(item.status_data,'TACT-LG'))}}%</div>
                                <div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" v-bind:style="'width:'+(100-percentage(item.status_data.jumlah_pemda,item.status_data.jumlah_pemda - buildTask(item.status_data,'TACT-LG')))+'%'">@{{(100 - percentage(item.status_data.jumlah_pemda,item.status_data.jumlah_pemda - buildTask(item.status_data,'TACT-LG')))}}%</div>

                            </div>
                        </div>

                    </div>


                </td>
            </tr>
            <tr v-if="item.roles.includes('TACT-REGIONAL') && buildTask(item.status_data,'TACT-REGIONAL')">
                <td colspan="8" class="p-0" >
                    <div class="d-flex align-items-stretch">
                        <div class="mr-2 flex-column d-flex px-3">
                            <p class="p-0 m-0">
                                <small>@{{item.name}}</small>
                            </p>
                            <p class="p-0 m-0"><b class="text-primary"><b>@{{buildTask(item.status_data,'TACT-REGIONAL')}} PEMDA</b></b>  LAGI HARUS TERVERIFIKASI</p>
                        </div>
                        <div style="background-color:#ddd;"  class="flex-grow-1 d-flex p-3 align-self-center ">
                            <div class="progress d-flex " style="width:100%;">

                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" v-bind:style="'width:'+percentage(item.status_data.jumlah_pemda,item.status_data.jumlah_pemda - buildTask(item.status_data,'TACT-REGIONAL'))+'%'">@{{percentage(item.status_data.jumlah_pemda,item.status_data.jumlah_pemda - buildTask(item.status_data,'TACT-REGIONAL'))}}%</div>
                                <div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" v-bind:style="'width:'+(100-percentage(item.status_data.jumlah_pemda,item.status_data.jumlah_pemda - buildTask(item.status_data,'TACT-REGIONAL')))+'%'">@{{(100 - percentage(item.status_data.jumlah_pemda,item.status_data.jumlah_pemda - buildTask(item.status_data,'TACT-REGIONAL')))}}%</div>

                            </div>
                        </div>

                    </div>


                </td>
            </tr>
            <tr>
                <td colspan="8" class="bg-secondary"></td>
            </tr>
            </template>
        </tbody>
    </table>
   </div>
</div>
@stop

@section('js')
<script>
    var app=new Vue({
        el:'#app',
        data:{
            direct_pemda:<?=$direct_pemda?'true':'false'?>,
            pemda_list:<?=json_encode($pemdas)?>,
            data:<?=json_encode($data)?>,
            kpi_last:''
        },

        methods:{
            buildTask:function(data,context){
                switch(context){
                    case 'TACT-LG':
                        var ps=data.jumlah_pemda;
                        var done=data.tervalidasi;
                        var tugas=Math.ceil(ps - done);
                        return tugas;

                    break;
                    case 'TACT-REGIONAL':
                        var ps=data.jumlah_pemda;
                        var done=data.tervalidasi+data.terverifikasi;
                        var tugas=Math.ceil(ps-done);
                        return tugas;
                    break;
                    default:
                    return 0;
                    break;
                }

            },
            terdata:function(){

            },
            terverifikasi:function(){

            },
            tervalidasi:function(){

            },
            percentage:function(all,data){
                var p= Math.ceil((data/all)*100).toFixed(2);
                return p;
            },
            editForm:function(item,contex){

            },
            directDetail:function(item){
                window.location.href=(('{{route('mod.dataset.form.listing',['tahun'=>$StahunRoute,'id_dataset'=>'@?'])}}').yoneReplaceParam('@?',[item.id]));
            }

        }
    })
</script>
@stop
