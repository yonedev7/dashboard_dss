@extends('adminlte::page')


@section('content')
<script>
    var page_meta=<?=json_encode($page_meta)?>;
</script>
<div class="parallax pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
    <div class="container">
        <section >
            <h3><b>{{$page_meta['title']}}</b></h3>
            <p><b>{!!$page_meta['keterangan']!!}</b></p>
           
        </section>
      
    </div>
   
</div>
<div class="card">
    <div class="card-body">
        @include('sistem_informasi.partials.filter',['req'=>$req])
    </div>
</div>

<div class="" id="app">
    
    <div class="p-3">
        <div class="table-responsive">
        <table class="table table-bordered" id="table-show">
            <thead class="thead-dark">
                <tr>
                    <th style="width:100px;">AKSI</th>
                    <th>KODE</th>
                    <th>NAMA DATASET</th>
                    <th>JENIS DATASET</th>
                    <th>KLASIFIKASI LINGKUP</th>
                    <th>KLASIFIKASI BANTUAN</th>
                    <th>PENANGUNG JAWAB</th>
                    <th>KETEGORI DATASET</th>
                    <th>DATA TERVERIFIKASI</th>
                    <th>DATA BELUM TERVERIFIKASI</th>
                </tr>
            </thead>
            <tbody>
            @foreach($data as $k=>$d)
                <tr>
                    @php
                    @endphp
                        <td>
                            @if($d->pusat)
                            <a class="btn btn-success btn-sm" href="{{route('mod.dataset.form.listing',['tahun'=>$StahunRoute,'id_dataset'=>$d->id,'tw'=>4])}}">From Input</a>

                            @else
                            <a class="btn btn-success btn-sm" href="{{route('mod.dataset.form.listing',['tahun'=>$StahunRoute,'id_dataset'=>$d->id,'tw'=>4])}}">From Input</a>
                            @endif
                        </td>
                        <td>{{$d->id}}</td>
                        <td>{{$d->name}}</td>
                        <td>{{$d->pusat?'Pusat':'Kedaerahan'}} - {{$d->jenis_dataset?'Multy Row':'Single Row'}}</td>
                    @php
                    $kla=json_decode($d->klasifikasi_2??'[]',true);
                    @endphp
                    <td>{{$d->klasifikasi_1}}</td>
                    <td>{{implode(',',$kla)}}</td>
                    <td>{{$d->penangung_jawab}}</td>
                    @php
                    $kat=json_decode($d->kategori,true);
                    @endphp
                    <td>{{implode(', ',$kat)}}</td>
                    <td>{{number_format($d->rekap['terverifikasi'],0,'.',',')}} Pemda</td>
                    <td>{{number_format($d->rekap['belum_terverifikasi'],0,'.',',')}} Pemda</td>

            
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>    
        
    </div>    
</div>


@stop

@section('js')
<script>
    $('#table-show').DataTable();
</script>

@stop