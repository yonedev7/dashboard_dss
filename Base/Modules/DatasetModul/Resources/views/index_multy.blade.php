@extends('adminlte::page')


@section('content')
<script>
    var page_meta=<?=json_encode($page_meta)?>;
</script>
<div class="parallax pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
    <div class="container">
        <section >
            <h3><b>{{$page_meta['title']}} Per-PEMDA</b></h3>
            <h3><b> {{$page_meta['tw']==4?'Final':'Tahap '.$page_meta['tw']}} Tahun {{$page_meta['tahun']}}</b></h3>
            <p><b>{!!$page_meta['keterangan']!!}</b></p>
           
        </section>
      
    </div>
   
</div>
@if(isset($page_meta['monev']))
@include('data.partials.filter_basis_monev',['basis'=>$basis])

@else
@include('data.partials.filter_basis',['basis'=>$basis])

@endif


@include('data.partials.filter_jenis_daerah',['init'=>[
    'component'=>[
        'id'=>'filter_bispan',
        'name'=>$page_meta['title'],
        'target_react'=>['app'],
        'basis'=>$basis,
    ],
   
]])
<div class="p-3">
    <div class="table-responsive" id="app-data">
        <button class="btn btn-success mb-3" @click="showModalAdd"><i class="fa fa-plus"></i> Tambah Data PEMDA</button>
        <hr>
        <div class="modal fade" tabindex="-1"  id="modal-add" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Pilih Pemda</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="table-pemda-list">
                        <thead class="thead-dark">
                            <tr>
                                <th>AKSI</th>
                                <th>KODEPEMDA</th>
                                <th>NAMAPEMDA</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="item,k in add_pemda.data">
                            <td>
                                <div class="btn-group">
                                    @if(!isset($_GET['monev']))
                                    <a v-bind:href="('{{route('mod.dataset.detail',['tahun'=>$StahunRoute,'id_dataset'=>$page_meta['id'],'tw'=>1,'kodepemda'=>'xxxx'])}}').replace('xxxx',item.kodepemda)" class="btn btn-success"><i class="fa fa-arrow-right"></i> Tahap I</a>
                                    <a v-bind:href="('{{route('mod.dataset.detail',['tahun'=>$StahunRoute,'id_dataset'=>$page_meta['id'],'tw'=>2,'kodepemda'=>'xxxx'])}}').replace('xxxx',item.kodepemda)" class="btn btn-success"><i class="fa fa-arrow-right"></i> Tahap II</a>
                                    <a v-bind:href="('{{route('mod.dataset.detail',['tahun'=>$StahunRoute,'id_dataset'=>$page_meta['id'],'tw'=>3,'kodepemda'=>'xxxx'])}}').replace('xxxx',item.kodepemda)" class="btn btn-success"><i class="fa fa-arrow-right"></i> Tahap II</a>
                                   @endif
                                    <a v-bind:href="('{{route('mod.dataset.detail',['tahun'=>$StahunRoute,'id_dataset'=>$page_meta['id'],'tw'=>4,'kodepemda'=>'xxxx'])}}').replace('xxxx',item.kodepemda)" class="btn btn-success"><i class="fa fa-arrow-right"></i>
                                        @if(!isset($_GET['monev']))
                                        Tahap IV
                                        @endif
                                        </a>

                                    </div>
                            </td>
                            <td>@{{item.kodepemda}}</td>
                            <td>@{{item.nama_pemda}}</td>

                             </tr>
                        </tbody>
                    </table></div>
            </div>
            <div class="modal-footer">
               
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
        </div>

    <template v-if="loading&&data.length">
       <div class="text-center p-2">
        <div class="spinner-border text-primary" role="status">
        <span class="sr-only">Loading...</span>
        </div>
        </div>
    </template>
    <table class="table table-bordered table-striped" id="table-data">
        <thead class="thead-dark">
            <tr>
                <th>NO.</th>
                <th>AKSI</th>
                <th>KODEPEMDA</th>
                <th>NAMAPEMDA</th>
                <th>STATUS VERIFIKASI</th>
                <th>JUMLAH DATA</th>
                <th>JUMLAH DATA TERVERIFIKASI</th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="item,k in data">
                <td>@{{k+1}}</td>
                <td>
                    <a v-bind:href="('{{route('mod.dataset.detail',['tahun'=>$StahunRoute,'id_dataset'=>$page_meta['id'],'tw'=>$page_meta['tw'],'kodepemda'=>'xxxx'])}}').replace('xxxx',item.kodepemda)" class="btn btn-success btn-sm"><i class="fa fa-arrow-right"></i> Detail</a>
                </td>
                <td>@{{item.kodepemda}}</td>
                <td>@{{item.name}}</td>
                <td>@{{item.terdata!=item.terverifikasi?'Belum Terverifikasi Sepenuhnya':'Terverifikasi'}}</td>

                <td>@{{item.terdata}}</td>
                <td>@{{item.terverifikasi}}</td>
            </tr>
           
        </tbody>
            
    </table>
    <div v-if="loading" class="text-center">
            
            <div class="spinner-border text-primary" role="status">
            <span class="sr-only">Loading...</span>
            </div>
            <div class="spinner-border text-secondary" role="status">
            <span class="sr-only">Loading...</span>
            </div>
            <div class="spinner-border text-success" role="status">
            <span class="sr-only">Loading...</span>
            </div>
            <div class="spinner-border text-danger" role="status">
            <span class="sr-only">Loading...</span>
            </div>
        
    </div>
</div>
</div>

@stop

@section('js')
<script>

    var app=new Vue({
        el:'#app-data',
        data:{
            data:[],
            current_page:1,
            filter:'provinsi kab/kot',
            basis:'{{$basis}}',
            loading:false,
            add_pemda:{
                ref_table:null,
                data:[]
              
            }
        },
        created:function(){
            this.load();
           
            self=this;
           setTimeout((ref)=>{
            req_ajax.post('{{route('api-web-mod.dataset.meta_pemda',['tahun'=>$StahunRoute,'kodepemda'=>0])}}',{
                like:true
            }).then(res=>{
                res.data.forEach(el=>{
                    ref.add_pemda.data.push(el);
                });
               setTimeout((ref)=>{
                    ref.add_pemda.ref_table=$('#table-pemda-list').DataTable();
               },500,ref);
            });

           },300,self)
        },
        methods:{
            showModalAdd:()=>{
                $('#modal-add').modal();
            },
            react:function(data){
                if(data.method=='filter_change'){
                    this.filter=data.data.filter;
                    this.current_page=1;
                    this.data=[];
                    this.load();
                }
            },
            load:function(){
                self=this;
                this.loading=true;
                req_ajax.post('{{route('api-web-mod.dataset.ajax_load_multy',['tahun'=>$StahunRoute,'id_dataset'=>$page_meta['id'],'tw'=>$page_meta['tw']])}}',{
                    page:this.current_page,

                }).then(function(res){
                    res.data.data.forEach(el => {
                        self.data.push(el);
                    });
                    self.loading=false;
                    if(res.data.last_page!=self.current_page){
                        self.current_page+=1;
                        self.load();
                    }else{
                       setTimeout(() => {
                        $('#table-data').DataTable();
                       }, 300);
                    }
                });
            }
        }
    });

</script>
@stop