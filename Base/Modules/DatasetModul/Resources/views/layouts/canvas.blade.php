@extends('datasetmodul::layouts.master')

@section('content')

<table class="table ">
    <thead>
        <tr>
            <th>
                <div id="header-dss" class="text-uppercase align-self-center">
                    <span><img src="{{asset('assets/img/brand/logo.png')}}" height="20px" alt=""></span>    <b class="my-3">{{Carbon\Carbon::now()->format('d F Y h:i a')}}</b>
                </div>

            </th>
        </tr>

    </thead>
    <tbody>
        <tr>
            <td id="content-canvas" class="d-flex  justify-content-center"></td>
        </tr>
    </tbody>

</table>




@stop