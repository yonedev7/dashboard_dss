<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Module DatasetModul</title>

       {{-- Laravel Mix - CSS File --}}
       <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <script>
        const dss{{$SpageId}}={
            'base':'{{url('api')}}',
            'token':'{{Auth::check()?Auth::User()->_web_api_token():''}}',
            'user':<?=json_encode(Auth::check()?(Auth::User()):'{}')?>   
        }
       </script>
        <style media="all">
            @media print {
                .pagebreak { 
                    page-break-before: always;
                    break-inside: avoid;

                 } /* page-break-after works, as well */
                
            }
             body{
                margin-top:35px!important;
                background-color:#fff;
            }
            .table-print{
                width:100%;
            }
            .table-print.table tr{
                border:none;
            }
            @page{
                margin-top:35px;
                margin-bottom:35px;

            }
        </style>
    </head>
    <body>
        @yield('content')
        <script src="{{asset('js/app.js?v='.date('is'))}}"></script>
            <script>

            initial_ajax(dss{{$SpageId}});
            window.xdss{{md5(url()->current())}}=dss{{$SpageId}};
            </script>
        @yield('js')
        @stack('js')

        @stack('js_push')

        {{-- Laravel Mix - JS File --}}
        {{-- <script src="{{ mix('js/datasetmodul.js') }}"></script> --}}
    </body>
</html>
