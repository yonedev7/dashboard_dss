@extends('adminlte::page')


@section('content')
<script>
    var page_meta=<?=json_encode($page_meta)?>;
</script>
<div class="parallax pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
    <div class="container">
        <section >
            <h3><b>{{$page_meta['title']}} </b></h3>
            <h5><b>Tahun {{$StahunRoute}}</b></h5>
            <p><b>{!!$page_meta['keterangan']!!}</b></p>
        </section>
        <p>
            @if(strtoupper($dataset->klasifikasi_1)!='DATA BUMDAM')
            <span class="badge bg-primary">LINGKUP DATA PEMDA</span>
            @else
            <span class="badge bg-primary">LINGKUP DATA BUMDAM</span>
            @endif

            @php
            $kla=json_decode($dataset->klasifikasi_2??'[]',true);
            @endphp

            @if(count($kla))
            <span class="badge bg-success">TIPE  {{strtoupper(implode(',',$kla))}}</span>
            @endif
        </p>
      
    </div>
   
</div>

@if(!isset($page_meta['pemda']))
@if(isset($page_meta['monev']))
@include('data.partials.filter_basis_monev',['basis'=>$basis])
@else

@endif



@endif


@include('data.admin_partials.table_input',['init'=>[
    'component'=>[
        'id'=>'table_input',
        'pemda_list'=>isset($page_meta['pemda_list'])?$page_meta['pemda_list']:[],
        'name'=>$page_meta['title'],
        'target_react'=>['l1'],
        'basis'=>$basis,
        'id_dataset'=>$page_meta['id'],
        'tw'=>$page_meta['tw'],
        'tahun'=>$StahunRoute,
        'dataset'=>$dataset,
        'monev'=>isset($page_meta['monev'])?$page_meta['monev']:null,
        'def_kodepemda'=>isset($page_meta['pemda'])?$page_meta['pemda']['kodepemda']:null,
        'def_name'=>isset($page_meta['pemda'])?$page_meta['pemda']['nama_pemda']:null,
        'def_regional_1'=>isset($page_meta['pemda'])?$page_meta['pemda']['regional_1']:null,
        'def_regional_2'=>isset($page_meta['pemda'])?$page_meta['pemda']['regional_2']:null,
        'def_tahun_proyek'=>isset($page_meta['pemda'])?$page_meta['pemda']['tahun_proyek']:0,
        'def_tipe_bantuan'=>isset($page_meta['pemda'])?$page_meta['pemda']['tipe_bantuan']:null,

    ],
    'map_table'=>[
        'ajax_data'=>route('api-web-mod.dataset.ajax_load_single',['tahun'=>$StahunRoute,'id_dataset'=>$page_meta['id'],'tw'=>$page_meta['tw']]),
        'columns'=>$columns
        
    ]
   
]])


@stop