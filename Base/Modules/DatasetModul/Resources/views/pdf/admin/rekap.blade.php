@extends('datasetmodul::layouts.master')

@section('content')
<style media="all">

    
</style>
<div class="pt-3" id="app">
    <table class="table-print table">
        <thead>
            <tr>
                <th>
                    <div id="header-dss" class="text-uppercase align-self-center">
                        <span><img src="{{asset('assets/img/brand/logo.png')}}" height="20px" alt=""></span>    <b class="my-3">{{Carbon\Carbon::now()->format('d F Y')}}</b>
                    </div>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><h3 class="text-center"><b>DATASET UTAMA</b></h3></td>
            </tr>
            <tr v-for="item in charts" >
                <td>
                    <charts :options="item.chart_pie"></charts>

                </td>
            </tr>
            
           

                    

        </tbody>
    </table>
    <div class="pagebreak">
        <table class="table-print table">
            <thead>
                <tr>
                    <th>
                        <div id="header-dss" class="text-uppercase align-self-center">
                            <span><img src="{{asset('assets/img/brand/logo.png')}}" height="20px" alt=""></span>    <b class="my-3">{{Carbon\Carbon::now()->format('d F Y')}}</b>
                        </div>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr class="">
                    <td><h3 class="text-center"><b>DATASET PENDUKUNG</b></h3></td>
                </tr>
                <tr v-for="item in charts_2" >
                    <td>
                        <charts :options="item.chart_pie"></charts>
                    </td>
                </tr>
            </tbody>
        </table>

    </div>
    

</div>
@stop


@section('js')
<script>
    var app=new Vue({
        el:'#app',
        data:{
            dataset_utama:<?=json_encode($dataset_utama)?>,
            dataset_pendukung:<?=json_encode($dataset_pendukung)?>,
            charts:[],
            charts_2:[]

        },
        created:function(){
            this.dataset_utama.forEach(element => {
                this.charts.push({
                    id_dataset:element.id,
                    tahun:{{$StahunRoute}},
                    name:element.name,
                    keterangan:element.tujuan,
                    chart_pie:{
                        chart:{
                            type:'pie',
                            height:400,
                            width:400
                        },
                        title:{
                            text:'',
                            enabled:false
                        },
                        tooltip: {
                        pointFormat: '{point.percentage:.1f} %<br>Total: {point.y:.0f} PEMDA'
                        },
                        plotOptions: {
                            pie: {
                                dataLabels: {
                                enabled: true,
                                format: '{point.percentage:.1f} %<br>Total: {point.y:.0f} PEMDA',
                                distance: -10,
                                }
                            }
                        },
                        subtitle:{
                            text:'',
                            enabled:true
                        },
                        xAxis:{
                            type:'category'
                        },
                        series:[]
                    },


                });
            });

            this.charts.forEach((el,k)=>{
                var ref=this;
                this.$isLoading(true);
                req_ajax.post(('{{route('api-web-mod.dataset.public.data.state',['tahun'=>'@?','id_dataset'=>'@?'])}}').yoneReplaceParam('@?',[(el.tahun||{{$StahunRoute}}),el.id_dataset])).then(res=>{
                    if(res.status==200){
                        if(res.data['series']!=undefined){
                            ref.charts[k].chart_pie.series=res.data.series;
                            ref.charts[k].chart_pie.subtitle.text=res.data.series[0].name+' LONGLIST';
                        }
                    }
                }).finally(function(){
                    ref.$isLoading(false);
                });

            });


            this.dataset_pendukung.forEach(element => {
                this.charts_2.push({
                    name:element.name,
                    keterangan:element.tujuan,
                    chart_pie:{
                        chart:{
                            type:'pie'
                        },
                        title:{
                            text:element.name+' Tahun {{$StahunRoute}}'
                        },
                        subtitle:{
                            text:''
                        },
                        series:[]
                    },


                });
            });
        }
    })
</script>

@stop