@extends('adminlte::page')


@section('content')
<script>
    var page_meta=<?=json_encode($page_meta)?>;
</script>
<div class="parallax pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
    <div class="container">
        <section >
            <h3><b>{{$page_meta['title']}}</b></h3>
            <p><b>{!!$page_meta['keterangan']!!}</b></p>
           
        </section>
      
    </div>
   
</div>
<div id="app" class="p-3">
    <div class="btn-group mb-3">
        <a href="{{route('mod.dataset.schema-data.create',['tahun'=>$StahunRoute])}}" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Tambah Data</a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-bordered bg-white" id="table-data">
            <thead class="thead-dark">
                <tr>
                    <th>AKSI</th>
                    <th>PRODUSEN DATA</th>
                    <th>NAMA DATA</th>
                    <th>SATUAN DATA</th>
                    <th>TIPE NILAI</th>
                    <th>KLASIFIKASI</th>
                    <th>TIPE PENGISIAN</th>
                    <th>VALIDATOR</th>
                    <th>INTEGRASI DATASET</th>

                </tr>
                </thead>
         </table>
    </div>

    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Hapus @{{modal_del.name}}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p>Menghapus data @{{modal_del.name}}</p>
        </div>
        <div class="modal-footer">
            <form v-bind:action="('{{route('mod.dataset.schema-data.delete',['tahun'=>$StahunRoute,'id'=>'xxxx'])}}').replace('xxxx',modal_del.id)" method="post">
                @csrf
            <button type="submit" class="btn btn-danger">Hapus</button>

            </form>
        </div>
        </div>
  </div>
</div>

</div>

@stop

@section('js')
<script>
    var app=new Vue({
        el:'#app',
        data:{
            data:[],
            loading:false,
            ref_table:null,
            modal_del:{
                name:'',
                id:0
            }
        },
        created:function(){
            var self=this;
            setTimeout((ref) => {
                ref.ref_table=$('#table-data').DataTable({
                    createdRow:function(row,data,index){
                        if(data.status){
                            $(row).addClass('bg-success');
                        }
                    },
                    columns:[
                        {
                            data:'id',
                            sort:false,
                            render:(data,type,row,index)=>{
                                return '<div class="btn-group">'+
                                '<a href="'+('{{route('mod.dataset.schema-data.show',['tahun'=>$StahunRoute,'id'=>'xxxx'])}}').replace('xxxx',row.id)+'" class="btn btn-success btn-sm"><i class="fa fa-arrow-right"></i></a>'+ 
                                '<button onclick="app.showDelete('+index.row+')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>'+ 
                                '</div>';
                            },
                            createdCell:(td, cellData, rowDat)=>{
                                $(td).addClass('bg-white');
                            }
                        },
                        {
                            data:'produsent_data'
                        },
                        {
                            data:'name'
                        },
                        {
                            data:'satuan'
                        },
                        {
                            data:'tipe_nilai'
                        },
                        {
                            data:'classification',
                            render:(data,type,row,index)=>{
                                return (data?'Data Klasifikasi':'Bukan Klasifikasi');
                            }

                        },
                        {
                            data:'type'
                        },
                        {
                            data:'validator'
                        },
                        {
                            data:'dataset_list',
                            render:(data)=>{
                                var d=(data||'').split('#//#');
                                var re='';
                                d.forEach(el=>{
                                    re+='<span class="badge badge-primary">'+el+'</span>';
                                });

                                return re;
                            }
                        }

                        
                    ]
                })
                ref.init();
            }, 300,self);
        },methods:{
            showDelete:function(index){
               if(this.data[index]!=undefined){
                   var data=this.data[index];
                   this.modal_del.name=data.name;
                    this.modal_del.id=data.id;
                    $('#modal-delete').modal();
                }
                
            },
            init:function(){
                this.current_page=1;
                this.load();
            },
            load:function(){
                var self=this;
                self.loading=true;
                req_ajax.post('{{route('api-web-mod.dataset.schema-data.load',['tahun'=>$StahunRoute])}}',{
                    page:this.current_page
                }).then(res=>{
                    self.loading=false;
                    res.data.data.forEach(element => {
                        self.data.push(element);
                    });

                    if(self.ref_table){
                        self.ref_table.clear().draw();
                        self.ref_table.rows.add(self.data).draw();

                    }

                    if(res.data.last_page!=self.current_page){
                        self.current_page+=1;
                        self.load();
                    }
                });
            }
        }
    });   
</script>


@stop