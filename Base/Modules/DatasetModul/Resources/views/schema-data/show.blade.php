@extends('adminlte::page')


@section('content')
<script>
    var page_meta=<?=json_encode($page_meta)?>;
</script>
<div class="parallax pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
    <div class="container">
        <section >
            <h3><b>{{$page_meta['title']}}</b></h3>
            <p><b>{!!$page_meta['keterangan']!!}</b></p>
           
        </section>
      
    </div>
   
</div>
 <form action="{{$url}}" method="post">
    @csrf
    <div class="card" id="app">
        <div class="card-header bg-dark">
            <h4 class="title">Form Perubahan Schema Data</h4>
            </div>
        <div class="card-body">
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label for="">Nama*</label>
                    <input type="text" class="form-control" v-model="name" name="name" required>
                </div>
                <div class="form-group">
                    <label for="">Definisi/Konsep*</label>
                    <textarea name="definisi_konsep" required class="form-control" v-model="definisi_konsep" id="" cols="30" rows="10"></textarea>
                </div>
                <div class="form-group" v-if="id==null">
                    <label for="">Tipe Nilai*</label>
                    <select name="tipe_nilai" required class="form-control" v-model="tipe_nilai" id="">
                        <option value="string">String</option>
                        <option value="numeric">Numeric</option>

                    </select>
                </div>
                <div class="form-group" v-if="id!=null">
                    <label for="">Tipe Nilai*</label>
                    <p><b class="text-primary">@{{tipe_nilai}}</b></p>
                </div>
                <div class="form-group">
                    <label for="">Satuan*</label>
                    <input type="text"name="satuan" required class="form-control" v-model="satuan">
                </div>
    
        
                <div class="form-group">
                    <label for="">Produsen Data*</label>
                    <input type="text"  name="produsent_data" required class="form-control" v-model="produsent_data">
                </div>
                <div class="form-group">
                    <label for="">Validator Data*</label>
                    <select  name="id_validator" required  class="form-control" v-model="id_validator" id="">
                        <option v-for="item,k in validator_list" v-bind:value="item.id">@{{item.name}}</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Cara Hitung</label>
                    <textarea-autosize  
                        class="form-control"
                        placeholder="Cara Hitung"
                        :min-height="50"
                        :max-width="'100%'"
                        name="cara_hitung"
                        v-model="cara_hitung"
                        />
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label for="">Arah Nilai*</label>
                    <select name="arah_nilai" required class="form-control" v-model="arah_nilai" id="">
                       <option value="1" v-if="classification!=true">Positif</option>
                       <option value="0" >Netral</option>
                       <option value="-1" v-if="classification!=true" >Negatif</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Merupakan Data Klasifikasi</label>
                    <p>
                        <toggle-button v-model="classification"
                        color="#28a745"
                        :sync="true"
                        :width="100"
                        :height="30"
                        :labels="{checked: 'Ya', unchecked: 'Bukan'}"/></p>
                        <input type="hidden" name="classification" v-model="classification">
                 </div>
                 
                <div class="form-group">
                    <label for="">Tipe Pengisian*</label>
                    <select name="type" required class="form-control" v-model="type" id="">
                        <option v-if="classification==false" value="input-value">Input-Value</option>
                        <option value="input-date" v-if="tipe_nilai=='string' && classification==false">Input-Date</option>
                        <option value="single_file" v-if="tipe_nilai=='string' && classification==false">Single File</option>
                        <option value="multy_file"  v-if="tipe_nilai=='string' && classification==false">Multy File</option>
                        <option value="options">Options</option>
                    </select>
                </div>
                <div class="form-group" v-if="type=='options'">
                    <button class="btn btn-success btn-sm mt-2 mb-2" @click="addRowOption()" type="button"><i class="fa fa-plus"></i> Option</button>

                    <label for="">Options List</label>
                    <table class="table table-bordered">
                        <thead class="thead-dark">
                            <tr>
                                <th>Aksi</th>
                                <th>Tag</th>
                                <th>Value</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="item,k in options">
                                <td>
                                    <div class="btn-group">
                                        <button v-if="k!=0" class="btn btn-danger btn-sm" @click="options.splice(k,1)" type="button" ><i class="fa fa-trash"></i></button>
                                        <button class="btn btn-primary btn-sm" v-if="k!=0" @click="moveOption(k,-1)" type="button" ><i class="fa fa-arrow-up"></i></button>
                                        <button class="btn btn-primary btn-sm" v-if="k!=(options.length-1)&&options[k+1]!=undefined" @click="moveOption(k,1)" type="button" ><i class="fa fa-arrow-down"></i></button>
                                    </div>
                                </td>
                                <td>
                                    <input  v-bind:name="'options['+k+'][tag]'" required  type="text" class="form-control" v-model="item.tag">
                                </td>   
                                <td>
                                    <input v-bind:name="'options['+k+'][value]'" required v-bind:type="tipe_nilai=='numeric'?'number':'text'" class="form-control" v-model="item.value">
                                </td>    
                            </tr>
                            </tbody>
                     </table>
                </div>
                <div class="form-group" v-if="!(['single_file','multy_file']).includes(type)">
                    <button class="btn btn-success btn-sm mt-2 mb-2" @click="addRowBind()" type="button"><i class="fa fa-plus"></i> Binding</button>

                    <label for="">Binding Nilai Tampilan</label>
                    <table class="table table-bordered">
                        <thead class="thead-dark">
                            <tr>
                                <th>Aksi</th>
                                <th>Tag</th>
                                <th>Logic</th>
                                <th>Logic Valid</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="item,k in bind_view">
                                <td>
                                    <div class="btn-group">
                                        <button class="btn btn-danger btn-sm" @click="bind_view.splice(k,1)" type="button" ><i class="fa fa-trash"></i></button>
                                        <button class="btn btn-primary btn-sm" v-if="k!=0" @click="moveBind(k,-1)" type="button" ><i class="fa fa-arrow-up"></i></button>
                                        <button class="btn btn-primary btn-sm" v-if="k!=(bind_view.length-1)&&bind_view[k+1]!=undefined" @click="moveBind(k,1)" type="button" ><i class="fa fa-arrow-down"></i></button>

                                    </div>
                                    </td>
                                <td>
                                    <input  v-bind:name="'bind_view['+k+'][tag]'" required  type="text" class="form-control" v-model="item.tag">
                                </td>   
                                <td>
                                    <input v-bind:name="'bind_view['+k+'][logic]'" required type="text" class="form-control" v-model="item.logic">
                                </td> 
                                <td>
                                   <button type="button" v-bind:class="'btn btn-sm rounded-circle '+(item.accept?'btn-success':'btn-danger')">
                                    <i class="fa fa-check " v-if="item.accept"></i>
                                    <i class="fa fa-times " v-if="!item.accept"></i>
                                    </button>
                                    <input type="hidden"  v-bind:name="'bind_view['+k+'][accept]'" v-model="item.accept" >

                                </td>   
                            </tr>
                            </tbody>

                     </table>
                </div>
                <div class="form-group" v-if="(tipe_nilai=='string'&&['single_file','multy_file'].includes(type))&&(classification!=true)">
                    <label for="">File Mime Accept </label>
                    <table class="table table-bordered">
                        <thead class="thead-dark">
                            <tr>
                                <th style="width:50px;">Accept</th>
                                <th>Extensi</th>
                            </tr>
                        </thead>
                        <tr v-for="item,k in mime_list">
                                <td>
                                    
                                    <input type="checkbox" v-bind:checked="checkFileAccept(item.mime)?true:false" @click="acceptFile(item.mime,$event)" name="file_accept[]" v-bind:required="file_accept.length==0?true:false"  :value="item.mime">
                                    </td>
                                <td>
                                    @{{item.extensi}}
                                </td>   
                                
                            </tr>
                     </table>
                </div>
            </div> 
        </div>   
          

        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-success ">Kirim</button>
         </div>
</div>

 </form>

@stop

@section('js')
<script>
var app=new Vue({
    el:'#app',
    data:{
        mime_list:[
            {
                "mime": "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                "extensi": "MS World (docx)"
            },
            {
                "mime": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                "extensi": "MS Excel (xlxs)"
            },
            {
                "mime": "application/vnd.openxmlformats-officedocument.presentationml.presentation",
                "extensi": "MS Power Point (pptx)"
            },
            {
                "mime": "application/pdf",
                "extensi": "PDF"
            },
            {
                "mime": "image/*",
                "extensi": "Image"
            }
        ],
        validator_list:<?=json_encode($validator_list)?>,
        name:page_meta['name']||'',
        id:page_meta['id']||null,
        cara_hitung:page_meta['cara_hitung']||null,
        id_validator:page_meta['id_validator']||null,
        satuan:page_meta['satuan']||'',
        definisi_konsep:page_meta['definisi_konsep']||'',
        tipe_nilai:page_meta['tipe_nilai']||'',
        type:page_meta['type']||'',
        produsent_data:page_meta['produsent_data']||'',
        options:JSON.parse(page_meta['options']||'[]'),
        bind_view:JSON.parse(page_meta['bind_view']||'[]'),
        file_accept:JSON.parse(page_meta['file_accept']||'[]'),
        classification:page_meta['classification']||false,
        arah_nilai:page_meta['arah_nilai']||0

    },
    created:function(){
        if(this.options.length==0){
            this.options.push({
                tag:null,
                value:null
            });
        }
    },
    watch:{
        "bind_view":{
            deep:true,
            handler:function(){
               this.checkLogic();
            }
        },
        tipe_nilai:function(){
            this.checkLogic();
            console.log(this.tipe_nilai=='string',['single_file','multy_file'].includes(this.type),this.tipe_nilai,this.type);
        },
        classification:function(val){
            if(val){
                this.arah_nilai=0;
                this.type='options';
            }else{
                this.arah_nilai=1;
            }
        }
    },
    methods:{
        checkLogic:function(){
            var index=0;
                this.bind_view.forEach(el => {

                    try {

                        logic=el.logic.replace('#val','val_xxx');
                        if(this.type=='numeric'){
                            var val_xxx=1;
                        }else{
                            var val_xxx='a';
                        }

                        var accepeble=([true,false]).includes(eval(logic));
                        if(accepeble){
                            this.bind_view[index].accept=true;
                        }else{
                            this.bind_view[index].accept=false;
                        }
                    }
                    catch(err) {
                        this.bind_view[index].accept=false;
                    }
                    
                    index+=1;
                });
        },
        moveBind:function(index,pointer){
            var request_index=(index+pointer)>-1?(index+pointer):0
            var element=this.bind_view.splice(index,1)[0];
            this.bind_view.splice(request_index,0,element)
        },
        moveOption:function(index,pointer){
            var request_index=(index+pointer)>-1?(index+pointer):0
            var element=this.options.splice(index,1)[0];
            this.options.splice(request_index,0,element)
        },
        addRowBind:function(){
            this.bind_view.push({
                tag:'',
                logic:'',
                accept:false
            });
        },
        addRowOption:function(){
            this.options.push({
                tag:'',
                value:null,
            });
        },
        checkFileAccept:function(mime){
           return this.file_accept.includes(mime);
        },
        acceptFile:function(mime,ev){
            if($(ev.target).prop('checked')){
                var exist=this.file_accept.indexOf(mime);
                if(exist<0){
                    this.file_accept.push(mime);
                }
            }else{
                var exist=this.file_accept.indexOf(mime);
                if(exist>=0){
                    this.file_accept.splice(exist,1);
                }
            }
        }


    }
});   
</script>

@stop