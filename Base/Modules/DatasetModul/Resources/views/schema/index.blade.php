@extends('adminlte::page')


@section('content')
<script>
    var page_meta=<?=json_encode($page_meta)?>;
</script>
<div class="parallax pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
    <div class="container">
        <section >
            <h3><b>{{$page_meta['title']}}</b></h3>
            <p><b>{!!$page_meta['keterangan']!!}</b></p>
           
        </section>
      
    </div>
   
</div>
<div id="app" class="p-3">
    <div class="btn-group mb-3">
        <a href="{{route('mod.dataset.schema.create',['tahun'=>$StahunRoute])}}" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Tambah Dataset</a>
    </div>
    <hr>
    <div class="table-responsive">
        <table class="table table-bordered " id="table-data">
            <thead class="thead-dark">
                <tr>
                    <th>AKSI</th>
                    <th>NAMA DATASET</th>
                    <th>JENIS DATASET</th>
                    <th>PENANGUNG JAWAB</th>
                    <th>STATUS DATASET</th>
                </tr>
                </thead>
         </table>
    </div>
    <div class="modal fade" id="bs-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Hapus Schema Dataset</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Dataset @{{form_delete.name}}</p>
            </div>
            <div class="modal-footer">
            <form v-bind:action="form_delete.url" method="post">
                @csrf
                <button type="submit" class="btn btn-danger">Hapus</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </form>
            </div>
    </div>
  </div>
</div>

</div>

@stop

@section('js')
<script>
    var app=new Vue({
        el:'#app',
        data:{
            data:[],
            form_delete:{
                name:null,
                url:null
            },
            loading:false,
            ref_table:null
        },
        created:function(){
            var self=this;
            setTimeout((ref) => {
                ref.ref_table=$('#table-data').DataTable({
                    createdRow:function(row,data,index){
                        if(data.status){
                            $(row).addClass('bg-success');
                        }
                    },
                    columns:[
                        {
                            data:'id',
                            sort:false,
                            render:(data,type,row,index)=>{
                                return '<div class="btn-group">'+
                                '<a href="'+('{{route('mod.dataset.schema.show',['tahun'=>$StahunRoute,'id'=>'xxxx'])}}').replace('xxxx',row.id)+'" class="btn btn-success btn-sm"><i class="fa fa-arrow-right"></i></a>'+ 
                                '<button type="button" onclick="app.delete('+(index.row)+')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>'+ 
                                '</div>';
                            },
                            createdCell:(td, cellData, rowDat)=>{
                                $(td).addClass('bg-white');
                            }
                        },
                        {
                            data:'name'
                        },
                        {
                            data:'jenis_dataset',
                            render:(data,type,row,index)=>{
                                return '('+(row.pusat?'Pusat':'Kedaerahan')+') - '+(row.klasifikasi_1||'')+' '+(row.jenis_dataset?'Multy Row':'Single Row');
                            }

                        },
                        {
                            data:'penangung_jawab'
                        },

                        {
                            data:'status',
                            type:'html',
                            render:(data,type,row,index,)=>{
                                
                                return data?'Publish':'Draf';
                            }

                        }
                    ]
                })
                ref.init();
            }, 300,self);
        },methods:{
            init:function(){
                this.current_page=1;
                this.load();
            },
            load:function(){
                var self=this;
                self.loading=true;
                req_ajax.post('{{route('api-web-mod.dataset.schema.load',['tahun'=>$StahunRoute])}}',{
                    page:this.current_page
                }).then(res=>{
                    self.loading=false;
                    res.data.data.forEach(element => {
                        self.data.push(element);
                    });

                    if(self.ref_table){
                        self.ref_table.clear().draw();
                        self.ref_table.rows.add(self.data).draw();

                    }

                    if(res.data.last_page!=self.current_page){
                        self.current_page+=1;
                        self.load();
                    }
                });
            },
            delete:function(index){
                if(this.data[index]!=undefined){
                    var data=this.data[index];
                    var url=('{{route('mod.dataset.schema.delete',['tahun'=>$StahunRoute,'id'=>'xxxx'])}}').replace('xxxx',data.id);
                    this.form_delete.url=url;
                    this.form_delete.name=data.name;
                    $('#bs-modal').modal();
                }
            }
        }
    });   
</script>


@stop