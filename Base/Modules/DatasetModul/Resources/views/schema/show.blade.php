@extends('adminlte::page')


@section('content')
<script>
    var page_meta=<?=json_encode($page_meta)?>;
</script>
<div class="parallax pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
    <div class="container">
        <section >
            <h3>Schema Dataset</h3>
            <h3><b>{{$page_meta['title']}}</b></h3>
            <p><b>{!!$page_meta['keterangan']!!}</b></p>
        </section>
      
    </div>
   
</div>

<div class="p-3" id="app">

    <div class="btn-group mb-2" v-if="form.id">
         <button @click="delete_dataset()" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> @{{form.name}}</button>    
    </div>
    <div class="card" >
        
        <form action="{{$url}}" id="form-dataset" method="post">
            @csrf
            <div>
                <input type="hidden" name="name" v-model="form.name">
                <input type="hidden" name="jenis_dataset" v-model="form.jenis_dataset">
                <input type="hidden" name="table" v-model="form.table" v-if="administrator">
                <input type="hidden" name="penangung_jawab" v-model="form.penangung_jawab">
                <input type="hidden" name="tujuan" v-model="form.tujuan">
                <input type="hidden" name="kode" v-model="form.kode">
                <input type="hidden" name="pusat" v-model="form.pusat">
                <input type="hidden" name="kategori" v-model="com_category">
                <input type="hidden" name="klasifikasi_1" v-model="form.klasifikasi_1">
                <input type="hidden" name="klasifikasi_2" v-model="com_klasifikasi_2">            
                <input type="hidden" name="jenis_distribusi" v-model="form.jenis_distribusi">
                <input type="hidden" name="login" v-model="form.login">
                <input type="hidden" name="status" v-model="form.status">
                <input type="hidden" name="tanggal_publish" v-model="form.tanggal_publish">
                <input type="hidden" name="list_data" v-model="dataEncode">
                <input type="hidden" name="list_data_recorded" v-model="dataRecordedEncode">
                <input type="hidden" name="permission" v-model="permission_com">
                <input type="hidden" name="admin_menu" v-model="form.admin_menu">

            </div>    
        </form>
        <div class="card-header with-border bg-dark">
            <h4 class="title">@{{step.title}}</h4>
        </div>
        <div class="card-body">
            <div class="" v-if="step.page==1">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Lingkup Dataset*</label>
                        <select name="" class="form-control" id="" v-model="form.pusat">
                                <option value="0">KEDAERAHAN</option>
                                <option value="1">PUSAT</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Jenis Record Data*</label>
                            <select name="" class="form-control" id="" v-model="form.jenis_dataset">
                                <option value="0">Single Row</option>
                                <option value="1">Multy Row</option>
                            </select>

                        </div>
                        <div class="form-group">
                            <label for="">Group Dataset</label>
                            <select name="" class="form-control" id="" v-model="form.admin_menu">
                                <option value="">Tidak Masuk Group</option>
                                <option value="0">Data Utama</option>
                                <option value="1">Data Pendukung</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Nama*</label>
                            <input type="text" required class="form-control" v-model="form.name">
                        </div>
                        <div class="form-group">
                            <label for="">Kategori</label>
                        <v-select multiple :options="list_kategori" v-model="form.kategori"></v-select>
                        </div>
                        <div class="form-group">
                            <label for="">Induk Data</label>
                        <v-select  :options="list_klasifikasi" v-model="form.klasifikasi_1"></v-select>
                        </div>
                        <div class="form-group">
                            <label for="">Lingkup Bantuan</label>
                        <v-select multiple :options="list_klasifikasi_2" v-model="form.klasifikasi_2"></v-select>
                        </div>
                        <div class="form-group" v-if="administrator">
                            <label for="">Table*</label>
                            <input type="text" required class="form-control" v-model="form.table">
                        </div>
                        <div class="form-group">
                            <label for="">Penangung Jawab*</label>
                            <input type="text" required class="form-control" v-model="form.penangung_jawab">
                        </div>
                        <div class="form-group">
                            <label for="">Tujuan*</label>
                            <textarea name="" class="form-control" id="" cols="30" rows="10" v-model="form.tujuan"></textarea>
                            
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Kodefikasi Data</label>
                            <input type="text" required class="form-control" v-model="form.kode">
                        </div>
                        <div class="form-group">
                            <label for="">Jenis Distribusi*</label>
                            <select name="" class="form-control" id="" v-model="form.jenis_distribusi">
                                <option value="1">Internal</option>
                                <option value="0">External</option>
                            </select>
                        </div>
                        <div class="form-group" v-if="form.jenis_distribusi==0">
                            <label for="">Perlu Login Untuk Melihat data*</label>
                            <select name="" class="form-control" id="" v-model="form.login">
                                <option value="1">Ya</option>
                                <option value="0">Tidak</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Status*</label>
                            <select name="" class="form-control" id="" v-model="form.status">
                                <option value="1">Publish</option>
                                <option value="0">Draf</option>
                            </select>

                        </div>
                        <div class="form-group" v-if="form.status"> 
                            <label for="">Tanggal Publish* </label>
                            <input type="date" required class="form-control" v-model="form.tanggal_publish">
                        </div>
                    
                    </div>
                </div>
                <div class="col-12">
                    <hr>
                    <div class="btn-group mb-2">
                        <button class="btn btn-primary" type="button" @click="step.page=2; step.title='Schema Data'"><i class="fa fa-arrow-right"></i> Tambah Data</button>
                    </div>
                </div>
            </div>
            <div v-if="step.page==2">
                <div class="row">
                    <div class="col-6">
                        <button class="btn btn-sm btn-success mb-2" @click="add(-1)"><i class="fa fa-plus"></i> Tambah Data</button>
                        <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead class="thead-dark">
                                <tr>
                                    <th style="width:200px;">AKSI</th>
                                    <th>NAMA DATA</th>
                                    <th>SETTING</th>
                                </tr></thead>
                                <tbody>
                                    <tr v-bind:class="index_update==k?'bg-navy':''" v-for="item,k in form.data">
                                        <td>
                                            <div class="btn-group">
                                                <button @click="showDelete(item,k)"  class="btn btn-danger btn-sm" type="button"><i class="fa fa-trash"></i></button> 
                                                <button @click="move(k,-1)" v-if="k!=0 " class="btn btn-primary btn-sm" type="button"><i class="fa fa-arrow-up"></i></button>  
                                                <button @click="move(k,1)" v-if="k!=(form.data.length-1) && form.data[k+1]!=undefined" class="btn btn-primary btn-sm" type="button"><i class="fa fa-arrow-down"></i></button>
                                                <button @click="add(k)" class="btn btn-success btn-sm" type="button"><i class="fa fa-plus"></i> <i class="fa fa-arrow-down"></i></button>    

                                            </div>
                                        </td> 
                                        <td>
                                            <p class="m-0">@{{item.name}} @{{item.satuan!='-'?'('+item.satuan+')':''}}</p>
                                            <small class="">
                                                <template v-if="item.classification">
                                                    <span class="badge bg-success">Klasifikasi</span>
                                                </template>
                                                <span class="text-primary">Field Name</span> : @{{item.field_name}}</small>
                                        </td> 
                                        <td>
                                            <button  @click='setting(k)' class="btn btn-warning btn-sm" type="button"><i class="fas fa-bars"></i></button>
                                        </td>    
                                    
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-6">
                        <input type="text" class="form-control mb-2" v-model="form_data.search" placeholder="Cari Jenis Data">
                        <p class="m-0 bg-warning p-2" v-if="form_data.name"><b>Tambah Data di Bawah @{{form_data.name}}</b></p>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>AKSI</th><th>NAMA DATA</th><th>INFO</th></tr>
                                </thead>
                                <tbody>
                                    <tr v-if="form_data.loading">
                                        <td class=" text-center" colspan="3">
                                            <div class="spinner-border text-primary" role="status">
                                                <span class="sr-only">Loading...</span>
                                                </div>
                                                <div class="spinner-border text-warning" role="status">
                                                <span class="sr-only">Loading...</span>
                                                </div>
                                                <div class="spinner-border text-success" role="status">
                                                <span class="sr-only">Loading...</span>
                                                </div>
                                                <div class="spinner-border text-danger" role="status">
                                                <span class="sr-only">Loading...</span>
                                                </div>
                                            </td></tr>
                                    <tr v-for="item,k in form_data.data">
                                        <td>
                                            <button @click="add_row(item)" class="btn btn-success btn-sm" type="button"><i class="fa fa-plus"></i></button>    
                                    
                                            </td>
                                        <td>@{{item.name}}</td>
                                        <td>
                                            <button @click="showInfo(item)" class="btn btn-warning btn-sm" type="button"><i class="fas fa-bars"></i></button>

                                            </td>
                                    </tr>
                                    <tr v-if="form_data.data.length==0&&!form_data.loading">
                                        <td colspan="3" class="bg-secondary text-center">Tidak Terdapat Data</td></tr>
                                </tbody>
                            </table>

                        </div></div>
                </div>    
                <div class="col-12">
                    <hr>
                    <div class="btn-group mb-2">
                        <button class="btn btn-primary" type="button" @click="step.page=1; step.title='Schema Dataset'"><i class="fa fa-arrow-left"></i> Schema Dataset</button>
                        <button class="btn btn-primary" type="button" @click="step.page=3; step.title='Role Dataset'"><i class="fa fa-arrow-right"></i> Role Dataset</button>
                        <button class="btn btn-success" type="button" @click="submit()">Kirim</button>
                    </div>
                </div>
                
            </div>
            <div v-if="step.page==3">
                <div class="row">
                    <div class="col-12" v-for="item,i in role.roles">
                    <h4><b>@{{item.name}}</b></h4>
                    <div class="d-flex">
                        <div v-for="permit,i in  item.permissions"  class="flex-column d-flex">
                        <label for="">@{{permit.name}}</label>
                        <toggle-button :height="25" class="mr-2" :width="150" v-model="permit.state" 
                          :labels="{checked: permit.name+' Allow', unchecked: permit.name+' Disabled'}"/>
                        </div>    
                    </div>
                    <hr>

                       
                    </div>
                </div>
                <div class="col-12">
                    <hr>
                    <div class="btn-group mb-2">
           
                        <button class="btn btn-primary" type="button" @click="step.page=2; step.title='Schema Data'"><i class="fa fa-arrow-left"></i> Tambah Data</button>

                        <button class="btn btn-success" type="button" @click="submit()">Kirim</button>
                    </div>
                </div>
            </div>

        

            <div class="modal fade" id="modal-info" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">@{{form_set.name}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p><b class="text-success">Produsen Data :</b> @{{form_set.produsent_data}}</p>
                        <p><b class="text-success">Validator :</b> @{{form_set.validator}}</p>
                        <p><b class="text-success">Satuan :</b> @{{form_set.satuan}}</p>
                        <p><b class="text-success">Tipe Nilai :</b> @{{form_set.tipe_nilai}}</p>
                        <p><b class="text-success">Definsi Konsep :</b> </p>
                        <p>@{{form_set.definisi_konsep}}</p>
                    
                        <p><b class="text-success">Tipe Pengisian :</b> @{{form_set.type}}</p>
                        <template v-if="form_set.type=='option' ">
                        <p><b class="text-success">Optional Nilai Pengisian :</b></p>
                        <p v-for="item,k  in json_decode(form_set.options) ">@{{k+1}}. @{{item.tag}} = @{{item.value}}</p>
                        </template>
                        <p><b class="text-success">Binding Nilai Pada Tampilan</b></p>
                        <p v-for="item,k in json_decode(form_set.options) ">@{{k+1}}. @{{item.tag}} = @{{item.logic}}</p>
                        <template v-if="form_set.type=='file' || form_set.type=='multy_file'">
                            <p><b class="text-success">Accept File Mime</b></p>
                            <p v-for="item,k in json_decode(form_set.options) ">@{{k+1}}. @{{item.tag}} = @{{item.mime}}</p>
                        </template>
                    </div>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="modal-set" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">@{{form_set.name}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                        <label for="" class="text-success">Field Name</label>
                        <input type="text" class="form-control" v-model="form_set.field_name">
                        </div>
                        <p><b class="text-success">Produsen Data :</b> @{{form_set.produsent_data}}</p>
                        <p><b class="text-success">Validator :</b> @{{form_set.validator}}</p>
                        <p><b class="text-success">Satuan :</b> @{{form_set.satuan}}</p>
                        <p><b class="text-success">Tipe Nilai :</b> @{{form_set.tipe_nilai}}</p>
                        <p><b class="text-success">Definsi Konsep :</b> </p>
                        <p>@{{form_set.definisi_konsep}}</p>
                        <p><b class="text-success">Tipe Pengisian :</b> @{{form_set.type}}</p>
                        <template v-if="form_set.type=='option' ">
                        <p><b class="text-success">Optional Nilai Pengisian :</b></p>
                        <p v-for="item,k  in json_decode(form_set.options) ">@{{k+1}}. @{{item.tag}} = @{{item.value}}</p>
                        </template>
                        <p><b class="text-success">Binding Nilai Pada Tampilan</b></p>
                        <p v-for="item,k in json_decode(form_set.options) ">@{{k+1}}. @{{item.tag}} = @{{item.logic}}</p>
                        <template v-if="form_set.type=='file' || form_set.type=='multy_file'">
                            <p><b class="text-success">Accept File Mime</b></p>
                            <p v-for="item,k in json_decode(form_set.options) ">@{{k+1}}. @{{item.tag}} = @{{item.mime}}</p>
                        </template>
                    </div>

                    
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal-delete" v-if="modal_del.name" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title">Hapus Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Hapus data @{{modal_del.name}}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" @click="deleteRawData()" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus</button>
                    </div>

                    </div>
                </div>
            </div>

            

        </div>
    </div>
    <div class="modal fade" id="modal-delete-dataset"  tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title">Hapus Dataset</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Hapus data @{{form.name}}</p>
            </div>
            <div class="modal-footer">
                <form v-bind:action="form_delete_dataset.url" method="post">
                    @csrf
                    <button type="submit" class="btn btn-danger">Hapus</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </form>
            </div>

        </div>
    </div>
</div>


</div>

@stop

@section('js')
@php
if(count(session()->getOldInput())){
    $old_data=session()->getOldInput();
    unset($old_data['_token']);
    unset($old_data['list_data']);
    unset($old_data['list_data_recorded']);
    $old_data['login']=($old_data['login']=='0'?false:true);
    $old_data['pusat']=($old_data['pusat']=='0'?false:true);
    $old_data['jenis_dataset']=($old_data['jenis_dataset']=='0'?false:true);
    foreach ($page_meta as $key => $value) {
       if(isset($old_data[$key])){
            $page_meta[$key]=$old_data[$key];
       }
    }



}

@endphp
<script>
    var old=<?=json_encode($page_meta);?>;
    

    var app=new Vue({
    el:'#app',
    data:{
        list_kategori:[
            'Master','Data Kesepakatan','Data Utama',
            'Data Pendukung','Sinkronisasi Aplikasi','Input Rekap'
        ],
        list_klasifikasi:['Data Pemda','Data BUMDAM'],
        list_klasifikasi_2:['Bantuan Stimulan','Bantuan Pendamping','Bantuan Basis Kinerja'],
        administrator:true,
        form_data:{
            loading:false,
            index:0,
            name:'',
            data:[],
            ref_timeout:null
        },
        role:{
            roles:<?=old('permission')?json_encode(old('permission')):json_encode($roles)?>,
        },
        modal_del:{
            name:'',
            index:0
        },
        form_delete_dataset:{
            url:null,
            name:null
        },
        form:{
            id:old.id,
            admin_menu:old.admin_menu,
            data_recorded:[],
            data:<?=json_encode($data)?>,
            jenis_dataset:old.jenis_dataset?1:0,
            status:old.status?1:0,
            kode:old.kode,
            table:old.table,
            name:old.name,
            kategori:JSON.parse(old.kategori||'[]'),
            tujuan:old.tujuan,
            klasifikasi_1:old.klasifikasi_1,
            klasifikasi_2:JSON.parse(old.klasifikasi_2||'[]'),
            penangung_jawab:old.penangung_jawab,
            jenis_distribusi:old.jenis_distribusi?1:0,
            login:old.login?1:0,
            pusat:old.pusat?1:0,
            tanggal_publish:old.tanggal_publish
        },
        index_update:-1,
        scope:[],
        loading:false,
        form_set:{
            id:0,
            admin_menu:0,

            produsent_data:'',
            validator:'',
            ref_timeout:null,
            old_field_name:'',
            name:'',
            field_name:'',
            id_data:0,
            tipe_nilai:'string',
            type:'',
            definisi_konsep:'',
            satuan:'',
            options:'[]',
            bind_value:'[]',
            file_accept:'[]',
        },
        step:{
            page:1,
            title:'Meta Dataset'
        }
       
    },
    computed:{
        permission_com:function(){
            var pe=[];
            this.role.roles.forEach(el => {
                
                var perms=Object.values(el.permissions).filter((p)=>{
                        return p.state==true;
                });
                var permit=perms.map(el=>{
                    return el.key;
                });

                
                
                pe.push({
                    'id':el.id,
                    'name':el.name,
                    'permissions':permit
                })
            });

            return JSON.stringify(pe);
            
        },
        dataEncode:function(){
            return JSON.stringify(this.form.data);
        },
        dataRecordedEncode:function(){
            return JSON.stringify(this.form.data_recorded);
        },
        com_category:function(){
            return JSON.stringify(this.form.kategori||[]);
        },
        com_klasifikasi_2:function(){
            return JSON.stringify(this.form.klasifikasi_2||[]);
        }

    },
    created:function(){
        var self=this;
        this.form.data.forEach((el,index)=>{

            self.form.data_recorded.push({
                id:el.id,
                field_name:el.field_name,
                new_field_name:null,
            });

            self.form.data[index].field_name=this.checkFieldUnique(this.form.data[index].field_name,index);
            
        });
    },
    watch:{
        "form.data.table":function(val){
            val=val.toLowerCase();
            val=val.replace(/ /g,'_');
            this.form.data.table=val;
        },
        "form.data":{
            deep:true,
            handler:function(){
                var index=0;
                self=this;
                this.form.data.forEach(el => {
                    el.index=index;
                    self.form.data[index]=el;
                    index+=1;
                });
            }
        },
        "form_data.search":function(val){
            if(this.form_data.ref_timeout){
                clearTimeout(this.form_data.ref_timeout);
            }
            var self=this;
            this.form_data.ref_timeout=setTimeout((ref) => {
                ref.form_data.loading=true;
                req_ajax.post('{{route('api-web-mod.dataset.schema.load_list_data',['tahun'=>$StahunRoute])}}',{
                    search:val
                }).then(res=>{
                    ref.form_data.ref_timeout=null;
                    ref.form_data.loading=false;
                    ref.form_data.data=res.data;
                });
            }, 1000,self);
        },
        "form_set.field_name":function(val){
            if(this.form.data[this.index_update]){
                var self=this;

                if(this.form_set.ref_timeout){
                    clearTimeout(this.form_set.ref_timeout);
                }

                this.form_set.ref_timeout=setTimeout((ref)=>{
                    if(val){
                        val=ref.checkFieldUnique(val,ref.index_update);
                        ref.form.data[ref.index_update].field_name=val;
                    }else{
                        ref.form.data[ref.index_update].field_name=ref.form_set.old_field_name;
                        ref.form_set.field_name=ref.form_set.old_field_name;
                    }
                    if(ref.form.data[ref.index_update].id){
                        ref.form.data_recorded.forEach((d,i)=>{
                            if(d.id==ref.form.data[ref.index_update].id){
                                if(d.field_name!=ref.form.data[ref.index_update].field_name){
                                    ref.form.data_recorded[i].new_field_name=ref.form.data[ref.index_update].field_name;
                                }else{
                                    ref.form.data_recorded[i].new_field_name=null;
                                }
                            }
                        });
                    }
                    ref.form_set.ref_timeout=null;

                },500,self);
            }
        }
    },
    methods:{
       
        delete_dataset:function(){
           if(this.form.id){
            var url=('{{route('mod.dataset.schema.delete',['tahun'=>$StahunRoute,'id'=>'xxxx'])}}').replace('xxxx',this.form.id);
            this.form_delete_dataset.url=url;
            this.form_delete_dataset.name=this.form.name;
            $('#modal-delete-dataset').modal();
           }
        },
        submit:function(){
            $('#form-dataset').submit()
        },
        showDelete:function(data,index){
            this.modal_del.name=data.name;
            this.modal_del.index=index;
            $('#modal-delete').modal();
            this.index_update=index;
        },
        showInfo:function(data){
            this.form_set=data;
            $('#modal-info').modal();
        },
        deleteRawData:function(){
            var index=this.modal_del.index;
            if(this.form.data[index]!=undefined){
                this.form.data.splice(index,1);
            }
            $('.modal').modal('hide');
        },
        checkFieldUnique:function(field,index_row){
            var valid=true;
            var index=0;
            var new_field=field.replace(/ /g,'_').toLowerCase();
            this.form.data.forEach(el=>{
                    if(el['field_name']==new_field){
                        if(index!=index_row){
                            valid=false;
                            new_field=new_field+'_'+parseInt(Math.random()*10);

                        }
                        index+=1;
                    }else{
                        index+=1;
                    }

            });


            if(valid==false){
                return this.checkFieldUnique(new_field,index_row);
            }else{
                return new_field;
            }
        },
        setting:function(index){
            this.index_update=index;
            this.form_set=this.form.data[index];
            this.form_set.old_field_name=this.form_set.field_name;
            $('#modal-set').modal();

            
        },
        json_decode:function(val){
            return JSON.parse(val);  
        },
        add:function(index){
            this.form_data.index=index+1;
            if(this.form.data[index]!=undefined){
                this.index_update=index;
                var data=this.form.data[index];
                this.form_data.name=data.name;
            }else{
                this.form_data.name='';

            }   
        },
        add_row:function(data){
            var valid=true;
            var index=0;
            var index_update_l=0;
            this.form.data.forEach(el=>{
                if(el.id_data==data.id_data){
                    valid=false;
                    index_update_l=index;
                }else{
                    index+=1;
                }
            });

            if(valid){
                data.field_name=this.checkFieldUnique(data.field_name,-1);
                this.form.data.splice(this.form_data.index,0,data);
                this.index_update=this.form_data.index;
                this.form.data_recorded.forEach((d,i)=>{
                    if(d.id==this.form.data[this.index_update].id){
                        if(d.field_name!=this.form.data[this.index_update].field_name){
                            this.form.data_recorded[i].new_field_name=this.form.data[this.index_update].field_name;
                        }
                    }
                });

            }else{

                this.index_update=index_update_l;

            }
        },
        move:function(index,pointer){
            const toIndex = (pointer+index)<0?0:(pointer+index);
            const element = this.form.data.splice(index, 1)[0];
            this.form.data.splice(toIndex, 0, element);
            this.index_update=toIndex;
        }

    }
});


    </script>

@stop