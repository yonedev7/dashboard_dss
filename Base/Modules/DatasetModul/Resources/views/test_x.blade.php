@extends('adminlte::page')

@section('content')
<style>
    .small-text{
        font-size:10px;
    }
    .focus-data-column{
        background: rgb(172, 255, 120);
    }
    .focus-data-row{
        background: rgb(235, 235, 235);
    }

</style>
<table id="xxxx" class="table table-bordered">
   
</table>
<div class="" id="app">
    <div class="p-3  rounded"  v-if="show_meta.display" style="z-index:9999; float:left; position: fixed; margin:auto; top:0; left:0; right:0; max-width:30%;" id="show_data_column">
        <div style="background-color:rgba(255, 255, 255, 0.921)" class="p-2 rounded">
            <h5><b>@{{show_meta.nama_pemda}} - @{{show_meta.tahun}}</b></h5>
            @{{show_meta.name}} - @{{((show_meta.satuan!='-' && show_meta.satuan!='')?'('+show_meta.satuan+')':'')}}
        </div>
    </div>
    <h5>Total : <b>@{{rows_data.length}} Data</b></h5>
    <div class="d-flex flex-column" style="height:calc(100vh - 50px)">
        <div class="table-responsive d-flex"  style="max-height:40vh; overflow:scoll; border-top:2px solid #000">
            <table class="table table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th rowspan="2">No</th>
                        <th rowspan="2">Select</th>
                        <th rowspan="2">Aksi</th>
    
                        <th v-for="col,ck in columns">
                            <span class="small-text">@{{col.name}}</span>
                        </th>
                    </tr>
                    <tr>
                        <th v-for="col,ck in columns" class="small-text">
                           <span> @{{ck+1}}</span>
                            <template >
                                <span  v-if="columns_includes_filter(col)" class="badge badge-danger"><i class="fa  fa-filter"></i></span>
                                <span v-else class="badge badge-success" @click="addFilter($event,col)"><i class="fa  fa-filter"></i></span>
                            </template>
                        </th>
    
                    </tr>
                </thead>
                <tbody>
                    <tr v-bind:id="'row-input-'+i" v-for="item,i in rows_data">
                        <td>@{{i+1}}</td>
                        <td></td>
                        <td></td>
                        <td v-for="col,ck in columns" @mouseover="showMetaColumn($event,col,item)" @mouseleave="disabledMetaColumn($event,col,item)">
                            @{{item[col.field_name]!=undefined?item[col.field_name]:'-'}} @{{((col.satuan!='-' && col.satuan!='')?col.satuan:'')}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="flex-grow-1 d-flex p-3" style="border-top:2px solid rgb(255, 255, 12);">
            <div class="row">
                <template v-if="form_edit.mode">
                    <div class="row">
                        <div class="col-6" v-for="col,k in columns_split">
                            <div class="form-group">
                                <label for="">@{{col.name}}</label>
                            </div>
                        </div>
                    </div>

                </template>
            </div>
        </div>
    </div>
    
</div>

@stop

@section('js')
<script>
    $(document).ready(function() {

    $('#xxxx').DataTable({
        serverSide: true,
        columns:[
            {
                render:function(data,show,row){
                    return data;
                },
                data:'id'
            },
            {
                data:'kodepemda',
                label:'Kodepemda'
            },
            {
                data:'name',
                label:'nama pemda'
            }
        ],
        processing: true,
        ajax: '{{url('test-data/2022/4/104')}}',
    });
});

var appVue=new Vue({
    el:'#app',
    data:{
        data:<?=$data?>,
        filter:[],
        form_edit:{
            mode:1,
            index_col:-1,
            index_row:-1,
            data:{}
        },
        show_meta:{
            display:0,
            name:'',
            nama_pemda:'',
            tahun:'',
            satuan:''
        },
        columns:<?=$columns?>,
        index_record:[],
    },
    computed:{
        columns_split:function(){
            var arr=this.columns;
            var len=2;
            var chunks = [], i = 0, n = arr.length;
            while (i < n) {
                chunks.push(arr.slice(i, i += len));
            }
            return chunks;
        },
        rows_data:function(){
           return this.data.filter(el=>{
                return ((['none','update']).includes(el.method) || el.id==null);
            });
        }
    },
    watch:{
        'form_edit.data':{
            deep:'intermediet',
            handler:function(val,old){
                this.data[this.form_edit.index_row]=val;
            }
        }
    },
    methods:{
        reIndexData:function(){
            this.data.forEach((el,k)=>{
                this.data[k].index=k;
            });
        },
        submitForm:function(){
            var form=new FromData();
                form.append('_token','{{csrf_token()}}');

        },
        showEdit:function(event,index_col,index_row){
            this.reIndexData();
            this.form_edit.index_col=index_col;
            this.form_edit.index_row=index_row;
        },
        showMetaColumn:function(ev,col,data){
            $(ev.target).addClass('focus-data-column');
            $(ev.target).parent().addClass('focus-data-row');
            this.show_meta.name=col.name;
            this.show_meta.satuan=col.satuan;
            this.show_meta.tahun=data.tahun;
            this.show_meta.nama_pemda=data.name;
            this.show_meta.display=1;
        },
        addFilter:function(ev,col){
            if(!$('body').hasClass('control-sidebar-slide-open')){
                $('[data-widget="control-sidebar"]').click();
            }
            window.filter_data.addFilter(col);
        },
        disabledMetaColumn:function(ev,col,data){
            $(ev.target).removeClass('focus-data-column');
            $(ev.target).parent().removeClass('focus-data-row');
            this.show_meta.display=0;

        },
        columns_includes_filter:function(col){
            return this.filter.filter(el=>{
                return el.field_name==col.field_name;
            }).length!=0;
        },

        deleteRow:function(index){
            this.data.splice(index,1);
        },
        addRow:function(index,to_index,data_def=null){

        },
        AddbulkSelect:function(input,index){
            
        }
    }
});
</script>


@stop

@section('right-sidebar')
    <div id="filter_data" class="text-dark">
    <div class="card">
        <div class="card-body text-dark">
            <p><b>@{{col.name}}</b></p>
            <div class="table-responsive" style="height: 300px; overflow:scroll;">
            <table class="table table-bordered" >
                <thead>
                    <tr>
                        <th></th>
                        <th>Value</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="item,i in freq_list">
                        <td>
                            <input type="checkbox" v-bind:value="i">
                        </td>
                        <td>
                        <p>@{{item.val}} <span class="badge badge-primary rounded-circle">@{{item.f}}</span> </p></td>
                    </tr>
                </tbody>
            </table>
        </div>
        </div>
    </div>    
    </div>



@stop


@push('js')
<script>
var filter_data=new Vue({
    el:'#filter_data',
    data:{
        col:{
            name:null,
            field:null,
            type:null,
            options:null,
            selected:[],
        },
        freq:[]
    },
    watch:{
        "col.name":function(){
            this.createFreqlist();
        }
    },
    methods:{
        addFilter:function(col){
           
            this.col.name=col.name;
            this.col.field=col.field_name;
            this.col.type=col.type;
            this.col.select=[];
            
        },
        createFreqlist:function(){
            var freq={};
            if(window.appVue!==undefined){
                window.appVue.data.forEach((el)=>{
                var v=(el[this.col.field]||'NULL').replace(/\s+/g);

                if(freq[v]==undefined){
                    freq[v]={
                        'f':1,
                        'val':(el[this.col.field]||'NULL')
                    }
                }else{
                    freq[v]['f']+=1;
                }
                
                
            });

            }
            this.freq=Object.values(freq);


        }
    },
    computed:{
        freq_list:function(){
            return this.freq;
        }
    }
});    
</script>

@endpush