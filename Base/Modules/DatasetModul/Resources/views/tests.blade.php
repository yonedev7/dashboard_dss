@extends('adminlte::page')

@section('content')
<style>
    .laravel-vue-datatable-td{
        padding:2px;
        font-size:12px;
    }
    .laravel-vue-datatable-th{
        padding:2px;
        font-size:12px;
    }

</style>
<div class="p-3">
    <p><b></b></p>
    <div class="btn-group">
        <button class="btn btn-primary">Validasi Data</button>
        <button class="btn btn-success">Verifikasi Data</button>
        <button class="btn btn-warning">Dokumen Pengesahan</button>


    </div>
</div>
<div class="h-100 " id="app">
    <div class="bg-white p-3 rounded" v-if="info_column.column_name" style="max-width:30%;z-index:999; position: fixed; top:70px; left:0; right:0; margin:auto; border:1px solid #ddd; ">
        <p class="m-0">@{{info_column.column_name}}</p>
        <p class="m-0"><small>@{{info_column.value}} @{{info_column.satuan}}</small></p>
    </div>

    <div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-full" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">@{{form.method=='edit'?'Rubah Data':'Tambah Data'}}</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-6" v-for="b,i in com_group_input">
                        <div class="form-group" v-for="col,c in b">
                            <label for="">@{{col.name}}</label>
                            <input type="text" class="form-control" v-model="form.data[col.field_name]">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save changes</button>
            </div>
          </div>
        </div>
      </div>
    <div class="d-flex flex-grow-1 bg-white p-3">
        
        <template>
        <data-table  ref="table_data_exist"  :filters="filters" :translate="translate" :headers="headers" :debounce-delay="1000" :columns="columns_table" :url="url_data" >
            <template slot="body" slot-scope="{ data }">
                <tbody >
                    <tr
                        :key="datakey"
                       
                        v-for="(item,datakey) in data">
                        <td 
                            :key="column.name"
                            v-for="column,key in columns_table" @mouseover="tdHover(column,item)" @mouseleave="tdHover(null,item)" >
                            <template v-if="column.label!='Aksi'">
                                
                            <data-table-cell 
                                :value="item"
                                :name="column.name"
                                :meta="column.meta"
                                :comp="column.component"
                                :classes="column.classes">
                            </data-table-cell>
                            </template>

                            <template v-else>
                                <div class="btn-group">
                                    <button @click="edit(item)" class="btn btn-warning btn-sm"><i class="fa fa-pen"></i></button>
                                    <button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>

                                </div>
                            </template>
                        </td>
                    </tr>
                </tbody>
            </template>
            <div slot="filters" slot-scope="{ tableFilters, perPage }">
                <div class="row mb-2">
                    <div class="col-md-4">
                        <select class="form-control" v-model="tableFilters.length">
                            <option :key="page" v-for="page in perPage">@{{ page }}</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <select
                            v-model="tableFilters.filters.columns_serach"
                            class="form-control">
                            <option value>All</option>
                            <option :value='c.field_name' v-for="c,ci in columns">@{{c.name}}</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <input
                            name="name"
                            class="form-control"
                            v-model="tableFilters.search"
                            placeholder="Search Table">
                    </div>
                </div>
            </div>


        </data-table>
        </template>
    </div>
</div>

@stop
@php

@endphp

@section('js')
<script>
   
var app=new Vue({
    el:'#app',
    created:function(){
        var self=this;
        var them_f={};

        this.columns.forEach(el=>{
           if(el.show){
                self.columns_table.push({
                    label:el.name,
                    name:el.field_name,
                    satuan:el.satuan
                });
           }
           if(el.field_name=='kodepemda'){
                them_f[el.field_name]=this.info_pemda.kodepemda;
            }else if(el.field_name=='kodebu'){
                them_f[el.field_name]=this.info_pemda.kodebu;
            }
        });
        Object.assign(this.form_def,them_f);
        Object.assign(this.form.data,them_f);

        this.headers.Authorization+=(window.xdss{{md5(url()->current())}}).token;
    },
    data:{

        info_pemda:{
            kodepemda:'',
            nama_pemda:'',
            kodebu:'',
            nama_bu:''
        },
        info_column:{
            column_name:'',
            value:'',
            satuan:''
        },
        url:'{{route('api-web-mod.dataset.form.load_data_pemda',['tahun'=>$StahunRoute,'id_dataset'=>$dataset->id,'kodepemda'=>'01101'])}}',
        filters: {
            columns_serach: '',
        },
        headers:{
            Authorization:'Bearer ',
        },
       
        columns:<?=json_encode($columns)?>,
        translate:{ 
            nextButton: '>', 
            previousButton: '<',
            placeholderSearch: 'Cari Data'
        },
        columns_table:[
            {
                label:'Aksi',
                name:'status_data',
               
                
            }
        ],
        form:{
            method:'create',
            data:{

            }
        },
        form_def:{}
    },
   
    methods:{
        chuckSplit:function(input,size){
            var chunked=[];
            var x=Math.ceil(input.length / size);
            if(x){
                return this.chunk(input,x);
            }else{
                return [input];
            }
        },  
        chunk:function(input,size){
            var chunked=[];
            Array.from({length: Math.ceil(input.length / size)}, (val, i) => {
                 chunked.push(input.slice(i * size, i * size + size))
            });

            return chunked;
        },
        changeIndex:function(){

        },
        edit:function(item){
            this.form.method='edit';
            this.columns_table.forEach(el=>{
                this.form.data[el.name]=item[el.name]||null;
            });

            $('#modal-edit').modal();
        },
        submit:function(){

        },
        tdHover:function(column=null,data){
            if(column!=null && (column['label']!=undefined?column.label!='Aksi':false)){
                this.info_column.column_name=column.label;
                this.info_column.value=(data[column.name]!=undefined?data[column.name]:null)
                this.info_column.satuan=column.satuan;

            }else{
                this.info_column.column_name=null;
            }
        },
        showRowMeta(id) {
            alert(`you clicked row ${id}`);
        }
    },
    computed:{
        url_data:function(){
            
            return this.url+'?'+( new URLSearchParams(this.filter).toString());
        },
        com_group_input:function(){
            var field_list=this.columns_table.map(el=>{
                return el.name;
            });
            var group_input=this.columns.filter(el=>{
                return field_list.includes(el.field_name);
            });

            console.log(group_input);

            return this.chuckSplit(group_input,2);
        }
    }

});

</script>

@stop