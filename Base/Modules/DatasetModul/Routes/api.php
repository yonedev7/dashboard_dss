<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::prefix('mod/{tahun}/module-dataset-p')->name('api-web-mod.dataset.2')->group(function(){
//     Route::prefix('form')->name('.form')->group(function(){
//         Route::get('/load-data/{id_dataset}/{kodepemda}', 'FormExcelController@loadDataPemda')->name('.load_data_pemda');
//     });
// });


Route::post('/get-column/{id_dataset}','MasterController@list_column_dataset')->name('api.kolum_dataset');


Route::prefix('mod/{tahun}/module-dataset-public/')->name('api-web-public-mod.dataset')->group(function(){
    Route::post('/load-cluster-data/{context}','ClusterDatasetAnggaranController@get')->name('.cluster.show');
});

Route::prefix('mod/{tahun}/module-dataset')->name('api-web-mod.dataset')->middleware('auth:api')->group(function(){
    Route::post('/','DatasetModulController@load')->name('.load');


    Route::post('/get-meta/{kodepemda}','DatasetModulController@ajax_kodepemda')->name('.meta_pemda');
    Route::post('/load-per-pemda/{id_dataset}/{tw}','DatasetModulController@ajax_load_multy')->name('.ajax_load_multy');
    Route::post('/load-dataset/{id_dataset}/{tw}','DatasetModulController@ajax_load_single')->name('.ajax_load_single');

    Route::post('/upload_file/{id_dataset}/{tw}','DatasetModulController@upload_file')->name('.upload_file');
    Route::prefix('form')->name('.form')->group(function(){
        Route::get('/load-data/{id_dataset}/{kodepemda}', 'FormExcelController@loadDataPemda')->name('.load_data_pemda');
    });
   


    Route::prefix('schema')->name('.schema')->group(function(){
        Route::post('/load', 'MasterController@load')->name('.load');
        Route::post('/load-list-data', 'MasterController@load_list_data')->name('.load_list_data');
    });

    Route::prefix('schema-data')->name('.schema-data')->group(function(){
        Route::post('/load', 'MasterDataController@load')->name('.load');
    });

});

include __DIR__.'/api_data.php';
include __DIR__.'/api_form.php';


