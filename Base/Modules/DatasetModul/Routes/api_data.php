<?php


Route::prefix('mod/{tahun}/module-pub-dataset')->name('api-web-mod.dataset.public')->group(function(){
    Route::prefix('data/{id_dataset}')->name('.data')->group(function(){
        Route::post('series-pemda/{kodepemda}','QueryController@getSeriesPemdaPublic')->name('.series.pemda');
        Route::post('series','QueryController@getSeriesPublic')->name('.series');
        Route::post('table-pemda/{kodepemda?}','QueryController@getTablePemdaPublic')->name('.table');
        Route::post('state-data','QueryController@getStatePemdaPublic')->name('.state');
    });
});



Route::prefix('mod/{tahun}/module-dataset')->name('api-web-mod.dataset')->middleware('auth:api')->group(function(){

    Route::post('get-subcribe','MasterController@getDtasetSub')->name('.get-subcribe');


    Route::post('bind-form/{iddataset}','Form\RouteController@index')->name('.form_bind.api');

    Route::post('update-subcribe','MasterController@updateDtasetSub')->name('.update-subcribe');


    Route::prefix('data/{id_dataset}')->name('.data')->group(function(){
        Route::post('series-column-keterisian/','QueryController@getKeterisianAdmin')->name('.series.column.keterisian');
        
        Route::post('series-pemda/{kodepemda}','QueryController@getSeriesPemda')->name('.series.pemda');
        Route::post('series-pemda-penilaian/{kodepemda}','QueryController@getSeriesPemdaPenilaian')->name('.series.pemda.penilaian');
        Route::post('series','QueryController@getSeries')->name('.series');
        Route::post('state-data','QueryController@getStatePemda')->name('.state');
        Route::post('table-pemda/{kodepemda}','QueryController@getTablePemda')->name('.table');
        
    });



    Route::prefix('form-handle')->name('.form-handle')->group(function(){
        Route::post('load-data/{id_dataset}','FromInputController@list_data')->name('.load');
        Route::post('save-dataset/{id_dataset}/{kodepemda}','FromInputController@save')->name('.save');
        Route::post('save-dataset-ddub-komitmen/{id_dataset}/{kodepemda}','FromInputController@saveKomitmen')->name('.save_ddub_komitmen');
        Route::post('get-dataset-ddub-komitmen/{id_dataset}/{kodepemda}','FromInputController@getKomitmen')->name('.get_ddub_komitmen');


        Route::post('verifikasi-data/{id_dataset}/{kodepemda}','FromInputController@verified')->name('.verified');
        Route::post('validasi-data/{id_dataset}/{kodepemda}','FromInputController@validate')->name('.validate');
        Route::post('rollback-data/{id_dataset}/{kodepemda}','FromInputController@rollback')->name('.rollback');

        Route::post('load-exist-data/{id_dataset}/{kodepemda}','FromInputController@load_exist_data')->name('.get-exist');
        Route::post('load-exist-data-bu/{id_dataset}/{kodepemda}','FromInputController@load_exist_bu_data')->name('.get-exist-bu');
        Route::post('load-columns/{id_dataset}','FromInputController@list_columns')->name('.load_columns');

        Route::prefix('muty')->name('.multy')->group(function(){
             Route::post('load-bu-list/{id_dataset}/{kodepemda}/','FromInputMultyController@loadBu')->name('.get-bu');
             Route::get('load-bu-exist-data/{id_dataset}/{kodepemda}/{kodebu}','FromInputMultyController@loadData')->name('.get-exist-data');
             Route::get('load-pemda-exist-data/{id_dataset}/{kodepemda}','FromInputMultyController@loadDataPemda')->name('.get-exist-data-pemda');
        });

        Route::post('/upload-file/{id_dataset}/{kodepemda}', 'FromInputController@upload_file')->name('.up_file');


    });

   
});