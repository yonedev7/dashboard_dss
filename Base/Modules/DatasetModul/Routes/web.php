<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/print-canvas',function(){
    return view('datasetmodul::layouts.canvas');
})->middleware('auth:web');

Route::prefix('dash/{tahun}/sistem-informasi/module-dataset')->name('mod.dataset')->middleware(['auth:web','bindTahun'])->group(function() {
   
    Route::prefix('build-env')->group(function(){
            Route::get('/','BuildPDFAdminController@index')->name('.index.s');
    });

    Route::prefix('build-excel/{id_dataset}')->group(function(){
        Route::get('/get/{kodepemda?}','FormExcelController@getExcel')->name('.download.excel');
});

    

   
    Route::get('/', 'DatasetModulController@index')->name('.index');
    Route::get('/print/{id_dataset}/{kodepemda?}', 'DatasetModulController@print')->name('.print');

    
    Route::get('/detail/{id_dataset}/{tw}/detail/{kodepemda?}', 'DatasetModulController@detail')->name('.detail');
    Route::put('/detail/{id_dataset}/{tw}/update', 'DatasetModulController@update')->name('.update');

    Route::prefix('dataset-list')->name('.dataset')->group(function($requ){
        Route::get('/','MasterController@list_access')->name('.index');
        Route::get('/type-doc',function($tahun,Request $request){
            $class= new \Modules\DatasetModul\Http\Controllers\MasterController(1);

            return $class->list_access($tahun,new \Illuminate\Http\Request());
        })->name('.index_doc');

    });


    Route::prefix('schema')->name('.schema')->group(function(){
        Route::get('/', 'MasterController@index')->name('.index');
        Route::get('/create', 'MasterController@create')->name('.create');
        Route::post('/store', 'MasterController@store')->name('.store');
        Route::post('/update/{id}', 'MasterController@update')->name('.update');
        Route::get('/show/{id}', 'MasterController@show')->name('.show');
        Route::post('/delete/{id}', 'MasterController@delete')->name('.delete');

    });




    Route::prefix('schema-data')->name('.schema-data')->group(function(){
        Route::get('/', 'MasterDataController@index')->name('.index');
        Route::get('/create', 'MasterDataController@create')->name('.create');
        Route::post('/store', 'MasterDataController@store')->name('.store');
        Route::post('/update/{id}', 'MasterDataController@update')->name('.update');
        Route::get('/show/{id}', 'MasterDataController@show')->name('.show');
        Route::post('/delete/{id}', 'MasterDataController@delete')->name('.delete');
    });

    Route::prefix('form')->name('.form')->group(function(){
        Route::get('/{id_dataset}/list', 'FromInputController@index')->name('.listing');
        Route::get('/{id_dataset}/detail/{kodepemda}', 'FromInputController@detail')->name('.detail');
        Route::get('/{id_dataset}/detail-pusat', 'FromInputController@detail')->name('.detail');
        Route::get('/{id_dataset}/detail-multy/{kodepemda}', 'FromInputMultyController@detail')->name('.detail_multy');

    });



});
