<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('master.desk_dataset', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_dataset')->unsined();
            $table->bigInteger('id_desk')->unsined();
            $table->timestamps();
            $table->foreign('id_dataset')
            ->references('id')
            ->on('master.dataset')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('id_desk')
            ->references('id')
            ->on('master.desk')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master.desk_dataset');

        //
    }
};
