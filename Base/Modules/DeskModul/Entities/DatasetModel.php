<?php

namespace Modules\DeskModul\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class DatasetModel extends Model
{
    use HasFactory;

    protected $table='master.dataset';

    protected $fillable = [
        'id',
        'name',
        'jenis_dataset',
        'keterangan',
        'klasifikasi_1',
        'klasifikasi_2',
        'pusat'
    ];
    
    protected static function newFactory()
    {
        return \Modules\DeskModul\Database\factories\DatasetModelFactory::new();
    }
}
