<?php

namespace Modules\DeskModul\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\DeskModul\Entities\DatasetModel;

class DeskDatasetModel extends Model
{
    use HasFactory;

    protected $table='master.desk_dataset';

    protected $fillable = [
        'id_desk',
        'id_dataset',
        'tahun'
    ];
    
    protected static function newFactory()
    {
        return \Modules\DeskModul\Database\factories\DeskDatasetModelFactory::new();
    }


    public function detail(){
        return $this->hasOne(DatasetModel::class,'id','id_dataset');
    }

}
