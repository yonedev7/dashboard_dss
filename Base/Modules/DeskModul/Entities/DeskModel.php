<?php

namespace Modules\DeskModul\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\DeskModul\Entities\DeskDatasetModel;
use Carbon\CarbonPeriod;
use Carbon\Carbon;
class DeskModel extends Model
{
    use HasFactory;

    protected $table='master.desk';
    protected $with=['dataset.detail'];
    protected $fillable = [
        'id',
        'name',
        'keterangan',
        'count_desk',
        'is_active',
        'tanggal_mulai',
        'tanggal_selesai'
    ];
    
    protected static function newFactory()
    {
        return \Modules\DeskModul\Database\factories\DeskModelFactory::new();
    }

    public function dataset(){
        return $this->hasMany(DeskDatasetModel::class,'id_desk','id');
    }

    public function is_running(){
        $arry=CarbonPeriod::create($this->tanggal_mulai??Carbon::now()->addDays(3),$this->tanggal_selesai??Carbon::now()->addDays(3))->toArray();
        $include= (array_filter($arry,function ($el){
            return $el->isSameDay(Carbon::now());
        }));

        return count($include)>0;
    }
}
