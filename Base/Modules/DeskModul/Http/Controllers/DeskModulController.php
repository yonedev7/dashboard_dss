<?php

namespace Modules\DeskModul\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use Modules\DeskModul\Entities\DeskModel;
use Carbon\CarbonPeriod;
class DeskModulController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

    public function dashboard($tahun,$id_desk,$number_desk,Request $request){
        $desk=DeskModel::whereHas('dataset')->find($id_desk);
        if($desk){
            if($desk->is_running()){
                config(['adminlte.page_type'=>'desk_dash']);
                return view("deskmodul::dashboard")->with(['desk'=>$desk,'number_desk'=>$number_desk,'fingerprint'=>$request->fingerprint()]);
            }else{
                return 'desk tidak active';
            }
        }else{
            return 'desk tidak tersedia';
        }
    }

    public function control($tahun,$id_desk,$number_desk,Request $request){
        $desk=DeskModel::whereHas('dataset')->find($id_desk);
        if($desk){
            if($desk->is_running()){
                config(['adminlte.page_type'=>'desk_dash']);
                return view("deskmodul::control_desk")->with(['desk'=>$desk,'number_desk'=>$number_desk,'fingerprint'=>$request->fingerprint()]);
            }else{
                return 'desk tidak active';
            }
        }else{
            return 'desk tidak tersedia';
        }

    }

    public function monitor($tahun,$id_desk,$number_desk,Request $request){
        $desk=DeskModel::whereHas('dataset')->find($id_desk);
        if($desk){
            if($desk->is_running()){
                config(['adminlte.page_type'=>'desk_dash']);
                return view("deskmodul::monitor")->with(['desk'=>$desk,'number_desk'=>$number_desk,'fingerprint'=>$request->fingerprint()]);
            }else{
                return 'desk tidak active';
            }
        }else{
            return 'desk tidak tersedia';
        }
    }
    public function index($tahun)
    {

        
        $desk = DeskModel::whereHas('dataset')->where([
            ['is_active','=',true],
        ])->orderBy('tanggal_mulai','asc')->get();
        foreach($desk as $k=>$d){
           $desk[$k]->is_running=$d->is_running();
        }

        return view('deskmodul::index')->with(['desks'=>$desk]);
    }

   
}
