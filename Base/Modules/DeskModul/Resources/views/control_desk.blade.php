@extends('adminlte::page')

@section('content')

<div class="flex-column d-flex f-h" id="app">
    <div class="p-2 text-center">
        <img src="{{asset('/assets/img/brand/logo.png')}}" height="30" alt="">
        <p class="mb-0"><span class="text-primary">CONTROL</span> {{$desk->name}} (DESK {{$number_desk}})</p>
        <small>@{{time_is}}</small>
    </div>
    <p class="bg-dark px-2">ID : {{$fingerprint}}</p>
    <div class="p-3">
        <div class="d-flex">
            <div class="btn-group mb-2">
                <button v-if="support.webrtc" @click="requestShareScreen" class="btn btn-primary"><i class="fa fa-desktop"></i> Share Screen</button>
            </div> 
        </div>
        <div class="flex-column flex-grow-1">
            <div class="flex-grow-1">
                <video ref="video_share_com" class="video-share"></video>
            </div>
        </div>
    </div>

</div>
@stop


@section('js')
<script src="{{asset('js/dss_sock.js?v='.date('is'))}}"></script>
<script>
    location.hash='{{$fingerprint}}';
    var namespace='-on-desk';
    var socket=new io('{{env('SOCKET_HOST')}}/on-desk',{
        auth:{
            sockid:'user:{{Auth::User()->id}}',
            channel:'#on-desk:{{$desk->id}}:{{$number_desk}}',
            meta:<?=json_encode(Auth::User())?>  
        },
        withCredentials: false,
    });

    socket.on('connect',function(){
       app.support.io=true;

    });

    socket.on('offer-broadcast-desk',function(message){
        message=JSON.parse(message);
        app.updateSignal((message));
    });

  

    var app=new Vue({
        el:'#app',
        created:function(){
            if (Peer.WEBRTC_SUPPORT) {
                // webrtc support!nso
            this.support.webrtc=true;
            } else {
            // fallback
            this.support.webrtc=false;

            console.log('browser not suport');

            }

            setInterval(function(ref){
            var str = "";

            var days = new Array("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Saptu");
            var months = new Array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

            var now = new Date();
            str += days[now.getDay()] + ", " + now.getDate() + " " + months[now.getMonth()] + " " + now.getFullYear() + " " + now.getHours() +":" + now.getMinutes() + ":" + now.getSeconds();

            ref.time_is=str;

          },1000,this)

        },
        data:{
            myfinger:'{{$fingerprint}}',
            time_is:'',
            support:{
                webrtc:false,
                io:false,
            },

            settings:{
                share_screen:{
                    peer:null,
                    srcObject:null,
                    enabled:false,
                    options:{
                        video: {
                            mediaSource: "screen",
                            width: { max: '800' },
                            height: { max: '500' },
                            frameRate: { max: '10' }
                        },
                        audio: false
                    }
                }
            }
        },
        methods:{
            requestShareScreen: async function(){
              
              this.buildVideoSS();

            },
            buildVideoSS:async function(){
                var self=this;
                this.createPeer();

                self.settings.share_screen.srcObject= await navigator.mediaDevices.getDisplayMedia(self.settings.share_screen.options).then(async function(stream){
                    return stream;
                }).catch((e)=>{
                    console.log(e);

                    alert(e);
                });

                this.settings.share_screen.srcObject.getTracks().forEach(track => {
                        console.log(track);
                       self.settings.share_screen.peer.addTrack(track,  self.settings.share_screen.srcObject);
                });
            },
            createPeer:async function(){
                var self=this;
                this.settings.share_screen.peer = new RTCPeerConnection({
                    initiator:true,
                    iceServers: [
                        {
                            urls: "stun:stun.stunprotocol.org"
                        }
                    ]
                });
                this.settings.share_screen.peer.onnegotiationneeded = function(){
                    console.log('per');
                    return self.handleNegotiationNeededEvent(self.settings.share_screen.peer)
                }


                return this.settings.share_screen.peer;
            },
            handleNegotiationNeededEvent: async function (peer) {
                var self=this;
                const offer = await self.settings.share_screen.peer.createOffer();
                await self.settings.share_screen.peer.setLocalDescription(offer);
                const payload = {
                    meta:<?=json_encode(Auth::User())?>,
                    sdp: self.settings.share_screen.peer.localDescription,
                    channel:this.myfinger
                };
                socket.emit('initiator-sharing',JSON.stringify(payload),function(data){
                        data=JSON.parse(data);
                        const desc = new RTCSessionDescription(data.sdp);
                        console.log(desc);
                        self.settings.share_screen.peer.setRemoteDescription(desc).catch(e => console.log(e));
                        
                });
            }

           
          
        },
        watch:{
            "settings.share_screen.srcObject":function(val,old){
                this.$refs.video_share_com.srcObject=val;
                this.$refs.video_share_com.play();
            }
        }
    });

    socket.on('disconnect', function() {
       app.support.io=false;
       setTimeout((network) => {
           console.log('re connect');
           network.connect();
       },500, socket);
   });
</script>

@stop