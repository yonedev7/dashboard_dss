@extends('adminlte::page')


@section('content')
<div class="p-2">
    <div class="pt-2 text-center">
        <img class="d-block mx-auto mb-1" src="{{asset('assets/img/brand/logo.png')}}" alt="" width="auto" height="50">
        <h2>{{$desk->name}} (DESK {{$number_desk}})</h2>
      </div>
    </div> 
</div>

@stop