@extends('adminlte::page')

@section('content')
<div id="app" class="p-3">
    <template v-for="item in desk">
        <div class="mb-2">
            <h4 class="text-uppercase mb-0"><b>Desk @{{item.name}}</b> </h4>
            <p class="m-0"><span class="badge bg-warning" style="font-size:12px;">@{{item.tanggal_mulai}} - @{{item.tanggal_selesai}}</span></p>
        </div>
    <div class="row">
            <div class="col-4" v-for="i in item.count_desk">
                <div class="card">
                    <div v-bind:class="'card-header '+(item.is_running?' bg-success':'')" >
                        <h4>Desk @{{i}}</h4>
                    </div>
                    <div class="card-body">
                        <ul class="list-group">
                            <label for="">List Data</label>
                            <template v-for="x in item.dataset">
                                <li class="list-group-item text-uppercase">
                                    @{{x.detail.name}} Tahun @{{x.tahun}}
                                </li>
                            </template>
                        </ul>
                    </div>
                    <div class="card-footer">
                        <div class="btn-group">
                            <a @click="openDahbosrd(item,i)" class="btn btn-sm btn-primary"> <i class="fa fa-dashboard"></i> Dashboard</a>
                            <a @click="openMonitor(item,i)" class="btn btn-sm btn-success"> <i class="fa fa-desktop"></i> Monitor</a>
                            <button class="btn btn-sm btn-warning"><i class="fa fa-arrow-right"></i> Detail</button>
                            <a @click="openControl(item,i)" class="btn btn-dark btn-sm"> <i class="fa fa-cogs"></i> Tools</a>


                        </div>
                    </div>
                </div>
            </div>
    </div>
    <hr>
</template>

</div>


@stop

@section('js')

<script>

    var app=new Vue({
        el:'#app',
        data:{
            desk:<?=json_encode($desks)?>
        },
        methods:{
            openMonitor:function(item,i){
                window.open(('{{route('mod.desk.monitor',['tahun'=>$StahunRoute,'id_desk'=>'@?','number_desk'=>'@?'])}}').yoneReplaceParam('@?',[item.id,i]),'monitor-desk');

            },
            openControl:function(item,i){
                window.open(('{{route('mod.desk.control',['tahun'=>$StahunRoute,'id_desk'=>'@?','number_desk'=>'@?'])}}').yoneReplaceParam('@?',[item.id,i]),'desk-control');
            },
            openDahbosrd:function(item,i){
                window.open(('{{route('mod.desk.dashboard',['tahun'=>$StahunRoute,'id_desk'=>'@?','number_desk'=>'@?'])}}').yoneReplaceParam('@?',[item.id,i]),'desk-dashboard');

            }
        }
    })
</script>
@stop