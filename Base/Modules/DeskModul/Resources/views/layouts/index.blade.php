@extends('adminlte::page')

@section('content')
<div id="app" class="p-3">
    <div class="d-flex">
        <div class="flex-column" style="width:30%;">
            <h4>User On Desk <small v-if="voice.send.state" class="text-success">@{{voice.send.state}}</small></h4>

    
            <ul class="list-group">
        
                <li  class="list-group-item d-flex justify-content-between align-items-center" v-for="user,i in user_online">
                    <span class="text-uppercase">@{{user.meta.name}}</span>
                  <span>
                    <span class="mr-1" v-if="(voice.send.button_hold && voice.send.sock==user.sock)">@{{voice.send.interval}}s</span>
                    <button  :class="'btn btn-sm  rounded-circle '+((voice.send.button_hold && voice.send.sock==user.sock)?'btn-danger':'btn-primary')" @mousedown="holdBtnVoice(user.sock)" @mouseup="releaseBtnVoice()"><i class="fa fa-microphone"></i></button>
                  </span>
                </li>
            </ul>
        </div>
        <div>
            <p>This example shows you the contents of the selected part of your display.
                Click the Start Capture button to begin.</p>
                
                <p><button id="start">Start Capture</button>&nbsp;<button id="stop">Stop Capture</button></p>
                
                <video id="video" autoplay></video>
                <br>
                
                <strong>Log:</strong>
                <br>
                <pre id="log"></pre>
            

        </div>
    </div>
</div>
<style>
    #video {
  border: 1px solid #999;
  width: 98%;
  max-width: 860px;
}

.error {
  color: red;
}

.warn {
  color: orange;
}

.info {
  color: darkgreen;
}
</style>
    
@endsection

@section('js')
<script>

function initCapture(){
     videoElem = document.getElementById("video");
logElem = document.getElementById("log");
 startElem = document.getElementById("start");
 stopElem = document.getElementById("stop");

startElem.addEventListener("click", function(evt) {
  startCapture();
  console.log('start');
}, false);

stopElem.addEventListener("click", function(evt) {
  stopCapture();
}, false);
}

setTimeout(function(){
    initCapture()
},1000);

// Options for getDisplayMedia()

var displayMediaOptions = {
  video: {
    cursor: "always"
  },
  audio: false
};
async function startCapture() {
  logElem.innerHTML = "";

  try {
    videoElem.srcObject = await navigator.mediaDevices.getDisplayMedia(displayMediaOptions);
    dumpOptionsInfo();
  } catch(err) {
    console.error("Error: " + err);
  }
}

function stopCapture(evt) {
  let tracks = videoElem.srcObject.getTracks();

  tracks.forEach(track => track.stop());
  videoElem.srcObject = null;
}
function dumpOptionsInfo() {
  const videoTrack = videoElem.srcObject.getVideoTracks()[0];

  console.info("Track settings:");
  console.info(JSON.stringify(videoTrack.getSettings(), null, 2));
  console.info("Track constraints:");
  console.info(JSON.stringify(videoTrack.getConstraints(), null, 2));
}

// Set event listeners for the start and stop buttons

</script>
<script>
    var socket=null;
    function initConnection(){
        if(socket){
            socket.disconnect();
        }

        socket=new io('{{env('SOCKET_HOST')}}',{
            auth:{
                sockid:'user:{{Auth::User()->id}}',
                channel:'#desk_glob',
                meta:<?=json_encode(Auth::User())?>  
            },
            withCredentials: false,
            extraHeaders: {
                "coo_set": document.cookie
            }
        });

        socket.on("connect", () => {
            const engine = socket.io.engine;
            console.log(engine.transport.name); // in most cases, prints "polling"

            engine.once("upgrade", () => {
                // called when the transport is upgraded (i.e. from HTTP long-polling to WebSocket)
                console.log(engine.transport.name); // in most cases, prints "websocket"
            });

            engine.on("packet", ({ type, data }) => {
                // called for each packet received
            });

            engine.on("packetCreate", ({ type, data }) => {
                // called for each packet sent
            });

            engine.on("drain", () => {
                // called when the write buffer is drained
            });

            engine.on("close", (reason) => {
                // called when the underlying connection is closed
            });
        });

        socket.on('user-list',function(message){
            app.user_online=message;
        });

        socket.on('receive-voicemail',function(message){
            console.log(message,'messss');
            
            app.voice.receive.data.push(message);
            if(app.voice.receive.data.length==1){
                app.paySoundTrack();
            }

        });

        socket.on('disconnect', function() {
            setTimeout(() => {
                
            }, timeout);
            setTimeout((network) => {
                console.log('re connect');
                network.connect();
            },500, socket);
        })
    
    }

    navigator.mediaDevices.getUserMedia({ audio: true })
    .then(stream => {
    });

    $(window).bind('beforeunload', function(){
        return "Do you want to exit this page?";
    });

    var app= new Vue({
        el:'#app',
        data:{
            user_online:[],
            voice:{
                send:{
                    sock:null,
                    state:'',
                    button_hold:false,
                    interval:30,
                    mediaStream:null,
                    data:[],
                    blob:null,
                    interval_controll:null,

                },
                receive:{
                    sock_play:'',
                    data:[]
                }
                    
                
            }
        },
        created: async function(){
          

        },
        methods:{
            initMediaRecord:async function(){
                var self=this;
                await navigator.mediaDevices.getUserMedia({ audio: true })
                    .then(stream => {
                        self.voice.send.mediaStream=new MediaRecorder(stream);
                        
                });

                self.voice.send.mediaStream.addEventListener("dataavailable", event => {
                    self.voice.send.data.push(event.data);
                });

                self.voice.send.mediaStream.addEventListener("stop", (ev) => {
                   setTimeout(function(t){
                        t.target.stream.getTracks().forEach(track => {
                            track.stop()
                        });
                   },800,ev);
                   
                });

                return true;
            },
            releaseBtnVoice:function(){
                this.voice.send.state='';
                this.voice.send.sock=null;
                this.voice.send.button_hold=false;

                if(this.voice.send.interval_controll!=null){
                    clearInterval(this.voice.send.interval_controll);
                    this.voice.send.interval_controll=null;
                }               

            },
            holdBtnVoice:async function(sock){
                
                if(this.voice.send.interval_controll){
                    clearInterval(this.voice.send.interval_controll);
                    this.voice.send.interval_controll=null;
                }

                console.log(this.voice.send.interval,'hold');
               if(this.voice.send.interval==30){
                await this.initMediaRecord();
                this.voice.send.state='record';
                this.voice.send.data=[];
                this.voice.send.mediaStream.start();

                this.voice.send.sock=sock;
                this.voice.send.button_hold=true;
                this.voice.send.interval_controll=setInterval(function(ref){
                    ref.voice.send.interval-=1;
                    if(ref.voice.send.interval<=0){
                       clearInterval(ref.voice.send.interval_controll);
                       ref.voice.send.interval_controll=null;
                       ref.releaseBtnVoice();
                    }
                },1000,this);

               }else{
                this.voice.send.state='bussy';
               }
                

            },
            sendVoice:function(sock){
                setTimeout(function(ref,con,id){
                    const blob = new Blob(ref.voice.send.data,{ type: "audio/wav" });
                    con.emit('send-voicemail',{
                    to:sock,
                    content:(blob)
                    },function(status){
                        if(status==200){
                            
                        }
                    });
                },200,this,socket,sock);
              
               

               
           
             
               
                
            },
            playSound:function(data){
                self=this;
                const blob = new Blob([data], { type: "audio/wav" });
                var audio=new Audio( window.URL.createObjectURL(blob));
                audio.play();
                audio.addEventListener('ended',function(){
                    self.voice.receive.data.splice(0,1);
                    self.paySoundTrack();
                });


            },
            paySoundTrack:function(){
                if(this.voice.receive.data[0]!=undefined){
                   this.playSound(this.voice.receive.data[0].content);
                }
            }
            
        },
        watch:{
            "voice.receive.data":function(val,old){
                if(old==[]){
                    console.log('ok triger run stack play');
                    this.paySoundTrack();
                }
            },
            "voice.send.sock":function(val,old){
                if(val==null){
                    this.voice.send.mediaStream.stop();
                    console.log('stop');
                    this.sendVoice(old);
                     setTimeout(function(ref){

                        ref.voice.send.interval=30;
                        console.log(ref.voice.send.interval,'reload');

                    },3000,this);
                }
            }
        }
    })




    
    function init(){



        window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
        window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction || {READ_WRITE: "readwrite"}; // This line should only be needed if it is needed to support the object's constants for older browsers
        window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;

        const localDB = indexedDB.open("dss_nuwas_local",1);
        localDB.onerror = event => {
        console.log("Why didn't you allow my web app to use IndexedDB?!");
        };
        localDB.onupgradeneeded = function (e) {
            var database = e.target.result;
            var objectStore = database.createObjectStore('chats',{
                keyPath:'id'
            });
            
        }
           
        localDB.onsuccess = e => {
         
            var database = e.target.result;

           tbchats=database.transaction('chats','readwrite').objectStore('chats');
           tbchats.put({
            id:new Date().getTime()+110,
            m:'dua'

           });

           tbchats=database.transaction('chats','readwrite').objectStore('chats');
           tbchats.put({
            id:new Date().getTime(),
            m:'satu'

           })
                
          

           


        };
       

        

        initConnection();


        setInterval(function(network_sock){
            if(!network_sock.connected){
                initConnection();
            }
        },2000,socket);

      
        }
       
  
    init();

</script>

@stop
