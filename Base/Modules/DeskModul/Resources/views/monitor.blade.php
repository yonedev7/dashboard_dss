@extends('adminlte::page')

@section('content')

<div class="flex-column d-flex f-h" id="app">
    <div class="p-2 text-center">
        <img src="{{asset('/assets/img/brand/logo.png')}}" height="30" alt="">
        <p class="mb-0">{{$desk->name}} (DESK {{$number_desk}})</p>
        <small>@{{time_is}}</small>
    </div>
    <div class="d-flex bg-secondary p-1">
        <div class="nav-desk"></div>
        <div class="flex-grow-1 nav-clien t text-dark">
            <ul class="list-group list-group-horizontal">
                <li  class="list-group-item d-flex justify-content-between align-items-center" v-for="user,i in user_online">
                    <span class="text-uppercase mr-3">@{{user.meta.name}}</span>
                  <span>
                    <small class="mr-1" style="min-width:100px;" >
                        <small  :class="'text-primary '+((user.sock==voice.receive.sock_play)?'visible-true':'visible-hide')"><i class="fa fa-bullhorn"></i></small>
                        {{-- <i  style="min-width:80px;" :class="'text-primary '+((voice.send.button_hold)?'visible-true':'visible-hide')"  class="text-success">@{{voice.send.state}}</i> --}}
                        <span  :class="'text-primary '+((voice.send.button_hold && voice.send.sock==user.sock)?'visible-true':'visible-hide')" >@{{voice.send.interval}}s</span>
                    </small>
                    <button  :class="'btn btn-sm  rounded-circle '+((voice.send.button_hold && voice.send.sock==user.sock)?'btn-danger':'btn-primary')" @mousedown="holdBtnVoice(user.sock)" @mouseup="releaseBtnVoice()"><i class="fa fa-microphone"></i></button>
                  </span>
                </li>
              
              </ul>
        </div>

    </div>
    <div class="mb-2">
        <ul class="list-group list-group-horizontal">
            <li  class="list-group-item d-flex justify-content-between align-items-center" v-for="item,i in share_screen.peer">
                <i class="fa fa-desktop mr-2"></i>
                <span class="text-uppercase mr-3">@{{item.meta.name}} </span>
                <button class="btn btn-primary btn-sm" @click="activeShareScreen(i)"><i class="fa fa-arrow-right"></i></button>
            </li>
        </ul>
        
    </div>

    <div class="flex-grow-1">
        <video id="chast_monitor" ref="share_screen" autoplay></video>
    </div>
</div>
<style>
    .visible-hide{
        visibility: hidden;
    }
    .visible-true:{
        visibility: visible;
    }
    .content-wrapper .full{
        height: 100vh;
    }
    #chast_monitor{
        width:100%;
        position: absolute;
        z-index: 1;
        background: #f1f1f1;
    }
    .f-h{
        height: 100vh;
    }
</style>

@stop

@section('js')
<script src="{{asset('/js/dss_sock.js?v='.date('is'))}}"></script>
@php
@endphp
<script>
    var namespace='-on-desk';
    var socket=new io('{{env('SOCKET_HOST')}}/on-desk',{
        auth:{
            sockid:'user:{{Auth::User()->id}}',
            channel:'#on-desk:{{$desk->id}}:{{$number_desk}}',
            meta:<?=json_encode(Auth::User())?>  
        },
        withCredentials: false,
    });

    socket.on('connect',function(){

    });

    socket.on('user-list'+namespace,function(message){
        message=JSON.parse(message);
        app.user_online=message.filter(el=>{
            return el.sock!='user:{{Auth::User()->id}}';
        });
    });
    socket.on('receive-voicemail'+namespace,function(message){
            
        app.voice.receive.data.push(message);
        if(app.voice.receive.data.length==1){
            app.paySoundTrack();
        }
    });

    socket.on('offer-broadcast-desk',function(message){
        if(app.share_screen.channel_subcribe==message.data.channel){
            app.share_screen.peer_con.signal(JSON.parse(message.data.offer));
        }
    });

    socket.on('initiator-list',function(    message){
        message=JSON.parse(message);
        var l={};
       
        app.share_screen.peer=message;

    
    });

    socket.on('disconnect', function() {
       
        setTimeout((network) => {
            console.log('re connect');
            network.connect();
        },500, socket);
    })

    var app= new Vue({
        el:'#app',
        data:{
            peer:null,
            srcObject:null,
            time_is:'',
            share_screen:{
                options:{

                },
                peer:[],
                srcObject:null,
                channel_subcribe:''
            },
            user_online:[],
            voice:{
                send:{
                    sock:null,
                    state:'',
                    button_hold:false,
                    interval:15,
                    mediaStream:null,
                    data:[],
                    blob:null,
                    interval_controll:null,

                },
                receive:{
                    sock_play:'',
                    data:[]
                }
                    
                
            }
        },
        created: async function(){
          setInterval(function(ref){
            var str = "";

            var days = new Array("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Saptu");
            var months = new Array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

            var now = new Date();
            str += days[now.getDay()] + ", " + now.getDate() + " " + months[now.getMonth()] + " " + now.getFullYear() + " " + now.getHours() +":" + now.getMinutes() + ":" + now.getSeconds();

            ref.time_is=str;

          },1000,this);
         
         
        },
        methods:{
            
            initMediaRecord:async function(){
                var self=this;
                await navigator.mediaDevices.getUserMedia({ audio: true })
                    .then(stream => {
                        self.voice.send.mediaStream=new MediaRecorder(stream);
                        
                });

                self.voice.send.mediaStream.addEventListener("dataavailable", event => {
                    self.voice.send.data.push(event.data);
                });

                self.voice.send.mediaStream.addEventListener("stop", (ev) => {
                   setTimeout(function(t){
                        t.target.stream.getTracks().forEach(track => {
                            track.stop()
                        });
                   },800,ev);
                   
                });

                return true;
            },
            releaseBtnVoice:function(){
                this.voice.send.state='';
                this.voice.send.sock=null;
                this.voice.send.button_hold=false;

                if(this.voice.send.interval_controll!=null){
                    clearInterval(this.voice.send.interval_controll);
                    this.voice.send.interval_controll=null;
                }               

            },
            holdBtnVoice:async function(sock){
                
                if(this.voice.send.interval_controll){
                    clearInterval(this.voice.send.interval_controll);
                    this.voice.send.interval_controll=null;
                }

               if(this.voice.send.interval==15){
                await this.initMediaRecord();
                this.voice.send.state='Record';
                this.voice.send.data=[];
                this.voice.send.mediaStream.start();

                this.voice.send.sock=sock;
                this.voice.send.button_hold=true;
                this.voice.send.interval_controll=setInterval(function(ref){
                    ref.voice.send.interval-=1;
                    if(ref.voice.send.interval<=0){
                       clearInterval(ref.voice.send.interval_controll);
                       ref.voice.send.interval_controll=null;
                       ref.releaseBtnVoice();
                    }
                },1000,this);

               }else{
                this.voice.send.state='Bussy';
               }
                

            },
            sendVoice:function(sock){
                setTimeout(function(ref,con,id){
                    const blob = new Blob(ref.voice.send.data,{ type: "audio/wav" });
                    con.emit('send-voicemail'+namespace,{
                    to:sock,
                    content:(blob)
                    },function(status){
                        if(status==200){
                            
                        }
                    });
                },200,this,socket,sock);
                
            },
            activeShareScreen: async function(i){
                var self=this;
                this.share_screen.channel_subcribe=this.share_screen.peer[i].channel;
                 await this.createPeer(self.share_screen.peer[i].channel);
                 
                self.peer.addTransceiver("video", { direction: "recvonly" });

            },
            createPeer:function(channel){
                var self=this;
                self.peer = new RTCPeerConnection({
                    iceServers: [
                        {
                            urls: "stun:stun.stunprotocol.org"
                        }
                    ]
                });
                self.peer.ontrack = self.handleTrackEvent;
                self.peer.onnegotiationneeded = () => self.handleNegotiationNeededEvent(self.peer,channel);
                return self.peer;
            },
            handleTrackEvent:function(e){
                this.$refs.share_screen.srcObject = e.streams[0];
                this.share_screen.srcObject=e.streams[0];
                console.log('add stream',e.streams[0]);
            },
            handleNegotiationNeededEvent: async function (peer,channel) {
                var self=this;
                const offer = await self.peer.createOffer();
                await self.peer.setLocalDescription(offer);
                const payload = {
                    meta:<?=json_encode(Auth::User())?>,
                    sdp: self.peer.localDescription,
                    channel:channel
                };
               socket.emit('join-video-channel',JSON.stringify(payload),function(data){
                    data=JSON.parse(data);
                    const desc = new RTCSessionDescription(data.sdp);
                    self.peer.setRemoteDescription(desc).catch(e => console.log(e));
               });
            },
            playSound:function(data){
                self=this;
                const blob = new Blob([data], { type: "audio/wav" });
                var audio=new Audio( window.URL.createObjectURL(blob));
                audio.play();
                audio.addEventListener('ended',function(){
                    self.voice.receive.sock_play='';
                    self.voice.receive.data.splice(0,1);
                    if(self.voice.receive.data.length==1){
                        self.voice.receive.data.splice(0,0);
                    }else{
                    self.voice.receive.data.splice(0,1);
                        
                    }
                    self.paySoundTrack();
                });


            },
            paySoundTrack:function(){
                this.voice.receive.sock_play='';
                if(this.voice.receive.data[0]!=undefined){
                    console.log(this.voice.receive.data);
                    this.voice.receive.sock_play=this.voice.receive.data[0].sock_sender;
                   this.playSound(this.voice.receive.data[0].content);
                }
            },
            com_peer:function(){
                var peer= Object.entries(this.share_screen.peer);
                return peer;

            },
            
            
        },
      
        computed:{
            
           
        },
        watch:{
           
            "voice.receive.data":function(val,old){
                if(old==[]){
                    this.paySoundTrack();
                }
            },
            "voice.send.sock":function(val,old){
                if(val==null){
                    this.voice.send.mediaStream.stop();
                    this.sendVoice(old);
                     setTimeout(function(ref){

                        ref.voice.send.interval=15;
                        console.log(ref.voice.send.interval,'reload');

                    },1500,this);
                }
            }
        }
    })
</script>
@stop