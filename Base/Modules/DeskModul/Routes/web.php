<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('dash/{tahun}/sistem-informasi/desk-modul')->name('mod.desk')->middleware('auth:web')->group(function() {
    Route::get('/', 'DeskModulController@index')->name('./index');


});

Route::prefix('session/{tahun}/desk-modul/')->name('mod.desk')->middleware(['auth:web','bindTahun'])->group(function(){
    Route::get('/dashboard/{id_desk}/{number_desk}', 'DeskModulController@dashboard')->name('.dashboard');
    Route::get('/monitor/{id_desk}/{number_desk}', 'DeskModulController@monitor')->name('.monitor');
    Route::get('/control/{id_desk}/{number_desk}', 'DeskModulController@control')->name('.control');
    Route::get('/detail/{id_desk}/{number_desk}', 'DeskModulController@detail')->name('.detail');
});
