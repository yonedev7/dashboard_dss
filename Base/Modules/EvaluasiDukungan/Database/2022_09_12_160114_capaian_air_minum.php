<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('output.capaian_air_minum',function(Blueprint $table){
            $table->id();
            $table->string('kodepemda',5);
            $table->integer('tahun');
            $table->float('jp_bumdam_kk_p')->nullable();
            $table->float('jp_bumdam_kk')->nullable();
            $table->float('jp_bumdam_sr')->nullable();
            $table->float('jp_nonbumdam')->nullable();
            $table->float('jp_p')->nullable();
            $table->float('jp')->nullable();
            $table->float('bjp_sr')->nullable();
            $table->float('bjp_p')->nullable();
            $table->float('air_layak')->nullable();
            $table->float('belum_terlayani_p')->nullable();
            $table->float('belum_terlayani')->nullable();
            $table->unique(['kodepemda','tahun'])->nullable();
            $table->mediumText('keterangan')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
