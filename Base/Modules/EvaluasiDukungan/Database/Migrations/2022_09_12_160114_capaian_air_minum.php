<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('output.capaian_air_minum',function(Blueprint $table){
            $table->id();
            $table->string('kodepemda',5);
            $table->integer('tahun');
            $table->float('kk')->nullable();
            $table->float('bumdam_kk')->nullable();
            $table->float('bumdam_sr')->nullable();
            $table->float('nonbumdam_kk')->nullable();
            $table->float('nonbumdam_sr')->nullable();
            $table->float('bjp_kk')->nullable();
            $table->float('bjp_sr')->nullable();    
            $table->unique(['kodepemda','tahun'])->nullable();
            $table->mediumText('keterangan')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
