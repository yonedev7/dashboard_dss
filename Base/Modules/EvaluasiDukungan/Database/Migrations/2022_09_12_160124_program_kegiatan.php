<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('output.program_kegiatan',function(Blueprint $table){
            $table->id();
            $table->string('kodepemda',5);
            $table->integer('tahun');
            $table->float('rkpd_ar_sub')->nullable();
            $table->float('rkpd_ar_ag')->nullable();

            $table->float('apbd_ar_sub')->nullable();
            $table->float('apbd_ar_ag')->nullable();
            $table->float('apbd_tot')->nullable();
            $table->mediumText('keterangan')->nullable();
            $table->timestamps();
           
            $table->unique(['kodepemda','tahun']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
