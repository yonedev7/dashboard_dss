<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('output.dana_pendamping',function(Blueprint $table){
            $table->id();
            $table->string('kodepemda',5);
            $table->integer('tahun');
            $table->float('rencana_ag')->nullable();
            $table->float('rencana_sr')->nullable();

            $table->float('realisasi_ag')->nullable();
            $table->float('realisasi_sr')->nullable();

            $table->float('deviasi_ag')->nullable();
            $table->float('deviasi_sr')->nullable();

            $table->mediumText('keterangan')->nullable();
            $table->unique(['kodepemda','tahun']);
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
