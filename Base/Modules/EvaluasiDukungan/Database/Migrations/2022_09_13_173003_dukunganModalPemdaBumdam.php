<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('output.dukungan_modal_pemda_bumdam',function(Blueprint $table){
            $table->id();
            $table->string('kodepemda',5);
            $table->integer('tahun');
            $table->bigInteger('kodebu')->nullable();
            $table->float('modal')->nullable();
            $table->float('modal_infra')->nullable();
            $table->float('laba')->nullable();
            $table->mediumText('keterangan')->nullable();
            $table->unique(['kodepemda','tahun','kodebu']);
            $table->timestamps();
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
