<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('output.rediness_criteria',function(Blueprint $table){
            $table->id();
            $table->string('kodepemda',5)->unique();
            $table->boolean('rc5')->nullable();
            $table->mediumText('rc5_lampiran')->default('[]');

            $table->mediumText('rc5_isu')->nullable();
            $table->mediumText('rc5_rktl')->nullable();
            $table->mediumText('rc5_keterangan')->nullable();

            $table->boolean('rc7')->nullable();
            $table->mediumText('rc7_lampiran')->default('[]');

            $table->mediumText('rc7_isu')->nullable();
            $table->mediumText('rc7_rktl')->nullable();
            $table->mediumText('rc7_keterangan')->nullable();

            $table->boolean('rc12')->nullable();
            $table->mediumText('rc12_lampiran')->default('[]');

            $table->mediumText('rc12_isu')->nullable();
            $table->mediumText('rc12_rktl')->nullable();
            $table->mediumText('rc12_keterangan')->nullable();

            $table->boolean('rc14')->nullable();
            $table->mediumText('rc14_lampiran')->default('[]');

            $table->mediumText('rc14_isu')->nullable();
            $table->mediumText('rc14_rktl')->nullable();
            $table->mediumText('rc14_keterangan')->nullable();

            $table->boolean('rc16')->nullable();
            $table->mediumText('rc16_lampiran')->default('[]');

            $table->mediumText('rc16_isu')->nullable();
            $table->mediumText('rc16_rktl')->nullable();
            $table->mediumText('rc16_keterangan')->nullable();

            $table->boolean('rc22')->nullable();
            $table->mediumText('rc22_lampiran')->default('[]');

            $table->mediumText('rc22_isu')->nullable();
            $table->mediumText('rc22_rktl')->nullable();
            $table->mediumText('rc22_keterangan')->nullable();
            $table->timestamps();
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
