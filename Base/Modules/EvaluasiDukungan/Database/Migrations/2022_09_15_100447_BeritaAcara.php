<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('output.berita_acara_evaluasi',function(Blueprint $table){
            $table->id();
            $table->string('kodepemda',5);
            $table->integer('tahun');
            $table->integer('status')->default(0);
            $table->string('path')->nullable();
            $table->string('nama')->nullable();
            $table->string('nip')->nullable();
            $table->string('ttd_path')->nullable();
            $table->unique(['kodepemda','tahun']);
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
