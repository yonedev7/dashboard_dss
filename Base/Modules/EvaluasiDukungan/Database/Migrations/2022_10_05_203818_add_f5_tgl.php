<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddF5Tgl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('output.rediness_criteria', function (Blueprint $table) {
            $table->date('date_target')->nullable();
            $table->date('date_finish')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('output.rediness_criteria', function (Blueprint $table) {
            // $table->date('date_target')->nullable();
            // $table->date('date_finish')->nullable();

        });
    }
}
