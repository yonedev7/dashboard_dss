<?php

namespace Modules\EvaluasiDukungan\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use Carbon\Carbon;
class BUMDAMFCRController extends Controller
{
    public function index($kodepemda,Request $request)
    {
        $tahun=$request->meta['tahun']??0;

       return [
        'code'=>200,
        'data'=>DB::table('output.dukungan_pemda_bumdam')->where([
            'kodepemda'=>$request->kodepemda,
            'kodebu'=>$request->meta['kodebu'],
            
        ])->where('tahun','<=',$tahun+1)
        ->orderBy('tahun','desc')->get()
        ];
    }

    public function store($kodepemda,Request $request)
    {
        $date=Carbon::now();
        $ids=[];
        $tahun=$request->meta['tahun']??0;
        foreach($request->data??[] as $d){
            $d=(Array)$d;
            $x=DB::table('output.dukungan_pemda_bumdam')->where([
                'kodepemda'=>$kodepemda,
                'tahun'=>$d['tahun'],
                'kodebu'=>$d['kodebu']
            ])->first();

            if($x){
                $ids[]=$x->id;
                DB::table('output.dukungan_pemda_bumdam')->where('id',$x->id)->update([
                    'updated_at'=>$date,
                    "fcr"=>$d['fcr']??false,
                    "nilai_fcr"=>$d['nilai_fcr']??0,
                    "subsidi"=>$d['subsidi']??false,
                    "nilai_subsidi"=>$d['nilai_subsidi'],
                    "keterangan"=>$d['keterangan'],
                ]);

            }else{
                $id=(DB::table('output.dukungan_pemda_bumdam')->insertGetId([
                    'kodepemda'=>$kodepemda,
                    'tahun'=>$d['tahun'],
                    'kodebu'=>$d['kodebu'],
                    "fcr"=>$d['fcr']??false,
                    "nilai_fcr"=>$d['nilai_fcr']??0,
                    "subsidi"=>$d['subsidi']??false,
                    "nilai_subsidi"=>$d['nilai_subsidi'],
                    "keterangan"=>$d['keterangan'],
                    'created_at'=>$date,
                    'updated_at'=>$date,
                ]));
                $ids[]=$id;
            }

           

        }

        if(count($ids)==0){
            DB::table('output.dukungan_pemda_bumdam')
            ->where('kodepemda',$kodepemda)
            ->where('tahun','<=',$tahun)

            ->delete();
        }else{
            DB::table('output.dukungan_pemda_bumdam')
            ->where('tahun','<=',$tahun)
            ->where('kodepemda',$kodepemda)
            ->whereNotIn('id',$ids)->delete();
        }

        return [
            'code'=>200,
            'data'=>'Data Berhasil Diperbarui'
        ];
    }
}
