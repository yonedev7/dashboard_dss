<?php

namespace Modules\EvaluasiDukungan\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use Carbon\Carbon;
class CapaianAirMinumController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index($kodepemda,Request $request)
    {

        $tahun=$request->meta['tahun']??0;


      if(strlen($kodepemda)>3 OR ($request->self_data && strlen($kodepemda)<=3)){

       if($request->agre){
         return [
            'code'=>200,
            'data'=>DB::table('output.capaian_air_minum')->where(
                'kodepemda','ilike',DB::raw("'".$kodepemda."%||".$kodepemda."'"),
            )->where('tahun','<=',$tahun+1)->orderBy('tahun','desc')
            ->get()
        ];
       }else{
        return [
            'code'=>200,
            'data'=>DB::table('output.capaian_air_minum')->where(
                'kodepemda','=',$kodepemda.'',
            )->where('tahun','<=',$tahun+1)->orderBy('tahun','desc')
            ->get()
        ];
       }

      }else{
    
        $pemdas=DB::table('master_pemda')
        ->where(
            [
                ['kodepemda','ilike',''.$kodepemda.'%'],
            ]
        )
        ->orWhere('kodepemda','=',$kodepemda)
        ->orderBy('kodepemda','asc')->get();
        
        $data=[];

        foreach($pemdas as $p){
            $d=[
                'meta'=>[
                    'nama_pemda'=>$p->nama_pemda,
                    'kodepemda'=>$p->kodepemda,
                ],
                'data'=>[]
            ];

            $data[]=$d;
        }


        return [
            'code'=>200,
            'data'=>$data,
            'mode'=>'Provinsi'
        ];
      }
    }

    public function store($kodepemda,Request $request)
    {
        $date=Carbon::now();
        $ids=[];

        $tahun=$request->meta['tahun']??0;


        foreach($request->data??[] as $d){
            $d=(Array)$d;
            $x=DB::table('output.capaian_air_minum')->where([
                'kodepemda'=>$kodepemda,
                'tahun'=>$d['tahun']
            ])->first();

            if($x){
                $ids[]=$x->id;
                DB::table('output.capaian_air_minum')->where('id',$x->id)->update([
                    'updated_at'=>$date,
                    "kk"=>$d['kk']??0,
                    "bumdam_kk"=>$d['bumdam_kk']??0,
                    "bumdam_sr"=>$d['bumdam_sr']??0,
                    "nonbumdam_kk"=>$d['nonbumdam_kk']??0,
                    "nonbumdam_sr"=>$d['nonbumdam_sr']??0,
                    "bjp_kk"=>$d['bjp_kk']??0,
                    "keterangan"=>$d['keterangan']??null,

                ]);

            }else{
                $id=(DB::table('output.capaian_air_minum')->insertGetId([
                    'kodepemda'=>$kodepemda,
                    'tahun'=>$d['tahun'],
                    "bumdam_kk"=>$d['bumdam_kk']??0,
                    "kk"=>$d['kk']??0,
                    "bumdam_sr"=>$d['bumdam_sr']??0,
                    "nonbumdam_kk"=>$d['nonbumdam_kk']??0,
                    "nonbumdam_sr"=>$d['nonbumdam_sr']??0,
                    "bjp_kk"=>$d['bjp_kk']??0,
                    "keterangan"=>$d['keterangan']??null,
                    'created_at'=>$date,
                    'updated_at'=>$date,
                ]));
                $ids[]=$id;
            }

           

        }

        if(count($ids)==0){
            DB::table('output.capaian_air_minum')
            ->where('kodepemda',$kodepemda)
            ->where('tahun','<=',$tahun)
            ->delete();
        }else{
            DB::table('output.capaian_air_minum')
            ->where('tahun','<=',$tahun)
            ->where('kodepemda',$kodepemda)
            ->whereNotIn('id',$ids)->delete();
        }

        return [
            'code'=>200,
            'data'=>'Data Berhasil Diperbarui'
        ];
    }


    public function getChart($tahun,$kodepemda){
       if(strlen($kodepemda)<=3){
        $data=DB::table('output.capaian_air_minum')->where([
            ['kodepemda','ilike',DB::raw("'".$kodepemda."%'")],
            ['tahun','<=',$tahun]
        ])
        ->orwhere([
            ['kodepemda','=',$kodepemda],
            ['tahun','<=',$tahun]
        ])->orderBy('tahun','asc')
        ->groupBy('tahun')
        ->selectRaw("
            sum(bumdam_kk) as bumdam_kk,
            sum(nonbumdam_kk) as nonbumdam_kk,
            sum(bjp_kk) as bjp_kk,
            sum(kk) as kk,
            tahun
        ")
        ->get();
       }else{
        $data=DB::table('output.capaian_air_minum')->where('kodepemda',$kodepemda)
        ->where('tahun','<=',$tahun)->orderBy('tahun','asc')->get();
       }
        $target=DB::table('output.target_rpjmn')
        ->where('kodepemda','=',substr($kodepemda,0,3))->first();
       

        $quadrant=[];
        if($target){
            if(strlen($kodepemda)<=3){
            
            }else{
                $pemda_x=DB::table('master.master_pemda')->where('kodepemda','ilike',substr($kodepemda,0,3).'%')->count();
                $target->target_jp= $target->target_jp/$pemda_x;
                $target->target_layak=$target->target_layak/$pemda_x;
                $target->target_bjp=$target->target_bjp/$pemda_x;
            }


            $quadrant=static::quadrant($target->target_jp,$target->target_layak);
        }
        $series_data=[
            'name'=>'Capaian Air Minum',
            "marker"=> [
                "symbol"=> 'circle'
            ],
              "color"=>'orange',
            'data'=>[]
        ];
        foreach($data as $d){
            $series_data['data'][]=[
                'name'=>$d->tahun,
                'color'=>'orange',
                'x'=>$this->percentage((int)$d->bumdam_kk+(int)$d->nonbumdam_kk,(int)$d->kk),
                'y'=>$this->percentage((int)$d->bumdam_kk+(int)$d->nonbumdam_kk+(int)$d->bjp_kk,(int)$d->kk),
            ];            

        }

        $quadrant[]=$series_data;
        return [
            'code'=>200,
            'data'=>$quadrant,
            'target'=>$target,
        ];


    }

    public function percentage($val,$pembagi){
        if($val){
            if($pembagi){
                return ($val/$pembagi)*100;
            }
        }

        return 0;
    }

    public static function quadrant($x,$y){
        $quadrant=[
            [
                'name'=>'Q1',
                'color'=>'rgba(198, 248, 70, 0.08)',
                'polygon'=> [
                    [0, 0],
                    [-100, 0],
                    [-100, 100],
                    [0, 100],
                    [0, 0],
                ],
            ],
            [
                'name'=>'Q2',
                'color'=>'rgba(70, 94, 248, 0.08)',
                'polygon'=> [
                    [0, 0],
                    [100, 0],
                    [100, 100],
                    [0, 100],
                    [0, 0],
                ],
            ],
            [
                'name'=>'Q3',
                'color'=>'rgba(248, 153, 70, 0.08)',
                'polygon'=> [
                    [0, 0],
                    [-100, 0],
                    [-100, -100],
                    [0, -100],
                    [0, 0],
                ],
            ],
            [
                'name'=>'Q4',
                'color'=>'rgba(248, 235, 70, 0.08)',
                'polygon'=> [
                    [0, 0],
                    [0, -100],
                    [100, -100],
                    [100, 0],
                    [0, 0],
                ],
            ]
            
        ];
        $quadrant_result=[];
        foreach($quadrant as $q){
            $poligon=[];
            foreach($q['polygon'] as $p){
                $xx=$x-$p[0];
                if($xx<-100){
                    $xx=-100;
                }
                if($xx>150){
                    $xx=150;
                }
                $yy=$y-$p[1];
                if($yy<-100){
                    $yy=-100;
                }
                if($yy>150){
                    $yy=150;
                }

                $poligon[]=[$xx,$yy];
            }
            $quadrant_result[]=[
                "name"=> $q['name'],
                "type"=> 'polygon',
                "showInLegend"=> false,
                "data"=>$poligon,
                "color"=>$q['color'],
                "enableMouseTracking"=> false,
            ];
        }

        return $quadrant_result;
    }

   
    
}
