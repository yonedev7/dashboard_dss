<?php

namespace Modules\EvaluasiDukungan\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use Carbon\Carbon;
class DDUBController extends Controller
{
    public function index($kodepemda,Request $request)
    {
        $tahun=$request->meta['tahun']??0;
        
        if($request->meta['tipe_bantuan']=='STIMULAN'){
            return [
                'code'=>200,
                'data'=>DB::table('output.ddub')->where([
                    'kodepemda'=>$request->kodepemda,
                ])->where('tahun','<=',$tahun+1)
                ->orderBy('tahun','desc')->get()
                ];
        }else if($request->meta['tipe_bantuan']=='PENDAMPING'){
            return [
                'code'=>200,
                'data'=>DB::table('output.dana_pendamping')->where([
                    'kodepemda'=>$request->kodepemda,
                ])->where('tahun','<=',$tahun+1)->orderBy('tahun','desc')->get()
                ];

        }
    }

    public function store($kodepemda,Request $request)
    {
        $tahun=$request->meta['tahun']??0;

        $date=Carbon::now();
        if($request->meta['tipe_bantuan']=='STIMULAN'){
            $ids=[];

            foreach($request->data??[] as $d){
                $d=(Array)$d;
                $x=DB::table('output.ddub')->where([
                    'kodepemda'=>$kodepemda,
                    'tahun'=>$d['tahun']
                ])->first();
    
                if($x){
                    $ids[]=$x->id;
                    DB::table('output.ddub')->where('id',$x->id)->update([
                        'updated_at'=>$date,
                        "rencana_modal"=>$d['rencana_modal'],
                        "rencana_modal_infra"=>$d['rencana_modal_infra'],
                        "rencana_modal_lainya"=>$d['rencana_modal_lainya'],
                        "rencana_sr"=>$d['rencana_sr'],

                        "rencana_ag"=>$d['rencana_ag'],
                        "realisasi_ag"=>$d['realisasi_ag'],
                        // "realisasi_modal_lainya"=>$d['realisasi_modal_lainya'],
                        "realisasi_sr"=>$d['realisasi_sr'],
                        "laba_bersih"=>$d['laba_bersih'],

                
                        "keterangan"=>$d['keterangan']
    
                    ]);
    
                }else{
                    $id=(DB::table('output.ddub')->insertGetId([
                        'kodepemda'=>$kodepemda,
                        'tahun'=>$d['tahun'],
                        "rencana_modal"=>$d['rencana_modal'],
                        "rencana_modal_infra"=>$d['rencana_modal_infra'],
                        "rencana_modal_lainya"=>$d['rencana_modal_lainya'],
                        "rencana_sr"=>$d['rencana_sr'],

                        "rencana_ag"=>$d['rencana_ag'],
                        "realisasi_ag"=>$d['realisasi_ag'],
                        // "realisasi_modal_lainya"=>$d['realisasi_modal_lainya'],
                        "realisasi_sr"=>$d['realisasi_sr'],
                        "laba_bersih"=>$d['laba_bersih'],
                                                
                        "keterangan"=>$d['keterangan'],
                        'created_at'=>$date,
                        'updated_at'=>$date,
                    ]));
                    $ids[]=$id;
                }
    
               
    
            }
    
            if(count($ids)==0){
                DB::table('output.ddub')->where('tahun','<=',$tahun)->where('kodepemda',$kodepemda)->delete();
            }else{
                DB::table('output.ddub')->where('kodepemda',$kodepemda)
                ->whereNotIn('id',$ids)->where('tahun','<=',$tahun)->delete();
            }
    
            return [
                'code'=>200,
                'data'=>'Data Berhasil Diperbarui'
            ];

        }elseif ($request->meta['tipe_bantuan']=='PENDAMPING') {
            $ids=[];

            foreach($request->data??[] as $d){
                $d=(Array)$d;
                $x=DB::table('output.dana_pendamping')->where([
                    'kodepemda'=>$kodepemda,
                    'tahun'=>$d['tahun']
                ])->first();
    
                if($x){
                    $ids[]=$x->id;
                    DB::table('output.dana_pendamping')->where('id',$x->id)->update([
                        'updated_at'=>$date,
                        "rencana_modal"=>$d['rencana_modal'],
                        "rencana_modal_infra"=>$d['rencana_modal_infra'],
                        "rencana_modal_lainya"=>$d['rencana_modal_lainya'],
                        "rencana_sr"=>$d['rencana_sr'],

                        "rencana_ag"=>$d['rencana_ag'],
                        "realisasi_ag"=>$d['realisasi_ag'],
                        // "realisasi_modal_lainya"=>$d['realisasi_modal_lainya'],
                        "realisasi_sr"=>$d['realisasi_sr'],
                        "laba_bersih"=>$d['laba_bersih'],

                        "keterangan"=>$d['keterangan']
    
                    ]);
    
                }else{
                    $id=(DB::table('output.dana_pendamping')->insertGetId([
                        'kodepemda'=>$kodepemda,
                        'tahun'=>$d['tahun'],
                        "rencana_modal"=>$d['rencana_modal'],
                        "rencana_modal_infra"=>$d['rencana_modal_infra'],
                        "rencana_modal_lainya"=>$d['rencana_modal_lainya'],
                        "rencana_sr"=>$d['rencana_sr'],

                        "rencana_ag"=>$d['rencana_ag'],
                        "realisasi_ag"=>$d['realisasi_ag'],
                        // "realisasi_modal_lainya"=>$d['realisasi_modal_lainya'],
                        "realisasi_sr"=>$d['realisasi_sr'],
                        "laba_bersih"=>$d['laba_bersih'],

                        "keterangan"=>$d['keterangan'],
                        'created_at'=>$date,
                        'updated_at'=>$date,
                    ]));
                    $ids[]=$id;
                }
    
               
    
            }
    
            if(count($ids)==0){
                DB::table('output.dana_pendamping')->where('tahun','<=',$tahun)->where('kodepemda',$kodepemda)->delete();
            }else{
                DB::table('output.dana_pendamping')->where('tahun','<=',$tahun)->where('kodepemda',$kodepemda)
                ->whereNotIn('id',$ids)->delete();
            }
    
            return [
                'code'=>200,
                'data'=>'Data Berhasil Diperbarui'
            ];
            # code...
        }
    }
}
