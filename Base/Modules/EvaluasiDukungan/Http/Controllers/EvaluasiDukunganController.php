<?php

namespace Modules\EvaluasiDukungan\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use App\Http\Controllers\FilterCtrl;
use Carbon\Carbon;
use Barryvdh\DomPDF\Facade\Pdf;
use Storage;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Validator;
use Alert;
class EvaluasiDukunganController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function home($tahun,Request $request)
    {
        $filter=FilterCtrl::buildFilterPemda($request);
        $filter['lokus']=true;
        $filter['sortlist']=(int)$tahun;

        $pemdas=FilterCtrl::filter_pemda($filter['regional']??null,$filter['lokus'],$filter['sortlist'],$filter['tipe_bantuan'],$filter['jenis_pemda'],$filter['pemda']);
        
        
        $pemda=DB::table('master_pemda')->whereIn('kodepemda',$pemdas)->orderBy('nama_pemda','asc')->get();
        
        return view('evaluasidukungan::admin.home')->with(['pemda'=>$pemda]);


        if($request->a){
            return view('evaluasidukungan::admin.home')->with(['pemda'=>$pemda]);

        }else{
            return view('evaluasidukungan::underconstruk')->with(['pemda'=>$pemda]);

        }


    }

    public function form($tahun,$kodepemda,Request $request)
    {

        $filter=FilterCtrl::buildFilterPemda($request);

        $filter['lokus']=true;
        $filter['sortlist']=(int)$tahun;

        $pemdas=FilterCtrl::filter_pemda($filter['regional']??null,$filter['lokus'],$filter['sortlist'],$filter['tipe_bantuan'],$filter['jenis_pemda'],$filter['pemda']);
        $pemda=DB::table('master.master_pemda')->where('kodepemda',$kodepemda)->first();
        $bantuan=DB::table('master.pemda_lokus')->where('kodepemda',$kodepemda)->get()->toArray();
        $bumdam=DB::table('master.master_bu as bu')->select('bu.*')
        ->join('master.master_bu_pemda as bup','bup.id_bu','=','bu.id')
        ->where('bup.kodepemda',$kodepemda)->get()->toArray();;

        $meta=[
            'pemda'=>[
                'kodepemda'=>$pemda->kodepemda,
                'nama_pemda'=>$pemda->nama_pemda,
                'regional'=>$pemda->regional_1
            ],
            'bantuan'=>[
                'select'=>[
                    'tipe_bantuan'=>$bantuan[0]->tipe_bantuan,
                    'tahun_bantuan'=>$bantuan[0]->tahun_bantuan,
                    'tahun_proyek'=>$bantuan[0]->tahun_proyek,
                ],
                'pilihan'=>$bantuan

            ],
            'bumdam'=>[
                'select'=>[
                    "id"=>$bumdam[0]->id,
                     "nama_bu"=> $bumdam[0]->nama_bu,
                ],
                'pilihan'=>$bumdam
            ]
        ];


        if(in_array($kodepemda,$pemdas)){
            $berita_acara=DB::table('output.berita_acara_evaluasi')->where('kodepemda',$kodepemda)
            ->where('tahun',$tahun)->first();
            $pemangku=[
                'nip'=>null,
                'nama'=>null,
                'jabatan'=>null
            ];
            
            if($berita_acara){
                $pemangku['nip']=$berita_acara->nip;
                $pemangku['nama']=$berita_acara->nama;
                $pemangku['jabatan']=$berita_acara->jabatan;

                if($berita_acara->status==3){
                    return view('evaluasidukungan::admin.show_berita_acara',['meta'=>$meta,'berita_acara'=>$berita_acara]);
                }
            }
            return view('evaluasidukungan::admin.form',['meta'=>$meta,'pemangku'=>$pemangku]);

        }else{
            return abort('404');
        }
        

    }

    public function listPemda($tahun,$deskId,Request $request){
        $filter=FilterCtrl::buildFilterPemda($request);
        $filter['lokus']=true;
        $filter['sortlist']=(int)$tahun;
        $pemdas=FilterCtrl::filter_pemda($filter['regional']??null,$filter['lokus'],$filter['sortlist'],$filter['tipe_bantuan'],$filter['jenis_pemda'],$filter['pemda']);
        $pemda=DB::table('master_pemda')->whereIn('kodepemda',$pemdas)->orderBy('nama_pemda','asc')->get();
        
        return $pemda;
    }


    public function exportExcel($tahun,$kodepemda,Request $request){
        
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load(__DIR__.'/../../EVAL_EXPORT_THEM.xlsx');
        $sheet=$spreadsheet->setActiveSheetIndexByName('TEMATIK I');
        $request['only_data']=true;
        $data=$this->buildBertiaAcara($tahun,$kodepemda,$request);

        $pilihan_tahun=[$tahun-4,$tahun-3,$tahun-2,$tahun-1,$tahun,$tahun+1];

        
     
        if(isset($data['form_1']['data_turunan'])){
            $sheet= $spreadsheet->setActiveSheetIndexByName('TEMATIK I-PROV');
           
            $sheetIndex = $spreadsheet->getIndex(
                $spreadsheet->getSheetByName('TEMATIK I')
            );
            $spreadsheet->removeSheetByIndex($sheetIndex);
            $sheet= $spreadsheet->setActiveSheetIndexByName('TEMATIK I-PROV');
            $sheet->setTitle('TEMATIK I');
            $sheet=$spreadsheet->setActiveSheetIndexByName('TEMATIK I');


            $num_row=21;
            $num_row2=9;

            $data_x=[
            ];
          
            foreach($data['form_1']['data_turunan'] as $pkey=>$x){
                $sheet->setCellValue('A'.($num_row-5),(int)$x['meta']->kodepemda);
                $sheet->setCellValue('B'.($num_row-5),$x['meta']->nama_pemda);
                
                foreach($pilihan_tahun as $key=>$th){
                    if(!isset($data_x[$th])){
                        $data_x[$th]=[
                            'B'=>[],
                            'C'=>[],
                            'D'=>[],
                            'F'=>[],
                            'G'=>[],
                            'K'=>[],

                        ];
                    }

                    $data_x[$th]['B'][]='B'.($num_row+$key);
                    $data_x[$th]['C'][]='C'.($num_row+$key);
                    $data_x[$th]['D'][]='D'.($num_row+$key);
                    $data_x[$th]['F'][]='F'.($num_row+$key);
                    $data_x[$th]['G'][]='G'.($num_row+$key);
                    $data_x[$th]['K'][]='K'.($num_row+$key);

                    $sheet->setCellValue('A'.($num_row+$key),$th);

                    foreach($x['data'] as $d){

                        if($d->tahun==$th){
                            $sheet->setCellValue('A'.($num_row+$key),$d->tahun);

                            $sheet->setCellValue('B'.($num_row+$key),$d->kk);

                            $sheet->setCellValue('C'.($num_row+$key),$d->bumdam_kk);

                            $sheet->setCellValue('D'.($num_row+$key),$d->bumdam_sr);

                            $sheet->setCellValue('F'.($num_row+$key),$d->nonbumdam_kk);

                            $sheet->setCellValue('G'.($num_row+$key),$d->nonbumdam_sr);

                            $sheet->setCellValue('K'.($num_row+$key),$d->bjp_kk);

                            $sheet->setCellValue('Q'.($num_row+$key),$d->keterangan);
        
                        }
                    }
            
                }

                $num_row+=12;

            }

            $h=($sheet->getHighestDataRow());
            $sheet->removeRow($num_row-5, ($h-($num_row-5)));
            $key=0;
            $sheet->setCellValue('A'.($num_row2-5),(int)$data['kodepemda']);
            $sheet->setCellValue('B'.($num_row2-5),$data['nama_pemda'].' (AGGREGATE)');
            
            foreach($data_x as $th=>$x){
                $sheet->setCellValue('A'.($num_row2+$key),$th);
                foreach($x as $k=>$d){
                    $sheet->setCellValue($k.($num_row2+$key),'='.implode('+',$d));
                }

                $key+=1;
            }


        }else{

            $sheetIndex = $spreadsheet->getIndex(
                $spreadsheet->getSheetByName('TEMATIK I-PROV')
            );
            $spreadsheet->removeSheetByIndex($sheetIndex);

            $sheet->setCellValue('A4',(int)$data['kodepemda']);
            $sheet->setCellValue('B4',$data['nama_pemda']);
    
            foreach($pilihan_tahun as $key=>$th){
                foreach($data['form_1']['data'] as $d){
                    if($d->tahun==$th){
                        $sheet->setCellValue('A'.(9+$key),$d->tahun);
                        $sheet->setCellValue('B'.(9+$key),$d->kk);
                        $sheet->setCellValue('C'.(9+$key),$d->bumdam_kk);
                        $sheet->setCellValue('D'.(9+$key),$d->bumdam_sr);
                        $sheet->setCellValue('F'.(9+$key),$d->nonbumdam_kk);
                        $sheet->setCellValue('G'.(9+$key),$d->nonbumdam_sr);
                        $sheet->setCellValue('K'.(9+$key),$d->bjp_kk);
                        $sheet->setCellValue('q'.(9+$key),$d->keterangan);
    
                    }
                }
            }

        }

       

        $sheet=$spreadsheet->setActiveSheetIndexByName('TEMATIK II');
        $sheet->setCellValue('A4',(int)$data['kodepemda']);
        $sheet->setCellValue('B4',$data['nama_pemda']);
        foreach($pilihan_tahun as $key=>$th){
            foreach($data['form_2']['data'] as $d){
                if($d->tahun==$th){
                    $sheet->setCellValue('A'.(9+$key),$d->tahun);
                    $sheet->setCellValue('B'.(9+$key),$d->rkpd_ar_sub);
                    $sheet->setCellValue('C'.(9+$key),$d->rkpd_ar_ag);
                    $sheet->setCellValue('D'.(9+$key),$d->apbd_ar_sub);
                    $sheet->setCellValue('E'.(9+$key),$d->apbd_ar_ag);
                    $sheet->setCellValue('H'.(9+$key),$d->apbd_tot);
                    $sheet->setCellValue('J'.(9+$key),$d->keterangan);


                }
            }
        }

        $sheet=$spreadsheet->setActiveSheetIndexByName('TEMATIK III');
        $BANTUAN=array_keys($data['form_3']);

        if(count($data['form_3'])>1){
            // A4->L15 -> A18->l29

        }else{
            if($BANTUAN[0]=='PENDAMPING'){
            $sheet->setCellValue('B'.(10-6),"Pemenuhan Dana Pendamping Dalam Dukungan Serta Capaian SR ");

            }else{
            $sheet->setCellValue('B'.(10-6),"Pemenuhan DDUB Dalam Dukungan Serta Capaian SR ");

            }
            $sheet->setCellValue('A4',(int)$data['kodepemda']);
            $sheet->setCellValue('B4',$data['nama_pemda']);
            $sheet->removeRow(18,13);

        }
    
        foreach($pilihan_tahun as $key=>$th){
           if(count($data['form_3'])==1){

                foreach($data['form_3'][$BANTUAN[0]]['data'] as $d){
                    if($d->tahun==$th){
                        $sheet->setCellValue('A'.(10+$key),$d->tahun);
                        $sheet->setCellValue('B'.(10+$key),$d->rencana_ag);
                        $sheet->setCellValue('C'.(10+$key),$d->rencana_sr);
                        $sheet->setCellValue('D'.(10+$key),$d->realisasi_ag);
                        $sheet->setCellValue('E'.(10+$key),$d->realisasi_sr);
                        $sheet->setCellValue('H'.(10+$key),$d->rencana_modal);
                        $sheet->setCellValue('I'.(10+$key),$d->rencana_modal_infra);
                        $sheet->setCellValue('K'.(10+$key),$d->laba_bersih);
                        $sheet->setCellValue('L'.(10+$key),$d->keterangan);
                    }
                }
           }else{
            if($BANTUAN[0]=='PENDAMPING'){
                $sheet->setCellValue('B'.(10-5),"Pemenuhan Dana Pendamping Dalam Dukungan Serta Capaian SR ");
    
                }else{
                $sheet->setCellValue('B'.(10-5),"Pemenuhan DDUB Dalam Dukungan Serta Capaian SR ");
    
                }
            if($BANTUAN[1]=='PENDAMPING'){
                $sheet->setCellValue('B'.(24-5),"Pemenuhan Dana Pendamping Dalam Dukungan Serta Capaian SR ");
    
            }else{
            $sheet->setCellValue('B'.(24-5),"Pemenuhan DDUB Dalam Dukungan Serta Capaian SR ");

            }
            foreach($data['form_3'][$BANTUAN[0]]['data'] as $d){

                if($d->tahun==$th){

                    $sheet->setCellValue('A'.(10+$key),$d->tahun);
                    $sheet->setCellValue('B'.(10+$key),$d->rencana_ag);
                    $sheet->setCellValue('C'.(10+$key),$d->rencana_sr);
                    $sheet->setCellValue('D'.(10+$key),$d->realisasi_ag);
                    $sheet->setCellValue('E'.(10+$key),$d->realisasi_sr);
                    $sheet->setCellValue('H'.(10+$key),$d->rencana_modal);
                    $sheet->setCellValue('I'.(10+$key),$d->rencana_modal_infra);
                    $sheet->setCellValue('K'.(10+$key),$d->laba_bersih);

                    $sheet->setCellValue('L'.(10+$key),$d->keterangan);
                }
            }

            foreach($data['form_3'][$BANTUAN[1]]['data'] as $d){
                if($d->tahun==$th){
                    $sheet->setCellValue('A'.(24+$key),$d->tahun);
                    $sheet->setCellValue('B'.(24+$key),$d->rencana_ag);
                    $sheet->setCellValue('C'.(24+$key),$d->rencana_sr);
                    $sheet->setCellValue('D'.(24+$key),$d->realisasi_ag);
                    $sheet->setCellValue('E'.(24+$key),$d->realisasi_sr);
                    $sheet->setCellValue('H'.(24+$key),$d->rencana_modal);
                    $sheet->setCellValue('I'.(24+$key),$d->rencana_modal_infra);
                    $sheet->setCellValue('K'.(24+$key),$d->laba_bersih);
                    $sheet->setCellValue('L'.(24+$key),$d->keterangan);
                }
            }


           }
        }


        $sheet=$spreadsheet->setActiveSheetIndexByName('TEMATIK IV');

        if(count($data['form_4']['data'])>1){
            foreach($data['form_4']['data'] as $d){
                // copy
                // A4->F13 ++ 10;
            }
        }

            $row_num=5;
            foreach($data['form_4']['data'] as $d){
                $sheet->setCellValue('A'.(($row_num-1)),(int)$data['kodepemda']);
                $sheet->setCellValue('B'.(($row_num-1)),$data['nama_pemda']." / ".$d['meta']->nama_bu);
                foreach($pilihan_tahun as $key=>$th){
                    foreach($d['data'] as $x){
                    if($x->tahun==$th){
                        $sheet->setCellValue('A'.($key+($row_num+3)),$x->tahun);
                        $sheet->setCellValue('B'.($key+($row_num+3)),$x->nilai_fcr);
                        $sheet->setCellValue('C'.($key+($row_num+3)),$x->fcr?'FCR':'NONFCR');
                        $sheet->setCellValue('D'.($key+($row_num+3)),$x->subsidi?'ADA':'TIDAK ADA');
                        $sheet->setCellValue('E'.($key+($row_num+3)),$x->subsidi?$x->nilai_subsidi:null);
                        $sheet->setCellValue('F'.($key+($row_num+3)),$x->keterangan);


                    }


                }

             }
             $row_num+=10;

        }



        $sheet=$spreadsheet->setActiveSheetIndexByName('TEMATIK V');
        $x=$data['form_6']['data'];
        $sheet->setCellValue('B9',$x['rc5']?'ADA':'TIDAK ADA');
        $sheet->setCellValue('C9',$x['rc5_isu']);
        $sheet->setCellValue('D9',$x['rc5_rktl']);
        $sheet->setCellValue('E9',$x['rc5_keterangan']);


        $sheet->setCellValue('B10',$x['rc7']?'ADA':'TIDAK ADA');
        $sheet->setCellValue('C10',$x['rc7_isu']);
        $sheet->setCellValue('D10',$x['rc7_rktl']);
        $sheet->setCellValue('E10',$x['rc7_keterangan']);


        $sheet->setCellValue('B11',$x['rc12']?'ADA':'TIDAK ADA');
        $sheet->setCellValue('C11',$x['rc12_isu']);
        $sheet->setCellValue('D11',$x['rc12_rktl']);
        $sheet->setCellValue('E11',$x['rc12_keterangan']);



        $sheet->setCellValue('B12',$x['rc14']?'ADA':'TIDAK ADA');
        $sheet->setCellValue('C12',$x['rc14_isu']);
        $sheet->setCellValue('D12',$x['rc14_rktl']);
        $sheet->setCellValue('E12',$x['rc14_keterangan']);

        $sheet->setCellValue('B13',$x['rc16']?'ADA':'TIDAK ADA');
        $sheet->setCellValue('C13',$x['rc16_isu']);
        $sheet->setCellValue('D13',$x['rc16_rktl']);
        $sheet->setCellValue('E13',$x['rc16_keterangan']);



        $sheet->setCellValue('B14',$x['rc22']?'ADA':'TIDAK ADA');
        $sheet->setCellValue('C14',$x['rc22_isu']);
        $sheet->setCellValue('D14',$x['rc22_rktl']);
        $sheet->setCellValue('E14',$x['rc22_keterangan']);

        $sheet=$spreadsheet->setActiveSheetIndexByName('TEMATIK I');

    
        $fileName=urlencode('Evaluasi Dukungan '.$data['nama_pemda']." Tahun ".$tahun.".xlsx");
        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'. urlencode($fileName).'"');
        $writer->save('php://output');

    }

  
    public function storeCatatan(Request $request)
    {
        //
        $now=Carbon::now();
        DB::beginTransaction();
        
        try{
            $get=DB::table('output.catatan_eval_dukungan')->where('kodepemda',$request->meta['kodepemda'])
            ->where('tahun',$request->meta['tahun'])
            ->where('form_id',str_replace('form_','',$request->meta['form_key']))->first();
            if($get){
                DB::table('output.catatan_eval_dukungan')->where('id',$get->id)->update([
                    'catatan'=>$request->catatan,
                    'updated_at'=>$now
                ]);
            }else{
                DB::table('output.catatan_eval_dukungan')->insertGetId([
                    'catatan'=>$request->catatan,
                    'kodepemda'=>$request->meta['kodepemda'],
                    'tahun'=>$request->meta['tahun'],
                    'form_id'=>str_replace('form_','',$request->meta['form_key']),
                    'updated_at'=>$now,
                    'created_at'=>$now
    
    
                ]);
            }
            DB::commit();
            return [
                'code'=>200,
                'message'=>'Berhasil Menyimpan Catatan',
            ];
        }catch(\Exception $e){
            DB::rollback();
            return [
                'code'=>500,
                'message'=>'Gagal Menyimpan Catatan',

            ];
        }
    }

    public function refData($tahun,$kodepemda,Request $request){

        if($request->form=='form_1'){
            $data_capail=DB::table('dataset.data_dukcapil')->where('kodepemda',$kodepemda)
            ->orderBy('tahun','desc')->get();

            if(strlen($kodepemda)<=3){
                $data_capail=DB::table('dataset.data_dukcapil')->where('kodepemda','ilike',$kodepemda.'%')
                ->orderBy('tahun','desc')->get();
                $data_calmer=DB::table('dataset.bumdam_periodik')->where('kodepemda','ilike',$kodepemda.'%')
                ->groupBy('kodepemda','tahun')
                ->selectRaw("sum(jumlahsr) as jumlah_sr,tahun,kodepemda")->get();
            }else{
                $data_capail=DB::table('dataset.data_dukcapil')->where('kodepemda',$kodepemda)
                ->orderBy('tahun','desc')->get();
                $data_calmer=DB::table('dataset.bumdam_periodik')->where('kodepemda','=',$kodepemda)
                ->groupBy('kodepemda','tahun')
                ->selectRaw("sum(jumlahsr) as jumlah_sr,tahun,kodepemda")->get();
            }
            $data=[];
            $calmer=[];

            foreach($data_capail as $ke=>$d){
                $data[$d->tahun.'_'.$d->kodepemda]=$d->jumlah_kk;
            }
            foreach($data_calmer as $ke=>$d){
                $calmer[$d->tahun.'_'.$d->kodepemda]=$d->jumlah_sr;
            }
            return [
                'code'=>200,
                'data'=>[
                    'dukcapil'=>$data,
                    'sicalmer'=>$calmer
                ]

            ];
        }else{

        }

        return [
            'code'=>200,
            'data'=>[]
        ];

    }


    public function pengesahan($tahun,$kodepemda,Request $request){

        $valid=Validator::make($request->all(),[
            'pemangku_nama'=>'required|string',
            'pemangku_nip'=>'required|string',
            'pemangku_jabatan'=>'required|string',

        ]);

        if($valid->fails()){
            Alert::error('','Data Pengesahan Belum Lengkap');

            return back();
        }

        $pdf=$this->buildBertiaAcara($tahun,$kodepemda,$request);
        if($pdf){
           $file= Storage::put('public/avaluasi-dukungan/'.$tahun.'/'.$kodepemda.'.pdf',$pdf);
           $file=Storage::url($file);
           DB::table('output.berita_acara_evaluasi')->updateOrInsert([
            'kodepemda'=>$kodepemda,
            'tahun'=>$tahun,
           ],[
            'kodepemda'=>$kodepemda,
            'tahun'=>$tahun,
            'status'=>1,
            'path'=>'storage/'.'avaluasi-dukungan/'.$tahun.'/'.$kodepemda.'.pdf',
            'nama'=>$request->pemangku_nama,
            'jabatan'=>$request->pemangku_jabatan,
            'nip'=>$request->pemangku_nip,
            'created_at'=>Carbon::now()
           ]);
        }

        return back();

    }


    static function tgl_indo($pecahkan){
        $bulan = array (
            1 =>'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );

        switch(strtolower($pecahkan[0])){
            case 'sunday':
                $pecahkan[0]="Minggu";
            break;
            case 'monday':
                $pecahkan[0]="Senin";
            break;
            case 'tuesday':
                $pecahkan[0]="Selasa";
            break;
            case 'wednesday':
                $pecahkan[0]="Rabu";
            break;
            case 'thursday':
                $pecahkan[0]="Kamis";
            break;
            case 'friday':
                $pecahkan[0]="Jum'at";
            break;
            case 'satuday':
                $pecahkan[0]="Saptu";
            break;
        }
        $pecahkan[2]=$bulan[(int)$pecahkan[2]];

        return $pecahkan;
       
    }

   



   
    public static function buildBertiaAcara($tahun,$kodepemda,Request $request){
        $pemda=DB::table('master.master_pemda')->where('kodepemda',$kodepemda)
        ->selectRaw("INITCAP(nama_pemda) as nama_pemda,kodepemda,regional_1,regional_2")->first();
        if(!$pemda){
            return abort(404);
        }

        $ba=DB::table('output.berita_acara_evaluasi')->where('kodepemda',$kodepemda)
        ->where('tahun',$tahun)->first();

       if($ba){

        $pemangku=[
            'pemangku_nip'=>$request->pemangku_nip??$ba->nip,
            'pemangku_nama'=>$request->pemangku_nama??$ba->nama,
            'pemangku_jabatan'=>$request->pemangku_jabatan??$ba->jabatan
        ];
        
       }else{
        $pemangku=[
            'pemangku_nip'=>$request->pemangku_nip,
            'pemangku_nama'=>$request->pemangku_nama,
            'pemangku_jabatan'=>$request->pemangku_jabatan
        ];
       }

        

        $data=[
            'nama_pemda'=>$pemda->nama_pemda,
            'kodepemda'=>$kodepemda,
            'tahun'=>$tahun
        ];

        setlocale(LC_ALL, 'IND');
        $date=Carbon::parse("06-10-2022")->format('l-d-m-Y');
        $date=static::tgl_indo(explode('-',$date));

       $kt=[];

        $data['date']=$date;
        $data['NAMA_PEMANGKU_BANGDA']="Nitta Rosalin, SE, MA";
        $data['NIP_PEMANGKU_BANGDA']="19711109 199703 2 011";
        $data['NIP_PEMANGKU_PEMDA']=$pemangku['pemangku_nip']??"............................................";
        $data['NAMA_PEMANGKU_PEMDA']=$pemangku['pemangku_nama']??".................................................";
        $data['NAMA_PEMANGKU_PEMDA_JABATAN']=$pemangku['pemangku_jabatan']??".................................................";
        // form 1

        if(strlen($kodepemda)<=3){
            $data['form_1']=[
                'data'=>DB::table('output.capaian_air_minum')->where([
                    ['kodepemda','ilike',DB::raw("'".$kodepemda."%'")],
                    ['tahun','<=',$tahun+1],
                ])
                ->groupBy('tahun')
                ->selectRaw("
                    tahun,
                    sum(kk) as  kk,
                    sum(bumdam_kk) as  bumdam_kk,
                    sum(bumdam_sr) as  bumdam_sr,
                    sum(nonbumdam_kk) as  nonbumdam_kk,
                    sum(nonbumdam_sr) as  nonbumdam_sr,
                    sum(bjp_kk) as  bjp_kk,
                    sum(bjp_kk) as  bjp_kk,
                    '' as keterangan
                ")
                ->orderBy('tahun','desc')->get(),
            ];
            

            $pemdas=DB::table('master.master_pemda')
            ->selectRaw("INITCAP(nama_pemda) as nama_pemda,kodepemda,regional_1,regional_2")
            ->where('kodepemda','ilike',$kodepemda.'%')->orderBy('kodepemda','asc')->get();
            $data['form_1']['data_turunan']=[];
            foreach($pemdas as $p){
                $data['form_1']['data_turunan'][]=[
                    'meta'=>$p,
                    'data'=>DB::table('output.capaian_air_minum')->where([
                        'kodepemda'=>$p->kodepemda,
                    ])->where('tahun','<=',$tahun+1)->orderBy('tahun','desc')->get()
                ];
            }


        }else{
            $data['form_1']=[
                'data'=>DB::table('output.capaian_air_minum')->where([
                    'kodepemda'=>$kodepemda,
                ])->where('tahun','<=',$tahun+1)->orderBy('tahun','desc')->get(),
            ];

        }
       
        $index_baseline='x';
        $index_exist='x';

        if(count( $data['form_1']['data'])){
            foreach($data['form_1']['data'] as $k=>$d){
                if($d->tahun<=$tahun){
                    if($index_exist=='x'){
                        $index_exist=$k;
                    }else{
                    
                        if($index_baseline=='x'){
                            $index_baseline=$k;
                        }
                    }
                }
            }


            


            


            if(isset($data['form_1']['data'][$index_baseline])){
                $x1=[
                    'kk'=>$data['form_1']['data'][$index_baseline]->kk,
                    'bumdam_kk'=>$data['form_1']['data'][$index_baseline]->bumdam_kk,
                    'nonbumdam_kk'=>$data['form_1']['data'][$index_baseline]->nonbumdam_kk,
                    'bjp_kk'=>$data['form_1']['data'][$index_baseline]->bjp_kk,
                    'tahun'=>$data['form_1']['data'][$index_baseline]->tahun,
                    'persentase_layak'=>static::percentage($data['form_1']['data'][$index_baseline]->bjp_kk+$data['form_1']['data'][$index_baseline]->bumdam_kk+$data['form_1']['data'][$index_baseline]->nonbumdam_kk, ($data['form_1']['data'][$index_baseline]->kk))

                ];
            }else{
                $x1=[
                    'kk'=>$data['form_1']['data'][$index_exist]->kk,
                    'bumdam_kk'=>$data['form_1']['data'][$index_exist]->bumdam_kk,
                    'nonbumdam_kk'=>$data['form_1']['data'][$index_exist]->nonbumdam_kk,
                    'bjp_kk'=>$data['form_1']['data'][$index_exist]->bjp_kk,
                    'tahun'=>$data['form_1']['data'][$index_exist]->tahun,
                    'persentase_layak'=>static::percentage($data['form_1']['data'][$index_exist]->bjp_kk+$data['form_1']['data'][$index_exist]->bumdam_kk+$data['form_1']['data'][$index_exist]->nonbumdam_kk, ($data['form_1']['data'][$index_exist]->kk))

                ];
            }


            $x2=(array)$data['form_1']['data'][$index_exist];


            $x2['persentase_layak']=static::percentage($x2['bumdam_kk']+$x2['nonbumdam_kk']+$x2['bjp_kk'],$x2['kk']);
            
           

           

            $data['form_1']['cal']=[
            
                'layak'=>$x2['persentase_layak']-$x1['persentase_layak'],
                'kk_layak'=>($x2['bumdam_kk']+$x2['nonbumdam_kk']+$x2['bjp_kk'])-($x1['bumdam_kk']+$x1['nonbumdam_kk']+$x1['bjp_kk']),
                'kk_bumdam'=>($x2['bumdam_kk']-$x1['bumdam_kk']),
                'tahun'=>$x2['tahun'],
                'tahun_x'=>$x1['tahun'],
            ];
        }else{
            $data['form_1']['cal']=[
                'layak'=>0,
                'kk_layak'=>0,
                'kk_bumdam'=>0,
                'tahun_x'=>$tahun-1,
                'tahun'=>$tahun
                
            ];

        }

        $kt['form_1']=array_filter($data['form_1']['data']->toArray(),function($el){
            return $el->keterangan;
        });

        $data['form_2']=[
            'data'=>DB::table('output.program_kegiatan')->where([
                'kodepemda'=>$kodepemda,
            ])->where('tahun','<=',$tahun+1)->orderBy('tahun','desc')->get()  
        ];

        $kt['form_2']=array_filter($data['form_2']['data']->toArray(),function($el){
            return $el->keterangan;
        });

        $index_exist='x';
        $index_baseline='x';
        if(count( $data['form_2']['data'])){
            foreach($data['form_2']['data'] as $k=> $x){
                if($x->tahun<=$tahun){
                    if($index_exist=='x'){
                        $index_exist=$k;
                    }else if($index_baseline=='x'){
                        $index_baseline=$k;
                    }
                }
            }

            $x2=(array) $data['form_2']['data'][$index_exist];

            if(isset($data['form_2']['data'][$index_baseline])){
                $x1=(array)$data['form_2']['data'][$index_baseline];
            }else{
                $x1=(array)$data['form_2']['data'][$index_exist];
            }

            $data['form_2']['cal']=[
                'anggaran'=>$x2['apbd_ar_ag'],
                'persentase_ag'=>static::percentage($x2['apbd_ar_ag'],$x2['apbd_tot']),
                'rkpd'=>$x2['rkpd_ar_ag'],
                'tahun'=>$x2['tahun'],
                'sink_anggaran'=>$x2['apbd_ar_ag']-$x2['apbd_ar_ag'],
            ];

        }else{
            $data['form_2']['cal']=[
                'rkpd'=>0,
                'persentase_ag'=>0,
                'anggaran'=>0,
                'tahun'=>$tahun,
                'sink_anggaran'=>0,
            ];

        }

       

        $tipe_bantuan=DB::table('master.pemda_lokus')->where('kodepemda',$kodepemda)->get();
        $form_3=[];
        $kSTIMULAN=[];
        $kPENDAMPING=[];
        foreach($tipe_bantuan as $d){
            if($d->tipe_bantuan=='STIMULAN'){
                $form_3['STIMULAN']=[
                    'data'=>DB::table('output.ddub')->where([
                        'kodepemda'=>$kodepemda,
                    ])->where('tahun','<=',$tahun+1)
                    ->orderBy('tahun','desc')->get()
                ];

                $kSTIMULAN=array_filter($form_3['STIMULAN']['data']->toArray(),function($el){
                    return $el->keterangan;
                });

                $form_3['STIMULAN']['cal']=[
                    'perencanaan'=>0,
                    'realisasi'=>0,
                    'tahun'=>$tahun,
                    'tahun_x'=>$tahun,
                ];

                if(count($form_3['STIMULAN']['data'])){
                   
                    foreach( $form_3['STIMULAN']['data'] as $d){
                        $form_3['STIMULAN']['cal']['perencanaan']+=$d->rencana_ag;
                        $form_3['STIMULAN']['cal']['realisasi']+=$d->realisasi_ag;
                        if($d->tahun>$form_3['STIMULAN']['cal']['tahun']){
                            $form_3['STIMULAN']['cal']['tahun']=$d->tahun;
                        }

                        if($d->tahun<$form_3['STIMULAN']['cal']['tahun']){
                            $form_3['STIMULAN']['cal']['tahun_x']=$d->tahun;
                        }
                    }
                }
                
            }else if($d->tipe_bantuan=='PENDAMPING'){
                $form_3['PENDAMPING']=[
                    'data'=>DB::table('output.dana_pendamping')->where([
                        'kodepemda'=>$kodepemda,
                    ])->where('tahun','<=',$tahun+1)->orderBy('tahun','desc')->get()
                ];

                $kPENDAMPING=array_filter($form_3['PENDAMPING']['data']->toArray(),function($el){
                    return $el->keterangan;
                });
                $form_3['PENDAMPING']['cal']=[
                    'perencanaan'=>0,
                    'realisasi'=>0,
                    'tahun'=>$tahun,
                    'tahun_x'=>$tahun,
                ];

                if(count($form_3['PENDAMPING']['data'])){
                   
                    foreach( $form_3['PENDAMPING']['data'] as $d){
                        $form_3['PENDAMPING']['cal']['perencanaan']+=$d->rencana_ag;
                        $form_3['PENDAMPING']['cal']['realisasi']+=$d->realisasi_ag;
                        if($d->tahun>$form_3['PENDAMPING']['cal']['tahun']){
                            $form_3['PENDAMPING']['cal']['tahun']=$d->tahun;
                        }
                        if($d->tahun<$form_3['PENDAMPING']['cal']['tahun']){
                            $form_3['PENDAMPING']['cal']['tahun_x']=$d->tahun;
                        }
                    }
                }

            }
        }

        $data['form_3']= $form_3;

        $kt['form_3']=array_merge($kPENDAMPING,$kSTIMULAN);

        $bumdam=DB::table('master.master_bu as bu')->join('master.master_bu_pemda as mpb','mpb.id_bu','=',
        'bu.id')->where('mpb.kodepemda',$kodepemda)
        ->selectRaw('bu.*')->get();

        $data['form_4']['data']=[];
        $kt['form_4']=[];
        foreach($bumdam as $key=>$bu){
            $data['form_4']['data'][$key]=[
                'meta'=>$bu,
                'data'=>DB::table('output.dukungan_pemda_bumdam')->where([
                    'kodepemda'=>$kodepemda,
                    'kodebu'=>$bu->id,
                ])->where('tahun','<=',$tahun)
                ->orderBy('tahun','desc')->get(),
                
            ];

            $x=array_filter( $data['form_4']['data'][$key]['data']->toArray(),function($el){
                return $el->keterangan;
            });
            if(count($x)){
                $kt['form_4']=array_merge( $kt['form_4'],$x);
            }

            if(count( $data['form_4']['data'][$key]['data'])){
                $x2=(array) $data['form_4']['data'][$key]['data'][0];
                $data['form_4']['cal'][$key]=[
                    'subsidi'=>$x2['nilai_subsidi'],
                    'tahun'=>$x2['tahun']
                ];
            }
            else{
                $data['form_4']['cal'][$key]=[
                    'subsidi'=>0,
                    'tahun'=>$tahun
                ];
            }

        }
        // s


        $kt['form_5']=[];

        $data['form_5']['data']=[];

        foreach($bumdam as $key=>$bu){
            $data['form_5']['data'][$key]=[
                'meta'=>$bu,
                'data'=>DB::table('output.dukungan_modal_pemda_bumdam')->where([
                    'kodepemda'=>$kodepemda,
                    'kodebu'=>$bu->id
                ])->where('tahun','<=',$tahun+1)->orderBy('tahun','desc')->get()
            ];

            if(count($data['form_5']['data'][$key]['data'])){
                $x2=(array)$data['form_5']['data'][$key]['data'][0];
                $data['form_5']['cal'][$key]=[
                    'modal'=>$x2['modal'],
                    'modal_infra'=>$x2['modal_infra'],
                    'laba'=>$x2['laba'],
                    'tahun'=>$x2['tahun']
                ];
            }
            else{
                $data['form_5']['cal'][$key]=[
                    'modal'=>0,
                    'modal_infra'=>0,
                    'laba'=>0,
                    'tahun'=>$tahun

                ];
            }




            if(count( $data['form_4']['data'][$key]['data'])){
                $x2=(array) $data['form_4']['data'][$key]['data'][0];
                $data['form_4']['cal'][$key]=[
                    'subsidi'=>$x2['nilai_subsidi'],
                    'tahun'=>$x2['tahun']
                ];
            }
            else{
                $data['form_4']['cal'][$key]=[
                    'subsidi'=>0,
                    'tahun'=>$tahun
                ];
            }

        }

        $data['form_5']['data']=[];
        $kt['form_5']=[];
        foreach($bumdam as $key=>$bu){
            $data['form_5']['data'][$key]=[
                'meta'=>$bu,
                'data'=>DB::table('output.dukungan_modal_pemda_bumdam')->where([
                    'kodepemda'=>$kodepemda,
                    'kodebu'=>$bu->id
                ])->where('tahun','<=',$tahun+1)->orderBy('tahun','desc')->get()
            ];

            if(count($data['form_5']['data'][$key]['data'])){
                $x2=(array)$data['form_5']['data'][$key]['data'][0];
                $data['form_5']['cal'][$key]=[
                    'modal'=>$x2['modal'],
                    'modal_infra'=>$x2['modal_infra'],
                    'laba'=>$x2['laba'],
                    'tahun'=>$x2['tahun']

                ];
            }
            else{
                $data['form_5']['cal'][$key]=[
                    'modal'=>0,
                    'modal_infra'=>0,
                    'laba'=>0,
                    'tahun'=>$tahun

                ];
            }


        }
       
        $kt['form_6']=[];
        $data['form_6']=[
            'data'=>(array)(DB::table('output.rediness_criteria')->where([
                'kodepemda'=>$kodepemda,
            ])->first()??[
                "rc5"=>0,
                "rc5_lampiran"=>[],
                "rc5_isu"=>'',
                "rc5_date_target"=>null,
                "rc5_date_done"=>null,

                "rc5_rktl"=>'',
                "rc5_keterangan"=>'',
                "rc7"=>0,
                "rc7_date_target"=>null,
                "rc7_date_done"=>null,
                "rc7_lampiran"=>[],
                "rc7_isu"=>'',
                "rc7_rktl"=>'',
                "rc7_keterangan"=>'',
                "rc12"=>0,
                "rc12_date_target"=>null,
                "rc12_date_done"=>null,
                "rc12_lampiran"=>[],
                "rc12_isu"=>'',
                "rc12_rktl"=>'',
                "rc12_keterangan"=>'',

                "rc14"=>0,
                "rc14_date_target"=>null,
                "rc14_date_done"=>null,
                "rc14_lampiran"=>[],
                "rc14_isu"=>'',
                "rc14_rktl"=>'',
                "rc14_keterangan"=>'',

                "rc16"=>0,
                "rc16_date_target"=>null,
                "rc16_date_done"=>null,
                "rc16_lampiran"=>[],
                "rc16_isu"=>'',
                "rc16_rktl"=>'',
                "rc16_keterangan"=>'',

                "rc22"=>0,
                "rc22_date_target"=>null,
                "rc22_date_done"=>null,
                "rc22_lampiran"=>[],
                "rc22_isu"=>'',
                "rc22_rktl"=>'',
                "rc22_keterangan"=>'',
            ])
        ];


        $catatan=[
           [
            'form'=>1,
            'title'=> 'Evaluasi Capaian Air Minum Layak',
           ],
           [
            'form'=>2,
            'title'=> 'Evaluasi Program Kegiatan Pengelolaan Air Minum Daerah',
           ],
           [
            'form'=>3,
            'title'=> 'Evaluasi Pemenuhan DDUB dan Dana Pendamping Serta Pemetaan  Dukungan PEMDA',
           ], [
            'form'=>4,
            'title'=> 'Evaluasi Dukungan Kebijakan PEMDA Dalam Pemenuhan Tarif Air Minum (FCR)',
           ]
           , [
            'form'=>5,
            'title'=> 'Hubungan Dukungan PEMDA kepad BUMDAM',
           ]
           , [
            'form'=>6,
            'title'=> 'Evaluasi Pemenuhan Readiness Criteria Pemerintah Daerah Dalam Komitmen dan Konsistensi Keikutsertaan Dalam Program NUWSP',
           ]
        ];
        $have_catatan=false;
        foreach([5,7,12,14,16,22] as $k){
            if($data['form_6']['data']['rc'.$k.'_keterangan']){
                $kt['form_6'][]=$data['form_6']['data']['rc'.$k.'_keterangan'];
            }
        }

        foreach($catatan as $key=>$c){
            $ct=DB::table('output.catatan_eval_dukungan')->where([
                'tahun'=>$tahun,
                'kodepemda'=>$kodepemda,
                'form_id'=>$c['form']
            ])->first();

            if($ct){
                $catatan[$key]['catatan']=$ct->catatan;
                $catatan[$key]['catatan_kt']=$kt['form_'.$c['form']];
                $catatan[$key]['catatan_kt'][]=$ct->catatan;



            }else{
                $catatan[$key]['catatan']=null;
                $catatan[$key]['catatan_kt']=$kt['form_'.$c['form']];
            }

            if(count($catatan[$key]['catatan_kt'])){
                $have_catatan=true;
            }
        }

        $data['catatan']=$catatan;
        $data['have_catatan']=$have_catatan;
        
        if($request->only_data){
            return $data;
        }


       if($request->pre==1){
        return Pdf::loadView('evaluasidukungan::admin.berita_acara_2', $data)->stream('evaluasi-dukungan-'.$data['nama_pemda'].'-tahun-'.$tahun.'.pdf');

       }
       elseif($request->pre==2){

        $html=view('evaluasidukungan::admin.berita_acara_2', $data)->render();
        return $html;
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        header("Content-Description: File Transfer");
        header('Content-Disposition: attachment; filename="generated.docx"');
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');
        $objWriter->saveAs("php://output");

       }
//dd($data);
        if($request->pemangku_nama){
           return Pdf::loadView('evaluasidukungan::admin.berita_acara_2', $data)->output();
        }else{
            return Pdf::loadView('evaluasidukungan::admin.berita_acara_2', $data)->stream('evaluasi-dukungan-'.$data['nama_pemda'].'-tahun-'.$tahun.'.pdf');

        }
    }

    public function getCatatan(Request $request)
    {
        $get=DB::table('output.catatan_eval_dukungan')->where('kodepemda',$request->meta['kodepemda'])
        ->where('tahun',$request->meta['tahun'])->where('form_id',str_replace('form_','',$request->meta['form_key']))->first();
        if($get){
            return [
                'code'=>200,
                'data'=>$get->catatan
            ];
        }else{
            return [
                'code'=>200,
                'data'=>''
            ];
        }
    }

   
   public static  function percentage($a,$b){
    if($a==0 OR $b==0){
        return 0;
    }else{
        return ($a/$b)*100;
     }
   }
}
