<?php

namespace Modules\EvaluasiDukungan\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use Carbon\Carbon;

class RKPDAPBDController extends Controller
{
    public function index($kodepemda,Request $request)
    {
        $tahun=$request->meta['tahun']??0;

       return [
        'code'=>200,
        'data'=>DB::table('output.program_kegiatan')->where([
            'kodepemda'=>$request->kodepemda,
        ])->where('tahun','<=',$tahun+1)->orderBy('tahun','desc')->get()
        ];
    }

    public function store($kodepemda,Request $request)
    {
        $date=Carbon::now();
        $ids=[];
        $tahun=$request->meta['tahun']??0;


        foreach($request->data??[] as $d){
            $d=(Array)$d;
            $x=DB::table('output.program_kegiatan')->where([
                'kodepemda'=>$kodepemda,
                'tahun'=>$d['tahun']
            ])->first();

            if($x){
                $ids[]=$x->id;
                DB::table('output.program_kegiatan')->where('id',$x->id)->update([
                    'updated_at'=>$date,
                    "rkpd_ar_sub"=>$d['rkpd_ar_sub'],
                    "rkpd_ar_ag"=>$d['rkpd_ar_ag'],
                    "apbd_ar_sub"=>$d['apbd_ar_sub'],
                    "apbd_ar_ag"=>$d['apbd_ar_ag'],
                    "apbd_tot"=>$d['apbd_tot'],
                   
                    "keterangan"=>$d['keterangan']

                ]);

            }else{
                $id=(DB::table('output.program_kegiatan')->insertGetId([
                    'kodepemda'=>$kodepemda,
                    'tahun'=>$d['tahun'],
                    "rkpd_ar_sub"=>$d['rkpd_ar_sub'],
                    "rkpd_ar_ag"=>$d['rkpd_ar_ag'],
                    "apbd_ar_sub"=>$d['apbd_ar_sub'],
                    "apbd_ar_ag"=>$d['apbd_ar_ag'],
                    "apbd_tot"=>$d['apbd_tot'],
                    "keterangan"=>$d['keterangan'],
                    'created_at'=>$date,
                    'updated_at'=>$date,
                ]));
                $ids[]=$id;
            }

           

        }

        if(count($ids)==0){
            DB::table('output.program_kegiatan')->where('tahun','<=',$tahun)->where('kodepemda',$kodepemda)->delete();
        }else{
            DB::table('output.program_kegiatan')->where('tahun','<=',$tahun)->where('kodepemda',$kodepemda)
            ->whereNotIn('id',$ids)->delete();
        }

        return [
            'code'=>200,
            'data'=>'Data Berhasil Diperbarui'
        ];
    }

    public function getChart($tahun,$kodepemda){
        $data=DB::table('output.program_kegiatan')->where('tahun','<=',$tahun)->where('kodepemda',$kodepemda)->get();

        $series=[
            [
                'name'=>'RKPD',
                'data'=>[]
            ],
            [
                'name'=>'APBD',
                'data'=>[]
            ],
            [
                'name'=>'Sinkronisasi',
                'data'=>[]
            ],
        ];

        foreach($data as $k=>$d){
            $series[0]['data'][$k]=[
                'name'=>$d->tahun,
                'y'=>(int)$d->rkpd_ar_ag
            ];
            $series[1]['data'][$k]=[
                'name'=>$d->tahun,
                'y'=>(int)$d->apbd_ar_ag
            ];
            $series[2]['data'][$k]=[
                'name'=>$d->tahun,
                'y'=>((int)$d->apbd_ar_ag) - ((int)$d->rkpd_ar_ag)
            ];
        }

        return [
            'code'=>200,
            'data'=>[$series[2]]
        ];
    }
}


