<?php

namespace Modules\EvaluasiDukungan\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use Carbon\Carbon;
class ReadinessCriteria extends Controller
{
    public function index($kodepemda,Request $request)
    {
        $tahun=$request->meta['tahun']??0;

       return [
        'code'=>200,
        'data'=>(DB::table('output.rediness_criteria')->where([
            'kodepemda'=>$request->kodepemda,
        ])->first()??[
            "rc5"=>0,
            "rc5_date_done"=>null,
            "rc5_date_target"=>null,

            "rc5_lampiran"=>[],
            "rc5_isu"=>'',
            "rc5_rktl"=>'',
            "rc5_keterangan"=>'',

            "rc7"=>0,
            "rc7_date_done"=>null,
            "rc7_date_target"=>null,
            "rc7_lampiran"=>[],
            "rc7_isu"=>'',
            "rc7_rktl"=>'',
            "rc7_keterangan"=>'',

            "rc12"=>0,
            "rc12_date_done"=>null,
            "rc12_date_target"=>null,
            "rc12_lampiran"=>[],
            "rc12_isu"=>'',
            "rc12_rktl"=>'',
            "rc12_keterangan"=>'',

            "rc14"=>0,
            "rc14_date_done"=>null,
            "rc14_date_target"=>null,
            "rc14_lampiran"=>[],
            "rc14_isu"=>'',
            "rc14_rktl"=>'',
            "rc14_keterangan"=>'',

            "rc16"=>0,
            "rc16_date_done"=>null,
            "rc16_date_target"=>null,
            "rc16_lampiran"=>[],
            "rc16_isu"=>'',
            "rc16_rktl"=>'',
            "rc16_keterangan"=>'',

            "rc22"=>0,
            "rc22_date_done"=>null,
            "rc22_date_target"=>null,
            "rc22_lampiran"=>[],
            "rc22_isu"=>'',
            "rc22_rktl"=>'',
            "rc22_keterangan"=>'',
        ])
        ];
    }

    public function store($kodepemda,Request $request)
    {
        $date=Carbon::now();
        $ids=[];
        $tahun=$request->meta['tahun']??0;

        $d=$request->data??[];
        foreach(['rc5','rc7','rc12','rc14','rc16','rc22'] as $x){
            if(isset($d[$x.'_lampiran'])){
                $d[$x.'_lampiran']=json_encode($d[$x.'_lampiran']);
            }
        }

        $x=DB::table('output.rediness_criteria')->where([
            'kodepemda'=>$kodepemda,
        ])->first();

        if($x){
            $ids[]=$x->id;
            $d['updated_at']=$date;
            DB::table('output.rediness_criteria')->where('id',$x->id)->update($d);

        }else{
            $d['updated_at']=$date;
            $d['created_at']=$date;
            $d['kodepemda']=$kodepemda;

            
            $id=DB::table('output.rediness_criteria')->insertGetId($d);

            $ids[]=$id;
        }

        
        if(count($ids)==0){
            DB::table('output.rediness_criteria')->where('kodepemda',$kodepemda)->delete();
        }else{
            DB::table('output.rediness_criteria')->where('kodepemda',$kodepemda)
            ->whereNotIn('id',$ids)->delete();
        }

        return [
            'code'=>200,
            'data'=>'Data Berhasil Diperbarui'
        ];
    }
}
