<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=0.1, shrink-to-fit=yes">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <style>
            @media print {
              .break_before {page-break-before: always!important}

            }

            .justify{
              text-align: justify;
               text-justify: inter-word;
            }
            .break_before {page-break-before: always!important}

            h1,h2,h3,h4,h5,p,li,td{
                font-family:Verdana, Geneva, Tahoma, sans-serif
            }
            .tdata td,.tdata th{
                font-size: 10px!important;
                border-left:1px solid #ddd;
                border-bottom:1px solid #ddd;
                padding: 5px;

            }
            .table {
                display: table;
                border-spacing: 0px;
                border: 1px solid #ddd;
                width:100%;
            }
            .bg-secondary{
                background-color: #f1f1f1;
            }

        </style>
        @php
        function checkZero($a,$b){
            if($a==0 OR $b==0){
                return 0;
            }else{
                return ($a/$b)*100;
            }
        }

        @endphp
    </head>
    <body>
        <table>
            <thead>
              <tr>
                <th colspan="4" style="text-align: center">
                  <img src="{{public_path('assets/img/brand/head.png')}}" style="width:100%;" alt="">
                </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td colspan="4" style="text-align: center; padding-top:10px; padding-bottom:20px;">
                  <h3 style="margin:0;padding:0;">BERITA ACARA</h3>
                  <p style="margin:0;padding:0;">
                    <b>Evaluasi Dukungan Pemerintah Daerah Terhadap Program Air Minum</b>
                  </p>
                  <p style="margin:0;padding:0;">
                    <b>Dalam Kerangka National Urban Water Supply Project (NUWSP) Tahun Anggaran {{$tahun}}</b>
                  </p>
                </td>
              </tr>
              <tr>
                <td colspan="4" class="justify">&nbsp;&nbsp;&nbsp; Pada hari ini, {{$date[0]}} tanggal {{$date[1]}} bulan {{$date[2]}} Tahun {{$date[3]}}, telah dilaksanakan Forum Desk Evaluasi Dukungan Pemerintah Daerah terhadap Program Air Minum dalam Kerangka NUWSP bersama pemerintah daerah {{$nama_pemda}} dengan rincian hasil pembahasan sebagai berikut: </td>
              </tr>
              <tr>
                <td colspan="4">
                  <ol>
                    <li class="justify">Sudah dilakukan pembahasan desk untuk melihat capaian dan evaluasi terhadap kinerja, menggali permasalahan pelaksanaan program dan kegiatan serta menyusun tindak lanjut kegiatan pemerintah daerah untuk mendukung BUMD Air Minum dalam kerangka NUWSP</li>
                  </ol>
                </td>
              </tr>
              <tr>
                <td colspan="4">
                  <ol start="2">
                    <li class="justify">Hasil Evaluasi Dukungan Pemerintah Daerah terhadap Program Air Minum dalam Kerangka NUWSP sebagaimana dimaksud pada poin 1 adalah sebagaimana terlampir</li>
                  </ol>
                </td>
              </tr>
              <tr>
                <td colspan="4">
                  <ol start="3">
                    <li class="justify">Pemetaan Dukungan Pemerintah Daerah terhadap BUMD Air Minum tahun {{$tahun}} berdasarkan hasil pembahasan yang telah dievaluasi sebagai berikut:</li>
                  </ol>
                </td>
              </tr>
              <tr>
                <td colspan="4" style="padding-left:50px;">
                  <ol type="a">
                    @php
                    $p1=$form_1['cal']['layak']>0?'peningkatan':'penurunan';
                    @endphp
                    <li class="justify">Pencapaian akses layanan air minum layak mengalami <b>{{$p1}}</b> yaitu <b> {{number_format($form_1['cal']['layak'],2,',','.')}}%</b> dari jumlah KK  pada tahun <b>{{$form_1['cal']['tahun_x']}}</b>  hingga tahun <b>{{$form_1['cal']['tahun']}}</b> dengan uraian sebagai berikut :</li>
                  </ol>
                  <table class="table  tdata table-bordered">
                    <thead class="bg-secondary">
                      <tr>
                        <!---->
                        <th rowspan="4">Tahun</th>
                        <th rowspan="4">Jumlah KK</th>
                        <th colspan="12">Air Munum Layak</th>
                        @if(strlen($kodepemda)>3)
                         {{-- <th rowspan="4">Ketarangan</th> --}}
                        @endif
                      </tr>
                      <tr>
                        <th colspan="6">JP</th>
                        <th rowspan="3">KK Total</th>
                        <th rowspan="3">SR Total</th>
                        <th colspan="2" rowspan="2">BJP</th>
                        <th rowspan="2" colspan="2">Air Minum Layak</th>
                      </tr>
                      <tr>
                        <th colspan="3">Kontribusi JP BUMDAM</th>
                        <th colspan="3">Kontribusi JP Non BUMDAM</th>
                      </tr>
                      <tr>
                        <th>KK</th>
                        <th>SR</th>
                        <th>% (KK)</th>
                        <th>KK</th>
                        <th>SR</th>
                        <th>% (KK)</th>
                        <th>KK</th>
                        <th>%</th>
                        <th>KK</th>
                        <th>%</th>
{{-- 
                        <th>KK</th>
                        <th>%</th> --}}
                      </tr>
                    </thead>
                    <tbody> @foreach($form_1['data'] as $d) <tr>
                        <td>{{$d->tahun}}</td>
                        <td>{{number_format($d->kk,0,',','.')}}</td>
                        <td>{{number_format($d->bumdam_kk,0,',','.')}}</td>
                        <td>{{number_format($d->bumdam_sr,0,',','.')}}</td>
                        <td>{{number_format(checkZero($d->bumdam_kk,$d->kk),2,',','.')}}</td>
                        <td>{{number_format($d->nonbumdam_kk,0,',','.')}}</td>
                        <td>{{number_format($d->nonbumdam_sr,0,',','.')}}</td>
                        <td>{{number_format(checkZero($d->nonbumdam_kk,$d->kk),2,',','.')}}</td>
                        <td>{{number_format($d->bumdam_kk+$d->nonbumdam_kk,0,',','.')}}</td>
                        <td>{{number_format($d->bumdam_sr+$d->nonbumdam_sr,0,',','.')}}</td>
                        <td>{{number_format($d->bjp_kk,0,',','.')}}</td>
                        <td>{{number_format(checkZero($d->bjp_kk,$d->kk),2,',','.')}}</td>
                        <td>{{number_format($d->bumdam_kk+$d->nonbumdam_kk+$d->bjp_kk,0,',','.')}}</td>
                        <td>{{number_format(checkZero($d->bumdam_kk+$d->nonbumdam_kk+$d->bjp_kk,$d->kk),2,',','.')}}</td>
                       
                        @if(strlen($kodepemda)>3)
                        {{-- <td>{!!$d->keterangan!!}</td> --}}

                        @endif
                      </tr> @endforeach </tbody>
                  </table>

                  <table  class="table  tdata table-bordered">
                    <thead class="bg-secondary">
                      <tr>
                        <th colspan="4">
                          Evaluasi Capaian Air Minum Layak
                        </th>
                      </tr>
                      <tr>
                        <th rowspan="2" style="width:50px;">Tahun</th>
                        <th colspan="2">Belum Telayani</th>
                        <th rowspan="2">Keterangan</th>

                      </tr>
                      <tr>
                        <th>KK</th>
                        <th>%</th>

                      </tr>
                    </thead>
                    <tbody>
                      @foreach($form_1['data'] as $d)
                      <tr>
                        <td>{{$d->tahun}}</td>
                         <td>{{number_format($d->kk-($d->bumdam_kk+$d->nonbumdam_kk+$d->bjp_kk),0,',','.')}}</td>
                        <td>{{number_format(checkZero(($d->kk-($d->bumdam_kk+$d->nonbumdam_kk+$d->bjp_kk)),($d->kk)),2,',','.')}}</td>
                        <td>{!!$d->keterangan!!}</td>

                      </tr>

                      @endforeach
                    </tbody>
                  </table>
                  
                  @if(strlen($kodepemda)<=3)
                    <p>* Data Merupakan Aggregasi Dari Pemda Kab/Kota Antara Lain:</p>
                  @endif

                </td>
              </tr>
              @if(strlen($kodepemda)<=3)
              @foreach($form_1['data_turunan'] as $t)
                @if(count($t['data']))
                
                      <tr>
                          <td colspan="4">
                              <ul style="list-style-type: none;">
                                  <li>
                                    <table class="table  tdata table-bordered">
                                      <thead class="bg-secondary">
                                        <tr>
                                          <th colspan="14">{{$t['meta']->nama_pemda}}</th>
                                        </tr>
                                        <tr>
                                          <!---->
                                          <th rowspan="4">Tahun</th>
                                          <th rowspan="4">Jumlah KK</th>
                                          <th colspan="12">Air Munum Layak</th>
                                          @if(strlen($kodepemda)>3)
                                           {{-- <th rowspan="4">Ketarangan</th> --}}
                                          @endif
                                        </tr>
                                        <tr>
                                          <th colspan="6">JP</th>
                                          <th rowspan="3">KK Total</th>
                                          <th rowspan="3">SR Total</th>
                                          <th colspan="2" rowspan="2">BJP</th>
                                          <th rowspan="2" colspan="2">Air Minum Layak</th>
                                        </tr>
                                        <tr>
                                          <th colspan="3">Kontribusi JP BUMDAM</th>
                                          <th colspan="3">Kontribusi JP Non BUMDAM</th>
                                        </tr>
                                        <tr>
                                          <th>KK</th>
                                          <th>SR</th>
                                          <th>% (KK)</th>
                                          <th>KK</th>
                                          <th>SR</th>
                                          <th>% (KK)</th>
                                          <th>KK</th>
                                          <th>%</th>
                                          <th>KK</th>
                                          <th>%</th>
                  {{-- 
                                          <th>KK</th>
                                          <th>%</th> --}}
                                        </tr>
                                      </thead>

                                    <tbody> @foreach($t['data'] as $d) <tr>
                                      <td>{{$d->tahun}}</td>
                                      <td>{{number_format($d->kk,0,',','.')}}</td>
                                      <td>{{number_format($d->bumdam_kk,0,',','.')}}</td>
                                      <td>{{number_format($d->bumdam_sr,0,',','.')}}</td>
                                      <td>{{number_format(checkZero($d->bumdam_kk,$d->kk),2,',','.')}}</td>
                                      <td>{{number_format($d->nonbumdam_kk,0,',','.')}}</td>
                                      <td>{{number_format($d->nonbumdam_sr,0,',','.')}}</td>
                                      <td>{{number_format(checkZero($d->nonbumdam_kk,$d->kk),2,',','.')}}</td>
                                      <td>{{number_format($d->bumdam_kk+$d->nonbumdam_kk,0,',','.')}}</td>
                                      <td>{{number_format($d->bumdam_sr+$d->nonbumdam_sr,0,',','.')}}</td>
                                      <td>{{number_format($d->bjp_kk,0,',','.')}}</td>
                                      <td>{{number_format(checkZero($d->bjp_kk,$d->kk),2,',','.')}}</td>
                                      <td>{{number_format($d->bumdam_kk+$d->nonbumdam_kk+$d->bjp_kk,0,',','.')}}</td>
                                      <td>{{number_format(checkZero($d->bumdam_kk+$d->nonbumdam_kk+$d->bjp_kk,$d->kk),2,',','.')}}</td>
                                     
                                      @if(strlen($kodepemda)>3)
                                      {{-- <td>{!!$d->keterangan!!}</td> --}}
              
                                      @endif
                                    </tr> @endforeach </tbody>
                                </table>
              
                                <table  class="table  tdata table-bordered">
                                  <thead class="bg-secondary">
                                    <tr>
                                      <th colspan="4">
                                        Evaluasi Capaian Air Minum Layak ({{$t['meta']->nama_pemda}})
                                      </th>
                                    </tr>
                                    <tr>
                                      <th rowspan="2" style="width:50px;">Tahun</th>
                                      <th colspan="2">Belum Telayani</th>
                                      <th rowspan="2">Keterangan</th>
              
                                    </tr>
                                    <tr>
                                      <th>KK</th>
                                      <th>%</th>
              
                                    </tr>
                                  </thead>
                                  <tbody>
                                    @foreach($t['data'] as $d)
                                    <tr>
                                      <td>{{$d->tahun}}</td>
                                       <td>{{number_format($d->kk-($d->bumdam_kk+$d->nonbumdam_kk+$d->bjp_kk),0,',','.')}}</td>
                                      <td>{{number_format(checkZero(($d->kk-($d->bumdam_kk+$d->nonbumdam_kk+$d->bjp_kk)),($d->kk)),2,',','.')}}</td>
                                      <td>{!!$d->keterangan!!}</td>
              
                                    </tr>
              
                                    @endforeach
                                  </tbody>
                                </table>
                                    
                              
                                  </li>
                              </ul >
                          </td>
                        </tr>
                  
                @endif

              @endforeach


              @endif

              <tr>
                <td colspan="4" style="padding-left:50px;">
                  <ol type="a" start="2">
                    @php
                        $p1=$form_2['cal']['sink_anggaran'];
                    @endphp
                    <li class="justify">Pemda {{$nama_pemda}} pada Tahun {{$form_2['cal']['tahun']}} telah berkomitmen dalam perencanaan penyusunan program dan kegiatan Pengelolaan Air Minum Sebesar <b>Rp. {{number_format($form_2['cal']['rkpd'],0,',','.')}}</b> dari  anggaran dinas/urusan,
                        serta telah melaksanakan penganggaran <b>Rp. {{number_format($form_2['cal']['persentase_ag'],2,',','.')}}%</b> atau <b>Rp. {{number_format($form_2['cal']['anggaran'],0,',','.')}}</b> dari total APBD dengan uraian sebagai berikut: </li>
                  </ol>
                  <table class="table  tdata table-bordered">
                    <thead class="bg-secondary">
                      <tr>
                        <th rowspan="3">Tahun</th>
                        <th colspan="6">Program Kegiatan Pengelolaan Air Minum Daerah</th>
                        <th>Total Anggaran</th>
                        <th>% SPAM dari APBD </th>
                        <th rowspan="3">Keterangan </th>
                      </tr>
                      <tr>
                        <th colspan="2">Perencanaan (RKPD)</th>
                        <th colspan="2">Anggaran (APBD)</th>
                        <th colspan="2">Sinkronisasi</th>
                        <th rowspan="2">Jumlah</th>
                        <th rowspan="2">%</th>
                      </tr>
                      <tr>
                        <th>Jumlah Sub / kegiatan</th>
                        <th>Pagu Indikatif</th>
                        <th>Jumlah Sub / kegiatan</th>
                        <th>Anggaran</th>
                        <th>Gap Keg / Sub Kegiatan</th>
                        <th>Gap Pagu Indikatif &amp; Anggaran</th>
                      </tr>
                    </thead>
                        <tbody> @foreach($form_2['data'] as $d) <tr>
                            @php
                            // dd($d);
                            @endphp
                            <td>{{$d->tahun}}</td>
                            <td>{{number_format($d->rkpd_ar_sub,0,',','.')}}</td>
                            <td>{{number_format($d->rkpd_ar_ag,0,',','.')}}</td>
                            <td>{{number_format($d->apbd_ar_sub,0,',','.')}}</td>
                            <td>{{number_format($d->apbd_ar_ag,0,',','.')}}</td>
                            <td>{{number_format($d->apbd_ar_sub-$d->rkpd_ar_sub,0,',','.')}}</td>
                            <td>{{number_format($d->apbd_ar_ag-$d->rkpd_ar_ag,0,',','.')}}</td>
                            <td>{{number_format($d->apbd_tot,0,',','.')}}</td>
                            <td>{{number_format(checkZero($d->apbd_ar_ag,$d->apbd_tot),2,',','.')}}</td>

                          
                            <td>{!!$d->keterangan!!}</td>
                          </tr> @endforeach </tbody>
                  </table>
                </td>
              </tr>
              @php
              $p_op=2;
            @endphp
              @if(isset($form_3['STIMULAN']))
              <tr>
                @php
                    $p_op+=1;
                @endphp
                <td colspan="4" style="padding-left:50px;">
                  <ol type="a" start="{{$p_op}}">
                    <li  class="justify">Dukungan Pemda {{$nama_pemda}} dalam pemenuhan DDUB telah merencanakan total DDUB sebesar <b>Rp. {{number_format($form_3['STIMULAN']['cal']['perencanaan'],0,',','.')}}</b> dan telah terealisasi sampai dengan <b>{{$form_3['STIMULAN']['cal']['tahun']}}</b> sebasar <b>Rp. {{number_format($form_3['STIMULAN']['cal']['realisasi'],0,',','.')}}</b>, dengan uraian sebagai berikut:</li>
                  </ol>
                  <table class="table tdata table-bordered">
                    <thead class="bg-secondary">
                      <tr>
                        <th rowspan="3">Tahun</th>
                        <th colspan="6">Pemenuhan DDUB Dalam Dukungan Serta Capaian SR</th>
                        <th colspan="3">Pemetaan Dukungan Pemda</th>
                        <th rowspan="2">Laba Bersih</th>
                        <th rowspan="3">Keterangan</th>
                      </tr>
                      <tr>
                        <th colspan="2">Rencana</th>
                        <th colspan="2">Realisasi</th>
                        <th colspan="2">Deviasi</th>
                        <th >Penyertaan Modal</th>
                        <th >Infrastruktur</th>
                        <th >Jumlah</th>




                      </tr>
                      <tr>
                        <th>RP.</th>
                        <th>SR</th>
                        <th>RP.</th>
                        <th>SR</th>
                        <th>RP.</th>
                        <th>SR</th>
                        <th>RP.</th>
                        <th>RP.</th>
                        <th>RP.</th>
                        <th>RP.</th>

                      </tr>
                    </thead>
                      
                        <tbody> @foreach($form_3['STIMULAN']['data'] as $d) 
                      
                          <tr>
                        
                          
                            <td>{{$d->tahun}}</td>
                            <td>{{number_format($d->rencana_ag,0,',','.')}}</td>
                            <td>{{number_format($d->rencana_sr,0,',','.')}}</td>
                            <td>{{number_format($d->realisasi_ag,0,',','.')}}</td>
                            <td>{{number_format($d->realisasi_sr,0,',','.')}}</td>
                            <td>{{number_format($d->rencana_ag-$d->realisasi_ag,0,',','.')}}</td>
                            <td>{{number_format($d->rencana_sr-$d->realisasi_sr,0,',','.')}}</td>
                            <td>{{number_format($d->rencana_modal,0,',','.')}}</td>
                            <td>{{number_format($d->rencana_modal_infra,0,',','.')}}</td>
                            <td>{{number_format($d->rencana_modal_infra+$d->rencana_modal,0,',','.')}}</td>
                            <td>{{number_format($d->laba_bersih,0,',','.')}}</td>


                            <td>{!!$d->keterangan!!}</td>
                          </tr> @endforeach </tbody>
                  </table>
                </td>
              </tr>
              @endif
              @if(isset($form_3['PENDAMPING']))
              <tr>
                @php
                    $p_op+=1;
                @endphp
                <td colspan="4" style="padding-left:50px;">
                  <ol type="a" start="{{$p_op}}">
                    <li  class="justify">Dukungan Pemda {{$nama_pemda}} dalam pemenuhan Dana Pendamping telah merencanakan total Dana Pendamping sebesar <b>Rp. {{number_format($form_3['PENDAMPING']['cal']['perencanaan'],0,',','.')}}</b> dan telah terealisasii sampai dengan <b>{{$form_3['PENDAMPING']['cal']['tahun']}}</b> sebasar <b>Rp. {{number_format($form_3['PENDAMPING']['cal']['realisasi'],0,',','.')}}</b>, dengan uraian sebagai berikut:</li>
                  </ol>
                  <table class="table tdata table-bordered">
                    <thead class="bg-secondary">
                      <tr>
                        <th rowspan="3">Tahun</th>
                        <th colspan="6">Pemenuhan Dana Pendamping Dalam Dukungan Serta Capaian SR</th>
                        <th colspan="3">Pemetaan Dukungan Pemda</th>
                        <th rowspan="2">Laba Bersih</th>
                        <th rowspan="3">Keterangan</th>
                      </tr>
                      <tr>
                        <th colspan="2">Rencana</th>
                        <th colspan="2">Realisasi</th>
                        <th colspan="2">Deviasi</th>
                        <th >Penyertaan Modal</th>
                        <th >Infrastruktur</th>
                        <th >Jumlah</th>
                      </tr>
                      <tr>
                        <th>RP.</th>
                        <th>SR</th>
                        <th>RP.</th>
                        <th>SR</th>
                        <th>RP.</th>
                        <th>SR</th>
                        <th>RP.</th>
                        <th>RP.</th>
                        <th>RP.</th>
                        <th>RP.</th>

                      </tr>
                    </thead>
                        <tbody> @foreach($form_3['PENDAMPING']['data'] as $d) <tr>
                          
                            <td>{{$d->tahun}}</td>
                            <td>{{number_format($d->rencana_ag,0,',','.')}}</td>
                            <td>{{number_format($d->rencana_sr,0,',','.')}}</td>
                            <td>{{number_format($d->realisasi_ag,0,',','.')}}</td>
                            <td>{{number_format($d->realisasi_sr,0,',','.')}}</td>
                            <td>{{number_format($d->rencana_ag-$d->realisasi_ag,0,',','.')}}</td>
                            <td>{{number_format($d->rencana_sr-$d->realisasi_sr,0,',','.')}}</td>
                            <td>{{number_format($d->rencana_modal,0,',','.')}}</td>
                            <td>{{number_format($d->rencana_modal_infra,0,',','.')}}</td>
                            <td>{{number_format($d->rencana_modal_infra+$d->rencana_modal,0,',','.')}}</td>
                            <td>{{number_format($d->laba_bersih,0,',','.')}}</td>
                            <td>{!!$d->keterangan!!}</td>
                          </tr> @endforeach </tbody>
                  </table>
                </td>
              </tr>
              @endif
            @foreach($form_4['data'] as $key=>$data)
            <tr>
                @php
                    $p_op+=1;
                @endphp
                <td colspan="4" style="padding-left:50px;">
                  <ol type="a" start="{{$p_op}}">
                    <li  class="justify">Tarif air minum (FCR dan Non-FCR) merupakan target  {{$data['meta']->nama_bu}} yang harus didukung oleh kebijakan Pemda, Pemda {{$nama_pemda}} Pada Tahun <b>{{$form_4['cal'][$key]['tahun']}}</b>  {!!$form_4['cal'][$key]['subsidi']?'telah memberikan  subsidi sebesar <b>Rp. '.number_format($form_4['cal'][$key]['subsidi'],0,',','.').'</b>':''!!}, dengan uraian sebagai berikut :</li>
                  </ol>
                  <table class="table tdata table-bordered">
                    <thead class="bg-secondary">
                      <tr>
                        <th rowspan="2">Tahun</th>
                        <th colspan="4">Dukungan Kebijakan PEMDA Dalam Pemenuhan Tarif Air Minum (FCR) {{$data['meta']->nama_bu}}</th>
                        <th rowspan="2">Keterangan</th>
                      </tr>
                      <tr>
                        <th> % FCR </th>
                        <th> Klasifikasi FCR/NON FCR </th>
                        <th> Subsidi Tarif (Ada,Tidak Ada) </th>
                        <th> Besaran Subsidi </th>
                      </tr>
                    </thead>
                    <tbody> @foreach($data['data'] as $d) <tr>
                          @php
                          @endphp
                        <td>{{$d->tahun}}</td>
                        <td>{{number_format($d->nilai_fcr,0,',','.')}}</td>
                        <td>{{$d->fcr?'FCR':'NONFCR'}}</td>
                        <td>{{$d->subsidi?'Ada':'-'}}</td>
                        <td>{{$d->subsidi?number_format($d->nilai_subsidi,0,',','.'):'-'}}</td>
                        <td>{!!$d->keterangan!!}</td>
                      </tr> @endforeach </tbody>
                  </table>
                </td>
              </tr>

            @endforeach

           
              <tr>
                <td colspan="4" style="padding-left:50px;">
                  <ol type="a" start="{{$p_op+1}}">
                    <li  class="justify">Sebagai syarat pemenuhan Readiness Criteria Pemerintah Daerah dalam keikutsertaan  Program NUWSP, berikut status, tindak lanjut dan isu strategis yang merupakan komitmen dan konsistensi pemda :</li>
                  </ol>
                  <table class="table tdata table-bordered">
                    <thead class="bg-secondary">
                      <tr>
                        <th colspan="6">RC PEMDA Dalam Komitmen dan Konsistensi Dalam Program NUWSP</th>
                        <th rowspan="3">Keterangan</th>
                      </tr>
                      <tr>
                        <th rowspan="2" style="max-width: 400px;">Item</th>
                        <th style="width:30%;" colspan="5">Indentifikasi</th>
                      </tr>
                      <tr>
                        <th>Ada/Tidak Ada</th>
                        <th>Tanggal Pemenuhan</th>
                        <th>Target Pemenuhan</th>
                        <th>Isu Strategis / Permasalahan</th>
                        <th>RKTL</th>
                      </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="width:30%;"  >RC-5; RKAP BUMDAM, Rencana Bisnis BUMDAM, dan RISPAM Kabupaten/Kota yang sudah mengakomodir usulan kegiatan NUWSP</td>
                            <td >{{$form_6['data']['rc5']?'Ada':'Tidak Ada'}}</td>
                            <td >{{$form_6['data']['rc5']?$form_6['data']['rc5_date_done']:'-'}}</td>
                            <td >{{$form_6['data']['rc5']?'-':$form_6['data']['rc5_date_target']}}</td>
                            <td >{!!$form_6['data']['rc5_isu']!!}</td>
                            <td >{!!$form_6['data']['rc5_rktl']!!}</td>
                            <td >{!!$form_6['data']['rc5_keterangan']!!}</td>
                        </tr>
                        <tr>
                            <td style="width:30%;"  >RC-7; Dana Daerah Urusan Bersama (DDUB)</td>
                            <td >{{$form_6['data']['rc7']?'Ada':'Tidak Ada'}}</td>
                            <td >{{$form_6['data']['rc7']?$form_6['data']['rc7_date_done']:'-'}}</td>
                            <td >{{$form_6['data']['rc7']?'-':$form_6['data']['rc7_date_target']}}</td>
                            <td >{!!$form_6['data']['rc7_isu']!!}</td>
                            <td >{!!$form_6['data']['rc7_rktl']!!}</td>
                            <td >{!!$form_6['data']['rc7_keterangan']!!}</td>
                        </tr>
                        <tr>
                            <td style="width:30%;"  >RC-12; Surat Pernyataan Komitmen penyediaan dana pembangunan Jaringan Distribusi Sekunder dan Tersier, disetujui oleh Bupati/Walikota dan Direktur BUMDAM</td>
                            <td >{{$form_6['data']['rc12']?'Ada':'Tidak Ada'}}</td>
                            <td >{{$form_6['data']['rc12']?$form_6['data']['rc12_date_done']:'-'}}</td>
                            <td >{{$form_6['data']['rc12']?'-':$form_6['data']['rc12_date_target']}}</td>
                            <td >{!!$form_6['data']['rc12_isu']!!}</td>
                            <td >{!!$form_6['data']['rc12_rktl']!!}</td>
                            <td >{!!$form_6['data']['rc12_keterangan']!!}</td>
                        </tr>
                        <tr>
                            <td style="width:30%;"  >RC-14; Surat Kesediaan Menerima Hibah Barang Milik Negara (BMN)</td>
                            <td >{{$form_6['data']['rc14']?'Ada':'Tidak Ada'}}</td>
                            <td >{{$form_6['data']['rc14']?$form_6['data']['rc14_date_done']:'-'}}</td>
                            <td >{{$form_6['data']['rc14']?'-':$form_6['data']['rc14_date_target']}}</td>
                            <td >{!!$form_6['data']['rc14_isu']!!}</td>
                            <td >{!!$form_6['data']['rc14_rktl']!!}</td>
                            <td >{!!$form_6['data']['rc14_keterangan']!!}</td>
                        </tr>
                        <tr>
                            <td style="width:30%;" >RC-16; Salinan Dokumen Pelaksanaan Anggaran (DPA) / Peraturan Daerah Penyertaan Modal / RKAP PDAM Tahun Anggaran berjalan untuk kepastian pelaksanaan kegiatan DDUB (Bantuan Stimulan) dan/atau pembangunan Jaringan Distribusi Bagi (JDB) (Bantuan Pendamping)</td>
                            <td >{{$form_6['data']['rc16']?'Ada':'Tidak Ada'}}</td>
                            <td >{{$form_6['data']['rc16']?$form_6['data']['rc16_date_done']:'-'}}</td>
                            <td >{{$form_6['data']['rc16']?'-':$form_6['data']['rc16_date_target']}}</td>
                            <td >{!!$form_6['data']['rc16_isu']!!}</td>
                            <td >{!!$form_6['data']['rc16_rktl']!!}</td>
                            <td >{!!$form_6['data']['rc16_keterangan']!!}</td>
                        </tr>
                        <tr>
                            <td style="width:30%;" >RC-22; Dokumen lengkap perjanjian Kerjasama antara BUMDAM dengan pihak ketiga</td>
                            <td >{{$form_6['data']['rc22']?'Ada':'Tidak Ada'}}</td>
                            <td >{{$form_6['data']['rc22']?$form_6['data']['rc22_date_done']:'-'}}</td>
                            <td >{{$form_6['data']['rc22']?'-':$form_6['data']['rc22_date_target']}}</td>
                            <td >{!!$form_6['data']['rc22_isu']!!}</td>
                            <td >{!!$form_6['data']['rc22_rktl']!!}</td>
                            <td >{!!$form_6['data']['rc22_keterangan']!!}</td>
                        </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
           
            </tbody>
          </table>
          <div class="break_before"></div>
          <table >
            <thead>
              <tr>
                <th colspan="3" style="text-align: center">
                  <img src="{{public_path('assets/img/brand/head.png')}}" style="width:100%;" alt="">
                </th>
              </tr>
            </thead>
            <tbody>
              <tr >
                <td colspan="3">
                  <p  class="justify">Demikian berita acara ini disusun untuk menjadi informasi seluruh stakeholder terkait dan diharapkan dapat dipergunakan sebagai bahan evaluasi sebagaimana mestinya sehingga dapat memberikan rekomendasi peningkatan capaian terhadap target Nasional dan Pemda dalam pencapaian akses jaringan air minum perpipaan.</p>
                </td>
              </tr>
              <tr>
                <td colspan="3">
                  <p  class="justify">Koordinasi dan informasi selanjutnya terkait hasil pelaksanaan desk, kelengkapan berita acara, maupun pengelolaan NUWSP di daerah secara umum dapat dilayangkan melalui email <a href="mailto:perkim.bangda@gmail.com">perkim.bangda@gmail.com</a>
                  </p>
                  <br>
                  <br>
                </td>
              </tr>
              <tr>
                <td style="text-align: center">
                  <p style="margin:0; padding:0; line-height:12px;" style="font-size:12px;">Kasubdit Perumahan dan Kawasan Permukiman</p>
		  <p style="margin:0;padding:0;" style="font-size: 12px; line-height:12px;">	Ditjen Bina Pembangunan Daerah
						Kementerian Dalam Negeri</p>
                  <br>
                  <br>
                  <br>
                  <p style="margin:0;padding:0;">
                    <u><small>{{($NAMA_PEMANGKU_BANGDA)}}</small></u>
                  </p>
                  <p style="margin:0;padding:0;"><small>NIP. {{$NIP_PEMANGKU_BANGDA}}</small></p>
                </td>
                <td style="width:50px;"></td>
                <td style="text-align: center">
                  <p style="margin:0;padding:0; text-transform: capitalize;">Pemda {{$nama_pemda}}</p>
                  <p style="margin:0;padding:0;">
                    <small>{{$date[0]}} {{$date[1]}} {{$date[2]}} {{$date[3]}}</small>
                  </p>
                  <br>
                  <br>
                  <br>
                  <p style="margin:0;padding:0;">
                    <u><small>{{($NAMA_PEMANGKU_PEMDA)}}</small></u>
                  </p>
                  <p style="margin:0;padding:0;"><small>NIP. {{$NIP_PEMANGKU_PEMDA}}</small></p>
                </td>
              </tr>
            </tbody>
          </table>

          @if($have_catatan)
          <table class="break_before" >
            <thead>
                <tr>
                    <th  style="text-align: center">
                        <img src="{{public_path('assets/img/brand/head.png')}}" style="width:100%;" alt="">
                      </th>
                </tr>
                <tr>
                    <th style="text-align: center">
                        <p><b>LAMPIRAN CATATAN</b></p>
                    </th>
                </tr>

            </thead>
            <tbody>
                @foreach($catatan as $kc=>$c)
                @if($c['catatan'])
                <tr>
                    <td>
                        <p><b>{{$c['title']}}</b></p>
                        <p style="padding-left:50px; margin:0;">{!!$c['catatan']!!}</p>
                    </td>
                </tr>
                @endif
                @endforeach
            </tbody>
          </table>
          @endif
      
        
    </body>
</html>