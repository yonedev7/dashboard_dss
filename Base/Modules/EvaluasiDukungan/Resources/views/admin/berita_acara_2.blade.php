<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=0.1, shrink-to-fit=yes">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <style>
          
            @page {
                margin: 150px 30px 15px 55px;
            }

            .break_before {page-break-before: always!important}

            #catatan_list .catatan_detail{
                text-align: justify;
            }

            #catatan_list ol li{
                margin-bottom:7px;
            }

            .break-posible{
                page-break-before:auto !important;
            }

            .justify{
              text-align: justify;
               text-justify: inter-word;
            }
            .tematik{
                padding-left:50px;
            }
            .underline-content{
               
                padding-left:20px;
                padding-right: 20px;
                position: relative;

            }
            .underline{
                border-bottom: 1px solid #222;
                bottom:10px;
            }

            .ttd{
                height:80px;
               
            }

            #pendahuluan ol{
                margin-bottom:0px;
                margin-top:0px;
            }
            #pendahuluan ol li{
                margin-bottom: 10px;
            }
            /* .break_before {page-break-before: always!important} */

            h1,h2,h3,h4,h5,p,li,td,th,*{
                font-family:Verdana, Geneva, Tahoma, sans-serif
            }
            table, tr, td, th, tbody, thead, tfoot {
                page-break-inside: avoid !important;
            }
            .tdata td,.tdata th{
                font-size: 10px!important;
                border-left:1px solid #ddd;
                border-bottom:1px solid #ddd;
                padding: 5px;

            }
            .table {
                display: table;
                border-spacing: 0px;
                border: 1px solid #ddd;
                width:100%;
            }
            .bg-secondary{
                background-color: #f1f1f1;
            }

        </style>
        @php
        $p_op=2;
        function checkZero($a,$b){
            if($a==0 OR $b==0){
                return 0;
            }else{
                return ($a/$b)*100;
            }
        }

        @endphp
    </head>
    <body>
        <div class="" style="position:fixed;  top:-145px; width:100%;">
             <img src="{{public_path('assets/img/brand/head.png')}}" style="width:100%;" alt="">
        </div>
        <div>
            <div style="text-align: center">
                <h3 style="margin:0;padding:0;">BERITA ACARA</h3>
                <p style="margin:0;padding:0;">
                  <b>Evaluasi Dukungan Pemerintah Daerah Terhadap Program Air Minum</b>
                </p>
                <p style="margin:0;padding:0;">
                  <b>Dalam Kerangka National Urban Water Supply Project (NUWSP) Tahun Anggaran {{$tahun}}</b>
                </p>
            </div>
            <div id="pendahuluan" class="justify">
                <p>&nbsp;&nbsp;&nbsp; Pada hari ini, {{$date[0]}} tanggal {{$date[1]}} bulan {{$date[2]}} Tahun {{$date[3]}}, telah dilaksanakan Forum Desk Evaluasi Dukungan Pemerintah Daerah terhadap Program Air Minum dalam Kerangka NUWSP bersama pemerintah daerah {{$nama_pemda}} dengan rincian hasil pembahasan sebagai berikut:</p>
                <ol>
                    <li>Sudah dilakukan pembahasan desk untuk melihat capaian dan evaluasi terhadap kinerja, menggali permasalahan pelaksanaan program dan kegiatan serta menyusun tindak lanjut kegiatan pemerintah daerah untuk mendukung BUMD Air Minum dalam kerangka NUWSP.</li>
                    <li>Hasil Evaluasi Dukungan Pemerintah Daerah terhadap Program Air Minum dalam Kerangka NUWSP sebagaimana dimaksud pada poin 1 adalah sebagaimana terlampir.</li>
                    <li>Pemetaan Dukungan Pemerintah Daerah terhadap BUMD Air Minum tahun {{$tahun}} berdasarkan hasil pembahasan yang telah dievaluasi sebagai berikut:</li>
                </ol>
            </div>

            <div id="tematik_1" class="tematik">
                <ol type="a" start="1">
                    @php
                    if($form_1['cal']['layak']>0){
                        $p1='peningkatan';
                    }elseif($form_1['cal']['layak']<0){
                        $p1='penurunan';
                    }else{
                        $p1='staknan capaian';
                    }
                    @endphp
                    <li>
                        
                        <p class="justify">Pencapaian akses layanan air minum layak mengalami <b>{{$p1}}</b> {!!$p1!='staknan capaian'?' yaitu sebesar <b>'.number_format($form_1['cal']['layak'],2,',','.').'%</b> dari jumlah KK':''!!} pada tahun  {!!($form_1['cal']['tahun_x']!=$form_1['cal']['tahun'])?' <b>'.$form_1['cal']['tahun_x'].'</b> hingga tahun <b>'.$form_1['cal']['tahun'].'</b>':' <b>'.$form_1['cal']['tahun_x'].'</b>'!!}</b> dengan uraian sebagai berikut :</p>
                        <table class="table  tdata table-bordered">
                            <thead class="bg-secondary">
                              <tr>
                                <!---->
                                <th rowspan="4">Tahun</th>
                                <th rowspan="4">Jumlah KK</th>
                                <th colspan="12">Air Munum Layak</th>
                                @if(strlen($kodepemda)>3)
                                 {{-- <th rowspan="4">Ketarangan</th> --}}
                                @endif
                              </tr>
                              <tr>
                                <th colspan="6">JP</th>
                                <th rowspan="3">KK Total</th>
                                <th rowspan="3">SR Total</th>
                                <th colspan="2" rowspan="2">BJP</th>
                                <th rowspan="2" colspan="2">Air Minum Layak</th>
                              </tr>
                              <tr>
                                <th colspan="3">Kontribusi JP BUMDAM</th>
                                <th colspan="3">Kontribusi JP Non BUMDAM</th>
                              </tr>
                              <tr>
                                <th>KK</th>
                                <th>SR</th>
                                <th>% (KK)</th>
                                <th>KK</th>
                                <th>SR</th>
                                <th>% (KK)</th>
                                <th>KK</th>
                                <th>%</th>
                                <th>KK</th>
                                <th>%</th>
        
                              </tr>
                              <tr>
                                <th>1</th>
                                <th>2</th>
                                <th>3</th>
                                <th>4</th>
                                <th>5</th>
                                <th>6</th>
                                <th>7</th>
                                <th>8</th>
                                <th>9</th>
                                <th>10</th>
                                <th>11</th>
                                <th>12</th>
                                <th>13</th>
                                <th>14</th>


                              </tr>
                            </thead>
                            <tbody> @foreach($form_1['data'] as $d) <tr>
                                <td>{{$d->tahun}}</td>
                                <td>{{number_format($d->kk,0,',','.')}}</td>
                                <td>{{number_format($d->bumdam_kk,0,',','.')}}</td>
                                <td>{{number_format($d->bumdam_sr,0,',','.')}}</td>
                                <td>{{number_format(checkZero($d->bumdam_kk,$d->kk),2,',','.')}}</td>
                                <td>{{number_format($d->nonbumdam_kk,0,',','.')}}</td>
                                <td>{{number_format($d->nonbumdam_sr,0,',','.')}}</td>
                                <td>{{number_format(checkZero($d->nonbumdam_kk,$d->kk),2,',','.')}}</td>
                                <td>{{number_format($d->bumdam_kk+$d->nonbumdam_kk,0,',','.')}}</td>
                                <td>{{number_format($d->bumdam_sr+$d->nonbumdam_sr,0,',','.')}}</td>
                                <td>{{number_format($d->bjp_kk,0,',','.')}}</td>
                                <td>{{number_format(checkZero($d->bjp_kk,$d->kk),2,',','.')}}</td>
                                <td>{{number_format($d->bumdam_kk+$d->nonbumdam_kk+$d->bjp_kk,0,',','.')}}</td>
                                <td>{{number_format(checkZero($d->bumdam_kk+$d->nonbumdam_kk+$d->bjp_kk,$d->kk),2,',','.')}}</td>
                               
                               
                              </tr> @endforeach </tbody>
                        </table>

                     
                    
                    
                        </li>
                </ol>
                
                <div style="padding-left:40px;">
                    @if(strlen($kodepemda)<=3)
                    @foreach($form_1['data_turunan'] as $t)
                        @if(count($t['data']))
                            <table class="table tdata table-bordered break-posible" style="margin-bottom:10px;" >
                                    <thead class="bg-secondary">
                                    <tr>
                                        <th colspan="14">{{$t['meta']->nama_pemda}}</th>
                                    </tr>
                                    <tr>
                                        <th rowspan="4">Tahun</th>
                                        <th rowspan="4">Jumlah KK</th>
                                        <th colspan="12">Air Munum Layak</th>
                                        @if(strlen($kodepemda)>3)
                                        @endif
                                    </tr>
                                    <tr>
                                        <th colspan="6">JP</th>
                                        <th rowspan="3">KK Total</th>
                                        <th rowspan="3">SR Total</th>
                                        <th colspan="2" rowspan="2">BJP</th>
                                        <th rowspan="2" colspan="2">Air Minum Layak</th>
                                    </tr>
                                    <tr>
                                        <th colspan="3">Kontribusi JP BUMDAM</th>
                                        <th colspan="3">Kontribusi JP Non BUMDAM</th>
                                    </tr>
                                    <tr>
                                        <th>KK</th>
                                        <th>SR</th>
                                        <th>% (KK)</th>
                                        <th>KK</th>
                                        <th>SR</th>
                                        <th>% (KK)</th>
                                        <th>KK</th>
                                        <th>%</th>
                                        <th>KK</th>
                                        <th>%</th>
                                    </tr>
                                    <tr>
                                        <th>1</th>
                                        <th>2</th>
                                        <th>3</th>
                                        <th>4</th>
                                        <th>5</th>
                                        <th>6</th>
                                        <th>7</th>
                                        <th>8</th>
                                        <th>9</th>
                                        <th>10</th>
                                        <th>11</th>
                                        <th>12</th>
                                        <th>13</th>
                                        <th>14</th>
        
        
                                      </tr>
                                    </thead>
    
                                    <tbody> @foreach($t['data'] as $d) <tr>
                                        <td>{{$d->tahun}}</td>
                                        <td>{{number_format($d->kk,0,',','.')}}</td>
                                        <td>{{number_format($d->bumdam_kk,0,',','.')}}</td>
                                        <td>{{number_format($d->bumdam_sr,0,',','.')}}</td>
                                        <td>{{number_format(checkZero($d->bumdam_kk,$d->kk),2,',','.')}}</td>
                                        <td>{{number_format($d->nonbumdam_kk,0,',','.')}}</td>
                                        <td>{{number_format($d->nonbumdam_sr,0,',','.')}}</td>
                                        <td>{{number_format(checkZero($d->nonbumdam_kk,$d->kk),2,',','.')}}</td>
                                        <td>{{number_format($d->bumdam_kk+$d->nonbumdam_kk,0,',','.')}}</td>
                                        <td>{{number_format($d->bumdam_sr+$d->nonbumdam_sr,0,',','.')}}</td>
                                        <td>{{number_format($d->bjp_kk,0,',','.')}}</td>
                                        <td>{{number_format(checkZero($d->bjp_kk,$d->kk),2,',','.')}}</td>
                                        <td>{{number_format($d->bumdam_kk+$d->nonbumdam_kk+$d->bjp_kk,0,',','.')}}</td>
                                        <td>{{number_format(checkZero($d->bumdam_kk+$d->nonbumdam_kk+$d->bjp_kk,$d->kk),2,',','.')}}</td>
                                    
                                       
                                    </tr>
                                @endforeach 
                                </tbody>
                            </table>
                        @endif
                    @endforeach
    
                @endif
                </div>


                </div>
       
            </div>

            <div id="tematik2" class="tematik">
                <ol type="a" start="2">
                    @php
                        $p1=$form_2['cal']['sink_anggaran'];
                    @endphp
                    <li class="justify">Pemda {{$nama_pemda}} pada Tahun <b>{{$form_2['cal']['tahun']}}</b> telah berkomitmen dalam perencanaan penyusunan program dan kegiatan Pengelolaan Air Minum Sebesar <b>Rp. {{number_format($form_2['cal']['rkpd'],0,',','.')}}</b> dari  anggaran dinas / urusan,
                        serta telah melaksanakan penganggaran <b>Rp. {{number_format($form_2['cal']['persentase_ag'],2,',','.')}}%</b> atau <b>Rp. {{number_format($form_2['cal']['anggaran'],0,',','.')}}</b> dari total APBD dengan uraian sebagai berikut: </li>
                  </ol>

                  <div style="padding-left:40px;">
                    <table class="table  tdata table-bordered">
                        <thead class="bg-secondary">
                          <tr>
                            <th rowspan="3">Tahun</th>
                            <th colspan="6">Program Kegiatan Pengelolaan Air Minum Daerah</th>
                            <th>Total Anggaran</th>
                            <th>% SPAM dari APBD </th>
                          </tr>
                          <tr>
                            <th colspan="2">Perencanaan (RKPD)</th>
                            <th colspan="2">Anggaran (APBD)</th>
                            <th colspan="2">Sinkronisasi</th>
                            <th rowspan="2">Jumlah</th>
                            <th rowspan="2">%</th>
                          </tr>
                          <tr>
                            <th>Jumlah Sub / kegiatan</th>
                            <th>Pagu Indikatif</th>
                            <th>Jumlah Sub / kegiatan</th>
                            <th>Anggaran</th>
                            <th>Gap Keg / Sub Kegiatan</th>
                            <th>Gap Pagu Indikatif &amp; Anggaran</th>
                          </tr>
                        </thead>
                            <tbody> @foreach($form_2['data'] as $d) <tr>
                                @php
                                // dd($d);
                                @endphp
                                <td>{{$d->tahun}}</td>
                                <td>{{number_format($d->rkpd_ar_sub,0,',','.')}}</td>
                                <td>{{number_format($d->rkpd_ar_ag,0,',','.')}}</td>
                                <td>{{number_format($d->apbd_ar_sub,0,',','.')}}</td>
                                <td>{{number_format($d->apbd_ar_ag,0,',','.')}}</td>
                                <td>{{number_format($d->apbd_ar_sub-$d->rkpd_ar_sub,0,',','.')}}</td>
                                <td>{{number_format($d->apbd_ar_ag-$d->rkpd_ar_ag,0,',','.')}}</td>
                                <td>{{number_format($d->apbd_tot,0,',','.')}}</td>
                                <td>{{number_format(checkZero($d->apbd_ar_ag,$d->apbd_tot),2,',','.')}}</td>
    
                              
                              </tr> @endforeach </tbody>
                      </table>
                </div>


            </div>

            <div id="tematik3" class="tematik">
                @if(isset($form_3['STIMULAN']))
                @php
                    $p_op+=1;
                @endphp
                <ol type="a" start="{{$p_op}}">
                    <li  class="justify">Dukungan Pemda {{$nama_pemda}} dalam pemenuhan DDUB telah merencanakan total DDUB sebesar <b>Rp. {{number_format($form_3['STIMULAN']['cal']['perencanaan'],0,',','.')}}</b> dan telah terealisasi sampai dengan <b>{{$form_3['STIMULAN']['cal']['tahun']}}</b> sebesar <b>Rp. {{number_format($form_3['STIMULAN']['cal']['realisasi'],0,',','.')}}</b>, dengan uraian sebagai berikut:</li>
                </ol>
                <div style="padding-left:40px;">
                    <table class="table tdata table-bordered">
                        <thead class="bg-secondary">
                          <tr>
                            <th rowspan="3">Tahun</th>
                            <th colspan="6">Pemenuhan DDUB Dalam Dukungan Serta Capaian SR</th>
                            <th colspan="3">Pemetaan Dukungan Pemda</th>
                          </tr>
                          <tr>
                            <th colspan="2">Rencana</th>
                            <th colspan="2">Realisasi</th>
                            <th colspan="2">Deviasi</th>
                            <th >Penyertaan Modal</th>
                            <th >Infrastruktur</th>
                            <th >Jumlah</th>
    
    
    
    
                          </tr>
                          <tr>
                            <th>RP.</th>
                            <th>SR</th>
                            <th>RP.</th>
                            <th>SR</th>
                            <th>RP.</th>
                            <th>SR</th>
                            <th>RP.</th>
                            <th>RP.</th>
                            <th>RP.</th>
    
                          </tr>
                        </thead>
                          
                            <tbody> @foreach($form_3['STIMULAN']['data'] as $d) 
                          
                              <tr>
                            
                              
                                <td>{{$d->tahun}}</td>
                                <td>{{number_format($d->rencana_ag,0,',','.')}}</td>
                                <td>{{number_format($d->rencana_sr,0,',','.')}}</td>
                                <td>{{number_format($d->realisasi_ag,0,',','.')}}</td>
                                <td>{{number_format($d->realisasi_sr,0,',','.')}}</td>
                                <td>{{number_format($d->rencana_ag-$d->realisasi_ag,0,',','.')}}</td>
                                <td>{{number_format($d->rencana_sr-$d->realisasi_sr,0,',','.')}}</td>
                                <td>{{number_format($d->rencana_modal,0,',','.')}}</td>
                                <td>{{number_format($d->rencana_modal_infra,0,',','.')}}</td>
                                <td>{{number_format($d->rencana_modal_infra+$d->rencana_modal,0,',','.')}}</td>
    
    
                              </tr> @endforeach
                              <tr>
                                <td class="bg-secondary" colspan="10"><b>Laba Bersih (Rp.)</b></td>
                            </tr>
                            @foreach($form_3['STIMULAN']['data'] as $d) 
                            <tr>
                                <td>{{$d->tahun}}</td>
                                <td colspan="9"> {{number_format($d->laba_bersih,0,',','.')}}</td>
                            </tr>
                            @endforeach
                            
                            </tbody>
                      </table>
    
                   </div>
                @endif

                @if(isset($form_3['PENDAMPING']))
                @php
                    $p_op+=1;
                @endphp
                <ol type="a" start="{{$p_op}}">
                    <li  class="justify">Dukungan Pemda {{$nama_pemda}} dalam pemenuhan Dana Pendamping telah merencanakan total Dana Pendamping sebesar <b>Rp. {{number_format($form_3['PENDAMPING']['cal']['perencanaan'],0,',','.')}}</b> dan telah terealisasii sampai dengan <b>{{$form_3['PENDAMPING']['cal']['tahun']}}</b> sebasar <b>Rp. {{number_format($form_3['PENDAMPING']['cal']['realisasi'],0,',','.')}}</b>, dengan uraian sebagai berikut:</li>
                  </ol>
                <div style="padding-left:40px;">
                    <table class="table tdata table-bordered">
                        <thead class="bg-secondary">
                          <tr>
                            <th rowspan="3">Tahun</th>
                            <th colspan="6">Pemenuhan Dana Pendamping Dalam Dukungan Serta Capaian SR</th>
                            <th colspan="3">Pemetaan Dukungan Pemda</th>
                          </tr>
                          <tr>
                            <th colspan="2">Rencana</th>
                            <th colspan="2">Realisasi</th>
                            <th colspan="2">Deviasi</th>
                            <th >Penyertaan Modal</th>
                            <th >Infrastruktur</th>
                            <th >Jumlah</th>
                          </tr>
                          <tr>
                            <th>RP.</th>
                            <th>SR</th>
                            <th>RP.</th>
                            <th>SR</th>
                            <th>RP.</th>
                            <th>SR</th>
                            <th>RP.</th>
                            <th>RP.</th>
                            <th>RP.</th>
    
                          </tr>
                        </thead>
                            <tbody> 
                                @foreach($form_3['PENDAMPING']['data'] as $d) 
                                <tr>
                              
                                <td>{{$d->tahun}}</td>
                                <td>{{number_format($d->rencana_ag,0,',','.')}}</td>
                                <td>{{number_format($d->rencana_sr,0,',','.')}}</td>
                                <td>{{number_format($d->realisasi_ag,0,',','.')}}</td>
                                <td>{{number_format($d->realisasi_sr,0,',','.')}}</td>
                                <td>{{number_format($d->rencana_ag-$d->realisasi_ag,0,',','.')}}</td>
                                <td>{{number_format($d->rencana_sr-$d->realisasi_sr,0,',','.')}}</td>
                                <td>{{number_format($d->rencana_modal,0,',','.')}}</td>
                                <td>{{number_format($d->rencana_modal_infra,0,',','.')}}</td>
                                <td>{{number_format($d->rencana_modal_infra+$d->rencana_modal,0,',','.')}}</td>
                                </tr> 
                            @endforeach 
                            <tr>
                                <td class="bg-secondary" colspan="10"><b>Laba Bersih (Rp.)</b></td>
                            </tr>
                            @foreach($form_3['PENDAMPING']['data'] as $d) 
                            <tr>
                                <td>{{$d->tahun}}</td>
                                <td  colspan="9">{{number_format($d->laba_bersih,0,',','.')}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                      </table>
                    </div>

                @endif
            </div>

            <div class="tematik" id="tematik4">
                @foreach($form_4['data'] as $key=>$data)
                @php
                    $p_op+=1;
                @endphp
                <ol type="a" start="{{$p_op}}">
                    <li  class="justify">Tarif air minum (FCR dan Non-FCR) merupakan target  {{$data['meta']->nama_bu}} yang harus didukung oleh kebijakan Pemda {{$nama_pemda}} Pada Tahun <b>{{$form_4['cal'][$key]['tahun']}}</b>  {!!$form_4['cal'][$key]['subsidi']?'telah memberikan  subsidi sebesar <b>Rp. '.number_format($form_4['cal'][$key]['subsidi'],0,',','.').'</b>':''!!}, dengan uraian sebagai berikut :</li>
                </ol>
                <div style="padding-left:40px;">
                    <table class="table tdata table-bordered">
                        <thead class="bg-secondary">
                          <tr>
                            <th rowspan="2">Tahun</th>
                            <th colspan="4">Dukungan Kebijakan PEMDA Dalam Pemenuhan Tarif Air Minum (FCR) {{$data['meta']->nama_bu}}</th>
                          </tr>
                          <tr>
                            <th> % FCR </th>
                            <th> Klasifikasi FCR/NON FCR </th>
                            <th> Subsidi Tarif (Ada,Tidak Ada) </th>
                            <th> Besaran Subsidi </th>
                          </tr>
                        </thead>
                        <tbody> @foreach($data['data'] as $d) <tr>
                              @php
                              @endphp
                            <td>{{$d->tahun}}</td>
                            <td>{{number_format($d->nilai_fcr,0,',','.')}}</td>
                            <td>{{$d->fcr?'FCR':'NONFCR'}}</td>
                            <td>{{$d->subsidi?'Ada':'-'}}</td>
                            <td>{{$d->subsidi?number_format($d->nilai_subsidi,0,',','.'):'-'}}</td>
                          </tr> @endforeach 
                        
                        
                        </tbody>
                      </table>

                </div>
                @endforeach
            </div>

            <div id="tematik5" class="tematik">
                <ol type="a" start="{{$p_op+1}}">
                    <li  class="justify">Sebagai syarat pemenuhan Readiness Criteria Pemerintah Daerah dalam keikutsertaan  Program NUWSP, berikut status, tindak lanjut dan isu strategis yang merupakan komitmen dan konsistensi pemda :</li>
                </ol>
                <div style="padding-left:40px;">
                    <table class="table tdata table-bordered">
                        <thead class="bg-secondary">
                          <tr>
                            <th colspan="5">RC PEMDA Dalam Komitmen dan Konsistensi Dalam Program NUWSP</th>
                            {{-- <th rowspan="3">Keterangan</th> --}}
                          </tr>
                          <tr>
                            <th rowspan="2" style="max-width: 400px;">Item</th>
                            <th style="width:27%;" colspan="4">Indentifikasi</th>
                          </tr>
                          <tr>
                            <th>Ada / Tidak Ada</th>
                            <th>Tanggal Pemenuhan / Target</th>
                            <th>Isu Strategis / Permasalahan</th>
                            <th>RKTL</th>
                          </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="width:27%;"  >RC-5; RKAP BUMDAM, Rencana Bisnis BUMDAM, dan RISPAM Kabupaten/Kota yang sudah mengakomodir usulan kegiatan NUWSP</td>
                                <td >{{$form_6['data']['rc5']?'Ada':'Tidak Ada'}}</td>
                                <td >
                                    <small>{{$form_6['data']['rc5']?'Pemenuhan':'Target'}}</small>
                                    <p>{{$form_6['data']['rc5']?$form_6['data']['rc5_date_done']??'-':$form_6['data']['rc5_date_target']??'-'}}</p>
                                </td>
                                <td >{!!$form_6['data']['rc5_isu']!!}</td>
                                <td >{!!$form_6['data']['rc5_rktl']!!}</td>
                                {{-- <td >{!!$form_6['data']['rc5_keterangan']!!}</td> --}}
                            </tr>
                            <tr>
                                <td style="width:27%;"  >RC-7; Dana Daerah Urusan Bersama (DDUB)</td>
                                <td >{{$form_6['data']['rc7']?'Ada':'Tidak Ada'}}</td>
                                <td >
                                    <small>{{$form_6['data']['rc7']?'Pemenuhan':'Target'}}</small>
                                    
                                    <p>{{$form_6['data']['rc7']?$form_6['data']['rc7_date_done']??'-':$form_6['data']['rc7_date_target']??'-'}}</p>
                                </td>
                                <td >{!!$form_6['data']['rc7_isu']!!}</td>
                                <td >{!!$form_6['data']['rc7_rktl']!!}</td>
                                {{-- <td >{!!$form_6['data']['rc7_keterangan']!!}</td> --}}
                            </tr>
                            <tr>
                                <td style="width:27%;"  >RC-12; Surat Pernyataan Komitmen penyediaan dana pembangunan Jaringan Distribusi Sekunder dan Tersier, disetujui oleh Bupati/Walikota dan Direktur BUMDAM</td>
                                <td >{{$form_6['data']['rc12']?'Ada':'Tidak Ada'}}</td>
                                <td >
                                    <small>{{$form_6['data']['rc12']?'Pemenuhan':'Target'}}</small>
                                    
                                    <p>{{$form_6['data']['rc12']?$form_6['data']['rc12_date_done']??'-':$form_6['data']['rc12_date_target']??'-'}}</p></td>
                                <td >{!!$form_6['data']['rc12_isu']!!}</td>
                                <td >{!!$form_6['data']['rc12_rktl']!!}</td>
                                {{-- <td >{!!$form_6['data']['rc12_keterangan']!!}</td> --}}
                            </tr>
                            <tr>
                                <td style="width:27%;"  >RC-14; Surat Kesediaan Menerima Hibah Barang Milik Negara (BMN)</td>
                                <td >{{$form_6['data']['rc14']?'Ada':'Tidak Ada'}}</td>
                                <td >
                                    <small>{{$form_6['data']['rc14']?'Pemenuhan':'Target'}}</small>
                                    <p>{{$form_6['data']['rc14']?$form_6['data']['rc14_date_done']??'-':$form_6['data']['rc14_date_target']??'-'}}</p>
                                    </td>
                                <td >{!!$form_6['data']['rc14_isu']!!}</td>
                                <td >{!!$form_6['data']['rc14_rktl']!!}</td>
                                {{-- <td >{!!$form_6['data']['rc14_keterangan']!!}</td> --}}
                            </tr>
                            <tr>
                                <td style="width:27%;" >RC-16; Salinan Dokumen Pelaksanaan Anggaran (DPA) / Peraturan Daerah Penyertaan Modal / RKAP PDAM Tahun Anggaran berjalan untuk kepastian pelaksanaan kegiatan DDUB (Bantuan Stimulan) dan/atau pembangunan Jaringan Distribusi Bagi (JDB) (Bantuan Pendamping)</td>
                                <td >{{$form_6['data']['rc16']?'Ada':'Tidak Ada'}}</td>
                                <td >
                                    <small>{{$form_6['data']['rc16']?'Pemenuhan':'Target'}}</small>
                                    
                                    <p>{{$form_6['data']['rc16']?$form_6['data']['rc16_date_done']??'-':$form_6['data']['rc16_date_target']??'-'}}</p>
                                    </td>
                                <td >{!!$form_6['data']['rc16_isu']!!}</td>
                                <td >{!!$form_6['data']['rc16_rktl']!!}</td>
                                {{-- <td >{!!$form_6['data']['rc16_keterangan']!!}</td> --}}
                            </tr>
                            <tr>
                                <td style="width:27%;" >RC-22; Dokumen lengkap perjanjian Kerjasama antara BUMDAM dengan pihak ketiga</td>
                                <td >{{$form_6['data']['rc22']?'Ada':'Tidak Ada'}}</td>
                                <td >
                                    <small>{{$form_6['data']['rc22']?'Pemenuhan':'Target'}}</small>
                                    <p> {{$form_6['data']['rc22']?$form_6['data']['rc22_date_done']??'-':$form_6['data']['rc22_date_target']??'-'}}</p>
                                   </td>
                                <td >{!!$form_6['data']['rc22_isu']!!}</td>
                                <td >{!!$form_6['data']['rc22_rktl']!!}</td>
                                {{-- <td >{!!$form_6['data']['rc22_keterangan']!!}</td> --}}
                            </tr>
                        </tbody>
                      </table>
                </div>
            </div>


            <div class="break_before">
                <p  class="justify">Demikian berita acara ini disusun untuk menjadi informasi seluruh stakeholder terkait dan diharapkan dapat dipergunakan sebagai bahan evaluasi sebagaimana mestinya sehingga dapat memberikan rekomendasi peningkatan capaian terhadap target Nasional dan Pemda dalam pencapaian akses jaringan air minum perpipaan.</p>
                <p  class="justify">Koordinasi dan informasi selanjutnya terkait hasil pelaksanaan desk, kelengkapan berita acara, maupun pengelolaan NUWSP di daerah secara umum dapat dilayangkan melalui email <a href="mailto:perkim.bangda@gmail.com">perkim.bangda@gmail.com</a>.
            </div>

           <table style="width: 100%;">
                    <thead>
                        <tr>
                            <th style="width:50%"></th>
                            <th style="width:50%"></th>

                        </tr>
                    </thead>
                   <tbody>
                    <tr>
                        <td style="width:50%"></td>
                        <td style="width:50%;  " >
                            <p style="text-align:right;">{{$date[0]}}, {{$date[1]}}  {{$date[2]}}  {{$date[3]}}</p>
                        </td>
                    </tr>
                    <tr style="text-align: center">
                        <td>
                            <small>Kasubdit Perumahan dan Kawasan Permukiman</small>
                        </td>
                        <td rowspan="2">
                            <small>{{($NAMA_PEMANGKU_PEMDA_JABATAN)}}</small>
                        </td>
                    </tr>
                    <tr style="text-align: center">
                        <td>Ditjen Bina Pembangunan Daerah<br>
                            Kementerian Dalam Negeri</td>
                    </tr>
                    <tr>
                        <td class="ttd"></td>
                        <td class="ttd"></td>

                    </tr>
                    <tr  style="text-align: center">
                        <td class="underline-content">
                            <small>{{($NAMA_PEMANGKU_BANGDA)}}</small>
                            <div class="underline"></div>
                        </td>
                        <td class="underline-content">
                            <small>{{($NAMA_PEMANGKU_PEMDA)}}</small>
                            <div class="underline"></div>

                        </td>
                    </tr>
                    <tr style="text-align: center">
                        <td><small>NIP. {{$NIP_PEMANGKU_BANGDA}}</small></td>
                        <td>
                            <small>NIP. {{$NIP_PEMANGKU_PEMDA}}</small>
                        </td>

                    </tr>
                   </tbody>
           </table>

        @if($have_catatan)
            <div class="break_before" id="catatan_list">
                <h3 style="text-align: center;"><b>LAMPIRAN CATATAN</b></h3>
                
                @foreach($catatan as $kc=>$c)
                    @if($c['catatan_kt'])
                        <p style="margin:0;"><b>{{$c['title']}}</b></p>
                        <div style="padding-left:40px;!important">
                            <p style="margin:0;" class="catatan_detail" >{!!$c['catatan']!!}</p>
                        </div>
                       <div style="padding-left:40px;">
                        @php
                        eval('$dataxx=$form_'.($kc+1).';');
                    @endphp
                    @if($kc==0)
                    
                        @if(strlen($kodepemda)<=3)
        
                            <ol>
                                @foreach($dataxx['data_turunan'] as $t)
                                    @php
                                    $exist_k=array_filter($t['data']->toArray(),function($l){return $l->keterangan;});
                                    @endphp
                                    @if(count($exist_k))
                                    <li>

                                        <p>{{$t['meta']->nama_pemda}}</p>
                                        <ol>
                                            @foreach($exist_k as $x)
                                                @if($x->keterangan)
                                                <li> <b>{{$x['tahun']}}</b> - {!!$x->keterangan!!}</li>
                                                @endif
                                            @endforeach
                                        </ol>
                                    </li>

                                    @endif
                                @endforeach
                            </ol>
                        @else
                            @php
                            $exist_k=array_filter($dataxx['data']->toArray(),function($l){return $l->keterangan;});
                            @endphp
                            @if(count($exist_k))
                            <p><b><small>Keterangan Data :</small></b></p>
                                <ol>
                                    @foreach($exist_k as $x)
                                    
                                        @if($x->keterangan)
                                        <li> <b>{{$x->tahun}}</b> - {!!$x->keterangan!!}</li>
                                        @endif
                                    @endforeach
                                </ol>
                            @endif
                        
                         @endif   
                    @elseif($kc==2)
                       @if(isset($dataxx['STIMULAN']))
                            @if(count($dataxx['STIMULAN']['data']->toArray()))
                            @php
                            $exist_k=array_filter($dataxx['STIMULAN']['data']->toArray(),function($l){return $l->keterangan;});
                            @endphp
                            @if(count($exist_k))
                            <p><b><small>Keterangan Data  DDUB:</small></b></p>
                                <ol>
                                    @foreach($exist_k as $x)
                                    
                                        @if($x->keterangan)
                                        <li> <b>{{$x->tahun}}</b> - {!!$x->keterangan!!}</li>
                                        @endif
                                    @endforeach
                                </ol>
                            @endif

                         @endif

                       @endif
                       @if(isset($dataxx['PENDAMPING']))

                       @if(count($dataxx['PENDAMPING']['data']->toArray()))
                       @php
                       $exist_k=array_filter($dataxx['PENDAMPING']['data']->toArray(),function($l){return $l->keterangan;});
                       @endphp
                       @if(count($exist_k))
                       <p><b><small>Keterangan Data Dana Pendamping:</small></b></p>
                           <ol>
                               @foreach($exist_k as $x)
                               
                                   @if($x->keterangan)
                                   <li> <b>{{$x->tahun}}</b> - {!!$x->keterangan!!}</li>
                                   @endif
                               @endforeach
                           </ol>
                       @endif

                       @endif
                       @endif
                        
                    @elseif($kc==3)
                        @foreach($dataxx['data'] as $k)
                            @php
                            $exist_k=array_filter($k['data']->toArray(),function($l){return $l->keterangan;});
                            @endphp
                            @if($exist_k)
                            <p><b>Keterangan Data {{$k['meta']->nama_bu}}:</b></p>
                            <ol>
                                @foreach($exist_k as $x)
                                   
                                    @if($x->keterangan)
                                    <li> <b>{{$x->tahun}}</b> - {!!$x->keterangan!!}</li>
                                    @endif
                                @endforeach
                            </ol>

                            @endif
                        @endforeach

                    @elseif($kc==5)

                        @php
                        $exist_k=[];
                        foreach([5,7,12,14,16,22] as $k){
                            if($form_6['data']['rc'.$k.'_keterangan']){
                                $exist_k[]=['RC-'.$k,$form_6['data']['rc'.$k.'_keterangan']];
                            }
                        }
                        @endphp
                        @if(count($exist_k))
                        <p><b>Keterangan Data RC:</b></p>
                        <ol>
                            @foreach($exist_k as $k)
                              
                                
                                <li><b>{{$k[0]}} </b> - {!!$k[1]!!}</li>
    
                            @endforeach
                           </ol>

                        @endif
                
                    @elseif($kc==1)
                        @php
                        $exist_k=array_filter($dataxx['data']->toArray(),function($l){return $l->keterangan;});
                        @endphp
                        @if(count($exist_k))
                           <p><b><small>Keterangan Data :</small></b></p>
                            <ol>
                                @foreach($exist_k as $x)
                                   
                                    @if($x->keterangan)
                                    <li> <b>{{$x->tahun}}</b> - {!!$x->keterangan!!}</li>
                                    @endif
                                @endforeach
                            </ol>
                        @endif

                    @endif
                       </div>
                        
                    @endif
                @endforeach
            </div>
        @endif
    </body>
</html>