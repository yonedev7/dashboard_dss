<div id="{{$id_component}}">
   <div class="card">
    <div class="card-body">
        <div class="btn-group mb-1">
            <button class="btn btn-primary" @click="addData(item)" v-for='item in tahun_add'>
                <i class="fa fa-plus"></i> @{{item}}
            </button>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th rowspan="4">AKSI</th>
                        <th rowspan="4">Tahun</th>
                        <th colspan="9">Air Munum Layak</th>
                        <th rowspan="3" colspan="2">Belum Terlayani</th>
                        <th rowspan="4" style="min-width:300px;">Ketarangan</th>
    
                    </tr>
                    <tr>
                        <th colspan="6">JP</th>
                        <th colspan="2" rowspan="2">BJP</th>
                        <th  rowspan="2">Air Minum Layak</th>
    
                    </tr>
                    <tr>
                        <th colspan="3">Kontribusi JP BUMDAM</th>
                        <th>Kontribusi JP Non BUMDAM</th>
                        <th colspan="2" >Jumlah JP</th>
    
                    </tr>
                    <tr>
                        <th style="min-width:100px;" >% (KK)</th>
                        <th style="min-width:100px;">KK</th>
                        <th style="min-width:100px;">SR</th>
                        <th style="min-width:100px;" ></th>
                        <th style="min-width:100px;" >%</th>
                        <th style="min-width:100px;" >%</th>
                        <th style="min-width:100px;" >SR</th>
                        <th style="min-width:100px;">%</th>
                        <th style="min-width:100px;">SR</th>
                        <th style="min-width:100px;">%</th>
                        <th style="min-width:100px;">SR / KK</th>
    
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="item,i in data">
                        <td >
                            <button @click="deleteItem(i)" class="btn btn-danger btn-sm" ><i class="fa fa-trash"></i></button>
                        </td>
                        <td>@{{item.tahun}}</td>
                        <td style="min-width:100px;">
                            <formulate-input type="number" step="0.1" v-model="item.jp_bumdam_kk_p" ></formulate-input>
                            
                        </td >
                        <td style="min-width:100px;">
                            <formulate-input type="number" step="1" v-model="item.jp_bumdam_kk" ></formulate-input>
    
                        </td>
                        <td style="min-width:100px;">
                            <formulate-input type="number" step="1" v-model="item.jp_bumdam_sr" ></formulate-input>
    
                        </td>
                        <td style="min-width:100px;">
                            <formulate-input type="number" step="1" v-model="item.jp_nonbumdam" ></formulate-input>
    
                        </td>
                        <td style="min-width:100px;">
                            <formulate-input type="number" step="0.1" v-model="item.jp_p" ></formulate-input>
    
                        </td>
                        <td style="min-width:100px;">
                            <formulate-input type="number" step="1" v-model="item.jp" ></formulate-input>
    
                        </td>
                        <td style="min-width:100px;">
                            <formulate-input type="number" step="1" v-model="item.bjp_sr" ></formulate-input>
    
                        </td>
                        <td style="min-width:100px;">
                            <formulate-input type="number" step="1" v-model="item.bjp_p" ></formulate-input>
    
                        </td>
                        <td style="min-width:100px;">
                            <formulate-input type="number" step="1" v-model="item.air_layak" ></formulate-input>
    
                        </td>
                        <td style="min-width:100px;">
                            <formulate-input type="number" step="0.1" v-model="item.jp_bumdam_kk" ></formulate-input>
    
                        </td>
                        <td style="min-width:100px;">
                            <formulate-input type="number" step="0.1" v-model="item.belum_terlayani" ></formulate-input>
    
                        </td>
                        <td style="width:400px;">
                            <formulate-input type="textarea"  v-model="item.keterangan" ></formulate-input>
    
                        </td>
                        
    
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card-footer">
        <button class="btn btn-primary" @click="saveItem()">Simpan</button>
    </div>
   </div>

</div>

@push('js')
<script>
var {{$id_component}}=new Vue({
    props:{
        meta:Object
    },
    el:'#{{$id_component}}',
    data:{
       
        pilihan_tahun:['2019','2020','2021','2022','2023'],
        data:[
            {
                kodepemda:'',
                tahun:2021,
                jp_bumdam_kk_p:0,
                jp_bumdam_kk:0,
                jp_bumdam_sr:0,
                jp_nonbumdam:0,
                jp_p:0,
                jp:0,
                bjp_sr:0,
                bjp_p:0,
                air_layak:0,
                belum_terlayani:0,
            }
        ],
        data_def:{
                kodepemda:'',
                tahun:2021,
                jp_bumdam_kk_p:0,
                jp_bumdam_kk:0,
                jp_bumdam_sr:0,
                jp_nonbumdam:0,
                jp_p:0,
                jp:0,
                bjp_sr:0,
                bjp_p:0,
                air_layak:0,
                belum_terlayani:0,
            }
    },
    computed:{
        tahun_add:function(){
           return  this.pilihan_tahun.filter(el=>{
                return !(this.data.map(d=>{
                    return d.tahun+'';
                })).includes(el);
            })
        },
        
    },
    methods:{
        addData:function(tahun){
            var data=this.data_def;
            data.tahun=parseInt(tahun);
            this.data.push(JSON.parse(JSON.stringify(data)));
        },
        deleteItem:function(i){
            this.data.splice(i,1);
        },
        loadData:function(){
            ajax_req.post('{{route('api.web.mod-eval.form-1.save')}}',{
                data:this.data,
                meta:{
                    kodepemda:meta.pemda.kodepemda,
                    kodebu:meta.bumdam.kodebumdam,
                    tipe_bantuan:meta.bantuan.tipe_bantuan
                }
            }).then(res=>{

            }).finnaly(function(){

            });
        }
        },
        save:function(){
            ajax_req.post('{{route('api.web.mod-eval.form-1.save')}}',{
                data:this.data,
                meta:{
                    kodepemda:meta.pemda.kodepemda,
                    kodebu:meta.bumdam.kodebumdam,
                    tipe_bantuan:meta.bantuan.tipe_bantuan
                }
            }).then(res=>{

            }).finnaly(function(){

            });
        }
    }

})

</script>

@endpush