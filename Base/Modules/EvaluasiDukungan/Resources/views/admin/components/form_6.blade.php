<div id="form_1">
    <div class="btn-group mb-1">
        <button class="btn btn-primary" v-for='item in pilihan_tahun'>
            <i class="fa fa-plus"></i> @{{item}}
        </button>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead class="thead-dark">
                <tr>
                    <th rowspan="4">AKSI</th>

                    <th rowspan="4">Tahun</th>

                    <th colspan="9">Air Munum Layak</th>
                    <th rowspan="3" colspan="2">Belum Terlayani</th>
                </tr>
                <tr>
                    <th colspan="6">JP</th>
                    <th colspan="2" rowspan="2">BJP</th>
                    <th  rowspan="2">Air Minum Layak</th>

                </tr>
                <tr>
                    <th colspan="3">Kontribusi JP BUMDAM</th>
                    <th>Kontribusi JP Non BUMDAM</th>
                    <th colspan="2" >Jumlah JP</th>

                </tr>
                <tr>

                    <th >% (KK)</th>
                    <th >KK</th>
                    <th >SR</th>
                    <th ></th>
                    <th >%</th>
                    <th >%</th>
                    <th >SR</th>
                    <th >%</th>
                    <th >SR</th>
                    <th >%</th>
                    <th >SR / KK</th>

                </tr>
            </thead>
            <tbody>
                <tr v-for="item,i in data">
                    <td></td>
                    <td>@{{item.tahun}}</td>
                    <td>
                        <formulate-input type="number" v-model="item.jp_bumdam_kk_p" ></formulate-input>
                        
                    </td>
                    <td>
                        <formulate-input type="number" v-model="item.jp_bumdam_kk" ></formulate-input>

                    </td>
                    <td>
                        <formulate-input type="number" v-model="item.jp_bumdam_sr" ></formulate-input>

                    </td>
                    <td>
                        <formulate-input type="number" v-model="item.jp_bumdam_kk" ></formulate-input>

                    </td>
                    <td>
                        <formulate-input type="number" v-model="item.jp_bumdam_sr" ></formulate-input>

                    </td>
                    <td>
                        <formulate-input type="number" v-model="item.jp_bumdam_kk" ></formulate-input>

                    </td>
                    <td>
                        <formulate-input type="number" v-model="item.jp_bumdam_sr" ></formulate-input>

                    </td>
                    <td>
                        <formulate-input type="number" v-model="item.jp_bumdam_kk" ></formulate-input>

                    </td>
                    <td>
                        <formulate-input type="number" v-model="item.jp_bumdam_sr" ></formulate-input>

                    </td>
                    <td>
                        <formulate-input type="number" v-model="item.jp_bumdam_kk" ></formulate-input>

                    </td>
                    <td>
                        <formulate-input type="number" v-model="item.jp_bumdam_sr" ></formulate-input>

                    </td>

                </tr>
            </tbody>
        </table>
    </div>

</div>

@push('js')
<script>
var form_1=new Vue({
    el:'#form_1',
    data:{
        pilihan_tahun:['2019','2020','2021','2022'],
        data:[
            {
                kodepemda:'',
                tahun:2021,
                jp_bumdam_kk_p:0,
                jp_bumdam_kk:0,
                jp_bumdam_sr:0,
                jp_nonbumdam:0,
                jp_p:0,
                jp:0,
                bjp_sr:0,
                bjp_p:0,
                air_layak:0,
                belum_terlayani:0


            }
        ]
    },

})

</script>

@endpush