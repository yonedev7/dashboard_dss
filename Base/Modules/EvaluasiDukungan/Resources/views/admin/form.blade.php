@extends('adminlte::page')
@section('content_header')
    <h4>Evaluasi Dukungan Pemerintah Daerah Terhadap Program Air Minum Dalam Kerangka NUWSP</h4>
@stop

@section('css')
{{-- <style>
    td,th{
        font-size: 18px!important;
    }

    .bg-c1{
        background: #495C83!important;
        color:#fff!important;
    }
    .bg-c1-data{
        background: #5b6985!important;
        color:#fff!important;
    }
    .bg-c2{
        background:#6a7399!important;
        color:#fff!important;
        font-weight: 800;
    }

    .bg-c2-data{
        background:#767b8e!important;
        color:#fff!important;
        font-weight: 800;
    }

    .bg-c3{
        background:#9995be;
        color:#fff;
        font-weight: 800;
    }

    .bg-c3-data{
        background:#9e9cb2;
        color:#fff;
        font-weight: 800;
    }

    .bg-c4{
        background:#b19fca;
        color:#fff;
        font-weight: 800;
    }

    .bg-c4-data{
        background:#b0a8bc;
        color:#fff;
        font-weight: 800;
    }

    .bg-negasi{
        background: rgb(247, 247, 145);
        color:#000;
        font-weight: 8000;
    }

    .bg-negasi-data{
        background: rgb(240, 240, 188);
        color:#000;
        font-weight: 8000;
    }

    .bg-p-1{
        background: #64aefe;
        color:#fff;
        font-weight: 8000;


    }

    .bg-p-1-data{
        background: #81bcfb;
        color:#fff;
        font-weight: 8000;


    }

    .bg-p-2{
        background: #64aefe;
        color:#fff;
        font-weight: 8000;


    }

   
</style> --}}

{{-- <style>
  .bg-c1{
        background: #7FBCD2;
        font-weight: 800;
        color:#222;
    }

    .bg-c1-data{
        background: #b5e3f4;
        font-weight: 800;
        color:#222;
    }

    .bg-c2{
        background: #A5F1E9;
        font-weight: 800;
        color:#222;
    }
    .bg-c2-data{
        background: #d5fcf8;
        font-weight: 800;
        color:#222;
    }
    .bg-c3{
        background: #E1FFEE;
        font-weight: 800;
        color:#222;
    }
    .bg-c3-data{
        background: #eafdf2;
        font-weight: 800;
        color:#222;
    }
    .bg-p-1{
        background:#c8f0ec;
        color:#222;
        font-weight: 8000;


    }
    .bg-p-1-data{
        background:#e6fffd;
        color:#222;
        font-weight: 8000;


    }
    .bg-c4{
        background: #cfe4d8;
        font-weight: 800;
        color:#222;
    }
    .bg-c4-data{
        background: #e6f9ee;
        font-weight: 800;
        color:#222;
    }

    .bg-negasi{
        background: #FFEEAF;
        font-weight: 800;
        color:#222;
    }
    .bg-negasi-data{
        background: #fff5d1;
        font-weight: 800;
        color:#222;
    }
     
</style> --}}
<style>
    .area-text{
        width:100%;
    }
.input-size-18 ,input[type="text"],textarea{
    font-size: 14px;
    font-weight: 800;
    border: 0;
    color:#222;
    border-bottom:1px solid #222;
}
</style>

@stop

@section('content')


<div class="" id="app">
    <div ref="berita_acara" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="my-modal-title">Berita Acara Evaluasi Dukungan </h5>
                    <button class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{-- <a class="btn btn-success" :href="('{{route('ev.mod.form.ba',['tahun'=>$StahunRoute,'kodepemda'=>'x'])}}').replace('x',meta.pemda.kodepemda)">Berita Acara</a> --}}

                </div>
                <div class="modal-body">
                    <p><b>Priview Berita Acara</b></p>
                    <iframe ref="frame_ba" style="width:100%; height:80vh;" :src="ba_link" v-if="ba_link"></iframe>  
                    
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" @click="showPengesahan()">Pemangku Pemda</button>
                </div>
            </div>
        </div>
    </div>
    <div ref="berita_acara_pengesahan" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="my-modal-title">Pemangku Pemda Pada Berita Acara Evaluasi Dukungan </h5>
                    <button class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                </div>
                <form :action="('{{route('ev.mod.form.pengesahan',['tahun'=>$StahunRoute,'kodepemda'=>'x'])}}').replace('x',meta.pemda.kodepemda)" method="post">

                <div class="modal-body">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <p><b>@{{meta.pemda.nama_pemda}} Tahun {{$StahunRoute}}</b></p>
                    <div class="form-group">
                        <label for="">Nama</label>
                        <input type="text" required name="pemangku_nama" class="form-control" v-model="form_pengesahan.nama">
                    </div>
                    <div class="form-group">
                        <label for="">NIP</label>
                        <input type="text" required name="pemangku_nip"  class="form-control" v-model="form_pengesahan.nip">
                    </div>
                    <div class="form-group">
                        <label for="">Jabatan</label>
                        <input type="text" required name="pemangku_jabatan"  class="form-control" v-model="form_pengesahan.jabatan">
                    </div>
                
                    <p>Pastikan Data Benar Sebelum Dilakukan Update Data</p>
                    
                </div>
                <div class="modal-footer text-center">
                       
                    <button class="btn btn-primary" type="submit" v-if="form_pengesahan.nama && form_pengesahan.nip && form_pengesahan.jabatan" >Sahkan</button>

                </div>
            </form>

            </div>
        </div>
    </div>
    <div class="p-3 m-2 bg-white rounded">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">Pilih Daerah</label>
                    <p><b>@{{meta.pemda.nama_pemda}}</b></p>
                </div>
                <div class="form-group">
                    <label for="">Tipe Bantuan</label>
                    <template v-if="meta.bantuan.pilihan.length>1">
                        <v-select :required="true" class="" :options="meta.bantuan.pilihan" v-model="meta.bantuan.select" :label="'tipe_bantuan'"></v-select>
                    </template>
                    <template v-else>
                        <p class="mb-0"><b>@{{meta.bantuan.select.tipe_bantuan}}</b></p>
                    </template>
                    <div class="mt-2">
                        <span class="badge badge-success">Tahun Usulan : @{{meta.bantuan.select.tahun_bantuan}}</span>
                    <span class="badge badge-primary">Tahun Proyek : @{{meta.bantuan.select.tahun_proyek}}</span>
                    </div>

                
                </div>
            </div>
          
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">Nama BUMDAM</label>
                    <template v-if="meta.bumdam.pilihan.length>1">
                        <v-select class="" :options="meta.bantuan.pilihan" v-model="meta.bantuan.select" :label="'tipe_bantuan'"></v-select>
                    </template>
                    <template>
                        <p class="mb-0"><b>@{{meta.bumdam.select.nama_bu}}</b></p>
                    </template>
                </div>
                <div class="form-group">
                    <button @click="showCatatan" class="btn btn-primary btn-sm">
                       Catatan @{{list_form[focus].name}}
                    </button>
                </div>
            </div>
        </div>
    </div>
    
    <div class="p-2">
       
        <div class="card ">
            <div class="card-body">
                <button @click="buatBeritaAcara" class="btn btn-success"><i class="fa fa-check-double"></i> Buat Berita Acara</button>

                <button @click="focus=i[0]" :class="'btn  ml-2 '+(focus==i[0]?'btn-primary':'border')" v-for="i in Object.entries(list_form)">@{{i[1].name.replace('Form','Tematik')}}</button>
            </div>
        </div>
    </div>
    <h5 class="text-center"><b>@{{list_form[focus].title}}</b></h5>
    <div class="p-2" v-if="focus=='form_1'">
        <eval-form-1 :data-ref="refs_data" class="rounded shadow" :tahun="tahun" :pilihan_tahun="pilihan_tahun" ref="form_1" :meta="meta" :getter="('{{route('api.web.mod-eval.form-1.get',['kodepemda'=>'x'])}}').replace('x',meta.pemda.kodepemda)" :setter="('{{route('api.web.mod-eval.form-1.save',['kodepemda'=>'x'])}}').replace('x',meta.pemda.kodepemda)"></eval-form-1>
    </div>

    <div class="p-2"  v-if="focus=='form_2'">
        <eval-form-2  class="rounded shadow" :tahun="tahun" :pilihan_tahun="pilihan_tahun" ref="form_2" :meta="meta" :getter="('{{route('api.web.mod-eval.form-2.get',['kodepemda'=>'x'])}}').replace('x',meta.pemda.kodepemda)" :setter="('{{route('api.web.mod-eval.form-2.save',['kodepemda'=>'x'])}}').replace('x',meta.pemda.kodepemda)"></eval-form-2>
    </div>


    <div class="p-2"  v-if="focus=='form_3'">
        <eval-form-3 class="rounded shadow" :tahun="tahun" :pilihan_tahun="pilihan_tahun" ref="form_3" :meta="meta" :getter="('{{route('api.web.mod-eval.form-3.get',['kodepemda'=>'x'])}}').replace('x',meta.pemda.kodepemda)" :setter="('{{route('api.web.mod-eval.form-3.save',['kodepemda'=>'x'])}}').replace('x',meta.pemda.kodepemda)"></eval-form-3>
    </div>
    <div class="p-2"  v-if="focus=='form_4'">
        <eval-form-4 class="rounded shadow" :tahun="tahun" :pilihan_tahun="pilihan_tahun" ref="form_4" :meta="meta" :getter="('{{route('api.web.mod-eval.form-4.get',['kodepemda'=>'x'])}}').replace('x',meta.pemda.kodepemda)" :setter="('{{route('api.web.mod-eval.form-4.save',['kodepemda'=>'x'])}}').replace('x',meta.pemda.kodepemda)"></eval-form-4>
    </div>
    <div class="p-2"  v-if="focus=='form_5'">
        <eval-form-5 class="rounded shadow" :tahun="tahun" :pilihan_tahun="pilihan_tahun" ref="form_5" :meta="meta" :getter="('{{route('api.web.mod-eval.form-5.get',['kodepemda'=>'x'])}}').replace('x',meta.pemda.kodepemda)" :setter="('{{route('api.web.mod-eval.form-5.save',['kodepemda'=>'x'])}}').replace('x',meta.pemda.kodepemda)"></eval-form-5>
    </div>
    <div class="p-2"  v-if="focus=='form_6'">
        <eval-form-6 class="rounded shadow" :tahun="tahun" :pilihan_tahun="pilihan_tahun" ref="form_5" :meta="meta" :getter="('{{route('api.web.mod-eval.form-6.get',['kodepemda'=>'x'])}}').replace('x',meta.pemda.kodepemda)" :setter="('{{route('api.web.mod-eval.form-6.save',['kodepemda'=>'x'])}}').replace('x',meta.pemda.kodepemda)"></eval-form-6>
    </div>
    <div ref="catatan_form" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-full"  role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="my-modal-title">Catatan @{{list_form[focus].name}}</h5>
                    <button class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ckeditor  :editor="catatan.class_editor" :config="catatan.config"  v-model="catatan.catatan"></ckeditor>
                </div>
                <div class="modal-footer">
                    <button @click="simpanCatatan()" class="btn btn-primary ">Simpan</button>
                </div>
            </div>
        </div>
    </div>

</div>



@stop

@section('js')
<script>
    window.refs_data={

    };
    var app=new Vue({
        el:'#app',
        computed:{
            com_ba_link:function(){
                return ('{{route('ev.mod.form.ba',['tahun'=>$StahunRoute,'kodepemda'=>'x'])}}').replace('x',this.meta.pemda.kodepemda);
            }
        },
        watch:{
            "focus":function(val){
                this.getCatatan();
                var self=this; 
                this.loadRef();
            }
        },
        methods: {
            showCatatan:function(){
                this.getCatatan();
                $(this.$refs.catatan_form).modal({
                    backdrop:'static'
                });
            },
            showPengesahan:function(){
                $(this.$refs.berita_acara_pengesahan).modal({
                    backdrop:'static'
                });
            },

            loadRef:function(){
                window.refs_data=null;
                req_ajax.post(('{{route('api.web.mod-eval.form.ref.data',['tahun'=>$StahunRoute,'kodepemda'=>'x'])}}').replace('x',this.meta.pemda.kodepemda),{
                    'form':this.focus
                 }).then(res=>{
                    self.refs_data=res.data.data;
                    window.refs_data=res.data.data;
                    console.log('ref',window.refs_data,res.data);
                }).finally(function(){

                });

            },

            buatBeritaAcara:function(){
                this.ba_link=null;
                $(this.$refs.berita_acara).modal({
                    backdrop:'static'
                });
                setTimeout((ref) => {
                ref.ba_link=ref.com_ba_link;
                    
                }, 300,this);
               
            },
         
            simpanCatatan:function(){
                var self=this;
                self.$isLoading(true);

                req_ajax.post(('{{route('api.web.mod-eval.catatan.store',['kodepemda'=>'x'])}}').replace('x',this.meta.pemda.kodepemda),{
                        catatan:this.catatan.catatan,
                        meta:{
                            tahun:{{$StahunRoute}},
                            form_key:this.focus,
                            kodepemda:this.meta.pemda.kodepemda,
                        }
                    }).then(res=>{
                        self.$isLoading(false);
                    }).finally(function(){
                        self.getCatatan();
                    });
            },
            getCatatan:function(){
                var self=this;
                self.$isLoading(true);


                req_ajax.post(('{{route('api.web.mod-eval.catatan.get',['kodepemda'=>'x'])}}').replace('x',this.meta.pemda.kodepemda),{
                        meta:{
                            tahun:{{$StahunRoute}},
                            form_key:this.focus,
                            kodepemda:this.meta.pemda.kodepemda,
                        }
                    }).then(res=>{
                        if(res.data.code==200){
                            self.catatan.catatan=res.data.data;
                        }
                        self.$isLoading(false);
                    }).finally(function(){

                    });
            }
        },
        mounted() {
            var tahun={{$StahunRoute}}+1;

            do{
                this.pilihan_tahun.push(''+tahun);
                tahun-=1;
            }while(tahun>2017);


            this.loadRef();

            
        },
        data:{
            form_pengesahan:{
                nama:'{{$pemangku['nama']}}',
                nip:'{{$pemangku['nip']}}',
                jabatan:'{{$pemangku['jabatan']}}'
            },
            ba_link:'',
            refs_data:{},
            refs_data:{},
            pilihan_tahun:[],
            tahun:{{$StahunRoute}},
            meta:<?=json_encode($meta)?>,
            catatan:{
                id:'',
                class_editor:  window.CKClassicEditor,
                catatan:'',
                config:{
                    toolbar: [ 'bold', 'italic', '|', 'link' ]
                }
            },
            focus:"form_1",
            list_form:{
                form_1:{
                    name:'Tematik 1',
                    title:'Evaluasi Capaian Air Minum Layak',
                    catatan:""

                },
                form_2:{
                    name:'Tematik 2',
                    title:'Evaluasi Program Kegiatan Pengelolaan Air Minum Daerah',
                    catatan:""


                },
                form_3:{
                    name:'Tematik 3',
                    title:'Evaluasi Pemenuhan DDUB dan Dana Pendamping Serta Pemetaan  Dukungan PEMDA',
                    catatan:""

                },
                form_4:{
                    name:'Tematik 4',
                    title:'Evaluasi Dukungan Kebijakan PEMDA Dalam Pemenuhan Tarif Air Minum (FCR)',
                    catatan:""


                },
                // form_5:{
                //     name:'Form 5',
                //     title:'Hubungan Dukungan PEMDA kepad BUMDAM',
                //     catatan:""


                // },
                form_6:{
                    name:'Tematik 5',
                    title:'Evaluasi Pemenuhan Readiness Criteria Pemerintah Daerah Dalam Komitmen dan Konsistensi Keikutsertaan Dalam Program NUWSP',
                    catatan:""


                },



            },
            data:[{
                kodepemda:'0999',
                nama_pemda:'skjskjsk sjsk'
            }],
            data_chart:{
                data_1_1:{
                    title:{
                        text:'Evaluasi Capaian Air Minum Layak {{$StahunRoute}}'
                    },
                    chart:{
                        type:'line',

                    },
                    yAxis:[
                        {
                            title:{
                                text:'%'
                            },
                            opposite:true,
                        },
                        {
                            title:{
                                text:'JP'
                            }
                        },
                        {
                            title:{
                                text:'SR'
                            },
                            opposite:true
                        }
                    ],
                    xAxis:{
                        type:'category',
                    },
                    series:[
                        {
                            name:'JP-SR BUMDAM',
                            data:[
                                {
                                    name:'2019',
                                    y:555,
                                    yAxis:1
                                },
                                {
                                    name:'2020',
                                    y:100,
                                    yAxis:1

                                },
                                {
                                    name:'2021',
                                    yAxis:1,
                                    y:900

                                }
                            ]
                        },
                        {
                            name:'JP NON BUMDAM',
                            yAxis:2,
                            data:[
                                {
                                    name:'2019',
                                    y:555,
                                },
                                {
                                    name:'2020',
                                    y:100,

                                },
                                {
                                    name:'2021',
                                    y:900

                                }
                            ]
                        },
                        {
                            name:'BJP',
                            yAxis:1,
                            data:[
                                {
                                    name:'2019',
                                    y:555,
                                },
                                {
                                    name:'2020',
                                    y:100,

                                },
                                {
                                    name:'2021',
                                    y:900

                                }
                            ]
                        },
                        {
                            name:'Air Minum Layak (SR)',
                            yAxis:1,
                            data:[
                                {
                                    name:'2019',
                                    y:555,
                                },
                                {
                                    name:'2020',
                                    y:100,

                                },
                                {
                                    name:'2021',
                                    y:900

                                }
                            ]
                        },
                        {
                            name:'Belum Terlayani',
                            yAxis:1,
                            data:[
                                {
                                    name:'2019',
                                    y:555,
                                },
                                {
                                    name:'2020',
                                    y:100,

                                },
                                {
                                    name:'2021',
                                    y:900

                                }
                            ]
                        }
                    ]
                }
            }
            
        }
    });
</script>

@stop

