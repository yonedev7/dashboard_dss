@extends('adminlte::page')
@section('content_header')
    <h4>Evaluasi Dukungan Pemerintah Daerah Terhadap Program Air Minum Dalam Kerangka NUWSP</h4>
@stop

@section('content')

<div class="" id="app">

<div class="d-flex p-3 m-2">
    <div class="flex-column  bg-dark p-2 rounded" style="min-width:300px;">
        <input type="text" class="form-control mb-2" v-model="search" placeholder="Cari Pemda">
        <ul class="list-group" v-for="item in serachpemda">
            <li class="list-group-item text-dark" >
                <div class="d-flex">
                        <p class="flex-grow-1"> @{{item.nama_pemda}}</p>
                        <div class="btn-group w-20  ml-2 ">
                            <a :href="('{{route('ev.mod.form',['tahun'=>$StahunRoute,'kodepemda'=>'x'])}}').replace('x',item.kodepemda)" class=" w-50 btn btn-warning btn-sm "><i class="fa fa-pen"></i></a>
                            <button @click="loadData(item)"  class=" w-50 btn btn-primary btn-sm "><i class="fa fa-eye"></i></button>
                            <a :href="('{{route('ev.mod.build_excel',["tahun"=>$StahunRoute,'kodepemda'=>'xxx'])}}').replace('xxx',item.kodepemda)"class="btn btn-success" >Excel</a>
                        </div>
                    </div>
            </li>
        </ul>
        <div >
            <h5>    </h5>
        </div>
    </div>
    <div class="flex-grow-1 ml-2">
        <div class="card">
            <div class="card-body">
            <h5 class="text-center text-uppercase"><b>@{{meta.pemda.nama_pemda}}</b></h5>
            </div>
        </div>
        <template v-if="meta.pemda.kode">
            <chart-eval-form-1 :meta="meta" :getter="'{{route('api.web.mod-eval.form-1.get_chart',['tahun'=>$StahunRoute,'kodepemda'=>'xxx'])}}'" :tahun="{{$StahunRoute}}"></chart-eval-form-1>
            <chart-eval-form-2 :meta="meta" :getter="'{{route('api.web.mod-eval.form-2.get_chart',['tahun'=>$StahunRoute,'kodepemda'=>'xxx'])}}'" :tahun="{{$StahunRoute}}"></chart-eval-form-2>
            <chart-eval-form-3 :meta="meta" :getter="'{{route('api.web.mod-eval.form-3.get_chart',['tahun'=>$StahunRoute,'kodepemda'=>'xxx'])}}'" :tahun="{{$StahunRoute}}"></chart-eval-form-3>
            <chart-eval-form-4 :meta="meta" :getter="'{{route('api.web.mod-eval.form-4.get_chart',['tahun'=>$StahunRoute,'kodepemda'=>'xxx'])}}'" :tahun="{{$StahunRoute}}"></chart-eval-form-4>
            
        </template>
        </div>
    </div>
</div>

</div>


@stop

@section('js')
<script>
    var app=new Vue({
        el:'#app',
        created:function(){
           
        },
        computed:{
            allpemda:function(){
                return this.data.map(el=>{
                    return el.kodepemda;
                })
            },

            serachpemda:function(){
               return this.data.filter(el=>{
                    return (el.nama_pemda||'').toUpperCase().includes((this.search||'').toUpperCase());                });
            }
        },
        created:function(){
        },
        methods: {
            buildQuadrand:function(chart){
            },
            loadData:function(data){
                console.log(data);
                this.meta.pemda.nama_pemda=data.nama_pemda;
                this.meta.pemda.kode=data.kodepemda;
            }
          
        },
        data:{
            search:'',
            data:<?=json_encode($pemda)?>,
           
            meta:{
                pemda:{
                    nama_pemda:'',
                    kode:''
                }
            }
            
        }
    })
</script>

@stop

