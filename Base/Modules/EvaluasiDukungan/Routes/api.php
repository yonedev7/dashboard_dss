<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/evaluasidukungan', function (Request $request) {
    return $request->user();
});

Route::prefix('api-mod/evaluasi-dukungan/{tahun}')->group(function(){
    Route::get('build-excel-pemda/{kodepemda}','EvaluasiDukunganController@exportExcel');
    Route::get('build-ba-pemda/{kodepemda}','EvaluasiDukunganController@buildBertiaAcara');

    Route::get('list-desk/','EvaluasiDukunganController@listDesk');
    Route::post('list-pemda/{deskid}','EvaluasiDukunganController@listPemda');
});


Route::prefix('mod-eval-dukungan')->middleware('auth:api')->name('api.web.mod-eval')->group(function(){

    Route::post('refrensi-data/{tahun}/{kodepemda}/load-refs','EvaluasiDukunganController@refData')->name('.form.ref.data');

    Route::post('form-1/{kodepemda}/save','CapaianAirMinumController@store')->name('.form-1.save');
    Route::post('form-1/{kodepemda}/get','CapaianAirMinumController@index')->name('.form-1.get');
    Route::post('form-1/{tahun}/{kodepemda}/get-chart','CapaianAirMinumController@getChart')->name('.form-1.get_chart');

    Route::post('form-2/{kodepemda}/save','RKPDAPBDController@store')->name('.form-2.save');
    Route::post('form-2/{kodepemda}/get','RKPDAPBDController@index')->name('.form-2.get');
    Route::post('form-2/{tahun}/{kodepemda}/get-chart','RKPDAPBDController@getChart')->name('.form-2.get_chart');


    Route::post('form-3/{kodepemda}/save','DDUBController@store')->name('.form-3.save');
    Route::post('form-3/{kodepemda}/get','DDUBController@index')->name('.form-3.get');
    Route::post('form-3/{tahun}/{kodepemda}/get-chart','DDUBController@getChart')->name('.form-3.get_chart');

    Route::post('form-4/{kodepemda}/save','BUMDAMFCRController@store')->name('.form-4.save');
    Route::post('form-4/{kodepemda}/get','BUMDAMFCRController@index')->name('.form-4.get');
    Route::post('form-4/{tahun}/{kodepemda}/get-chart','BUMDAMFCRController@getChart')->name('.form-4.get_chart');


    Route::post('form-5/{kodepemda}/save','DukunganPemdaBumdamController@store')->name('.form-5.save');
    Route::post('form-5/{kodepemda}/get','DukunganPemdaBumdamController@index')->name('.form-5.get');
    Route::post('form-5/{tahun}/{kodepemda}/get-chart','DukunganPemdaBumdamController@getChart')->name('.form-5.get_chart');


    Route::post('form-6/{kodepemda}/save','ReadinessCriteria@store')->name('.form-6.save');
    Route::post('form-6/{kodepemda}/get','ReadinessCriteria@index')->name('.form-6.get');

    Route::post('catatan/{kodepemda}/get','EvaluasiDukunganController@getCatatan')->name('.catatan.get');
    Route::post('catatan/{kodepemda}/store','EvaluasiDukunganController@storeCatatan')->name('.catatan.store');



});