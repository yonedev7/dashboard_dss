<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('microservice/mod-eval/{tahun}/')->group(function(){
    Route::get('build-excel-pemda/{kodepemda}','EvaluasiDukunganController@exportExcel');
});

Route::prefix('dash/{tahun}/sistem-informasi/mod-evaluasi-dukungan')->middleware('auth:web')->group(function() {
    Route::get('/', 'EvaluasiDukunganController@home')->name('ev.mod.index');

    Route::get('/form-build-excel/{kodepemda}', 'EvaluasiDukunganController@exportExcel')->name('ev.mod.build_excel');

    Route::get('/form/{kodepemda}', 'EvaluasiDukunganController@form')->name('ev.mod.form');
    Route::post('/form-berita-acara-pengesahan/{kodepemda}', 'EvaluasiDukunganController@pengesahan')->name('ev.mod.form.pengesahan');

    Route::get('/form-berita-acara/{kodepemda}', 'EvaluasiDukunganController@buildBertiaAcara')->name('ev.mod.form.ba');

});
