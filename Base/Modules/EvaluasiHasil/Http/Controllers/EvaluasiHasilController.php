<?php

namespace Modules\EvaluasiHasil\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use App\Http\Controllers\FilterCtrl;
use Barryvdh\DomPDF\Facade\Pdf;


class EvaluasiHasilController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index($tahun,Request $request)
    {

        $filter=FilterCtrl::buildFilterPemda($request);
        $filter['lokus']=true;
        $filter['sortlist']=(int)$tahun;

        $pemdas=FilterCtrl::filter_pemda($filter['regional']??null,$filter['lokus'],$filter['sortlist'],$filter['tipe_bantuan'],$filter['jenis_pemda'],$filter['pemda']);
        $data=[];
        $sql1="(select * from ( (select c.kodepemda,mp.nama_pemda,((c.bumdam_kk+c.nonbumdam_kk+c.bjp_kk)/c.kk)*100 as layak from output.capaian_air_minum as c join master.master_pemda as mp on mp.kodepemda =c.kodepemda where ((c.bumdam_kk+c.nonbumdam_kk+c.bjp_kk)) >0 and c.kk>0 and tahun=$tahun and length(mp.kodepemda) >3 ) union (select mp.kodepemda,max(mp.nama_pemda) as nama_pemda,(((sum(cam.bumdam_kk) + sum(cam.nonbumdam_kk)+ sum(cam.bjp_kk))/sum(cam.kk))*100) as layak from master.master_pemda mp join output.capaian_air_minum cam on left(cam.kodepemda,3)=mp.kodepemda where length(mp.kodepemda) <=3 and cam.tahun=$tahun group by mp.kodepemda ) ) as x where x.layak <=100 and x.kodepemda in ('".implode("','",$pemdas)."') order by layak desc limit 5)";
        
        $data['tahun']=$tahun;
        $data['tematik_1']= DB::select(DB::raw($sql1));

        $data['tematik_2']= [
            'apbd'=>DB::select(DB::raw("select pk.kodepemda,mp.nama_pemda,pk.apbd_ar_ag  from output.program_kegiatan pk
            join master.master_pemda as mp on mp.kodepemda =pk.kodepemda 
            where tahun =$tahun  and pk.apbd_ar_ag>0 and  mp.kodepemda in ('".implode("','",$pemdas)."')  order by apbd_ar_ag desc limit 5")),
            'persentase_apbd'=>DB::select(DB::raw("select pk.kodepemda,mp.nama_pemda,(pk.apbd_ar_ag/pk.apbd_tot)*100 as pres  from output.program_kegiatan pk
            join master.master_pemda as mp on mp.kodepemda =pk.kodepemda
            and (pk.apbd_ar_ag >0 and pk.apbd_tot >0)  
            where tahun =$tahun  and pk.apbd_ar_ag>0 and  mp.kodepemda in ('".implode("','",$pemdas)."')  order by (pk.apbd_ar_ag/pk.apbd_tot)  desc limit 5"))

        ];

        $data['tematik_3']=[
            'ddub'=>[
                'rencana'=>DB::select(DB::raw("select pk.kodepemda,max(mp.nama_pemda) as nama_pemda,sum(pk.rencana_ag) as rencana_ag  from output.ddub pk
                join master.master_pemda as mp on mp.kodepemda =pk.kodepemda 
                where tahun <=$tahun and pk.rencana_ag is not null and mp.kodepemda in ('".implode("','",$pemdas)."') 
                group by pk.kodepemda
                order by sum(pk.rencana_ag ) desc limit 5")),

                 'realisasi'=> DB::select(DB::raw("select pk.kodepemda,max(mp.nama_pemda) as nama_pemda,sum(pk.realisasi_ag) as realisasi_ag  from output.ddub pk
                join master.master_pemda as mp on mp.kodepemda =pk.kodepemda 
                where tahun <=$tahun and pk.realisasi_ag is not null and mp.kodepemda in ('".implode("','",$pemdas)."') 
                group by pk.kodepemda
                order by sum(pk.realisasi_ag) desc limit 5"))
            ],
            'pendamping'=>[
                'rencana'=>DB::select(DB::raw("select pk.kodepemda,max(mp.nama_pemda) as nama_pemda,sum(pk.rencana_ag) rencana_ag  from output.dana_pendamping pk
                join master.master_pemda as mp on mp.kodepemda =pk.kodepemda 
                where tahun <=$tahun and pk.rencana_ag is not null and mp.kodepemda in ('".implode("','",$pemdas)."')
                group by pk.kodepemda
                 order by sum(pk.rencana_ag) desc limit 5")),
                'realisasi'=>DB::select(DB::raw("select pk.kodepemda,max(mp.nama_pemda) as nama_pemda,sum(pk.realisasi_ag ) as realisasi_ag 
                from output.dana_pendamping pk
                join master.master_pemda as mp on mp.kodepemda =pk.kodepemda 
                where tahun <=$tahun and pk.realisasi_ag is not null and mp.kodepemda in ('".implode("','",$pemdas)."')
                group by pk.kodepemda
                order by sum(pk.realisasi_ag) desc limit 5"))
            ]
        ];

        $data['tematik_4']=[
            'subsidi'=>DB::select(DB::raw("select pk.kodepemda,mp.nama_pemda,pk.nilai_subsidi  from output.dukungan_pemda_bumdam  pk
                join master.master_pemda as mp on mp.kodepemda =pk.kodepemda 
                where tahun =$tahun and pk.nilai_subsidi >0 and mp.kodepemda in ('".implode("','",$pemdas)."') and pk.subsidi is true order by pk.nilai_subsidi desc limit 5")),
            'fcr'=>DB::select(DB::raw("select pk.kodepemda,mp.nama_pemda,pk.nilai_fcr,(case when (pk.fcr) then 'FCR' else 'NONFCR' end) as kat  from output.dukungan_pemda_bumdam pk
                join master.master_pemda as mp on mp.kodepemda =pk.kodepemda 
                where tahun =$tahun and mp.kodepemda in ('".implode("','",$pemdas)."') and pk.nilai_fcr >0 order by pk.nilai_fcr desc limit 5"))
        ];

        if($request->preview){
            return view('evaluasihasil::dash',$data);
        }else{

        return Pdf::loadView('evaluasihasil::rekap', $data)->stream('evaluasi-dukungan-rekap-'.date('YMdhi').'.pdf');

        }

        return Pdf::loadView('evaluasihasil::rekap', $data)->output();
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('evaluasihasil::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('evaluasihasil::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('evaluasihasil::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
