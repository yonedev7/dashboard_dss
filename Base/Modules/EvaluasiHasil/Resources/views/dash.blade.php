@extends('adminlte::page')
@section('content_header')
    <h4 class="text-center"><b>Hasil Dukungan Pemda Terhadap Kerangka NUWSP</b></h4>
    <h4 class="text-center">Tahun {{$tahun}}</h4>
@stop

@section('content')
<div class="p-3">
    <hr>
    <div class="row">
        <div class="col-12 mb-3">
            <h5><b>Tematik I</b></h5>
            <div class="bg-white rounded shadow p-2">
                <p><b>Derah Dengan Capaian Air Minum Layak Tertinggi</b></p>
                <table class="table table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th>No.</th>
                            <th>Nama Pemda</th>
                            <th>Capaian Layak %</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tematik_1 as $k=>$d)
                            <tr>
                                <td>{{$k+1}}</td>
                                <td>{{$d->nama_pemda}}</td>
                                <td>{{number_format($d->layak,2,'.',',')}}%</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-12 mb-3">
            <h5><b>Tematik II</b></h5>
            <div class="row">
                <div class="col-6">
                    <div class="bg-white rounded shadow p-2">
                    <p><b>Daerah Dengan APBD Air Minum Tertinggi</b></p>

                        <table class="table table-bordered">
                            <thead class="thead-dark">
                                <tr>
                                    <th>
                                        No.
                                    </th>
                                    <th>
                                        Nama Pemda
                                    </th>
                                    <th>
                                        APBD Air Minum Pemda
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($tematik_2['apbd'] as $k=>$d)
                                    <tr>
                                        <td>{{$k+1}}</td>
                                        <td>{{$d->nama_pemda}}</td>
                                        <td>Rp. {{number_format($d->apbd_ar_ag,0,'.',',')}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-6">
                    <div class="bg-white rounded shadow p-2">
                    <p><b>Daerah Dengan Persentase APBD Air Minum Terhadap Total APBD Pemda </b></p>

                        <table class="table table-bordered">
                            <thead class="thead-dark">
                                <tr>
                                    <th>
                                        No.
                                    </th>
                                    <th>
                                        Nama Pemda
                                    </th>
                                    <th>
                                        % APBD Air Minum
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($tematik_2['persentase_apbd'] as $k=>$d)
                                    <tr>
                                        <td>{{$k+1}}</td>
                                        <td>{{$d->nama_pemda}}</td>
                                        <td>{{number_format($d->pres,0,'.',',')}} %</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
        <div class="col-12 mb-3">
            <h5><b>Tematik III</b></h5>
            <div class="row">
                <div class="col-12"><p class="text-center"><b>PEMENUHAN DDUB</b></p></div>
                <div class="col-6">
                    <div class="bg-white rounded shadow p-2">
                    <p><b>Daerah Dengan Perencanaan DDUB Tertinggi  Hingga Tahun {{$tahun}} </b></p>
                        <table class="table table-bordered">
                            <thead class="thead-dark">
                                <tr>
                                    <th>
                                        No.
                                    </th>
                                    <th>
                                        Nama Pemda
                                    </th>
                                    <th>
                                        DDUB Perencanaan
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($tematik_3['ddub']['rencana'] as $k=>$d)
                                    <tr>
                                        <td>{{$k+1}}</td>
                                        <td>{{$d->nama_pemda}}</td>
                                        <td>Rp. {{number_format($d->rencana_ag,0,'.',',')}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-6">
                    <div class="bg-white rounded shadow p-2">
                    <p><b>Daerah Dengan Realisasi DDUB Tertinggi  Hingga Tahun {{$tahun}}  </b></p>

                        <table class="table table-bordered">
                            <thead class="thead-dark">
                                <tr>
                                    <th>
                                        No.
                                    </th>
                                    <th>
                                        Nama Pemda
                                    </th>
                                    <th>
                                         DDUB Realisasi
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($tematik_3['ddub']['realisasi'] as $k=>$d)
                                    <tr>
                                        <td>{{$k+1}}</td>
                                        <td>{{$d->nama_pemda}}</td>
                                        <td>Rp. {{number_format($d->realisasi_ag,0,'.',',')}} </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-12 "><p class="text-center"><b>PEMENUHAN DANA PENDAMPING</b></p></div>
                <div class="col-6">
                    <div class="bg-white rounded shadow p-2">
                    <p><b>Daerah Dengan Perencanaan Dana Pendamping Tertinggi Hingga Tahun {{$tahun}}</b></p>
                        <table class="table table-bordered">
                            <thead class="thead-dark">
                                <tr>
                                    <th>
                                        No.
                                    </th>
                                    <th>
                                        Nama Pemda
                                    </th>
                                    <th>
                                        Perencanaan Dana Pendamping
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($tematik_3['pendamping']['rencana'] as $k=>$d)
                                    <tr>
                                        <td>{{$k+1}}</td>
                                        <td>{{$d->nama_pemda}}</td>
                                        <td>Rp. {{number_format($d->rencana_ag,0,'.',',')}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-6">
                    <div class="bg-white rounded shadow p-2">
                    <p><b>Daerah Dengan Realisasi Dana Pendamping Tertinggi  Hingga Tahun {{$tahun}}  </b></p>

                        <table class="table table-bordered">
                            <thead class="thead-dark">
                                <tr>
                                    <th>
                                        No.
                                    </th>
                                    <th>
                                        Nama Pemda
                                    </th>
                                    <th>
                                        Realisasi Dana Pendamping

                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($tematik_3['pendamping']['realisasi'] as $k=>$d)
                                    <tr>
                                        <td>{{$k+1}}</td>
                                        <td>{{$d->nama_pemda}}</td>
                                        <td>Rp. {{number_format($d->realisasi_ag,0,'.',',')}} </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
        <div class="col-12 mb-3">
            <h5><b>Tematik IV</b></h5>
            <div class="row">
                <div class="col-6">
                    <div class="bg-white rounded shadow p-2">
                    <p><b>Daerah Dengan Nilai Subsidi Kepada BUMDAM Tertinggi</b></p>

                        <table class="table table-bordered">
                            <thead class="thead-dark">
                                <tr>
                                    <th>
                                        No.
                                    </th>
                                    <th>
                                        Nama Pemda
                                    </th>
                                    <th>
                                        Nilai Subsidi 
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($tematik_4['subsidi'] as $k=>$d)
                                    <tr>
                                        <td>{{$k+1}}</td>
                                        <td>{{$d->nama_pemda}}</td>
                                        <td>Rp. {{number_format($d->nilai_subsidi,0,'.',',')}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-6">
                    <div class="bg-white rounded shadow p-2">
                    <p><b>BUMDAM  Dengan Persentase Capaian FCR Tertinggi </b></p>

                        <table class="table table-bordered">
                            <thead class="thead-dark">
                                <tr>
                                    <th>
                                        No.
                                    </th>
                                    <th>
                                        Nama Pemda
                                    </th>
                                    <th>
                                        % FCR
                                    </th>
                                    <th>
                                        Kategori FCR
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($tematik_4['fcr'] as $k=>$d)
                                    <tr>
                                        <td>{{$k+1}}</td>
                                        <td>{{$d->nama_pemda}}</td>
                                        <td>{{number_format($d->nilai_fcr,0,'.',',')}} %</td>
                                        <td>{{$d->kat}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
@stop