<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=0.1, shrink-to-fit=yes">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <style>
            @media print {
              .break_before {page-break-before: always!important}

            }

            .justify{
              text-align: justify;
               text-justify: inter-word;
            }
            .break_before {page-break-before: always!important}

            h1,h2,h3,h4,h5,p,li,td,th{
                font-family:Verdana, Geneva, Tahoma, sans-serif
            }
            .tdata td,.tdata th{
                font-size: 10px!important;
                border-left:1px solid #ddd;
                border-bottom:1px solid #ddd;
                padding: 5px;

            }
            .table {
                display: table;
                border-spacing: 0px;
                border: 1px solid #ddd;
                width:100%;
            }
            .bg-secondary{
                background-color: #f1f1f1;
            }

        </style>
        @php
        function checkZero($a,$b){
            if($a==0 OR $b==0){
                return 0;
            }else{
                return ($a/$b)*100;
            }
        }

        @endphp
    </head>
    <body>


<div class="p-3">
    <img src="{{public_path('assets/img/brand/head.png')}}" style="width:100%;" alt="">

    <hr>
    <div class="row">
        <div class="col-12 mb-3">
            <h5><b>Tematik I</b></h5>
            <div class="bg-white rounded shadow p-2">
                <p><b>Derah Dengan Capaian Air Minum Layak Tertinggi</b></p>
                <table class="table tdata table-bordered">
                    <thead class="bg-secondary">
                        <tr>
                            <th>No.</th>
                            <th>Nama Pemda</th>
                            <th>Capaian Layak %</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tematik_1 as $k=>$d)
                            <tr>
                                <td>{{$k+1}}</td>
                                <td>{{$d->nama_pemda}}</td>
                                <td>{{number_format($d->layak,2,'.',',')}}%</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-12 mb-3">
            <h5><b>Tematik II</b></h5>
            <div class="row">
                <div class="col-6">
                    <div class="bg-white rounded shadow p-2">
                    <p><b>Daerah Dengan APBD Air Minum Tertinggi</b></p>

                        <table class="table tdata table-bordered">
                            <thead class="bg-secondary">
                                <tr>
                                    <th>
                                        No.
                                    </th>
                                    <th>
                                        Nama Pemda
                                    </th>
                                    <th>
                                        APBD Air Minum Pemda
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($tematik_2['apbd'] as $k=>$d)
                                    <tr>
                                        <td>{{$k+1}}</td>
                                        <td>{{$d->nama_pemda}}</td>
                                        <td>Rp. {{number_format($d->apbd_ar_ag,0,'.',',')}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-6">
                    <div class="bg-white rounded shadow p-2">
                    <p><b>Daerah Dengan Persentase APBD Air Minum Terhadap Total APBD Pemda </b></p>

                        <table class="table tdata table-bordered">
                            <thead class="bg-secondary">
                                <tr>
                                    <th>
                                        No.
                                    </th>
                                    <th>
                                        Nama Pemda
                                    </th>
                                    <th>
                                        % APBD Air Minum
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($tematik_2['persentase_apbd'] as $k=>$d)
                                    <tr>
                                        <td>{{$k+1}}</td>
                                        <td>{{$d->nama_pemda}}</td>
                                        <td>{{number_format($d->pres,0,'.',',')}} %</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
        <div class="col-12 mb-3">
            <h5><b>Tematik III</b></h5>
            <div class="row">
                <div class="col-12"><p class="text-center"><b>PEMENUHAN DDUB</b></p></div>
                <div class="col-6">
                    <div class="bg-white rounded shadow p-2">
                    <p><b>Daerah Dengan Perencanaan DDUB Tertinggi  Hingga Tahun {{$tahun}} </b></p>
                        <table class="table tdata table-bordered">
                            <thead class="bg-secondary">
                                <tr>
                                    <th>
                                        No.
                                    </th>
                                    <th>
                                        Nama Pemda
                                    </th>
                                    <th>
                                        DDUB Perencanaan
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($tematik_3['ddub']['rencana'] as $k=>$d)
                                    <tr>
                                        <td>{{$k+1}}</td>
                                        <td>{{$d->nama_pemda}}</td>
                                        <td>Rp. {{number_format($d->rencana_ag,0,'.',',')}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-6">
                    <div class="bg-white rounded shadow p-2">
                    <p><b>Daerah Dengan Realisasi DDUB Tertinggi  Hingga Tahun {{$tahun}}  </b></p>

                        <table class="table tdata table-bordered">
                            <thead class="bg-secondary">
                                <tr>
                                    <th>
                                        No.
                                    </th>
                                    <th>
                                        Nama Pemda
                                    </th>
                                    <th>
                                         DDUB Realisasi
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($tematik_3['ddub']['realisasi'] as $k=>$d)
                                    <tr>
                                        <td>{{$k+1}}</td>
                                        <td>{{$d->nama_pemda}}</td>
                                        <td>Rp. {{number_format($d->realisasi_ag,0,'.',',')}} </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-12 "><p class="text-center"><b>PEMENUHAN DANA PENDAMPING</b></p></div>
                <div class="col-6">
                    <div class="bg-white rounded shadow p-2">
                    <p><b>Daerah Dengan Perencanaan Dana Pendamping Tertinggi Hingga Tahun {{$tahun}}</b></p>
                        <table class="table tdata table-bordered">
                            <thead class="bg-secondary">
                                <tr>
                                    <th>
                                        No.
                                    </th>
                                    <th>
                                        Nama Pemda
                                    </th>
                                    <th>
                                        Perencanaan Dana Pendamping
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($tematik_3['pendamping']['rencana'] as $k=>$d)
                                    <tr>
                                        <td>{{$k+1}}</td>
                                        <td>{{$d->nama_pemda}}</td>
                                        <td>Rp. {{number_format($d->rencana_ag,0,'.',',')}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-6">
                    <div class="bg-white rounded shadow p-2">
                    <p><b>Daerah Dengan Realisasi Dana Pendamping Tertinggi  Hingga Tahun {{$tahun}}  </b></p>

                        <table class="table tdata table-bordered">
                            <thead class="bg-secondary">
                                <tr>
                                    <th>
                                        No.
                                    </th>
                                    <th>
                                        Nama Pemda
                                    </th>
                                    <th>
                                        Realisasi Dana Pendamping

                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($tematik_3['pendamping']['realisasi'] as $k=>$d)
                                    <tr>
                                        <td>{{$k+1}}</td>
                                        <td>{{$d->nama_pemda}}</td>
                                        <td>Rp. {{number_format($d->realisasi_ag,0,'.',',')}} </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
        <div class="col-12 mb-3">
            <h5><b>Tematik IV</b></h5>
            <div class="row">
                <div class="col-6">
                    <div class="bg-white rounded shadow p-2">
                    <p><b>Daerah Dengan Nilai Subsidi Kepada BUMDAM Tertinggi</b></p>

                        <table class="table tdata table-bordered">
                            <thead class="bg-secondary">
                                <tr>
                                    <th>
                                        No.
                                    </th>
                                    <th>
                                        Nama Pemda
                                    </th>
                                    <th>
                                        Nilai Subsidi 
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($tematik_4['subsidi'] as $k=>$d)
                                    <tr>
                                        <td>{{$k+1}}</td>
                                        <td>{{$d->nama_pemda}}</td>
                                        <td>Rp. {{number_format($d->nilai_subsidi,0,'.',',')}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-6">
                    <div class="bg-white rounded shadow p-2">
                    <p><b>BUMDAM  Dengan Persentase Capaian FCR Tertinggi </b></p>

                        <table class="table tdata table-bordered">
                            <thead class="bg-secondary">
                                <tr>
                                    <th>
                                        No.
                                    </th>
                                    <th>
                                        Nama Pemda
                                    </th>
                                    <th>
                                        % FCR
                                    </th>
                                    <th>
                                        Kategori FCR
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($tematik_4['fcr'] as $k=>$d)
                                    <tr>
                                        <td>{{$k+1}}</td>
                                        <td>{{$d->nama_pemda}}</td>
                                        <td>{{number_format($d->nilai_fcr,0,'.',',')}} %</td>
                                        <td>{{$d->kat}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
    </body>
</html>