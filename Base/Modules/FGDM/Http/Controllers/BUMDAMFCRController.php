<?php

namespace Modules\FGDM\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use \Modules\FGDM\Http\Controllers\FGDMController;

class BUMDAMFCRController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index($tahun,Request $request)
    {   
       
            $data=DB::table('output.dukungan_pemda_bumdam as d')
            ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
            ->join('master.master_bu as bu','bu.id','=','d.kodebu')
            ->groupBy('d.tahun')
            ->groupBy('d.kodebu')
            ->orderBy('d.tahun','asc')
            ->where('d.tahun','<=',$tahun)
            ->selectRaw("d.tahun,string_agg(d.kodepemda::text,',') as pemdas_kode, 
            max(d.nilai_fcr) as nilai_fcr,
            max(bu.nama_bu) as nama_bu,
            max(d.fcr::integer) as fcr");
            $scope_pemda=FGDMController::filter_scope_pemda($tahun,$request);
            $data=$data->whereIn('d.kodepemda',$scope_pemda['kode']??['0000']);
            $data=$data->get();

            return [
                'code'=>200,
                'sumber'=>'Evaluasi Dukungan Pemda  Dalam Kerangka NUWSP Tahun '.$tahun,
                'title'=>'BUMDAM FCR - Pemda '.($scope_pemda['pemda']?' - '.$scope_pemda['pemda']['nama_pemda']:''),
                'pemda'=>$scope_pemda['pemda'],
                'data'=>[
                    'chart_type'=>'column',
                    'data'=>$data,
                    'y'=>[
                        [
                            'name'=>'Nama Bumdam',
                            'key'=>'nama_bu',
                            'number'=>false,
                            'satuan'=>'-',
                        ],
                        [
                            'name'=>'FCR',
                            'key'=>'fcr',
                            'number'=>false,

                            'satuan'=>'Rp',
                        ],
                        [
                            'name'=>'Nilai FCR ',
                            'key'=>'nilai_fcr',
                            'number'=>true,
                            'satuan'=>'SR',
                        ],
                       
                    ],
                    'x'=>[
                        'name'=>'Tahun',
                        'key'=>'tahun',
                        'satuan'=>null
                    ]
                ]
            ];

        
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('fgdm::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('fgdm::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('fgdm::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
