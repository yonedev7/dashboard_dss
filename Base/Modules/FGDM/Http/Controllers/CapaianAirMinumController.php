<?php

namespace Modules\FGDM\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use \Modules\FGDM\Http\Controllers\FGDMController;
class CapaianAirMinumController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    
    public function index($tahun,Request $request)
    {   
       
            $data=DB::table('output.capaian_air_minum as d')
            ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
            ->groupBy('d.tahun')
            ->orderBy('d.tahun','asc')
            ->where('d.tahun','<=',$tahun)
            ->selectRaw("d.tahun,string_agg(d.kodepemda::text,',') as pemdas_kode,
             sum(d.kk) as kk,
             sum(bumdam_kk) as bumdam_kk,
             sum(nonbumdam_kk) as nonbumdam_kk,
             ( sum(bumdam_kk)+sum(nonbumdam_kk)) as total_jp,
             sum(bjp_kk) as bjp_kk,
             ( sum(bumdam_kk)+sum(nonbumdam_kk)+sum(bjp_kk)) as air_layak,
             sum(d.kk)-( sum(bumdam_kk)+sum(nonbumdam_kk)+sum(bjp_kk)) as belum_terlayani     
             ");

             $scope_pemda=FGDMController::filter_scope_pemda($tahun,$request);

            $data=$data->whereIn('d.kodepemda',$scope_pemda['kode']??['0000']);
            

            $data=$data->get();



            return [
                'code'=>200,
                'sumber'=>'Evaluasi Dukungan Pemda  Dalam Kerangka NUWSP Tahun '.$tahun,
                'title'=>'Capaian Air Minum Pemda '.($scope_pemda['pemda']?' - '.$scope_pemda['pemda']['nama_pemda']:''),
                'pemda'=>$scope_pemda['pemda'],
                'data'=>[
                    'chart_type'=>'column',
                    'data'=>$data,
                    'y'=>[
                        [
                            'name'=>'Jumlah Total Keluarga (KK)',
                            'key'=>'kk',
                            'satuan'=>'KK',
                        ],
                        [
                            'name'=>'JP BUMDAM (KK)',
                            'key'=>'bumdam_kk',
                            'satuan'=>'KK',
                        ],
                        [
                            'name'=>'JP NON BUMDAM (KK)',
                            'key'=>'nonbumdam_jkk',
                            'satuan'=>'KK',
                        ],

                        [
                            'name'=>'JP Total (KK)',
                            'key'=>'total_jp',
                            'satuan'=>'KK',
                        ],

                        [
                            'name'=>'BJP (KK)',
                            'key'=>'bjp_kk',
                            'satuan'=>'KK',
                        ],
                        [
                            'name'=>'Pemenuhan Air Minum Layak (KK)',
                            'key'=>'air_layak',
                            'satuan'=>'KK',
                        ],
                        [
                            'name'=>'Belum Terlayani Air Minum (KK)',
                            'key'=>'belum_terlayani',
                            'satuan'=>'KK',
                        ],
                        
                    ],
                    'x'=>[
                        'name'=>'Tahun',
                        'key'=>'tahun',
                        'satuan'=>null
                    ]
                ]
            ];



    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('fgdm::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('fgdm::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('fgdm::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
