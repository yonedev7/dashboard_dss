<?php

namespace Modules\FGDM\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use \Modules\FGDM\Http\Controllers\FGDMController;

class DDUBController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

    public $config_dts;
    function __construct() {
        // $this->config_dts = config('fgdm.dataset.ddub');
        // $this->dts=DB::table('master.dataset')->where('id',$this->config_dts['id'])->first();
    }


    public function index($tahun,Request $request)
    {   
       
            $data=DB::table('output.ddub as d')
            ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
            ->groupBy('d.tahun')
            ->orderBy('d.tahun','asc')
            ->where('d.tahun','<=',$tahun)
            ->selectRaw("d.tahun,string_agg(d.kodepemda::text,',') as pemdas_kode, sum(d.rencana_ag) as rencana,sum(d.realisasi_ag) as realisasi,sum(rencana_sr) as rencana_sr,sum(realisasi_sr) as realisasi_sr");
            $scope_pemda=FGDMController::filter_scope_pemda($tahun,$request);

            $data=$data->whereIn('d.kodepemda',$scope_pemda['kode']??['0000']);

           
            $data=$data->get();

            return [
                'code'=>200,
                'sumber'=>'Evaluasi Dukungan Pemda  Dalam Kerangka NUWSP Tahun '.$tahun,
                'title'=>'DDUB Pemda '.($scope_pemda['pemda']?' - '.$scope_pemda['pemda']['nama_pemda']:''),
                'data'=>[
                    'chart_type'=>'column',
                    'data'=>$data,
                    'y'=>[
                        [
                            'name'=>'Rencana (Rp)',
                            'key'=>'rencana',
                            'satuan'=>'Rp',
                        ],
                        [
                            'name'=>'Rencana SR ',
                            'key'=>'rencana_sr',
                            'satuan'=>'SR',
                        ],
                        [
                            'name'=>'Realisasi (Rp)',
                            'key'=>'realisasi',
                            'satuan'=>'Rp',
                        ],
                        [
                            'name'=>'Realisasi SR ',
                            'key'=>'realisasi_sr',
                            'satuan'=>'SR',
                        ],
                    ],
                    'x'=>[
                        'name'=>'Tahun',
                        'key'=>'tahun',
                        'satuan'=>null
                    ]
                ]
            ];

        
    }

   
}
