<?php

namespace Modules\FGDM\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
class DanaPendampingController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index($tahun,Request $request)
    {   
       
            $data=DB::table('output.dana_pendamping as d')
            ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
            ->join('master.pemda_lokus as lk',[
                ['lk.tipe_bantuan','=',DB::raw("'PENDAMPING'")],
                ['lk.kodepemda','=','d.kodepemda'],

            ])->groupBy('d.tahun')
            ->orderBy('d.tahun','asc')
            ->where('d.tahun','<=',$tahun)
            ->selectRaw("d.tahun,string_agg(d.kodepemda::text,',') as pemdas_kode, sum(d.rencana_ag) as rencana,sum(d.realisasi_ag) as realisasi,sum(rencana_sr) as rencana_sr,sum(realisasi_sr) as realisasi_sr");

            if($request->kodepemda){
                $data=$data->where('d.kodepemda',$request->kodepemda);
            }
            $data=$data->get();

            return [
                'code'=>200,
                'sumber'=>'Evaluasi Dukungan Pemda Tahun'.$tahun,
                'title'=>'Dana Pendamping PEMDA',
                'data'=>[
                    'chart_type'=>'column',
                    'data'=>$data,
                    'y'=>[
                        [
                            'name'=>'Rencana (Rp)',
                            'key'=>'rencana',
                            'satuan'=>'Rp',
                        ],
                        [
                            'name'=>'Rencana SR ',
                            'key'=>'rencana_sr',
                            'satuan'=>'SR',
                        ],
                        [
                            'name'=>'Realisasi (Rp)',
                            'key'=>'realisasi',
                            'satuan'=>'Rp',
                        ],
                        [
                            'name'=>'Realisasi SR ',
                            'key'=>'realisasi_sr',
                            'satuan'=>'SR',
                        ],
                    ],
                    'x'=>[
                        'name'=>'Tahun',
                        'key'=>'tahun',
                        'satuan'=>null
                    ]
                ]
            ];

        
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('fgdm::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('fgdm::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('fgdm::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
