<?php

namespace Modules\FGDM\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use Storage;
use Barryvdh\DomPDF\Facade\Pdf;

class FGDMController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

     public function pdf($tahun,$kodepemda,Request $request){
        if(!$kodepemda){
            $kodepemda='LONGLIST';
        }

        $request['kodepemda']=$kodepemda;

        $ll=static::filter_scope_pemda($tahun,$request);
        $path=(module_path('fgdm').'/StorageTmp/'.($kodepemda?"profil-pemda/".$tahun."/".$kodepemda.".json":"/def.json"));
        if(file_exists($path)){
            $schema=json_decode(file_get_contents($path),true);

        }else{
            $schema=json_decode(file_get_contents(module_path('fgdm').'/StorageTmp/def.json'),true);
        }

        $component_list=json_decode(file_get_contents(module_path('fgdm').'/StorageTmp/components_schema.json'),true);

        $pdf= Pdf::setOption([
            'isJavascriptEnabled'=>true,
            'javascriptDelay'=>500000,
            'debugPng'=>true,
            'isHtml5ParserEnabled'=>true

        ])->loadView('fgdm::draf_preview',
                ['pemda'=>$ll,'meta_pemda'=>$ll['pemda'],'schema'=>$schema,'re'=>$request,'component_list'=>$component_list]
        );

        // return $pdf;
        // $pdf=$pdf->setOption('enable-javascript', true);
        // $pdf=$pdf->setOption('javascript-delay', 50000);
        // $pdf= $pdf->setOption('enable-smart-shrinking', true);
        // $pdf=$pdf->setOption('no-stop-slow-scripts', true);

        return $pdf->stream('profile-pemda-'.$ll['pemda']['nama_pemda'].'-tahun-'.$tahun.'.pdf');
     }

     public function pPemdaCreater($tahun,$kodepemda,Request $request){
    

        if(!$kodepemda){
            $kodepemda='LONGLIST';
        }

        $request['kodepemda']=$kodepemda;

        $ll=static::filter_scope_pemda($tahun,$request);
        
        $path=(module_path('fgdm').'/StorageTmp/'.($kodepemda?"/profile-pemda/".$tahun."/".$kodepemda.".json":"/def.json"));

        if(file_exists($path)){
            $schema=json_decode(file_get_contents($path),true);

        }else{
            $schema=json_decode(file_get_contents(module_path('fgdm').'/StorageTmp/def.json'),true);
        }

        $component_list=json_decode(file_get_contents(module_path('fgdm').'/StorageTmp/components_schema.json'),true);

        $finger=$request->fingerprint();
        return view('fgdm::build_schema.creator')->with(
            ['pemda'=>$ll,'fingerprint'=>$finger,'meta_pemda'=>$ll['pemda'],'schema'=>$schema,'re'=>$request,'component_list'=>$component_list]
        );
    
     }

    public function draf($tahun,$kodepemda,Request $request){

        Storage::disk('module-fgdm')->put($tahun.'/'.$request->finger_id.'/'.$kodepemda.'.json',json_encode($request->data));
        return [
            'code'=>200,
        ];

    }

    public function publish($tahun,$kodepemda,Request $request){
        Storage::disk('module-fgdm')->put('profile-pemda/'.$tahun.'/'.$kodepemda.'.json',json_encode($request->data));
        return [
            'code'=>200,
        ];

    }

    public function showDraf($tahun,$kodepemda,$finger,Request $request){

        if(!$kodepemda){
            $kodepemda='LONGLIST';
        }

        $request['kodepemda']=$kodepemda;

        $ll=static::filter_scope_pemda($tahun,$request);
        $path=(module_path('fgdm').'/StorageTmp/'.($kodepemda?"".$tahun."/".$finger."/".$kodepemda.".json":"/def.json"));
        // dd($path);
        if(file_exists($path)){
            $schema=json_decode(file_get_contents($path),true);

        }else{
            $schema=json_decode(file_get_contents(module_path('fgdm').'/StorageTmp/def.json'),true);
        }

        // dd($schema);

        $component_list=json_decode(file_get_contents(module_path('fgdm').'/StorageTmp/components_schema.json'),true);
        return view('fgdm::draf_preview')->with(
            ['pemda'=>$ll,'meta_pemda'=>$ll['pemda'],'schema'=>$schema,'re'=>$request,'component_list'=>$component_list]
        );

    }

    static public function filter_scope_pemda($tahun,Request $request){
        $where=[];
        $pemda=null;


        if(str_contains($request->kodepemda,'SORTLIST')){
            $sort_list=explode(' ',$request->kodepemda);
            if(count($sort_list)>1){
                $where=[
                    'lk.tahun_proyek'=>$sort_list[1]
                ];

                $pemda=[
                    'nama_pemda'=>'Pemda Sortlist '.$sort_list[1],
                    'kodepemda'=>$request->kodepemda,
                    'regional_1'=>'-'
                ];

            }else{
                $where=[
                    'lk.tahun_proyek'=>$tahun
                ];

                $pemda=[
                    'nama_pemda'=>'Pemda Sortlist '.$tahun,
                    'kodepemda'=>$request->kodepemda,
                    'regional_1'=>'-'
                ];
            }  
        }else{
            $kk=(int)$request->kodepemda??0;
            if($kk>10){
                $pemda=(array)DB::table('master.master_pemda')
                ->selectRaw("INITCAP(nama_pemda) as nama_pemda,kodepemda,regional_1,regional_2")
                ->where('kodepemda',$request->kodepemda)->first();

                $where=[
                    'mp.kodepemda'=>$request->kodepemda
                ];
            }else{
                $pemda=[
                    'nama_pemda'=>'Longlist NUWSP',
                    'kodepemda'=>$request->kodepemda,
                    'regional_1'=>'-'
                ];
            }

        }

        $scope_pemda=DB::table('master.master_pemda as mp')
        ->join('master.pemda_lokus as lk','lk.kodepemda','=','mp.kodepemda');
        if(count($where)){
            $scope_pemda=$scope_pemda->where($where);
        }
        $scope_pemda=$scope_pemda->groupBy('mp.kodepemda')->selectRaw('mp.kodepemda')->get()->pluck('kodepemda');


        return [
            'pemda'=>$pemda,
            'kode'=>$scope_pemda->toArray()
        ];
    } 

    public function index($tahun,Request $request)
    {
       if(!$request->kodepemda){
        $request['kodepemda']='LONGLIST';
       }

        $path=(module_path('fgdm').'/StorageTmp/'.($request->kodepemda?"/profile-pemda/".$tahun."/".$request->kodepemda.".json":"/def.json"));

        if(file_exists($path)){
            $schema=json_decode(file_get_contents($path),true);

        }else{
            $schema=json_decode(file_get_contents(module_path('fgdm').'/StorageTmp/def.json'),true);
        }

        $ll=static::filter_scope_pemda($tahun,$request);

        $pemdas=DB::table('master.master_pemda as mp')
        ->join('master.pemda_lokus as lk','lk.kodepemda','=','mp.kodepemda')
        ->select('mp.*')
        ->orderBy('mp.nama_pemda','asc')
        // ->whereIn('mp.kodepemda',$ll['kode'])
        ->groupBy('mp.kodepemda')->get();

        return view('fgdm::index')->with(['meta_pemda'=>$ll['pemda'],'re'=>$request,'pemda_list'=>$pemdas,'schema'=>$schema]);
    }

   public function builder($tahun,$kodepemda,Request $request){
        if(!$kodepemda){
            $kodepemda='LONGLIST';
        }



        $request['kodepemda']=$kodepemda;

        $ll=static::filter_scope_pemda($tahun,$request);

        
        $path=(module_path('fgdm').'/StorageTmp/'.($kodepemda?"/profile-pemda/".$tahun."/".$kodepemda.".json":"/def.json"));

        if(file_exists($path)){
            $schema=json_decode(file_get_contents($path),true);

        }else{
            $schema=json_decode(file_get_contents(module_path('fgdm').'/StorageTmp/def.json'),true);
        }

        $component_list=json_decode(file_get_contents(module_path('fgdm').'/StorageTmp/components_schema.json'),true);
        return view('fgdm::build_schema.update')->with(
            ['pemda'=>$ll,'meta_pemda'=>$ll['pemda'],'schema'=>$schema,'re'=>$request,'component_list'=>$component_list]
        );
   }
   
}
