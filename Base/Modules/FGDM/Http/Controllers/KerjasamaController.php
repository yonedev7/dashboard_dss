<?php

namespace Modules\FGDM\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use \Modules\FGDM\Http\Controllers\FGDMController;

class KerjasamaController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

    function __construct() {
        $this->config_dts = config('fgdm.dataset.kerjasama');
        $this->dts=DB::table('master.dataset')->where('id',$this->config_dts['id'])->first();
    }
    public function index($tahun,Request $request)
    {

        $data=DB::table(DB::raw('dataset.'.$this->dts->table.' as d'))
        ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
        ->join('master.pemda_lokus as lk',[
            ['lk.kodepemda','=','d.kodepemda']

        ])->groupBy('d.tahun')
        ->orderBy('d.tahun','asc')
        ->where('d.tahun','<=',$tahun)
        ->selectRaw("d.tahun,
        count(distinct(d.mitra_kerjasama)) as jum_mitra,
        string_agg(distinct(concat('(',d.mitra_kerjasama,' - ',d.jenis_kerjasama,')')),',') as mitra ,
        sum(d.nilai_kontrak_kerjasama) as nilai");
        $scope_pemda=FGDMController::filter_scope_pemda($tahun,$request);

        $data=$data->whereIn('d.kodepemda',$scope_pemda['kode']??['0000']);

        
        $data=$data->get();

        return [
            'code'=>200,
            'sumber'=>'Kerjasama Non Public Pemda '.$tahun,
            'title'=>'Kerjasama Non Public Pemda '.($scope_pemda['pemda']?' - '.$scope_pemda['pemda']['nama_pemda']:''),
            'pemda'=> $scope_pemda['pemda'],
            'data'=>[
                'chart_type'=>'column',
                'data'=>$data,
                'y'=>[
                    [
                        'name'=>'Jumlah Mitra',
                        'key'=>'jum_mitra',
                        'satuan'=>'Mitra',
                    ],
                    [
                        'name'=>'Mitra',
                        'key'=>'mitra',
                        'satuan'=>'-',
                    ],
                    [
                        'name'=>'Nilai Kerjasama (Rp.)',
                        'key'=>'nilai',
                        'satuan'=>'Rp',



                    ],
                ],
                'x'=>[
                    'name'=>'Tahun',
                    'key'=>'tahun',
                    'satuan'=>null
                ]
            ]
        ];


    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('fgdm::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('fgdm::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('fgdm::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
