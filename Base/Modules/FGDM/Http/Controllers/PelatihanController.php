<?php

namespace Modules\FGDM\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use \Modules\FGDM\Http\Controllers\FGDMController;

class PelatihanController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index($tahun,Request $request)
    {   
       
            $data=DB::table('dataset.pemda_pelatihan as d')
            ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
            ->where('d.tahun','<=',$tahun)
            ->selectRaw("d.tahun,
            d.kodepemda as pemdas_kode,
             (d.tema_pelatihan) as tema_pelatihan,
             mp.nama_pemda as nama_pemda,
             concat(d.date_mulai,' - ',d.date_selesai) as pelaksanaan,
             d.jumlah_peserta as p_l,
             d.peserta_disable as p_l_d,
             concat(d.tahun,' -',mp.nama_pemda) as x,
             d.jumlah_peserta_perempuan as p_p,
             d.peserta_perempuan_disable as p_p_d,
             ( d.jumlah_peserta +  d.jumlah_peserta_perempuan + d.peserta_disable + d.peserta_perempuan_disable ) as total_peserta
            ")->orderBy(DB::raw("concat(d.tahun,' -',mp.nama_pemda)",'asc'));

            $scope_pemda=FGDMController::filter_scope_pemda($tahun,$request);

            $data=$data->whereIn('d.kodepemda',$scope_pemda['kode']??['0000']);

           
            $data=$data->get();

            return [
                'code'=>200,
                'sumber'=>'Evaluasi Dukungan Pemda  Dalam Kerangka NUWSP Tahun '.$tahun,
                'title'=>'DDUB Pemda '.($scope_pemda['pemda']?' - '.$scope_pemda['pemda']['nama_pemda']:''),
                'pemda'=>$scope_pemda['pemda'],
                'data'=>[
                    'chart_type'=>'column',
                    'data'=>$data,
                    'y'=>[
                        [
                            'name'=>'Nama Pemda',
                            'key'=>'nama_pemda',
                            'chart'=>false,
                            'number'=>false,
                            'satuan'=>null,
                        ],
                        [
                            'name'=>'Tema Pelatihan',
                            'key'=>'tema_pelatihan',
                            'chart'=>false,
                            'number'=>false,
                            'satuan'=>null,
                        ],
                        [
                            'name'=>'Pelaksanaan',
                            'key'=>'pelaksanaan',
                            'chart'=>false,
                            'number'=>false,
                            'satuan'=>null,
                        ],
                        [
                            'name'=>'Jumlah Peserta',
                            'key'=>'total_peserta',
                            'chart'=>true,
                            'number'=>true,

                            'satuan'=>'Orang',
                        ],
                        [
                            'name'=>'Peserta Laki',
                            'key'=>'p_l',
                            'chart'=>true,
                            'number'=>true,


                            'satuan'=>'Orang',
                        ],
                        [
                            'name'=>'Peserta Laki Disable',
                            'key'=>'p_l_d',
                            'chart'=>true,
                            'number'=>true,


                            'satuan'=>'Orang',
                        ],
                        [
                            'name'=>'Peserta Perempuan',
                            'key'=>'p_l',
                            'chart'=>true,
                            'number'=>true,


                            'satuan'=>'Orang',
                        ],
                        [
                            'name'=>'Peserta Perempuan Disable',
                            'key'=>'p_p_d',
                            'chart'=>true,
                            'number'=>true,

                            'satuan'=>'Orang',
                        ],
                        
                    ],
                    'x'=>[
                        'name'=>'Tahun',
                        'key'=>'tahun',
                        'satuan'=>null
                    ]
                ]
            ];

        
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('fgdm::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('fgdm::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('fgdm::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
