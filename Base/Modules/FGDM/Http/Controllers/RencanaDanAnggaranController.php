<?php

namespace Modules\FGDM\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;

class RencanaDanAnggaranController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index($tahun,Request $request)
    {   
       
            $data=DB::table('output.program_kegiatan as d')
            ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
            ->groupBy('d.tahun')
            ->orderBy('d.tahun','asc')
            ->where('d.tahun','<=',$tahun)
            ->selectRaw("d.tahun,string_agg(d.kodepemda::text,',') as pemdas_kode,
             sum(d.rkpd_ar_sub) as rkpd_ar_sub,
             sum(d.rkpd_ar_ag) as rkpd_ar_ag,
             sum(d.apbd_ar_sub) as apbd_ar_sub,
            sum(d.apbd_ar_ag) as apbd_ar_ag,
             sum(d.apbd_tot) as apbd_tot,
            ( sum(d.apbd_ar_ag) - sum(d.rkpd_ar_ag)) as deviasi
             ");

             $scope_pemda=FGDMController::filter_scope_pemda($tahun,$request);

            $data=$data->whereIn('d.kodepemda',$scope_pemda['kode']??['0000']);
            

            $data=$data->get();



            return [
                'code'=>200,
                'sumber'=>'Evaluasi Dukungan Pemda  Dalam Kerangka NUWSP Tahun '.$tahun,
                'title'=>'Perencanaan dan Pengangaran '.($scope_pemda['pemda']?' - '.$scope_pemda['pemda']['nama_pemda']:''),
                'pemda'=>$scope_pemda['pemda'],
                'data'=>[
                    'chart_type'=>'column',
                    'data'=>$data,
                    'y'=>[
                        [
                            'name'=>'Jumlah Kegiatan RKPD AM (Kegiatan)',
                            'key'=>'rkpd_ar_sub',
                            'satuan'=>'Kegiatan',
                        ],
                        [
                            'name'=>'Jumlah Pagu RKPD AM (Rp.)',
                            'key'=>'rkpd_ar_ag',
                            'satuan'=>'Rp',
                        ],
                        [
                            'name'=>'Jumlah Kegiatan APBD AM (Kegiatan)',
                            'key'=>'rkpd_ar_sub',
                            'satuan'=>'Kegiatan',
                        ],
                        [
                            'name'=>'Jumlah Anggaran APBD AM (Rp.)',
                            'key'=>'rkpd_ar_ag',
                            'satuan'=>'Rp',
                        ],
                        [
                            'name'=>'Deviasi Perencanaan dan Anggaran AM (Rp.)',
                            'key'=>'deviasi',
                            'satuan'=>'Rp - Deviasi',
                        ],
                        [
                            'name'=>'Total APBD',
                            'key'=>'apbd_tot',
                            'satuan'=>'Rp',
                        ],

                        
                        
                    ],
                    'x'=>[
                        'name'=>'Tahun',
                        'key'=>'tahun',
                        'satuan'=>null
                    ]
                ]
            ];



    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('fgdm::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('fgdm::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('fgdm::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
