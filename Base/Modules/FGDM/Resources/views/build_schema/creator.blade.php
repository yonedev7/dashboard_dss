@extends('adminlte::page')

@section('content')

<div class="bg-white shadow  d-flex flex-column "  id="canva_builder_com" style="min-height:80vh;">
    <div class="p-3 border-bottom border-dark">
        <h5>Builder Profile {{$meta_pemda['nama_pemda']}} </h5>
        <div class="btn-group">
            <a class="btn btn-primary" href="javascript:void(0)" @click="saveSchema()">Save</a>
            <a href="javascript:void(0)" @click="togglePreview"  class="btn btn-warning">@{{preview?'Edit Mode':'Preview'}}</a>
        </div>
    </div>

    <div class="d-flex" style="min-height: 55vh;" v-if="preview==null" >
        <div class="componet-builder flex-grow-1 p-3  border-dark border-right flex-grow-1 "  style="overflow-y: scroll" >
                <yn-select-com :com_enable="com_list" :com="schema" ></yn-select-com>
        </div>
        
    </div>
    <div v-else class="flex-grow-1 w-100">
        <iframe :src="preview" class="w-100 h-100" style=""></iframe>   
    </div>
</div>
@stop

@section('js')
<script src="{{asset('js/app_builder.js?v='.date('is'))}}"></script>

<script>

    Vue.component('yn-column',yn_column.default);
    Vue.component('yn-loop-data',yn_loop_data.default);
    Vue.component('yn-data',yn_data.default);
    Vue.component('yn-markdown',yn_markdown.default);

    Vue.component('yn-select-com',yn_select_data.default);
    Vue.component('yn-section-cascading',yn_section_cascading.default);

    var canva_builder=(new Vue({
        el:"#canva_builder_com",
        data:{
            com_list:<?=json_encode($component_list)?>,
            schema:<?=json_encode($schema)?>,
            preview:null,
            link_preview:'{{route('fgdm.schema.draf',['tahun'=>$StahunRoute,'kodepemda'=>$meta_pemda['kodepemda'],'finger'=>$fingerprint])}}'
        },
        watch:{
            schema:{
                deep:true,
                handler:function(){
                    this.drafSchema();
                }
            }
        },
        methods: {
            togglePreview:function(){
                if(this.preview==null){
                    this.preview=this.link_preview;
                }else{
                    this.preview=null;
                }
            },
            saveSchema:function(){

                var self=this;
                req_ajax.post('{{route('fgdm.store',['tahun'=>$StahunRoute,'kodepemda'=>$meta_pemda['kodepemda'],'finger_id'=>$fingerprint])}}',{
                    data:this.schema
                }).then(res=>{
                    console.log(res);
                });

            },
            drafSchema:function(){
                var self=this;
                req_ajax.post('{{route('fgdm.draf.store',['tahun'=>$StahunRoute,'kodepemda'=>$meta_pemda['kodepemda'],'finger_id'=>$fingerprint])}}',{
                    data:this.schema
                }).then(res=>{
                    console.log(res);
                });
            }
        },
    }))
</script>

@stop