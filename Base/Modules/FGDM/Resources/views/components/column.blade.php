<div class="d-flex align-items-center">
   
    @foreach($data_collection as $item)
        <div class="mx-1" style="width:{{100/count($data_collection)}}%">
            @include($item['com_id'],['options'=>isset($item['options'])?$item['options']:[],'data_collection'=>isset($item['data_collection'])?$item['data_collection']:[]])
        </div>
    @endforeach
</div>