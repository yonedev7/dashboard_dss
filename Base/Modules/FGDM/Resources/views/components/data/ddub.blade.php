@php
$id=rand(0,1000);

if($re->kodepemda){
    $options['kodepemda']=$re->kodepemda;
}
@endphp


<div id="ddub-{{$id}}">
    <div v-if="print_mode==false" class=" border-top-0 border-left-0 border border-right-0 pb-2 border-dark">
        <div class="btn-group">
            <button class="btn btn-primary" @click="loadData">Reload</button>
        </div>
    </div>
    <div v-if="meta.display.text">
        <p>Komitmen DDUB sebesar Rp. @{{parseNum(na.rencana_ag)}} dengan target SR @{{parseNum(na.rencana_sr)}}. Realisasi DDB sebesar Rp.@{{parseNum(na.realisasi_ag)}} dan Capaian SR @{{parseNum(na.realisasi_sr)}} sehingga yang belum terrealisasi @{{parseNum(na.rencana_sr-na.realisasi_sr)}} SR</p>
    </div>
    <charts :options="op" v-if="meta.display.chart"  class="bg-white shadow p-3"></charts>
   
    <table v-if="meta.display.table" class="table table-bordered table-striped bg-white shadow " >
        <thead class="thead-dark">
            <tr>
                <th v-for="item in column">@{{item.name}}</th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="item in data">
                <td v-for="c in column" :class="c.name=='Tahun'?'bg-dark':''">
                    @{{parseNum(parseFloat(item[c.key]))}}
                    <template v-if="c.name=='Tahun' && meta.kodepemda[0]!='0'">
                        <br>
                        <small>@{{parseNum(item['pemdas_kode'].split(',').length)}} Pemda</small>
                    </template>    
                </td>
            </tr>
        </tbody>
    </table> 
</div> 


@push('js_push')
<script>
    if(window.ddub==undefined){
        window.ddub=[];
    }
     window.ddub.push(new Vue({
        el:'#ddub-{{$id}}',
        data:{
            data:[],
            column:[],
            meta:<?=json_encode($options)?>,
            print_mode:{{$print_mode?'true':'false'}},
            
            op:{
                chart:{
                    type:'column'
                },
                title:{
                    text:'DDUB'
                },
                subtitle:{
                    text:'',
                    enabled:true
                },
                plotOptions:{
                    series:{
                        dataLabels:{
                            enabled:true
                        }
                    }
                },
                xAxis:{
                    type:'category'
                },
                yAxis:[],
                series:[]
            }
        },
        mounted(){
            this.loadData();
        },
        computed:{
            na:function(){
                var komit=0;
                var real=0;
                var rencana_sr=0;
                var realisasi_sr=0;


                this.data.forEach(el=>{
                    komit+=parseFloat(el.rencana);
                    real+=parseFloat(el.realisasi);
                    realisasi_sr+=parseFloat(el.realisasi_sr);
                    rencana_sr+=parseFloat(el.rencana_sr);

                });

                return {
                    'rencana_ag':komit,
                    'rencana_sr':rencana_sr,
                    'realisasi_ag':real,
                    'realisasi_sr':realisasi_sr,
                    'last':this.data[this.data.length-1]!=undefined?this.data[this.data.length-1]:{
                        tahun:0
                    }
                }
            }
        },
        methods:{
            parseNum:(val)=>{
                
                if(isNaN(val)){
                        return 0;
                }else if((val+'').toUpperCase()=='INFINITE'){
                    return 0;
                }else{
                    return window.NumberFormat(val);
                }
            },
            loadData:function(){
                this.data=[];
                this.column=[];
                this.op.series=[];
                this.op.yAxis=[];
                var self=this;
                req_ajax.post('{{url('api/m-d/'.$StahunRoute.'/ddub')}}',{
                    kodepemda:this.meta.kodepemda

                }).then(res=>{
                    self.data=res.data.data.data;
                    self.column.push(res.data.data.x);
                    self.op.subtitle.text=(res.data.sumber);
                    self.op.title.text=(res.data.title);


                    self.column=self.column.concat(res.data.data.y);

                    res.data.data.data.forEach(el => {
                        res.data.data.y.forEach((c,i)=>{

                            var yIndex= self.op.yAxis.findIndex(function(el){
                                return el.title.text==c.satuan;
                            });

                            if(yIndex===-1){
                                console.log(c.satuan,'append',self.op.yAxis.length);
                                self.op.yAxis.push({
                                    title:{
                                        text:c.satuan
                                    }
                                });
                                yIndex=self.op.yAxis.length-1;
                            }

                            if(self.op.series[i]==undefined){
                                self.op.series.push({
                                    data:[],
                                    name:c.name,
                                    yAxis:yIndex
                                });
                            }

                           

                            self.op.series[i].data.push({
                                y:parseFloat(el[c.key]),
                                name:el.tahun
                            });


                        });
                    });


                }).finally(function(){
                    // self.isLoading(false);
                });

            }
        }
    }));
</script>

@endpush