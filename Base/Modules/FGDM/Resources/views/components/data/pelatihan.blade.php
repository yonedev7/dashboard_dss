@php
$id=rand(0,1000);

if($re->kodepemda){
    $options['kodepemda']=$re->kodepemda;
}
@endphp

<div id="fgdm-com-data-pelatihan-{{$id}}">
    <div v-if="print_mode==false" class=" border-top-0 border-left-0 border border-right-0 pb-2 border-dark">
        <div class="btn-group">
            <button class="btn btn-primary" @click="loadData">Reload</button>
        </div>
    </div>
    <div v-if="meta.display.text">
        <p>Pemda @{{pemda.nama_pemda}} Telah Mengikuti @{{parseNum(parseFloat(data.length))}} Pelatihan Dengan Rincian Sebagai Berikut</p>
    </div>
    <charts :options="op"  class="bg-white shadow p-3"  v-if="meta.display.chart"></charts>   
    <table v-if="meta.display.table" class="table table-bordered table-striped bg-white shadow">
        <thead class="thead-dark">
            <tr>
                <th v-if="item.name!='Nama Pemda'"  v-for="item in column">
                        @{{item.name}}
                </th>
                
            </tr>
        </thead>
        <tbody>
            <tr v-for="item in data">
                <td v-for="c in column"  v-if="c.name!='Nama Pemda'"  :class="c.name=='Tahun'?'bg-dark':''">
                   
                        <p v-if="c.number">@{{parseNum(parseFloat(item[c.key]))}}</p>
                        <p v-else>@{{item[c.key]}}</p>
                        <template v-if="c.name=='Tahun' && pemda.kodepemda[0]!='0'">
                            <br>
                        <small > @{{parseNum(item['pemdas_kode'].split(',').length)}} Pemda</small>
                        </template>   
                </td>
            </tr>
        </tbody>
    </table> 
</div> 



@push('js_push')
<script>
if(window.fgdm_com_data_pelatihan==undefined){
    window.fgdm_com_data_pelatihan=[];
}

 window.fgdm_com_data_pelatihan.push(new Vue({
        el:'#fgdm-com-data-pelatihan-{{$id}}',
        data:{
            data:[],
            column:[],
            meta:<?=json_encode($options)?>,
            print_mode:{{$print_mode?'true':'false'}},
            pemda:{
                kodepmeda:null,
                nama_pemda:null
            },
            op:{
                chart:{
                    type:'column'
                },
                title:{
                    text:'...'
                },
                subtitle:{
                    text:'',
                    enabled:true
                },
                plotOptions:{
                    series:{
                        dataLabels:{
                            enabled:true
                        }
                    }
                },
                xAxis:{
                    type:'category'
                },
                yAxis:[],
                series:[]
            }
        },
        mounted(){
            this.loadData();
        },
        methods:{
            parseNum:(val)=>{
                
                if(isNaN(val)){
                        return 0;
                }else if((val+'').toUpperCase()=='INFINITE'){
                    return 0;
                }else{
                    return window.NumberFormat(val);
                }
            },
            loadData:function(){
                this.data=[];
                this.column=[];
                this.op.series=[];
                this.op.yAxis=[];
                var self=this;
                req_ajax.post('{{url('api/m-d/'.$StahunRoute.'/pelatihan')}}',{
                    kodepemda:this.meta.kodepemda
                }).then(res=>{
                    self.pemda.nama_pemda=res.data.pemda?res.data.pemda.nama_pemda:'';
                    self.pemda.kodepemda=res.data.pemda?res.data.pemda.kodepemda:null;
                    self.data=res.data.data.data;
                    self.column.push(res.data.data.x);
                    self.op.subtitle.text='Sumber : '+(res.data.sumber);
                    self.op.title.text=(res.data.title);
                    self.column=self.column.concat(res.data.data.y);
                    var column_series=res.data.data.y.filter(l=>{
                        return l.chart && l.number;
                    });
                    res.data.data.data.forEach(el => {
                        column_series.forEach((c,i)=>{

                                var yIndex= self.op.yAxis.findIndex(function(el){
                                return el.title.text==c.satuan;
                                });

                                if(yIndex===-1){
                                    console.log(c.satuan,'append',self.op.yAxis.length);
                                    self.op.yAxis.push({
                                        title:{
                                            text:c.satuan
                                        }
                                    });
                                    yIndex=self.op.yAxis.length-1;
                                }

                                if(self.op.series[i]==undefined){
                                    self.op.series.push({
                                        data:[],
                                        name:c.name,
                                        yAxis:yIndex
                                    });
                                }

                            

                                self.op.series[i].data.push({
                                    y:parseFloat(el[c.key]),
                                    name:el.tahun
                                });


                        });
                    });


                }).finally(function(){
                    // self.isLoading(false);
                });

            }
        }
    }));

</script>

@endpush