@php
$id=rand(0,1000);

if($re->kodepemda){
    $options['kodepemda']=$re->kodepemda;
}
@endphp

<div id="fgdm-com-data-pmpd-{{$id}}">
    <div v-if="print_mode==false" class=" border-top-0 border-left-0 border border-right-0 pb-2 border-dark">
        <div class="btn-group">
            <button class="btn btn-primary" @click="loadData">Reload</button>
        </div>
    </div>
    <div v-if="meta.display.text">
        <p>Penyertaan modal Pemda @{{pemda.nama_pemda}} pada tahun @{{na[0].tahun}} sebesar Rp. @{{parseNum(parseFloat(na[0].modal_pemda)+parseFloat(na[0].modal_pemda_infra))}}  dan pada tahun @{{na[1].tahun}} sebesar Rp. @{{parseNum(parseFloat(na[1].modal_pemda)+parseFloat(na[1].modal_pemda_infra))}}.
            Terjadi @{{((parseFloat(na[0].modal_pemda)+parseFloat(na[0].modal_pemda_infra)/(parseFloat(na[1].modal_pemda)+parseFloat(na[1].modal_pemda_infra))))>0?'peningkatan':'penurunan'}}  @{{((parseFloat(na[0].modal_pemda)+parseFloat(na[0].modal_pemda_infra)/(parseFloat(na[1].modal_pemda)+parseFloat(na[1].modal_pemda_infra)))).toFixed(2)}}%
        </p>   
    </div>
    <charts :options="op" v-if="meta.display.chart"  class="bg-white shadow p-3"></charts>   
    <table v-if="meta.display.table" class="table table-bordered table-striped bg-white shadow ">
        <thead class="thead-dark">
            <tr>
                <th v-for="item in column">@{{item.name}}</th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="item in data">
                <td v-for="c in column" :class="c.name=='Tahun'?'bg-dark':''">
                    @{{parseNum(parseFloat(item[c.key]))}}
                    <template v-if="c.name=='Tahun' && meta.kodepemda[0]!='0'">
                        <br>
                       <small> @{{parseNum(item['pemdas_kode'].split(',').length)}} Pemda</small>
                    </template>    
                </td>
            </tr>
        </tbody>
    </table> 
</div> 



@push('js_push')
<script>
if(window.fgdm_com_data_pmpd==undefined){
    window.fgdm_com_data_pmpd=[];
}

 window.fgdm_com_data_pmpd.push(new Vue({
        el:'#fgdm-com-data-pmpd-{{$id}}',
        data:{
            data:[],
            column:[],
            meta:<?=json_encode($options)?>,
            print_mode:{{$print_mode?'true':'false'}},
            pemda:{
                kodepmeda:null,
                nama_pemda:null
            },
            op:{
                chart:{
                    type:'column'
                },
                title:{
                    text:'...'
                },
                subtitle:{
                    text:'',
                    enabled:true
                },
                plotOptions:{
                    series:{
                        dataLabels:{
                            enabled:true
                        }
                    }
                },
                xAxis:{
                    type:'category'
                },
                yAxis:[],
                series:[]
            }
        },
        mounted(){
            this.loadData();
        },
        computed:{
           na:function(){
            var data_reverse=this.data.reverse();
            var x=data_reverse[0]!=undefined?data_reverse[0]:{
                apbd_ar_ag:0,
                tahun:0
            };
            var y=data_reverse[1]!=undefined?data_reverse[1]:x;


            return [x,y,(x.modal_pemda + x.moda_pemda_infra) - (y.modal_pemda + y.moda_pemda_infra)];
           }

        },
        methods:{
            parseNum:(val)=>{
                
                if(isNaN(val)){
                        return 0;
                }else if((val+'').toUpperCase()=='INFINITE'){
                    return 0;
                }else{
                    return window.NumberFormat(val);
                }
            },
            loadData:function(){
                this.data=[];
                this.column=[];
                this.op.series=[];
                this.op.yAxis=[];
                var self=this;
                req_ajax.post('{{url('api/m-d/'.$StahunRoute.'/pmpd')}}',{
                    kodepemda:this.meta.kodepemda
                }).then(res=>{
                    self.pemda.nama_pemda=res.data.pemda?res.data.pemda.nama_pemda:'';
                    self.pemda.kodepemda=res.data.pemda?res.data.pemda.kodepemda:null;
                    
                    self.data=res.data.data.data;
                    self.column.push(res.data.data.x);
                    self.op.subtitle.text='Sumber : '+(res.data.sumber);
                    self.op.title.text=(res.data.title);


                    self.column=self.column.concat(res.data.data.y);

                    res.data.data.data.forEach(el => {
                        res.data.data.y.forEach((c,i)=>{

                            var yIndex= self.op.yAxis.findIndex(function(el){
                                return el.title.text==c.satuan;
                            });

                            if(yIndex===-1){
                                console.log(c.satuan,'append',self.op.yAxis.length);
                                self.op.yAxis.push({
                                    title:{
                                        text:c.satuan
                                    }
                                });
                                yIndex=self.op.yAxis.length-1;
                            }

                            if(self.op.series[i]==undefined){
                                self.op.series.push({
                                    data:[],
                                    name:c.name,
                                    yAxis:yIndex
                                });
                            }

                           

                            self.op.series[i].data.push({
                                y:parseFloat(el[c.key]),
                                name:el.tahun
                            });


                        });
                    });


                }).finally(function(){
                    // self.isLoading(false);
                });

            }
        }
    }));

</script>

@endpush