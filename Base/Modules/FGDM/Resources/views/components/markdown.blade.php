

<div class="{{$options['body_class']}}">
    @php
    $options['pemda']=$meta_pemda;
    @endphp

    @if($options['title'])
    @php
        $options['title']=str_replace('[TAHUN]',$StahunRoute,$options['title']);
        $options['title']=str_replace('[PEMDA]',$options['pemda']['nama_pemda'],$options['title']);
    @endphp

    <h3 class="{{$options['title_class']}}">{!!$options['title']!!}</h3>
    @endif
    @if($options['content'])

    @php
    $options['content']=str_replace('[TAHUN]',$StahunRoute,$options['content']);
    $options['content']=str_replace('[PEMDA]',$options['pemda']['nama_pemda'],$options['content']);
    @endphp
    <p class="{{$options['content_class']}}">{!!$options['content']!!}</p>
    @endif
    </div>

{{-- 
[
    title:
    content:
    title_class:
    content_class:

]    
    
--}}