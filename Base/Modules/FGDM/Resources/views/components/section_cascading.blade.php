
<ol type="{{$options['type']}}" start="{{$options['start_index']}}">

    @foreach($data_collection as $k=>$item)
    <li>
        @include($item['com_id'],['options'=>isset($item['options'])?$item['options']:[],'data_collection'=>isset($item['data_collection'])?$item['data_collection']:[]])

    </li>
    @endforeach

</ol>
