@extends('fgdm::layouts.master')


@php
$print_mode=true;
@endphp


@section('content')
<style>
    ol li::marker{
        font-size:24px;
    }
    
    ol li{
        margin-bottom:35px;
    }
</style>

<div class="bg-white">
    <div class="text-center mt-2">
        <h2>Profil {{$meta_pemda['nama_pemda']}}</h2>
        <h3>Tahun {{$StahunRoute}}</h3>
        <hr>
    </div>
    <div class="container ">
        @include('fgdm::components.loop_data',['data_collection'=>$schema['data_collection']])
    </div>
</div>


@stop
