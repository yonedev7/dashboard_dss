@extends('adminlte::page')

@section('content')
@php
$print_mode=false;    

@endphp
<div id="filter_scope" class="container-fluid my-3">
    <div class="bg-dark" style="min-height:10px;"></div>

    <form action="{{url()->current()}}" method="get">
        <div class="bg-white p-3 shadow rounded">

            <div class="d-flex align-items-center">
                <div class="flex-column">
                <div class="form-group mr-2">
                    <label for="">Cakupan</label>
                    <select name="" class="form-control" id="" v-model="cakupan">
                        <option value="PER-PEMDA">PER-PEMDA</option>
                        <option value="LONGLIST">LONGLIST</option>
                        <option value="SORTLIST 2022">SORTLIST 2022</option>
                        <option value="SORTLIST 2020">SORTLIST 2020</option>
                        <option value="SORTLIST 2019">SORTLIST 2019</option>
                    </select>
                </div>
                </div>
                <div v-if="cakupan=='PER-PEMDA'">
                <div class="form-group mr-2 " >
                    <label for="">Pemda</label>
                    <select name="" class="form-control" id="" v-model="kodepemda">
                        <option :value="item.kodepemda" v-for="item in list_pemda">@{{item.nama_pemda}}</option>
                    </select>
                </div>
                </div>
                <div>
                <input type="hidden" name="kodepemda" :value="last_data">
                <button type="submit" class="btn btn-primary mt-3">Filter</button>
                </div>
            </div>
        </div>
    </form>
        
</div>
    <div class="container-fluid ">
    @include('fgdm::components.loop_data',['data_collection'=>$schema['data_collection']])

    </div>


<hr>
{{-- <div class="container-fluid">
    @include('fgdm::components.data.capaian_air_minum',['options'=>[
        'kodepemda'=>$re->kodepemda,
        'display'=>[
            'chart'=>true,
            'table'=>false,
        ]
    ]]);
    <div id="kerjasama" class="bg-white shadow p-3">
        <div class="btn-group">
            <button class="btn btn-primary" @click="loadData">Reload</button>
        </div>
        <charts :options="op"></charts>   
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th v-for="item in column">@{{item.name}}</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="item in data">
                    <td v-for="c in column">
                        @{{parseNum(parseFloat(item[c.key]))}}
                    </td>
                </tr>
            </tbody>
        </table> 
    </div> 
       

    <div id="pendamping" class="bg-white shadow p-3">
        <div class="btn-group">
            <button class="btn btn-primary" @click="loadData">Reload</button>
        </div>
        <charts :options="op"></charts>   
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th v-for="item in column">@{{item.name}}</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="item in data">
                    <td v-for="c in column">
                        @{{parseNum(parseFloat(item[c.key]))}}
                    </td>
                </tr>
            </tbody>
        </table> 
    </div> 

   
</div>     --}}

@endsection


@section('js')
<script>

   

    // var pendamping=new Vue({
    //     el:'#pendamping',
    //     data:{
    //         data:[],
    //         column:[],
    //         op:{
    //             chart:{
    //                 type:'column'
    //             },
    //             title:{
    //                 text:'...'
    //             },
    //             subtitle:{
    //                 text:'',
    //                 enabled:true
    //             },
    //             plotOptions:{
    //                 series:{
    //                     dataLabels:{
    //                         enabled:true
    //                     }
    //                 }
    //             },
    //             xAxis:{
    //                 type:'category'
    //             },
    //             yAxis:[],
    //             series:[]
    //         }
    //     },
    //     mounted(){
    //         this.loadData();
    //     },
    //     methods:{
    //         parseNum:(val)=>{
                
    //             if(isNaN(val)){
    //                     return 0;
    //             }else if((val+'').toUpperCase()=='INFINITE'){
    //                 return 0;
    //             }else{
    //                 return window.NumberFormat(val);
    //             }
    //         },
    //         loadData:function(){
    //             this.data=[];
    //             this.column=[];
    //             this.op.series=[];
    //             this.op.yAxis=[];
    //             var self=this;
    //             req_ajax.post('{{url('api/m-d/'.$StahunRoute.'/pendamping')}}',{
    //                 kodepemda:null
    //             }).then(res=>{
    //                 self.data=res.data.data.data;
    //                 self.column.push(res.data.data.x);
    //                 self.op.subtitle.text=(res.data.sumber);
    //                 self.op.title.text=(res.data.title);


    //                 self.column=self.column.concat(res.data.data.y);

    //                 res.data.data.data.forEach(el => {
    //                     res.data.data.y.forEach((c,i)=>{

    //                         var yIndex= self.op.yAxis.findIndex(function(el){
    //                             return el.title.text==c.satuan;
    //                         });

    //                         if(yIndex===-1){
    //                             console.log(c.satuan,'append',self.op.yAxis.length);
    //                             self.op.yAxis.push({
    //                                 title:{
    //                                     text:c.satuan
    //                                 }
    //                             });
    //                             yIndex=self.op.yAxis.length-1;
    //                         }

    //                         if(self.op.series[i]==undefined){
    //                             self.op.series.push({
    //                                 data:[],
    //                                 name:c.name,
    //                                 yAxis:yIndex
    //                             });
    //                         }

                           

    //                         self.op.series[i].data.push({
    //                             y:parseFloat(el[c.key]),
    //                             name:el.tahun
    //                         });


    //                     });
    //                 });


    //             }).finally(function(){
    //                 // self.isLoading(false);
    //             });

    //         }
    //     }
    // })

    // var pendamping=new Vue({
    //     el:'#kerjasama',
    //     data:{
    //         data:[],
    //         column:[],
    //         op:{
    //             chart:{
    //                 type:'column'
    //             },
    //             title:{
    //                 text:'...'
    //             },
    //             subtitle:{
    //                 text:'',
    //                 enabled:true
    //             },
    //             plotOptions:{
    //                 series:{
    //                     dataLabels:{
    //                         enabled:true
    //                     }
    //                 }
    //             },
    //             xAxis:{
    //                 type:'category'
    //             },
    //             yAxis:[],
    //             series:[]
    //         }
    //     },
    //     mounted(){
    //         this.loadData();
    //     },
    //     methods:{
    //         parseNum:(val)=>{
                
    //             if(isNaN(val)){
    //                     return 0;
    //             }else if((val+'').toUpperCase()=='INFINITE'){
    //                 return 0;
    //             }else{
    //                 return window.NumberFormat(val);
    //             }
    //         },
    //         loadData:function(){
    //             this.data=[];
    //             this.column=[];
    //             this.op.series=[];
    //             this.op.yAxis=[];
    //             var self=this;
    //             req_ajax.post('{{url('api/m-d/'.$StahunRoute.'/kerjasama')}}',{
    //                 kodepemda:null
    //             }).then(res=>{
    //                 self.data=res.data.data.data;
    //                 self.column.push(res.data.data.x);
    //                 self.op.subtitle.text=(res.data.sumber);
    //                 self.op.title.text=(res.data.title);


    //                 self.column=self.column.concat(res.data.data.y);

    //                 res.data.data.data.forEach(el => {
    //                     res.data.data.y.forEach((c,i)=>{

    //                         var yIndex= self.op.yAxis.findIndex(function(el){
    //                             return el.title.text==c.satuan;
    //                         });

    //                         if(yIndex===-1){
    //                             console.log(c.satuan,'append',self.op.yAxis.length);
    //                             self.op.yAxis.push({
    //                                 title:{
    //                                     text:c.satuan
    //                                 }
    //                             });
    //                             yIndex=self.op.yAxis.length-1;
    //                         }

    //                         if(self.op.series[i]==undefined){
    //                             self.op.series.push({
    //                                 data:[],
    //                                 name:c.name,
    //                                 yAxis:yIndex
    //                             });
    //                         }

                           

    //                         self.op.series[i].data.push({
    //                             y:parseFloat(el[c.key]),
    //                             name:el.tahun
    //                         });


    //                     });
    //                 });


    //             }).finally(function(){
    //                 // self.isLoading(false);
    //             });

    //         }
    //     }
    // })
 
    var filter_scope=new Vue({
        el:'#filter_scope',
        data:{
            cakupan:null,
            kodepemda:null,
            last_data:'{{$re->kodepemda}}',
            list_pemda:<?=json_encode($pemda_list)?>   
        },
        created:function(){
            if(this.last_data.includes('LONGLIST')){
                this.cakupan='LONGLIST';
            }else if(this.last_data.includes('SORTLIST')){
                this.cakupan=this.last_data;
            }else if(this.last_data!=''){
                this.kodepemda=this.last_data;
            }else{
                this.cakupan='LONGLIST';
            }

            this.update();
        },
        methods:{
            update:function(){
                if(this.kodepemda){
                    this.cakupan='PER-PEMDA';
                }else if((this.list_pemda.filter(el=>{return el.kodepemda==this.last_data; })).length>0){
                    this.kodepemda=this.last_data;
                    this.update();
                }else{
                    this.last_data=this.cakupan;
                    
                }
            }
        },
        watch:{
            cakupan:function(val){
                if(val=='PER-PEMDA'){
                    this.kodepemda=this.list_pemda[0].kodepemda;
                    this.last_data=this.kodepemda;
                }else{
                    this.kodepemda=null;
                    this.last_data=this.val;

                }
                this.update();
            },
            kodepemda:function(val){
                if(val){
                    this.last_data=val;
                    this.cakupan='PER-PEMDA';
                }
            }
        }
    })
</script>


@stop
