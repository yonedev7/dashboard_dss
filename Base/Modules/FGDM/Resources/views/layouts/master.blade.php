<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Module FGDM</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="shortcut icon" href="{{ asset('/assets/img/brand/logo-1.png') }}" type="image/x-icon"/>
            
        <link rel="stylesheet" href="{{ asset(config('adminlte.laravel_mix_css_path', 'css/app.css').(env('JS_V')?'?v='.env('JS_V'):'')) }}">
        <script>
            const dss{{$SpageId}}={
                'base':'{{url('api')}}',
                'token':'{{Auth::check()?Auth::User()->_web_api_token():''}}',
                'user':<?=json_encode(Auth::check()?(Auth::User()):'{}')?>   
            }
        </script>

       {{-- Laravel Mix - CSS File --}}
       {{-- <link rel="stylesheet" href="{{ mix('css/fgdm.css') }}"> --}}
       @yield('css')
       @stack('css_push')



    </head>
    <body>
        @yield('content')

        {{-- Laravel Mix - JS File --}}

           {{-- Base Scripts --}}
        <script src="{{ asset(config('adminlte.laravel_mix_js_path', 'js/app.js').(env('JS_V')?'?v='.env('JS_V'):'')) }}"></script>


        <script src="{{asset('js/app_builder.js').(env('JS_V')?('?v='.env('JS_V')):'')}}"></script>



        @include('sweetalert::alert')
    <script>
        var MySession=<?=json_encode(Auth::User()??'{}')?>;
        ServerRoute=function(path){
            var host='{{url('')}}';
            host=_path.join(host,path).replaceAll('/{2+}','/');
            return host;

        };

        $(function(){
            isSafari=false;
    

        <?php
        $keyapp=md5(preg_replace('/[^\p{L}\p{N}\s]|[\'base64\']/', '', env('APP_KEY')));
        ?>
            var channel
            if(!isSafari){
            channel= new BroadcastChannel('{{$keyapp}}');

            }

            @if(Auth::check())
                if(!localStorage.getItem('app_auth{{$keyapp}}')){
                    localStorage.setItem('app_auth{{$keyapp}}',{{Auth::User()->id}});
                }else if(parseInt(localStorage.getItem('app_auth{{$keyapp}}'))!={{Auth::User()->id}}){
                    localStorage.setItem('app_auth{{$keyapp}}',{{Auth::User()->id}});
                    if(!isSafari){
                channel.postMessage({
                    method:'reload_env',
                    data:{
                        tahun:{{$Stahun}}
                    }
                });
            }
                }

            @else

            if(!localStorage.getItem('app_auth{{$keyapp}}')){
                localStorage.setItem('app_auth{{$keyapp}}','null');
            }else if((localStorage.getItem('app_auth{{$keyapp}}'))!='null'){
                localStorage.setItem('app_auth{{$keyapp}}','null');
                console.log('env_auth');

                if(!isSafari){
                channel.postMessage({
                    method:'reload_env',
                    data:{
                        tahun:{{$Stahun}}
                    }
                });
            }
            }


            @endif


            if(!isSafari){
                channel.onmessage = function(e) {
                    if(e.data.method=='reload_env'){
                        change_env(e.data.data.tahun);
                    }   
                };
            }else{
                var auth_k=localStorage.getItem('app_auth{{$keyapp}}');
                var auth_t=localStorage.getItem('app_th{{$keyapp}}');


                window.addEventListener('storage', () => {
                    console.log('asss');
                if(window.localStorage.getItem('app_auth{{$keyapp}}')!=auth_k){
                    window.location.reload();
                }

                if(window.localStorage.getItem('app_th{{$keyapp}}')!=auth_t){
                    window.location.reload();
                }
                });

            
            }
        



        });
    
    </script>

    </body>
    <style>
    .show-all-table{
    height:unset!important;
    max-height:unset!important;

    }    
    </style>
    <script>

    $('#btn-screenshoot').on('click',function(){
        console.log('a');
        $('.tableFixHead').addClass('show-all-table');
        setTimeout((ref) => {
            const screenshotTarget = document.body;

            html2canvas(screenshotTarget).then(async (canvas) => {
                await canvas.toBlob(function(blob){
                var url= window.URL.createObjectURL(blob);
                console.log(url);
                window.open(url);
                }, 'image/jpeg', 0.95);
                $('.tableFixHead').removeClass('show-all-table');

            });
            
        }, 300);
        
        
    });
    </script>
    <script>
        initial_ajax(dss{{$SpageId}});
    </script>
    

       @yield('js')
       @stack('js_push')
    </body>
</html>
