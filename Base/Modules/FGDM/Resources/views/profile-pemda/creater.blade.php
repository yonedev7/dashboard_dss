@extends('adminlte::page')


@section('content')

  <div class="row">
    <div class="col-8">
        <div class="bg-white p-3" id="profile">
            <div class="mt-3" v-for="item,x in bab">
                <h2>BAB @{{x+1}}. @{{item.title}}</h2>
                <ckeditor :ref="'ck_bab_'+x"  :editor="class_editor" :config="config"  v-model="bab[x].content"></ckeditor>
            </div>
        </div>
    </div>
  </div>

@stop


@section('js')
<script>

var App=new Vue({
    el:'#profile',
    data:{
        bab:[
            {
                title:'PERENCANAAN DAN PENGANGARAN',
                content:''
            },
            {
                title:'KERJASAMA',
                content:''
            },
            {
                title:'CAPACITY BUILDING DAN TEHNICAL ASSITENT',
                content:''
            },
            {
                title:'EVALUASI DUKUNGAN PEMDA DALAM KERANGKA NUWSP',
                content:''
            }
        ],
        class_editor:  window.CKClassicEditor,
        config:{

            toolbars: [
                        'Format',
                        ['Bold', 'Italic', 'Strike', 'Underline'],
                        ['BulletedList', 'NumberedList', 'Blockquote'],
                        ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                        ['Link', 'Unlink'],
                        ['FontSize', 'TextColor'],
                        // ['Image'],
                        ['Undo', 'Redo'],
                        ['Source', 'Maximize']
                        ]
            },
            mention: {
            feeds: [
                {
                    marker: '@',
                    feed: getFeedItems
                }
            ]
        }
    }
});


const items = [
    { id: '@swarley', userId: '1', name: 'Barney Stinson', link: 'https://www.imdb.com/title/tt0460649/characters/nm0000439' },
    { id: '@lilypad', userId: '2', name: 'Lily Aldrin', link: 'https://www.imdb.com/title/tt0460649/characters/nm0004989' },
    { id: '@marry', userId: '3', name: 'Marry Ann Lewis', link: 'https://www.imdb.com/title/tt0460649/characters/nm1130627' },
    { id: '@marshmallow', userId: '4', name: 'Marshall Eriksen', link: 'https://www.imdb.com/title/tt0460649/characters/nm0781981' },
    { id: '@rsparkles', userId: '5', name: 'Robin Scherbatsky', link: 'https://www.imdb.com/title/tt0460649/characters/nm1130627' },
    { id: '@tdog', userId: '6', name: 'Ted Mosby', link: 'https://www.imdb.com/title/tt0460649/characters/nm1102140' }
];

function getFeedItems( queryText ) {
    console.log('ss',queryText);
    // As an example of an asynchronous action, return a promise
    // that resolves after a 100ms timeout.
    // This can be a server request or any sort of delayed action.
    return new Promise( resolve => {
        setTimeout( () => {
            const itemsToDisplay = items
                // Filter out the full list of all items to only those matching the query text.
                .filter( isItemMatching )
                // Return 10 items max - needed for generic queries when the list may contain hundreds of elements.
                .slice( 0, 10 );

            resolve( itemsToDisplay );
        }, 100 );
    } );

    // Filtering function - it uses the `name` and `username` properties of an item to find a match.
    function isItemMatching( item ) {
        // Make the search case-insensitive.
        const searchString = queryText.toLowerCase();

        // Include an item in the search results if the name or username includes the current user input.
        return (
            item.name.toLowerCase().includes( searchString ) ||
            item.id.toLowerCase().includes( searchString )
        );
    }
}

</script>



@stop