<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/fgdm', function (Request $request) {
    return $request->user();
});

Route::prefix('m-d/{tahun}')->group(function(){

    Route::post('schema-creator/{kodepemda}/draf/store','FGDMController@draf')->name('fgdm.draf.store');
    Route::post('schema-creator/{kodepemda}/store','FGDMController@publish')->name('fgdm.store');
    Route::post('/rencana-dan-anggaran', "RencanaDanAnggaranController@index");
    Route::post('/ddub', "DDUBController@index");
    Route::post('/pmpd', "PMPDController@index");
    Route::post('/pendamping', "DanaPendampingController@index");
    Route::post('/pelatihan', "PelatihanController@index");
    Route::post('/bumdam-fcr', "BUMDAMFCRController@index");


    Route::post('/capaian-air-minum', "CapaianAirMinumController@index");
    Route::post('/kerjasama', "KerjasamaController@index");



    
});