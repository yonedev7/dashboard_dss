<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('fgdm/{tahun}')->group(function() {
    Route::get('/', 'FGDMController@index');
    Route::get('/pdf/{kodepemda}', 'FGDMController@pdf');

    Route::get('/kerja', 'KerjasamaController@index');

    Route::get('/builder/{kodepemda}', 'FGDMController@builder');

    Route::get('/profile-pemda/creater/{kodepemda}', 'FGDMController@pPemdaCreater');

    Route::get('/profile-pemda/creater/{kodepemda}/draf/{finger}', 'FGDMController@showDraf')->name('fgdm.schema.draf');

});
