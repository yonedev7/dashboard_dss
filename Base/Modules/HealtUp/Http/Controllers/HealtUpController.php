<?php

namespace Modules\HealtUp\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class HealtUpController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {

        $schema=[[140,"select * from information_schema.columns where table_schema = 'dataset' AND table_name = 'subsidi_air_minum'"],
        [141,"select * from information_schema.columns where table_schema = 'dataset' AND table_name = 'rispam'"],
        [122,"select * from information_schema.columns where table_schema = 'dataset' AND table_name = 'renja'"],
        [119,"select * from information_schema.columns where table_schema = 'dataset' AND table_name = 'pemda_pmpd'"],
        [124,"select * from information_schema.columns where table_schema = 'dataset' AND table_name = 'rkpa'"],
        [125,"select * from information_schema.columns where table_schema = 'dataset' AND table_name = 'rpjmd_pendukung'"],
        [121,"select * from information_schema.columns where table_schema = 'dataset' AND table_name = 'dok_pendukung'"],
        [146,"select * from information_schema.columns where table_schema = 'dataset' AND table_name = 'luas_wilayah'"],
        [136,"select * from information_schema.columns where table_schema = 'dataset' AND table_name = 'pemda_kerjasama_non_public'"],
        [145,"select * from information_schema.columns where table_schema = 'dataset' AND table_name = 'jumlah_penduduk_miskin'"],
        [144,"select * from information_schema.columns where table_schema = 'dataset' AND table_name = 'jumlah_penduduk'"],
        [153,"select * from information_schema.columns where table_schema = 'dataset' AND table_name = 'ikfd'"],
        [164,"select * from information_schema.columns where table_schema = 'dataset' AND table_name = 'bumdam_bpkp_laporan'"],
        [120,"select * from information_schema.columns where table_schema = 'dataset' AND table_name = 'pemda_ddub'"],
        [130,"select * from information_schema.columns where table_schema = 'dataset' AND table_name = 'spm_capaian_air_minum'"],
        [156,"select * from information_schema.columns where table_schema = 'dataset' AND table_name = 'pemda_spm'"],
        [147,"select * from information_schema.columns where table_schema = 'dataset' AND table_name = 'capain_air_minum_jp_bjp'"],
        [161,"select * from information_schema.columns where table_schema = 'dataset' AND table_name = 'bumdam_bpkp'"],
        [135,"select * from information_schema.columns where table_schema = 'dataset' AND table_name = 'bumdam_kondisi_bpkp'"],
        [160,"select * from information_schema.columns where table_schema = 'dataset' AND table_name = 'bumdam_fcr'"],
        [104,"select * from information_schema.columns where table_schema = 'dataset' AND table_name = 'pemda_apbd_2022'"],
        [123,"select * from information_schema.columns where table_schema = 'dataset' AND table_name = 'penetapan_tarif'"],
        [101,"select * from information_schema.columns where table_schema = 'dataset' AND table_name = 'rkpd_sipd_air_minum'"],
        [138,"select * from information_schema.columns where table_schema = 'dataset' AND table_name = 'pemda_pelatihan'"],
        [132,"select * from information_schema.columns where table_schema = 'dataset' AND table_name = 'bumdam_periodik'"]];
        return view('healtup::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function storeData($tahun,Request $request)
    {

        return DB::table('master.data')->insertGetId([
            'name'=>$request->name,
            'satuan'=>$request->satuan,
            'table'=>'xx',
            'id_validator'=>1,
            'type'=>'input-value',
            'tipe_nilai'=>$request->tipe_nilai
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('healtup::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('healtup::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
