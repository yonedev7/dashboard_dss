<?php
return [
    [   'type'=>'html',
        'title'=>'H1',
        'template'=>`
            <h1 v-html="inner"></h1>
        `,
        'component'=>'b-card',
        'child'=>[],
        'data'=>[
            'inner'=>[
            ],
            'params'=>[
                'title'=>[
                    'val'=>'title',
                    'type'=>'input-string'
                ],
                'img-src'=>[
                    'val'=>'',
                    'type'=>'input-string'
                ],
                'class'=>[
                    'val'=>'',
                    'type'=>'input-string'
                ]
            ],
        ],
        
    ],
    [   'type'=>'component',
        'title'=>'Card',
        'template'=>`
        <b-card :title="title" :img-src="img_src"></b-card>
        `,
        'component'=>'b-card',
        'child'=>[],
        'data'=>[
            'inner'=>[
            ],
            'params'=>[
                'title'=>[
                    'val'=>'title',
                    'type'=>'input-string'
                ],
                'img-src'=>[
                    'val'=>'',
                    'type'=>'input-string'
                ],
                'class'=>[
                    'val'=>'',
                    'type'=>'input-string'
                ]
            ],
        ],
        
    ]
];