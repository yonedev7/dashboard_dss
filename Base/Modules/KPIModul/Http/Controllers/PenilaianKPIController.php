<?php

namespace Modules\KPIModul\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use App\Http\Controllers\FilterCtrl;
use Auth;
use Alert;
use Carbon\Carbon;
class PenilaianKPIController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index($tahun,Request $request)
    {

        $kpi_list=DB::table('master.kpi')->orderBy('name','asc')->get();
        if($request->kpi){
            $kpi=DB::table('master.kpi')->where('id',$request->kpi)->first();
        }else{
            $kpi=DB::table('master.kpi')->orderBy('name','asc')->first();
        }

        if(!$kpi){
            $kpi=DB::table('master.kpi')->orderBy('name','asc')->first();
        }
        $request->kpi=$kpi->id;

        $dataset=DB::table('master.kpi_data_monev as monev')
        ->join('master.dataset as dts','dts.id','=','monev.id_data')
        ->orderBy('dts.index','asc')
        ->where('id_kpi',$kpi->id)->select('dts.id','dts.kategori','dts.tujuan','dts.name','dts.klasifikasi_1','dts.klasifikasi_2')->get()->toArray();



        $pemdas=FilterCtrl::filter_pemda($request->filter_regional,true,$request->filter_tahun_proyek,$request->filter_tipe_bantuan);
        $data_=[];
        $series=[];
        $x=[$tahun-4,$tahun-3,$tahun-2,$tahun-1,$tahun,$tahun+1,$tahun+2,$tahun+3,$tahun+4];

        foreach($x as $k=>$th){
            
            $data=DB::table('master.master_pemda as mp')
            ->leftjoin('analisa.penilaian_kpi as pkpi',[
                    ['pkpi.tahun','=',DB::raw($th)],
                    ['pkpi.id_kpi','=',DB::RAW($kpi->id)],
                    ['mp.kodepemda','=','pkpi.kodepemda']
            ])
            ->whereIn('mp.kodepemda',$pemdas)
            ->selectRaw("pkpi.keterangan,pkpi.id as id_penilaian,mp.nama_pemda,mp.kodepemda,pkpi.terpenuhi,".$th." as tahun")
            ->orderBy('pkpi.tahun','asc')
            ->orderBy('mp.kodepemda','asc')
            ->get();
            if($th==$tahun){
                $data_=$data;
            }

            foreach($data as $k=>$d){
                $cont=($d->id_penilaian!=null?($d->terpenuhi?'Terpenuhi':"Tidak Terpenuhi"):'Belum Dinilai');
            if(!isset( $series[$cont])){
                $series[$cont]=[
                    'name'=>$cont,
                    'color'=>($d->id_penilaian!=null?($d->terpenuhi?'blue':"red"):'orange'),
                    'data'=>[]
                ];
            }

            if(!isset( $series[$cont]['data'][$d->tahun])){
                    $series[$cont]['data'][$d->tahun]=[
                        'name'=>$d->tahun,
                        'y'=>0,
                        'kodepemda'=>[]
                    ];

                }

                $series[$cont]['data'][$d->tahun]['y']+=1;
                $series[$cont]['data'][$d->tahun]['kodepemda'][]=$d->kodepemda;
            }
        }

        foreach($series as $k=>$s){
            $series[$k]['data']=array_values($series[$k]['data']);
        }


        
    
        return view('kpimodul::index')->with(
            [
                'dataset'=>$dataset,
                'req'=>$request,
                'series'=>array_values($series),
                'data'=>$data_,
                'kpi'=>$kpi,
                'kpi_list'=>$kpi_list
            ]);
    }

    public function simpanPenilaian($tahun,$kodepemda,$kpi,Request $request)
    {
            $My=Auth::User();
            DB::table('analisa.penilaian_kpi')->updateOrInsert([
                'kodepemda'=>$kodepemda,
                'tahun'=>$tahun,
                'id_kpi'=>$kpi,
            ],[
                'terpenuhi'=>$request->penilaian??false,
                'keterangan'=>$request->keterangan,
                'kodepemda'=>$kodepemda,
                'tahun'=>$tahun,
                'id_user'=>$My->id,
                'id_kpi'=>$kpi,
                'updated_at'=>Carbon::now(),
            ]);

            Alert::success('','Data Berhasil Disimpan');
            return back();
    }

    
}
