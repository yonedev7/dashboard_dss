@extends('adminlte::page')


@section('content')
<div class="parallax pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
    <div class="container">
        <section >
            <h3><b>{{$kpi->name}} Tahun {{$StahunRoute}}</b></h3>
            <h4><b>{!!$kpi->keterangan!!}</b></h4>
        </section>
    </div>
</div>
<div id="app">
     <div class="bg-white mb-2 rounded shadow p-2">
        <form method="get" id="form-submit" action="{{url()->current()}}">
            <div class="row">
                <div class="col-3">
                    <div class="form-group">
                        
                        <label for="">KPI</label>
                        <select name="kpi" @change="submit_filter" class="form-control" id="" v-model="filter.kpi">
                           @foreach ($kpi_list as $item)
                           <option value="{{$item->id}}">{{$item->name}}</option>
                           @endforeach
    
                        </select>
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        
                        <label for="">Regional</label>
                        <select name="filter_regional" @change="submit_filter" class="form-control" id="" v-model="filter.filter_regional">
                            <option value=""></option>
                            <option value="I">I</option>
                            <option value="II">II</option>
                            <option value="III">III</option>
                            <option value="IV">IV</option>
    
                        </select>
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label for="">Tahun Proyek</label>
                        <select name="filter_tahun_proyek" @change="submit_filter" class="form-control" id="" v-model="filter.filter_tahun_proyek">
                            <option value=""></option>
                            <option value="{{$StahunRoute-3}}">{{$StahunRoute-3}}</option>
    
                            <option value="{{$StahunRoute-2}}">{{$StahunRoute-2}}</option>
                            <option value="{{$StahunRoute-1}}">{{$StahunRoute-1}}</option>
                            <option value="{{$StahunRoute}}">{{$StahunRoute}}</option>
                            <option value="{{$StahunRoute+1}}">{{$StahunRoute+1}}</option>
                            <option value="{{$StahunRoute+2}}">{{$StahunRoute+2}}</option>
                            <option value="{{$StahunRoute+3}}">{{$StahunRoute+3}}</option>
    
    
    
                        </select>
                    </div>
                    
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label for="">Tipe Bantuan</label>
                        <select name="filter_tipe_bantuan" @change="submit_filter" class="form-control" id="" v-model="filter.filter_tipe_bantuan">
                            <option value=""></option>
                            <option value="STIMULAN">STIMULAN</option>
                            <option value="PENDAMPINGAN">PENDAMPINGAN</option>
                            <option value="BASIS PROYEK">BASIS PROYEK</option>
    
                        </select>
                    </div>
    
                </div>
            </div>
        </form>
     </div>
    <div class="bg-white rounded shadow">
        <div class="col-12">
        <charts :options="charts"></charts>  

        </div>
    </div>
   <div class="p-3">
    <div class="row">
        <div class="col-12 mt-2 bg-white p-3">
            <h5 class="text-center">Penilaian Tahun {{$StahunRoute}}</h5>
    
            </div>
        <div class="col-12 bg-white shadow mt-3">
            <div class="form-group px-2 pt-2">
                <label for="">Cari Pemda</label>
                <input type="text" class="form-control" v-model="cari">
                <p class="mt-2 text-center"><b>Jumlah Pemda @{{com_data.length+' / '+data.length+' Pemda' }}</b></p>
            </div>
        </div>
        
        <div class="col-4" v-for="item in com_data">
            <div v-bind:class="'p-3 mt-2 rounded '+ (item.id_penilaian!=null?(item.terpenuhi?'bg-primary':'bg-danger'):'bg-white')">
                <h5>@{{item.nama_pemda}}</h5>
                <p>@{{(item.id_penilaian!=null?(item.terpenuhi?'Terpenuhi':'Tidak Terpenuhi'):'Belum Dinilai')}}</p>
                <button class="btn btn-success" @click="show_penilaian(item)">Detail</button>
            </div>
        </div>
    </div>
   </div>
    <div id="penilaian-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="my-modal-title">@{{form.pemda.nama_pemda}} <small>Penilaian {{$kpi->name}} Tahun {{$StahunRoute}}</small></h5>
                    <button class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Status Data</label>
                        <select name="" class="form-control" v-model="filter.status_data" id="">
                            <option value="2">Tervalidasi</option>
                            <option value="1">Terverifikasi</option>

                        </select>
                    </div>
                    <h5 class="bg-dark p-3">Kondisi Data (@{{filter.status_data==2?'Tervalidasi':'Terverifikasi'}})</h5>
                    <div class="p-3" style="height: 45vh; overflow:scroll; ">
                            <div class="row" style="background-color:#ddd;" >
                                <div class="col-6 my-2" v-if="item.series.length" v-for="item in charts_data">
                                    <div class="bg-white shadow rounded">
                                        <charts :options="item"></charts>
                                    </div>
                                    
                                </div>
                            </div>
                    </div>
                    <hr>
                    <h5 class="bg-dark p-3">Penilaian</h5>
                    <div class="" >
                    <div class="row">
                       
                            <div class="col-12">
                                <form  id="submit-form" :action="form.action" method="post">
                                    @csrf
                                <div class="form-group">
                                    <label for="">Penilaian Pemenuhan</label>
                                    <select name="penilaian" class="form-control " v-model="form.terpenuhi" id="">
                                        <option value="1">Terpenuhi</option>
                                        <option value="0">Tidak Terpenuhi</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Catatan</label>
                                    <textarea name="keterangan" v-model="form.keterangan" class="form-control" id="" cols="30" rows="10"></textarea>
                                </div>
                        </form>

                            </div>

                        <div>
                    </div>
                    </div>

                   
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default bg-secondary" aria-label="Close">Tutup</button>
                    <button class="btn btn-primary" @click="submitPenilaian()" type="submit">Kirim</button>
                </div>
            </div>
        </div>
    </div>
</div>

@stop


@section('js')
<script>
    var app=new Vue({
        el:'#app',
        methods:{
            submit_filter:function(){
                $('#form-submit').submit();
            },
            getData:function(){
                this.charts_data.forEach((el,k)=>{
                    this.charts_data[k].series=[];
                    this.charts_data[k].subtitle.text='';
                    var self=this;
                    this.$isLoading(true);
                    req_ajax.post(('{{route('api-web-mod.dataset.data.series.pemda.penilaian',['tahun'=>$StahunRoute,'id_dataset'=>'@?','kodepemda'=>'@?'])}}').yoneReplaceParam('@?',[el.meta.id_dataset,this.form.pemda.kodepemda]),{status:this.filter.status_data}).then(res=>{
                        if(res.data.data[0]!=undefined){
                            self.charts_data[k].subtitle.text=res.data.data[0].nama_pemda+' ('+(self.filter.status_data==2?'Tervalidasi':'Terverifikasi')+')';
                            self.charts_data[k]['series']=res.data.data[0].series;
                        }
                    }).finally(function(){
                        self.$isLoading(false);
                    });

                });
            },
            show_penilaian:function(item){
                this.form.pemda.nama_pemda=item.nama_pemda;
                this.form.pemda.kodepemda=item.kodepemda;
                this.form.terpenuhi=item.terpenuhi?1:0;
                this.form.keterangan=item.keterangan;
                this.getData();
                this.form.action=('{{route('mod.kpi.penilaian',['tahun'=>$StahunRoute,'kodepemda'=>'@?','kpi'=>'@?'])}}').yoneReplaceParam('@?',[this.form.pemda.kodepemda,{{$kpi->id}}]);
                
                $('#penilaian-modal').modal({
                    backdrop:'static'
                });
            },
            submitPenilaian:function(){
                $('#submit-form').submit();
            }
        },
        watch:{
            "filter.status_data":function(){
                this.getData();
            }
        },
        created:function(){
            var self=this;
            var c=[(this.filter.filter_regional?' Regional '+this.filter.filter_regional:''),
            (this.filter.filter_tahun_proyek?' Tahun Proyek '+this.filter.filter_tahun_proyek:''),
            (this.filter.filter_tipe_bantuan?' Tipe Bantuan '+this.filter.filter_tipe_bantuan:'')];
            
            this.charts.subtitle.text=c.filter(el=>{
                return el;
            }).join(', ');

            this.dataset.forEach(el=>{
                self.charts_data.push({
                    meta:{
                        id_dataset:el.id
                    },
                    chart:{
                        type:'line'
                    },
                    plotOptions:{
                        line:{
                            dataLabels:{
                                enabled:true,
                            }
                        }
                    },
                    title:{
                        text:el.name
                    },
                    subtitle:{
                        text:''
                    },
                    xAxis:{
                        type:'category'
                    },
                    series:[]
                })
            });
        },
        data:{
            filter:{
                filter_regional:'{{$req->filter_regional??''}}',
                kpi:'{{$req->kpi??''}}',
                status_data:2,
                filter_tahun_proyek:'{{$req->filter_tahun_proyek??''}}',
                filter_tipe_bantuan:'{{$req->filter_tipe_bantuan??''}}',
            },
            data:<?=json_encode($data)?>,
            cari:'',
            charts_data:[],
            dataset:<?=json_encode($dataset)?>,
            form:{
                action:'',
                pemda:{
                    kodepemda:'',
                    nama_pemda:'',
                    tahun:{{$StahunRoute}},
                },
                terpenuhi:0,
                keterangan:'',
                series:[],

            },
            charts:{
                chart:{
                    type:'line'
                },
                title:{
                    text:'Hasil Penilaian  {{$kpi->name}}'
                },
                subtitle:{
                    text:''
                },
                plotOptions:{
                   line:{
                    dataLabels:{
                        enabled:true,
                        format:"{point.y} Pemda"
                    }
                   }
                },
                xAxis:{
                    type:'category'
                },
                series:<?=json_encode($series)?>
            }

        },
        computed:{
            com_data:function(){
                var self=this;
                return this.data.filter(el=>{
                    return (el.nama_pemda||'').toLowerCase().includes((self.cari||'').toLowerCase())
                });
            }
        }
    })
</script>
@stop
