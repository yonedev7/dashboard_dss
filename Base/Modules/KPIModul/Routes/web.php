<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::prefix('dash/{tahun}/sistem-informasi/kpi-modul')->name('mod.kpi')->middleware('auth:web')->group(function() {
    Route::get('/', 'PenilaianKPIController@index')->name('.index');
    Route::post('/submit-penilaian/{kodepemda}/{kpi}', 'PenilaianKPIController@simpanPenilaian')->name('.penilaian');



});

