<?php

namespace Modules\LeveranceModule\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Storage;
class LeveranceModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    static function getContent(){
        $lev=[
            'header'=>[
                [
                    'field'=>'Komponen Proyek',
                    'label'=>'Komponen Proyek',
                    'type'=>'string',
                ],
                [
                    'field'=>'Total Biaya (USD Juta)',
                    'label'=>'Total Biaya (USD Juta)',
                    'type'=>'numeric',
                ],
                [
                    'field'=>'IBRD (USD Juta)',
                    'label'=>'IBRD (USD Juta)',
                    'type'=>'numeric',
                ],
                [
                    'field'=>'APBN CK (USD Juta)',
                    'label'=>'APBN CK (USD Juta)',
                    'type'=>'numeric',
                ],
                [
                    'field'=>'APBD/ PDAM (USD Juta)',
                    'label'=>'APBD/ PDAM (USD Juta)',
                    'type'=>'numeric',
                ],
                [
                    'field'=>'Sumber Dana Lain (USD Juta)',
                    'label'=>'Sumber Dana Lain (USD Juta)',
                    'type'=>'numeric',
                ],
            ],
            'data'=>[
                [
                    "Komponen Proyek"=> "1. Dukungan Investasi Infrastruktur Air Minum Perkotaan",
                    "Total Biaya (USD Juta)"=> 560,
                    "IBRD (USD Juta)"=> 70,
                    "APBN CK (USD Juta)"=> 75,
                    "APBD/ PDAM (USD Juta)"=> 100,
                    "Sumber Dana Lain (USD Juta)"=> 315
                ],
                [
                    "Komponen Proyek"=> "2. Bantuan Teknis dan Peningkatan KapasitasPemda dan PDAM",
                    "Total Biaya (USD Juta)"=> 15.5,
                    "IBRD (USD Juta)"=> 10,
                    "APBN CK (USD Juta)"=> 1,
                    "APBD/ PDAM (USD Juta)"=> 2.5,
                    "Sumber Dana Lain (USD Juta)"=> 2
                ],
                [
                    "Komponen Proyek"=> "3. Dukungan bagi Pemerintah dalam Pengembangan Kebijakan dan Peningkatan Strategi Pelayanan Air Minum",
                    "Total Biaya (USD Juta)"=> 6.8,
                    "IBRD (USD Juta)"=> 5,
                    "APBN CK (USD Juta)"=> 1,
                    "APBD/ PDAM (USD Juta)"=> null,
                    "Sumber Dana Lain (USD Juta)"=> 0.8
                ],
                [
                    "Komponen Proyek"=> "4. Dukungan Manajemen dan Pelaksanaan Proyek",
                    "Total Biaya (USD Juta)"=> 20.3,
                    "IBRD (USD Juta)"=> 15,
                    "APBN CK (USD Juta)"=> 3.3,
                    "APBD/ PDAM (USD Juta)"=> 2,
                    "Sumber Dana Lain (USD Juta)"=> null
        
                ],
                [
                    "Komponen Proyek"=> "Total",
                    "Total Biaya (USD Juta)"=> 602.6,
                    "IBRD (USD Juta)"=> 100,
                    "APBN CK (USD Juta)"=> 80.3,
                    "APBD/ PDAM (USD Juta)"=> 104.5,
                    "Sumber Dana Lain (USD Juta)"=> 317.8
                ]
            ]
        ];
        if(file_exists(storage_path('app/leverance.json'))){
            return json_decode(file_get_contents(storage_path('app/leverance.json')),true);
        }else{
            return $lev;
        }
    }
    public function index()
    {
        $data=static::getContent();
        return view('leverancemodule::index')->with(['data'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('leverancemodule::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('leverancemodule::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('leverancemodule::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
