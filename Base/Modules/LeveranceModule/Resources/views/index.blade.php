@extends('adminlte::page')

@section('content_header')

@stop

@section('content')
   <div class="container mt-3">
    <div class="d-flex bg-white rounded shadow">
        <table class="table table-bordered mb-0">
            <thead class="thead-dark">
                <tr>
                    @foreach ($data['header'] as $item)
                      <th>
                        {{$item['label']}}
                      </th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @foreach ($data['data'] as $item)
                    <tr>
                        @foreach ($data['header'] as $h)
                        <td>
                            @if($h['type']=='numeric')
                            {{number_format($item[$h['field']],2,'.',',')}}
                            @else
                            {{$item[$h['field']]}}
                            @endif
                        </td>
                    @endforeach
                    </tr>
                @endforeach

            </tbody>
        </table>
    </div>
   </div>
   
@endsection
