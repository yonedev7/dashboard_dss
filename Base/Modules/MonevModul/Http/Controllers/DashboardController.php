<?php

namespace Modules\MonevModul\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use Auth;
use Str;
use App\Http\Controllers\FilterCtrl;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

    public static function penilaian_kpi($tahun,$id_kpi,Request $request){
        $filter=['regional'=>$request->filter_regional,'sortlist'=>$request->filter_sortlist,'lokus'=>true,'tipe_bantuan'=>$request->filter_tipe_bantuan];
        $pemdas=FilterCtrl::filter_pemda($filter['regional'],$filter['lokus'],$filter['sortlist'],$filter['tipe_bantuan']);

        $data= DB::table('analisa.penilaian_kpi as pkpi')
        ->join('master.kpi as kpi',[['kpi.id','=','pkpi.id_kpi'],['pkpi.tahun','>=','kpi.tahun_mulai'],['pkpi.tahun','<=','kpi.tahun_selesai']])
        ->join('master.master_pemda as pd','pd.kodepemda','=','pkpi.kodepemda')
        ->where('pkpi.id_kpi',$id_kpi)
        ->where('pkpi.verifikasi',true)
        ->selectRaw("pkpi.tahun,pkpi.kodepemda,max(pd.nama_pemda) as nama_pemda , bool_and(pkpi.terpenuhi) as terpenuhi",)
        ->groupBy('pkpi.tahun','pkpi.kodepemda')
        ->orderBy('pkpi.tahun','asc')
        ->whereIn('pkpi.kodepemda',$pemdas)
        ->get();

        return $data;

        
    }

    static function metakpi($tahun){
        $data=DB::table('master.kpi as kpi')
        ->where('tahun_mulai','<=',$tahun)
        ->where('tahun_selesai','>=',$tahun)
        ->orderBy('id','asc')
        ->get()->toArray();

        foreach($data as $key=>$d){
            $data[$key]->data=DB::table('master.kpi_data_monev')->where('id_kpi',$d->id)->orderBy('index','asc')->get()->toArray();
        }

        return $data;
        
    }

    public function index($tahun,Request $request)
    {
        $meta_kpi=static::metakpi($tahun);
        $select_kpi=[];
        foreach($meta_kpi as $key=>$kpi){
            $select_kpi[]="(case when (max(pkpi.id_kpi)=".$kpi->id.") then bool_and(pkpi.terpenuhi) else false end  )  as pem_kpi_".$kpi->id;
        }

        if($request->kpi){
            $kpi_select=array_filter($meta_kpi,function($el) use ($request){
                $el=(array)$el;
                return $el['id']==$request->kpi;
            });
            if(count($kpi_select)){
                $kpi_select=array_values($kpi_select)[0];
            }else{
                $kpi_select=isset($meta_kpi[0])?$meta_kpi[0]:null;
            }

        }else{
            $kpi_select=isset($meta_kpi[0])?$meta_kpi[0]:null;
        }

        if(!$meta_kpi){
            return 'tidak terdapat KPI';
        }

        $filter=['regional'=>$request->filter_regional,'sortlist'=>$request->filter_sortlist,'lokus'=>true,'tipe_bantuan'=>$request->filter_tipe_bantuan];
        
        $pemdas=FilterCtrl::filter_pemda($filter['regional'],$filter['lokus'],$filter['sortlist'],$filter['tipe_bantuan']);
        
        $tables=(Array)DB::table('master.dataset as dts')->whereIn('id',array_map(function($el){
            return $el->id_data;
        },$kpi_select->data))->select('*')->get()->toArray();
        $raw_union=null;


        foreach($tables as $key=>$tb){
            $table_asli=$tb->table;
            $table_status_asli='dts_'.$tb->id;

            if(str_contains($tb->table,'[TAHUN]')){
                $tb->table=str_replace('[TAHUN]',$tahun,$tb->table);
                $table_status_asli='dts_'.$tb->id."_".$tahun;

            }

            $exist_table=DB::table('pg_tables')->whereIn('schemaname',['dataset','status_data'])->whereIn('tablename',[$tb->table,$table_status_asli])->get();

           $schema=array_filter($kpi_select->data,function($el) use ($tb){
               return $el->id_data==$tb->id;
           });
           $schema=array_values($schema);

           if(count($schema)){
               $schema=$schema[0];
           }

           if(count($exist_table)>=2){
                // if($raw_union==null){
                //     switch($schema->type_baseline){
                //         case 'basis_bantuan_stimulan':
                //             $raw_union=DB::table('dataset.'.$tb->table." as tb".$key)
                //             ->join('master.pemda_lokus as lk',[['lk.tipe_bantuan','=',DB::raw("'STIMULAN'")],['lk.kodepemda','=',("tb".$key).".kodepemda"],['lk.tahun_bantuan','>=',("tb".$key).".tahun"]])
                //             ->selectRaw( ("tb".$key.".").'kodepemda'.", max(".("tb".$key.".")."tahun) as tahun,'".$tb->name."' as exist_data")
                //             ->where(("tb".$key.".").'tahun','<=',$tahun)
                //             ->where(("tb".$key.".").'status','=',1)
                //             ->where(("tb".$key.".").'tw','=',4)
                //             ->groupBy(("tb".$key.".").'kodepemda');
                //         break;

                //         case 'basis_bantuan_pendamping':
                //             $raw_union=DB::table('dataset.'.$tb->table." as tb".$key)
                //             ->join('master.pemda_lokus as lk',[['lk.tipe_bantuan','=',DB::raw("'PENDAMPING'")],['lk.kodepemda','=',("tb".$key).".kodepemda"],['lk.tahun_bantuan','>=',("tb".$key).".tahun"]])
                //             ->selectRaw( ("tb".$key.".").'kodepemda'.", max(".("tb".$key.".")."tahun) as tahun,'".$tb->name."' as exist_data")
                //             ->where(("tb".$key.".").'tahun','<=',$tahun)
                //             ->where(("tb".$key.".").'status','=',1)
                //             ->where(("tb".$key.".").'tw','=',4)
                //             ->groupBy(("tb".$key.".").'kodepemda');
                //         break;

                //         case 'basis_bantuan_basis_kinerja':
                //             $raw_union=DB::table('dataset.'.$tb->table." as tb".$key)
                //             ->join('master.pemda_lokus as lk',[['lk.tipe_bantuan','=',DB::raw("'BASIS KINERJA'")],['lk.kodepemda','=',("tb".$key).".kodepemda"],['lk.tahun_bantuan','>=',("tb".$key).".tahun"]])
                //             ->selectRaw( ("tb".$key.".").'kodepemda'.", max(".("tb".$key.".")."tahun) as tahun,'".$tb->name."' as exist_data")
                //             ->where(("tb".$key.".").'tahun','<=',$tahun)
                //             ->where(("tb".$key.".").'status','=',1)
                //             ->where(("tb".$key.".").'tw','=',4)
                //             ->groupBy(("tb".$key.".").'kodepemda');
                //         break;
                //         default:
                //         $raw_union=DB::table('dataset.'.$tb->table." as tb".$key)
                //         ->selectRaw( ("tb".$key.".").'kodepemda'.", max(".("tb".$key.".")."tahun) as tahun,'".$tb->name."' as exist_data")
                //         ->where(("tb".$key.".").'tahun','<=',$tahun)
                //         ->where(("tb".$key.".").'status','=',1)
                //         ->where(("tb".$key.".").'tw','=',4)
                //         ->groupBy(("tb".$key.".").'kodepemda');
                //         break;
                //     }
                   
                //     if(str_contains($table_asli,'[TAHUN]')){
                //         foreach([$tahun,$tahun-1,$tahun-2,$tahun-3,$tahun-4,$tahun-5] as $k=>$th){
                            
                //         }
                //     }
                   
                // }else{
                //     $raw_union=$raw_union->union(DB::table('dataset.'.$tb->table." as tb".$key)
                //     ->selectRaw( ("tb".$key.".").'kodepemda'.", max(".("tb".$key.".")."tahun) as tahun,'".$tb->name."' as exist_data")
                //     ->where(("tb".$key.".").'tahun','<=',$tahun)
                //     ->where(("tb".$key.".").'status','=',true)
                //     ->where(("tb".$key.".").'tw','=',4)
                //     ->groupBy(("tb".$key.".").'kodepemda'));

                // }

                    $x=(DB::table('dataset.'.$tb->table." as tb".$key)
                    ->join('status_data.'.$table_status_asli." as std",[['std.kodepemda','=','tb'.$key.".".'kodepemda'],['std.tahun','=','tb'.$key.'.tahun']])
                    ->selectRaw( ("tb".$key.".").'kodepemda'.", max(".("tb".$key.".")."tahun) as tahun,'".$tb->name."' as exist_data")
                    ->where(("tb".$key.".").'tahun','<=',$tahun)
                    ->where(("tb".$key.".").'status','=',true)
                    ->where(("tb".$key.".").'tw','=',4)
                    ->whereIn(("tb".$key.".").'kodepemda',array_map(function($el){
                        return "'".$el."'";
                    },$pemdas))
                    ->groupBy(("tb".$key.".").'kodepemda'));
                    if($raw_union){
                        $x=$x->union($raw_union);
                    }
                    $raw_union=$x;


                    if(str_contains($table_asli,'[TAHUN]')){

                        foreach([$tahun-1,$tahun-2,$tahun-3,$tahun-4,$tahun-5] as $k=>$th){
                            $tb_bind=str_replace('[TAHUN]',$th,$table_asli);
                            $ex_bind=str_replace('[TAHUN]','%',$table_asli);

                           $ex=DB::table('pg_tables')->where('schemaname','dataset')->where('tablename','ilike',$ex_bind)->get()->pluck('tablename')->toArray();
                            if(in_array($tb_bind,$ex)){
                                $x=(DB::table('dataset.'.$tb_bind." as tb".$key)
                                ->selectRaw( ("tb".$key.".").'kodepemda'.", max(".("tb".$key.".")."tahun) as tahun,'".$tb->name."' as exist_data")
                                ->where(("tb".$key.".").'tahun','<=',$th)
                                ->where(("tb".$key.".").'status','=',true)
                                ->where(("tb".$key.".").'tw','=',4)
                                ->whereIn(("tb".$key.".").'kodepemda',array_map(function($el){
                                    return "'".$el."'";
                                },$pemdas))
                                ->groupBy(("tb".$key.".").'kodepemda'));
                                if($raw_union){
                                    $x=$x->union($raw_union);
                                }


                                $raw_union=$x;
                                
                            }else{
                            }
                           
                        }
                    }




           }

            
        }
    
            
            if($raw_union){
                $raw_union=(Str::replaceArray('?', $raw_union->getBindings(), $raw_union->toSql()));
                $raw_union=DB::table('master.master_pemda as pd')
                ->join(DB::raw('('.($raw_union).') as data'),'data.kodepemda','=','pd.kodepemda')
                ->leftJoin('master.pemda_lokus as lk','lk.kodepemda','=','pd.kodepemda')
                ->selectRaw("pd.kodepemda,max(pd.nama_pemda) as nama_pemda,
                max(pd.regional_1) as regional_1,
                max(pd.regional_2) as regional_2,
            ( string_agg(distinct(concat(lk.tipe_bantuan,' - ',lk.tahun_bantuan)),'@')) as lokus_bantuan,
                string_agg(concat(data.exist_data),'@') as exist_data,
                ".implode(',',$select_kpi))
                ->join('analisa.penilaian_kpi as pkpi',[['pkpi.kodepemda','=','pd.kodepemda'],['pkpi.verifikasi','=',DB::raw('true')],['pkpi.tahun','=',DB::raw($tahun)]])
                ->orderBy('pd.kodepemda','ASC')
                ->whereIn('pd.kodepemda',$pemdas)
                ->groupBy('pd.kodepemda')->get()->toArray();
            }

           


      
        $page_meta=[
            'title'=>'Keterisian '.$kpi_select->name.' Tahun '.$tahun,
            'keterangan'=>$kpi_select->keterangan
        ];


        
        return view('monevmodul::dashboard.index')->with([
            'page_meta'=>$page_meta,
            'pemda_list'=>$raw_union,
            'pemda_scope'=>count($pemdas),
            'kpi_list'=>$meta_kpi,
            'req'=>$request->all(),
            'indexKpi'=>$kpi_select->id
        ]);
    }

   
    public function detail($tahun,$kpi,$kodepemda,Request $request)
    {
        $meta_kpi=static::metakpi($tahun);


        if($kpi){
        $kpi_select=array_filter($meta_kpi,function($el) use ($kpi){
            $el=(array)$el;
            return $el['id']==$kpi;
        });

        if(count($kpi_select)){
            $kpi_select=array_values($kpi_select)[0];
        }else{
            $kpi_select=isset($meta_kpi[0])?$meta_kpi[0]:null;
        }

        }else{
            $kpi_select=isset($meta_kpi[0])?$meta_kpi[0]:null;
        }

        $penilaian_kpi=DB::table('analisa.penilaian_kpi as pkpi')->where('kodepemda',$kodepemda)
        ->where('tahun',$tahun)->where('id_kpi',$kpi_select->id)
        ->where('verifikasi',true)
        ->first();

        if(!$meta_kpi){
            return 'tidak terdapat KPI';
        }

       $chart=[];

       $pemda=DB::table('master.master_pemda')
       ->where('kodepemda',$kodepemda)
       ->first();

       if(!$pemda){
        return abort(404); 
       }
    
       $page_meta=[
            'title'=>''.$kpi_select->name,
            'id'=>$kpi_select->id,
            'pemda'=>$pemda,
            'kodepemda'=>$pemda->kodepemda,
            'keterangan'=>'PEMDA '.$pemda->nama_pemda
        ];

        foreach($kpi_select->data as $key=>$d){
            $d=(array)$d;
            if($d['id_data']){
                $kpi_select->data[$key]->dataset=DB::table('master.dataset as d')->where('id',$d['id_data'])->first();
                $kpi_select->data[$key]->pemda=$pemda;
            }
        }

       return view('monevmodul::dashboard.detail')->with([
            'page_meta'=>$page_meta,
            'kpi'=>$kpi_select,
            'kpi_list'=>static::metakpi($tahun),
            'fingerprint'=>$request->fingerprint()
       ]);
       

        // $proyek=DB::table('master.pemda_lokus')->where('kodepemda',$kodepemda)->orderBy('id','asc')->get()->toArray();
        // foreach($kpi_select->data as $key=>$kpi){
        //     $start_tahun=2019;
        //     $end_tahun=$tahun;
        //     if($kpi->type_baseline=='all_years'){
        //         $start_tahun=2019;
                
        //     }else if($kpi->type_baseline=='basis_bantuan_pendampingan'){
        //         foreach($proyek as $kp=>$p){
        //             if($p->tipe_bantuan=='PENDAMPINGAN'){
        //                 $start_tahun=$p->tahun_proyek;
        //             }
        //         }

        //     }else if($kpi->type_baseline=='basis_bantuan_stimulan'){
        //         foreach($proyek as $kp=>$p){
        //             if($p->tipe_bantuan=='STIMULAN'){
        //                 $start_tahun=$p->tahun_proyek??2019;
        //             }
        //         }
                
        //     }else if($kpi->type_baseline=='basis_bantuan_basis_kinerja'){
        //         if($p->tipe_bantuan=='BASIS KINERJA'){
        //             $start_tahun=$p->tahun_proyek??2019;
        //         }
        //     }

        //     $table=DB::table('master.dataset')->where('id',$kpi->id_data)->first();
        //     $filter_s=json_decode($kpi->field_select,true);
        //     $filter_select=[];
        //     foreach($filter_s as $k=>$f){
        //         $filter_select[$f['field']]=$f['name'];
        //     }

        //     $table_data=DB::table('master.dataset_data as sd')
        //     ->join('master.data as d','d.id','=','sd.id_data')
        //     ->orderby('sd.index','asc')
        //     ->selectRaw('sd.field_name,d.tipe_nilai,d.name,d.definisi_konsep,d.satuan')
        //     ->where('tipe_nilai','numeric')
        //     ->where('sd.id_dataset',$kpi->id_data)->get()->toArray();

        //     $table_data=array_filter($table_data,function($el) use ($filter_select){
        //            return  in_array($el->field_name,array_keys($filter_select));
        //     });



        //     $table_data=array_map(function($el) use ($filter_select){
        //             $el->name=$filter_select[$el->field_name];
        //             return $el;
        //     },$table_data);


            
        //     if($table and $start_tahun!=0){
        //         $chart_def=[
        //             'title'=>[
        //                 'text'=>''.$table->name
        //             ],
        //             'chart'=>[
        //                 "type"=>'line'
        //             ],
        //             'xAxis'=>[
        //                 'type'=>'category',
        //                 'crosshair'=>true
        //             ],
        //             'yAxis'=>[
                        

        //             ],
        //             'plotOptions'=>[
        //                 'line'=>[
        //                     'dataLabels'=>[
        //                         'enabled'=>true,

        //                         'format'=>" {y:,0f} ({point.per:,.2f}%)"
        //                     ]
        //                 ]
        //             ],
        //             'tooltip'=>[
        //                 'pointFormat'=>'{point.series.name}: {point.y:,0f} ({point.satuan})'

        //             ],
        //             'data'=>[

        //             ]
        //         ];
        //         $select=[];
                    
        //         foreach($table_data as $kt=>$el){
        //             $select[]='sum('.$el->field_name.') as '.$el->field_name.",'".$el->satuan."' as ".$el->field_name."_satuan";
        //             if(!isset($chart_def['yAxis'][$el->satuan])){
        //                 $chart_def['yAxis'][$el->satuan]=[
        //                     'lables'=>[
        //                             'format'=>"{value:,} ".$el->satuan
        //                     ],
        //                     'title'=>[
        //                         'text'=>$el->satuan,
        //                     ],
        //                     'opposite'=>($kt%2 == 0)?true:false
        //                 ];
        //             }
        //         }


        //         $chart_def['yAxis']=array_values($chart_def['yAxis']);
        //         foreach( $chart_def['yAxis'] as $kt=>$el){
        //             $chart_def['yAxis'][$kt]['opposite']=($kt!=0)?($kt%2 == 0)?true:false:false;
        //         }


        //         if(str_contains($table->table,'[TAHUN]')){
        //             $sql_union=null;
        //             foreach([$tahun,$tahun-1,$tahun-2,$tahun-3,$tahun-4] as $th){
        //                     $tbth=str_replace('[TAHUN]',$th,$table->table);
        //                     $exist_table=DB::table('pg_tables')->where('schemaname','dataset')->where('tablename',$tbth)->first();
        //                     if($exist_table){
        //                         $x=DB::table('dataset.'.$tbth.' as tb')
        //                         ->selectRaw("tb.tahun,max(updated_at) as updated_at ".($select?','.implode(',',$select):''))
        //                         ->where([
        //                             ['kodepemda','=',DB::raw("'".$kodepemda."'")],
        //                             ['tw','=',4],
        //                             ['status','=',1],
        //                         ])->where('tb.tahun','>=',$start_tahun)
        //                         ->where('tb.tahun','<=',$end_tahun)
        //                         ->groupBy('tb.tahun')
        //                         ->orderBy('tb.tahun','asc');
    
        //                         if($sql_union){
        //                             $sql_union=$sql_union->union($x);
        //                         }else{
        //                             $sql_union=$x;
        //                         }
        //                     }

        //             }
        //             $chart_def['data']=DB::table(DB::raw('('.(Str::replaceArray('?', $sql_union->getBindings(), $sql_union->toSql())).') as tbx '))
        //                 ->selectRaw("tahun,max(updated_at) as updated_at ".($select?','.implode(',',$select):''))
        //                 ->where('tbx.tahun','>=',$start_tahun)
        //                 ->where('tbx.tahun','<=',$end_tahun)
        //                 ->groupBy('tbx.tahun')
        //                 ->orderBy('tbx.tahun','asc')
        //                 ->get();

        //         }else{
        //             $exist_table=DB::table('pg_tables')->where('schemaname','dataset')->where('tablename',$table->table)->first();

        //             if($exist_table){
        //                 $chart_def['data']=DB::table('dataset.'.$table->table.' as tb')
        //                 ->selectRaw("tb.tahun,max(updated_at) as updated_at ".($select?','.implode(',',$select):''))
        //                 ->where([
        //                     ['kodepemda','=',$kodepemda],
        //                     ['tw','=',4],
        //                     ['status','=',1],
        //                 ])->where('tb.tahun','>=',$start_tahun)
        //                 ->where('tb.tahun','<=',$end_tahun)
        //                 ->groupBy('tb.tahun')
        //                 ->orderBy('tb.tahun','asc')
        //                 ->get();
    
    
        //                 // if($table->id==104){
        //                 //     dd($table_data);
        //                 // }
    
    
        //             }
                    
        //         }
               



               

        //         $data=[];
        //         foreach($chart_def['data'] as $k=>$d){
        //             $d=(Array)$d;
        //             $chart_def['data'][$key]=($d);

                   
        //             foreach($table_data as $kf=>$f){
        //                 if(!isset($data[$f->field_name])){
        //                     $data[$f->field_name]=[
        //                         'name'=>$f->name,
        //                         'per'=>0,
        //                         'satuan'=>$d[$f->field_name.'_satuan'],
        //                         'definisi'=>$f->definisi_konsep,
        //                         'data'=>[]
        //                     ];
        //                 }
        //                 if(!is_array($chart_def['data'][0])){
        //                     $chart_def['data'][0]=(Array)$chart_def['data'][0];
        //                 }
        //                 $f_data=(float)(isset($data[$f->field_name]['data'][0])?(float)( $data[$f->field_name]['data'][0]['y']):((float)$d[$f->field_name]));

                        
        //                 $gap=((float)$d[$f->field_name]) - $f_data;
                       
        //                 if($f_data==0){
        //                     $per=(float)$d[$f->field_name];
        //                 }else if($gap==0){
        //                     $per=0;
        //                 }else{
        //                     $per=($gap/$f_data*100);
        //                 }

        //                 $indexY=0;
        //                 foreach($chart_def['yAxis'] as $kt=>$el){
        //                     if($el['title']['text']==$f->satuan){
        //                          $indexY=$kt;

        //                     }
        //                 }
        //                 $data[$f->field_name]['data'][]=[
        //                     'name'=>$d['tahun'],
        //                     'per'=>$per,
        //                     'yAxis'=>$indexY,
        //                     'y'=>(float)$d[$f->field_name],
        //                     'satuan'=>$d[$f->field_name.'_satuan'],
        //                     'value'=>(float)$d[$f->field_name]
        //                 ];
                        
        //             }
                    
        //         }

        //         $chart_def['series']=array_values($data);
        //         // dd($chart_def);
        //         foreach($chart_def['series'] as $key=>$s){
        //             $nilai_awal=isset($s['data'][0])?$s['data'][0]['y']:0;
        //             $nilai_ahir=(count($s['data'])-1)>-1?$s['data'][count($s['data'])-1]['y']:0;
        //             $selisih=($nilai_ahir-$nilai_awal);
                   

        //         }
        //         unset($chart_def['data']);
        //         $chart[]=[
        //             "dataset"=>$table,
        //             "chart"=>$chart_def
        //         ];

                
        //     }


        // }
        

        // $chart=array_filter($chart,function($el){
        //     return count($el['chart']['series']);
        // });

        // return view('monevmodul::dashboard.detail')->with([
        //     'page_meta'=>$page_meta,
        //     'charts'=>array_values($chart),
        //     'penilaian_kpi'=>$penilaian_kpi,
        //     'kpi_list'=>array_values($meta_kpi),
        //     'fingerprint'=>$request->fingerprint()
        // ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('monevmodul::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
       
    }
}
