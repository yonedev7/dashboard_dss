<?php

namespace Modules\MonevModul\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Auth;
use Alert;
use DB;
use Carbon\Carbon;
use Str;
use App\Http\Controllers\FilterCtrl;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */


    public function select_pemda_input($tahun,$id_monev_data,Request $request){
        $sc=DB::table('master.monev_data')->find($id_monev_data);
        $MyUser=Auth::User();
        if($sc){
            $dataset=DB::table('master.dataset as dts')->find($sc->id_data_kesepakatan);
            if($dataset){
                if($dataset->jenis_dataset){
                    $scope_bantuan=json_decode($dataset->klasifikasi_2??'[]',true);
                    if(count($scope_bantuan)){
                        $scope_bantuan=array_map(function($el){
                            return str_replace('BANTUAN ','',strtoupper($el));
                        },$scope_bantuan);
                        $lokus=true;
                    }else{
                        $scope_bantuan=$request->filter_tipe_bantuan;
                        $lokus=false;
                    }

                    $regional=$request->filter_regional;
                    if($MyUser->roleCheck('TACT REGIONAL')){
                        $lokus=true;
                        $regional=$MyUser->regional;
                    }

                    $filter=['regional'=>$regional,'sortlist'=>$request->filter_sortlist,'lokus'=>$lokus,'tipe_bantuan'=>$scope_bantuan];
                    $pemdas=FilterCtrl::filter_pemda($filter['regional'],$filter['lokus'],$filter['sortlist'],$filter['tipe_bantuan']);
                    if(str_contains($dataset->table,'[TAHUN]')){
                        $dataset->table=str_replace('[TAHUN]',$tahun,$dataset->table);
                    }

                    $exist_table=DB::table('pg_tables')->where('schemaname','dataset')->where('tablename',$dataset->table)->first();
                    if($exist_table){
                        $sql_data=DB::table('dataset.'.$dataset->table.' as dtd')
                        ->where(['dtd.tw'=>DB::raw(4),'dtd.tahun'=>DB::raw($tahun)])
                        ->whereIn('dtd.kodepemda',(
                            array_map(function($el){ return "'".$el."'";},$pemdas)))
                        ->selectRaw("dtd.kodepemda,count(case when dtd.status=1 then dtd.id else null end) as terverifikasi,count(dtd.id) as terdata")
                        ->groupBy('dtd.kodepemda');
                        $sql_data=Str::replaceArray('?', $sql_data->getBindings(), $sql_data->toSql());

                    }else{
                        $sql_data="select 0 as terdata,0 as terverifikasi,'-1' as kodepemda ";

                    }


                    if(strtoupper($dataset->klasifikasi_1)=='DATA PEMDA'){
                        $pemda_list=DB::table('master.master_pemda as pd')
                        ->leftJoin(DB::raw( "(".$sql_data.") as d"),'d.kodepemda','=','pd.kodepemda')
                        ->leftJoin('master.pemda_lokus as lk','lk.kodepemda','=','pd.kodepemda')
                        ->selectRaw("pd.kodepemda,max(pd.nama_pemda) as nama_pemda,
                        max(pd.regional_1) as regional_1,
                        max(pd.regional_2) as regional_2,
                        ( string_agg(distinct(concat(lk.tipe_bantuan,' - ',lk.tahun_bantuan)),'@')) as tipe_bantuan,
                        max(d.terdata) as terdata,
                        max(d.terverifikasi) as terverifikasi
                        ")
                        ->orderBy('pd.kodepemda','ASC')
                        ->whereIn('pd.kodepemda',$pemdas)
                        ->groupBy('pd.kodepemda')->get()->toArray();
        
                    }else{
                        $pemda_list=DB::table('master.master_pemda as pd')
                        ->leftJoin(DB::raw( "(".$sql_data.") as d"),'d.kodepemda','=','pd.kodepemda')
                        ->leftJoin('master.pemda_lokus as lk','lk.kodepemda','=','pd.kodepemda')
                        ->join('master.master_bu_pemda as bub','bub.kodepemda','=','pd.kodepemda')
                        ->leftJoin('master.master_bu as bu','bu.id','=','bub.id_bu')
                        ->selectRaw("
                        max(bub.kodepemda) as kodepemda,
                        max(pd.nama_pemda) as nama_pemda,
                        max(bub.id_bu) as kodebu,
                        concat(max(bub.id_bu),'@',max(bub.kodepemda)) as kodebu_integrate,
                        max(bu.nama_bu) as nama_bu,
                        max(pd.regional_1) as regional_1,
                        max(pd.regional_2) as regional_2,
                        concat( max(pd.nama_pemda),' - ',max(bu.nama_bu)) as name,
                        ( string_agg(distinct(concat(lk.tipe_bantuan,' - ',lk.tahun_bantuan)),'@')) as tipe_bantuan,
                        max(d.terdata) as terdata,
                        max(d.terverifikasi) as terverifikasi
                        ")
                        ->orderBy(DB::raw('max(bub.kodepemda)'),'ASC')
                        ->whereIn('pd.kodepemda',$pemdas)
                        ->groupBy('bub.id')->get()->toArray();
        
                    }

                }
                $page_meta=['title'=>$dataset->name,'keterangan'=>$dataset->tujuan];
                return view('monevmodul::form.select_pemda')->with(['id_monev_data'=>$id_monev_data,'req'=>$request,'pemdas'=>$pemda_list,'page_meta'=>$page_meta]);
            }

        }
    }

    public function form_edit($tahun,$id_monev_data,$kodepemda=null,Request $request){
        $sc=DB::table('master.monev_data')->find($id_monev_data);
        if($sc){
            $dataset=DB::table('master.dataset as dts')->find($sc->id_data_kesepakatan);
            if($dataset){
                if($dataset->jenis_dataset){
                    if($kodepemda){
                    return \Modules\DatasetModul\Http\Controllers\DatasetModulController::detail($tahun,$sc->id_data_kesepakatan,4,$kodepemda,$request);

                    }else{
                        return redirect()->route('mod.monev-admin.form.select-pemda-input',['tahun'=>$tahun,'id_monev_data'=>$id_monev_data]);

                    }
                }else{
                    return \Modules\DatasetModul\Http\Controllers\DatasetModulController::detail($tahun,$sc->id_data_kesepakatan,4,null,$request);

                }
            }

        }
    }

    public function update_ddub($tahun,$id_dataset,$tw,Request $request){
        $data_check=[];
        $uid=Auth::User()->id;
        $now=Carbon::now();


       DB::beginTransaction();
       try{
        foreach($request->data??[] as $k=>$d){
            if($d['method']=='update'){
                 foreach($d as $f=>$val){
                     if(str_contains($f,'bind_real_')){
                         $data_check[]=[
                             'id'=>isset($d['bind_id_'.str_replace('bind_real_','',$f)])?$d['bind_id_'.str_replace('bind_real_','',$f)]:null,
                             'tahun'=>str_replace('bind_real_','',$f),
                             'tw'=>$tw,
                             'status'=>$d['status'],
                             'kodepemda'=>$d['kodepemda'],   
                             'ddub_komitmen'=>(int)$d['pagu'],
                             'ddub_perencanaan'=>(int)$d['bind_pagu_'.str_replace('bind_real_','',$f)],
                             'ddub_realisasi'=>(int)$d[$f],
                             'ddub_gap'=>((int)$d['pagu']) - ((int)$d[$f]),
                             'user_pengisi'=>$uid
                             
                         ];
                         
                     }
                 }
            }else{
                $deleid=explode('-',$d['id']);
 
                if(count($deleid)){
                    DB::table('dataset.pemda_ddub')->whereIn('id',$deleid)->delete();
                }
 
            }
         }
 
 
 
         foreach($data_check as $d){
             
            if($d['id']){
             $new=$d;
             unset($new['id']);
             DB::table('dataset.pemda_ddub')->updateOrInsert([
                 'id'=>$d['id'],
             ],
             $new);
            }else{
             $new=$d;
             unset($new['id']);
             DB::table('dataset.pemda_ddub')->updateOrInsert([
                 'kodepemda'=>$d['kodepemda'],
                 'tw'=>$d['tw'],
                 'tahun'=>$d['tahun'],
             ],
             $new);
            }
         }
         DB::commit();
       }catch(\Exection $e){
            DB::rollBack();
            Alert::error('',$e->getMessage());   
        return back();
       }
        Alert::success('','Berhasil Memperbarui data DDUB');
        return back();
    }

    public function ddub($tahun,$id_dataset,$tw,$kodepemda=null,Request $request){
        $page_meta=(array)DB::table('master.dataset')->find($id_dataset);
        $page_meta['title']=$page_meta['name'];
        $page_meta['keterangan']=$page_meta['definisi_konsep'];
        $page_meta['tw']=$tw;
        $page_meta['monev']=$tahun;
        $request['basis']=\_g_basis($request);

        if(!in_array(strtoupper($request->basis),['LONGLIST','SORTLIST'])){
            $request['basis']='LONGLIST';
        }

        $columns=[
            [
                'key'=>'status',
                'label'=>'Verifikasi',
                'editable'=>true,
                'tipe_nilai'=>'numeric',
                'type'=>'options',
                'options'=>[
                  [
                    'tag'=>'Terverifikasi',
                    'value'=>1
                  ],
                  [
                    'tag'=>'Belum Terverifikasi',
                    'value'=>0
                  ]
                ],
                'bind_view'=>[
                    [
                      'logic'=>'#val == 0',
                      'tag'=>'Belum Teverifikasi'
                    ],
                    [
                        'logic'=>'#val == 1',
                        'tag'=>'Teverifikasi'
                        ],
                  ]


            ],
            [
                'key'=>'regional_1',
                'label'=>'Regional',
                'editable'=>false,
                'tipe_nilai'=>'string',
                'type'=>'input-value',
                'bind_view'=>[],


            ],
          
            [
                'key'=>'regional_2',
                'label'=>'Wilayah',
                'editable'=>false,
                'tipe_nilai'=>'string',
                'type'=>'input-value',
                'bind_view'=>[],


            ],
            
            [
                'key'=>'tipe_bantuan',
                'label'=>'Tipe Bantuan',
                'editable'=>false,
                'tipe_nilai'=>'string',
                'type'=>'input-value',
                'bind_view'=>[],


            ],
            [
                'key'=>'tahun_proyek',
                'label'=>'Tahun Proyek',
                'editable'=>false,
                'tipe_nilai'=>'string',
                'type'=>'input-value',
                'bind_view'=>[
                    [
                      'logic'=>'#val == 1',
                      'tag'=>'LONGLIST'
                    ],
                    [
                        'logic'=>'#val > 200',
                        'tag'=>'[val]'
                        ],
                        [
                        'logic'=>'(#val == 0) || (#val==null)',
                        'tag'=>''
                    ],
                  ]


            ],
            [
                'key'=>'kodepemda',
                'label'=>'Kodepemda',
                'editable'=>$kodepemda!=null?false:true,
                'tipe_nilai'=>'string',
                'type'=>'input-value',
                'bind_view'=>[],



            ],
            [
                'key'=>'name',
                'label'=>'Nama Pemda',
                'editable'=>false,
                'tipe_nilai'=>'string',
                'type'=>'input-value',
                'bind_view'=>[],

            ],
          
            [
                    'id'=>null,
                    'key'=>'pagu',
                    'label'=>'Komitmen DDUB',
                    'editable'=>true,
                    'tipe_nilai'=>'numeric',
                    'type'=>'input-value',
                    'definsisi_konsep'=>'',
                    'classification'=>false,
                    'produsent_data'=>'-',
                    'validator'=>'-',
                    'satuan'=>'Rp.',
                    'file_accept'=>[],
                    'options'=>[],
                    'bind_view'=>[],
            ]
        ];
        for($i=2019;$i<=2023;$i++){
            $columns[]=[
                'id'=>null,
                'key'=>'bind_pagu_'.$i,
                'label'=>'Pagu DDUB '.$i,
                'editable'=>true,
                'tipe_nilai'=>'numeric',
                'type'=>'input-value',
                'definsisi_konsep'=>'',
                'classification'=>false,
                'produsent_data'=>'-',
                'validator'=>'-',
                'satuan'=>'Rp.',
                'file_accept'=>[],
                'options'=>[],
                'bind_view'=>[],
            ];
            $columns[]=[
                'id'=>null,
                'key'=>'bind_real_'.$i,
                'label'=>'Realisasi DDUB '.$i,
                'editable'=>true,
                'tipe_nilai'=>'numeric',
                'type'=>'input-value',
                'definsisi_konsep'=>'',
                'classification'=>false,
                'produsent_data'=>'-',
                'validator'=>'-',
                'satuan'=>'Rp.',
                'file_accept'=>[],
                'options'=>[],
                'bind_view'=>[],
            ];
        }
        $pemda_list=DB::table('master.master_pemda as mp')
        ->selectRaw('kodepemda, nama_pemda as name,tahun_proyek,tipe_bantuan,regional_1,regional_2')
        ->orderBy('kodepemda');
        $where=_filter_l1($tahun,'provinsi kab/kota',$request->basis,[['mp.kodepemda','!=','0']]);
        unset($where[0]);
        $pemda_list=$pemda_list->where($where)->get();
        $page_meta['pemda_list']=$pemda_list;
        return view('monevmodul::form.ddub')->with(['page_meta'=>$page_meta,'basis'=>$request->basis,'columns'=>$columns]);
    }

   public function load_ddub($tahun,$id_dataset,$tw,Request $request){
       $tahun_list=[];
       $paginate=50;
       $select=[
            "string_agg(d.id::varchar,'-') as id",
            "mp.kodepemda",
           'max(mp.nama_pemda) as name',
           'max(mp.tahun_proyek) as tahun_proyek',
           'max(mp.tipe_bantuan) as tipe_bantuan',
           'max(mp.regional_1) as regional_1',
           'max(mp.regional_2) as regional_2',
           'max(case when (d.tahun='.$tahun.') then d.ddub_komitmen else 0 end)  pagu',
           'max(case when (d.tahun='.$tahun.') then d.status else 0 end) as status',

       ];
       for ($i=2019; $i<=2023 ; $i++) { 
            $tahun_list[]=$i;
            $select[]="max(case when (d.tahun=$i) then d.id else null end) as bind_id_".$i;
            $select[]="max(case when (d.tahun=$i) then d.ddub_realisasi else 0 end) as bind_real_".$i;
            $select[]="max(case when (d.tahun=$i) then d.ddub_perencanaan else 0 end) as bind_pagu_".$i;

            
       }

       
       $data=DB::table('dataset.pemda_ddub as d')
       ->selectRaw(implode(',',$select))
        ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda');
        $where=_filter_l1($tahun,$request->filter,$request->basis,[['d.tw','=',$tw]]);
        unset($where[0]);
        $data=$data->where($where)->whereIn('d.tahun',$tahun_list)->groupBy('mp.kodepemda')
        ->orderBy('mp.kodepemda','asc')
        ->paginate($paginate);
        

       return [
            'data'=>$data->items(),
            'current_page'=>$data->currentPage(),
            'last_page'=>$data->lastPage(),
            'paginate_count'=>$paginate,
            'total'=>$data->total()
       ];
   }

    public function index($tahun,Request $request)
    {
        $pp=[];

        $filter=['regional'=>$request->filter_regional,'sortlist'=>$request->filter_sortlist,'lokus'=>true,'tipe_bantuan'=>$request->filter_tipe_bantuan];
        $pemdas=FilterCtrl::filter_pemda($filter['regional'],$filter['lokus'],$filter['sortlist'],$filter['tipe_bantuan']);
        
        $page_meta=[
            'title'=>'Monitoring Dan Evaluasi',
            'keterangan'=>''
        ];

        $data=DB::table('master.monev_data as sc')
        ->join('master.dataset as dts','sc.id_data_kesepakatan','=','dts.id')
        ->selectRaw("max(dts.name) as name,sc.id,max(sc.kpi_name) as kpi_name,
        REGEXP_REPLACE(replace( concat(COALESCE(sc.id_data_base_1,0),'@',COALESCE(sc.id_data_base_2,0),'@',COALESCE(sc.id_data_base_3,0),'@'),'0@',''),'@$','') as enabled_sinkron, max(dts.table) as table_data,max(sc.id_data_kesepakatan) as id_dataset")
        ->groupBy('sc.id')
        ->orderBy('sc.kpi_name','asc')
        ->get();

        foreach($data as $k=>$d){
            if(str_contains($d->table_data,'[TAHUN]')){
                $d->table_data=str_replace('[TAHUN]',$tahun,$d->table_data);
                $datap[$k]->table_data=$d->table_data;
            }

            $exist=DB::table('information_schema.tables')->where('table_schema','dataset')
            ->where('table_name',$d->table_data)->first();

            if($exist){
                $data[$k]->exist_table=true;
                $data[$k]->data_report=DB::table('dataset.'.$d->table_data.' as d')
                ->join('master.pemda_lokus as lk','lk.kodepemda','=','d.kodepemda')
                ->whereIn('d.kodepemda',$pemdas)
                ->where([
                    ['d.tahun','=',$tahun],
                    ['d.tw','=',4]
                ])->selectRaw("count(distinct(case when d.status=1 then d.kodepemda else null end )) as terverifikasi,count(distinct(d.kodepemda)) as terdata")->first();
               
                $data[$k]->dataset=DB::table('master.dataset')->find($d->id_dataset);
                if($data[$k]->data_report){
                    $data[$k]->data_report=(Array)$data[$k]->data_report;
                }else{
                    $data[$k]->data_report=[
                        'terverifikasi'=>0,
                        'terdata'=>0,
                    ];
                }
            }else{
                $data[$k]->exist_table=false;
                $data[$k]->data_report=[
                    'terverifikasi'=>0,
                    'terdata'=>0,
                ];
            }

            $pp=[];
            foreach($data as $k=>$d){
                if(!isset($pp[$d->kpi_name??'-'])){
                    $pp[$d->kpi_name??'-']=[
                        'name'=>$d->kpi_name,
                        'data'=>[]
                    ];
                }
                $pp[$d->kpi_name??'-']['data'][]=$d;
            }
           
        }

    

        return view('monevmodul::form.index')->with(['page_meta'=>$page_meta,'data'=>array_values($pp),'req'=>$request->all()]);
    }

    public function sink($tahun,$id_monev_data,$id_dataset,$kodepemda=null,Request $request){
        $dataset=DB::table('master.dataset')->find($id_dataset);
        $req=$request->all();
        if(isset($req['lokus'])){
            if($req['lokus']=='false'){
                $req['lokus']=false;
            }
        }else{
            $req['lokus']=false;
        }


        if($dataset){
            if($dataset->jenis_dataset){

            }else{
                
            }

            if(str_contains($dataset->table,'[TAHUN]')){
                $dataset->table=str_replace('[TAHUN]',$tahun,$dataset->table);
            }


            $exist=DB::table('information_schema.tables')->where('table_schema','dataset')
            ->where('table_name',$dataset->table)
            ->first();

            if($exist){
                $columns=DB::table('master.dataset_data as d')
                ->join('master.data as sd','sd.id','=','d.id_data')
                ->selectRaw("sd.*,d.field_name, 300 as minwidth")
                ->where('d.id_dataset',$dataset->id)
                ->orderBy('d.index','asc')
                ->get()->toArray();
                

                $select=['d.status','d.keterangan','d.kodepemda','mp.nama_pemda','mp.regional_1','mp.regional_2'];
                $columns_like=[];
                $columns2=[
                    [
                        'field_name'=>'status',
                        'name'=>'STATUS DATA',
                        'clasification'=>false,
                        'tipe_nilai'=>'string',
                        'satuan'=>null,
                        'minwidth'=>200,
                        'options'=>'[{"tag":"Terverifikasi","value":1},{"tag":"Belum Terverifikasi","value":0}]',
                        'bind_view'=>'[{"tag":"Terverifikasi","logic":"#val == 1"},{"tag":"Belum Terverifikasi","logic":"#val == 0"}]',

                    ],
                    [
                        'field_name'=>'regional_1',
                        'name'=>'REGIONAL',
                        'minwidth'=>200,

                        'clasification'=>false,
                        'tipe_nilai'=>'string',
                        'satuan'=>null,
                        'options'=>'[]',
                        'bind_view'=>'[]',

                    ],
                    [
                        'field_name'=>'regional_2',
                        'name'=>'WILAYAH',
                        'minwidth'=>250,
                        'clasification'=>false,
                        'tipe_nilai'=>'string',
                        'satuan'=>null,
                        'options'=>'[]',
                        'bind_view'=>'[]',

                    ],
                    [
                        'name'=>'KODEPEMDA',
                        'minwidth'=>200,

                        'field_name'=>'kodepemda',
                        'clasification'=>false,
                        'tipe_nilai'=>'string',
                        'satuan'=>null,
                        'options'=>'[]'
                    ],
                    [
                        'name'=>'NAMA PEMDA',
                        'minwidth'=>250,

                        'field_name'=>'nama_pemda',
                        'clasification'=>false,
                        'tipe_nilai'=>'string',
                        'satuan'=>null,
                        'options'=>'[]'
                    ]
                ];
                $columns=array_merge($columns2,$columns);

                foreach($columns as $k=>$c){
                    $c=(Array)$c;
                    if(in_array($c['field_name'],['regional_1','regional_2','nama_pemda'])){
                    $select[]='mp.'.$c['field_name'];

                    }else{
                    $select[]='d.'.$c['field_name'];

                    }
                    $columns_like[$c['field_name']]=[
                        'val'=>null,
                        'field_name'=>$c['field_name'],
                        'field'=>$c['field_name'],

                        'op'=>'like',
                        'name'=>$c['name'],
                        'label'=>$c['name'],
                        'satuan'=>$c['satuan']
                    ];
                }


              
                

                return view('monevmodul::form.sink')->with(['id_monev_data'=>$id_monev_data,'dataset'=>$dataset,'columns'=>$columns,'columns_like'=>$columns_like]);

            }
           

        }else{
            return abort(404);
        }
        
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('monevmodul::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('monevmodul::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('monevmodul::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
