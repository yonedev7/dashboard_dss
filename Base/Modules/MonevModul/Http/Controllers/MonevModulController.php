<?php

namespace Modules\MonevModul\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Auth;
use Alert;
use DB;
class MonevModulController extends Controller
{
    

     public function index_admin($tahun,$kpi=0,Request $request)
     {
        {
            $meta_kpi=static::metakpi($tahun);
            $select_kpi=[];
            foreach($meta_kpi as $key=>$kpi){
                $select_kpi[]="(case when (max(pkpi.id_kpi)=".$kpi->id.") then bool_and(pkpi.terpenuhi) else false end  )  as pem_kpi_".$kpi->id;
            }
    
            if($request->kpi){
                $kpi_select=array_filter($meta_kpi,function($el) use ($request){
                    $el=(array)$el;
                    return $el['id']==$request->kpi;
                });
                if(count($kpi_select)){
                    $kpi_select=array_values($kpi_select)[0];
                }else{
                    $kpi_select=isset($meta_kpi[0])?$meta_kpi[0]:null;
                }
    
            }else{
                $kpi_select=isset($meta_kpi[0])?$meta_kpi[0]:null;
            }
    
            if(!$meta_kpi){
                return 'tidak terdapat KPI';
            }
    
            $filter=['regional'=>$request->filter_regional,'sortlist'=>$request->filter_sortlist,'lokus'=>true,'tipe_bantuan'=>$request->filter_tipe_bantuan];
            
            $pemdas=FilterCtrl::filter_pemda($filter['regional'],$filter['lokus'],$filter['sortlist'],$filter['tipe_bantuan']);
            
            $tables=(Array)DB::table('master.dataset as dts')->whereIn('id',array_map(function($el){
                return $el->id_data;
            },$kpi_select->data))->select('*')->get()->toArray();
            $raw_union=null;
    
    
            foreach($tables as $key=>$tb){
                $table_asli=$tb->table;
                $table_status_asli='dts_'.$tb->id;
    
                if(str_contains($tb->table,'[TAHUN]')){
                    $tb->table=str_replace('[TAHUN]',$tahun,$tb->table);
                    $table_status_asli='dts_'.$tb->id."_".$tahun;
    
                }
    
                $exist_table=DB::table('pg_tables')->whereIn('schemaname',['dataset','status_data'])->whereIn('tablename',[$tb->table,$table_status_asli])->get();
    
               $schema=array_filter($kpi_select->data,function($el) use ($tb){
                   return $el->id_data==$tb->id;
               });
               $schema=array_values($schema);
    
               if(count($schema)){
                   $schema=$schema[0];
               }
    
               if(count($exist_table)>=2){
                   
    
                        $x=(DB::table('dataset.'.$tb->table." as tb".$key)
                        ->join('status_data.'.$table_status_asli." as std",[['std.kodepemda','=','tb'.$key.".".'kodepemda'],['std.tahun','=','tb'.$key.'.tahun']])
                        ->selectRaw( ("tb".$key.".").'kodepemda'.", max(".("tb".$key.".")."tahun) as tahun,'".$tb->name."' as exist_data")
                        ->where(("tb".$key.".").'tahun','<=',$tahun)
                        ->where(("tb".$key.".").'status','=',true)
                        ->where(("tb".$key.".").'tw','=',4)
                        ->whereIn(("tb".$key.".").'kodepemda',array_map(function($el){
                            return "'".$el."'";
                        },$pemdas))
                        ->groupBy(("tb".$key.".").'kodepemda'));
                        if($raw_union){
                            $x=$x->union($raw_union);
                        }
                        $raw_union=$x;
    
    
                        if(str_contains($table_asli,'[TAHUN]')){
    
                            foreach([$tahun-1,$tahun-2,$tahun-3,$tahun-4,$tahun-5] as $k=>$th){
                                $tb_bind=str_replace('[TAHUN]',$th,$table_asli);
                                $ex_bind=str_replace('[TAHUN]','%',$table_asli);
    
                               $ex=DB::table('pg_tables')->where('schemaname','dataset')->where('tablename','ilike',$ex_bind)->get()->pluck('tablename')->toArray();
                                if(in_array($tb_bind,$ex)){
                                    $x=(DB::table('dataset.'.$tb_bind." as tb".$key)
                                    ->selectRaw( ("tb".$key.".").'kodepemda'.", max(".("tb".$key.".")."tahun) as tahun,'".$tb->name."' as exist_data")
                                    ->where(("tb".$key.".").'tahun','<=',$th)
                                    ->where(("tb".$key.".").'status','=',true)
                                    ->where(("tb".$key.".").'tw','=',4)
                                    ->whereIn(("tb".$key.".").'kodepemda',array_map(function($el){
                                        return "'".$el."'";
                                    },$pemdas))
                                    ->groupBy(("tb".$key.".").'kodepemda'));
                                    if($raw_union){
                                        $x=$x->union($raw_union);
                                    }
    
    
                                    $raw_union=$x;
                                    
                                }else{
                                }
                               
                            }
                        }
    
    
    
    
               }
    
                
            }
        
                
                if($raw_union){
                    $raw_union=(Str::replaceArray('?', $raw_union->getBindings(), $raw_union->toSql()));
                    $raw_union=DB::table('master.master_pemda as pd')
                    ->join(DB::raw('('.($raw_union).') as data'),'data.kodepemda','=','pd.kodepemda')
                    ->leftJoin('master.pemda_lokus as lk','lk.kodepemda','=','pd.kodepemda')
                    ->selectRaw("pd.kodepemda,max(pd.nama_pemda) as nama_pemda,
                    max(pd.regional_1) as regional_1,
                    max(pd.regional_2) as regional_2,
                ( string_agg(distinct(concat(lk.tipe_bantuan,' - ',lk.tahun_bantuan)),'@')) as lokus_bantuan,
                    string_agg(concat(data.exist_data),'@') as exist_data,
                    ".implode(',',$select_kpi))
                    ->join('analisa.penilaian_kpi as pkpi',[['pkpi.kodepemda','=','pd.kodepemda'],['pkpi.verifikasi','=',DB::raw('true')],['pkpi.tahun','=',DB::raw($tahun)]])
                    ->orderBy('pd.kodepemda','ASC')
                    ->whereIn('pd.kodepemda',$pemdas)
                    ->groupBy('pd.kodepemda')->get()->toArray();
                }
    
               
    
    
          
            $page_meta=[
                'title'=>'Keterisian '.$kpi_select->name.' Tahun '.$tahun,
                'keterangan'=>$kpi_select->keterangan
            ];
    
    
            
            return view('monevmodul::dashboard.index')->with([
                'page_meta'=>$page_meta,
                'pemda_list'=>$raw_union,
                'pemda_scope'=>count($pemdas),
                'kpi_list'=>$meta_kpi,
                'req'=>$request->all(),
                'indexKpi'=>$kpi_select->id
            ]);
        }
        # code...
     }

    public function load_dataset_data($tahun,$id_dataset,Request $request){

        $dataset=DB::table('master.dataset')->find($id_dataset);
        $req=$request->all();
        if(isset($req['lokus'])){
            if($req['lokus']=='false'){
                $req['lokus']=false;
            }
        }else{
            $req['lokus']=false;
        }


        if($dataset){
            if($dataset->jenis_dataset){

            }else{
                
            }

            if(str_contains($dataset->table,'[TAHUN]')){
                $dataset->table=str_replace('[TAHUN]',$tahun,$dataset->table);
            }

            $exist=DB::table('information_schema.tables')->where('table_schema','dataset')
            ->where('table_name',$dataset->table)
            ->first();


            if($exist){

                if($request->colums){

                    $select=[];
                    foreach($request->colums??[] as $k=>$c){
                        $c=(Array)$c;
                        if(in_array($c['field_name'],['regional_1','regional_2','nama_pemda'])){
                            $select[]='mp.'.$c['field_name'];
                        }else{
                            $select[]='d.'.$c['field_name'];
                        }
                    }

                    $select=array_unique($select);
                    $data=DB::table('dataset.'.$dataset->table.' as d')
                    ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
                    ->where([
                        ['d.tahun','=',$tahun],
                        ['d.tw','=',4]
                    ])
                    ->selectRaw(implode(',',$select));
                    if($req['lokus']){
                        $data=$data->join('master.pemda_lokus as lk','lk.kodepemda','=','d.kodepemda');
                    }

                    $data=$data->orderBy('d.kodepemda','asc')->orderBy('d.index','asc')->paginate(500);
                    return [
                        'code'=>200,
                        'data'=>$data->items(),
                        'last_page'=>$data->lastPage(),
                    ];
                }
            }
        }

        return [
            'code'=>200,
            'data'=>[],
            'last_page'=>1,
        ];

    }


    public function saved_chart($tahun,Request $request ){
        $uid=Auth::guard('api')->user()->id;
        $a=DB::table('analisa.saved_chart')->insert([
            'id_user'=>$uid,
            'type'=>$request->type,
            'id_dataset'=>$request->id_dataset,
            'name'=>$request->title,
            'data'=>json_encode($request->data)
        ]);

        if($a){
            return [
                'code'=>200
            ];
        }else{

            return [
                'code'=>500
            ];

        }


    }


   
    public function load($tahun,Request $request){
        $paginate=5;

        if($request->basis=='LONGLIST'){
            $paginate=40;
        }

        $table=[
            'ddub'=>[
                'multy'=>0,
                'table'=>'pemda_ddub',
                'tahun'=>$tahun,

                'select'=>"ddub_perencanaan,ddub_realisasi,ddub_gap"
            ],
            'pmpd'=>[
                'multy'=>0,
                'tahun'=>$tahun,
                'table'=>'pemda_pmpd',
                'select'=>"modal_pemda,modal_pemda_infra"
            ],
            'kerjasama'=>[
                'multy'=>1,
                'tahun'=>$tahun,

                'table'=>'pemda_kerjasama_non_public',
                'select'=>"string_agg(concat(tahun_mulai_kerjasama,'@|@',mitra_kerjasama,'@|@',bentuk_kegiatan_kerjasama,'@|@',nilai_kontrak_kerjasama),'||') as kerjasama"
            ],
            'pelatihan'=>[
                'multy'=>1,
                'tahun'=>$tahun,
                'table'=>'pemda_kebutuhan_pelatihan',
                'select'=>"string_agg(concat(tema_pelatihan,'@|@',jenis_pelatihan,'@|@',jumlah_peserta),'||') as pelatihan"
            ],
            'fcr'=>[
                'multy'=>0,
                'tahun'=>$tahun-1,
                'table'=>'data_kondisi_pdam',
                'select'=>"nilai_fcr"
            ]

        ];
        $pemda=DB::table('master.master_pemda as mp');
        $where=\_filter_l1($tahun,$request->filter,$request->base);
        unset($where[0]);
        $pemda=$pemda->where($where)->orderBy('mp.kodepemda')->paginate($paginate);
        $data=[];
        foreach($pemda as $k=>$p){
            $dum=[
                'kodepemda'=>(int)$p->kodepemda,
                'name'=>$p->nama_pemda,
                'tipe_bantuan'=>$p->tipe_bantuan,
                'tahun_proyek'=>$p->tahun_proyek,
                'regional_1'=>$p->regional_1,
                'regional_3'=>$p->regional_2,
            ];

            foreach($table as $key=>$tb){
                $dum[$key]=DB::table('dataset.'.$tb['table'])->where([
                    'tahun'=>$tb['tahun'],
                    'kodepemda'=>$p->kodepemda,
                    'status'=>1,
                    'tw'=>4
                ])->selectRaw($tb['select']);
                if($tb['multy']){
                    $dum[$key]=$dum[$key]->groupBy('kodepemda');
                }
                $dum[$key]=(Array)$dum[$key]->first();
                $dum[$key]=count($dum[$key])==0?null:$dum[$key];

            }

            if(isset($dum['kerjasama']['kerjasama'])){
                $v=explode('||',$dum['kerjasama']['kerjasama']);
                $d=['mitra'=>[]];
                foreach($v as $k=>$i){
                    $d['mitra'][$k]=explode('@|@',$i);
                    $d['mitra'][$k]=[
                        'tahun_kerjasama'=>$d['mitra'][$k][0],
                        'name'=>$d['mitra'][$k][1],
                        'bentuk_kegiatan'=>$d['mitra'][$k][2],
                        'nilai_kontrak'=>(float)$d['mitra'][$k][3],
                    ];
                }
                $dum['kerjasama']=$d;
            }

            if(isset($dum['pelatihan']['pelatihan'])){
                $v=explode('||',$dum['pelatihan']['pelatihan']);
                $d=['tema'=>[]];
                foreach($v as $k=>$i){
                    $d['tema'][$k]=explode('@|@',$i);
                    $d['tema'][$k]=[
                        'name'=>$d['tema'][$k][0],
                        'pelatihan'=>$d['tema'][$k][1],
                        'jumlah_peserta'=>(int)$d['tema'][$k][2],
                    ];
                }
                $dum['pelatihan']=$d;
            }
            

            $data[]=$dum;

        }

        return [
            'data'=>$data,
            'curent_page'=>$pemda->currentPage(),
            'last_page'=>$pemda->lastPage(),
            'total'=>$pemda->total(),
            'paginate_count'=>$paginate,
        ];

    }

    

    public function index($tahun,Request $request)
    {
        if(!$request->basis){
            $basis='LONGLIST';
        }else{
            $basis=$request->basis;
        }
        $page_meta=['title'=>'Monitoring dan Evaluasi','keterangan'=>''];
        
        $request->filter_lokus=true;
        return view('monevmodul::index')->with(['page_meta'=>$page_meta,'basis'=>$basis,'req'=>$request->all()]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('monevmodul::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('monevmodul::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('monevmodul::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
