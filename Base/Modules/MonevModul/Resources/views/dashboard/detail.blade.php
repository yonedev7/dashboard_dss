@extends('adminlte::page')


@section('content')
<script>
    var page_meta=<?=json_encode($page_meta)?>;
</script>
<style>
.wv-50{
    width:50vw;
}
    </style>
    <div id="app">
<div class="parallax pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
    <div class="container">
        <section >
            <h3><b>{{$page_meta['title']}} Tahun {{$StahunRoute}}</b></h3>
            <h4><b>{!!$page_meta['keterangan']!!}</b></h4>
        </section>
       <div class="btn-group">
        @php
        array_walk($kpi_list,function($el,$key) use ($page_meta,$kpi_list,$StahunRoute){
            if($page_meta['id']==$el->id){
                if($key!=0){
                    @endphp
                    <a href="{{route('mod.monev.detail',['tahun'=>$StahunRoute,'kpi'=>$kpi_list[$key-1]->id,'kodepemda'=>$page_meta['pemda']->kodepemda])}}" class="btn btn-primary btn-sm"><i class="fa fa-arrow-left"></i> {{$kpi_list[$key-1]->name}} </a>
                    @php

                }
                @endphp
                <button v-bind:class="'btn btn-sm '+(penilaian_kpi.terpenuhi?'btn-success':'btn-navy bg-dark')" > {{$page_meta['title']}} : @{{penilaian_kpi.terpenuhi?'Terpenuhi':'Tidak Terpenuhi'}}</button>
                <button v-if="!penilaian_kpi.terpenuhi" class="btn btn-warning btn-sm" @click="modalCatatan()" >Catatan</button>
                <button v-else class="btn btn-warning btn-sm" >@{{com_catatan_updated_at}}</button>

                @php

                if((count($kpi_list)-1)>$key ){
                    @endphp
                    <a  href="{{route('mod.monev.detail',['tahun'=>$StahunRoute,'kpi'=>$kpi_list[$key+1]->id,'kodepemda'=>$page_meta['pemda']->kodepemda])}}"  class="btn btn-primary btn-sm">{{$kpi_list[$key+1]->name}} <i class="fa fa-arrow-right"></i></a>
                    @php
                }
            }
        });
        @endphp
        </div>

      


      
    </div>
   
</div>
<div class="" >
    <div v-if="com_chart.length==0" class="text-center mt-4">Data Tidak Tersedia</div>
    <template v-for="row,i in com_chart">
        <div class="col-12 p-0  justify-content-center mb-2">
           <div class="d-flex row m-3">
            <template v-for="col,c in row">
                   <div class="flex-column col-6 bg-white rounded">
                    <charts :options="col.chart"></charts>
                    </div>
                </template>
            </div>
        </div>
    </template>
    <div :style="{'height':`${banding.height}px`}" v-if="banding.display">
    </div>
    <div v-bind:class="'spare-col frame-banding '+(banding.display?'enabled':'')"  :style="{'height':`${banding.height}px`}">
        <div class="d-flex btn-h-pos">
            <button class="btn btn-primary  "  @mousedown="getHeight(true)"  @mouseleave="getHeight(false)" @mouseup="getHeight(false)" ><i class="fa  fa-arrow-up" ></i></button>
            <input type="range" min="0" max="2" step="0.01" class="ml-2"  v-model="banding.zoom">
        </div>
        <button class="btn btn-default rounded-circle btn-close" @click="banding.display=false" ><i class="fa fa-times" ></i></button>
        <div class="loading-iframe text-center" v-if="banding.loading">
            <div class="spinner-border text-primary" role="status">
                <span class="sr-only">Loading...</span>
                </div>
                <div class="spinner-border text-secondary" role="status">
                <span class="sr-only">Loading...</span>
                </div>
            </div>
        <iframe  @load="onLoad"  v-bind:src="banding.frame"  :style="{'height':`${banding.height}px`}" frameborder="0" class="spare-col"></iframe>
    </div>
    
</div>
<div class="modal fade" id="modal-catatan" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Catatan Penilaian KPI</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p><b>Pembaruan : @{{com_catatan_updated_at}}</b></p>
                    <p>@{{penilaian_kpi.keterangan}}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                </div>
            </div>
        </div>
</div>
<style>
        .spare-col{
            // height:45vh;
        }
        .loading-iframe{
            width: 100%;
            position: absolute;
            bottom:30px;
            height: 100px;
        }

        .frame-banding{
            position: fixed;
            bottom:0;
            background: #f1f1f1;
            width: 100%;
            display: none;
            border-top:1px solid #000000;

        }
        .frame-banding.enabled{
            display: block;
        }

        .frame-banding iframe{
            overflow: scroll;
            width: 100%;
        }
        .frame-banding .btn-h-pos{
            position: absolute;
            left: 10px;
            right: auto;
            top:-25px;
            background: #b6b5b58d;
            padding:5px;
            
            border-radius:5px;


        }

        .frame-banding .btn-h-pos input[type="range"]{
           background:#ddd;
           width:300px;
        }
        .frame-banding .btn-close{
            position: absolute;
            right: 10px;
            background: #ddd;
            color:#000;
            top:-20px;
        }
        
    </style>
@stop

@section('js')
<script>
var timeout_btn_h=null;

var app=new Vue({
    el:'#app',
    data:{
        kpi:<?=json_encode($kpi)?>,
        penilaian_kpi:{
            terpenuhi:false,
            catatan:'',
            updated_at:null,
            keterangan:null,
        },
        charts:[],
        charts_:[],
        mouseX:0,
        windowheight:400,
        mouseY:0,
        banding:{
            display:false,
            frame:null,
            height:400,
            loading:true,
            zoom:1,
            channel:null,
        }
    },
    created:function(){
       var self=this;
       var pe=<?=json_encode($penilaian_kpi??[])?>;

       this.kpi.data.forEach(el=>{
            self.charts.push({
                chart:{
                    chart:{
                        type:'area'
                    },
                    title:{
                        text:el.dataset.name,  
                    },
                    subtitle:{
                        text:el.pemda.nama_pemda,  
                    },
                    plotOptions: {
                        area: {
                            dataLabels: {
                                enabled: true
                            },
                            enableMouseTracking: false
                        }
                    },
                    series:[]
                },
                dataset:el.dataset,
                pemda:el.pemda  
                
            });
       });

       self.charts.forEach((el,k)=>{
            req_ajax.post(("{{route('api-web-mod.dataset.public.data.series.pemda',['tahun'=>$StahunRoute,'id_dataset'=>'@?','kodepemda'=>'@?'])}}").yoneReplaceParam('@?',[el.dataset.id,el.pemda.kodepemda])).then(function(res){
                
                if(res.data.data[0]!=undefined){
                self.charts[k].chart['series']=res.data.data[0].series;

                }
            });
       });

       if(pe['keterangan']!=undefined){
          Object.assign(self.penilaian_kpi,pe);
       }
        

        document.onmousemove =(evt)=>{
            if (window.event){
                window.app.mouseX=window.event.x;
                window.app.mouseY=window.event.y;
            }
            else{
                window.app.mouseX.mouseX=evt.x;
                window.app.mouseY.mouseY=evt.y;
            }
        }

        this.banding.channel=new BroadcastChannel('sink_{{$fingerprint}}');

        // $('#btn-bangding-hight').mousedown(function(event){
           
        // }).bind('mouseup mouseleave', function() {
        //     if(timeout_btn_h){
        //         clearInterval(timeout_btn_h);
        //         console.log('leave');
        //     }
        // });;
    },
    watch:{
        "banding.display":function(val){
            if(val){
                var self=this;
                this.banding.height= (parseInt((window.innerHeight/100)*40))<400?parseInt((window.innerHeight/100)*45):400;
            }
        },
        'banding.zoom':function(val){
            this.banding.channel.postMessage({
                zoom:val
            });
        }
    },
    methods:{
        modalCatatan:function(){
            $('#modal-catatan').modal();
        },
        onLoad:function(){
            if(this.banding.frame){
                this.banding.loading=false;
            }
        },
        getHeight:function(create){
          if(create){
              var self=this;
            window.timeout_btn_h=setInterval((ref) => {
                    var h=window.innerHeight - ref.mouseY;
                    self.banding.height=h;
            }, 20,self);
          }else if(window.timeout_btn_h){
              clearInterval(window.timeout_btn_h);
          }
        },
        buildFrame:function(dataset){
            var url=('{{route('mod.monev-admin.form.sink',['tahun'=>$StahunRoute,'id_monev_data'=>2,'id_dataset'=>'xxxx','no_header'=>true,'fingerprint'=>$fingerprint])}}').replace('xxxx',dataset.id);
            if(this.banding.frame!=url){
                this.banding.zoom=1;
                this.banding.loading=true;
                this.banding.frame=url;
            }
            this.banding.display=true;
        },
        tooltip:function(ref,tooltip){
            console.log(ref);
        },
        chuck:function(array,size){
            var chunked=[];
            Array.from({length: Math.ceil(array.length / size)}, (val, i) => {
            chunked.push(array.slice(i * size, i * size + size))
            });
            return chunked;
        }
    },
    computed:{
        com_catatan_updated_at:function(){
            if(this.penilaian_kpi.updated_at){
            var time= moment(this.penilaian_kpi.updated_at);

            }else{
                var time=moment();
            }
            console.log(time,this.penilaian_kpi.updated_at||null);
            return time.format('LL');
        },
        com_chart:function(){
            var ch=this.charts.filter(e=>{
                return e.chart.series.length;
            });
            ch=this.chuck(ch,2);
            return ch;
        }
    }
});

</script>
@stop