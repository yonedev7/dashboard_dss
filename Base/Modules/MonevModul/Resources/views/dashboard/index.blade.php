@extends('adminlte::page')


@section('content')
<div class="container">
    <table class="table table-bordered mt-2">
        <thead class="thead-dark">
            <tr>
                <th>KPI</th>
                <th>INDIKATOR</th>
                <th>TARGET</th>
                <th>CAPAIAN</th>
                <th>CAPAIAN PERSENTASE</th>
            </tr>
        </thead>
        <tr>
            <td>I</td>
            <td>Pemda menigkat dukungan finansial kepada PDAM melalui peningkatan kontribusi modalnya, pemberian hibah, atau penyediaan jaminan bagi PDAM untuk mendapatkan pembiayaan non publik</td>
            <td>40 Daerah</td>
            <td>33 Daerah</td>
            <td>83%</td>
        </tr>
        <tr>
            <td>II</td>
            <td>Pemda/PDAM mendapatkan bantuan program pendampingan sebagai hasil utilisasi pembiyaan non public untuk pembangunan infrastruktur</td>
            <td>20 Daerah</td>
            <td>8 Daerah</td>
            <td>40%</td>
        </tr>
        <tr>
            <td>III</td>
            <td>Jumlah PEMDA DAN PDAM Berpartisipasi aktif dalam program pelatihan dan peningkatan kapasitas</td>
            <td>200 Daerah</td>
            <td>203 Daerah</td>
            <td>102%</td>
        </tr>
        <tr>
            <td>IV</td>
            <td>PDAM telah mencapai FULL COST RECOVERY</td>
            <td>50 Daerah</td>
            <td>78 Daerah</td>
            <td>156%</td>
        </tr>
    </table>
</div>
@stop