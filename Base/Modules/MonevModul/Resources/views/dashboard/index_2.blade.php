@extends('adminlte::page')


@section('content')
<script>
    var page_meta=<?=json_encode($page_meta)?>;
</script>
<style>
.bg-white-dirty{
    background: #eeeded;
}    
</style>
<div id="app" class="">
    <div class="p-3">
        <div class="d-flex">
            <div class="flex-column p-2" v-for="item,key in kpi_menu">
                <h4><b>@{{item.name}}</b></h4> 
                <small class="text-wight-bold"> @{{item.keterangan}}</small> 
                <p><a v-bind:href="('{{route('mod.monev.index',['tahun'=>$StahunRoute,'kpi'=>'xxxx'])}}').replace('xxxx',item.id)" class="btn btn-sm btn-success"><i class="fa fa-arrow-right"></i> Detail</a></p>
            </div>
        </div>
    </div>
    <div class="col-12 bg-dark" style="height:5px;"></div>
    <div class="parallax pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
    <div class="container">
        <section >
            <h3><b>{{$page_meta['title']}}</b></h3>
            <p><b>{!!$page_meta['keterangan']!!}</b></p>
        </section>
      
    </div>
   
</div>


        
    <div class="card">
        
        <div class="card-body">
        <form action="{{url()->current()}}" method="get" id="form-filter">

            <div class="row">
                <div class="col-3">
                    <div class="form-group">
                        <label for="">Regional </label>
                        <input type="hidden" name="kpi" value="{{$indexKpi}}">
                        <input type="hidden" name="meta" value="{{isset($_GET['meta'])?$_GET['meta']:null}}">

                        <select  v-model="filter.filter_regional"  name="filter_filter_regional" class="form-control" id="">
                            <option value=""></option>
                            <option value="I">I</option>
                            <option value="II">II</option>
                            <option value="III">III</option>
                            <option value="IV">IV</option>
                            <option value="V">V</option>

                        </select>
                    </div>
                </div>
                
                <div class="col-3">
                    <div class="form-group">
                        <label for="">Tahun Proyek</label>
                        <select  v-model="filter.filter_sortlist"  name="filter_sortlist" class="form-control" id="">
                            <option value=""></option>
                            <option value="{{$StahunRoute}}">{{$StahunRoute}}</option>
                            <option value="{{$StahunRoute-1}}">{{$StahunRoute-1}}</option>

                            
                        </select>
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label for="">Tipe Bantuan</label>
                        <select  v-model="filter.filter_tipe_bantuan" name="filter_tipe_bantuan" class="form-control" id="">
                            <option value=""></option>
                            <option value="STIMULAN">STIMULAN</option>
                            <option value="PENDAMPING">PENDAMPING</option>
                            <option value="BASIS KINERJA">BASIS KINERJA</option>
                        </select>
                    </div>
                </div>
            </div>
        </form>
        <hr>
        <div class="row">
    <div class="col-3" v-for="item,i in charts">
        <charts :options="item"></charts>
        
    </div>
</div>

        </div>
        <div class="card-footer">
            <strong>Jumlah PEMDA : @{{jumlah_pemda}} / {{$pemda_scope}} PEMDA </strong></div>
        </div>
    <div class="container-fluid">
      <div class="table-responsive">
        <table v-for="(b,i) in com_pemda_list" class="table table-bordered">
            <thead class="thead-dark">
                <tr>

                <template v-for="raw,r in b">
                        <th rowspan="2">
                        DETAIL
                        </th>
                        <th colspan="4">
                            PEMENUHAN
                        </th>
                        <th rowspan="2">
                            REGIONAL
                        </th>
                        <th rowspan="2">
                            WILAYAH
                        </th>
                        <th rowspan="2">
                            KODEPEMDA
                        </th>
                        <th rowspan="2">
                            NAMA PEMDA
                        </th>
                        <th rowspan="2">
                            TIPE BANTUAN
                        </th>    
                    
                   
                </template>
            </tr>
            <tr>

                <template v-for="raw,r in b">
                        <th v-for="kpi,kkpi in kpi_menu">@{{kpi.name}}</th>
                        
                </template>
            </tr>   

            </thead>
            <tbody v-if="b[0]!=undefined">
                <tr v-for="raw,r in b[0]">
                    <template  v-for=" d,di in Array.from(Array(b.length).keys())">
                        <td  v-bind:class="di==0?'':'bg-white-dirty'">
                        <template v-if="b[di]!=undefined">
                        <template v-if="b[di][r]!=undefined">
                            <a v-bind:href="('{{route('mod.monev.detail',['tahun'=>$StahunRoute,'kpi'=>$indexKpi,'kodepemda'=>'xxxx'])}}').replace('xxxx',b[di][r].kodepemda)" class="btn btn-success btn-sm"><i class="fa fa-arrow-right"></i></a>
                            </template>
                        </template>
                    </td>
                 
                    <td  v-bind:class="di==0?'':'bg-white-dirty'" v-for="kpi,kkpi in kpi_menu">
                        <template v-if="b[di]!=undefined">
                            <template v-if="b[di][r]!=undefined">
                                <i class="fa fa-check text-success" v-if="b[di][r]['pem_kpi_'+kpi.id]"></i>
                                <i class="fa fa-times text-danger" v-if="!b[di][r]['pem_kpi_'+kpi.id]"></i>

                            </template>
                        </template>
                    </td>
                   
                    <td  v-bind:class="di==0?'':'bg-white-dirty'">
                        <template v-if="b[di]!=undefined">
                        <template v-if="b[di][r]!=undefined">
                            @{{b[di][r].filter_regional_1}}
                            </template>
                        </template>
                    </td>
                    <td  v-bind:class="di==0?'':'bg-white-dirty'">
                        <template v-if="b[di]!=undefined">
                        <template v-if="b[di][r]!=undefined">
                            @{{b[di][r].filter_regional_2}}
                            </template>
                        </template>
                    </td>
                    <td  v-bind:class="di==0?'':'bg-white-dirty'">
                        <template v-if="b[di]!=undefined">
                        <template v-if="b[di][r]!=undefined">
                            @{{b[di][r].kodepemda}}
                            </template>
                        </template>
                    </td>
                    <td  v-bind:class="di==0?'':'bg-white-dirty'">
                        <template v-if="b[di]!=undefined">
                        <template v-if="b[di][r]!=undefined">
                            @{{b[di][r].nama_pemda}}
                            </template>
                        </template>
                    </td>    
                    <td  v-bind:class="di==0?'':'bg-white-dirty'">
                        <template v-if="b[di]!=undefined">
                        <template v-if="b[di][r]!=undefined">
                            @{{(b[di][r].lokus_bantuan||'').replace(/@/g,', ').replace(/- ,/g,'')}}
                            </template>
                        </template>
                    </td>  
                    
                    </template>
                    
                    
                </tr>
            </tbody>
        </table>    
    </div>
    </div>
</div>

@stop

@section('js')
<script>
var app=new Vue({
    el:'#app',
    data:{
        pemda_list:<?=json_encode($pemda_list)?>,
        charts:[],
        kpi_menu:<?=json_encode($kpi_list)?>,
        index_selected:{{$indexKpi}},
        jumlah_pemda:0,

        filter:{
            filter_sortlist:"{{$req['filter_sortlist']??''}}",
            filter_tipe_bantuan:"{{$req['filter_tipe_bantuan']??''}}",
            filter_regional:"{{$req['filter_regional']??''}}",
        }

    },
    computed:{
        com_pemda_list:function(){
            var pemdas=this.pemda_list;
            // return pemdas;
            // if(this.filter.filter_sortlist){
            //     pemdas=pemdas.filter(el=>{
            //         return (el.lokus_bantuan.includes('- '+this.filter.filter_sortlist));
            //     });
            // }
            // if(this.filter.filter_tipe_bantuan){
            //     pemdas=pemdas.filter(el=>{
            //         return (el.lokus_bantuan.includes(this.filter.filter_tipe_bantuan));
            //     });
            // }
            // if(this.filter.filter_regional){
            //     pemdas=pemdas.filter(el=>{
            //         return (el.filter_regional_1==(this.filter.filter_regional));
            //     });
            // }

            if(pemdas.length){
                this.jumPem(pemdas.length);
                var l=(pemdas.length/3);
                if(l%(parseInt(l))!=0){
                    l=l+1;
                }
                pemdas=this.chuck(pemdas,l);
                pemdas=this.chuck(pemdas,2);
                return pemdas;
            }else{
                this.jumPem(0);


                return [];
            }


        }
    },
    watch:{
        filter:{
            deep:true,
            handler:function(){
                $('#form-filter').submit();
            }
        }
    },
    created:function(){
        this.getKpiPenilaian();
    },
    methods:{
        jumPem:function(j){
            this.jumlah_pemda=j;
        },
        getKpiPenilaian:function(){
            var self=this;
            var charts={};
            this.kpi_menu.map(el=>{
                if(charts[el.id]==undefined){
                    charts[el.id]={
                        id:el.id,
                        title:{
                            text:'Pemenuhan '+el.name
                        },
                        plotOptions:{
                            line:{
                                dataLabels:{
                                    enabled:true,
                                    format:" {y:,0f} PEMDA"
                                }
                            }
                        },
                        subtitle:{
                            text:'Tahun {{$StahunRoute}}'
                        },
                        chart:{
                            type:'line',
                            height:250,
                        },
                        xAxis:{
                            type:'category'
                        },
                        yAxis:{
                            title:{
                                text:'PEMDA'
                            }
                        },
                        series:[
                            {
                                name:'Terpenuhi',
                                data:[]
                            },
                            {
                                name:'Tidak Terpenuhi',
                                data:[]
                            },
                            
                        ]
                    }
                }
                req_ajax.post(('{{route('api-web-mod.monev.penilaian_kpi',['tahun'=>$StahunRoute,'id_kpi'=>'xxxx'])}}').replace('xxxx',el.id),this.filter).then((res)=>{
                   

                   var pemenuhan={
                            'terpenuhi':{},
                            'tidak_terpenuhi':{}

                   };
                   res.data.forEach(item=>{
                      if(item.terpenuhi){
                        if(pemenuhan['terpenuhi']==undefined){
                            pemenuhan['terpenuhi']={};
                        }
                        console.log(pemenuhan);
                        if(pemenuhan['terpenuhi'][item.tahun]==undefined){
                            pemenuhan['terpenuhi'][item.tahun]={
                                name:item.tahun,
                                y:0
                            };
                        }
                        pemenuhan['terpenuhi'][item.tahun]['y']+=1;

                      }else{

                        if(pemenuhan['tidak_terpenuhi']==undefined){
                            pemenuhan['tidak_terpenuhi']={};
                        }
                        if(pemenuhan['tidak_terpenuhi'][item.tahun]==undefined){
                            pemenuhan['tidak_terpenuhi'][item.tahun]={
                                name:item.tahun,
                                y:0
                            };
                        }
                        pemenuhan['tidak_terpenuhi'][item.tahun]['y']+=1;

                      }
                   });

                   charts[el.id]['series'][0]['data']=Object.values(pemenuhan['terpenuhi']);
                   charts[el.id]['series'][1]['data']=Object.values(pemenuhan['tidak_terpenuhi']);

                   self.charts.push(charts[el.id]);
                   self.charts.sort(function(a, b){return a.id - b.id});
                //    console.log('chart ter',self.charts);
                });
            });
           

        },
        chuck:function(array,size){
            var chunked=[];
            Array.from({length: Math.ceil(array.length / size)}, (val, i) => {
            chunked.push(array.slice(i * size, i * size + size))
            });
            return chunked;
        }
    }
})    
</script>

@stop