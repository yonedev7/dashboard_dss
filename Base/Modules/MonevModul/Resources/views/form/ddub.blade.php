@extends('adminlte::page')


@section('content')
<script>
    var page_meta=<?=json_encode($page_meta)?>;
</script>
<div class="parallax pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
    <div class="container">
        <section >
            <h3><b>{{$page_meta['title']}} </b></h3>
            <h5><b>DATASET Tahap {{$page_meta['tw']}} Tahun {{$StahunRoute}}</b></h5>
            <p><b>{!!$page_meta['keterangan']!!}</b></p>
           
        </section>
      
    </div>
   
</div>
@if(isset($page_meta['monev']))
    @if(!isset($page_meta['pemda']))
<!--        @include('data.partials.filter_basis_monev',['basis'=>$basis])-->
        @include('data.partials.filter_jenis_daerah',['init'=>[
            'component'=>[
                'id'=>'filter_bispan',
                'name'=>$page_meta['title'],
                'target_react'=>['table_input'],
                'basis'=>$basis,
            ],
        
        ]])
    @endif
@endif


@include('data.admin_partials.table_input',['init'=>[
    'component'=>[
        'id'=>'table_input',
        'name'=>$page_meta['title'],
        'target_react'=>['l1'],
        'basis'=>$basis,
        'pemda_list'=>isset($page_meta['pemda_list'])?$page_meta['pemda_list']:[],
        'id_dataset'=>$page_meta['id'],
        'tw'=>$page_meta['tw'],
        'tahun'=>$StahunRoute,
        'monev'=>$page_meta['monev'],
        'def_kodepemda'=>isset($page_meta['pemda'])?$page_meta['pemda']['kodepemda']:null,
        'def_name'=>isset($page_meta['pemda'])?$page_meta['pemda']['nama_pemda']:null,
        'def_regional_1'=>isset($page_meta['pemda'])?$page_meta['pemda']['regional_1']:null,
        'def_regional_2'=>isset($page_meta['pemda'])?$page_meta['pemda']['regional_2']:null,
        'def_tahun_proyek'=>isset($page_meta['pemda'])?$page_meta['pemda']['tahun_proyek']:0,
        'def_tipe_bantuan'=>isset($page_meta['pemda'])?$page_meta['pemda']['tipe_bantuan']:null,

    ],
    'map_table'=>[
        'submit_url'=>route('mod.monev-admin.form.update_ddub',['tahun'=>$StahunRoute,'id_dataset'=>$page_meta['id'],'tw'=>$page_meta['tw']]),
        'ajax_data'=>route('api-web-mod.monev.load_ddub',['tahun'=>$StahunRoute,'id_dataset'=>$page_meta['id'],'tw'=>$page_meta['tw']]),
        'columns'=>$columns
        
    ]
   
]])

@stop