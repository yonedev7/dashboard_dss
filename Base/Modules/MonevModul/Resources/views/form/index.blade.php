@extends('adminlte::page')

@section('content')
<div class="parallax pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
    <div class="container">
        <section >
            <h3><b>Monitoring Dan Evaluasi</b></h3>
            <p><b></b></p>
        
        </section>
    </div>
</div>
<div id="app">

   <div class="p-3">
        <div v-for="kpi,k in data">
    <div class="row">
            <h4 class="col-12">@{{kpi.name}}</h4>
            <div class="col-4" v-for="item,i in kpi.data">
                <div class="small-box bg-success">
                    <div class="inner">
                        <h3>@{{item.kpi_name}}</h3>
                            <p class="text-uppercase">@{{item.dataset.name}} Tahun {{$StahunRoute}}</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="#" @click="detail(item)" class="small-box-footer">Detail <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
            </div>
        </div>
        <hr>

        
    </div>
   </div>
</div>
@stop

@section('js')
<script>
    var app=new Vue({
        el:'#app',
        data:{
            data:<?=json_encode($data)?>,
        },
        methods:{
            detail:function(item){
                window.open(('{{route('mod.dataset.form.listing',['tahun'=>$StahunRoute,'id_dataset'=>'@?'])}}').yoneReplaceParam('@?',[item.dataset.id]),'monev');
            }
        }
    })
</script>
@stop