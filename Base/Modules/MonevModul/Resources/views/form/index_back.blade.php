@extends('adminlte::page')


@section('content')
    <script>
        var page_meta=<?=json_encode($page_meta)?>;
    </script>
    <div class="parallax pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
        <div class="container">
            <section >
                <h3><b>{{$page_meta['title']}}</b></h3>
                <p><b>{!!$page_meta['keterangan']!!}</b></p>
            
            </section>
        </div>
    </div>
    <div class="container">
        @include('sistem_informasi.partials.filter',['req'=>$req])    
    </div>

    <div class="" id="app">
        <div class="container">
          
        </div>
                        
        <div  class="table-responsive">
            <table class="table table-bordered table-striped" id="table-show">
                <thead class="thead-dark">
                    <th>Aksi</th>
                    <th>Nama</th>
                    <th>Jumlah Data</th>
                    <th>Jumlah Data Terverifikasi</th>
                </thead>
                <tbody>
                    <tr v-for="item,key in data">
                        <td >
                            <div class="btn-group">
                            <a href="javascript:void(0)" v-if="item.enabled_sinkron" @click="directSinkron(item)" class="btn btn-info btn-sm">Sinkronisasi</a>
                            <a v-bind:href="('{{route('mod.monev-admin.form.edit_data',['tahun'=>$StahunRoute,'id_monev_data'=>'xxxx'])}}').replace('xxxx',item.id)" class="btn btn-success btn-sm">From Edit</a>
                        </div>
                        </td>
                        <td>@{{item.name}}</td>
                        <td>@{{item.data_report.terdata}} PEMDA</td>
                        <td>@{{item.data_report.terverifikasi}} PEMDA</td>

                    </tr>    
                </tbody>
            </table>
        </div>
        
    </div>

@stop


@section('js')
@php
@endphp
<script>
    var app=new Vue({
    el:'#app',
    watch:{
        lokus_nuwas:(val)=>{
           setTimeout(() => {
            $('#ff').submit();
           }, 300);
        }
    },
    methods:{
        directSinkron:function(item){
            var ids=item.enabled_sinkron.split('@');
            console.log(ids);
            ids.forEach((el,key) => {
                var url='{{route('mod.monev-admin.form.sink',['tahun'=>$StahunRoute,'id_monev_data'=>'xxxx','id_dataset'=>'yyyy'])}}';
                url=url.replace('xxxx',item.id).replace('yyyy',el);
                setTimeout(function(url){
                    window.open(url,'_blank');
                    console.log(url);
                },(100*key||5),url)
            });
        }
    },
    data:{
        lokus_nuwas:<?=isset($req['lokus'])?((bool)$req['lokus']?'true':'false'):'false'?>,
        data:<?=json_encode($data)?>
    }
    });    

</script>




@stop