@extends('adminlte::page')


@section('content')
<script>
    var page_meta=<?=json_encode($page_meta)?>;
</script>
<div class="parallax pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
    <div class="container">
        <section >
            <h3><b>{{$page_meta['title']}}</b></h3>
            <p><b>{!!$page_meta['keterangan']!!}</b></p>
           
        </section>
      
    </div>
   
</div>
@include('data.partials.filter_basis_monev',['basis'=>$basis])
@include('data.partials.filter_jenis_daerah',['init'=>[
    'component'=>[
        'id'=>'filter_bispan',
        'name'=>$page_meta['title'],
        'target_react'=>['l1'],
        'basis'=>$basis,
    ],
   
]])

@include('data.admin_partials.table_list_tw_monev',[
    'init'=>[
        'component'=>[
            'name'=>$page_meta['title'],
            'id'=>'table_list',
            'basis'=>$basis,
            'nasional'=>$page_meta['nasional']??false
        ],
        'map_table'=>[
            'monev'=>true,
            'ajax_data'=>route('api-web-mod.dataset.load',['tahun'=>$StahunRoute,'monev'=>$StahunRoute])
        ]
    ]
]);

@stop