@extends('adminlte::page')


@section('content')
<script>
    var page_meta=<?=json_encode($page_meta)?>;
</script>
<div class="parallax pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
    <div class="container">
        <section >
            <h3><b>{{$page_meta['title']}}</b></h3>
            <h3><b>Tahun {{$StahunRoute}}</b></h3>
            <p><b>{!!$page_meta['keterangan']!!}</b></p>
           
        </section>
      
    </div>
   
</div>
<div class="container pt-2">
    @include('sistem_informasi.partials.filter',['req'=>$req])
</div>
<div class="table-responsive">
    <table class="table table-bordered">
        <thead class="thead-dark">
            <tr>
                <th>AKSI</th>
                <th>REGIONAL</th>
                <th>WILAYAH</th>
                <th>TIPE BANTUAN</th>
                <th>KODE PEMDA</th>
                <th>NAMA PEMDA</th>
                <th>JUMLAH DATA</th>
                <th>JUMLAH DATA TERVERIFIKASI</th>
                </tr>
            </thead>
            <tbody>
                @foreach($pemdas as $k=>$d)
                <tr>
                    <td>
                        <a class="btn btn-success btn-sm" href="{{route('mod.monev-admin.form.edit_data',['tahun'=>$StahunRoute,'id_monev_data'=>$id_monev_data,'kodepemda'=>$d->kodepemda])}}">Form Input</a>
                    </td>
                    <td>{{$d->regional_1}}</td>
                    <td>{{$d->regional_2}}</td>
                    <td>{{$d->tipe_bantuan}}</td>
                    <td>{{$d->kodepemda}}</td>
                    <td>{{$d->nama_pemda}}</td>
                    <td>{{number_format($d->terdata??0,0,'.',',')}}</td>
                    <td>{{number_format($d->terverifikasi??0,0,'.',',')}}</td>
                </tr>    
                @endforeach
            </tbody>
        </table>    
</div>


@stop

