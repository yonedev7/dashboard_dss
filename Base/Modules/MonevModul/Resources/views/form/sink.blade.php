@extends('adminlte::page_sink',['title_page'=>$dataset->name])
@section('content')
@include('monevmodul::form.sink_clone_min_1_tahun',['dataset'=>$dataset,'tahun'=>$StahunRoute])
@include('monevmodul::form.sink_clone_plus_1_tahun',['dataset'=>$dataset,'tahun'=>$StahunRoute])
<div class=" " id="sink_saved">
    @if(!isset($_GET['no_header']))

    <div class="d-flex scoll-y bg-dark p-1">
        <a href="{{\Request::getRequestUri()}}" target="_blank" class="btn btn-danger btn-sm mr-2">New Tab</a>
        <template v-for="item,i in data" >
            <button class="btn btn-primary mr-2 btn btn-sm" v-if="item.type=='default'" @click="proccesDefault(item)" >@{{evalDef(item.name)}}</button>
            <button class="btn btn-warning mr-2 btn btn-sm" v-else-if="item.type=='static'" @click="show(item)" >@{{item.name}}</button>
            <button class="btn btn-warning mr-2 btn btn-sm" v-else="item.type=='dinamic'" @click="show(item)" >@{{item.name}}</button>
    
        </template>
    </div>

        <div class="parallax pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
            <div class="container">
                <section>
                    <h3><b>{{$dataset->name}} - @{{tahun}}</b></h3>
                    <p><b>{!!$dataset->definisi_konsep!!}</b></p>
                </section>
            </div>
        </div>
        @endif
</div>

<div class="" id="app">
    <div class="d-flex flex-column mt-2 mb-2 p-2">
        <div class="d-flex scroll-x col-12 p-0">
            <div class="form-group mr-2 w-150">
                <label for="">Chart</label>
                <select name="" class="form-control" id="" v-model="chart.chart.type">
                    <option value="bar">BAR</option>
                    <option value="column">COLUMN</option>
                    <option value="pie">PIE</option>
                    <option value="scatter">SCATTER</option>
                </select>
            </div>
            <div class="form-group mr-2 w-150" v-if="!(['scatter','pie']).includes(chart.chart.type)">
                <label for="">Frame Limit</label>
                <input type="number" class="form-control" v-model="chart_schema.max">
            </div>


            <div class="form-group mr-2 w-200">
                <label for="">@{{chart.chart.type=='scatter'?'Label I':'Xaxis I'}}</label>
                <select name="" class="form-control" id="" v-model="chart_schema.XaxisI">
                    <option value=""></option>
                    {{-- <option :value="c.field_name"  v-for="c,i in columns" v-if="!(chart.chart.type=='scatter')" class="text-uppercase">@{{c.name.toUpperCase()}}</option> --}}
                    <option :value="c.field_name"  v-for="c,i in columns" class="text-uppercase">@{{c.name.toUpperCase()}}</option>
                </select>
            </div>
            <div class="form-group mr-2 w-200"  >
                <label for="">@{{chart.chart.type=='scatter'?'Label II':'Xaxis II'}}</label>
                <select name="" class="form-control" id="" v-model="chart_schema.XaxisII">
                    <option value=""></option>
                    <option :value="c.field_name"  v-for="c,i in columns" class="text-uppercase">@{{c.name.toUpperCase()}}</option>
                </select>
            </div>
            <div class="form-group mr-2 w-200"  v-if="chart.chart.type=='scatter'" >
                <label for="">Xaxis</label>
                <select name="" class="form-control" id="" v-model="chart_schema.XaxisIII">
                    <option value=""></option>
                    <option :value="c.field_name" v-for="c,i in columns"  v-if="(chart.chart.type=='scatter') && (c.tipe_nilai=='numeric') && ((c.classification)?((c.options=='[]'||c.options==null)?true:false):true)" class="text-uppercase">@{{c.name.toUpperCase()}}</option>
                </select>
            </div>

            <div class="form-group mr-2 w-200" >
                <label for="">Yaxis I</label>
                <select name="" class="form-control" id="" v-model="chart_schema.Yaxis">
                    <option value=""></option>
                    <option :value="c.field_name" v-for="c,i in columns"  v-if="(c.tipe_nilai=='numeric') && ((c.classification)?((c.options=='[]'||c.options==null)?true:false):true)" class="text-uppercase">@{{c.name.toUpperCase()}}</option>
                </select>
            </div>
            <div class="form-group mr-2 w-200" v-if="!(['pie']).includes(chart.chart.type)">
                <label for="">Yaxis II</label>
                <select name="" class="form-control" id="" v-model="chart_schema.YaxisII">
                    <option value=""></option>
                    <option :value="c.field_name" v-for="c,i in columns"  v-if="(c.tipe_nilai=='numeric') && ((c.classification)?((c.options=='[]'||c.options==null)?true:false):true)" class="text-uppercase">@{{c.name.toUpperCase()}}</option>
                </select>
            </div>
            <div class="form-group mr-2 w-200"  v-if="!(['pie']).includes(chart.chart.type)">
                <label for="">Yaxis III</label>
                <select name="" class="form-control" id="" v-model="chart_schema.YaxisIII">
                    <option value=""></option>
                    <option :value="c.field_name" v-for="c,i in columns"  v-if="(c.tipe_nilai=='numeric') && ((c.classification)?((c.options=='[]'||c.options==null)?true:false):true)" class="text-uppercase">@{{c.name.toUpperCase()}}</option>
                </select>
            </div>
            
        </div>
    </div>
    <div class="px-3">
        
        <div class="">
            <charts  :options="chart" class="mb-3"  :constructor-type="'mapChart'" v-if="com_chart_schema"></charts>
           
            
        </div>
    </div>
    <div class="d-flex mt-2 mb-2 p-2 pt-0">
        
        <div class="from-group mr-2 w-200 bg-dark rounded p-2">
            <label for="">Table</label>
            <p><strong>@{{data_com.length}} Data</strong></p>
        </div>
        <div class="from-group mr-2 w-200 bg-dark rounded p-2" v-if="com_chart_schema">
            <label for="">Chart</label>
            <p><strong>@{{chart.series[0].data.length}} Data</strong></p>
        </div>
        <div class="from-group mr-2 w-150" >
            <label for="">Tahun</label>
            <select name="" class="form-control" v-if="!load_data.proccess"  id="" v-model="chart_schema.tahun" >
                <option :value="it" v-for="it,th in com_tahun">@{{it}}</option>
            </select>
        </div>
       <div class="d-flex align-self-center mr-2" v-if="com_chart_schema">
            <div class="btn-group h-50 align-middle">
                <button class="btn-success btn btn-sm" @click="saveStaticChart">Save Static Chart</button>
                <button class="btn-primary btn btn-sm " @click="saveSchemaChart">Save Schema Chart</button>
            </div>
       </div>
        <div class="from-group mr-2 w-200" v-if="com_chart_schema">
            <label for="">Sorted By</label>
            <select name="" class="form-control" id="" v-model="chart_schema.sortedBy">
                <option :value="f" v-for="f,k in sortedlist">@{{columns_like[f].name}}</option>
            </select>
        </div>
        <div class="form-group mr-2 w-200" v-if="com_chart_schema && chart_schema.sortedBy">
            <label for="">Sort</label>
            <select name="" class="form-control" id="" style="width:200px"  v-model="chart_schema.sort">
                <option value=""></option>
                <option value="min">MAX</option>
                <option value="max">MIN</option>
            </select>

        </div>
    </div>
    <div class="form-group mb-2" v-if="chart_schema.subtitle">
        <input type="text" class="form-control" v-model="chart_schema.subtitle">
    </div>
    <div class="table-responsive">
        
        <table class="table table-bordered table-striped">
            <thead class="thead-dark">
                <tr>
                    <th v-for="c,i in columns"  v-bind:class="'text-uppercase w-'+c.minwidth">
                        @{{c.name}}
                    </th>
                </tr>
                <tr>
                 
                    <th v-for="c,i in columns" v-bind:class="'w-'+c.minwidth">
                        <template v-if="(c.tipe_nilai=='numeric') && ((c.classification)?((c.options=='[]'||c.options==null)?true:false):true)">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <select class="custom-select" style="width:70px" v-model="columns_like[c.field_name]['op']" >
                                        <option value="like" selected>like<option>
                                        <option value="not like">not like<option>
                                        <option value="like prefix">like prefix<option>
                                        <option value="=" >=<option>
                                        <option value=">">><option>
                                        <option value="<"><<option>
                                      </select>
                                </div>
                                <input type="number" step="0.01" class="form-control" v-model="columns_like[c.field_name]['val']">
                              </div>
                        </template>
                        <template v-if="(['string','text']).includes(c.tipe_nilai)">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <select class="custom-select" style="width:70px" v-model="columns_like[c.field_name]['op']" >
                                        <option selected>like<option>
                                        <option value="like prefix">like prefix<option>
                                        <option value="not like">not like<option>


                                      </select>
                                </div>
                                <input type="text" class="form-control" v-model="columns_like[c.field_name]['val']">
                              </div>
                        </template>    
                    </th>

                </tr>
                <tr>
                    <th v-for="c,i in columns"   v-bind:class="'text-uppercase w-'+c.minwidth" >
                        @{{i+1}}
                    </th>
                </tr>
            </thead>
            <tbody >
                <tr v-for="item,i in data_com">
                
                    <td v-for="c,ci in columns">
                        <template v-if="(c.tipe_nilai=='numeric')" >
                        <template  v-if="(c.bind_view).length">
                            @{{bindView(item[c.field_name],c.bind_view)}} 
                        </template>
                        <template  v-else-if="(c.options).length">
                            @{{bindOptions(item[c.field_name],c.options)}} 
                        </template>
                        <template v-else>
                            @{{formatAngka(item[c.field_name])}} 
                        </template>
                            
                            @{{((c.satuan)?((c.satuan!='-')?"("+c.satuan+')':''):'')}}
                        </template>
                        <template v-if="(['string','text']).includes(c.tipe_nilai)">
                            <template v-if="(c.bind_view).length">
                                @{{bindView(item[c.field_name],c.bind_view)}} 
                            </template>
                            <template v-else-if="(c.options).length">
                                @{{bindOptions(item[c.field_name],c.options)}} 
                            </template>
                            <template v-else>
                                @{{c.field_name=='kodepemda'?parseInt(item[c.field_name]):item[c.field_name]}}
                            </template>

                           
                        </template>
                    </td>
                </tr>
                <tr>
                    <td v-bind:colspan="columns.length" v-if="data_com.length==0">
                        <p class="text-center">Data Tidak Tersedia</p>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>  
    <div id="modal-save" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title">  Save @{{form_save.type=='static'?'Static':'Schema'}} Chart</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" class="form-control" v-model="form_save.title">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" v-if="!form_save.load" class="btn btn-primary" @click="sendToServer">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
            
        </div>
    </div>  
</div>
<style>
    .w-300{
        min-width: 300px;

    }
    .highcharts-data-table{
        display: none!important;
    }
    .h-50{
        height: 50px;
    }
    .scroll-x{
        overflow-x: scroll;
    }
    .w-250{
        min-width: 250px;

    }
    .w-200{
        min-width: 200px;

    }
    .w-150{
        min-width: 150px;
    }
</style>
@stop

@section('js')
<script>
   
    @if(isset($_GET['fingerprint']))
    var channel= new BroadcastChannel('sink_{{$_GET['fingerprint']}}');
    channel.onmessage = function(e) {
       if(e.data['zoom']!=undefined){
       
        // $('body').css('zoom',(parseFloat(e.data['zoom'])*100)+'%'); /* Webkit browsers */
        $('body').css('zoom',e.data['zoom']); /* Other non-webkit browsers */
        $('body').css('-moz-transform','scale('+e.data['zoom']+', 1)'); /* Moz-browsers */
        
       }
    };
    @endif

</script>

<script>
    Vue.prototype.Object = window.Object;
    var app=new Vue({
        el:'#app',
        data:{
           
            id:{{$dataset->id}},
            data:[],
            columns:<?=json_encode($columns)?>,
            columns_like:<?=json_encode($columns_like)?>,
            colors:[
                "#7cb5ec",
                "#f7a35c",
                "#90ee7e",
                "#7798BF",
                "#aaeeee",
                "#ff0066",
                "#eeaaee",
                "#55BF3B",
                "#DF5353",
                "#7798BF",
                "#aaeeee"
            ],
            chart:{
                exporting:{
                    sourceWidth:function(){
                        return window.width;
                    },
                    showTable:false,
                    allowHTML:true,
                    plotOptions:{
                        chart: {
                            backgroundColor: '#ffff',
                            plotBackgroundImage:'{{asset('assets/img/back-fs.png')}}'
                        }
                    }
                },
                title:{
                    text:'{{$dataset->name}} {{$StahunRoute}}' ,
                },
                subtitle:{
                    text:'',
                },
                chart:{
                    type:'bar',
                    height:450,
                    zoomType: 'xy'
                    
                },
                legend:{
                    enabled:false
                },
                xAxis:{
                    type:'category',
                    min: 0,
                    max: 4,
                    scrollbar: {
                        enabled: true
                    },
                },
                yAxis:[],
                plotOptions:{
                    scatter: {
                        dataLabels: {
                            enabled: true,
                            pointFormat: ''
                        },
                        tooltip: {
                            headerFormat: '<b>{point.name}</b><br>',
                            pointFormat: '{series.name}',
                            footerFormat:' x= {point.x:,.2f} , y= {point.y:,.2f}',
                            clusterFormat: 'Clustered points: {point.clusterPointsAmount} Data'
                        },
                        marker: {
                            radius: 5
                        },
                        cluster: {
                            enabled: true,
                            allowOverlap: false,
                            layoutAlgorithm: {
                                type: 'grid',
                                gridSize: 40
                            },
                            dataLabels: {
                                style: {
                                    fontSize: '9px'
                                },
                                y: -1
                            },
                            marker: {
                                lineColor: 'rgba(0, 0, 0, 0.1)'
                            },
                            zones: [
                                {
                                    from: 1,
                                    to: 1,
                                    marker: {
                                        fillColor: 'red',
                                        radius: 5
                                    }
                                },
                                {
                                    from: 1,
                                    to: 2,
                                    marker: {
                                        fillColor: '#AAE0EE',
                                        radius: 12
                                    }
                                },
                                {
                                    from: 3,
                                    to: 5,
                                    marker: {
                                        fillColor: '#65CDEF',
                                        radius: 13
                                    }
                                }, {
                                    from: 6,
                                    to: 9,
                                    marker: {
                                        fillColor: '#0DA9DD',
                                        radius: 15
                                    }
                                },{
                                    from: 10,
                                    to: 100,
                                    marker: {
                                        fillColor: '#2583C5',
                                        radius: 18
                                    }
                                },
                                {
                                    from: 11,
                                    to: 100000,
                                    marker: {
                                        fillColor: '#2583C5',
                                        radius: 18
                                    }
                                }
                            ]
                        }
                    },
                    series:{
                        dataLabels:{
                            enabled:true
                        },
                        events:{
                            legendItemClick:(e)=>{
                                // window.app.changeVisible(e.target.index,!e.target.visible);
                            }
                        }
                    },
                    pie: {
                        // allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.2f} %'
                        }
                    }
                },
                series:[
                    {
                        name:'',
                        data:[]
                    }
                ]
            },
            chart_schema:{
                tahun:{{$StahunRoute}},
                title:'{{$dataset->name}}',
                max:5,
                subtitle:'',
                Yaxis:null,
                sortedBy:null,
                sortedlist:[],
                YaxisII:null,
                YaxisIII:null,
                XaxisI:null,
                XaxisII:null,
                XaxisIII:null,
                sort:null,
                epsilon:12
            },
            form_save:{
                type:'',
                data:{
                },
                title:'',
                load:false,
            },
            load_data:{
                proccess:false,
                page:1
            }
            
        },
        created:function(){
            this.columns=this.columns.map(el=>{
                if(el['options']==undefined){
                    el['options']=[];
                }
                if(el['bind_view']==undefined){
                    el['bind_view']=[];
                }
                if(typeof el.options === 'string'){
                    el.options=JSON.parse(el.options);
                }

                if(typeof el.bind_view === 'string'){
                    el.bind_view=JSON.parse(el.bind_view);
                }


                return el;
            });

            var self=this;
            setTimeout(function(ref){
                ref.getNewData();
            },300,self);

            this.triggerChange();

        },
        methods:{
            getNewData:function(){
                if(!this.load_data.proccess){
                    this.load_data.page=1;
                    this.data=[];
                    this.load_data_proccess();
                    return true;
                }else{
                    return false;
                }
            },
            load_data_proccess:function(){
                this.load_data.proccess=true;
                var url=('{{route('api-web-mod.monev.load_data',['tahun'=>'xxxx','id_dataset'=>$dataset->id])}}').replace('xxxx',this.chart_schema.tahun);

                var self=this;
                var data={colums:this.columns_like,page:this.load_data.page};
                window.req_ajax.post(url,data).then(function(res){
                    res.data.data.forEach(el=>{
                        self.data.push(el);
                    });

                    if(self.load_data.page<res.data.last_page){
                        self.load_data.page+=1;
                        self.load_data_proccess();
                    }else{
                        self.load_data.proccess=false;
                    }
                }).catch(function (error) {
                    self.load_data.proccess=false;
                });
            },
            triggerChange:function(){
                var self=this;
                setTimeout((ref) => {
                   
                
                    if(window['v_app_clone_{{$dataset->id.'_th_min_1_'}}']!=undefined){
                        window['v_app_clone_{{$dataset->id.'_th_min_1_'}}'].changeEnv({
                            columns_like:ref.columns_like,
                            tahun:ref.chart_schema.tahun-1,
                            chart_schema:ref.chart_schema,
                            columns:ref.columns
                        });
                    }
                    if(window['v_app_clone_{{$dataset->id.'_th_plus_1_'}}']!=undefined){
                        window['v_app_clone_{{$dataset->id.'_th_plus_1_'}}'].changeEnv({
                            columns_like:ref.columns_like,
                            tahun:ref.chart_schema.tahun+1,
                            chart_schema:ref.chart_schema,
                            columns:ref.columns
                        });
                    }

                    if(window['option_app_{{$dataset->id}}']!=undefined){
                        window['option_app_{{$dataset->id}}'].changeEnv(ref.chart_schema.tahun);
                    }

                   
                
                }, 1000,self);
            },
            sendToServer:function(){
                var data=this.form_save.data;
                data.title=this.form_save.title;
                if(data.title){
                    this.form_save.load=true;
                    req_ajax.post('{{route('api-web-mod.monev.saved_chart',['tahun'=>$Stahun])}}',data,function(res){
                    this.form_save.load=false;
                        if(res.data.code==200){
                            alert('chart berhasil disimpan');
                        }else{
                            alert('chart gagal disimpan');
                        }

                    });
                    $('.modal').modal('hide')
                }else{

                }
            },
            saveStaticChart:function(){
                this.form_save.data={
                    'id_dataset':this.id,
                    'title':'',
                    'type':'static',
                    'data':this.chart
                };

                this.form_save.title='';
                this.form_save.type='static';
                $('#modal-save').modal();

            },
            saveSchemaChart:function(){
               
                var data={
                    'id_dataset':this.id,
                    'title':'',
        
                    'type':'dinamic',
                    'data':{
                        'type':this.chart.type,
                        'columns':this.columns,
                        'columns_like':this.columns_like,
                        'chart_schema':this.columns_like,
                    }
                }

                this.form_save.data=data;
                this.form_save.title='';
                this.form_save.type='dinamic';
                $('#modal-save').modal();





            },
            changeVisible:function(index,vi){
                // var self=this;
                // if(this.chart.series[index]!=undefined){
                //     this.chart.series[index].visible=vi;
                //     this.sortData();
                //     setTimeout((ref) => {
                //         var ser=ref.chart.series.sort((a,b)=>(a.visible?2:1) - (b.visible?2:1)).reverse();
                //         ref.chart.series=ser;
                //     }, (700),self);
                   
                // }
            },
            sortData:function(){
                var self=this;
                var val=this.chart_schema.sort;
                if(val){
                   if(this.chart_schema.sortedBy){
                        if(this.chart['series']==undefined){
                            this.chart['series']=[];
                        }
                            this.chart.series.forEach((ser,index)=>{
                            if(ser.visible){
                                    ser.data=ser.data.sort((a,b) => a.y - b.y);
                                    if(val=='min'){ 
                                        ser.data.reverse();
                                    }
                                    self.chart.series[index].data=ser.data;
                                    
                            }
                        });

                        var index_first=0;
                        this.chart.series.forEach((el,key)=>{
                            if(el.name==self.columns_like[self.chart_schema.sortedBy].name){
                                index_first=key;
                            }

                        });

                        if(index_first!=0){
                            var dum_ser=this.chart.series;
                            var dump=dum_ser.splice(index_first,1)[0];
                            dum_ser.splice(0,0,dump);
                            self.chart.series=dum_ser;
                        }

                        this.triggerChange();
                   }


                }

            },
            onlyUnique:function(value, index, self) {
                return (self.indexOf(value||'')) === (index);
            },
            bindView:function(val,op){
                var b=op.filter(el=>{
                    var ev_text=el.logic.replaceAll('#val',val);
                    if(eval(ev_text)){
                        return true;
                    }else{
                        return false;
                    }
                });
                if(b.length){
                    return b[0].tag;
                }else{
                    return null;
                }
            },  
            bindOptions:function(val,op){
                var b=op.filter(el=>{
                    if((el.value==val)){
                        return true
                    }else{
                        return false;
                    }
                });
                if(b.length){
                    return b[0].tag;
                }else{
                    return null;
                }
            }, 
            formatAngka:function(angka){
                    var number_string =(angka+'');
                    var split   		= number_string.split('.');
                    split[0]        = (split[0]+'').replace(/[^,\d]/g, '').toString();
                    var sisa     		= split[0].length % 3,
                    rupiah     		= split[0].substr(0, sisa),
                    ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

                    if(ribuan){
                        var separator = sisa ? '.' : '';
                        rupiah += separator + ribuan.join('.');
                    }
                    if(split[1]!=undefined){
                        split[1]=this.formatAngka(split[1]);
                    }
                    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                    return rupiah;
            },
            buildChartData:function(){
                var self=this;
                var satuan={};
                if(this.com_chart_schema){
                    var series={};
                    var series_meta=[this.chart_schema.Yaxis,this.chart_schema.YaxisII,this.chart_schema.YaxisIII];
                    series_meta=series_meta.filter(el=>{
                        return (el!=null && el!='');
                    });

                    series_meta=series_meta.filter(self.onlyUnique);

                    
                    series_meta.forEach((s,sy)=>{
                        var data={};
                        var oldX=0;


                        if(series[s]==undefined){
                            series[s]={
                                name:this.columns_like[s].name,
                                visible:true,
                                color:self.colors[sy],
                                data:[]
                            }
                        }

                        if(satuan[this.columns_like[s].satuan||'-']==undefined){
                            var lsa=Object.keys(satuan).length;
                            if(lsa%2==0){
                                satuan[this.columns_like[s].satuan||'-']={
                                    title:{
                                        text:this.columns_like[s].satuan||'-'
                                    },
                                    opposite:true
                                };
                            }else{
                                satuan[this.columns_like[s].satuan||'-']={
                                    title:{
                                        text:this.columns_like[s].satuan||'-'
                                    },
                                    opposite:false
                                };
                            }
                        }

                        var lsa=Object.keys(satuan);

                        var index_satuan=lsa.indexOf(this.columns_like[s].satuan);



                        this.data_com.forEach(el => {
                            
                                var key=[this.chart_schema.XaxisI,this.chart_schema.XaxisII];
                                key=key.filter(el=>{
                                        return (el!=null && el!='');
                                    });
                                key=key.filter(self.onlyUnique);

                                key=key.map(kll=>{
                                    return (el[kll]?el[kll]+'':'').toUpperCase();
                                });

                                key=key.join(' ');

                                
                                if(key!=' '){
                                    if(data[key]==undefined){
                                        if(this.chart.chart.type=='scatter'){
                                            data[key]={
                                                name:key,
                                                yAxis:index_satuan||0,
                                                y:0,
                                                x:0,
                                            };
                                        }else{
                                            data[key]={
                                                name:key,
                                                yAxis:index_satuan||0,
                                                y:0,
                                            };
                                        }
                                    }

                                  

                                    if(this.chart.chart.type=='scatter'){
                                        data[key]['y']+=parseFloat(el[s]||0);
                                        data[key]['x']+=parseFloat(el[this.chart_schema.XaxisIII]||0);
                                        oldX=Math.max(oldX,parseFloat(el[this.chart_schema.XaxisIII]||0));
                                        this.chart.xAxis.max=oldX;
                                    }else{
                                        data[key]['y']+=parseFloat(el[s]||0);
                                    }
                                   


                                }
                               
                        });

                        data=Object.values(data);
                       
                        series[s].data=(data);

                    });

                    series=Object.values(series);
                    if(series.length>1){
                        this.chart.legend.enabled=true;
                    }else{
                        this.chart.legend.enabled=false;
                    }
                    satuan=Object.values(satuan);
                    this.chart.yAxis=satuan;
                    this.chart.series=series;
                    this.sortData();
                    this.triggerChange();


                }
            }
        },
        watch:{
            data_com:function(val){
              
                if((['pie']).includes(this.chart.chart.type)){
                    this.chart.xAxis.max=val.length;
                }else if(!(['scatter']).includes(this.chart.chart.type)){
                    this.chart_schema.max=23;

                    this.chart.xAxis.max=23;
                }

                this.buildChartData();
            },
            "chart_schema.max":function(val){
               if(val){
                    if((val||0)<1){
                        var c=(val||0) * 10;
                        if(c<1){
                            this.chart.xAxis.max=0;
                        }else{
                            this.chart.xAxis.max=(c-1);
                        }
                    }else{
                        this.chart.xAxis.max=(val-1);
                    }

               }else{
                this.chart.xAxis.max=0;
               }
               this.buildChartData();

            },
            'chart_schema.epsilon':function(val){
                this.chart.plotOptions.scatter.cluster.layoutAlgorithm.gridSize=val;
            },
            com_chart_schema:function(val){
                if(val){
                    this.buildChartData();
                }
            },
            "chart_schema.tahun":function(val=0,old){
                if(val<2){
                    val=2019;
                }

                this.chart.title.text=this.chart_schema.title+' '+val;
                var re= this.getNewData();
                if(!re){
                   this.com_chart_schema.tahun=old;
                }else{
                    this.triggerChange();
                }
            },
            "chart_schema.sortedBy":function(val){
                if(val){
                    this.sortData(); 
                }else{
                    this.chart_schema.sort=null;
                }
            },
            'chart_schema.subtitle':function(val){
                this.chart.subtitle.text=val;
            },
            "columns_like":{
                deep:true,
                handler:function(){
                        var c=(Object.values(this.columns_like).filter(el=>{
                            return (el.val !=null && el.val !='');
                        })).map(r=>{
                            if(r['val']==undefined){
                            }
                            return (r.name+' : '+r.val).toLowerCase();
                        });

                        this.chart_schema.subtitle=c.join(' & ');
                        this.triggerChange();
                    }
            },
            "chart_schema.sort":function(val){
                this.sortData();         
            },
            "chart_schema.Yaxis":function(){
                this.buildChartData()
            },
            "chart_schema.YaxisII":function(){
                this.buildChartData()
            },
            "chart_schema.YaxisIII":function(){
                this.buildChartData()
            },
            "chart_schema.XaxisI":function(){
                this.buildChartData()
            },
            "chart_schema.XaxisII":function(){
                this.buildChartData()
            },
            "chart.chart.type":function(val){
                if((['pie','scatter'].includes(this.chart.chart.type))){
                    this.chart_schema.YaxisII=null;
                    this.chart_schema.YaxisIII=null;
                }
                if(val=='scatter'){
                    this.chart.xAxis.scrollbar.enabled=false;
                    this.chart.xAxis.type='linear';
                    this.chart_schema.XaxisII=null;
                    this.chart.plotOptions.series.dataLabels.enabled=false;
                }else{
                    // this.chart.xAxis.max=4;
                    this.chart.xAxis.scrollbar.enabled=true;
                    this.chart.xAxis.type='category';
                    this.chart.plotOptions.series.dataLabels.enabled=true;

                }
                this.buildChartData();
            }
        },
        computed:{
           com_tahun:function(){
               var v=[];
                for(var i=this.chart_schema.tahun-3;i<this.chart_schema.tahun+3;i++){
                    v.push(i);
                }
                return v;
           },
            com_chart_schema:function(){ 
               var self=this;
                if((this.chart_schema.XaxisI || this.chart_schema.XaxisII) && (  ((this.chart_schema.Yaxis || this.chart_schema.YaxisII) || this.chart_schema.YaxisIII) && (this.chart.chart.type=='scatter'?this.chart_schema.XaxisIII:true))){
                    
                    this.sortedlist=([this.chart_schema.Yaxis,this.chart_schema.YaxisII,this.chart_schema.YaxisIII]).filter((el)=>{return el;}).filter(self.onlyUnique);
                    return true;

                }else{

                    return false;
                }

            },
            data_com:function(){
                return this.data.filter((el,index)=>{
                    var display=true;
                    for (const [key, value] of Object.entries(this.columns_like)) {
                       if(display){
                        if(value['val']!=undefined && value['val']){
                            if(value['op']=='like'){
                                var f1=(value['val']||'').split('@').map(e=>{return (e||'').trim()}).filter(el=>{return (el!=null||el!='')});
                                f1.forEach(f2=>{
                                    if(!(el[key]+'').toLowerCase().includes((f2).toLowerCase())){
                                        display=false;
                                     }

                                });
                            }else  if(value['op']=='like prefix'){
                                var f1=(value['val']||'').split('@').map(e=>{return (e||'').trim()}).filter(el=>{return (el!=null||el!='')});

                                f1.forEach(f2=>{
                                    if(!(el[key]+'').toLowerCase().includes((f2).toLowerCase())){
                                        display=false;
                                     }
                                     var l=(f2).length;
                                        if(!((el[key]+'').substring(0,l)).toLowerCase()==(f2.toLowerCase())){
                                            display=false;
                                        }

                                });
                                
                            }else  if(value['op']=='not like'){
                                var f1=(value['val']||'').split('@').map(e=>{return (e||'').trim()}).filter(el=>{return (el!=null||el!='')});

                                f1.forEach(f2=>{
                                    if((el[key]+'').toLowerCase().includes((f2).toLowerCase())){
                                        display=false;
                                     }

                                });
                            }
                            else{
                                display=eval((el[key]||0)+ " "+(value['op']=='='?'==':value['op'])+' '+value['val']);
                            }
                        }
                       }
                    }
                    return display;
                });
            }
        }
    })
</script>

@stop


@push('js_push')

<script>
    console.log('ss');
    var option_app_{{$dataset->id}}=new Vue({
        el:'#sink_saved',
        data:{
            data:[],
            tahun:{{$StahunRoute}},
            load_data:{
                proccess:false,
                page:1
            }
        },
        created:function(){
           var self=this;
           setTimeout(function(ref){
                ref.init();
           },1000,self);
        },
        methods:{
            proccesDefault:function(data){
                data.target.show();
            },
            show:function(){

            },
            changeEnv:function(th){
                this.tahun=th;
            },
            evalDef:function(text){
                return text.replace('th-1',this.tahun - 1).replace('th+1',this.tahun+1);
            },
            init:function(){
                if(this.load_data.proccess==false){
                    this.data=[];
                    if(window['v_app_clone_{{$dataset->id.'_th_min_1_'}}']!=undefined){
                        this.data.push({
                            type:'default',
                            name:'Data Tahun th-1',
                            target:window['v_app_clone_{{$dataset->id.'_th_min_1_'}}']
                        });
                    }

                    if(window['v_app_clone_{{$dataset->id.'_th_plus_1_'}}']!=undefined){
                        this.data.push({
                            type:'default',
                            name:'Data Tahun th+1',
                            target:window['v_app_clone_{{$dataset->id.'_th_plus_1_'}}']
                        });

                    }
                }
            }
        }
    });

</script>

@endpush