
<div id="app_clone_{{$dataset->id.'_th_plus_1_'}}">
    
    <div class="modal fade" id="app_clone_{{$dataset->id.'_th_plus_1_'}}_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-full" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title"></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="d-flex">
                    <div class="form-group mr-2 w-150">
                        <label for="">Chart</label>
                        <select name="" class="form-control" id="" v-model="chart.chart.type">
                            <option value="bar">BAR</option>
                            <option value="column">COLUMN</option>
                            <option value="pie">PIE</option>
                            <option value="scatter">SCATTER</option>
                        </select>
                    </div>
                </div>
                <charts  :options="chart" class="mb-3"  :constructor-type="'mapChart'" v-if="com_chart_schema"></charts>
                <p class="text-center" v-if="!com_chart_schema">Chart Belum Tersedia</p>
                <div class="table-responsive">
                    <datatable :columns="Object.values(columns_like)" class="table table-bordered" :data="data_com"></datatable>

                </div>
            </div>
            
          </div>
        </div>
      </div>

</div>

@push('js_push')
<script>
    Vue.prototype.Object = window.Object;

    var v_app_clone_{{$dataset->id.'_th_plus_1_'}}=new Vue({
        el:"#app_clone_{{$dataset->id.'_th_plus_1_'}}",
        data:{
            list_saved:[],
            id:{{$dataset->id}},
            data:[],
            columns:[],
            columns_like:{},
            colors:[
                "#7cb5ec",
                "#f7a35c",
                "#90ee7e",
                "#7798BF",
                "#aaeeee",
                "#ff0066",
                "#eeaaee",
                "#55BF3B",
                "#DF5353",
                "#7798BF",
                "#aaeeee"
            ],
            chart:{
                title:{
                    text:'' ,
                },
                subtitle:{
                    text:'',
                },
                chart:{
                    type:'bar',
                    height:450,
                    
                },
                legend:{
                    enabled:false
                },
                xAxis:{
                    type:'category',
                    min: 0,
                    max: 4,
                    scrollbar: {
                        enabled: true
                    },
                },
                yAxis:{
                    title:{
                        text:''
                    }
                },
                
                plotOptions:{
                    series:{
                        dataLabels:{
                            enabled:true
                        },
                        events:{
                            legendItemClick:(e)=>{
                                // window.app.changeVisible(e.target.index,!e.target.visible);
                            }
                        }
                    },
                    pie: {
                        // allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                        }
                    }
                },
                series:[
                    {
                        name:'',
                        data:[]
                    }
                ]
            },
            chart_schema:{
                tahun:0,
                title:'{{$dataset->name}}',
                max:5,
                subtitle:'',
                Yaxis:null,
                sortedBy:null,
                sortedlist:[],
                YaxisII:null,
                YaxisIII:null,
                XaxisI:null,
                XaxisII:null,
                XaxisIII:null,
                sort:null,
            },
            form_save:{
                type:'',
                data:{
                },
                title:'',
                load:false,
            },
            load_data:{
                proccess:false,
                page:1
            }
            
        },
        created:function(){
            this.columns=this.columns.map(el=>{
                if(el['options']==undefined){
                    el['options']=[];
                }
                if(el['bind_view']==undefined){
                    el['bind_view']=[];
                }
                if(typeof el.options === 'string'){
                    el.options=JSON.parse(el.options);
                }

                if(typeof el.bind_view === 'string'){
                    el.bind_view=JSON.parse(el.bind_view);
                }


                return el;
            });
        },
        methods:{
            getNewData:function(){
                if(this.load_data.proccess==false){
                    this.load_data.page=1;
                    this.data=[];
                    var self=this;
                    setTimeout((ref) => {
                        ref.load_data_proccess();
                    }, 600,self);
                    return true;
                }else{
                    return false;
                }
            },
            load_data_proccess:function(){
                this.load_data.proccess=true;
                var url=('{{route('api-web-mod.monev.load_data',['tahun'=>'xxxx','id_dataset'=>$dataset->id])}}').replace('xxxx',this.chart_schema.tahun);
                var self=this;
                var self=this;
                var data={colums:this.columns_like,page:this.load_data.page};

                window.req_ajax.post(url,data).then(function(res){
                   
                    
                    res.data.data.forEach(el=>{
                        self.data.push(el);
                    });

                    if(self.load_data.page<res.data.last_page){
                        self.load_data.page+=1;
                        self.load_data_proccess();
                    }else{
                        self.load_data.proccess=false;
                    }
                }).catch(function (error) {
                    self.load_data.proccess=false;
                });
            },
            show:()=>{
                $('#app_clone_{{$dataset->id.'_th_plus_1_'}}_modal').modal();
            },
            changeEnv:function(val){
                var self=this;
                Object.assign(self.columns_like,val.columns_like);
                this.columns=val.columns;
                Object.assign(self.chart_schema,val.chart_schema);
                this.chart_schema.tahun=val.tahun;


            },
            sendToServer:function(){
                var data=this.form_save.data;
                data.title=this.form_save.title;
                if(data.title){
                    this.form_save.load=true;
                    req_ajax.post('{{route('api-web-mod.monev.saved_chart',['tahun'=>$tahun])}}',data,function(res){
                    this.form_save.load=false;
                        if(res.data.code==200){
                            alert('chart berhasil disimpan');
                        }else{
                            alert('chart gagal disimpan');
                        }

                    });
                    $('.modal').modal('hide')
                }else{

                }
            },
            saveStaticChart:function(){
                this.form_save.data={
                    'id_dataset':this.id,
                    'title':'',
                    'type':'static',
                    'data':this.chart
                };

                this.form_save.title='';
                this.form_save.type='static';
                $('#modal-save').modal();

            },
            saveSchemaChart:function(){
               
                var data={
                    'id_dataset':this.id,
                    'title':'',
        
                    'type':'dinamic',
                    'data':{
                        'type':this.chart.type,
                        'columns':this.columns,
                        'columns_like':this.columns_like,
                        'chart_schema':this.columns_like,
                    }
                }

                this.form_save.data=data;
                this.form_save.title='';
                this.form_save.type='dinamic';
                $('#modal-save').modal();





            },
            changeVisible:function(index,vi){
                // var self=this;
                // if(this.chart.series[index]!=undefined){
                //     this.chart.series[index].visible=vi;
                //     this.sortData();
                //     setTimeout((ref) => {
                //         var ser=ref.chart.series.sort((a,b)=>(a.visible?2:1) - (b.visible?2:1)).reverse();
                //         ref.chart.series=ser;
                //     }, (700),self);
                   
                // }
            },
            sortData:function(){
                var self=this;
                var val=this.chart_schema.sort;
                if(val){
                   if(this.chart_schema.sortedBy){
                        if(this.chart['series']==undefined){
                            this.chart['series']=[];
                        }
                            this.chart.series.forEach((ser,index)=>{
                            if(ser.visible){
                                    ser.data=ser.data.sort((a,b) => a.y - b.y);
                                    if(val=='min'){ 
                                        ser.data.reverse();
                                    }
                                    self.chart.series[index].data=ser.data;
                                    
                            }
                        });

                        var index_first=0;
                        this.chart.series.forEach((el,key)=>{
                            if(el.name==self.columns_like[self.chart_schema.sortedBy].name){
                                index_first=key;
                            }

                        });

                        if(index_first!=0){
                            var dum_ser=this.chart.series;
                            var dump=dum_ser.splice(index_first,1)[0];
                            dum_ser.splice(0,0,dump);
                            self.chart.series=dum_ser;
                        }
                   }


                }       
            },
            onlyUnique:function(value, index, self) {
                return (self.indexOf(value||'')) === (index);
            },
            bindView:function(val,op){
                var b=op.filter(el=>{
                    var ev_text=el.logic.replaceAll('#val',val);
                    if(eval(ev_text)){
                        return true;
                    }else{
                        return false;
                    }
                });
                if(b.length){
                    return b[0].tag;
                }else{
                    return null;
                }
            },  
            bindOptions:function(val,op){
                var b=op.filter(el=>{
                    if((el.value==val)){
                        return true
                    }else{
                        return false;
                    }
                });
                if(b.length){
                    return b[0].tag;
                }else{
                    return null;
                }
            }, 
            formatAngka:function(angka){
                    var number_string =(angka+'');
                    var split   		= number_string.split('.');
                    split[0]        = (split[0]+'').replace(/[^,\d]/g, '').toString();
                    var sisa     		= split[0].length % 3,
                    rupiah     		= split[0].substr(0, sisa),
                    ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

                    if(ribuan){
                        var separator = sisa ? '.' : '';
                        rupiah += separator + ribuan.join('.');
                    }
                    if(split[1]!=undefined){
                        split[1]=this.formatAngka(split[1]);
                    }
                    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                    return rupiah;
            },
            buildChartData:function(){
                var self=this;
                if(this.com_chart_schema){
                    var series={};
                    var series_meta=[this.chart_schema.Yaxis,this.chart_schema.YaxisII,this.chart_schema.YaxisIII];
                    series_meta=series_meta.filter(el=>{
                        return (el!=null && el!='');
                    });

                    series_meta=series_meta.filter(self.onlyUnique);

                    
                    series_meta.forEach((s,sy)=>{
                        var data={};
                        var oldX=0;


                        if(series[s]==undefined){
                            series[s]={
                                name:this.columns_like[s].name,
                                visible:true,
                                color:self.colors[sy],
                                data:[]
                            }
                        }



                        this.data_com.forEach(el => {
                            
                                var key=[this.chart_schema.XaxisI,this.chart_schema.XaxisII];
                                key=key.filter(el=>{
                                        return (el!=null && el!='');
                                    });
                                key=key.filter(self.onlyUnique);

                                key=key.map(kll=>{
                                    return (el[kll]||'').toUpperCase();
                                });

                                key=key.join(' ');

                                
                                if(key!=' '){
                                    if(data[key]==undefined){
                                        if(this.chart.chart.type=='scatter'){
                                            data[key]={
                                                name:key,
                                                y:0,
                                                x:0,
                                            };
                                        }else{
                                            data[key]={
                                                name:key,
                                                y:0,
                                            };
                                        }
                                    }

                                    if(this.chart.chart.type=='scatter'){
                                        data[key]['y']+=parseFloat(el[s]||0);
                                        data[key]['x']+=parseFloat(el[this.chart_schema.XaxisIII]||0);
                                        oldX=Math.max(oldX,parseFloat(el[this.chart_schema.XaxisIII]||0));
                                        this.chart.xAxis.max=oldX;
                                    }else{
                                            data[key]['y']+=parseFloat(el[s]||0);
                                    }

                                }
                               
                        });

                        data=Object.values(data);
                        
                        series[s].data=(data);

                    });

                    series=Object.values(series);
                    if(series.length>1){
                        this.chart.legend.enabled=true;
                    }else{
                        this.chart.legend.enabled=false;
                    }
                    this.chart.series=series;
                    this.sortData();


                }
            }
        },
        watch:{
            data_com:function(val){
              
                if((['pie']).includes(this.chart.chart.type)){
                    this.chart.xAxis.max=val.length;
                }else if(!(['scatter']).includes(this.chart.chart.type)){
                    this.chart_schema.max=23;

                    this.chart.xAxis.max=23;
                }

                this.buildChartData();
            },
            "chart_schema.max":function(val){
               if(val){
                    if((val||0)<1){
                        var c=(val||0) * 10;
                        if(c<1){
                            this.chart.xAxis.max=0;
                        }else{
                            this.chart.xAxis.max=(c-1);
                        }
                    }else{
                        this.chart.xAxis.max=(val-1);
                    }

               }else{
                this.chart.xAxis.max=0;
               }
               this.buildChartData();

            },
            com_chart_schema:function(val){
                if(val){
                    this.buildChartData();
                }
            },
            "chart_schema.tahun":function(val,old){
                this.chart.title.text=this.chart_schema.title+' '+val;
               var re= this.getNewData();

               if(!re){
                   this.com_chart_schema.tahun=old;

               }
            },
            "chart_schema.sortedBy":function(val){
                if(val){
                    this.sortData(); 
                }else{
                    this.chart_schema.sort=null;
                }
            },
            'chart_schema.subtitle':function(val){
                this.chart.subtitle.text=val;
            },
            "columns_like":{
                deep:true,
                handler:function(){
                        var c=(Object.values(this.columns_like).filter(el=>{
                            return (el.val !=null && el.val !='');
                        })).map(r=>{
                            if(r['val']==undefined){
                            }
                            return (r.name+' : '+r.val).toLowerCase();
                        });

                        this.chart_schema.subtitle=c.join(' & ');

                    }
            },
            "chart_schema.sort":function(val){
                this.sortData();         
            },
            "chart_schema.Yaxis":function(){
                this.buildChartData()
            },
            "chart_schema.YaxisII":function(){
                this.buildChartData()
            },
            "chart_schema.YaxisIII":function(){
                this.buildChartData()
            },
            "chart_schema.XaxisI":function(){
                this.buildChartData()
            },
            "chart_schema.XaxisII":function(){
                this.buildChartData()
            },
            "chart.chart.type":function(val){
                if((['pie','scatter'].includes(this.chart.chart.type))){
                    this.chart_schema.YaxisII=null;
                    this.chart_schema.YaxisIII=null;
                }
                if(val=='scatter'){
                    this.chart.xAxis.scrollbar.enabled=false;
                    this.chart.xAxis.type='linear';
                    this.chart_schema.XaxisII=null;
                    this.chart.plotOptions.series.dataLabels.enabled=false;
                }else{
                    // this.chart.xAxis.max=4;
                    this.chart.xAxis.scrollbar.enabled=true;
                    this.chart.xAxis.type='category';
                    this.chart.plotOptions.series.dataLabels.enabled=true;

                }
                this.buildChartData();
            }
        },
        computed:{
           
            com_chart_schema:function(){ 
               var self=this;
                if((this.chart_schema.XaxisI || this.chart_schema.XaxisII) && (  ((this.chart_schema.Yaxis || this.chart_schema.YaxisII) || this.chart_schema.YaxisIII) && (this.chart.chart.type=='scatter'?this.chart_schema.XaxisIII:true))){
                    
                    this.sortedlist=([this.chart_schema.Yaxis,this.chart_schema.YaxisII,this.chart_schema.YaxisIII]).filter((el)=>{return el;}).filter(self.onlyUnique);
                    return true;

                }else{

                    return false;
                }

            },
            data_com:function(){
                return this.data.filter((el,index)=>{
                    var display=true;
                    for (const [key, value] of Object.entries(this.columns_like)) {
                       if(display){
                        if(value['val']!=undefined && value['val']){
                            if(value['op']=='like'){
                                if(!(el[key]+'').toLowerCase().includes(value['val'].toLowerCase())){
                                    display=false;
                                 }
                            }else  if(value['op']=='like prefix'){
                                var l=(value['val']||'').length;
                                if(!((el[key]+'').substring(0,l)).toLowerCase()==(value['val'].toLowerCase())){
                                    display=false;
                                 }
                            }
                            else{
                                display=eval((el[key]||0)+ " "+(value['op']=='='?'==':value['op'])+' '+value['val']);
                            }
                        }
                       }
                    }
                    return display;
                });
            }
        }
    });
</script>

@endpush