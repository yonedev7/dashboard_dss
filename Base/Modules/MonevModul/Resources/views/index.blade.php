@extends('adminlte::page')


@section('content')
<script>
    var page_meta=<?=json_encode($page_meta)?>;
</script>
<div class="parallax pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
    <div class="container">
        <section >
            <h3><b>{{$page_meta['title']}}</b></h3>
            <p><b>{!!$page_meta['keterangan']!!}</b></p>
        </section>
      
    </div>
   
</div>
@push('css_push')
<style>
    .table-hover tbody tr:hover {
        color: #212529;
        background-color: #1062e63d!important;
    }

    .bg-info-soft{
        background: #e7f0f8;
        
    }
    .bg-warning-soft{
        background: #f7f3ea;
    }

    .bg-success-soft{
        background:#ecf4ee;
    }
    .bg-teal-soft{
        background: #ebf5f2;
    }
</style>
@endpush
@include('data.partials.filter_basis_monev')


<div class="card" id="app">
    <div class="card-body">
        <div class="table table-responsive" style="height: calc(100vh - 80px);">
            <table class="table table-bordered table-hover table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th rowspan="4" width="5%"></th>
                        <th rowspan="4" width="10%"></th>
                        <th rowspan="4" width="10%"></th>
                        <th colspan="4" width="20%">KPI I 40 Pemda meningkatkan dukungan finansial kepada
BUMDAM
</th>
                        <th colspan="3" width="20%">KPI II 20 Pemda/PDAM mendapatkan program pendamping</th>
                        <th colspan="3" width="20%">KPI III 200 pemda 
berpartisipasi aktif dalam 
program pelatihan dan 
peningkatan kapasitas
</th>
                        <th colspan="1" width="15%">KPI IV 50 PDAM telah 
mencapai tarif yang full costrecovery 
</th>
                    </tr>
                    <tr>
                        <th colspan="11" class="text-center">TAHUN KEGIATAN BERJALAN BASIS {{$basis}} NUWSP</th>
                        </tr>
                        <tr>
                        <th colspan="11" class="text-center">ELEMENT DATA</th>
                    </tr>
                    <tr>
                        <th colspan="2">DDUB</th>
                        <th colspan="2">PMPD</th>
                        <th colspan="1">MITRA KERJASAMA NON PUBLIC</th>
                        <th colspan="1">BENTUK KERJASAMA NON PUBLIC</th>
                        <th>BANTUAN NUWSP</th>

                        
                        <th colspan="1">TEMA PELATIHAN</th>
                        <th colspan="1">KEBUTUHAN PELATIHAN</th>
                        <th colspan="1">JUMLAH PESERTA</th>
                        <th colspan="1">FCR LAPORAN TAHUN {{$StahunRoute-1}}</th>

                    </tr>
                    <tr style="position:sticky;top:0">
                            <th>NO.</th>
                            <th>KODEPEMDA</th>
                            <th>NAMAPEMDA</th>
                            <th class="bg-info-soft">JUMLAH PAGU</th>
                            <th class="bg-info-soft">JUMLAH REALISASI</th>
                            <th class="bg-info-soft">JUMLAH MODAL PEMDA KEPADA BUMDAM</th>
                            <th class="bg-info-soft">JUMLAH PENDANAAN PEMBANGUNAN INFRASTRUKTUR</th>

                            <th class="bg-success-soft">JUMLAH MITRA</th>
                            <th>NILAI KONTRAK</th>
                            <th>JENIS BANTUAN</th>

                            <th>JUMLAH TEMA</th>
                            <th>JUMLAH KEBUTUHAN</th>
                            <th>JUMLAH PESERTA</th>

                            <th>FCR/ NON FCR</th>
                    </tr>
            </thead>
            <tbody>
                <tr v-if="loading">
                    <td colspan="14" class="text-center">
                        <div class="progress" v-html="com_proggress">     
                        </div>
                    </td>
                </tr>
                <tr v-for="item,k in data">
                    <td >@{{k+1}}</td>
                    <td >@{{item.kodepemda}}</td>
                    <td>@{{item.name}}</td>
                    <td class="bg-info-soft">@{{item['ddub']!=null?'Rp. '+formatAngka(item['ddub']['ddub_perencanaan']):''}}</td>
                    <td class="bg-info-soft">@{{item['ddub']!=null?'Rp. '+formatAngka(item['ddub']['ddub_realisasi']):''}}</td>
                    <td class="bg-info-soft">@{{item['pmpd']!=null?'Rp. '+formatAngka(item['pmpd']['modal_pemda']):''}}</td>
                    <td class="bg-info-soft" >@{{item['pmpd']!=null?'Rp. '+formatAngka(item['pmpd']['modal_pemda_infra']):''}}</td>
                    <td class="bg-success-soft" v-html="bindMitraKerjasama(item['kerjasama']!=null?item['kerjasama']['mitra']:'')"></td>
                    <td class="bg-success-soft" v-html="bindBentukKerjasama(item['kerjasama']!=null?item['kerjasama']['mitra']:'')"></td>
                    <td class="bg-success-soft" v-html="bindJenisBantuan(item['tipe_bantuan'])"></td>
                    <td class="bg-warning-soft" v-html="bindTemaPelatihan(item['pelatihan']!=null?item['pelatihan']['tema']:'')"></td>
                    <td class="bg-warning-soft" v-html="bindPelatihan(item['pelatihan']!=null?item['pelatihan']['tema']:'')"></td>
                    <td class="bg-warning-soft" v-html="bindJumlahPeserta(item['pelatihan']!=null?item['pelatihan']['tema']:'')"></td>
                    <td class="bg-teal-soft" v-html="bindFcr(item['fcr']!=null?item['fcr']['nilai_fcr']:'')"></td>
                </tr>
            </tbody>
            <tfoot style="position:sticky;bottom:0">
                <tr class="bg-secondary">
                    <th colspan="14" class="text-center">NILAI DATA</th>    
                </tr>
                <tr class="bg-white">
                    <td colspan="3"><b>Total</b></td>
                  
                    <td class="bg-info-soft" >Rp. @{{formatAngka(total_ddub_perencanaan)}}</td>
                    <td class="bg-info-soft">Rp. @{{formatAngka(total_ddub_realisasi)}}</td>
                    <td class="bg-info-soft">Rp. @{{formatAngka(total_pmpd_modal_pemda)}}</td>
                    <td class="bg-info-soft" >Rp. @{{formatAngka(total_pmpd_modal_pemda_infra)}}</td>
                    <td class="bg-success-soft">@{{total_kerjasama_mitra}} Mitra</td>
                    <td class="bg-success-soft">Rp. @{{formatAngka(total_kerjasama_nilai_kontrak)}}</td>
                    <td class="bg-success-soft">@{{total_bantuan_pendamping}} Pemda  (Bantuan Pendamping)</td>
                    <td class="bg-warning-soft" >@{{total_pelatihan_tema}} Tema</td>
                    <td class="bg-warning-soft" >@{{total_pelatihan_pelatihan}} Pelatihan</td>
                    <td class="bg-warning-soft" >@{{formatAngka(total_pelatihan_jumlah_peserta)}} Orang</td>
                    <td class="bg-teal-soft" >@{{total_fcr_fcr}} Pemda FCR</td>

                </tr>
                
            <tr class="bg-secondary" >
                <td colspan="3">Total Data Pemda (@{{total}} Pemda)</td>
                <td class="bg-info-soft text-dark" colspan="2">@{{total_pemda_ddub}} Pemda</td>
                <td class="bg-info-soft text-dark" colspan="2">@{{total_pemda_pmpd}} Pemda</td>
                <td class="bg-success-soft text-dark" colspan="2">@{{total_pemda_kerjasama}} Pemda</td>
                <td class="bg-success-soft text-dark" >@{{total_pemda_bantuan}} Pemda</td>
                <td class="bg-warning-soft text-dark " colspan="3">@{{total_pemda_pelatihan}} Pemda</td>
                <td class="bg-teal-soft text-dark"  colspan="1">@{{total_pemda_fcr}} Pemda</td>
                
                </tr>
            </tfoot>
            </table>
         </div>    
    </div>
</div>

@stop

@section('js')
<script>
    var app=new Vue({
        el:'#app',
        data:{
            data:[],
            total:0,
            loading:false,
            current_page:1,
            filter:'provinsi & kota/kab',
            basis:'{{$basis}}'

        },
        computed:{
            com_proggress:function(){
                return '<div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="'+((this.data.length/this.total)*100)+'" aria-valuemin="0" aria-valuemax="100" style="width:'+((this.data.length/this.total)*100)+'%">'+(((this.data.length/this.total)*100).toFixed(2))+'%</div>';
            },
            total_pemda_bantuan:function(){
                var d=this.data.filter((el)=>{
                    return (el.tipe_bantuan!=null && el.tipe_bantuan!='');
                });
                return d.length;
            },
            total_bantuan_pendamping:function(){
                var d=this.data.filter((el)=>{
                    if(el.tipe_bantuan!=null){
                        if(el.tipe_bantuan.toUpperCase().includes('PENDAMPING')){
                            return true;
                        }
                    }else{
                        return false;
                    }
                });
                return d.length;
            },
            total_ddub_perencanaan:function(){
                var d=this.data.map((el)=>{
                    if(el.ddub!=null){
                        return parseFloat(el.ddub.ddub_perencanaan)||0;
                    }else{
                        return 0;
                    }
                });
                return d.reduce((a, b) => a + b, 0);

            },
            total_ddub_realisasi:function(){
                var d=this.data.map((el)=>{
                    if(el.ddub!=null){
                        return parseFloat(el.ddub.ddub_realisasi)||0;
                    }else{
                        return 0;
                    }
                });
                return d.reduce((a, b) => a + b, 0);

            },
            total_pmpd_modal_pemda:function(){
                var d=this.data.map((el)=>{
                    if(el.pmpd!=null){
                        return parseFloat(el.pmpd.modal_pemda)||0;
                    }else{
                        return 0;
                    }
                });
                return d.reduce((a, b) => a + b, 0);

            },
            total_pmpd_modal_pemda_infra:function(){
                var d=this.data.map((el)=>{
                    if(el.pmpd!=null){
                        return parseFloat(el.pmpd.modal_pemda_infra)||0;
                    }else{
                        return 0;
                    }
                });
                return d.reduce((a, b) => a + b, 0);

            },
            total_kerjasama_nilai_kontrak:function(){
                var d=this.data.map((el)=>{
                    if(el.kerjasama!=null){
                        return el.kerjasama.mitra.map((m)=>{
                            return parseFloat(m.nilai_kontrak)||0;
                        }).reduce((a, b) => a + b, 0);
                    }else{
                        return 0;
                    }
                });
                return d.reduce((a, b) => a + b, 0);

            },
            total_pelatihan_tema:function(){
                var d=this.data.map((el)=>{
                    if(el.pelatihan!=null){
                        var mit=el.pelatihan.tema.map(m=>{
                            m.name=m.name.replace(/[\s,\r]/g,' ');
                            return m.name;
                        });

                        mit=mit.filter((x, i, a) => a.indexOf(x) == i);
                        return mit.length;
                    }else{
                        return 0;
                    }
                });
                return d.reduce((a, b) => a + b, 0);

            },
            total_pelatihan_pelatihan:function(){
                var d=this.data.map((el)=>{
                    if(el.pelatihan!=null){
                        var mit=el.pelatihan.tema;
                        return mit.length;
                    }else{
                        return 0;
                    }
                });
                return d.reduce((a, b) => a + b, 0);

            },
            total_pelatihan_jumlah_peserta:function(){
                var d=this.data.map((el)=>{
                    if(el.pelatihan!=null){
                        return el.pelatihan.tema.map((m)=>{
                            return parseInt(m.jumlah_peserta)||0;
                        }).reduce((a, b) => a + b, 0);
                    }else{
                        return 0;
                    }
                });
                return d.reduce((a, b) => a + b, 0);

            },
            total_kerjasama_mitra:function(){
                var d=this.data.map((el)=>{
                    if(el.kerjasama!=null){
                        var mit=el.kerjasama.mitra.map(m=>{
                            m.name=m.name.replace(/[\s,\r]/g,' ');
                            return m.name;
                        });

                        mit=mit.filter((x, i, a) => a.indexOf(x) == i);
                        return mit.length;
                    }else{
                        return 0;
                    }
                });
                return d.reduce((a, b) => a + b, 0);

            },
            total_fcr_fcr:function(){
                var d=this.data.filter((el)=>{
                    if(el.fcr!=null){
                        if(parseFloat(el.fcr.nilai_fcr)>=100){
                            return true;
                        }
                    }else{
                        return false;
                    }
                });
                
                return d.length;

            },

            
            total_pemda_ddub:function(){
                var d=this.data.filter((el)=>{
                    return el.ddub!=null;
                });

                return d.length;
            },
            total_pemda_pelatihan:function(){
                var d=this.data.filter((el)=>{
                    return el.pelatihan!=null;
                });

                return d.length;
            },
            total_pemda_fcr:function(){
                var d=this.data.filter((el)=>{
                    return el.fcr!=null;
                });

                return d.length;
            },
            total_pemda_kerjasama:function(){
                var d=this.data.filter((el)=>{
                    return el.kerjasama!=null;
                });

                return d.length;
            },
            total_pemda_pmpd:function(){
                var d=this.data.filter((el)=>{
                    return el.pmpd!=null;
                });

                return d.length;
            }

        },
        created:function(){
            this.init();
            
        },
        methods:{
            bindJenisBantuan:function(val){
                return ((val||'').toUpperCase()=='PENDAMPINGAN'?'<p><span class="badge bg-primary">'+val+'</span></p>':val);
            },
            bindMitraKerjasama:function(val){
               if(val){

                var bind=val.map((el,i)=>{
                    el.name=el.name.replace(/[\s,\r]/g,' ');
                    return [el.name,parseFloat(el.nilai_kontrak)];
                });

                var newArr=[];
                bind.forEach((element ,index)=> {
                    var index_exist=-1;
                    newArr.forEach((el,i)=>{
                        if(el.name==element[0]){
                            index_exist=i;
                        }
                    });
                    if(index_exist==-1){
                        newArr.push({
                            name:element[0],
                            nilai_kontrak:element[1]
                        });
                    }else{
                        newArr[index_exist]['nilai_kontrak']+=element[1];
                    }
                });

                newArr=newArr.map((el,i)=>{
                    return (i+1)+'. '+el.name;
                });

                return newArr.join('<br>');
               }else{
                   return '';
               }
            },
            bindTemaPelatihan:function(val){
               if(val){

                var bind=val.map((el,i)=>{
                    el.name=el.name.replace(/[\s,\r]/g,' ');
                    return [el.name,el.pelatihan,parseFloat(el.jumlah_peserta)];
                });

                var newArr=[];
                bind.forEach((element ,index)=> {
                    var index_exist=-1;
                    newArr.forEach((el,i)=>{
                        if(el.name==element[0]){
                            index_exist=i;
                        }
                    });
                    if(index_exist==-1){
                        newArr.push({
                            name:element[0],
                            palatihan:element[1],
                            jumlah_peserta:element[2]
                        });
                    }else{
                        newArr[index_exist]['jumlah_peserta']+=element[1];
                    }
                });

                newArr=newArr.map((el,i)=>{
                    return (i+1)+'. '+el.name;
                });

                return newArr.join('<br>');
               }else{
                   return '';
               }
            },
            bindPelatihan:function(val){
               if(val){
                
                var filter=val.filter(el=>{
                    return el.pelatihan!=null&&el.pelatihan!='';
                });

                var bind=filter.map((el,i)=>{
                    el.name=el.name.replace(/[\s,\r]/g,' ');
                    el.pelatihan=el.pelatihan.replace(/[\s,\r]/g,' ');
                    return  (i+1)+'. '+el.name+' - '+el.pelatihan;
                });

                return bind.join('<br>');
               }else{
                   return '';
               }
            },
            bindJumlahPeserta:function(val){
               if(val){
                
                var filter=val.filter(el=>{
                    return el.pelatihan!=null&&el.pelatihan!='';
                });

                var bind=filter.map((el,i)=>{
                    el.name=el.name.replace(/[\s,\r]/g,' ');
                    el.pelatihan=el.pelatihan.replace(/[\s,\r]/g,' ');
                    return  (i+1)+'. '+el.pelatihan+' ('+this.formatAngka(el.jumlah_peserta)+' Peserta)';
                });

                return bind.join('<br>');
               }else{
                   return '';
               }
            },
            bindFcr:function(val){
                if(val){
                    return (parseFloat(val)>=100?'FCR':'NON-FCR')+' ('+parseFloat(val).toFixed(1)+'%)';
                }else{
                    return '';
                }
            },
            bindBentukKerjasama:function(val){
                if(val){
                    var bind=val.map((el,i)=>{
                        el.bentuk_kegiatan=el.bentuk_kegiatan.replace(/[\s,\r]/g,' ');
                        el.name=el.name.replace(/[\s,\r]/g,' ');
                        return (i+1)+'. '+el.name+' (Tahun Kerjasama '+el.tahun_kerjasama+') - '+el.bentuk_kegiatan+' (Nilai Kontrak Rp. <b class="text-success">'+this.formatAngka(el.nilai_kontrak)+'</b>)';
                    });

                    return bind.join('<br>');

                }else{
                    return '';
                }
               
            },
            formatAngka:function(angka){
                var number_string = (angka+'').replace(/[^,\d]/g, '').toString(),
                split   		= number_string.split(','),
                sisa     		= split[0].length % 3,
                rupiah     		= split[0].substr(0, sisa),
                ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
    
                // tambahkan titik jika yang di input sudah menjadi angka ribuan
                if(ribuan){
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                }
    
                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                return rupiah;
            },
            init:function(){
                this.current_page=1;
                this.data=[];
                this.total=1;
                this.load();
            },
            load:function(){
                this.loading=true;
                var self=this;
                req_ajax.post('{{route('api-web-mod.monev.load',['tahun'=>$StahunRoute])}}',{
                    filter:this.filter,
                    base:this.basis,
                    page:this.current_page
                }).then(res=>{
                    self.loading=false;
                    self.total=res.data.total;
                    res.data.data.forEach(element => {
                        self.data.push(element);
                    });
                    if(res.data.last_page!=self.current_page){
                        self.current_page+=1;
                        self.load();
                    }
                    
                }).catch(function(err){
                    self.loading=false;
                });
            }
        }
    });    
</script>

@stop
