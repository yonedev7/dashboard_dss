<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('mod/{tahun}/module-monev')->name('api-web-mod.monev')->middleware('auth:api')->group(function(){
    Route::post('load','MonevModulController@load')->name('.load');
    Route::post('load_ddub/{id_dataset}/{tw}','FormController@load_ddub')->name('.load_ddub');
    Route::post('penilaian-kpi/{id_kpi}','DashboardController@penilaian_kpi')->name('.penilaian_kpi');
    Route::post('saved-chart','MonevModulController@saved_chart')->name('.saved_chart');
    Route::post('load_dataset_data/{id_dataset}','MonevModulController@load_dataset_data')->name('.load_data');




});