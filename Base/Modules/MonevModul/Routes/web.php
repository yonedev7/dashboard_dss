<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('session/{tahun}/mod-monev')->middleware('bindTahun')->name('mod.monev')->group(function(){
        Route::prefix('module-monev')->group(function() {
            Route::get('/', 'DashboardController@index')->middleware('auth:web')->name('.index');
            Route::get('/detail-kpi/{kpi}/{kodepemda}', 'DashboardController@detail')->middleware('auth:web')->name('.detail');
        });
});

Route::prefix('dash/{tahun}/sistem-informasi/mod-monev')->name('mod.monev-admin')->middleware(['auth:web','bindTahun'])->group(function(){
        Route::prefix('form')->name('.form')->group(function() {
            Route::get('/','FormController@index')->name('.index');
            Route::get('/input/{id_monev_data}/{kodepemda?}','FormController@form_edit')->name('.edit_data');
            Route::get('/input-multy/{id_monev_data}','FormController@select_pemda_input')->name('.select-pemda-input');
            Route::get('/sinkronisasi/{id_monev_data}/{id_dataset}/{kodepemda?}','FormController@sink')->name('.sink');
            Route::get('/form-bind/show/{id_dataset}/{tw}','FormController@ddub')->name('.ddub');
            Route::put('/form-bind/update/{id_dataset}/{tw}/','FormController@update_ddub')->name('.update_ddub');
        });
});
