<?php

return [
   'PEMDA'=>[
       [
            'name'=>'RKPD',
            'keterangan'=>'',
            'view'=>'parameterprofile::view_bind.pemda.rkpd'
        ],
        [
            'name'=>'RKPD 1 TAHUN SEBELUMNYA',
            'keterangan'=>'',
            'view'=>'parameterprofile::view_bind.pemda.rkpd_min_1'
        ],
    ],
    'BUMDAM'=>[
        [
            'name'=>'AUDIT BPKP',
            'keterangan'=>'',
            'view'=>'parameterprofile::view_bind.bumdam.bpkp'
        ],
        [
            'name'=>'AUDIT BPKP 1 TAHUN SEBELUMYA',
            'keterangan'=>'',
            'view'=>'parameterprofile::view_bind.bumdam.bpkp_min_1'
        ],


    ]
   
];