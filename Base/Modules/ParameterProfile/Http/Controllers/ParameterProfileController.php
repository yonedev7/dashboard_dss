<?php

namespace Modules\ParameterProfile\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use Alert;
use Auth;
use Validator;
use JamesDordoy\LaravelVueDatatable\Http\Resources\DataTableCollectionResource;
class ParameterProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index($tahun,Request $request)
    {
        $page_meta=['title'=>'Parameter Profile','keterangan'=>''];
        return view('parameterprofile::index')->with(['page_meta'=>$page_meta]);
    }

    public function load_table($tahun,Request $request){
       

        $data=DB::table('master.dataset')
        ->selectRaw("id,".'"table"'.",name,'' as aksi");
        $data=$data->orderBy('name','asc');
        $data=$data->get();
       
        return $data;
    }
    public function load_field($tahun,Request $request){
        $data=DB::table('master.dataset_data as d')
        ->join('master.data as md','md.id','=','d.id_data')
        ->where('d.id_dataset',$request->dataset)
        ->selectRaw("d.id,md.name,d.field_name");
        $data=$data->orderBy('name','asc');
        $data=$data->get();
       
        return $data;
    

    }

    public function load_view($tahun,Request $request){
        if($request->type==0){
            return config('parameterprofile.view_list.PEMDA')??[];
        }else{
            return config('parameterprofile.view_list.BUMDAM')??[];
        }
    }

    public function load($tahun,Request $request){
        $paginate=20;
        $data=DB::table('dataset.params_bind as d')
        ->leftJoin('master.dataset as dts','dts.id','=','d.id_dataset')
        ->leftJoin('master.dataset_data as dtsd','dtsd.id','=','d.id_field')
        ->leftJoin('master.data as dt','dt.id','=','dtsd.id_data')
        ->selectRaw("d.*,dts.name as info_table,dt.name as info_field")
        ->orderBy('d.type','asc')->orderBy('name','asc')->paginate($paginate);
        return [
            'data'=>$data->items(),
            'last_page'=>$data->lastPage(),
            'current_page'=>$data->currentPage(),
            'paginate_count'=>$paginate
        ];


    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('parameterprofile::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store($tahun,Request $request)
    {
        if($request->type==0){
            $request['last']=$request['last']=='on'?true:false;
            $valid=Validator::make($request->all(),[
                'id_dataset'=>'required|numeric|exists:dataset,id',
                'id_field'=>'required|numeric|exists:dataset_data,id',
                'name'=>'required|string|unique:params_bind,name',
                'keterangan'=>'nullable|string',
                'last'=>'boolean',
                'type_scope'=>'boolean'
            ]);   

            if($valid->fails()){
                Alert::error('',$valid->errors()->first());
                return back();
            }

            $data=[
                'name'=>str_replace(' ','_',strtolower($request->name)),
                'id_dataset'=>$request->id_dataset,
                'id_field'=>$request->id_field,
                'type'=>$request->type,
                'type_scope'=>$request->type_scope,
                'keterangan'=>$request->keterangan,
                'last'=>$request->last,
            ];
        }elseif($request->type==1){
            $request['last']=$request['last']=='on'?true:false;

            $valid=Validator::make($request->all(),[
                'table'=>'required|string',
                'field'=>'required|string',
                'name'=>'required|string|unique:params_bind,name',
                'keterangan'=>'nullable|string',
                'last'=>'boolean',
                'type_scope'=>'boolean'
            ]);   

            if($valid->fails()){
                Alert::error('',$valid->errors()->first());
                return back();
            }

            $data=[
                'name'=>str_replace(' ','_',strtolower($request->name)),
                'table'=>$request->table,
                'field'=>$request->field,
                'type'=>$request->type,
                'id_dataset'=>null,
                'id_field'=>null,
                'bind'=>'[]',
                'type_scope'=>$request->type_scope,
                'keterangan'=>$request->keterangan,
                'last'=>$request->last,
            ];
        }elseif($request->type==2){

            $valid=Validator::make($request->all(),[
                'bind'=>'required|string',
                'name'=>'required|string|unique:params_bind,name',
                'keterangan'=>'nullable|string',
            ]);   

            if($valid->fails()){
                Alert::error('',$valid->errors()->first());
                return back();
            }

            $data=[
                'name'=>str_replace(' ','_',strtolower($request->name)),
                'bind'=>$request->bind,
                'keterangan'=>$request->keterangan,
                'type'=>$request->type,
                'type_scope'=>$request->type_scope,
                'last'=>false,
            ];

        }


        $data=DB::table('dataset.params_bind')->insert($data);
        Alert::success('','Berhasil Menambahkan data');
        return back();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('parameterprofile::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('parameterprofile::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update($tahun,$id,Request $request)
    {
        if($request->type==0){
            $request['last']=$request['last']=='on'?true:false;
            $valid=Validator::make($request->all(),[
                'id_dataset'=>'required|numeric|exists:dataset,id',
                'id_field'=>'required|numeric|exists:dataset_data,id',
                'name'=>'required|string|unique:params_bind,name,'.$id,
                'keterangan'=>'nullable|string',
                'last'=>'boolean',
                'type_scope'=>'boolean'
            ]);   

            if($valid->fails()){
                Alert::error('',$valid->errors()->first());
                return back();
            }

            $data=[
                'name'=>str_replace(' ','_',strtolower($request->name)),
                'id_dataset'=>$request->id_dataset,
                'id_field'=>$request->id_field,
                'type'=>$request->type,
                'field'=>null,
                'table'=>null,
                'bind'=>'[]',
                'type_scope'=>$request->type_scope,
                'keterangan'=>$request->keterangan,
                'last'=>$request->last,
            ];
        }else if($request->type==1){
            $request['last']=$request['last']=='on'?true:false;

            $valid=Validator::make($request->all(),[
                'table'=>'required|string',
                'field'=>'required|string',
                'name'=>'required|string|unique:params_bind,name,'.$id,
                'keterangan'=>'nullable|string',
                'last'=>'boolean',
                'type_scope'=>'boolean'
            ]);   

            if($valid->fails()){
                Alert::error('',$valid->errors()->first());
                return back();
            }

            $data=[
                'name'=>str_replace(' ','_',strtolower($request->name)),
                'table'=>$request->table,
                'field'=>$request->field,
                'type'=>$request->type,
                'id_dataset'=>null,
                'id_field'=>null,
                'bind'=>'[]',
                'type_scope'=>$request->type_scope,
                'keterangan'=>$request->keterangan,
                'last'=>$request->last,
            ];


        }else if($request->type==2){

            $valid=Validator::make($request->all(),[
                'bind'=>'required|string',
                'name'=>'required|string|unique:params_bind,name,'.$id,
                'keterangan'=>'nullable|string',
            ]);   

            if($valid->fails()){
                Alert::error('',$valid->errors()->first());
                return back();
            }

            $data=[
                'name'=>str_replace(' ','_',strtolower($request->name)),
                'bind'=>$request->bind,
                'table'=>null,
                'field'=>null,
                'type'=>$request->type,
                'type_scope'=>$request->type_scope,
                'id_dataset'=>null,
                'id_field'=>null,
                'keterangan'=>$request->keterangan,
                'last'=>false,
            ];

        }

        $data=DB::table('dataset.params_bind')->where('id',$id)->update($data);
        Alert::success('','Berhasil Memperbarui data');
        return back();


        
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function delete($tahun,$id)
    {

       $d= DB::table('dataset.params_bind')->where('id',$id)->delete();
       if($d){
           Alert::success('','Berhasil Menghapus Parameter');
       }

       return back();
        //
    }
}
