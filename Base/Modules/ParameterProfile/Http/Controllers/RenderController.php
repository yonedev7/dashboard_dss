<?php

namespace Modules\ParameterProfile\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use Carbon\Carbon;
class RenderController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

     public static function render($params,$tahun,$pemda){
        $result=[];
        $pro_params=DB::table('dataset.params_bind as pr')->whereIn('pr.name',$params)
        ->leftJoin('master.dataset as dts','dts.id','=','pr.id_dataset')
        ->leftJoin('master.dataset_data as dtsdt','dtsdt.id','=','pr.id_field')
        ->leftJoin('master.data as dt','dt.id','=','dtsdt.id_data')
        ->selectRaw("
        pr.*,
        (case when dts.id IS NOT NULL then concat('dataset.',dts.table) else pr.table end ) as table_target,
        (case when dtsdt.id IS NOT NULL then dtsdt.field_name else pr.field end ) as field_target,
        '[]' as bind_view_nilai,
         dt.tipe_nilai as tipe_nilai,
         dt.satuan as satuan_nilai
        ")
        ->get();

        foreach($pro_params as $key=>$d){
            if($d->type==0){
               if($d->last){
                    $result['[dss_params_'.$d->name.']']=DB::table(DB::raw("".$d->table_target." as tg_tb"))
                    ->selectRaw("max(".$d->field_target.") as tg_fld,tg_tb.tahun as tg_tahun")
                    ->where('tg_tb.kodepemda',$pemda['kodepemda'])
                    ->where('tg_tb.tw',4)
                    ->where('tg_tb.status',1)
                    ->groupBy('tg_tb.tahun','desc')
                    ->first();
                    if($result['[dss_params_'.$d->name.']']){
                    $result['[dss_params_'.$d->name.']']= [$result['[dss_params_'.$d->name.']']->tg_fld,$result['[dss_params_'.$d->name.']']->tg_tahun];

                    }else{
                        $result['[dss_params_'.$d->name.']']=[($d->tipe_nilai=='numeric'?0:''),($tahun)];

                    }

               }else{
                    $result['[dss_params_'.$d->name.']']=DB::table(DB::raw("".$d->table_target." as tg_tb"))
                    ->selectRaw("(".$d->field_target.") as tg_fld")
                    ->where('tg_tb.kodepemda',$pemda['kodepemda'])
                    ->where('tg_tb.tw',4)
                    ->where('tg_tb.tahun',$tahun)
                    ->where('tg_tb.status',1)
                    ->first();

                    if($result['[dss_params_'.$d->name.']']){
                        $result['[dss_params_'.$d->name.']']= [$result['[dss_params_'.$d->name.']']->tg_fld,$tahun];
                    }else{
                        $result['[dss_params_'.$d->name.']']=[($d->tipe_nilai=='numeric'?0:''),$tahun];
                    }
               }

            //    bind View
            $bind_nilai=json_decode($d->bind_view_nilai);
            if(count($bind_nilai)){
                $index=-1;
                do{
                    $index++;

                    $logic=str_replace('val',$result['[dss_params_'.$d->name.']'][0],$bind_nilai[$index]['logic']);
                    $eval=eval($logic);
                }while($eval OR (!isset($bind_nilai[$index+1])));

                $result['[dss_params_'.$d->name.']']=((!in_array($d->satuan_nilai,['','-']))?$d->satuan_nilai.' ':'').($bind_nilai[$index]['tag']??'').' ('.$result['[dss_params_'.$d->name.']'][1].')';

            }elseif($d->tipe_nilai=='numeric'){ 
                $result['[dss_params_'.$d->name.']']=((!in_array($d->satuan_nilai,['','-']))?$d->satuan_nilai.' ':'').number_format((float)$result['[dss_params_'.$d->name.']'][0],2,'.',',').' ('.$result['[dss_params_'.$d->name.']'][1].')';
            }else{
                $result['[dss_params_'.$d->name.']']=((!in_array($d->satuan_nilai,['','-']))?$d->satuan_nilai.' ':'').$result['[dss_params_'.$d->name.']'][0].' ('.$result['[dss_params_'.$d->name.']'][1].')';
            }


               

            }elseif($d->type==1){
                if($d->last){
                    $result['[dss_params_'.$d->name.']']=DB::table(DB::raw("".$d->table_target." as tg_tb"))
                    ->selectRaw("max(".$d->field_target.") as tg_fld")
                    ->where('tg_tb.kodepemda',$pemda['kodepemda'])
                    ->where('tg_tb.tw',4)
                    ->where('tg_tb.status',1)
                    ->groupBy('tg_tb.tahun','desc')
                    ->first();
                    if($result['[dss_params_'.$d->name.']']){
                    $result['[dss_params_'.$d->name.']']= $result['[dss_params_'.$d->name.']']->tg_fld;
                    }

               }else{
                $result['[dss_params_'.$d->name.']']=DB::table(DB::raw("".$d->table_target." as tg_tb"))
                    ->selectRaw("(".$d->field_target.") as tg_fld")
                    ->where('tg_tb.kodepemda',$pemda['kodepemda'])
                    ->where('tg_tb.tw',4)
                    ->where('tg_tb.tahun',$tahun)
                    ->where('tg_tb.status',1)
                    ->first();
                    if($result['[dss_params_'.$d->name.']']){
                        $result['[dss_params_'.$d->name.']']= $result['[dss_params_'.$d->name.']']->tg_fld;
                    }
               }
            }else{
                $view_list=json_decode($d->bind,true);
            
                $result['[dss_params_'.$d->name.']']='';
                foreach($view_list as $vk=>$v){
                    $result['[dss_params_'.$d->name.']'].=view($v['view'])
                    ->with(['tahun'=>$tahun,'pemda'=>$pemda])
                    ->render();
                }

            }
        }

        foreach($params as $pk=>$p){
            if(!isset($result['[dss_params_'.$p.']'])){
                switch($p){
                    case "tahun":
                        $result['[dss_params_'.$p.']']=$tahun;
                    break;
                    case "tanggal":
                        $result['[dss_params_'.$p.']']=Carbon::now()->format('d F Y');
                    break;
                    case "namapemda":
                        $result['[dss_params_'.$p.']']=$pemda['nama_pemda'];
                    break;
                    case "kodepemda":
                        $result['[dss_params_'.$p.']']=(int)$pemda['kodepemda'];
                    break;
                    case "tipe_bantuan":
                        $result['[dss_params_'.$p.']']=$pemda['tipe_bantuan'];
                    break;
                    case "tahun_proyek":
                        $result['[dss_params_'.$p.']']=$pemda['tahun_proyek']>1?$pemda['tahun_proyek']:'';
                    break;
                    default:
                    $result['[dss_params_'.$p.']']='';
                    break;
                }

            }
        }

        return $result;

     }
   
}
