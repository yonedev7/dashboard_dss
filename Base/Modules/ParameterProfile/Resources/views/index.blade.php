@extends('adminlte::page')


@section('content')
<script>
    var page_meta=<?=json_encode($page_meta)?>;
</script>
<div class="parallax"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
    <div class="container">
        <section >
            <h3><b>{{$page_meta['title']}}</b></h3>
            <p><b>{!!$page_meta['keterangan']!!}</b></p>
           
        </section>
      
    </div>
   
</div>
<div class="p-3" id="app">
    <div class="btn-group mb-2">
        <button class="btn btn-success btn-sm" @click="showFormAdd"><i class="fa fa-plus"></i> Tambah</button>
    </div>
    <hr>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover" id="table-data">
            <thead class="thead-dark">
                <tr>
                    <th>Aksi</th>
                    <th>Parameter</th>
                    <th>keterangan</th>
                    <th>Tipe</th>
                    <th>Table</th>
                    <th>Field</th>
                    <th>Last Data</th>
                    <th>Bind View</th>
                </tr>
            </thead>
        </table>
    </div>
    <div class="modal fade" tabindex="-1" id="modal-delete" role="dialog">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <form v-bind:action="modal_delete.action_url" method="post">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title">Hapus Parameter </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                    <p>Hapus Parameter [dss_params_@{{modal_delete.name}}]</p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit"  class="btn btn-danger">Hapus</button>
                        
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" id="modal-form-add" role="dialog">
        <div class="modal-dialog  modal-full" role="document">
            <div class="modal-content">
                <form v-bind:action="form_add.action_url" method="post">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title">@{{form_add.title}} </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="d-flex row align-items-stretch">
                            <div class="flex-column col-6">
                                <div class="form-group">
                                    <label for="">Params Name @{{form_add.name?'[dss_params_'+form_add.name+']':''}}</label>
                                    <input type="text" class="form-control" v-model="form_add.name" required name="name">
                                </div>
                                <div class="form-group">
                                    <label for="">Tipe</label>
                                    <select name="type_scope" class="form-control" v-model="form_add.type_scope" required id="">
                                        <option value="0">PEMDA</option>
                                        <option value="1">BUMDAM</option>
                                    </select>
                                    <input type="hidden" v-model="form_add.type" name="type">
                                </div>
                                <nav>
                                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                        <a v-bind:class="'nav-item nav-link '+(form_add.type==0?'active':'') " @click="form_add.type=0" >Dataset</a>
                                        <a v-bind:class="'nav-item nav-link '+(form_add.type==1?'active':'') "  @click="form_add.type=1">Manual</a>
                                        <a v-bind:class="'nav-item nav-link '+(form_add.type==2?'active':'') "  @click="form_add.type=2">Template</a>
                                    </div>
                                    </nav>
                                    <div class="tab-content" id="nav-tabContent">
                                    <div v-bind:class="'tab-pane fade   pt-3 '+(form_add.type==0?'show active':'')">
                                        <div class="form-group">
                                            <label for="">Table</label>
                                            <button type="button" class="btn btn-secondary btn-sm " style="width:100%; min-height:40px;" @click="loadTable">@{{form_add.info_table}}</button>
                                            <input type="hidden" name="id_dataset" v-model="form_add.id_dataset">    
                                        </div>
                                        <div class="form-group" v-if="form_add.info_table">
                                            <label for="">Field</label>
                                            <button type="button" class="btn btn-secondary btn-sm " style="width:100%; min-height:40px;" @click="loadField">@{{form_add.info_field}}</button>
                                            <input type="hidden"  name="id_field" v-model="form_add.id_field" class="form-control">    
                                        </div>
                                        <div class="form-group">
                                            <label for="">Gunakan Data Terahir</label>
                                            <p><input type="checkbox" name="last" v-model="form_add.last"> @{{form_add.last?'Ya':'Tidak'}}</p>
                                        </div>
                                    </div>
                                    <div v-bind:class="'tab-pane fade   pt-3 '+(form_add.type==1?'show active':'')">
                                        <div class="form-group">
                                            <label for="">Table</label>
                                            <input type="text" name="table" v-model="form_add.table" class="form-control">    

                                        </div>
                                        <div class="form-group">
                                            <label for="">Field</label>
                                            <input type="text"  name="field" v-model="form_add.field" class="form-control">    
                                        </div>
                                        <div class="form-group">
                                            <label for="">Gunakan Data Terahir</label>
                                            <p><input type="checkbox" name="last" v-model="form_add.last"> @{{form_add.last?'Ya':'Tidak'}}</p>
                                        </div>
                                    </div>
                                    <div v-bind:class="'tab-pane fade pt-3 '+(form_add.type==2?'show active':'')" >
                                        <div class="form-group">
                                            <label for="">Template</label>
                                            <table class="table table-bordered">
                                                <thead class="thead-dark">
                                                <tr>
                                                    <th>Aksi</th>
                                                    <th>Name</th>
                                                    <th>View</th>
                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                    <tr v-for="item,k in form_add.bind">
                                                        <td>
                                                            <button type="button" class="btn btn-danger btn-sm" @click="deleteView(k)"><i class="fa fa-trash"></i></button>
                                                        </td>
                                                        <td>
                                                            @{{item.name}}
                                                        </td>
                                                        <td>
                                                            @{{item.view}}
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                            </table>
                                            <input type="hidden" name="bind" v-bind:value="JSON.stringify(form_add.bind)">    

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="">Keterangan</label>
                                        <textarea name="keterangan" class="form-control" id="" cols="30" rows="10" v-model="form_add.keterangan"></textarea>
                                    </div>
                            </div>
                            <div class="flex-column col-6">
                                <p><b>@{{form_add.title_load}}</b></p>
                                <div class="p-2 text-center" v-if="form_add.loading">
                                    <div class="spinner-border text-primary" role="status">
                                    <span class="sr-only">Loading...</span>
                                    </div>
                                    <div class="spinner-border text-secondary" role="status">
                                    <span class="sr-only">Loading...</span>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                <table class="table table-bordered" id="table-list-resources">
                                    <thead class="thead-dark">
                                    
                                        </thead>
                                        <tbody>
                                    </tbody>
                                    </table>
                                </div>
                            </div>
                        
                        </div>
                    
                    </div>
                    <div class="modal-footer">
                        <button type="submit"  class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
        </div>
</div>
@stop

@section('js')
<script>
    var app=new Vue({
        el:'#app',
        data:{
            data:[],
            ref_datatable:null,
            curent_page:1,
            modal_delete:{
                action_url:null,
                name:null
            },
            form_add:{
                action_url:'',
                title:'',
                title_load:'',
                id:'',
                name:null,
                keterangan:null,
                type_scope:0,
                type:0,
                field:null,
                table:null,
                bind:[],
                last:false,
                info_table:'',
                info_field:'',
                info_bind_view:'',
                is_bind:false,
                id_dataset:null,
                id_field:null,
                table_list:{
                    columns:[
                        {
                            title:'Aksi',
                            data:'id',
                            type:'html',
                            render:(data,type,row,index)=>{
                                return ('<button type="button" onclick="app.addTable(\''+row.name+'\','+data+')" class="btn btn-success btn-sm"><i class="fa fa-plus"></button>');
                            }
                        },
                        {
                            title:'Dataset',
                            data:'name',
                        }
                    ],
                    data:[]
                },
                field_list:{
                    columns:[
                        {
                            title:'Aksi',
                            data:'id',
                            type:'html',
                            render:(data,type,row,index)=>{
                               return ('<button type="button" onclick="app.addField(\''+row.name+'\','+data+')" class="btn btn-success btn-sm"><i class="fa fa-plus"></button>');
                            }
                        },
                        {
                            title:'Field',
                            data:'name',
                        }
                    ],
                    data:[]
                },
                view_list:{
                    columns:[
                        {
                            title:'Aksi',
                            type:'html',
                            data:'name',
                            render:(data,type,row,index)=>{
                                return ('<button type="button" onclick="app.addView(`xx`,`id_x`)" class="btn btn-success btn-sm"><i class="fa fa-plus"></button>').replace('id_x',row.view).replace(`xx`,row.name);
                            }
                        },
                        {
                            title:'View',
                            data:'name',
                        },
                        {
                            title:'Keterangan',
                            data:'keterangan',
                        }
                    ],
                    data:[]
                },
                form_columns:[],
                ref_table:null,
                loading:false

            }
        },
        watch:{
            "form_add.name":function(val,old){
                if((val).includes(' ')){
                    this.form_add.name=val.replace(/ /g,'_');
                }
                this.form_add.name=this.form_add.name.toLowerCase();
            },
            "form_add.is_bind":function(val,old){
                if(val){
                    this.form_add.table=null;
                    this.form_add.field=null;
                    this.form_add.last=false;

                }else{
                    this.form_add.bind=null;
                }
            },
            "form_add.type":function(val){
                if(val==2){
                    this.loadView();
                }
                else if(val==0){
                    this.loadTable();
                }else{
                    if(this.form_add.ref_table!=null){
                    this.form_add.ref_table.clear().draw();
                    this.form_add.ref_table.destroy();
                    this.form_add.ref_table=null;
                    $('#table-list-resources thead').html('');

                    }
                    
                }
                

            },
            "form_add.type_scope":function(val){
                if(this.form_add.type==2){
                    this.loadView();
                }
                this.form_add.bind=[];  
            }
        },
        methods:{
            deleteView:function(index){
                if(this.form_add.bind[index]!=undefined){
                    this.form_add.bind.splice(index,1);
                }
            },
            loadEdit:function(index){
                if(this.data[index]!=undefined){
                    var data=this.data[index];
                    this.form_add.id=data.id;
                    this.form_add.name=data.name;
                    this.form_add.type=data.type;
                    this.form_add.last=data.last?'on':'off';
                    this.form_add.type_scope=data.type_scope;
                    this.form_add.table=data.table;
                    this.form_add.field=data.field;
                    this.form_add.info_table=data.info_table;
                    this.form_add.info_field=data.info_field;
                    this.form_add.keterangan=data.keterangan;
                    this.form_add.id_dataset=data.id_dataset;
                    this.form_add.id_field=data.id_field;
                    this.form_add.bind=JSON.parse(data.bind);
                    this.form_add.title='Ubah Parameter';

                    this.form_add.action_url=('{{route('mod.parameter-profile.update',['tahun'=>$StahunRoute,'id'=>'xxxx'])}}').replace('xxxx',data.id);
                    $('#modal-form-add').modal();

                }
            },
            addField:function(name,id){
                this.form_add.info_field=name;
                this.form_add.id_field=id;
            },
            addView:function(name,id){
                var data={
                    name:name,
                    view:id
                };
                if(!this.form_add.bind.some(el=>el.view===id)){
                     this.form_add.bind.push(data);
                }
            },
            addTable:function(name,id){
                this.form_add.info_table=name;
                this.form_add.id_dataset=id;
                this.loadField();
            },
            loadTable:function(){
                self=this;
                self.form_add.title_load='List Dataset';
                if(this.form_add.ref_table!=null){
                    this.form_add.ref_table.clear().draw();
                    this.form_add.ref_table.destroy();
                    this.form_add.ref_table=null;
                    $('#table-list-resources thead').html('');

                }

                this.form_add.form_columns=this.form_add.table_list.columns;
                this.form_add.ref_table=$('#table-list-resources').DataTable({
                    columns:this.form_add.table_list.columns,
                    data:this.form_add.table_list.data
                });
                console.log('this');

            },
            loadField:function(){
                self=this;
                self.form_add.title_load='List Data Pada Dataset '+this.form_add.info_table;
                if(this.form_add.ref_table!=null){
                    this.form_add.ref_table.clear().draw();
                    this.form_add.ref_table.destroy();
                    this.form_add.ref_table=null;
                    $('#table-list-resources thead').html('');


                }

                if(this.form_add.id_dataset){
                    this.form_add.loading=true;
                    req_ajax.post('{{route('api-web-mod.parameter-profile.field_load',['tahun'=>$StahunRoute])}}',{
                        'dataset':this.form_add.id_dataset,
                    }).then(res=>{
                            if(self.form_add.ref_table!=null){
                                self.form_add.ref_table.clear().draw();
                                self.form_add.ref_table.destroy();
                                self.form_add.ref_table=null;
                                $('#table-list-resources thead').html('');

                            }
                            self.form_add.loading=false;
                            self.form_add.field_list.data=res.data;
                            self.form_add.ref_table=$('#table-list-resources').DataTable({
                            columns:self.form_add.field_list.columns,
                            data:self.form_add.field_list.data
                        });
                    })
                }else if(this.form_add.table){
                    req_ajax.post('{{route('api-web-mod.parameter-profile.field_load',['tahun'=>$StahunRoute])}}',{
                        'table':this.form_add.table,
                    }).then(res=>{
                            self.form_add.loading=false;
                            self.form_add.field_list.data=res.data;
                            self.form_add.ref_table=$('#table-list-resources').DataTable({
                            columns:self.form_add.field_list.columns,
                            data:self.form_add.field_list.data
                        });
                    })
                }
                

            },
            loadDelete:function(index){

                if(this.data[index]!=undefined){
                   var data=this.data[index];
                    this.modal_delete.action_url=('{{route('mod.parameter-profile.delete',['tahun'=>$StahunRoute,'id'=>'xxxx'])}}').replace('xxxx',data.id);
                    this.modal_delete.name=data.name;
                    $('#modal-delete').modal();
                }

            },
            loadView:function(){
                self=this;
                if(this.form_add.ref_table!=null){
                    this.form_add.ref_table.clear().draw();
                    this.form_add.ref_table.destroy();
                    this.form_add.ref_table=null;
                    $('#table-list-resources thead').html('');


                }
                this.form_add.loading=true;
                this.form_add.title_load='List Template Parameter '+(this.form_add.type_scope==0?'PEMDA':'BUMDAM');
                req_ajax.post('{{route('api-web-mod.parameter-profile.view_load',['tahun'=>$StahunRoute])}}',{
                    'type':this.form_add.type_scope
                }).then(res=>{
                    if(self.form_add.ref_table!=null){
                        self.form_add.ref_table.clear().draw();
                        self.form_add.ref_table.destroy();
                        self.form_add.ref_table=null;
                        $('#table-list-resources thead').html('');


                    }
                    self.form_add.view_list.data=res.data;
                    self.form_add.loading=false;
                    self.form_add.ref_table=$('#table-list-resources').DataTable({
                        columns:self.form_add.view_list.columns,
                        data:self.form_add.view_list.data||[]
                    });
                });
                
            },
            showFormAdd:function(){
                this.form_add.action_url='{{route('mod.parameter-profile.store',['tahun'=>$StahunRoute])}}';
                this.form_add.id=null;
                this.form_add.name=null;
                this.form_add.type=0;
                this.form_add.last='off';
                this.form_add.type_scope=0;
                this.form_add.table=null;
                this.form_add.field=null;
                this.form_add.info_table=null;
                this.form_add.info_field=null;
                this.form_add.keterangan=null;
                this.form_add.id_dataset=null;
                this.form_add.id_field=null;
                this.form_add.bind=[];
                this.form_add.title='Tambah Parameter';
                $('#modal-form-add').modal();
            },
            init:function(){
                this.curent_page=1;
                this.load();
            },
            load:function(){
                self=this;
                req_ajax.post('{{route('api-web-mod.parameter-profile.load',['tahun'=>$Stahun])}}').then(res=>{
                    res.data.data.forEach(el => {
                        self.data.push(el);
                    });

                    self.drawTable();
                    if(res.data.last_page<self.curent_page){
                        self.curent_page+=1;
                        self.load();
                    }
                })
            },
            drawTable:function(){
                if(this.ref_datatable){
                    this.ref_datatable.clear();
                    this.ref_datatable.rows.add(this.data);
                    this.ref_datatable.draw();
                }
            }
        },
        created(){
            self=this;
            this.init();
            this.form_add.loading=true;

            req_ajax.post('{{route('api-web-mod.parameter-profile.table_load',['tahun'=>$StahunRoute])}}').then(res=>{
                self.form_add.table_list.data=res.data;
                self.form_add.loading=false;

            });
           

            setTimeout((self) => {
                self.ref_datatable=$('#table-data').DataTable({
                    columns:[
                        {   type:'html',
                            render:(data,type,row,index)=>{
                                return '<div class="btn-group"><button onclick="app.loadEdit('+index.row+')" class="btn btn-success btn-sm"><i class="fa fa-arrow-right"></i></button>'+
                                 '<button  onclick=app.loadDelete('+index.row+') class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button></div>';

                            }
                        },
                        {data:'name',render:(data)=>{return '[dss_params_'+data+']';}},
                        {data:'keterangan'},
                        {data:'type',
                        render:function(data){
                            switch(data){
                                case 0:
                                    return 'Dataset';
                                break;
                                case 1:
                                    return 'Manual';
                                break;
                                case 2:
                                    return 'Template';
                                break;
                            }
                        }},
                        {data:'table',
                        render:function(data,type,row,index){
                            if(row.type==0){
                                return row.info_table;
                            }else{
                                return row.table;
                            }
                        }},
                        {data:'field',render:function(data,type,row,index){
                            if(row.type==0){
                                return row.info_field;
                            }else{
                                return row.field;
                            }
                        }},
                        {data:'last',render:(data,type,row)=>{
                            if(row.type!=2){
                                return data?'Active':'Non Active';
                                
                            }
                            return '';
                        }},
                        {data:'bind',type:'html',render:(data)=>{
                            data=JSON.parse(data).map(el=>{
                                return el.name+' ('+el.view+')';
                            });

                            return data.join(', ');

                        }}

                    ]
                });

                self.drawTable();
            }, 1000,self);
        }
    });    
</script>

@stop