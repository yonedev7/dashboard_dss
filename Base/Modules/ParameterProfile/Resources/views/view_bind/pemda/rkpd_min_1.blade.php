@php
    $data=DB::table('dataset.program_kegiatan_rkpd as rkpd')->where([
        'status'=>1,
        'tw'=>4,
        'tahun'=>$tahun-1,
        'kodepemda'=>$pemda['kodepemda'],
        'kodeprogram'=>'1.03.03'
    ])->get();
@endphp

<h4>RKPD {{$pemda['nama_pemda']}} TAHUN {{$tahun-1}}</h4>
<table class="table table-bordered table-striped">
    <thead class="thead-dark">
        <tr>
            <th>Kode SKPD</th>
            <th>Nama SKPD</th>
            <th>Kode Program</th>
            <th>Program</th>
            <th>Kode Kegiatan</th>
            <th>Kegiatan</th>
            <th>Kode Sub Kegiatan</th>
            <th>Sub Kegiatan</th>
            <th>Pagu</th>
        </tr>
        <tr>
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
            <th>5</th>
            <th>6</th>
            <th>7</th>
            <th>8</th>
            <th>9</th>


        </tr>

    </thead>
    <tbody>
        @foreach($data as $d)


        <tr>
            <td>{{$d->kodeskpd}}</td>
            <td>{{$d->skpd}}</td>
            <td>{{$d->kodeprogram}}</td>
            <td>{{$d->namaprogram}}</td>
            <td>{{$d->kodekegiatan}}</td>
            <td>{{$d->namakegiatan}}</td>
            <td>{{$d->kodesubkegiatan}}</td>
            <td>{{$d->namasubkegiatan}}</td>
            <td>Rp. {{number_format($d->pagu,2,'.',',')}}</td>

        </tr>
        @endforeach
    </tbody>
    <tfoot class="bg-dark">
        <tr>
            <th colspan="8">TOTAL PAGU AIR MINUM</th>
            <th>Rp. {{number_format(array_sum(array_map(function($el){return $el->pagu;},$data->toArray())),2,'.',',')}}</th>
        </tr>
    </tfoot>
</table>