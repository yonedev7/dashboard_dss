<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('mod/{tahun}/module-parameter-profile')->name('api-web-mod.parameter-profile')->middleware('auth:api')->group(function(){
    Route::post('/','ParameterProfileController@load')->name('.load');
    Route::post('/load-table-listed','ParameterProfileController@load_table')->name('.table_load');
    Route::post('/load-view-listed','ParameterProfileController@load_view')->name('.view_load');
    Route::post('/load-field-listed','ParameterProfileController@load_field')->name('.field_load');



});