<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('dash/{tahun}/sistem-informasi/module-parameter-profile')->name('mod.parameter-profile')->middleware(['auth:web','bindTahun'])->group(function() {
    Route::get('/', 'ParameterProfileController@index')->name('.index');
    Route::post('/store', 'ParameterProfileController@store')->name('.store');
    Route::post('/update/{id}', 'ParameterProfileController@update')->name('.update');
    Route::post('/delete/{id}', 'ParameterProfileController@delete')->name('.delete');



});
