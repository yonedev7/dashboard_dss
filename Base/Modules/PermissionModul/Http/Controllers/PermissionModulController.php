<?php

namespace Modules\PermissionModul\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Validator;
use Alert;
class PermissionModulController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {

        $data=Permission::all();
        $page_meta=[
            'title'=>'Permissions',
            'keterangan'=>''
        ];
        return view('permissionmodul::permissions.index')->with(['page_meta'=>$page_meta,'permissions'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create($tahun)
    {
        $page_meta=[
            'title'=>'Tambah Permissions',
            'keterangan'=>''
        ];
        return view('permissionmodul::permissions.create')->with(['page_meta'=>$page_meta]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store($tahun,Request $request)
    {
        //
        $valid=Validator::make($request->all(),[
            'name'=>'required|string|unique:permissions,name',
            'guard'=>'string|nullable|in:web,api'

        ]);

        if($valid->fails()){
            Alert::error('',$valid->errors()->first());
            return back()->withInput();
        }

       $per= Permission::create(['name'=>$request->name,'guard_name'=>$request->guard]);
       if($per){
        Alert::success('','Berahasil Membuat Permission Baru');
        return redirect()->route('mod.permission.permis.index',['tahun'=>$tahun]);
       }

     


    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('permissionmodul::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('permissionmodul::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
