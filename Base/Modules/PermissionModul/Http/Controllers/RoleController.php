<?php

namespace Modules\PermissionModul\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Validator;
use Alert;
class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $data=Role::with('permissions')->get();
        

        $page_meta=[
            'title'=>'Role',
            'keterangan'=>''
        ];

        return view('permissionmodul::roles.index')->with(['page_meta'=>$page_meta,'roles'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create($tahun)
    {
        $page_meta=[
            'title'=>'Tambah Role',
            'keterangan'=>''
        ];
        
        return view('permissionmodul::roles.create')->with(['page_meta'=>$page_meta]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store($tahun,Request $request)
    {
        //
        $valid=Validator::make($request->all(),[
            'name'=>'required|string|unique:roles,name',
            'guard'=>'required|string|in:web,api'
        ]);

        if($valid->fails()){
            Alert::error('',$valid->errors()->first());
            return back()->withInput();
        }

        $role = Role::create(['guard_name' => $request->guard, 'name' => $request->name]);

        Alert::success('','Berahasil Membuat Role Baru');
        return redirect()->route('mod.permission.role.index',['tahun'=>$tahun]);


    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('permissionmodul::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('permissionmodul::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update($tahun,$id,Request $request)
    {
        
        $valid=Validator::make($request->all(),[
            'name'=>'required|string|unique:roles,name,'.$id,
            'guard'=>'required|string|in:web,api'
        ]);

        if($valid->fails()){
            Alert::error('',$valid->errors()->first());
            return back()->withInput();
        }

        $role = Role::create(['guard_name' => $request->guard, 'name' => $request->name]);

        Alert::success('','Berahasil Membuat Role Baru');
        return redirect()->route('mod.permission.role.index',['tahun'=>$tahun]);

    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
