<?php

namespace Modules\PermissionModul\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Validator;
use Alert;
use Illuminate\Support\Facades\Hash;
use DB;
use Auth;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $data=User::with(['roles'])->get();
        foreach($data as $key=>$d){
            $data[$key]->permissions=$d->getPermissionsViaRoles();

        }
        $roles=Role::all();
        
    
       
        $page_meta=['title'=>'Users','keterangan'=>''];
        return view('permissionmodul::users.index')->with(['users'=>$data,'page_meta'=>$page_meta,'roles'=>$roles]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('permissionmodul::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store($tahun,Request $request)
    {
        //

        $valid=Validator::make($request->all(),[
            'name'=>'required|string',
            'email'=>'required|email|unique:users,email',
            'password'=>'required|string|min:8|confirmed'

        ]);

        if($valid->fails()){
            Alert::error('',$valid->errors()->first());
            return back()->withInput();
        }

        $role=[];

        if($request->role){
            $role=Role::whereIn('id',array_values($request->role))->get();
        }

      


        DB::beginTransaction();

        try{
            $user=User::create([
                'name'=>$request->name,
                'email'=>$request->email,
                'password'=>Hash::make($request->password),
            ]);
    
            if(count($role)){
                $user->syncRoles($role);
            }
            DB::commit();
        }catch(\Exection $e){
            DB::rollback();
            Alert::error('',$e->getMessage());
            return back()->withInput();
        }

        Alert::success('','Berhasil Menambah User');
        return back();
        

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('permissionmodul::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('permissionmodul::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request,$tahun, $id)
    {
        $re=array_keys($request->all());
        $user=Auth::User();
        $v=[];
        $data=[];

        if(in_array('password',$re)){
            $v['password']='required|string|min:8|confirmed';
             $data['password']=Hash::make($request->password);

        }
        if(in_array('email',$re)){
            $v['email']='required|email|unique:users,email,'.$id;
            $data['email']=($request->email);

        }
       
        if(in_array('name',$re)){
            $v['name']='required|string';
            $data['name']=($request->name);

        }

        $valid=Validator::make($request->all(),$v);
        
        if($valid->fails()){
            Alert::error('',$valid->errors()->first());
            return back()->withInput();
        }


        DB::beginTransaction();
        try{
        $user=User::find($id);
        $user->update($data);
        $role=[];
        if($request->role){
            $role=Role::whereIn('id',array_values($request->role))->get();
        }
        if(count($role)){
            $user->syncRoles($role);
        }
        DB::commit();
        }catch(\Exection $e){
            DB::rollback();
            Alert::error('',$e->getMessage());
            return back()->withInput();
        }

        Alert::success('','Berhasil Merubah User');
        return back();

        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
