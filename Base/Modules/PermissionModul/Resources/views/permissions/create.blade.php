@extends('adminlte::page')


@section('content')
<script>
    var page_meta=<?=json_encode($page_meta)?>;
</script>
<div class="parallax pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
    <div class="container">
        <section >
            <h3><b>{{$page_meta['title']}}</b></h3>
            <p><b>{!!$page_meta['keterangan']!!}</b></p>
        </section>
      
    </div>
  
</div>

<form action="{{route('mod.permission.permis.store',['tahun'=>$StahunRoute])}}" method="post">
    @csrf
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                    <label for="">Nama Permision</label>
                    <input type="text" name="name" class="form-control" required>    
                    </div>    
                </div>
            </div>    
        </div>
        <div class="card-footer">
            <button class="btn btn-success" type="submit">Tambah</button>  </div>
    </div>
   
    </form>

@stop