@extends('adminlte::page')


@section('content')
<script>
    var page_meta=<?=json_encode($page_meta)?>;
</script>
<div class="parallax pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
    <div class="container">
        <section >
            <h3><b>{{$page_meta['title']}}</b></h3>
            <p><b>{!!$page_meta['keterangan']!!}</b></p>
        </section>
      
    </div>
   
</div>

<div class="p-3" id="app">
    <div class="btn-group mb-1">
        <a href="{{route('mod.permission.role.create',['tahun'=>$StahunRoute])}}" class="btn btn-success">Tambah Role</a>
    </div>
    <div class="table-responsive bg-white pt-3">
        <table class="table table-bordered table-striped" id="table-data">
            <thead class="thead-dark">
            
                <tr>
                    <th>Aksi</th>
                    
                    <th>Nama</th>
                    <th>Permission</th>
                </tr>
            </thead>
        </table>    
    </div>
</div>

@stop

@section('js')
<script>
    var app=new Vue({
        el:'#app',
        data:{
            data:<?=json_encode($roles)?>,
            ref_table:null,

        },
        created:function(){
            var self=this;
            setTimeout((ref) => {
                ref.ref_table=$('#table-data').DataTable({
                    data:ref.data,
                    columns:[
                        {
                            render:function(data,type,row,index){
                                return `
                                    <div class="btn-group">
                                        <button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                        <button class="btn btn-warning btn-sm"><i class="fa fa-pen"></i></button>
                                        <button class="btn btn-primary btn-sm"><i class="fa fa-shield"></i> Permissions</button>
                                    </di>
                                `;
                            }
                        },
                        {
                            data:'name'
                        },
                        {
                            data:'permissions',
                            render:function(data,type,row,index){
                                return ((data||[]).map((el,key)=>{
                                    return '<button  dissable class="btn disable btn-primary btn-sm mb-1"> '+el.name+'</button>';
                                }).join('  '));
                            }
                        },
                       
                       
                    ]
                })

            }, 300,self);
        }
    })
    
    </script>


@stop