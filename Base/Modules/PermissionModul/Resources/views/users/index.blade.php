@extends('adminlte::page')


@section('content')
<script>
    var page_meta=<?=json_encode($page_meta)?>;
</script>
<div class="parallax pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
    <div class="container">
        <section >
            <h3><b>{{$page_meta['title']}}</b></h3>
            <p><b>{!!$page_meta['keterangan']!!}</b></p>
        </section>
      
    </div>
   
</div>

<div class="p-3 bg-white" id="app">
    <div class="btn-group mb-3">
        <button class="btn btn-success btn-sm" @click="showFormAdd()"><i class="fa fa-pluss"></i> Tambah User</button>
    </div>
    <div class="table-responsive  pt-3">
        <table class="table table-bordered table-striped" id="table-data">
            <thead class="thead-dark">
            
                <tr>
                    <th>Aksi</th>
                    <th>Group</th>
                    <th>Permission</th>
                    <th>Nama</th>
                    <th>Email</th>
                </tr>
            </thead>
        </table>    
    </div>

    <div class="modal fade" id="modal-add" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@{{form.title_form}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <form v-bind:action="form.action" method="post">
                @csrf
                <div class="modal-body">
                <div class="form-group"  v-if="form.content==1||form.content==3">
                    <label for="">Nama</label>
                    <input type="text" class="form-control" required name="name" v-model="form.name">
                </div>
                <div class="form-group" v-if="form.content==1||form.content==3">
                    <label for="">Email</label>
                    <input type="email" class="form-control" required name="email" v-model="form.email">
                </div>
                <div class="form-group" v-if="form.content==1||form.content==2">
                    <label for="">Password</label>
                    <input type="password" min="8" class="form-control" required name="password" v-model="form.password">
                    <small v-if="(form.password||'').length<8 " class="text-dark bg-warning rounded p-1 mt-2">Minimal Password 8 Karakter</small>
                </div>
                <div class="form-group" v-if="form.content==1||form.content==2">
                    <label for="">Password Konfirmasi</label>
                    <input type="password" min="8" class="form-control" required name="password_confirmation"  v-model="form.password_confirmation">
                    <small v-if="form.password!=form.password_confirmation" class="text-dark bg-warning rounded p-1 mt-2">Password Tidak Sama</small>
               
                </div>
                <div class="form-group"  v-if="form.content==1||form.content==3">
                    <label for="">Role</label>

                    <input v-for="r,k in form.role" type="hidden" v-bind:name="'role['+k+']'" v-bind:value="r">

                    <v-select :options="roles"  label="name" multiple :reduce="l => l.id" v-model="form.role"></v-select>
                </div>

            </div>
            <div class="modal-footer">
                <button type="submit" v-if="form.accept" class="btn btn-primary">Kirim</button>
            </div>    
                </form>
            </div>
        </div>
        </div>
</div>

@stop

@section('js')
<script>
    var app=new Vue({
        el:'#app',
        data:{
            data:<?=json_encode($users)?>,
            ref_table:null,
            roles:<?=json_encode($roles)?>,
            ref_time:null,
            form:{
                action:'',
                content:1,
                title_form:'',
                accept:false,
                name:'',
                email:'',
                password:'',
                password_confirmation:'',
                role:[]

            }

        },
        computed:{
            form_role:function(){
                return JSON.stringify(this.form.role);
            }
        },  
        watch:{
            "form":{
                deep:true,
                handler:function(data){
                   if(this.ref_time){
                       clearTimeout(this.ref_time);
                       this.ref_time=null;
                   }

                   this.ref_time=setTimeout(function(ref){
                        ref.checkFormAccept();
                   },300,this);
                }
            }
        },
        methods:{
            checkFormAccept:function(){
                var data=this.form;

                if(this.form.content==1){
                    //  console.log('new',(data.password==data.password_confirmation) , (data.role.length) , data.name , data.email , ((data.password||'').length>=8));

                        if((data.password==data.password_confirmation) && (data.role.length) && data.name && data.email && ((data.password||'').length>=8)){
                            this.form.accept=true;
                        }else{
                            this.form.accept=false;
                        }
                   }else if(this.form.content==2){
                        // console.log('password', (data.password==data.password_confirmation) && ((data.password||'').length>=8));

                        if((data.password==data.password_confirmation) && ((data.password||'').length>=8)){
                            this.form.accept=true;
                        }else{
                            this.form.accept=false;
                        }

                   }else if(this.form.content==3){
                    // console.log('edit', (data.role.length) , data.name , data.email );

                        if( (data.role.length) && data.name && data.email ){
                            this.form.accept=true;
                        }else{
                            this.form.accept=false;
                        }
                   }
            },
            showFormAdd:function(){
                this.form.name='';
                this.form.content=1;
                this.form.email='';
                this.form.password='';
                this.form.password_confirmation='';
                this.form.role=[];
                this.form.accept=false;
                this.form.action='{{route('mod.permission.user.store',['tahun'=>$StahunRoute])}}';
                this.form.title_form='Tambah User';
                $('#modal-add').modal();
            },
          
            showEditPassword:function(index){
                var data=this.data[index];

                this.form.content=2;
                this.form.name='';
                this.form.email='';
                this.form.password='';
                this.form.password_confirmation='';
                this.form.role=[];
                this.form.accept=false;
                this.form.action=('{{route('mod.permission.user.update',['tahun'=>$StahunRoute,'id'=>'xxxx'])}}').replace('xxxx',data.id);
                this.form.title_form='Edit Password ('+data.name+')';
                $('#modal-add').modal();
            },
            showEdit:function(index){
                var data=this.data[index];
                this.form.content=3;
                this.form.name=data.name;
                this.form.email=data.email;
                this.form.password='';
                this.form.password_confirmation='';
                this.form.role=data.roles.map(el=>{return el.id});
                this.form.accept=false;
                this.form.action=('{{route('mod.permission.user.update',['tahun'=>$StahunRoute,'id'=>'xxxx'])}}').replace('xxxx',data.id);
                this.form.title_form='Edit User ('+data.name+')';
                $('#modal-add').modal();
            },

         },
        created:function(){
            var self=this;
            setTimeout((ref) => {
                ref.ref_table=$('#table-data').DataTable({
                    data:ref.data,
                    columns:[
                        {
                            render:function(data,type,row,index){
                                return `
                                    <div class="btn-group">
                                        <button onclick="app.showEdit(`+index.row+`)"  class="btn btn-warning btn-sm"><i class="fa fa-pen"></i></button>
                                        <button onclick="app.showEditPassword(`+index.row+`)"  class="btn btn-primary btn-sm"><i class="fa fa-pen"></i> Password</button>
                                    </di>
                                `;
                            }
                        },
                        {
                            data:'roles',
                            render:function(data,type,row,index){
                                return ((data||[]).map(el=>{
                                    return el.name;
                                }).join(' , '));
                            }
                        },
                        {
                            data:'permissions',
                            render:function(data,type,row,index){
                                return ((data||[]).map(el=>{
                                    return el.name;
                                }).join(' , '));
                            }
                        },
                        {
                            data:'name'
                        },
                        {
                            data:'email'
                        }
                    ]
                })

            }, 300,self);
        }
    })
    
    </script>


@stop