<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('dash/{tahun}/sistem-informasi/module-permission')->name('mod.permission')->middleware(['auth:web','bindTahun','can:init-superadmin'])->group(function() {
    Route::prefix('/')->name('.user')->group(function() {
        Route::get('/','UserController@index')->name('.index');
        Route::post('/store','UserController@store')->name('.store');
        Route::post('/update/{id}','UserController@update')->name('.update');
        Route::post('/password-update/{id}','UserController@password_update')->name('.password_update');

    });

  
    Route::prefix('roles')->name('.role')->group(function() {
        Route::get('/','RoleController@index')->name('.index');
        Route::get('/create','RoleController@create')->name('.create');
        Route::post('/store','RoleController@store')->name('.store');
        Route::post('/update/{id}','UserContRoleControllerroller@update')->name('.update');
        Route::post('/delete/{id}','UserContRoleControllerroller@delete')->name('.delete');


    });

    Route::prefix('permissions')->name('.permis')->group(function() {
        Route::get('/','PermissionModulController@index')->name('.index');
        Route::get('/create','PermissionModulController@create')->name('.create');
        Route::post('/store','PermissionModulController@store')->name('.store');
        Route::post('/update/{id}','PermissionModulController@update')->name('.update');
        Route::post('/delete/{id}','PermissionModulController@delete')->name('.delete');
 
    });

});


