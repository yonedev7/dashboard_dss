<?php
return [
    // [   'type'=>'html',
    //     'title'=>'H1',
    //     'template'=>'
    //         <h1 v-html="inner"></h1>
    //     ',
    //     'component'=>'b-card',
    //     'child'=>[],
    //     'data'=>[
    //         'inner'=>[
    //         ],
    //         'params'=>[
    //             'title'=>[
    //                 'val'=>'title',
    //                 'type'=>'input-string'
    //             ],
    //             'img-src'=>[
    //                 'val'=>'',
    //                 'type'=>'input-string'
    //             ],
    //             'class'=>[
    //                 'val'=>'',
    //                 'type'=>'input-string'
    //             ]
    //         ],
    //     ],
        
    // ],
    [   'type'=>'component',
        'title'=>'Card',
        'template'=>'
        <b-card :title="t_target.title.val" :img-src="t_target.img_src.val">
             <b-card-text>
             {{t_target.content.val}}
             </b-card-text>
        </b-card>
        ',
        'component'=>'b-card',
        'child'=>[],
        'data'=>[
            'inner'=>[
            ],
            'params'=>[
                'title'=>[
                    'val'=>'title',
                    'type'=>'input-string'
                ],
                'content'=>[
                    'val'=>'title',
                    'type'=>'input-string'
                ],
                'img_src'=>[
                    'val'=>'http://localhost/SERVER3/dashboard_dss/assets/img/brand/logo.png',
                    'type'=>'input-string'
                ],
                'class'=>[
                    'val'=>'',
                    'type'=>'input-string'
                ]
            ],
        ],
        
    ]
];