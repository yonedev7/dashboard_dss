<?php

namespace Modules\ProfileModul\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;

class ProfileModulController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

    public function test()
    {

        return view('profilemodul::admin.test')->with(['components'=>config('profilemodul')['component_list']]);
    }
    public function index($tahun,Request $request)
    {
       $data= DB::table('master.master_pemda as mp')
        ->selectRaw('mp.*,p.publish,p.publish_date,p.file')
        ->join('dataset.profile as p','p.kodepemda','=','mp.kodepemda')
        ->where('mp.nama_pemda','ilike','%'.$request->q.'%')
        ->where([
            'p.tahun'=>$tahun,
            'p.publish'=>1
        ])->paginate(10);
        return view('profilemodul::index')->with('collection',$data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('profilemodul::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('profilemodul::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('profilemodul::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
