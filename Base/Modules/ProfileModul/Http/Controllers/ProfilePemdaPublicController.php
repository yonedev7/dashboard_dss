<?php

namespace Modules\ProfileModul\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;

class ProfilePemdaPublicController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

    public function getProfile($tahun,$kodepemda,Request $request){
        $dataset=[];
        if($request->id_dataset){
            $id_dataset=explode(',',$request->id_dataset);
            $dataset=DB::table('master.dataset')->selectRaw('id,name')->whereIn('id',$id_dataset)->orderBy('id_menu','asc')->orderBy('index','asc')->get();
        }

        foreach($dataset as $k=>$d){
            $dataset[$k]->columns=[];
            $dataset[$k]->data=[];
            $dataset[$k]->status_data=[];

        }

        $pemda=DB::table('master.master_pemda')->where('kodepemda',$kodepemda)->first();

        return view('profilemodul::admin.pemda.pdf')->with(['pemda'=>$pemda,'dataset'=>$dataset]);
    } 

    public function index()
    {
        // return abort(503);

        return view('profilemodul::pemda.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('profilemodul::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('profilemodul::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('profilemodul::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
