@extends('datasetmodul::layouts.master')


@section('content')
<style>
    tr th,tr td{
        font-size:10px;
    }
</style>
<div class="" id="app">
    <table class="table">
        <thead>
            <tr>
                <th>
                    <div id="header-dss" class="text-uppercase align-self-center">
                        <span><img src="{{asset('assets/img/brand/logo.png')}}" height="20px" alt=""></span>    <b class="my-3">{{Carbon\Carbon::now()->format('d F Y')}}</b>
                    </div>
                </th>
            </tr>
        </thead>
        <tbody>
            <template  v-for="item,k in dataset" v-if="dataset.length>0">
            <template v-if="item.data.length">
                <tr  class="p-3">
                    <td>  <div class="p-3">
                        <charts :options="item.chart"></charts>
                     </div>
                      <hr></td>
                </tr>
                <tr>
                    <td>
                        <table class="table table-bordered" :id="'dts_table_'+item.id" v-if="item.data.length">
                            <thead class="thead-dark">
                                <tr>
                                    <th class="text-uppercase text-center" :colspan="item.columns.length">@{{item.name+' '+pemda.nama_pemda+' Tahun '}} <?=$StahunRoute?> (@{{item.status_data[1]}})</th>
                                </tr>
                                <tr>
                                    <th v-for="c,i in item.columns">@{{c.ag_title||c.name}} (@{{c.ag_satuan||c.satuan}})</th>
                                </tr>
                            </thead>
                            <tbody>
                               <template v-for="d,di in item.data">
                                <tr>
                                    <td v-for="c,i in item.columns" >
                                        <row-cell-yone :with-satuan="false" :dataset="item" :col="c" :item="d" :actual="true"></row-cell-yone>
                                    </td>
                                </tr>
                                </template>
                            </tbody>
                        </table>
                    </td>

                </tr>
        </template>
            </template>
        </tbody>
    </table>
</div>
@stop

@section('js')
@include('datasetmodul::form.component_vue.show_data')

<script>
    var app=new Vue({
        el:'#app',
        data:{
            dataset:<?=json_encode($dataset)?>,
            pemda:<?=json_encode($pemda)?>,
        },
        created:function(){
            var self=this;
            this.dataset.forEach((el,k) => {
                self.dataset[k].data=[];
                self.dataset[k].columns=[];
                self.dataset[k].status_data=[];

                req_ajax.post(('{{route('api-web-mod.dataset.data.table',['tahun'=>$StahunRoute,'id_dataset'=>'@?','kodepemda'=>$pemda->kodepemda])}}').yoneReplaceParam('@?',[el.id])).then(res=>{
                    self.dataset[k].columns=res.data.columns;
                    self.dataset[k].data=res.data.data;
                    if(res.data.data.length){
                        switch(res.data.data[0].status_data){
                            case 2:
                                self.dataset[k].status_data=[2,'TERVALIDASI'];
                            break;
                            case 1:
                                self.dataset[k].status_data=[2,'TERVERIFIKASI'];
                            break;
                            case null:
                                if(res.data.data[0].id_data){
                                    self.dataset[k].status_data=[2,'TERINPUT'];
                                }else{
                                    self.dataset[k].status_data=[0,'BELUM TERDAPAT STATUS'];
                                }
                            break;
                        }
                    };

                }).finally(function(){

                });

                self.dataset[k]['chart']={
                    chart:{

                    },
                    title:{
                        text:el.name
                    },
                    subtitle:{
                        text:self.pemda.nama_pemda
                    },
                    series:[]
                }
               

            });
        }
    })
</script>

@stop