@extends('adminlte::page_sink')

@section('content')
<style>
    .pdf-profile-content{
        background:#fff;
    }
    .pdf-item{
        padding: 15px;
    }
</style>
<div class="  bg-white" id="app">
   

    <vhtml2pdf
        :show-layout="true"
        :float-layout="true"
        :enable-download="false"
        :preview-modal="true"
        filename="hehehe.pdf"
        :paginate-elements-by-height="1000"
        :pdf-quality="2"
        pdf-format="a4"
        pdf-orientation="portrait"
        pdf-content-width="800"
        :html-to-pdf-options="options"
        :manual-pagination="false"
        ref="pdfProfile"


        v-on:before-download="beforeDownload($event)"
    >
        <section slot="pdf-content">
            
            <section class="pdf-item">

                <dts-rkpd :done-com="layoutDone" class="d-flex" link="{{route("api-web-mod.dataset.public.data.table",["tahun"=>"@?","id_dataset"=>"@?","kodepemda"=>"@?"])}}" :id_dataset="104" kodepemda="01109" :tahun="{{$StahunRoute}}"></dts-rkpd> 
                
            </section>
            <section class="pdf-item ">

                <dts-rkpd :done-com="layoutDone" class="d-flex" link="{{route("api-web-mod.dataset.public.data.table",["tahun"=>"@?","id_dataset"=>"@?","kodepemda"=>"@?"])}}" :id_dataset="104" kodepemda="01102" :tahun="{{$StahunRoute}}"></dts-rkpd> 
                
            </section>
            <section class="pdf-item ">

                <dts-rkpd :done-com="layoutDone" class="d-flex" link="{{route("api-web-mod.dataset.public.data.table",["tahun"=>"@?","id_dataset"=>"@?","kodepemda"=>"@?"])}}" :id_dataset="104" kodepemda="01101" :tahun="{{$StahunRoute}}"></dts-rkpd> 
                
            </section>
            <section class="pdf-item ">

                <dts-rkpd :done-com="layoutDone" class="d-flex" link="{{route("api-web-mod.dataset.public.data.table",["tahun"=>"@?","id_dataset"=>"@?","kodepemda"=>"@?"])}}" :id_dataset="104" kodepemda="014" :tahun="{{$StahunRoute}}"></dts-rkpd> 
                
            </section>

            <section class="pdf-item ">

                <dts-rkpd :done-com="layoutDone" class="d-flex" link="{{route("api-web-mod.dataset.public.data.table",["tahun"=>"@?","id_dataset"=>"@?","kodepemda"=>"@?"])}}" :id_dataset="101" kodepemda="014" :tahun="{{$StahunRoute}}"></dts-rkpd> 
                
            </section>
            
            
                
                    


        </section>
    </vhtml2pdf>
    <div class="btn-group" v-if="layout" style="position: fixed; z-index:9999999">
        <button class="btn btn-primary" @click="generateReport()"><i class="fa fa-pdf"></i> Download</button>
    </div>
</div>
@stop
@section('js')

    <script>
        
        var app=new Vue({
            el:'#app',
            data:{
                layout:false,
                lengthRender:0,
                options:{
                    margin:0,
                    enableLinks:true,
                    image:{type: 'jpeg', quality: 1}
                },
                components_build:[]
            },
            created:function(){
            
            },
            mounted:function(){
                this.$refs.pdfProfile.$refs.pdfContent.attributes.class.value='pdf-profile-content';
                this.$refs.pdfProfile.$on('beforeDownload',this.beforeDownload);
            },
            methods:{
                layoutDone:function(){
                    this.lengthRender+=1;
                    console.log('lay done',this.lengthRender);

                    if(this.lengthRender>=4){
                        this.layout=true;
                        this.lengthRender=0;

                    }
                },
                generateReport () {
                    this.$refs.pdfProfile.generatePdf()
                },
                beforeDownload:async function({ html2pdf, options, pdfContent }) {
                    await html2pdf().set(options).from(pdfContent).toPdf().get('pdf').then((pdf) => {
                        const totalPages = pdf.internal.getNumberOfPages()
                        for (let i = 1; i <= totalPages; i++) {
                            pdf.setPage(i)
                            pdf.setFontSize(10)
                            pdf.setTextColor(150)
                            pdf.text('Page ' + i + ' of ' + totalPages, (pdf.internal.pageSize.getWidth() * 0.88), (pdf.internal.pageSize.getHeight() - 0.3))
                        } 
                    }).save()
                }
            }
        })
    </script>

@stop