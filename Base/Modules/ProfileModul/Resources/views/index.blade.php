@extends('adminlte::page')

@section('content')
{{-- <style>
    .div-diagon {
  background-color: skyblue;
  padding: 5% 20px; /* Added a percentage value for top/bottom padding to keep the wrapper inside of the parent */
    min-height: 40vh;
  -webkit-transform: skewY(-5deg);
  -moz-transform: skewY(-5deg);
  -ms-transform: skewY(-5deg);
  -o-transform: skewY(-5deg);
  transform: skewY(-5deg);
}

.div-diagon > .wrapper {
  -webkit-transform: skewY(5deg);
  -moz-transform: skewY(5deg);
  -ms-transform: skewY(5deg);
  -o-transform: skewY(5deg);
  transform: skewY(5deg);
}
</style>
<div class="div-diagon bg-success">
    <h3>Profile PEMDA</h3>
    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repudiandae, odio? Aliquam non voluptas minima dolorem voluptate impedit assumenda, molestias consequuntur nisi blanditiis illum mollitia aut ad soluta rerum accusamus exercitationem!</p>
    <div class="">
        <a href="{{route('mod.profile.pemda.index',['tahun'=>$StahunRoute])}}" class="btn bg-white text-dark">Detail</a>
    </div>
</div>
<div class="div-diagon bg-primary ">
    <h3>Profile BUMDAM</h3>
    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut, laboriosam recusandae laborum molestias animi laudantium similique excepturi dolor eos dignissimos, quibusdam dolorum quia accusantium repellat earum porro optio sed. Quisquam.</p>
    <a href="{{route('mod.profile.bumdam.index',['tahun'=>$StahunRoute])}}" class="btn btn-default bg-white text-dark">Detail</a>

</div> --}}
<div class="container" id="app">
  <form class="mt-3 mb-3">
    <input type="text"  name="q" class="form-control" placeholder="Cari Pemda">
  </form>
  <div class="row">
    <div :class="path.length>0?'col-4':'col-12'" style="height:80vh">
      <div class="table-responsive bg-white" >
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>NAMA PEMDA</th>
              <th style="width:100px;">DETAIL</th>
      
      
            </tr>
          </thead>
          <tbody>
           @foreach ($collection as $item)
           <tr>
            <td>{{$item->nama_pemda}}</td>
            <td>
              <button class="btn btn-primary" @click="path=('{{route('mod.profile.build.profile',['tahun'=>$StahunRoute,'kodepemda'=>$item->kodepemda])}}')">DETAIL</button>
            </td>
    
          </tr>
           @endforeach
          </tbody>
        </table>
        {{$collection->links()}}
      </div>

      </div>
      <div class="col-8">
        <iframe :src="path" frameborder="0" style="height:80vh; width:100%;"></iframe>
      </div>
    </div> 
    
</div>

@endsection


@section('js')
<script>
  var app=new Vue({
    el:'#app',
    data:{
      path:'',
    }
  })
</script>


@stop
