<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('session/{tahun}/module-profile/')->name('mod.profile')->middleware(['bindTahun'])->group(function() {
    Route::get('/','ProfileModulController@index')->name('.index');

    Route::get('/test','ProfileModulController@test')->name('.test');


    Route::prefix('/pemda')->name('.pemda')->group(function() {
        Route::get('/','ProfilePemdaPublicController@index')->name('.index');
    });

    Route::prefix('/bumdam')->name('.bumdam')->group(function() {
        Route::get('/','ProfileBumdamPublicController@index')->name('.index');
    });


});

Route::prefix('dash/{tahun}/sistem-informasi/module-profile/')->name('mod.profile')->middleware(['bindTahun'])->group(function() {
    Route::prefix('/pdf')->name('.pdf')->group(function() {
        Route::get('/pemda/{kodepemda}','ProfilePemdaPublicController@getProfile')->name('.pdf.pemda');
    });
}); 


