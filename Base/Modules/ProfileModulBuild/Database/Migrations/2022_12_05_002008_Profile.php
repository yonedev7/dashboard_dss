<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Profile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('dataset.profile',function(Blueprint $table){
            $table->id();
            $table->string('kodepemda');
            $table->integer('tahun');
            $table->text('dataset_list');
            $table->boolean('publish')->default(false);
            $table->datetime('publish_date')->nullable();
            $table->string('file')->nullable();

            $table->unique(['kodepemda','tahun']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExist('dataset.profile');
    }
}
