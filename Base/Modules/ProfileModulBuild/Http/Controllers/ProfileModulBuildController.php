<?php

namespace Modules\ProfileModulBuild\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Http\Controllers\FilterCtrl;
use DB;
use Storage;    
use Dompdf\Dompdf;
use iio\libmergepdf\Merger;
use iio\libmergepdf\Pages;
use Carbon\Carbon;
class ProfileModulBuildController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

    public function saveSchema($tahun,$kodepemda,Request $request){
        Storage::put('/profile-chache/'.$kodepemda.'-'.$tahun.'-'.$request->finger.'.json',json_encode($request->data));

    }
    public function index($tahun,Request $request)
    {
        $filter=FilterCtrl::buildFilterPemda($request);
        $pemdas=FilterCtrl::filter_pemda($filter['regional'],$filter['lokus'],$filter['sortlist'],$filter['tipe_bantuan']);


        $data=DB::table('master.master_pemda as mp')
        ->selectRaw("mp.*,p.publish,p.id as exist_data")
        ->where('mp.nama_pemda','ilike','%'.($request->q??'').'%')
        ->whereIn('mp.kodepemda',$pemdas)
        ->leftJoin('dataset.profile as p',[['p.kodepemda','=','mp.kodepemda'],['p.tahun','=',DB::raw($tahun)]])
        ->paginate(555);


        
        return view('profilemodulbuild::index')->with(['data'=>$data,'finger'=>$request->fingerprint()]);
    }

    public function detail($tahun,$kodepemda,Request $request){
        $filter=FilterCtrl::buildFilterPemda($request);

        $finger=$request->fingerprint();

        $status='PEMBUATAN SKEMA';


        $pemdas=FilterCtrl::filter_pemda($filter['regional'],$filter['lokus'],$filter['sortlist'],$filter['tipe_bantuan']);
        if(in_array($kodepemda,$pemdas)){
                $data=(array)DB::table('dataset.profile')->where([
                    'kodepemda'=>$kodepemda,
                    'tahun'=>$tahun,
                ])->first();

            if($data){
                $status=$data['publish']?'PUBLISH':'DRAF';
                if($data['dataset_list']){
                    $data['dataset_list']=json_decode($data['dataset_list']??'[]');

                }else{
                    $data=[];


                    if(file_exists(storage_path('app/profile-chache/'.$kodepemda.'-'.$tahun.'-'.$finger.'.json'))){
                        $json=file_get_contents(storage_path('app/profile-chache/'.$kodepemda.'-'.$tahun.'-'.$finger.'.json'));
                        $data['dataset_list']=json_decode($json??'[]',true);
                    }else{
                        $data['dataset_list']=[];

                    }
                    // xx
                } 



            }else if(file_exists(storage_path('app/profile-chache/'.$kodepemda.'-'.$tahun.'-'.$finger.'.json'))){
                $json=file_get_contents(storage_path('app/profile-chache/'.$kodepemda.'-'.$tahun.'-'.$finger.'.json'));
                $data['dataset_list']=json_decode($json??'[]',true);
            }else{
                $data['dataset_list']=[];

            }
        $dataset=DB::table('master.dataset')->where('status',1)->orderBy('name','asc')->get();


            $pemda=DB::table('master.master_pemda')->where([
                'kodepemda'=>$kodepemda,
            ])->first();

            


            return view('profilemodulbuild::detail')->with(['status'=>$status,'data'=>(array)$data,'finger'=>$finger,'kodepemda'=>$kodepemda,'pemda'=>$pemda,'dataset'=>$dataset??[]]);
        }





    }

    public function datasetBuild($tahun,$id,$kodepemda,$mode_print=false){
       $dataset= DB::table('master.dataset')->where('status',1)->find($id);
       if($dataset){
            $datafiled=DB::table('master.data as d')
            ->selectRaw("d.*,dd.field_name")
            ->join('master.dataset_data as dd','dd.id_data','=','d.id')
            ->where('id_dataset',$dataset->id)->orderBy('index','asc')
            ->get();

            $TABLE=$dataset->table;
            $STATUS='dts_'.$dataset->id;

            if(str_contains($dataset->table,'[TAHUN]')){
                $TABLE=str_replace('[TAHUN]',$tahun,$dataset->table);
                $STATUS='dts_'.$dataset->id.'_'.$tahun;
            }

            $data=DB::table($TABLE.' as d')->selectRaw(implode(',',array_map(function($el){
                return 'd.'.$el->field_name;
            },$datafiled->toArray())))
            ->join($STATUS.' as st',[['st.kodepemda','=','d.kodepemda'],['st.tahun','=','d.tahun']])
            ->where('st.status',2)
            ->where('d.kodepemda',$kodepemda)
            ->orderBy('d.index','asc')->get()->toArray();

            foreach($data as $k=>$d){
                $data[$k]=(array)$d;
            }

            $manifest=[
                'dataset'=>$dataset,
                'column'=>$datafiled,
                'data'=>$data
            ];
            $pemda=DB::table('master.master_pemda')->where([
                'kodepemda'=>$kodepemda,
            ])->first();


            return view('profilemodulbuild::table')->with(['pemda'=>$pemda,'data'=>$manifest,'mode_print'=>$mode_print]);
       }

    }

    public function probuild($tahun,$d,$kodepemda,$dompdf,$k=0){
        $return=0;
        $pemda=DB::table('master.master_pemda')->where([
            'kodepemda'=>$kodepemda,
        ])->first();
        if(is_array($d)){

            if($d['type']=='richtext'){
                $html=view('profilemodulbuild::richtext',['data'=>$d,'mode_print'=>true])->render();
                $html=view("profilemodulbuild::print",['content_inject'=>$html])->render();
                if($k==0){
                    $html=view("profilemodulbuild::head",['pemda'=>$pemda])->render().$html;
                }
                $dompdf->loadHtml( $html);

            $return=1;

            }else{

            }

        }else{
            $return=1;
            $html=view("profilemodulbuild::print",['content_inject'=>$this->datasetBuild($tahun,$d,$kodepemda,true)->render()])->render();
            if($k==0){
                $html=view("profilemodulbuild::head",['pemda'=>$pemda])->render().$html;
            }
            $dompdf->loadHtml( $html);

        }

        return [
            'pdf_schema'=>$dompdf,
            'state'=>$return
        ];
    }

    public function save($tahun,$kodepemda,Request $request){
        $pemda=DB::table('master.master_pemda')->where([
            'kodepemda'=>$kodepemda,
        ])->first();
        $file='';
        try{
            if($request->production){
                $skipindex=[];
                    $pdf_arr=[];
                    foreach($request->data??[] as $k=>$d){
                            if(!in_array($k,$skipindex)){
                                if(!is_array($d)){
                                    $xx=$this->probuild($tahun,$d,$kodepemda,new Dompdf(),count($pdf_arr));
                                    if($xx['state']){
                                        $xx['pdf_schema']->setPaper('A4', in_array($d,[101,104])?'landscape':'potrait');
                                        $xx['pdf_schema']->render();
                                        Storage::put('public/dist-profile/'.$tahun.'/'.$kodepemda.'-cha/'.count($pdf_arr).'.pdf',$xx['pdf_schema']->output());
                                        $pdf_arr[]=storage_path('app/public/dist-profile/'.$tahun.'/'.$kodepemda.'-cha/'.count($pdf_arr).'.pdf');
                                                                          
                                    }
                                }
    
                        
                        }
                    }
                    
                    if(count($pdf_arr??[])){
                        $merger = new Merger;
                        foreach($pdf_arr as $d){
                            $merger->addFile($d);
                        }
                        Storage::put('public/dist-profile/'.$tahun.'/'.$kodepemda.'.pdf',$merger->merge());
                        $file='storage/dist-profile/'.$tahun.'/'.$kodepemda.'.pdf';
                    }
    
               
            }
        DB::beginTransaction();

            DB::table('dataset.profile')->updateOrInsert([
                'kodepemda'=>$kodepemda,
                'tahun'=>$tahun,
            ],[
                'kodepemda'=>$kodepemda,
                'tahun'=>$tahun,
                'dataset_list'=>json_encode($request->data??[]),
                'publish'=>$request->production?true:false,
                'file'=>$file,
                'publish_date'=>$request->production?Carbon::now():null
            ]);

            DB::commit();

        }catch(\Ecetions $e){
            dd($e);
            DB::rollback();

        }

        return 'ok';



    }


    public function profile($tahun,$kodepemda,Request $request){

        $dataset=[];
        $status='PEMBUATAN SKEMA';

        $pemda=DB::table('master.master_pemda')->where([
            'kodepemda'=>$kodepemda,
        ])->first();
        if($request->finger){
            if(file_exists(storage_path('app/profile-chache/'.$kodepemda.'-'.$tahun.'-'.$request->finger.'.json'))){
            $json=file_get_contents(storage_path('app/profile-chache/'.$kodepemda.'-'.$tahun.'-'.$request->finger.'.json'));
            $dataset=json_decode($json,true);
            }else{
                $dataset=[];
            }
            $data=DB::table('master.master_pemda as mp')
            ->selectRaw("mp.*,p.dataset_list,p.publish as publish_p,p.publish_date,p.id as exist_data")
            ->where('mp.nama_pemda','ilike','%'.($request->q??'').'%')
            ->where('mp.kodepemda',$kodepemda)
            ->leftJoin('dataset.profile as p',[['p.kodepemda','=','mp.kodepemda'],['p.tahun','=',DB::raw($tahun)]])->first();
            if($data){
             $status=$data->publish_p?'PUBLISH'.' - '.Carbon::parse($data->publish_date)->format('d F Y'):'DRAF';

             

            }

        }else{
            $data=DB::table('master.master_pemda as mp')
            ->selectRaw("mp.*,p.dataset_list,p.publish as publish_p,p.publish_date,p.id as exist_data")
            ->where('mp.nama_pemda','ilike','%'.($request->q??'').'%')
            ->where('mp.kodepemda',$kodepemda)
            ->where('p.publish',1)
            ->leftJoin('dataset.profile as p',[['p.kodepemda','=','mp.kodepemda'],['p.tahun','=',DB::raw($tahun)]])->first();

            if($data){
             $dataset=$data->dataset_list?json_decode($data->dataset_list,true):[];
             $status=$data->publish_p?'PUBLISH'.' - '.Carbon::parse($data->publish_date)->format('d F Y'):'DRAF';

             

            }


        }

        return view('profilemodulbuild::profile')->with(['status'=>$status,'class'=>$this,'pemda'=>$pemda,'dataset'=>$dataset,'kodepemda'=>$kodepemda]);

    }


   
   
}
