@extends('adminlte::page')
@section('css')
<style>
    .truncate-text {
  width: 100%;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
</style>
@stop
@section('content_header')
<h3>PROFIL PEMDA DAN BUMDAM {{$pemda->nama_pemda}}</h3>
@stop
@section('content')

<div class="container-fluid" id="app">
    <div class="row">
        <div class="col-md-3 bg-white " style="height:80vh; overflow-y:scroll;">
            <div class="">
                <div class="btn-group btn-group-sm  my-2" style="width:100%">
                    <button class="btn btn-warning" @click="saveProfile(0)">Simpan Sebagai Draf</button>
                    <button class="btn btn-primary" @click="saveProfile(1)">Simpan Sebagai Final</button>

                </div>
                <hr>
                <button class="btn btn-primary col-12 my-2" @click="addDeskripsi()">Tambah Deskripsi</button>

                <button class="btn btn-primary col-12 my-2" @click="showForm()">Tambah Dataset</button>
                <ul class="list-group">
                    <li class="list-group-item" v-for="item,i in show_added">
                        <template v-if="item['type']!='richtext'">
                        <button class="btn btn-sm btn-danger btn-circle" @click="deleteDataset(item.id,i)"><i  class="fa fa-times"></i></button> @{{item.name}}

                        </template>
                        <template  v-else> 
                        <div class="d-flex">
                            <button class="btn btn-sm btn-danger btn-circle" @click="deleteDataset(item,i)"><i  class="fa fa-times"></i></button> <span class="truncate-text flex-grow-1 mt-1 pl-2">@{{item.title||'Deskripsi'}}</span>
                            <button class="btn btn-sm btn-warning btn-circle" @click="showFormRich(i,item)" ><i  class="fa fa-pen"></i></button>
                        </div>
                       

                        </template>

                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-9 bg-white flex-column">
            <iframe :src="'{{route('mod.profile.build.profile',['tahun'=>$StahunRoute,'kodepemda'=>$kodepemda])}}?finger={{$finger}}&&step='+step" class="flex-grow-1 d-flex " style="height:80vh;width:100%;" frameborder="0"></iframe>
        </div>
    </div>

    <div ref="modal_rich" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <h4>Ubah Deskripsi</h4>
                    <button class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                   <div class="form-group">
                    <input type="text" class="form-control" v-model="form_rich.title">
                   </div>
                   <div class="form-group">
                   <textarea name="" class="form-control" v-model="form_rich.content"  id="" cols="30" rows="10"></textarea>
                   </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary"  data-dismiss="modal" aria-label="Close" @click="saveFromRich()">Simpan</button></div> 
            </div>
        </div>
    </div>
    <div ref="modal_dataset" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">

                   
                    <h4>List Dataset</h4>
                    <button class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                   <div class="" style="height:60vh; width:100%; overflow-y:scroll; overflow-x:clip;">
                    <table class="table table-bordered" style="max-width:100%;">
                        <thead>
                            <tr>
                                <th>AKSI</th>
                                <th>DATASET</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="item in dtsnocheck">
                                <td>
                                    <button @click="addDataset(item)" class=" btn btn-primary"><i class="fa fa-plus"></i></button>
                                </td>
                                <td><h4><b>@{{item.name}}</b></h4>
                                <p class="truncate-text" style="max-width:600px;">@{{item.tujuan}}</p></td>

                            </tr>
                        </tbody>
                    </table>
                   </div>
                </div>
            </div>
        </div>
    </div>
</div>
@php
    
@endphp
@stop

@section('js')
<script>

    var app=new Vue({
        el:'#app',
        data:{
            step:0,
            dataset:<?=json_encode($dataset??[])?>,
            form:{
                dataset:<?=json_encode($data['dataset_list']??[])?>,
            },
            form_rich:{
                index:-1,
                title:"",
                content:""
            }
        },
        watch:{
           
            "form.dataset":{
                deep:true,
                handler:function(){
                var self=this;
                $.post('{{route('mod.profile.build.save-schema',['tahun'=>$StahunRoute,'kodepemda'=>$kodepemda])}}',{
                    finger:'{{$finger}}',
                    data:this.form.dataset
                },function(res){
                    self.step+=1;
                });
            }
            }
        },
        methods:{
            showFormRich(index,i){
                this.form_rich.index=-1;
                this.form_rich.title=i.title;
                this.form_rich.content=i.content;
                this.form_rich.index=index;

                $(this.$refs.modal_rich).modal({
                    backdrop:'static'
                });

            },
            saveFromRich(){
                if(this.form_rich.index!=-1){
                    this.form.dataset[this.form_rich.index].title=this.form_rich.title;
                    this.form.dataset[this.form_rich.index].content=this.form_rich.content;

                }
            },

            saveProfile(production){
                var self=this;
                self.$isLoading(true);
                $.post('{{route('mod.profile.build.save-schema.production',['tahun'=>$StahunRoute,'kodepemda'=>$kodepemda])}}',{
                    finger:'{{$finger}}',
                    data:this.form.dataset,
                    production:production
                },function(res){
                    self.step+=1;
                self.$isLoading(false);

                });
               
            },
            addDeskripsi(){
                this.form.dataset.push({
                    type:'richtext',
                    title:'',
                    content:''
                });
            },
            deleteDataset(id,index=-1){
                if(index!=-1){
                    this.form.dataset.splice(index,1);
                }
                var index=this.form.dataset.indexOf(''+id);

                if(index!=-1){
                    this.form.dataset.splice(index,1);
                }

                
            },
            showForm(){
                $(this.$refs.modal_dataset).modal({
                    backdrop:'static'
                });
            },
            addDataset(item){
                if(!this.form.dataset.includes(item.id)){
                    this.form.dataset.push(item.id);
                }

            }
        },
        computed:{
            dtsnocheck:function(){
                return this.dataset.filter(el=>{
                    return !this.form.dataset.includes(el.id);
                });
            },
            show_added:function(){
                var data=[];
                this.form.dataset.forEach(el=>{
                    if(typeof el != 'object'){

                        var d=this.dataset.filter(x=>{
                                return (x.id==el);
                         });
                        if(d.length){
                            data.push(d[0]);
                        }
                    }else{
                        data.push(el);
                    }
                   
                });
                return data;
            }
        }
    })
</script>

@stop