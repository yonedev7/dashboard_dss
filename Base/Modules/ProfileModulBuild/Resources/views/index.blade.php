@extends('adminlte::page')

@section('content_header')
<h3>Profil PEMDA dan BUMDAM</h3>
@stop
@section('content')
  <div class="container">
    <div class="col-md-12">

        <div class="table-responsive bg-white">

          <table class="table table-bordered">
            <thead>
              <tr>
                <th>AKSI</th>
                <th>KODEPEMDA</th>
                <th>NAMAPEMDA</th>
                <th>STATUS PROFIL</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($data as $item)
              <tr>
                <td>
                  <a href="{{route('mod.profile.build.detail',['tahun'=>$StahunRoute,'kodepemda'=>$item->kodepemda])}}" class="btn btn-primary">PEMETAAN</a>
                </td>
                <td>{{(int)$item->kodepemda}}</td>
                <td>{{$item->nama_pemda}}</td>
                <td>
                  @if($item->exist_data)
                    @if($item->publish)
                    PUBLISH
                    @else
                    DRAF

                    @endif
                  @else
                    BELUM DIPETAKAN
                  @endif
                </td>

              </tr>
                  
              @endforeach
            </tbody>
          </table>
        </div>

    </div>
  </div>
@endsection
