<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>PROFILE</title>
        <style>
             th,td{
                    border:1px solid rgb(93, 93, 93);
                }
            @media screen
                {
                    th,td{
                    border:1px solid rgb(93, 93, 93);
                }
                    table {
                        border-collapse: collapse;
                        font-family:"Times New Roman",Georgia;
                        font-size:10px;
                        // more
                    }
                }

            @media print
            {
                th,td{
                    border:1px solid rgb(93, 93, 93);
                }
                
                table {
                    border-collapse: collapse;
                    width:100%;
                    font-family:Verdana, Arial;
                    font-size:10px;
                    // more
                }
            }
        </style>
       {{-- Laravel Mix - CSS File --}}
       {{-- <link rel="stylesheet" href="{{ mix('css/profilemodulbuild.css') }}"> --}}

    </head>
    <body>
        @yield('content')

        {{-- Laravel Mix - JS File --}}
        {{-- <script src="{{ mix('js/profilemodulbuild.js') }}"></script> --}}
    </body>
</html>
