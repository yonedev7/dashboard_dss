@extends('adminlte::page_sink')
@section('css')
<style>
    .table-rotate{
        transform:rotate(270deg);
    }
    .print-break {page-break-after: always;}
</style>
@stop
@section('content')

<div class="container-fluid">
    <h2 class="text-center mt-2">PROFIL {{$pemda->nama_pemda}}</h2>
    <p class="text-center">DALAM KERANGKA NUWSP TAHUN {{$StahunRoute}}</p>
    <p class="text-center"><span class="badge badge-primary">{{$status}}</span></p>
    <hr>
    <div  class="" >
        @foreach ($dataset??[] as $item)
            @if(is_array($item))
                @if($item['type']=='richtext')
                @include('profilemodulbuild::richtext',['data'=>$item])
                @endif
            @else
            {!!$class->datasetBuild($StahunRoute,$item,$kodepemda)!!}
            @endif
        
        
    @endforeach
    </div>
</div>

@stop