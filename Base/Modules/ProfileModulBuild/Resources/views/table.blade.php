

<h3>{{$data['dataset']->name}}</h3>
<p><small>{{$pemda->nama_pemda}}</small></p>
<p>{!!$data['dataset']->tujuan!!}</p>

<div class="{{isset($prindmode)?'':'table-responsive'}}">
    <table class="table table-striped  table-bordered ">
        <thead>
            <tr>
                @foreach($data['column'] as $c)
                <th>
                    {{$c->name}} {{$c->satuan}}
                </th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            @foreach($data['data'] as $item)
            <tr>
                @foreach($data['column'] as $c)
                <td>{{$item[$c->field_name]}}</td>
                @endforeach
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
