<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/profile-pemda-build-schema/{tahun}/{kodepemda}','ProfileModulBuildController@saveSchema')->name('mod.profile.build.save-schema');
Route::post('/profile-pemda-build-schema/{tahun}/{kodepemda}/save','ProfileModulBuildController@save')->name('mod.profile.build.save-schema.production');
