<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::prefix('dash/{tahun}/sistem-informasi/build-profile')->name('mod.profile.build')->group(function(){
    Route::get('/', 'ProfileModulBuildController@index')->name('.index');
    Route::get('/{kodepemda}', 'ProfileModulBuildController@detail')->name('.detail');
    Route::get('/{kodepemda}/profile', 'ProfileModulBuildController@profile')->name('.profile');



});

Route::prefix('profilemodulbuild')->group(function() {
});
