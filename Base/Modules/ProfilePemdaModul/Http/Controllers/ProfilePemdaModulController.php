<?php

namespace Modules\ProfilePemdaModul\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use Alert;
use Modules\ParameterProfile\Http\Controllers\RenderController;

class ProfilePemdaModulController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index($tahun,Request $request)
    {
        $basis=strtoupper($request->basis??'NASIONAL');
        $data=DB::table('master_pemda as mp')
        ->leftJoin('dataset.profile_pemda as pp',[
            ['pp.kodepemda','=','mp.kodepemda'],
            ['pp.tahun','=',DB::raw($tahun)],
            ['pp.tw','=',DB::raw(4)],
        ])
        ->where('mp.kodepemda','!=','0')
        ->orderBy('mp.kodepemda','asc')
        ->selectRaw('mp.nama_pemda,mp.tipe_bantuan,mp.tahun_proyek,mp.regional_1,mp.regional_2,pp.*, mp.kodepemda')
        ->get();
        return view('profilepemdamodul::index')->with(['basis'=>$basis,'data'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('profilepemdamodul::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */

    

    public function load_content($tahun,Request $request){
        $re=[
            'content'=>$request->content,
            'list_param'=>[],
            'tahun'=>$tahun,
            'kodepemda'=>$request->kodepemda,
            'pemda'=>(array)DB::table('master.master_pemda')->where('kodepemda',$request->kodepemda)->first()
           ];
    
           if($re['pemda']){
                preg_match_all('/\[(dss_params_\w+)\]/i',  $re['content'], $param_list);
                
                if(isset($param_list[1])){
                    $param_list=array_map(function($el){
                        return str_replace('dss_params_','',$el);
                    },array_unique($param_list[1]));
                    $re['list_param']=$param_list;
                }
                if(count($re['list_param'])){
                    $params=RenderController::render($re['list_param'],$re['tahun'],$re['pemda']);
                    foreach($params as $k=>$v){
                        $re['content']=str_replace($k,$v,$re['content']);
                    }
    
                }
                
           }else{
                $re['content']='';
           }
          
           return ($re);
    }

    public function createorshow($tahun,$kodepemda,Request $request)
    {
        $data=DB::table('master_pemda as mp')
        ->leftJoin('dataset.profile_pemda as pp',[
            ['pp.kodepemda','=','mp.kodepemda'],
            ['pp.tahun','=',DB::raw($tahun)],
            ['pp.tw','=',DB::raw(4)],
        ])
        ->where('mp.kodepemda','=',$kodepemda)
        ->orderBy('mp.kodepemda','asc')
        ->selectRaw('mp.nama_pemda,mp.tipe_bantuan,mp.tahun_proyek,mp.regional_1,mp.regional_2,pp.*, mp.kodepemda')
        ->first();

        if($data->status==null){
            $data->status=0;
            $data->tahun=$tahun;
        }
        return view('profilepemdamodul::show')->with(['data'=>$data]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('profilepemdamodul::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $tahun,$kodepemda)
    {
        $data=$request->data;

        $data['kodepemda']=$kodepemda;
        $data['tahun']=$tahun;
        $data['tw']=4;
        $data['status']=$request->status==1?1:0;
        DB::table('dataset.profile_pemda')->updateOrInsert(
            [
                'kodepemda'=>$data['kodepemda'],
                'tahun'=>$data['tahun'],
                'tw'=>$data['tw'],


            ]
        ,
        
            $data
        
        );

        Alert::success('','Berhasil Memperbarui Data');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
