<?php

namespace Modules\ProfilePemdaModul\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use Alert;
use Modules\ParameterProfile\Http\Controllers\RenderController;
use Storage;
class ProfilePemdaSchemaCtrl extends Controller
{

   public  $load_schema;

    function __construct() {

        $this->load_schema=json_decode(file_get_contents(storage_path('app/schema_json/profile_pemda.json')),true);
      }

    public function load_pemda($tahun,$kodepemda){

        return view('profilepemdamodul::schema_detail')->with(['schema'=>array_map(function($el){
            return [
                'name'=>$el['name'],
                'data'=>[
                    
                ]
            ];
        },$this->load_schema),'kodepemda'=>$kodepemda]);
    
    }

    public function index($tahun,$kodepemda,Request $request){

        $schema=$this->load_schema[$request->part??0];
        $chrt=[
            
        ];

        $pemda=(array)DB::table('master.master_pemda')->where('kodepemda',$kodepemda)->first();

        foreach($schema['chart'] as $kc=>$ch){
            if(!isset($chrt[$kc])){
                $chrt[$kc]=[
                    'chart'=>[
                        'type'=>$ch['type'],

                    ],
                    'title'=>[
                        'styles'=>[
                            "textColor"=>'$fff'
                        ],
                        'text'=>$schema['name']
                    ],
                    'subtitle'=>[
                        'text'=>$pemda['nama_pemda'].' Tahun '.$tahun
                    ],
                    'xAxis'=>[
                        "type"=>'category'
                    ],
                    "tooltip"=> [
                        "pointFormat"=> '{series.name}: <b>{point.percentage:.1f}%</b>'
                    ],
                    'series'=>[]
                ];
            }
           

            foreach($ch['series'] as $ks=>$sr){
                $dataset=[];
                foreach($sr['id_dataset'] as $id_dts){
                    $tb=['','dts_'.$id_dts];
                    $dts=DB::table('master.dataset')->find($id_dts);
                    $tb[0]=$dts->table;
                    if(str_contains($tb[0],'[TAHUN]')){
                        $tb[0]=str_replace('[TAHUN]',$tahun,$tb[0]);
                        $tb[1].='_'.$tahun;
                    }
                    $dataset[]=$tb;
                
                }
             

                if(!isset($chrt[$kc]['series'][$ks])){
                    $chrt[$kc]['series'][$ks]=[
                        'name'=>$sr['name'],
                        'data'=>[]
                    ];
                }
                $sql_query=$sr['sql'];
                $sql_query=str_replace('[TABLE_DATA]','$'.'dataset',$sql_query);
                $sql_query=str_replace('[TAHUN]',$tahun,$sql_query);
                $sql_query=str_replace('[TW]',4,$sql_query);
                $sql_query=str_replace('[KODEPEMDA]',$kodepemda,$sql_query);
                preg_match_all('/\{.*.\[[\w+]\]\}/U', $sql_query, $output_array);
                foreach($output_array as $k=>$s){
                    foreach($s as $m=>$d){
                        eval("\$s_e=".str_replace('{','',str_replace('}','',$d)).";");
                        $sql_query=str_replace($d,$s_e,$sql_query);
                    }
                }
            
                $chrt[$kc]['series'][$ks]['data']=(array)DB::select($sql_query);
                foreach($chrt[$kc]['series'][$ks]['data'] as $kkk=>$x){
                    $chrt[$kc]['series'][$ks]['data'][$kkk]->y=(float) $x->y;
                }
            }
        }
       

        return [
            'name'=>$schema['name'],
            'charts'=>$chrt
        ];
       



    }

}