@extends('adminlte::page')

@section('content_header')
<h3>Profile PEMDA</h3>
<hr>
@stop
@section('content')
<div class="bg-dark">
    <div class="container text-dark">
    
        <div class="d-flex ">
            <div class="card m-2">
                <div class="card-body">
                    <h5><b>Profil Tersedia</b></h5>
                    <p> PEMDA</p>
                </div>
            </div>
            <div class="card m-2">
                <div class="card-body">
                    <h5><b>Profil Terverifikasi</b></h5>
                    <p> PEMDA</p>
                </div>
            </div>
        </div>
    </div>
</div>
@include('tematik.filter')
<div class="table-responsive p-3">
    <table class="table table-striped table-bordered" id="table-list">
        <thead class="bg-dark">
            <tr>
                <th>Regional</th>
                <th>KODEPEMDA</th>
                <th>Nama PEMDA</th>
                <th>Tahun Proyek</th>
                <th>Tipe Bantuan</th>
                <th>Status Profile</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $d)
            <tr>
                <td>{{$d->regional_1}} / {{$d->regional_2}}</td>
                <td>{{$d->kodepemda}}</td>
                <td>{{$d->nama_pemda}}</td>
                <td>{{$d->tahun_proyek<=1?'':$d->tahun_proyek}}</td>
                <td>{{$d->tipe_bantuan}}</td>
                <td class="{{$d->status?'bg-primary':'bg-warning'}}">
                    <b>{{$d->status?'TERVERIFIKASI':'BELUM TERVERIFIKASI'}}</b>
                </td>
                <td>
    
                    <a href="{{route('dash.profile-pemda.detail',['tahun'=>$Stahun,'kodepemda'=>$d->kodepemda])}}" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i> Detail</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@stop

@section('js')
<script>
    $('#table-list').DataTable()
</script>

@stop