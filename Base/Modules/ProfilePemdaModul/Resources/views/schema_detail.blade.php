@extends('adminlte::page')

@section('content_header')
<h3>Profile PEMDA</h3>
<hr>
@stop
@section('content')
<div class="bg-dark py-3" id="app">
<div class="row">
    <div class="col-3" v-for="item in schema">
        <template  v-for="c in item.data">
        <charts :options="c"></charts>
        </template>
    </div>
</div>
</div>
@stop

@section('js')
<script>
    var app=new Vue({
        el:'#app',
        created:function(){
            var self=this;
            this.schema.forEach((el,k)=> {
               req_ajax.post(('{{route('api-web.mod-profilepemdamodule.load.schema',['tahun'=>'@?','kodepemda'=>'@?'])}}').yoneReplaceParam('@?',[self.tahun,self.kodepemda]),{
                part:k
               }).then(function(res){
                self.schema[k].data=res.data.charts;
               });
            });
        },
        data:{
            kodepemda:'{{$kodepemda}}',
            tahun:{{date('Y')}},
            schema:<?=json_encode($schema)?>,
        }
    })
</script>

@stop