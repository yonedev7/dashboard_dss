@extends('adminlte::page')

@section('content_header')
<h3>PROFIL {{$data->nama_pemda}} {{$Stahun}}</h3>
<hr>
@stop
@section('content')
<style>
    .dropdown-menu{
        z-index:99;
    }
   
    ul.nav {
        white-space: nowrap;
        overflow-x: auto;
    }

    ul.nav li {
        display: inline-block;
        float: none;
    }
    .scroll-nav{
        overflow-y:hidden;
        overflow-x:scroll;
        position: relative;
    }
    .scroll-nav ul,.scroll-nav ul li{
        position: relative;
    }
</style>
<div class="" id="profile-ppp">
    <div class="" style="position: relative;">
        <nav class="navbar  navbar-expand-lg navbar-dark bg-dark" style="position:relative" >
            <a class="navbar-brand" href="javascript:void(0)">
                <img src="{{asset('assets/img/brand/logo-1.png')}}" width="30" height="30" alt="">
              </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bav-xxx" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse scroll-nav" id="bav-xxx">
              <ul class="navbar-nav  mr-auto">
                <li class="nav-item " v-for="item,k in menu">
                  <a v-bind:class="'nav-link font-weight-bold '+(menu_index[0]==k?'active':'')" @click="menu_index=[k]" href="javascript:void(0)" v-bind:id="'nav-profile-'+k" >
                    @{{item.title}}
                  </a>
                 
                </li>
              </ul>
            </div>
          </nav>

          <nav class="navbar  navbar-expand-lg navbar-dark bg-secondary" style="position:relative" >
            <a class="navbar-brand" hhref="javascript:void(0)">SUB:</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bav-xxx-2" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse scroll-nav" id="bav-xxx-2">
              <ul class="navbar-nav  mr-auto">
               
                <li class="nav-item " v-for="item,k in menu[menu_index[0]].sub">
                  <a v-bind:class="'nav-link font-weight-bold '+(menu_index.length==2?(menu_index[1]==k?'active':''):'')" href="javascript:void(0)" @click="menu_index=[menu_index[0],k]" v-bind:id="'nav-profile-sub-'+k" >
                    @{{item.title}}
                  </a>
                 
                </li>
              </ul>
            </div>
          </nav>
    
         
     
    </div>
    <div class="container">
        <form action="{{route('dash.profile-pemda.update',['tahun'=>$Stahun,'kodepemda'=>$data->kodepemda])}}" method="post">
            @method('PUT')
            @csrf
            <template v-for="item,k in menu">
                <input type="hidden" v-bind:name="'data['+item.field+']'" v-model="item.content">
                <template v-for="sub,ks in item.sub">
                    <input type="hidden" v-bind:name="'data['+sub.field+']'" v-model="sub.content">
                </template>
            </template>
            <div class="row">
                <div class="col-6">
                    <div class="input-group mb-3 mt-3">
                        <select name="status" class="custom-select" id="" v-model="data.status">
                            <option value="0">BELUM TERVERIFIKASI</option>
                            <option value="1">TERVERIFIKASI</option>
                 
                        </select>
                        <div class="input-group-append">
                           <button class="btn btn-primary" type="submit">UPDATE</button>
                           <button class="btn btn-warning" @click="modalParameter()" type="button">PARAMETER LIST</button>
                        </div>
                         </div>
                       </div>
                </div>
            </div>
        </form>
    </div>

    <div class="modal fade" tabindex="-1" id="modal-parameter" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title">List Paramater </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover" id="table-data-parameter">
                                <thead class="thead-dark"></thead>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        
                    </div>
            </div>
        </div>
    </div>
</div>

<div class="">
      <textarea id="editor-container">
      </textarea>
</div>
<div style="display: none" id="content_edited"></div>


@stop

@section('js')
<link href="//cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
<script src="//cdn.quilljs.com/1.3.6/quill.min.js"></script>
<script src="https://cdn.ckeditor.com/4.18.0/standard/ckeditor.js"></script>

 <script>

    CKEDITOR.plugins.add('YoneMedia',
    {
        init: function (editor) {
            console.log(
                CKEDITOR.plugins.getPath('Media')
            );
            var pluginName = 'YoneMedia';
            editor.ui.addButton('Media',
                {
                    label: 'Media',
                    command: 'OpenWindow',
                    icon: CKEDITOR.plugins.getPath('Media') + 'mybuttonicon.gif'
                });
            var cmd = editor.addCommand('OpenWindow', { exec: showMyDialogMedia });
        },
        
    });

    function showMyDialog(e) {
        e.insertHtml(' Hello ');
            
    }
   
    
      var editor=CKEDITOR.replace( 'editor-container' ,{
        contentsCss: [ '{{asset('css/print_profile.css')}}', 'https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css'],
        htmlClass: ' bg-secondary',
        bodyClass: 'container bg-white',
        height:500,
        tollbar:null
      });
        

       
    var app=new Vue({
        el:'#profile-ppp',
        data:{
            menu_index:[0],
            data:<?=json_encode($data)?>,
            menu:<?=json_encode($SmenuProfilePemda)?>,
            summernote:null,
            content_sementara:[],
            parameter:{
                ref_table:null,
                
                data:[]
            }
           
        },
        created:function(){


            this.summerInit();
            var self=this;
            setTimeout((self) => {
                self.parameter.ref_table=$('#table-data-parameter').DataTable({
                    columns:[
                        {data:'name',render:(data)=>{return '[dss_params_'+data+']';},title:'Parameter'},
                        {data:'keterangan',title:'Keterangan'},
                        {data:'type',
                        title:'Tipe',
                        render:function(data){
                            switch(data){
                                case 0:
                                    return 'Dataset';
                                break;
                                case 1:
                                    return 'Manual';
                                break;
                                case 2:
                                    return 'Template';
                                break;
                            }
                        }},
                        {data:'table',
                        title:'Table',
                        render:function(data,type,row,index){
                            if(row.type==0){
                                return row.info_table;
                            }else{
                                return row.table;
                            }
                        }},
                        {data:'field',title:'Field',render:function(data,type,row,index){
                            if(row.type==0){
                                return row.info_field;
                            }else{
                                return row.field;
                            }
                        }},
                        {data:'last',title:'Last Data',render:(data,type,row)=>{
                            if(row.type!=2){
                                return data?'Active':'Non Active';
                                
                            }
                            return '';
                        }},
                        {data:'bind',type:'html',title:'Bind View',render:function(data){
                            var data=JSON.parse(data);
                            if(data!=[]){
                                console.log(data);
                                data=data.map(el=>{
                                    return el.name+' ('+el.view+')';
                                });
                            }

                            return data.join(', ');

                        }}

                    ]
                });
                self.loadParameter();
            },500,self);

        },
        watch:{
            menu_index:function(val,old){
                var deltaOld=window.editor.getData();
                if(val.length==1){
                    var delta=this.menu[this.menu_index[0]].content
                }else{
                    var delta=this.menu[this.menu_index[0]].sub[this.menu_index[1]].content
                }
               
                if(old.length==1){
                    this.menu[old[0]].content=deltaOld;
                }else{
                    this.menu[old[0]].sub[old[1]].content=deltaOld;
                }
                window.editor.setData(delta);

              
            },
        },
        methods:{
            modalParameter:function(){
                $('#modal-parameter').modal();
            },
            loadParameter:function(){
                var self=this;
                req_ajax.post('{{route('api-web-mod.parameter-profile.load',['tahun'=>$Stahun])}}').then(res=>{
                    res.data.data.forEach(el => {
                        self.parameter.data.push(el);
                    });

                   if(self.parameter.ref_table!=null){
                    self.parameter.ref_table.clear().draw();
                    self.parameter.ref_table.rows.add(self.parameter.data).draw();
                   }

                    if(res.data.last_page<self.curent_page){
                        self.curent_page+=1;
                        self.loadParameter();
                        if(self.parameter.ref_table!=null){
                            self.parameter.ref_table.clear().draw();
                            self.parameter.ref_table.rows.add(self.parameter.data).draw();
                        }
                    }
                })
            },
            summerInit:function(){
                return new Promise((resolve, reject) => {
                    this.menu.forEach((el,key,arr) => {
                        this.menu[key].content=this.data[el.field]?this.data[el.field]:'';

                            el.sub.forEach((elsub,keysub,arr2)=>{
                            if(this.data[elsub.field]){
                                this.menu[key].sub[keysub].content=this.data[elsub.field];
                            }else{
                                this.menu[key].sub[keysub].content="";
                            }

                            if((arr.length-1)==key && (arr2.length-1)==keysub ){
                                if(this.menu_index.length==1){
                                    var delta=(this.menu[this.menu_index[0]].content);
                                }else{
                                    var delta=(this.menu[this.menu_index[0]].sub[this.menu_index[1]].content);
                                }
                                window.editor.setData(delta);

                                resolve(this.menu);
                            }
                        
                        });
                    });
                });



                 
            },

        }
    });
    editor.on('change', function() { 
        content=this.getData();
        $('#content_edited').html(content);
        $('#content_edited table').addClass('table table-bordered table-stiped');
        $('#content_edited table thead').addClass('thead-dark');
        content= $('#content_edited').html();
        console.log(content);

        if(app.menu_index.length==1){
            app.menu[app.menu_index[0]].content=content;
        }else{
            app.menu[app.menu_index[0]].sub[app.menu_index[1]].content=content;
        }
    });

    
</script>
@stop