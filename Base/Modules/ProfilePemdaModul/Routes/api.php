<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/profilepemdamodul', function (Request $request) {
    return $request->user();
});

Route::prefix('{tahun}/mod-profile')->name('api-web.mod-profilepemdamodule')->group(function(){    
    Route::post('/load-content-profile','ProfilePemdaModulController@load_content')->name('.load_profile_content');
    Route::post('/load-schema/{kodepemda}','ProfilePemdaSchemaCtrl@index')->name('.load.schema');

});