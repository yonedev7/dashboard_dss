<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::prefix('dash/{tahun}/sistem-informasi/profile-pemda')->name('dash.profile-pemda')->middleware(['auth:web','bindTahun'])->group(function(){
    Route::get('/', 'ProfilePemdaModulController@index')->name('.index');
    Route::get('/detail/{kodepemda}', 'ProfilePemdaModulController@createorshow')->name('.detail');


    Route::PUT('/update/{kodepemda}', 'ProfilePemdaModulController@update')->name('.update');
    


});

Route::prefix('session/{tahun}/sistem-informasi/mod-pemda-profile')->group(function(){
    Route::get('/load-schema/{kodepemda}', 'ProfilePemdaSchemaCtrl@load_pemda')->name('.schema');
    
});

