<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MasterNomen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public $tahun;
    public $connection_schema;
    function __construct($tahun=null){
        $this->tahun=$tahun??2022;
        $this->connection_schema='sink_modul';
    } 

    public function up()
    {
        //
        if(!Schema::connection($this->connection_schema)->hasTable('master.nomenklatur')){
            Schema::connection($this->connection_schema)->create('master.nomenklatur',function(Blueprint $table){
                $table->string('kode')->primary();
                $table->uuid('uuid')->unique()->default(DB::raw('(gen_random_uuid())'));
                $table->string('name')->index();
                $table->boolean('only_prov')->default(false)->index();
                $table->string('kodekegiatan')->nullable()->index();
                $table->string('namekegiatan')->nullable()->index();
                $table->string('kodeprogram')->nullable()->index();
                $table->string('namaprogram')->nullable()->index();
                $table->timestamps();
            });
       }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
