<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Urusan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public $tahun;
    public $connection_schema;
    function __construct($tahun=null){
        $this->tahun=$tahun??2022;
        $this->connection_schema='sink_modul';
    } 

    public function up()
    {
        //
        if(!Schema::connection($this->connection_schema)->hasTable('master.urusan')){
            Schema::connection($this->connection_schema)->create('master.urusan',function(Blueprint $table){
                $table->string('kode')->primary();
                $table->string('context')->nullable();
                $table->uuid('uuid')->unique()->default(DB::raw('(gen_random_uuid())'));
                $table->string('name')->index();
                $table->unique(['kode','context']);
                $table->timestamps();
            });
       }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
