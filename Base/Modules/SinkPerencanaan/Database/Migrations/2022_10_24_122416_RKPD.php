<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Query\Expression;

class RKPD extends Migration
{
    public $tahun;
    public $connection_schema;
    function __construct($tahun=null){
        $this->tahun=$tahun??2022;
        $this->connection_schema='sink_modul';
    } 

    public function up()
    {
        //
       if(!Schema::connection($this->connection_schema)->hasTable('master.rkpd_'.$this->tahun)){
        $tahun=$this->tahun;

            Schema::connection($this->connection_schema)->create('master.rkpd_'.$this->tahun,function(Blueprint $table) use ($tahun){
                $table->id();
                $table->string('uuid')->unique()->default(DB::raw('(concat(\'dss_\',gen_random_uuid()))'));                
                $table->string('context');
                $table->bigInteger('id_parent')->nullable()->unsigned()->index();

                $table->string('kodepemda')->index();
                $table->string('urusan_kode')->nullable()->unsigned();
                $table->string('kode')->nullable()->index();
                $table->string('name')->index();
                $table->double('pagu',20,3)->default(0);
                $table->json('label')->default('[]');
                $table->integer('tahun');
                $table->mediumText('keterangan')->nullable();
                $table->unique(['kodepemda','tahun','kode','urusan_kode']);

                $table->timestamps();
                $table->foreign('urusan_kode')->references('kode')->on('master.urusan')->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('id_parent')->references('id')->on('master.rkpd_'.$tahun)->onDelete('cascade')->onUpdate('cascade');
                

            });
       }
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
