<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Query\Expression;

class RKPDIndikator extends Migration
{
    public $tahun;
    public $connection_schema;
    function __construct($tahun=null){
        $this->tahun=$tahun??2022;
        $this->connection_schema='sink_modul';
    } 

    public function up()
    {
        //
        if(!Schema::connection($this->connection_schema)->hasTable('master.rkpd_ind_'.$this->tahun)){
            $tahun=$this->tahun;
            Schema::connection($this->connection_schema)->create('master.rkpd_ind_'.$this->tahun,function(Blueprint $table) use ($tahun){
                $table->id();
                $table->integer('tahun');
                $table->string('uuid')->unique()->default(DB::raw('(concat(\'dss_\',gen_random_uuid()))'));  
                $table->string('kodepemda')->index();
                $table->bigInteger('rkpd_id')->unsigned();
                $table->text('name');
                $table->mediumText('target');
                $table->mediumText('capaian')->nullable();
                $table->mediumText('capaian_persentasi')->nullable();
                $table->string('satuan');
                $table->mediumText('keterangan')->nullable();
                $table->jsonb('label')->default(new Expression('(\'[]\'::jsonb)'));
                $table->timestamps();
                $table->foreign('rkpd_id')->references('id')->on('master.rkpd_'.$tahun)->onDelete('cascade')->onUpdate('cascade');

            });
       }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
