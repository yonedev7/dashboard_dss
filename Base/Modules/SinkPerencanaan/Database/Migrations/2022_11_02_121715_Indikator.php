<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Query\Expression;


class Indikator extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public $tahun;
    public $connection_schema;
    function __construct($tahun=null){
        $this->tahun=$tahun??2022;
        $this->connection_schema='sink_modul';
    } 

    

    public function up()
    {
        //
        if(!Schema::connection($this->connection_schema)->hasTable('master.indikator_'.$this->tahun)){
            $tahun=$this->tahun;
            Schema::connection($this->connection_schema)->create('master.indikator_'.$this->tahun,function(Blueprint $table) use ($tahun){
                $table->id();
                $table->string('uuid')->unique()->default(DB::raw('(concat(\'dss_\',gen_random_uuid()))'));
                $table->integer('tahun');
                $table->bigInteger('sumber_id')->nullable()->unsigned();
                $table->string('kodepemda')->nullable();
                $table->string('context')->index();
                $table->text('keterangan_sumber')->nullable();
                $table->string('urusan_kode')->index()->nullable();
                $table->boolean('with_dif_scope')->default(true)->comment('true for kota : provinsi');
                $table->text('name')->nullable()->index();
                $table->mediumText('target');
                $table->mediumText('target1');
                $table->mediumText('target2');
                $table->mediumText('target3');
                $table->mediumText('target4');
                $table->string('satuan');
                $table->mediumText('keterangan')->nullable();
                $table->jsonb('label')->default(new Expression('(\'[]\'::jsonb)'));
                $table->timestamps();
                $table->foreign('sumber_id')->references('id')->on('master.sumber_indikator_pusat_'.$tahun)->onDelete('cascade')->onUpdate('cascade');


            });
       }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
