<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Query\Expression;

class SinkIndikatorRKPD extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public $tahun;
    public $connection_schema;
    function __construct($tahun=null){
        $this->tahun=$tahun??2022;
        $this->connection_schema='sink_modul';
    } 
    public function up()
    {
        //
          //
       if(!Schema::connection($this->connection_schema)->hasTable('sink.ind_rkpd_nomen_'.$this->tahun)){
        $tahun=$this->tahun;
        Schema::connection($this->connection_schema)->create('sink.ind_rkpd_nomen_'.$this->tahun,function(Blueprint $table) use ($tahun){
            $table->id();
            $table->string('uuid')->unique()->default(DB::raw('(concat(\'dss_\',gen_random_uuid()))'));  
            $table->integer('tahun');
            $table->bigInteger('id_indikator')->unsigned();
            $table->string('nomenklatur_rkpd')->unsigned();
            $table->string('urusan_kode')->unsigned()->index();
            $table->jsonb('label')->default(new Expression('(\'[]\'::jsonb)'));
            $table->mediumText('keterangan')->nullable();
            $table->bigInteger('user_created')->unsigned();
            $table->bigInteger('user_updated')->unsigned();
            $table->timestamps();
            $table->index(['id_indikator','nomenklatur_rkpd','tahun'],'inx_'.'ind_rkpd_nomen_'.$this->tahun);
            $table->unique([
                'id_indikator','nomenklatur_rkpd','tahun','urusan_kode'
            ]);

            $table->foreign('id_indikator')->references('id')->on('master.indikator_'.$tahun)->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('nomenklatur_rkpd')->references('kode')->on('master.nomenklatur')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('urusan_kode')->references('kode')->on('master.urusan')->onDelete('cascade')->onUpdate('cascade');

        });
   }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
