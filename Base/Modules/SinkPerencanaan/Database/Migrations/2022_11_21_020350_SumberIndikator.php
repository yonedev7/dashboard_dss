<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SumberIndikator extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public $tahun;
    public $connection_schema;
    function __construct($tahun=null){
        $this->tahun=$tahun??2022;
        $this->connection_schema='sink_modul';
    } 
    public function up()
    {
        //

        if(!Schema::connection($this->connection_schema)->hasTable('master.sumber_indikator_pusat_'.$this->tahun)){
            $tahun=$this->tahun;
            Schema::connection($this->connection_schema)->create('master.sumber_indikator_pusat_'.$this->tahun,function(Blueprint $table) use ($tahun){
                $table->id();
                $table->string('uuid')->unique()->default(DB::raw('(concat(\'dss_\',gen_random_uuid()))'));  
                $table->bigInteger('parent_id')->nullable()->unsigned();
                $table->integer('tahun');
                $table->string('context')->nullable();
                $table->string('urusan_kode')->unsigned()->index();
                $table->text('uraian');

                
                $table->bigInteger('user_created')->unsigned();
                $table->bigInteger('user_updated')->unsigned();
                $table->timestamps();
              
                $table->foreign('urusan_kode')->references('kode')->on('master.urusan')->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('parent_id')->references('id')->on('master.sumber_indikator_pusat_'.$tahun)->onDelete('cascade')->onUpdate('cascade');


            });
       }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('master.sumber_indikator_pusat_'.$this->tahun);
    }
}
