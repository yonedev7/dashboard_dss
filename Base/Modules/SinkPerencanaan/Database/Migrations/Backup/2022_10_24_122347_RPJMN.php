<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Query\Expression;

class RPJMN extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public $tahun;
    public $connection_schema;
    function __construct($tahun=null){
        $this->tahun=$tahun??2022;
        $this->connection_schema='sink_modul';
    } 

    public function up()
    {
       if(!Schema::connection($this->connection_schema)->hasTable('master.rpjmn_'.$this->tahun)){
            $tahun=$this->tahun;
            Schema::connection($this->connection_schema)->create('master.rpjmn_'.$this->tahun,function(Blueprint $table) use ($tahun){
                $table->id();
                $table->string('uuid')->unique()->default(DB::raw('(concat(\'dss_\',gen_random_uuid()))'));
                $table->bigInteger('id_parent')->nullable()->unsigned()->index();
                $table->string('context');
                $table->string('kode')->nullable()->index();
                $table->string('name')->nullable()->index();
                $table->jsonb('label')->default(new Expression('(\'[]\'::jsonb)'));
                $table->integer('tahun');
                $table->mediumText('keterangan')->nullable();
                $table->timestamps();
                $table->foreign('id_parent')->references('id')->on('master.rpjmn_'.$tahun)->onDelete('cascade')->onUpdate('cascade');

            });
       }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
