<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Query\Expression;

class RPJDIndikator extends Migration
{
    public $tahun;
    public $connection_schema;
    function __construct($tahun=null){
        $this->tahun=$tahun??2022;
        $this->connection_schema='sink_modul';
    } 

    public function up()
    {
        //
        if(!Schema::connection($this->connection_schema)->hasTable('master.rpjmd_ind_'.$this->tahun)){
            $tahun=$this->tahun;
            Schema::connection($this->connection_schema)->create('master.rpjmd_ind_'.$this->tahun,function(Blueprint $table) use ($tahun){
                $table->id();
                $table->string('uuid')->unique()->default(DB::raw('(concat(\'dss_\',gen_random_uuid()))')); 
                $table->integer('tahun');
                $table->string('kodepemda')->index();
                $table->bigInteger('rpjmd_id')->unsigned()->index();
                $table->string('name');
                $table->mediumText('target');
                $table->string('satuan');
                $table->mediumText('keterangan')->nullable();
                $table->jsonb('label')->default(new Expression('(\'[]\'::jsonb)'));
                $table->timestamps();
                $table->foreign('rpjmd_id')->references('id')->on('master.rpjmd_'.$tahun)->onDelete('cascade')->onUpdate('cascade');

            });
       }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
