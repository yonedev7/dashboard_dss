<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Query\Expression;

class KinerjaUrusan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public $tahun;
    public $connection_schema;
    function __construct($tahun=null){
        $this->tahun=$tahun??2022;
        $this->connection_schema='sink_modul';
    } 
    
    public function up()
    {
        //
        if(!Schema::connection($this->connection_schema)->hasTable('master.kinerja_urusan_'.$this->tahun)){
            $tahun=$this->tahun;
            Schema::connection($this->connection_schema)->create('master.kinerja_urusan_'.$this->tahun,function(Blueprint $table) use ($tahun){
                $table->id();
                $table->string('uuid')->unique()->default(DB::raw('(concat(\'dss_\',gen_random_uuid()))'));  
                $table->integer('tahun');
                $table->string('context');
                $table->bigInteger('id_parent')->nullable()->unsigned()->index();
                
                $table->string('urusan_kode')->unsigned()->index();
                $table->text('name')->nulable();
                $table->jsonb('label')->default(new Expression('(\'[]\'::jsonb)'));

                $table->mediumText('keterangan')->nullable();
                $table->timestamps();
                $table->foreign('urusan_kode')->references('kode')->on('master.urusan')->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('id_parent')->references('id')->on('master.kinerja_urusan_'.$tahun)->onDelete('cascade')->onUpdate('cascade');


            });
       }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
