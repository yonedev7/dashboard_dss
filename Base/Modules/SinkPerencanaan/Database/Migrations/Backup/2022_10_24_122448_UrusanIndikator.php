<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Query\Expression;

class UrusanIndikator extends Migration
{
    public $tahun;
    public $connection_schema;
    function __construct($tahun=null){
        $this->tahun=$tahun??2022;
        $this->connection_schema='sink_modul';
    } 

    public function up()
    {
        //
        if(!Schema::connection($this->connection_schema)->hasTable('master.kinerja_urusan_ind_'.$this->tahun)){
            $tahun=$this->tahun;
            Schema::connection($this->connection_schema)->create('master.kinerja_urusan_ind_'.$this->tahun,function(Blueprint $table) use ($tahun){
                $table->id();
                $table->string('uuid')->unique()->default(DB::raw('(concat(\'dss_\',gen_random_uuid()))'));  
                $table->integer('tahun');
                $table->bigInteger('kinerja_urusan_id')->unsigned();
                $table->boolean('with_dif_scope')->default(false);
                $table->string('name')->nullable()->index();
                $table->string('name_kota')->nullable()->index();
                $table->mediumText('target');
                $table->mediumText('target_kota')->nullable();
                $table->string('satuan');
                $table->string('satuan_kota')->nullable();
                $table->jsonb('label')->default(new Expression('(\'[]\'::jsonb)'));

                $table->mediumText('keterangan')->nullable();
                $table->timestamps();
                $table->foreign('kinerja_urusan_id')->references('id')->on('master.kinerja_urusan_'.$tahun)->onDelete('cascade')->onUpdate('cascade');


            });
       }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
