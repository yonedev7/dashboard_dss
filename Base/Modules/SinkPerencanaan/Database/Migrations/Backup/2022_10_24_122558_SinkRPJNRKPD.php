<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Query\Expression;

class SinkRPJNRKPD extends Migration
{
    public $tahun;
    public $connection_schema;
    function __construct($tahun=null){
        $this->tahun=$tahun??2022;
        $this->connection_schema='sink_modul';
    } 

    public function up()
    {
        //
       if(!Schema::connection($this->connection_schema)->hasTable('sink.rpjmn_ind_rkpd_nomen_'.$this->tahun)){
            $tahun=$this->tahun;
            Schema::connection($this->connection_schema)->create('sink.rpjmn_ind_rkpd_nomen_'.$this->tahun,function(Blueprint $table) use ($tahun){
                $table->id();
                $table->string('uuid')->unique()->default(DB::raw('(concat(\'dss_\',gen_random_uuid()))'));  
                $table->integer('tahun');
                $table->bigInteger('ind_rpjmn_id')->unsigned();
                $table->string('nomenklatur_rkpd')->unsigned();
                $table->string('urusan_kode')->unsigned()->index();
                $table->jsonb('label')->default(new Expression('(\'[]\'::jsonb)'));

                $table->mediumText('keterangan')->nullable();
                $table->bigInteger('user_created')->unsigned();
                $table->bigInteger('user_updated')->unsigned();
                $table->timestamps();
                $table->index(['ind_rpjmn_id','nomenklatur_rkpd','tahun'],'inx_'.'rpjmn_ind_rkpd_nomen_'.$this->tahun);
                $table->unique([
                    'ind_rpjmn_id','nomenklatur_rkpd','tahun','urusan_kode'
                ]);

                $table->foreign('ind_rpjmn_id')->references('id')->on('master.rpjmn_ind_'.$tahun)->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('nomenklatur_rkpd')->references('kode')->on('master.nomenklatur')->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('urusan_kode')->references('kode')->on('master.urusan')->onDelete('cascade')->onUpdate('cascade');

            });
       }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
