<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SinkUrusanRKPD extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public $tahun;
    public $connection_schema;
    function __construct($tahun=null){
        $this->tahun=$tahun??2022;
        $this->connection_schema='sink_modul';
    } 

    public function up()
    {
        //
       if(!Schema::connection($this->connection_schema)->hasTable('sink.urusan_rkpd_'.$this->tahun)){
            $tahun=$this->tahun;
            Schema::connection($this->connection_schema)->create('sink.urusan_rkpd_'.$this->tahun,function(Blueprint $table) use ($tahun){
                $table->id();
                $table->uuid('uuid')->unique();

                
                $table->bigInteger('urusan_id')->nullable()->unsigned();
                $table->string('kodepemda')->index();
                $table->integer('tahun');
                $table->bigInteger('ind_urusan_id')->unsigned();
                $table->bigInteger('ind_rkpd_id')->unsigned();
                $table->mediumText('label')->default('[]');
                $table->mediumText('keterangan')->nullable();
                $table->bigInteger('user_created')->unsigned();
                $table->bigInteger('user_updated')->unsigned();
                $table->timestamps();

                $table->unique([
                    'urusan_id','ind_urusan_id','ind_rkpd_id','kodepemda','tahun'
                ]);

                $table->foreign('ind_urusan_id')->references('id')->on('master.urusan_ind_'.$tahun)->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('ind_rkpd_id')->references('id')->on('master.rkpd_ind_'.$tahun)->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('urusan_id')->references('id')->on('master.urusan')->onDelete('cascade')->onUpdate('cascade');
            });
       }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
