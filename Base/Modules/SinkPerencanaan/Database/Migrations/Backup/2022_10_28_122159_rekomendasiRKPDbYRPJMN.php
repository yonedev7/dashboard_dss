<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Query\Expression;

class RekomendasiRKPDbYRPJMN extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public $tahun;
    public $connection_schema;
    function __construct($tahun=null){
        $this->tahun=$tahun??2022;
        $this->connection_schema='sink_modul';
    } 

    public function up()
    {
        //

        if(!Schema::connection($this->connection_schema)->hasTable('sink.rekom_rpjmn_'.$this->tahun)){
            $tahun=$this->tahun;
            Schema::connection($this->connection_schema)->create('sink.rekom_rpjmn_'.$this->tahun,function(Blueprint $table) use ($tahun){
                $table->id();
                $table->string('uuid')->unique()->default(DB::raw('(concat(\'dss_\',gen_random_uuid()))'));  
                $table->integer('tahun');
                $table->bigInteger('sink_id')->unsigned();
                $table->string('kodepemda')->index();
                $table->string('urusan_kode')->unsigned()->index();

                $table->jsonb('hightlight_rkpd_ind')->default(new Expression('(\'[]\'::jsonb)'));
                $table->jsonb('penilaian')->default(new Expression('(\'{}\'::jsonb)'));
                $table->mediumText('rekomendasi')->nullable();
                $table->integer('status')->default(0)->comment('0=def 1=locked');
                $table->bigInteger('user_created')->unsigned();
                $table->bigInteger('user_updated')->unsigned();
                $table->timestamps();
                $table->unique([
                    'sink_id','kodepemda','tahun'
                ]);

                $table->foreign('sink_id')->references('id')->on('sink.rpjmn_'.$tahun)->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('urusan_kode')->references('kode')->on('master.urusan')->onDelete('cascade')->onUpdate('cascade');



            });
       }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
