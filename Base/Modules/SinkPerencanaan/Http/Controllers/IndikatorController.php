<?php

namespace Modules\SinkPerencanaan\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use DB;
use Alert;
use Auth;
use Carbon\Carbon;
class IndikatorController extends SinkBaseController
{
  
    public function index($tahun,Request $request){
        $kodepemdas=($this->getKodePemda($tahun));

        
        $scope=$request->pemda_scope??'KAB-KOTA';
        if($scope=='KAB-KOTA'){
            
            $pemda=DB::table('master.master_pemda as mp')
            ->where(DB::RAW("LENGTH(mp.kodepemda)"),'>',3)
            ->whereIn('kodepemda',$kodepemdas)
            ->orderBy('nama_pemda')->get();



        }else{
            $pemda=DB::table('master.master_pemda as mp')
            ->where(DB::RAW("LENGTH(mp.kodepemda)"),'=',3)
            ->whereIn('kodepemda',$kodepemdas)
            ->orderBy('nama_pemda')->get();
        }
        
        return view('sinkperencanaan::home')->with(['pemda'=>$pemda,'scope'=>$scope]);


    }
    public function index2($tahun,Request $request){
        $kodepemdas=($this->getKodePemda($tahun));

        
        $scope=$request->pemda_scope??'KAB-KOTA';
        if($scope=='KAB-KOTA'){
            
            $pemda=DB::table('master.master_pemda as mp')
            ->where(DB::RAW("LENGTH(mp.kodepemda)"),'>',3)
            ->whereIn('kodepemda',$kodepemdas)
            ->orderBy('nama_pemda')->get();



        }else{
            $pemda=DB::table('master.master_pemda as mp')
            ->where(DB::RAW("LENGTH(mp.kodepemda)"),'=',3)
            ->whereIn('kodepemda',$kodepemdas)
            ->orderBy('nama_pemda')->get();
        }
        
        return view('sinkperencanaan::home2')->with(['pemda'=>$pemda,'scope'=>$scope]);


    }

    public function saveRekom($tahun,$kodeurusan,$kodepemda,Request $request){
        $my=Auth::guard('api')->User();

        $data=DB::connection($this->scmcon)->table('sink.rekom_indikator_'.$tahun)->where(
            [
                'tahun'=>$tahun,
                'urusan_kode'=>$kodeurusan,
                'kodepemda'=>$kodepemda
            ]
        )->first();

        if($data){
            DB::beginTransaction();
            try{

               $exist= DB::connection($this->scmcon)->table('sink.rekom_indikator_'.$tahun)->where(
                    [
                        'tahun'=>$tahun,
                        'sink_id'=>$request->data['sink_id'],
                        'urusan_kode'=>$kodeurusan,
                        'kodepemda'=>$kodepemda
                    ]
                )->first();

                if($exist){
                    DB::connection($this->scmcon)->table('sink.rekom_indikator_'.$tahun)->where(
                        [
                            'tahun'=>$tahun,
                            'sink_id'=>$request->data['sink_id'],
                            'urusan_kode'=>$kodeurusan,
                            'kodepemda'=>$kodepemda
                        ]
                    )->update(
                        [
                            'sink_id'=>$request->data['sink_id'],
                            'rekomendasi'=>$request->data['rekomendasi'],
                            'user_updated'=>$my->id,
                        ]
                    );

                }else{
                    DB::connection($this->scmcon)->table('sink.rekom_indikator_'.$tahun)->insert(
                        [
                            'tahun'=>$tahun,
                            'sink_id'=>$request->data['sink_id'],
                            'urusan_kode'=>$kodeurusan,
                            'kodepemda'=>$kodepemda,
                            'rekomendasi'=>$request->data['rekomendasi'],
                            'user_updated'=>$my->id,
                            'user_created'=>$my->id,

                        ]
                    );

                }
                
                
                DB::commit();
                $data=DB::connection($this->scmcon)->table('sink.rekom_indikator_'.$tahun)->where(
                    [
                        'tahun'=>$tahun,
                        'urusan_kode'=>$kodeurusan,
                        'kodepemda'=>$kodepemda,
                        'sink_id'=>$request->data['sink_id'],

                    ]
                )->first();

                return [
                    'code'=>200,
                    'data'=>$data,
                    'message'=>'',
                    'message_error'=>null,
                ];
    

            }catch(\Exception $e){
                DB::rollBack();
                return [
                    'code'=>500,
                    'data'=>null,
                    'message'=>'',
                    'message_error'=>$e->getMessage(),
                ];
    
                
            }
        }else{
            DB::beginTransaction();
            try{

                DB::connection($this->scmcon)->table('sink.rekom_indikator_'.$tahun)->insert(
                    [
                        'tahun'=>$tahun,
                        'sink_id'=>$request->data['sink_id'],
                        'urusan_kode'=>$kodeurusan,
                        'kodepemda'=>$kodepemda,
                        'rekomendasi'=>$request->data['rekomendasi'],
                        'user_created'=>$my->id,
                        'user_updated'=>$my->id,

                    ]
                );
                DB::commit();
                $data=DB::connection($this->scmcon)->table('sink.rekom_indikator_'.$tahun)->where(
                    [
                        'tahun'=>$tahun,
                        'urusan_kode'=>$kodeurusan,
                        'kodepemda'=>$kodepemda
                    ]
                )->first();
                return [
                    'code'=>200,
                    'data'=>$data,
                    'message'=>'',
                    'message_error'=>null,
                ];
    

            }catch(\Exception $e){
                DB::rollBack();
                return [
                    'code'=>500,
                    'data'=>null,
                    'message'=>'',
                    'message_error'=>$e->getMessage(),
                ];
    
                
            }

        }
    }

    public function indSinkNomen($tahun,$scope,Request $request){
        $time=Carbon::now();
        $my=Auth::Guard('api')->User();
        $exts=DB::connection($this->scmcon)->table('sink.ind_rkpd_nomen_'.$tahun)
        ->where([
            'id_indikator'=>$request->id_indikator,
            'nomenklatur_rkpd'=>$request->nomenklatur_rkpd
        ])->first();

        if($exts){
            $exts=DB::connection($this->scmcon)->table('sink.ind_rkpd_nomen_'.$tahun)
            ->where([
                'id_indikator'=>$request->id_indikator,
                'nomenklatur_rkpd'=>$request->nomenklatur_rkpd,
                'kodepemda'=>$request->kodepemda,
            ])->delete();
        }else{
            $exts=DB::connection($this->scmcon)->table('sink.ind_rkpd_nomen_'.$tahun)
            ->insert([
                'id_indikator'=>$request->id_indikator,
                'nomenklatur_rkpd'=>$request->nomenklatur_rkpd,
                'user_created'=>$my->id,
                'user_updated'=>$my->id,
                'kodepemda'=>$request->kodepemda,
                'urusan_kode'=>'1.03',
                'updated_at'=>$time,
                'tahun'=>$tahun,
                'created_at'=>$time,            
            ]);
            
        
        }


        return [
            'code'=>200,
            'data'=>DB::connection($this->scmcon)->table('sink.ind_rkpd_nomen_'.$tahun)
            ->where('id_indikator',$request->id_indikator)->selectRaw("nomenklatur_rkpd")->get()->pluck('nomenklatur_rkpd')
        ];

    }

    public function master_pemda($tahun,$kodepemda,Request $request){
        $pemda=DB::table('master.master_pemda')->where('kodepemda',$kodepemda)->first();

        $scope=strlen($kodepemda)<=3?'PROVINSI':'KAB-KOTA';
        $data=DB::connection($this->scmcon)->table('master.indikator_'.$tahun)
        ->whereIn('context',['RPJMD','RAD-AMPL'])
        ->where('kodepemda',$kodepemda)->orderBy('id','desc')->get();

        $nomen=DB::connection($this->scmcon)
        ->table('master.nomenklatur')
        ->where('only_prov',strlen($kodepemda)<=3?TRUE:FALSE)
        ->orderBy('kode','asc')->get();

        foreach($data as $k=>$d){
            $data[$k]->nomen_tag=DB::connection($this->scmcon)->table('sink.ind_rkpd_nomen_'.$tahun)
            ->where('kodepemda',$kodepemda)
            ->where('id_indikator',$d->id)->selectRaw('nomenklatur_rkpd')->get()->pluck('nomenklatur_rkpd');
        }

       return view('sinkperencanaan::master_pemda')->with(['pemda'=>$pemda,'data'=>$data,'scope'=>$scope,'nomen'=>$nomen]);



    }

    public function master($tahun,$scope,Request $request){
        $data=DB::connection($this->scmcon)->table('master.indikator_'.$tahun)
        ->whereNotIn('context',['RPJMD','RAD-AMPL'])
        // ->where('with_dif_scope',$scope=='KAB-KOTA'?TRUE:FALSE)
        ->orderBy('id','desc')->get();


        $nomen=DB::connection($this->scmcon)
        ->table('master.nomenklatur')
        ->where('only_prov',$scope=='PROVINSI'?TRUE:FALSE)
        ->orderBy('kode','asc')->get();



        foreach($data as $k=>$d){
            $data[$k]->nomen_tag=DB::connection($this->scmcon)->table('sink.ind_rkpd_nomen_'.$tahun)
            ->where('kodepemda',NULL)
            ->where('id_indikator',$d->id)->selectRaw('nomenklatur_rkpd')->get()->pluck('nomenklatur_rkpd');
        }



       return view('sinkperencanaan::master')->with(['data'=>$data,'scope'=>$scope,'nomen'=>$nomen]);
    }


    public function delete($tahun,$scope,Request $request){
        $exist=DB::connection($this->scmcon)->table('master.indikator_'.$tahun)->where([
            'id'=>$request->id,
            'with_dif_scope'=>$scope=='KAB-KOTA'?TRUE:FALSE,

        ])->first();

        if($exist){
            DB::connection($this->scmcon)->table('master.indikator_'.$tahun)->where([
                'id'=>$exist->id,
            ])->delete();
            Alert::success('Berhasil');
            return back();
        }
    }

    public function storeUpdate($tahun,$scope,Request $request){
        if($request->id){
            $x=DB::connection($this->scmcon)->table('master.indikator_'.$tahun)->where('id',$request->id)->update([

                'name'=>$request->name,
                'target'=>$request->target,
                'satuan'=>$request->satuan,
                'context'=>$request->context,
                'urusan_kode'=>'1.03',
                'tahun'=>$tahun,
                'kodepemda'=>$request->kodepemda,
                'with_dif_scope'=>$scope=='KAB-KOTA'?TRUE:FALSE,
                'keterangan_sumber'=>$request->keterangan_sumber,
                'keterangan'=>$request->keterangan,
            ]);

            if($x){
                Alert::success('Berhasil');
                return back();
            }else{
                Alert::error('Gagal');

                return back();
            }

            
        }else{

            $x=DB::connection($this->scmcon)->table('master.indikator_'.$tahun)->insertGetId([
                'name'=>$request->name,
                'target'=>$request->target,
                'satuan'=>$request->satuan,
                'context'=>$request->context,
                'urusan_kode'=>'1.03',
                'tahun'=>$tahun,
                'kodepemda'=>$request->kodepemda,

                'with_dif_scope'=>$scope=='KAB-KOTA'?TRUE:FALSE,
                'keterangan_sumber'=>$request->keterangan_sumber,
                'keterangan'=>$request->keterangan,
            ]);

            if($x){
                Alert::success('Berhasil');
                return back();
            }else{
                Alert::error('Gagal');

                return back();
            }
        }
    }
   
}
