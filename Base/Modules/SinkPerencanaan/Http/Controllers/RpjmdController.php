<?php

namespace Modules\SinkPerencanaan\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
class RpjmdController extends SinkBaseController
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

    public function nomenSink($tahun,$kodeurusan){

        $data=DB::connection($this->scmcon)->table('master.rpjmd_ind_'.$tahun)->get();
        return view('sinkperencanaan::nomen.rpjmn')->with([
            'data'=>$data
        ]);

    }

    public function sinkNomenTag($tahun,Request $request){

    }

    public function index($tahun)
    {
        $data=DB::connection('sink_modul')->table('master.rpjmn_'.$tahun)
        ->where('context','PN')->oRwhere('context','MAJOR PROJECT')->orderBy('id','asc')->get();
        return view('sinkperencanaan::rpjmn.master.index')->with(['data'=>$data]);
    }
    public function get_ind_pp($tahun,$id){
        $data=DB::connection('sink_modul')->table('master.rpjmn_ind_'.$tahun)
        ->where('context','PP')->orderBy('id','asc')->get();
        return $data;
    }

    public function get_pp($tahun,$id){
        $data=DB::connection('sink_modul')->table('master.rpjmn_'.$tahun)
        ->where('context','PP')->orderBy('id','asc')->get();
        return $data;
    }

   
    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('sinkperencanaan::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('sinkperencanaan::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('sinkperencanaan::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
