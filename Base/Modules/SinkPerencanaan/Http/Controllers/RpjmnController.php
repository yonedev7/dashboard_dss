<?php

namespace Modules\SinkPerencanaan\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Auth;
class RpjmnController extends SinkBaseController
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

    public function nomenSink($tahun,$kodeurusan){


        $data=DB::connection($this->scmcon)->table('master.rpjmn_ind_'.$tahun)->get();
        $nomen=DB::connection($this->scmcon)->table('master.nomenklatur')->where('kode','ilike',$kodeurusan.'%')->orderBy('kode')->get();


        foreach($data as $key=>$d){
            $data[$key]->nomen_tag=DB::connection($this->scmcon)->table('sink.rpjmn_ind_rkpd_nomen_'.$tahun.' as ss')->join('master.nomenklatur as nm','nm.kode','=','ss.nomenklatur_rkpd')->selectRaw('nm.*')->get();
            $data[$key]->draf=false;
        }
        
        return view('sinkperencanaan::nomen.rpjmn')->with([
            'data'=>$data,
            'nomen'=>$nomen,
            'kodeurusan'=>$kodeurusan,
        ]);

    }

    public function sinkRPJMD($tahun,$kodeurusan,$kodepemda,Request $request){
        $ids=[];

        foreach($request->rpjmd??[] as $k=>$d){

            $exist=DB::connection($this->scmcon)
            ->table('sink.ind_rpjmd_rpjmn_'.$tahun)->where([
                'id_rpjmn'=>$request->rpjmn_id,
                'kodepemda'=>$kodepemda,
                'tahun'=>$tahun
            ])->where('id_rpjmd',$d['id'])->first();

            if($exist){
                $ids[]=$exist->id;
            }else{
                $new=DB::connection($this->scmcon)
                ->table('sink.ind_rpjmd_rpjmn_'.$tahun)
                ->insertGetId([
                    'id_rpjmn'=>$request->rpjmn_id,
                    'kodepemda'=>$kodepemda,
                    'id_rpjmd'=>$d['id'],
                    'tahun'=>$tahun
                ]);

                if($new){
                    $ids[]=$new;
                }

            }
    
        }

       $del= DB::connection($this->scmcon)
        ->table('sink.ind_rpjmd_rpjmn_'.$tahun)->where([
            'id_rpjmn'=>$request->rpjmn_id,
            'kodepemda'=>$kodepemda,
            'tahun'=>$tahun
        ])->whereNotIn('id',$ids)->delete();

          

        return [
            'code'=>200,
            'data'=>DB::connection($this->scmcon)
            ->table('sink.ind_rpjmd_rpjmn_'.$tahun.' as irp')
            ->join('master.indikator_'.$tahun.' as i',[
                ['i.id','=','irp.id_rpjmd'],
                ['i.kodepemda','=',DB::raw("'".$kodepemda."'")]
            ])
            ->selectRaw("i.*")
            ->where([
                'irp.id_rpjmn'=>$request->rpjmn_id,
                'irp.kodepemda'=>$kodepemda,
                'irp.tahun'=>$tahun
            ])->get()
        ];




    }

    public function sinkNomenTag($tahun,Request $request){
        $data=$request->data;
        $ids=[];
        $my=Auth::guard('api')->User();
        foreach($data['nomen_tag']??[] as $k=>$d){
            $x=DB::connection($this->scmcon)->table('sink.rpjmn_ind_rkpd_nomen_'.$tahun)->updateOrInsert(
                [
                'idn_rpjmn_id'=>$data['id'],
                'nomenklatur_rkpd'=>$d['kode'],
                'urusan_kode'=>$request->kodeurusan,
                'tahun'=>$tahun,
            ],
            [
                'ind_rpjmn_id'=>$data['id'],
                'uuid'=>uniqid(),
                'nomenklatur_rkpd'=>$d['kode'],
                'urusan_kode'=>$request->kodeurusan,
                'tahun'=>$tahun,
                'user_created'=>$my->id,
                'user_updated'=>$my->id,
            ]
        
            );

            // dd($x);
        }

        if(count($ids)){
            DB::connection($this->scmcon)->table('sink.rpjmn_ind_rkpd_nomen_'.$tahun)
            ->where('idn_rpjmn_id',$data['id'])
            ->where('urusan_kode',$request->kodeurusan)
            ->whereNotIn('id',$ids)
            ->delete();

        }else{
            // DB::connection($this->scmcon)->table('sink.rpjmn_ind_rkpd_nomen_'.$tahun)
            // ->where('idn_rpjmn_id',$data['id'])
            // ->where('urusan_kode',$request->kodeurusan)->delete();
        }

    }

    public function saveRekom($tahun,$kodeurusan,$kodepemda,Request $request){
        $my=Auth::guard('api')->User();

        $data=DB::connection($this->scmcon)->table('sink.rekom_rpjmn_'.$tahun)->where(
            [
                'tahun'=>$tahun,
                'urusan_kode'=>$kodeurusan,
                'kodepemda'=>$kodepemda
            ]
        )->first();

        if($data){
            DB::beginTransaction();
            try{

                DB::connection($this->scmcon)->table('sink.rekom_rpjmn_'.$tahun)->where(
                    [
                        'tahun'=>$tahun,
                        'sink_id'=>$request->data['sink_id'],
                        'urusan_kode'=>$kodeurusan,
                        'kodepemda'=>$kodepemda
                    ]
                )->update(
                    [
                        'rekomendasi'=>$request->data['rekomendasi'],
                        'user_updated'=>$my->id,
                    ]
                );
                
                DB::commit();
                $data=DB::connection($this->scmcon)->table('sink.rekom_rpjmn_'.$tahun)->where(
                    [
                        'tahun'=>$tahun,
                        'urusan_kode'=>$kodeurusan,
                        'kodepemda'=>$kodepemda,
                        'sink_id'=>$request->data['sink_id'],

                    ]
                )->first();
                return [
                    'code'=>200,
                    'data'=>$data,
                    'message'=>'',
                    'message_error'=>null,
                ];
    

            }catch(\Exception $e){
                DB::rollBack();
                return [
                    'code'=>500,
                    'data'=>null,
                    'message'=>'',
                    'message_error'=>$e->getMessage(),
                ];
    
                
            }
        }else{
            DB::beginTransaction();
            try{

                DB::connection($this->scmcon)->table('sink.rekom_rpjmn_'.$tahun)->insert(
                    [
                        'tahun'=>$tahun,
                        'sink_id'=>$request->data['sink_id'],
                        'urusan_kode'=>$kodeurusan,
                        'kodepemda'=>$kodepemda,
                        'rekomendasi'=>$request->data['rekomendasi'],
                        'user_created'=>$my->id,
                        'user_updated'=>$my->id,

                    ]
                );
                DB::commit();
                $data=DB::connection($this->scmcon)->table('sink.rekom_rpjmn_'.$tahun)->where(
                    [
                        'tahun'=>$tahun,
                        'urusan_kode'=>$kodeurusan,
                        'kodepemda'=>$kodepemda
                    ]
                )->first();
                return [
                    'code'=>200,
                    'data'=>$data,
                    'message'=>'',
                    'message_error'=>null,
                ];
    

            }catch(\Exception $e){
                DB::rollBack();
                return [
                    'code'=>500,
                    'data'=>null,
                    'message'=>'',
                    'message_error'=>$e->getMessage(),
                ];
    
                
            }

        }
    }


    public function index($tahun)
    {
        $data=DB::connection('sink_modul')->table('master.rpjmn_'.$tahun)
        ->where('context','PN')->oRwhere('context','MAJOR PROJECT')->orderBy('id','asc')->get();
        foreach($data as $k=>$d){
            $data[$k]->child_context=['PP'];
            $data[$k]->with_child_append=false;
            $data[$k]->indikator=[];

            if($d->context=='MAJOR PROJECT'){
                $data[$k]->indikator=DB::connection('sink_modul')->table('master.rpjmn_ind_'.$tahun)
                ->where('rpjmn_id',$d->id)->get();
            }

        }

        return view('sinkperencanaan::rpjmn.master.index')->with(['data'=>$data]);
    }
    public function loadNomen($tahun,$id,Request $request){

        $data=DB::connection('sink_modul')->table('master.rpjmn_'.$tahun)
        ->whereIn('context',$request->context??[])
        ->where('id_parent',$id)
        ->orderBy('id','asc')->get();
        
        foreach($data as $k=>$d){
            $data[$k]->with_child_append=true;
            $data[$k]->expend_child=false;
            $data[$k]->childs=[];
            $data[$k]->indikator=[];

            switch ($d->context) {
                case 'PP':
                $data[$k]->child_context=['KP'];
                break;

                case 'KP':
                    $data[$k]->child_context=['PROP'];
                    # code...
                    break;
                
                default:
                    $data[$k]->child_context=[];
    
                    # code...
                    break;
            }
            if($d->context=='PROP'){
                $data[$k]->indikator=DB::connection('sink_modul')->table('master.rpjmn_ind_'.$tahun)
                ->where('rpjmn_id',$d->id)->get();
            }
        }
        return $data;
    }


    public function editNomen($tahun,Request $request){
        $time=Carbon::now();

        $parent=DB::connection('sink_modul')->table('master.rpjmn_'.$tahun)
        ->where('context',$request->parent['context'])
        ->where('id',$request->parent['id'])->first();

        if($parent){
            DB::beginTransaction();
            try{
                DB::connection('sink_modul')->table('master.rpjmn_'.$tahun)->where('id',$request->parent['id'])->update([
                    'name'=>$request->data['name'],
                    'keterangan'=>$request->data['keterangan'],
                    'updated_at'=>$time,
                ]);
                DB::commit();
            }catch(\Exception $e){
                DB::rollBack();

                return [
                    'code'=>500,
                    'data'=>null,
                    'message'=>'',
                    'message_error'=>$e->getMessage(),
                ];
    

            }


        }else{

        }

    }
    public function addNomen($tahun,Request $request){
        $time=Carbon::now();
        if($request->data['context']=='PN' OR $request->data['context']=='MAJOR PROJECT'){
            $parent=(Object)[
                'id'=>null
            ];
        }else{
            $parent=DB::connection('sink_modul')->table('master.rpjmn_'.$tahun)
            ->where('context',$request->parent['context'])
            ->where('id',$request->parent['id'])->first();
        }
       
        if($parent){
            DB::beginTransaction();
            try{
                DB::connection('sink_modul')->table('master.rpjmn_'.$tahun)->insert([
                    'id_parent'=>$parent->id,
                    'name'=>$request->data['name'],
                    'tahun'=>$tahun,
                    'context'=>$request->data['context'],
                    'keterangan'=>$request->data['keterangan'],
                    'created_at'=>$time,
                    'updated_at'=>$time,
                ]);
                DB::commit();
            }catch(\Exception $e){
                DB::rollBack();

                return [
                    'code'=>500,
                    'data'=>null,
                    'message'=>'',
                    'message_error'=>$e->getMessage(),
                ];
    

            }


            return [
                'code'=>200,
                'data'=>null,
                'message'=>'Success',
                'message_error'=>'',
            ];



        }
        
        

    }

   
}
