<?php

namespace Modules\SinkPerencanaan\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Auth;
use DB;
class SinkBaseController extends Controller
{
    public $scmcon='sink_modul';


    public function getKodePemda($tahun){
        $_user=Auth::User();
        $roles=$_user->getRoleNames()->toArray();
        if(in_array('PEMDA',$roles)){
            return [$_user->kodepemda];
        }else if(in_array('TACT-REGIONAL',$roles)){
            return DB::table('master.master_pemda')->where('regional_1',$_user->regional)->get()->pluck('kodepemda')->toArray();
        }else{
            return DB::table('master.master_pemda')->where('kodepemda','!=','0')->get()->pluck('kodepemda')->toArray();
        }
    }
    
}
