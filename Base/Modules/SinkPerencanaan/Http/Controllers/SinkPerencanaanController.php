<?php

namespace Modules\SinkPerencanaan\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;

class SinkPerencanaanController extends SinkBaseController
{
    

    public function index_sink_rpjmd($tahun,$kodeurusan,$kodepemda){
        $pemda=DB::table('master.master_pemda')->where('kodepemda',$kodepemda)->first();

        $rpjmn=DB::connection('sink_modul')
        ->table('master.indikator_'.$tahun.' as ind')
        ->where('ind.urusan_kode',$kodeurusan)
        ->whereIn('ind.context',['RPJMN'])
        ->groupBy('ind.id')
        ->selectRaw('ind.id,MAX(ind.context) as context,max(ind.target) as target,max(ind.satuan) as satuan,max(ind.name) as name,max(ind.keterangan_sumber) as keterangan_sumber,max(ind.keterangan) as keterangan')
        ->get()->toArray();

        foreach($rpjmn as $k=>$d){
            $rpjmn[$k]->rpjmd=DB::connection('sink_modul')
            ->table('sink.ind_rpjmd_rpjmn_'.$tahun.' as rpm')
            ->join('master.indikator_'.$tahun." as i",[['i.id','=','rpm.id_rpjmd'],['i.kodepemda','=',DB::raw("'".$kodepemda."'")]])
            ->selectRaw("i.*")
            ->where('rpm.kodepemda',$kodepemda)->where('rpm.tahun',$tahun)->where('rpm.id_rpjmn',$d->id)->get()->toArray();
        }


        $rpjmd=DB::connection('sink_modul')
        ->table('master.indikator_'.$tahun.' as ind')
        ->where('ind.urusan_kode',$kodeurusan)
        ->whereIn('ind.context',['RPJMD'])
        ->whereRaw("(ind.kodepemda='{$kodepemda}')")
        ->groupBy('ind.id')
        ->selectRaw('ind.id,MAX(ind.context) as context,max(ind.target) as target,max(ind.satuan) as satuan,max(ind.name) as name,max(ind.keterangan_sumber) as keterangan_sumber,max(ind.keterangan) as keterangan')
        ->get()->toArray();


        return view('sinkperencanaan::sink.rpjmd')->with(['pemda'=>$pemda,'rpjmn'=>$rpjmn,'rpjmd'=>$rpjmd,'kodeurusan'=>$kodeurusan]);


    }

    
    public function menu($tahun){
        
        $data=DB::table('master.master_pemda')->where('kodepemda','ilike','0%')->get();
        return view('sinkperencanaan::index',['data'=>$data]);
    }

    public function index($tahun,$kodeurusan='1.03',$kodepemda){

        $pemda=DB::table('master.master_pemda')->where('kodepemda',$kodepemda)->first();

        if($pemda){
            if(str_contains($pemda->nama_pemda,'KOTA')){
                $for='KOTA';
            }else if(str_contains($pemda->nama_pemda,'KABUPATEN')){
                $for='KOTA';
            }else if(str_contains($pemda->nama_pemda,'PROVINSI')){
                $for='PROVINSI';
            }
        }

        $SINK='RKP';

        $rpjmn=DB::connection('sink_modul')
        ->table('master.indikator_'.$tahun.' as ind')
        ->join('sink.ind_rkpd_nomen_'.$tahun.' as n','n.id_indikator','=','ind.id')
        ->where('ind.urusan_kode',$kodeurusan)
        ->whereRaw("( (ind.kodepemda is null ) OR (ind.kodepemda='{$kodepemda}'))")
        ->groupBy('ind.id')
        ->where('ind.context','ilike',$SINK.'%')

        ->selectRaw('ind.id,MAX(ind.context) as context,max(ind.target) as target,max(ind.satuan) as satuan,max(ind.name) as name,max(ind.keterangan_sumber) as keterangan_sumber,max(ind.keterangan) as keterangan')
        ->get()->toArray();


        foreach($rpjmn as $k=>$d){
            $rekom=DB::connection($this->scmcon)->table('sink.rekom_indikator_'.$tahun)->where([
                'kodepemda'=>$kodepemda,
                'tahun'=>$tahun,
                'sink_id'=>$d->id
            ])->first();

            if($rekom){
                $rpjmn[$k]->rekomendasi=$rekom;
            }else{
                $rpjmn[$k]->rekomendasi=(Object)[
                    'uuid'=>null,
                    'tahun'=>$tahun,
                    'sink_id'=>$d->id,
                    'kodepemda'=>$kodepemda,
                    'rekomendasi'=>null,
                    'status'=>0,
                    'hightlight_rkpd_ind'=>[],
                    'penilaian'=>[
                        'deviasi_code'=>0,
                        'value'=>0,
                        'value_text'=>null
                    ]
                ];
            }
           

            $rpjmn[$k]->subkegiatan=DB::connection('sink_modul')
            ->table('sink.ind_rkpd_nomen_'.$tahun.' as mrpjmn')
            ->join('master.nomenklatur as nm',[
                ['nm.kode','=','mrpjmn.nomenklatur_rkpd'],
                ['nm.only_prov','=',DB::raw(strlen($kodepemda)>3?'false':'true')]
            ])
            ->leftJoin('master.rkpd_'.$tahun.' as rkpd',[['rkpd.kode','=','nm.kode'],['rkpd.kodepemda','=',DB::raw("'".$kodepemda."'")],['rkpd.tahun','=',DB::raw($tahun)]])
            ->where('nm.kode','ilike',$kodeurusan.'%')
            ->where('mrpjmn.id_indikator',$d->id)
            ->orderBy('nm.kode','asc')
            ->selectRaw("nm.*,rkpd.pagu,rkpd.uuid as uuid_rkpd")->get()->toArray();

            foreach($rpjmn[$k]->subkegiatan as $sk =>$s){
                $rpjmn[$k]->subkegiatan[$sk]->indikator=DB::connection('sink_modul')
                ->table('sink.ind_rkpd_nomen_'.$tahun.' as mrpjmn')
                ->leftJoin('master.rkpd_'.$tahun.' as rkpd',[['rkpd.kodepemda','=',DB::raw("'".$kodepemda."'")],['mrpjmn.nomenklatur_rkpd','=','rkpd.kode']])
                ->join("master.rkpd_ind_".$tahun.' as rkpd_ind','rkpd_ind.rkpd_id','=','rkpd.id')
                ->selectRaw(
                    'rkpd_ind.*'
                )->orderBy('rkpd.kode','asc')
                ->where('mrpjmn.nomenklatur_rkpd','=',$s->kode)
                ->where('mrpjmn.id_indikator',$d->id)
                ->where('rkpd.kodepemda',$kodepemda)
                ->where('rkpd.uuid','=',$s->uuid_rkpd)
                ->get();
                foreach( $rpjmn[$k]->subkegiatan[$sk]->indikator as $x=>$xx){
                    $rpjmn[$k]->subkegiatan[$sk]->indikator[$x]->label=json_decode($xx->label,true);
                }

            }

        }


        $rpjmn=array_filter((array)$rpjmn,function($el){
                return (count((array)$el->subkegiatan))?true:false;            
        });
        

        if($pemda){
            return view('sinkperencanaan::sink.indikator')->with([
                '__kodeurusan'=>$kodeurusan,
                '__kodepemda'=>$kodepemda,
                'pemda'=>$pemda,
                'ind_listed'=>array_values($rpjmn),
                '__url_rekom'=>route('api.mod-sink.m.indikator.rekom.save',['tahun'=>$tahun,'kodeurusan'=>$kodeurusan,'kodepemda'=>$kodepemda])
            ]);
    
        }
        // DB::connection('modul_sink')->table('master.rkpd_'.$tahun)->where('kodepemda',$kodepemda)
        
    }


}
