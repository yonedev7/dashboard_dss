<?php

namespace Modules\SinkPerencanaan\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use Alert;
use Carbon\Carbon;
use Auth;
class SumberIndikatorController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

    public function index2($tahun){
        return view('home');
    }

    public function index($tahun,$context)
    {
      
        $tahun_list_5=[$tahun-3,$tahun-2,$tahun-1,$tahun,$tahun+1];


        $data=DB::connection('sink_modul')->table('master.sumber_indikator_pusat_'.$tahun)
        ->where('context','ilike', $context.'%')->where('parent_id',null)->get();
        foreach($data as $key=>$d){
            $data[$key]->ind=DB::connection('sink_modul')->table('master.indikator_'.$tahun)->where('sumber_id',$d->id)->get();
            $data[$key]->level=0;
            $data[$key]->sub=$this->loopDown($d->id,$tahun, $context,1,true);
        }

        return view('sinkperencanaan::master.sumber_ind')->with(['data'=>$data,'context'=>$context,'kodepemda'=>null,'tahun_list'=>$tahun_list_5]);
    }

    public function indexPemda($tahun,$context,$kodepemda)
    {
      
        $data=DB::connection('sink_modul')->table('master.sumber_indikator_pusat_'.$tahun)
        ->where('context','ilike', $context.'%')->where('parent_id',null)->get();
        foreach($data as $key=>$d){
            $data[$key]->ind=DB::connection('sink_modul')->table('master.indikator_'.$tahun)->where('sumber_id',$d->id)->get();;
            $data[$key]->level=0;
            $data[$key]->sub=$this->loopDown($d->id,$tahun, $context,1);
        }

        return view('sinkperencanaan::master.sumber_ind')->with(['data'=>$data,'context'=>$context,'kodepemda'=>$kodepemda]);
    }

    public function loopDown($id_parent,$tahun,$prefix_context,$l=1,$with_ind=false,$kodepemda=null){
        $data=DB::connection('sink_modul')->table('master.sumber_indikator_pusat_'.$tahun)
        ->where('context','ilike',$prefix_context.'%')
        ->where('parent_id',$id_parent);

        if($kodepemda){
            $data=$data->where('kodepemda',$kodepemda);
        }
        $data=$data->get();
        foreach($data as $key=>$d){
            if($with_ind){
                $data[$key]->ind=DB::connection('sink_modul')->table('master.indikator_'.$tahun)->where('sumber_id',$d->id)->get();;

            }
            $data[$key]->level=$l;
            $data[$key]->sub=$this->loopDown($d->id,$tahun,$prefix_context,$l+1,$with_ind,$kodepemda);
        }
        return $data;
    }

    public function loopUp($id_parent,$tahun,$prefix_context,$l=1,$with_ind=false,$kodepemda){

        $data=DB::connection('sink_modul')->table('master.sumber_indikator_pusat_'.$tahun)
        ->where('context','ilike',$prefix_context.'%')
        ->where('id',$id_parent)->get();
        if($kodepemda){
            $data=$data->where('kodepemda',$kodepemda);
        }
        $data=$data->get();
        foreach($data as $key=>$d){
            if($with_ind){
                $data[$key]->ind=DB::connection('sink_modul')->table('master.indikator_'.$tahun)->where('sumber_id',$d->id)->get();

            }
            $data[$key]->level=$l;
            $data[$key]->sub=$this->loopUp($d->parent_id,$tahun,$prefix_context,$l+1,$with_ind,$kodepemda);
        }
        return $data;

    }

    public function addInd($tahun,$context,Request $request){
        $my=Auth::User();
        $now=Carbon::now();
        DB::beginTransaction();
          try{
            DB::connection('sink_modul')->table('master.indikator_'.$tahun)->insert([
                'context'=>$request->context,
                'name'=>$request->name,
                'sumber_id'=>$request->parent_id,
                'urusan_kode'=>'1.03',
                'target'=>$request->target,
                'target1'=>$request->target1,
                'target2'=>$request->target2,
                'target3'=>$request->target3,
                'target4'=>$request->target4,
                'satuan'=>$request->satuan,
                'type'=>$request->type,
                'type_pemda'=>$request->for,
                'tahun'=>$tahun,
                'updated_at'=>$now,
                'created_at'=>$now,
            ]
            );
            Alert::success('Berhasil');
            DB::commit();

          }catch(\Exception $e){
            DB::rollback();
            Alert::error('Gagal',$e->getMessage());
            return back()->withInput();
             
          }
          return back();

    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('sinkperencanaan::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store($tahun,$context,Request $request)
    {
        //
        $my=Auth::User();
        $now=Carbon::now();
        DB::beginTransaction();
          try{
            DB::connection('sink_modul')->table('master.sumber_indikator_pusat_'.$tahun)->insert([
                'context'=>$request->context,
                'uraian'=>$request->uraian,
                'parent_id'=>$request->parent_id,
                'urusan_kode'=>'1.03',
                'user_created'=>$my->id,
                'user_updated'=>$my->id,
                'tahun'=>$tahun,
                'updated_at'=>$now,
                'created_at'=>$now,
            ]
            );
            Alert::success('Berhasil');
            DB::commit();

          }catch(\Exception $e){
            DB::rollback();
            Alert::error('Gagal',$e->getMessage());
            return back()->withInput();
             
          }
          return back();

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('sinkperencanaan::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('sinkperencanaan::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update($tahun,$context,Request $request)
    {
        //
        $my=Auth::User();
        $now=Carbon::now();
        DB::beginTransaction();
        try{
          DB::connection('sink_modul')->table('master.sumber_indikator_pusat_'.$tahun)->where('id',$request->id)
          ->where('context','ilike',$context.'%')->update([
            'uraian'=>$request->uraian,
            'updated_at'=>$now,
            'user_updated'=>$my->id]);
          Alert::success('Berhasil');
          DB::commit();

        }catch(\Exception $e){
          DB::rollback();
          Alert::error('Gagal',$e->getMessage());
          return back()->withInput();
           
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function delete($tahun,$context,Request $request)
    {
        DB::beginTransaction();
          try{
            DB::connection('sink_modul')->table('master.sumber_indikator_pusat_'.$tahun)->where('id',$request->id)
            ->where('context','ilike',$context.'%')->delete();
            Alert::success('Berhasil');
            DB::commit();

          }catch(\Exception $e){
            DB::rollback();
            Alert::error('Gagal',$e->getMessage());
            return back()->withInput();
             
          }
          return back();
    }
}
