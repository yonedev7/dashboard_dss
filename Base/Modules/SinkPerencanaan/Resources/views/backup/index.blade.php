@extends('adminlte::page')

@section('content')
   <div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="card rounded shadow mt-2">
                <div class="card-body ">
                    <h3>MASTER RPJMN</h3>
                    <a href="{{route('mod-sink.m.rpjmn',['tahun'=>$StahunRoute])}}" class="btn btn-primary text-white">Master</a>
                    <a href="{{route('sink.rpjmn',['tahun'=>$StahunRoute,'kodeurusan'=>'1.03'])}}" class="btn btn-success text-white">Sinkronisasi Nomenklatur RKPD</a>
    
                    
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card rounded shadow mt-2">
                <div class="card-body ">
                    <h3>INDIKATOR URUSAN WAJIB</h3>
                    <a href="" class="btn btn-primary text-white">Master</a>
                    <a href="" class="btn btn-success text-white">Sinkronisasi Nomenklatur RKPD</a>
    
                    
                </div>
            </div>
        </div>
       
       
    </div>
        <div id="d-a" class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <input type="text" class="form-control" v-model="input">
                    </div>
                </div>
            </div>
       
            <div class="col-md-4" v-for="itm in mdata">
                <div class="card">
                    <div class="card-body">
                        <h4>@{{itm.nama_pemda}}</h4>
                        <hr>
                        <div class="btn-group">
                            <a v-bind:href="('{{route('mod-sink.rpjmn.index',['tahun'=>$StahunRoute,'kodeurusan'=>'1.03','kodepemda'=>'xx'])}}').replace('xx',itm.kodepemda)" class="btn btn-primary">RPJMN</a>
                            <a class="btn btn-primary">URUSAN</a>

                        </div>
                    </div>
                </div>
            </div>
       
        </div>
   </div>
@endsection

@section('js')
<script>
    var App=new Vue({
        el:'#d-a',
        data:{
            data:<?=json_encode($data)?>,
            input:''
        },
        computed:{
            mdata:function(){

                return this.data.filter(el=>{
                    return (''||el.nama_pemda).toLowerCase().includes((''||this.input).toLowerCase());
                });
            }
        }
    });

</script>

@stop