@extends('adminlte::page')
@section('content_header')
    <h4>Sinkronisasi RKP </h4>
@stop


@section('content')
<div class="container-fluid">
    <div id="app">
        <div class="row">
            <div class="col-md-7 bg-white rounded-right pt-2">
                <div v-if=" data[page_active]!=undefined">
                    <h4>@{{ data[page_active].name}} <small>@{{ data[page_active].satuan}}</small></h4>
                    <p>@{{ data[page_active].target}}</p>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="">
                            <thead class="thead-dark sticky-top" style="">
                                <tr>
                                    <th style="width:200px;">Nomenklatur</th>
                                    <th>Nama</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="nm,nmi in data[page_active].nomen_tag">
                                    <td>@{{nm.kode}}</td>
                                    <td>@{{nm.name}}</td>
                                </tr>
                            </tbody>
                        </table>
                        
                    </div>

                   
        
                </div>
                <div class="sticky-top" style="top:75px ">
                    <div class="btn-group">
                        <button class="btn btn-primary" @click="page_active-=1"   v-if="page_active >=(data.length-1)"><i class="fa fa-arrow-left"></i></button>
                        <button class="btn btn-info">@{{(page_active+1)+' / '+data.length}}</button>
                        <button class="btn btn-primary " @click="submitForm" v-if="data[page_active].draf==true">Simpan</button>
                        <button class="btn btn-primary" @click="page_active+=1" v-if="page_active<(data.length-1)"><i class="fa fa-arrow-right"></i></button>

                    </div>
                </div>
            
            </div>
            <div class="col-md-5">
                <div class="bg-white p-2 rounded">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Check</th>
                                <th>Nomenklatur</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="nm,nmi in nomen">
                                <td>
                                    <button class="btn btn-primary btn-sm" :disabled="isChecked(nm.kode)" @click="checkNomen(nm)"><i class="fa fa-arrow-left"></i></button>
                                </td>
                                <td><small>(@{{nm.kode}})</small> @{{nm.name}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('js')
<script>
    var DF=new Vue({
        el:'#app',
        data:{
            data:<?=json_encode($data)?>,
            nomen:<?=json_encode($nomen)?>,
            page_active:0,

        },
        computed:{

        },
        methods: {
            isChecked:function(kode){
                return  this.data[this.page_active].nomen_tag.filter((el)=>{
                    return el.kode==kode;
                }).length?true:false;
            },
            checkNomen:function(nomen){
                this.data[this.page_active].draf=true;
                var index=this.data[this.page_active].nomen_tag.indexOf(nomen);
                if(index===-1){

                this.data[this.page_active].nomen_tag.push(nomen);
                }else{
                
                    this.data[this.page_active].nomen_tag.splice(index,1);
                }

            },
            submitForm:function(){
                this.$isLoading(true);
                var self=this;
                var index=this.page_active;
                req_ajax.post('{{route('api.mod-sink.sink.rpjmn.save',['tahun'=>$StahunRoute])}}',{
                    data:this.data[index],
                    kodeurusan:'{{$kodeurusan}}'
                })
                .then(res=>{
                    console.log(res);
                    self.$isLoading(false);
                });

            }
        },
    })

</script>
@stop

