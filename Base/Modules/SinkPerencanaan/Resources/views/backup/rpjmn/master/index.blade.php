@extends('adminlte::page')

@section('content')

<div class="container-fluid pt-2">
    <div id="rpjmn" class="mt-2">
       
        <div class="card">
            <div class="card-header">
                <h5><b>MASTER RPJMN</b></h5>

                <div class="btn-group-sm mt-2 mb-2">
                    <button class="btn btn-primary" @click="formAddPn">Tambah PN</button>
                </div>
            </div>
            <div class="card-body">
                
                <table class="table table-bordered table-striped" >
                    <thead>
                        <tr>
                            <th>Aksi</th>
    
                            <th>LEVEL</th>
                            <th>NAMA</th>
                            <th>KETERANGAN</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="item in data">
                            <td>
                                <div class="btn-group-vertical">
    
                                <button  v-if="item.context!='MAJOR PROJECT'" @click="getNestNomen(item)" class="btn btn-primary btn-sm">Load @{{item.child_context.join(' , ')}}</button>
                                <button @click="formEditPn(item)" class="btn btn-warning btn-sm">Edit @{{item.context}}</button>
                                </div>
    
                            </td>
                            <td>@{{item.context}}</td>
                            <td>@{{item.name}}</td>
                            <td>@{{item.keterangan}}</td>
    
    
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div id="modal-x" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-full" role="document">
                <div class="modal-content">
                   
                      <template v-if="data_modal.form_enable">
                        <div class="modal-header">
                            <h5 class="modal-title"><span class="badge badge-primary">@{{data_modal.form.load_parent.context}}</span> <b>@{{data_modal.form.load_parent.name}}</b></h5>
                          
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                            <div class="modal-body">
                                <div class="btn-group-sm mb-2">
                                    <button v-if="data_modal.form.form_data.context!='PN'"  @click="data_modal.form_enable=false;" class="btn btn-warning">Tutup Form</button>
                                    <span class="ml-2">@{{(data_modal.form.form_append?'Tambah':'Ubah')+' '+(data_modal.form.form_append?data_modal.form.load_parent.child_context.join(' / '):data_modal.form.form_data.context)}} </span>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group" v-if="data_modal.form.form_append">
                                            <label for="">Level</label>
                                            <select name="" class="form-control" v-model="data_modal.form.form_data.context" id="">
                                                <template v-if="data_modal.form.form_append">
                                                    <option :value="item" v-for="item in data_modal.form.load_parent.child_context">@{{item}}</option>
                                                </template>
                                                <template v-else>
                                                    <option :value="data_modal.form.form_data.context">@{{data_modal.form.form_data.context}}</option>
                                                </template>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Nama</label>
                                           <textarea name="" class="form-control" v-model="data_modal.form.form_data.name"  id="" cols="30" rows="5"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Keterangan</label>
                                           <textarea name="" class="form-control" v-model="data_modal.form.form_data.keterangan"  id="" cols="30" rows="5"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-group">
                                    <button @click="submitForm" class="btn btn-primary" v-if="showSubmitBtnNomen">Kirim</button>
                                </div>
                                
                            </div>
                      </template>
                      <template v-else>
                        <div class="modal-header">
                            <h5 class="modal-title"><span class="badge badge-primary">@{{data_modal.load_parent.context}}</span> <b>@{{data_modal.load_parent.name}}</b></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                            <div class="modal-body">
                                <div class="btn-group-sm mb-2">
                                    <button @click="loadForm(data_modal.load_parent,1)" class="btn btn-primary">Tambah @{{data_modal.load_parent.child_context.join(' / ')}}</button>
                                </div>
    
                        <table class="table table-bordered table-striped">
                            
    
                            <thead>
                                <tr>
                                    <th>AKSI</th>
                                    <th colspan="3">LEVEL</th>
                                    <th>NAMA</th>
                                    <th>TARGET</th>
                                    <th>SATUAN</th>
                                    <th>KETERANGAN</th>
                                </tr>
                            </thead>
                            <tbody>
                                <template  v-for="item,k in data_modal.data">
                                    <tr  :key="'PP-MAJOR-'+k">
                                        <td>
                                            <div class="btn-group-vertical">
                                                <button v-if="item.context!='MAJOR PROJECT'" @click="getNestNomen(item)" class="btn btn-info btn-sm">Load @{{item.child_context.join(' , ')}}</button>
                                                <button v-if="item.expend_child" @click="loadForm(item,1)" class="btn btn-primary btn-sm">Tambah @{{item.child_context.join(' / ')}}</button>
                                                <button @click="loadForm(item,0)" class="btn btn-warning btn-sm">Edit @{{item.context}}</button>
    
                                            </div>
                                        </td>
                                        <td >@{{item.context}}</td>
                                        <td></td>
                                        <td></td>
    
                                        
                                        <td>@{{item.name}}</td>
                                        <td></td>
                                        <td></td>

                                        <td>@{{item.keterangan}}</td>
                                    </tr>
                                    <template  v-for="ind,kind in item.indikator">
                                        <tr :key="item.context+'-IND-'+kind">
                                            <td>
                                            
                                                <div class="btn-group-vertical">
                                                <button @click="loadForm(ind,0)" class="btn btn-warning btn-sm">Edit @{{ind.context}}</button>

                                                   
                                                </div>
                                            </td>
                                            <td >@{{item.context}} IND</td>

                                            <td></td>
                                            <td></td>

                                        

                                            <td>@{{ind.name}}</td>
                                            <td></td>
                                            <td></td>
                                            
                                            <td>@{{ind.keterangan}}</td>
                                        </tr>
                                        
                                    </template>
                                    
                                    <template  v-for="item2,k2 in item.childs">
                                        <tr :key="'KP-'+k2">
                                            <td>
                                                
                                                <div class="btn-group-vertical">
                                                    <button @click="getNestNomen(item2)" class="btn btn-info btn-sm">Load @{{item2.child_context.join(' , ')}}</button>
                                                    <button v-if="item2.expend_child" @click="loadForm(item2,1)" class="btn btn-primary btn-sm">Tambah @{{item2.child_context.join(' / ')}}</button>
                                                    <button @click="loadForm(item2,0)" class="btn btn-warning btn-sm">Edit @{{item2.context}}</button>
                                                </div>
                                            </td>
                                            <td></td>
                                            <td >@{{item2.context}}</td>
                                            <td></td>
    
                                            <td>@{{item2.name}}</td>
                                            <td></td>
                                            <td></td>
                                            <td>@{{item2.keterangan}}</td>
                                        </tr>
                                       
                                        <template  v-for="item3,k3 in item2.childs">
                                            <tr :key="'PROP-'+k3">
                                                <td>
                                                
                                                    <div class="btn-group-vertical">
                                                    <button @click="loadForm(item3,0)" class="btn btn-warning btn-sm">Edit @{{item3.context}}</button>
    
                                                        {{-- <button @click="getNestNomen(item3)" class="btn btn-info btn-sm">Load @{{item3.child_context.join(' , ')}}</button>
                                                        <button @click="loadForm(item3,1)" class="btn btn-primary btn-sm">Tambah @{{item3.child_context.join(' / ')}}</button>
                                                     --}}
                                                    </div>
                                                </td>
                                                <td></td>
                                                <td></td>
    
                                                
                                                <td >@{{item3.context}}</td>
    
                                                <td>@{{item3.name}}</td>
                                                <td></td>
                                                <td></td>
                                                <td>@{{item3.keterangan}}</td>
                                            </tr>
                                            <template  v-for="ind,kind in item3.indikator">
                                                <tr :key="'PROP-IND-'+kind" class="bg-hightlight">
                                                    <td>
                                                    
                                                        <div class="btn-group-vertical">
                                                        <button @click="loadForm(item3,0)" class="btn btn-warning btn-sm">Edit @{{ind.context}}</button>
        
                                                            {{-- <button @click="getNestNomen(item3)" class="btn btn-info btn-sm">Load @{{item3.child_context.join(' , ')}}</button>
                                                            <button @click="loadForm(item3,1)" class="btn btn-primary btn-sm">Tambah @{{item3.child_context.join(' / ')}}</button>
                                                         --}}
                                                        </div>
                                                    </td>
                                                    <td></td>
                                                    <td></td>
        
                                                    
                                                    <td >@{{item3.context}} IND</td>
        
                                                    <td>@{{ind.name}}</td>
                                                    <td v-html="ind.target"></td>
                                                    <td>@{{ind.satuan}}</td>
                                                    <td>@{{ind.keterangan}}</td>
                                                </tr>
                                                
                                            </template>
                                            
                                        </template>

        
                                    </template>
                                    
                                </template>
                            </tbody>
                        </table>
                    </div>
    
                      </template>
                        
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop


@section('js')
<script>
    var rpjmd=new Vue({
        el:'#rpjmn',
        data:{
            data:<?=json_encode($data)?>,
            data_modal:{
                form:{
                    form_append:false,
                    load_parent:{
                        id:null,
                        context:null,
                        name:'',
                        child_context:[]

                    },
                    form_data:{
                        id:null,
                        context:null,
                        name:'',
                        child_context:[],
                        keterangan:null,
                        id_parent:null

                    }
                },
                form_enable:false,
                load_parent:{
                    id:null,
                    context:null,
                    name:'',
                    child_context:[]
                },
                data:[],
            }
        },
        computed:{
            showSubmitBtnNomen:function(){
                return (this.data_modal.form.form_data.context && this.data_modal.form.form_data.name);
            }
        },
        methods: {
            submitForm:function(){
                var self=this;
                this.$isLoading(true);
                if(this.data_modal.form.form_append){
                    req_ajax.post('{{route('api.mod-sink.m.rpjmn.add',['tahun'=>$StahunRoute])}}',{
                        parent:this.data_modal.form.load_parent,
                        data:this.data_modal.form.form_data
                    }).then(res=>{

                    }).finally(()=>{
                        if(self.data_modal.form.form_data.context!='PN' || self.data_modal.form.form_data.context!='MAJOR PROJECT'){
                            self.getNestNomen(self.data_modal.form.load_parent);
                            self.data_modal.form_enable=false;
                            self.data_modal.form.form_data={
                                id:null,
                                context:null,
                                name:'',
                                child_context:[],
                                keterangan:null,
                                id_parent:null
                            }
                            self.$isLoading(false);
                        }else{
                            window.location.href=window.location.href;

                        }
                       


                    });
                }else{
                    req_ajax.post('{{route('api.mod-sink.m.rpjmn.edit',['tahun'=>$StahunRoute])}}',{
                        parent:this.data_modal.form.load_parent,
                        data:this.data_modal.form.form_data
                    }).then(res=>{

                    }).finally(()=>{
                        if(self.data_modal.form.form_data.context!='PN' || self.data_modal.form.form_data.context!='MAJOR PROJECT'){
                            self.getNestNomen(self.data_modal.form.load_parent);
                            self.data_modal.form_enable=false;
                            self.data_modal.form.form_data={
                                id:null,
                                context:null,
                                name:'',
                                child_context:[],
                                keterangan:null,
                                id_parent:null
                            }
                            self.$isLoading(false);

                        }else{
                            window.location.href=window.location.href;
                        }
                        

                    });

                }

            },
            loadForm:function(item,is_append){
                this.data_modal.form.form_append=is_append;
                this.data_modal.form_enable=true;
                if(is_append){
                    this.data_modal.form.load_parent=item;
                }else{
                    this.data_modal.form.load_parent=item;
                    this.data_modal.form.form_data=item;



                }

            },
            formAddPn:function(){
                this.data_modal.form.load_parent={
                    id:null,
                    name:'RPJMN',
                    context:'',
                    child_context:['PN','MAJOR PROJECT']
                };
                this.data_modal.form.form_data={
                        id:null,
                        context:null,
                        name:'',
                        child_context:[],
                        keterangan:null,
                        id_parent:null
                }
                this.data_modal.form_enable=true;
                this.data_modal.form.form_append=true;
                $('#modal-x').modal({
                        backdrop:'static'
                });

            },
            formEditPn:function(item){
                this.data_modal.form.load_parent=item;
                this.data_modal.form.form_data=item;
                this.data_modal.form_enable=true;
                this.data_modal.form.form_append=false;
                $('#modal-x').modal({
                        backdrop:'static'
                });

            },
            getNestNomen:function(item,sub_parent=null){
                var self=this;
                item.expend_child=true;
                req_ajax.post(('{{route('api.mod-sink.m.rpjmn.load-nest',['tahun'=>$StahunRoute,'id_parent'=>'___id'])}}').replace('___id',item.id),{
                    context:item.child_context
                }).then((res)=>{
                    if(res.data){
                        if(item.with_child_append==false){
                            self.data_modal.data=res.data;
                            Object.assign(self.data_modal.load_parent,item);


                        }else{
                            item.childs=res.data;
                        }
                    }
                }).finally(()=>{
                    self.data_modal.form_enable=false;
                    $('#modal-x').modal({
                        backdrop:'static'
                    });

                });
            }
        },
    });
</script>


@stop

@section('css')
<style>
    .bg-hightlight{
        background-color: aquamarine;
    }

</style>

@stop