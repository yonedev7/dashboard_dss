@extends('adminlte::page')

@section('content_header')
    <div class="contianer-fluid">
        
    </div>
@stop


@section('content')
    <div class="container mb-4 " >
        <div class="bg-white rounded shadow p-2 border">
            <h3 class="my-3"><b class="text-white bg-dark rounded px-2 shadow " >Pemetaan Pusat</b></h3>
        </div>
       
    </div>
    <div class="container">
    <div class="row mt-2">
        <div class="col-md-4">
            <div class="card"  >
                <div class="card-body" >
                    <h4>RPJMN</h4>
                    <a href="{{route('mod-sink.master.pusat',['tahun'=>$StahunRoute,'context'=>'RPJMN'])}}" class="btn btn-primary">Detail
                    <i class="fa fa-arrow-right"></i>
                    </a>
                </div>
            </div>
        </div>
            <div class="col-md-4">
                <div class="card"  >
                    <div class="card-body" >
                        <h4>SDGS</h4>
                        <a href="{{route('mod-sink.master.pusat',['tahun'=>$StahunRoute,'context'=>'SDGS'])}}" class="btn btn-primary">Detail
                        <i class="fa fa-arrow-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <h4>RKP</h4>
                        <a  href="{{route('mod-sink.master.pusat',['tahun'=>$StahunRoute,'context'=>'RKP'])}}"  class="btn btn-primary">Detail
                            <i class="fa fa-arrow-right"></i>
                            </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <h4>SPM</h4>
                        <a  href="{{route('mod-sink.master.pusat',['tahun'=>$StahunRoute,'context'=>'SPM'])}}"  class="btn btn-primary">Detail
                            <i class="fa fa-arrow-right"></i>
                            </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <h4>Urusan</h4>
                        <a  href="{{route('mod-sink.master.pusat',['tahun'=>$StahunRoute,'context'=>'URUSAN'])}}"  class="btn btn-primary">Detail
                            <i class="fa fa-arrow-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        
        
    </div>

    <div class="container mb-4 " >
        <div class="bg-white rounded shadow p-2 border">
            <h3 class="my-3"><b class="text-white bg-dark rounded px-2 shadow " >Pemetaan Daerah</b></h3>
        </div>
       
    </div>
    <div class="container">
        <div class="row mt-2">
            <div class="col-md-4">
                <div class="card"  >
                    <div class="card-body" >
                        <h4>RPJMD</h4>
                        <a href="{{route('mod-sink.master.pusat',['tahun'=>$StahunRoute,'context'=>'RPJMD'])}}" class="btn btn-primary">Detail
                        <i class="fa fa-arrow-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            
        
        </div>
    </div>

    <div class="container mb-4 " >
        <div class="bg-white rounded shadow p-2 border">
            <h3 class="my-3"><b class="text-white bg-dark rounded px-2 shadow " >Sinkronisasi</b></h3>
        </div>
       
    </div>
    <div class="container">
        <div class="row mt-2">
            <div class="col-md-4">
                <div class="card"  >
                    <div class="card-body" >
                        <h4>SDGS <i class="fa fa-arrow-right"></i> RPJMN</h4>
                        <a href="{{route('mod-sink.master.pusat',['tahun'=>$StahunRoute,'context'=>'RPJMD'])}}" class="btn btn-primary">Detail
                        <i class="fa fa-arrow-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card"  >
                    <div class="card-body" >
                        <h4>RPJMN <i class="fa fa-arrow-right"></i> RPJMD</h4>
                        <a href="{{route('mod-sink.master.pusat',['tahun'=>$StahunRoute,'context'=>'RPJMD'])}}" class="btn btn-primary">Detail
                        <i class="fa fa-arrow-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card"  >
                    <div class="card-body" >
                        <h4>RPJMD <i class="fa fa-arrow-right"></i> RKPD</h4>
                        <a href="{{route('mod-sink.master.pusat',['tahun'=>$StahunRoute,'context'=>'RPJMD'])}}" class="btn btn-primary">Detail
                        <i class="fa fa-arrow-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card"  >
                    <div class="card-body" >
                        <h4>RKP <i class="fa fa-arrow-right"></i> RKPD</h4>
                        <a href="{{route('mod-sink.master.pusat',['tahun'=>$StahunRoute,'context'=>'RPJMD'])}}" class="btn btn-primary">Detail
                        <i class="fa fa-arrow-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card"  >
                    <div class="card-body" >
                        <h4>SPM <i class="fa fa-arrow-right"></i> RKPD</h4>
                        <a href="{{route('mod-sink.master.pusat',['tahun'=>$StahunRoute,'context'=>'RPJMD'])}}" class="btn btn-primary">Detail
                        <i class="fa fa-arrow-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card"  >
                    <div class="card-body" >
                        <h4>KINERJA URUSAN <i class="fa fa-arrow-right"></i> RKPD</h4>
                        <a href="{{route('mod-sink.master.pusat',['tahun'=>$StahunRoute,'context'=>'RPJMD'])}}" class="btn btn-primary">Detail
                        <i class="fa fa-arrow-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            
        
        </div>
    </div>
    
@stop

@section('js')

@stop