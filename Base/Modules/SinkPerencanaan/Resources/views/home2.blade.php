@extends('adminlte::page')

@section('content_header')
    <div class="contianer-fluid">
        
    </div>
@stop

@section('content')
<div class="container-fluid" id="app">
    @if(array_intersect(['SUBDIT','SUPERADMIN','ADMIN'],Auth::User()->getRoleNames()->toArray()) OR Auth::User()->can('is_admin') )

    <div class="bg-white p-2">
        <div class="bg-white d-flex">
                <div class="col-md-2">
                    <a href="{{route('mod-sink.index',['tahun'=>$StahunRoute,'pemda_scope'=>($scope=='KAB-KOTA'?'PROVINSI':'KAB-KOTA')])}}" class="btn btn-success mt-3 align-self-center">Pindah ke {{$scope=='KAB-KOTA'?'PROVINSI':'KAB-KOTA'}}</a>
                </div>
               
                <div class="col-md-5">
                    <h4>Indikator Nasional</h4>
                    <a href="{{route('mod-sink.master',['tahun'=>$StahunRoute,'scopemda'=>($scope=='KAB-KOTA'?'KAB-KOTA':'PROVINSI')])}}" class="btn btn-primary text-white"><span class="text-white">Mastering {{$scope}}</span> <i class="text-white  fa fa-arrow-right"></i></a>
                </div>

            </div>
    </div>
       <div class=" sticky-top bg-white p-2 rounded-bottom mb-3 shadow" style="top:57px;">
        <h4 class="my-3 ">Rekomendasi Sinkronisasi Perencanaan Daerah {{$scope}}</h4>
        <div class="form-group bg-white">
            <input type="text" placeholder="Cari Nama Pemda" class="form-control " v-model="input">
        </div>
       </div>
       @endif

        <div class="row">
            <div class="col-md-4" v-for="item in comPemda">
                <div class="bg-white p-2 mb-3  shadow rounded">
                    <h4>@{{item.nama_pemda}}</h4>
                    <a :href="('{{route('mod-sink.master.pemda',['tahun'=>$StahunRoute,'kodepemda'=>'xx'])}}').replace('xx',item.kodepemda)"  class="btn btn-primary text-white mb-1"><span class="text-white">Indikator Daerah</span></a>
                    <a :href="('{{route('mod-sink.sink.ind.rpjmd',['tahun'=>$StahunRoute,'kodeurusan'=>'1.03','kodepemda'=>'xx'])}}').replace('xx',item.kodepemda)" class="btn btn-primary text-white mb-1"><span class="text-white">Sinkronisasi RPJMN-RPJMD</span></a>
                    <a :href="('{{route('mod-sink.sink.ind.index',['tahun'=>$StahunRoute,'kodeurusan'=>'1.03','kodepemda'=>'xx'])}}').replace('xx',item.kodepemda)" class="btn btn-primary text-white mb-1"><span class="text-white">Rekomendasi RKPD</span></a>


                
                </div>
            </div>
           
        </div>
</div>

@stop


@section('js')

<script>
    var app =new Vue({
        el:'#app',
        data:{
            pemda:<?=json_encode($pemda)?>,
            input:null,

        },
        computed:{
            comPemda:function(){
               return this.pemda.filter(el=>{
                    return (el.nama_pemda.toLowerCase()).includes((this.input||'').toLowerCase());
                });
            }
        }

    })
</script>
@stop