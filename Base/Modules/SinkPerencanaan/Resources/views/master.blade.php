@extends('adminlte::page')

@section('content_header')
    <div class="contianer-fluid">
        <h4>Mastering Indikator Nasional </h4>
        <p>{{$scope}}</p>
        <button class="btn btn-success" onclick="app.modalD(0)"><i class="fa fa-pluss"></i> Tambah Indikator</button>
    </div>
@stop

@section('content')
<div class="" id="app">
    <div class="bg-white">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Aksi</th>
                    <th>Sumber</th>
                    <th>Nama Indikator</th>
                    <th>Target</th>
                    <th>Satuan</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="item,i in data">
                    <td>
                        <button @click="modalDelete(item)" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                        <button @click="modalD(1,item)" class="btn btn-warning btn-sm"><i class="fa fa-pen"></i></button>
                        <button @click="modalSink(item,i)" class="btn btn-primary btn-sm">Pemetaan Nomen RKPD (@{{item.nomen_tag.length}})</button>
                    </td>
                    <td>@{{item.context}}</td>
                    <td>@{{item.name}}</td>
                    <td>@{{item.target}}</td>
                    <td>@{{item.satuan}}</td>




                </tr>
            </tbody>
        </table>
    </div>

    <div ref="modal_sinkronisasi" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="my-modal-title">Nomenklatur RKPD Terkait</h5>
                    <button class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>@{{sink.data.name}}</p>
                    <div class="">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nomenklatur</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="item in nomen">
                                    <td>
                                        <template v-if="isActiveNomen(item.kode)">
                                            <button class="btn btn-sm btn-danger" @click="activeNomen(item.kode)"><i class="fa fa-minus"></i></button>
                                        </template>
                                        <template v-else>
                                            <button class="btn btn-sm btn-primary" @click="activeNomen(item.kode)"><i class="fa fa-plus"></i></button>
                                        </template>
                                    </td>
                                    <td>(@{{item.kode}}) -  @{{item.name}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div ref="modal_del" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
               <form action="{{route('mod-sink.delete',['tahun'=>$StahunRoute,'scopemda'=>$scope])}}" method="post">
                @csrf
                @method('DELETE')
                <div class="modal-header">
                    <h5 class="modal-title" id="my-modal-title">Hapus Indikator @{{form.data_del.context}}</h5>
                    <button class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" v-model="form.data_del.id">
                    <p>Menghapus Indikator "@{{form.data_del.name}}"</p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit">Kirim</button>
                </div>
               </form>
            </div>
        </div>
    </div>
    <div ref="modal_form" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog  modal-lg" role="document">
           
            <div class="modal-content">
              <form action="{{route('mod-sink.ui',['tahun'=>$StahunRoute,'scopemda'=>$scope])}}" method="post">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="my-modal-title">@{{form.mode==0?'Tambah Indikator':'Ubah Indikator'}}</h5>
                    <button class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" v-model="form.data.id" name="id">
                        <div class="col-6">
                            <div class="form-group" v-if="form.data.id==null">
                                <label for="">Sumber</label>
                                <select name="context" class="form-control" id="" required v-model="form.data.context">
                                    <option value="RPJMN">RPJMN</option>
                                    <option value="RKP">RKP</option>
                                    <option value="SDGS">SDGS</option>
                                    <option value="SPM">SPM</option>
                                    <option value="URUSAN">URUSAN</option>

                                </select>
                            </div>
                            <div class="form-group" v-else>
                                <label for="">Sumber</label>
                                <p><b>@{{form.data.context}}</b></p>

                            </div>
                            <div class="form-group">
                                <label for="">keterangan Sumber</label>
                                <textarea name="keterangan_sumber" class="form-control" id="" rows="5" v-model="form.data.keterangan_sumber" ></textarea>
                            </div>
                           
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="">Nama</label>
                                <textarea name="name" class="form-control" id="" rows="3" required v-model="form.data.name"> </textarea>
                            </div>
                            <div class="form-group">
                                <label for="">Target</label>
                                <textarea name="target" class="form-control" id="" rows="3" required v-model="form.data.target"></textarea>
                            </div>

                            <div class="form-group">
                                <label for="">Satuan</label>
                                <input type="text" name="satuan" class="form-control" id="" required v-model="form.data.satuan" />
                            </div>
                         
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit">Kirim</button>
                </div>
              </form>
            </div>
        </div>
    </div>
</div>
@stop


@section('js')
<script>

    var app=new Vue({
        el:'#app',
        data:{
            data:<?=json_encode($data)?>,
            nomen:<?=json_encode($nomen)?>,
            form:{
                mode:0,
                data:{
                    context:'',
                    id:null,
                    keterangan_sumber:'',
                    keteranagan:'',
                    nama:'',
                    terget:'',
                    satuan:''
                },
                data_del:{
                    context:'',
                    id:'',

                }
            },
            sink:{
                index:0,
                data:{
                    context:'',
                    id:null,
                    keterangan_sumber:'',
                    keteranagan:'',
                    nama:'',
                    terget:'',
                    satuan:'',
                    nomen_tag:[

                    ]
                }
            }
        },
        methods: {
            isActiveNomen:function(kode){
                if(this.data[this.sink.index]!==undefined){
                   return this.data[this.sink.index].nomen_tag.filter(el=>{
                        return el==kode
                    }).length;
                }else{
                    return false;
                }
            },
            activeNomen:function(nomen){
                var self=this;
                if(this.data[this.sink.index]!==undefined){
                    self.$isLoading(true);

                    req_ajax.post('{{route('api.mod-sink.sink.nomen-ind',['tahun'=>$StahunRoute,'scope'=>$scope])}}',{
                        id_indikator:this.sink.data.id,
                        nomenklatur_rkpd:nomen
                    }).then(res=>{
                        if(res.data.code==200){
                            self.data[self.sink.index].nomen_tag=res.data.data;
                        }else{

                        }
                    }).finally(function(){
                        self.$isLoading(false);
                    });
                }else{
                    return false;
                }

            },
            modalSink:function(data,index){
                this.sink.index=index;
                this.sink.data=data;
                $(this.$refs.modal_sinkronisasi).modal({
                    backdrop:'static'
                });
            },
            modalDelete:function(data){
                this.form.data_del=data;
                $(this.$refs.modal_del).modal({
                    backdrop:'static'
                });
            },
            modalD:function(mode,data={}){
                this.form.mode=mode;
                if(mode==0){
                    this.form.data.id=null;
                    this.form.data.context='';
                    this.form.data.keterangan_sumber='';
                    this.form.data.keteranagan='';
                    this.form.data.context='';
                    this.form.data.name='';
                    this.form.data.terget='';
                    this.form.data.satuan='';

                }else{
                    this.form.data=data;
                }

                $(this.$refs.modal_form).modal({
                    backdrop:'static'
                });
            }
        },
    })
</script>

@stop