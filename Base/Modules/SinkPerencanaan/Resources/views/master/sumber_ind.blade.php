@extends('adminlte::page')

@section('content_header')
    <div class="contianer-fluid">
        <h4>{{$context}}</h4>
        <button class="btn bg-white" onclick=" app.addForm(null);">Tambah</button>
    </div>
@stop



@section('content')
    <style>
        .bg-ind{
            background: rgb(238, 234, 234);
        }
    </style>
    <div class="container-fluid" id="app">
        <div class="bg-white table-responsive rounded">
            <table class="table table-bordered">
               <thead>
                <tr>
                    <th :colspan="context=='RPJMN'?7:3">Uraian {{str_replace('_',' ',$context)}}</th>
                </tr>
                <tr class="bg-dark">
                    <th>Nama Indikator</th>
                    <template v-if="context=='RPJMN'">
                    <th v-for="th in tahun_list">Target @{{th}}</th>
                                    
                    </template>
                    <template v-else>
                    <th>Target</th>
                    </template>
                    
                    <th>Satuan</th>

                </tr>
               </thead>
               <tbody>

                    <template  v-for="item in data">
                        <tr>
                            <td :colspan="context=='RPJMN'?7:3" >
                                <div class="btn-group btn-group-sm mb-2">
                                    <button class="btn btn-primary" @click="addForm(btn[0],item)"  v-for="btn in transformSchema(list_schema[context][item.context.split('#').at(-1)].context)"><i class="fa fa-plus"></i> @{{btn[1].name}}</button>
                                    <button class="btn btn-dark" @click="addIndikator(item)" v-if="list_schema[context][item.context.split('#').at(-1)].use_ind" ><i class="fa fa-plus"></i> Indikator</button>
                                    <button class="btn btn-warning" @click="editForm(item)"><i class="fa fa-pen"></i></button>
                                    <button class="btn btn-danger"  @click="deleteForm(item)"><i class="fa fa-trash"></i></button>
                                </div>
                                <p class="m-0" ><span class="badge badge-primary" v-html="item.context.split('#').join(spliter).replaceAll('_',' ')"> </span> <h4 v-html="item.uraian"></h4></p>

                            </td>
                        </tr>
                        <tr v-for="ind in item.ind" class="bg-ind" v-if="list_schema[context][item.context.split('#').at(-1)].use_ind">
                            <td>
                                <div>
                                    <span class="badge badge-primary">@{{ind.type.replaceAll('_',' ')}}</span> <span class="badge badge-success">@{{ind.type_pemda.replaceAll('#',', ')}}</span>
                                </div>
                                <p>@{{ind.name}}</p>
                                <div class="btn-group btn-group-sm border">
                                    <button class="btn btn-warning" @click="editFormInd(ind)"><i class="fa fa-pen"></i></button>
                                    <button class="btn btn-danger" @click="deleteFormInd(ind)"><i class="fa fa-trash"></i></button>
                                </div>
                            </td>
                            <template v-if="context=='RPJMN'">
                                <td>@{{ind.target}}</td>
                                <td>@{{ind.target1}}</td>
                                <td>@{{ind.target2}}</td>
                                <td>@{{ind.target3}}</td>
                                <td>@{{ind.target4}}</td>

                            </template>
                            <template v-else>
                            <td>@{{ind.target3}}</td>
                            </template>
                            <td>@{{ind.satuan}}</td>

                        </tr>
                        <template  v-for="item1 in item.sub">
                            <tr>
                                <td :colspan="context=='RPJMN'?7:3" class="pl-5" >
                                  
                                    <div class="btn-group btn-group-sm mb-2">
                                        <button class="btn btn-primary" @click="addForm(btn[0],item1)"  v-for="btn in transformSchema(list_schema[context][item1.context.split('#').at(-1)].context)"><i class="fa fa-plus"></i> @{{btn[1].name}}</button>
                                        <button class="btn btn-dark" @click="addIndikator(item1)" v-if="list_schema[context][item1.context.split('#').at(-1)].use_ind" ><i class="fa fa-plus"></i> Indikator</button>
                                        <button class="btn btn-warning" @click="editForm(item1)"><i class="fa fa-pen"></i></button>
                                        <button class="btn btn-danger"  @click="deleteForm(item1)" ><i class="fa fa-trash"></i></button>
                                    </div>
                                    <p class="m-0" ><span class="badge badge-primary" v-html="item1.context.split('#').join(spliter).replaceAll('_',' ')"> </span> <h4 v-html="item1.uraian"></h4> </p>


                                </td>
                            </tr>
                            <tr v-for="ind in item1.ind" class="bg-ind" v-if="list_schema[context][item1.context.split('#').at(-1)].use_ind">
                                <td class="pl-5">
                                    <div>
                                        <span class="badge badge-primary">@{{ind.type.replaceAll('_',' ')}}</span> <span class="badge badge-success">@{{ind.type_pemda.replaceAll('#',', ')}}</span>
                                    </div>
                                    <p>@{{ind.name}}</p>
                                    <div class="btn-group btn-group-sm border">
                                        <button class="btn btn-warning" @click="editFormInd(ind)"><i class="fa fa-pen"></i></button>
                                        <button class="btn btn-danger" @click="deleteFormInd(ind)"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                                <template v-if="context=='RPJMN'">
                                    <td>@{{ind.target}}</td>
                                    <td>@{{ind.target1}}</td>
                                    <td>@{{ind.target2}}</td>
                                    <td>@{{ind.target3}}</td>
                                    <td>@{{ind.target4}}</td>
    
                                </template>
                                <template v-else>
                                <td>@{{ind.target3}}</td>
                                </template>
                                <td>@{{ind.satuan}}</td>
    
                            </tr>
                            <template  v-for="item2 in item1.sub">
                                <tr>
                                    <td :colspan="context=='RPJMN'?7:3" style="padding-left:80px;" >
                                        <div class="btn-group btn-group-sm mb-2">
                                            <button class="btn btn-primary" @click="addForm(btn[0],item2)"  v-for="btn in transformSchema(list_schema[context][item2.context.split('#').at(-1)].context)"><i class="fa fa-plus"></i> @{{btn[1].name}}</button>
                                            <button class="btn btn-dark" @click="addIndikator(item2)" v-if="list_schema[context][item2.context.split('#').at(-1)].use_ind" ><i class="fa fa-plus"></i> Indikator</button>
                                            <button class="btn btn-warning" @click="editForm(item2)"><i class="fa fa-pen"></i></button>
                                            <button class="btn btn-danger" @click="deleteForm(item2)"><i class="fa fa-trash"></i></button>
                                        </div>

                                        <p class="m-0" ><span class="badge badge-primary" v-html="item2.context.split('#').join(spliter).replaceAll('_',' ')"> </span> <h4 v-html="item2.uraian"></h4> </p>
                                    </td>
                                </tr>
                                <tr v-for="ind in item2.ind" class="bg-ind" v-if="list_schema[context][item2.context.split('#').at(-1)].use_ind">
                                    <td style="padding-left:80px;">
                                        <div>
                                            <span class="badge badge-primary">@{{ind.type.replaceAll('_',' ')}}</span> <span class="badge badge-success">@{{ind.type_pemda.replaceAll('#',', ')}}</span>
                                        </div>
                                        <p>@{{ind.name}}</p>
                                        <div class="btn-group btn-group-sm border">
                                            <button class="btn btn-warning" @click="editFormInd(ind)"><i class="fa fa-pen"></i></button>
                                            <button class="btn btn-danger" @click="deleteFormInd(ind)"><i class="fa fa-trash"></i></button>
                                        </div>
                                        
                                    </td>
                                    <template v-if="context=='RPJMN'">
                                        <td>@{{ind.target}}</td>
                                        <td>@{{ind.target1}}</td>
                                        <td>@{{ind.target2}}</td>
                                        <td>@{{ind.target3}}</td>
                                        <td>@{{ind.target4}}</td>
        
                                    </template>
                                    <template v-else>
                                    <td>@{{ind.target3}}</td>
                                    </template>
                                    <td>@{{ind.satuan}}</td>
        
                                </tr>
                                <template  v-for="item3 in item2.sub">
                                    <tr>
                                        
                                        <td :colspan="context=='RPJMN'?7:3" style="padding-left:110px;" >
                                            <div class="btn-group btn-group-sm mb-2">
                                                <button class="btn btn-primary" @click="addForm(btn[0],item3)"  v-for="btn in transformSchema(list_schema[context][item3.context.split('#').at(-1)].context)"><i class="fa fa-plus"></i> @{{btn[1].name}}</button>
                                                <button class="btn btn-dark" @click="addIndikator(item3)" v-if="list_schema[context][item3.context.split('#').at(-1)].use_ind" ><i class="fa fa-plus"></i> Indikator</button>
                                                <button class="btn btn-warning" @click="editForm(item3)"><i class="fa fa-pen"></i></button>
                                                <button class="btn btn-danger" @click="deleteForm(item3)"><i class="fa fa-trash"></i></button>
                                            </div>
                                        <p class="m-0" ><span class="badge badge-primary" v-html="item3.context.split('#').join(spliter).replaceAll('_',' ')"> </span> <h4 v-html="item3.uraian"></h4> </p>

                                        </td>
                                    </tr>
                                    <tr v-for="ind in item3.ind" class="bg-ind" v-if="list_schema[context][item3.context.split('#').at(-1)].use_ind">
                                        <td>
                                            <div>
                                                <span class="badge badge-primary">@{{ind.type.replaceAll('_',' ')}}</span> <span class="badge badge-success">@{{ind.type_pemda.replaceAll('#',', ')}}</span>
                                            </div>
                                            <p>@{{ind.name}}</p>
                                            <div class="btn-group btn-group-sm border">
                                                <button class="btn btn-warning" @click="editFormInd(ind)"><i class="fa fa-pen"></i></button>
                                                <button class="btn btn-danger" @click="deleteFormInd(ind)"><i class="fa fa-trash"></i></button>
                                            </div>
                                        </td>
                                        <template v-if="context=='RPJMN'">
                                            <td>@{{ind.target}}</td>
                                            <td>@{{ind.target1}}</td>
                                            <td>@{{ind.target2}}</td>
                                            <td>@{{ind.target3}}</td>
                                            <td>@{{ind.target4}}</td>
            
                                        </template>
                                        <template v-else>
                                        <td>@{{ind.target3}}</td>
                                        </template>
                                        <td>@{{ind.satuan}}</td>
            
                                    </tr>
                                </template>
                            </template>
                        </template>
                    </template>
               </tbody>
            </table>
        </div>

        <div ref="modal_add_infikator" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-full" role="document">
                <div class="modal-content">
                   <form action="{{route('mod-sink.master.pusat.indikator.add',['tahun'=>$StahunRoute,'context'=>$context])}}" method="post">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="my-modal-title">Tambah Indikator <small v-html="form_add_indikator.parent_context.split('#').join(spliter).replaceAll('_',' ')"></small></h5>
                        <button class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <input type="hidden" name="parent_id" :value="form_add_indikator.parent_id">
                            <input type="hidden" class="form-control" name="context" :value="form_add_indikator.parent_context+'#IND-'+form_add_indikator.type">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Nama Indikator</label>
                                    <textarea name="name" required class="form-control" v-model="form_add_indikator.name" id="" cols="30" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="d-flex">
                                    <template v-if="context=='RPJMN'">
                                        <div class="form-group mx-1" v-for="th,kth in tahun_list">
                                            <label for="">Target @{{th}}</label>
                                            <textarea :required="kth==3?'required':false" :name="'target'+(kth==0?'':kth)" v-model="form_add_indikator['target'+(kth==0?'':kth)]" class="form-control" id="" cols="30" rows="3"></textarea>
                                        </div>
                                    </template>
                                    <template v-else>
                                        <div class="form-group ">
                                            <label for="">Target {{$StahunRoute}}</label>
                                            <textarea required name="target3" class="form-control" id="" cols="30" rows="3"></textarea>
                                        </div>
                                    </template>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Satuan Indikator</label>
                                   <input type="text" name="satuan" required class="form-control" v-model="form_add_indikator.satuan">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Tipe Indikator</label>
                                    <select name="type" required class="form-control" id="" v-model="form_add_indikator.type">
                                    <option :value="it" v-for=" it in ['PROGRAM','KEGIATAN','SUB_KEGIATAN']">@{{it.replaceAll('_',' ')}}</option>
                                   </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Jenis Pemda Distribusi</label>
                                    <v-select required   multiple :options="['KOTA','KABUPATEN','PROVINSI']" v-model="form_add_indikator.type_pemda"></v-select>
                                    <input type="text" style="visibility: hidden" name="for" required :value="form_add_indikator.type_pemda.join('#')">
                                </div>
                            </div>
                        </div>
                        <hr>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary"  data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn bg-primary"> Kirim</button>
                    </div>
                   </form>
                </div>
            </div>
        </div>
        <div ref="modal_edit" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-danger" role="document">
                <div class="modal-content">
                   <form action="{{route('mod-sink.master.pusat.update',['tahun'=>$StahunRoute,'context'=>$context])}}" method="post">
                    @method('PUT')
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="my-modal-title">Ubah <small v-html="form_edit.context.split('#').join(spliter).replaceAll('_',' ')"></small></h5>
                        <button class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Rubah Uraian "@{{form_edit.uraian_old}}"</p>
                        <input type="hidden" name="id" :value="form_edit.id">
                        <div class="form-group">
                            <label for="">Uraian <span class="text-capitalize" v-if="form_add.schema.context.length==1">@{{form_edit.context.replaceAll('_',' ').toLowerCase()}}</span></label>
                            <textarea required name="uraian" class="form-control" id="" cols="30" v-model="form_edit.uraian" rows="4"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary"  data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn bg-primary"> Kirim</button>
                    </div>
                   </form>
                </div>
            </div>
        </div>
        <div ref="modal_delete" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-danger" role="document">
                <div class="modal-content">
                   <form action="{{route('mod-sink.master.pusat.delete',['tahun'=>$StahunRoute,'context'=>$context])}}" method="post">
                    @method('DELETE')
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="my-modal-title">Hapus <small v-html="form_delete.context.split('#').join(spliter).replaceAll('_',' ')"></small></h5>
                        <button class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Hapus "@{{form_delete.uraian}}"</p>
                        <input type="hidden" name="id" :value="form_delete.id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary"  data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn bg-white"><i class="fa fa-trash"></i> Hapus</button>
                    </div>
                   </form>
                </div>
            </div>
        </div>
        <div ref="modal_add" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form action="{{route('mod-sink.master.pusat.store',['tahun'=>$StahunRoute,'context'=>$context])}}" method="post">
                        @csrf
                        <div class="modal-header">
                            <h5 class="modal-title" id="my-modal-title">Tambah {{str_replace('_',' ',$context)}}</h5>
                            <button class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div v-if="form_add.schema.context.length>1" class="form-group">
                                <label for="">Jenis</label>
                                <select required name="" class="form-control"  id="" v-model="form_add.context">
                                    <option :value="i[0]" v-for="i in (transformSchema(form_add.schema.context))" >
                                        @{{i[1].name}}
                                    </option>
                                </select>
                                
                            </div>
                            <input type="hidden" name="parent_id" :value="form_add.parent_id">
                            <input type="hidden" name="context" :value="(form_add.context_def?form_add.context_def+'#'+form_add.context:context+'#'+form_add.context)">
                            <div class="form-group">
                                <label for="">Uraian <span class="text-capitalize" v-if="form_add.schema.context.length==1">@{{form_add.context.replaceAll('_',' ').toLowerCase()}}</span></label>
                                <textarea required name="uraian" class="form-control" id="" cols="30" v-model="form_add.uraian" rows="4"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Kirim</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@stop

@section('js')
<script>
    var app=new Vue({
        el:'#app',
        methods: {
            addIndikator(data){
                this.form_add_indikator.parent_context=data.context;
                this.form_add_indikator.parent_uraian=data.uraian;
                this.form_add_indikator.parent_id=data.id;
                this.form_add_indikator.type_pemda=[];
                this.form_add_indikator.type='';
                this.form_add_indikator.target='';
                this.form_add_indikator.target1='';
                this.form_add_indikator.target2='';
                this.form_add_indikator.target3='';
                this.form_add_indikator.target4='';

                $(this.$refs.modal_add_infikator).modal({
                    backdrop:'static'
                });

            },
            deleteForm(data){
                this.form_delete.id=data.id;
                this.form_delete.context=data.context;
                this.form_delete.uraian=data.uraian;

                $(this.$refs.modal_delete).modal({
                    backdrop:'static'
                });
            },
            editForm(data){
                console.log(data);
                this.form_edit.id=data.id;
                this.form_edit.context=data.context;
                this.form_edit.uraian=data.uraian;
                this.form_edit.uraian_old=data.uraian;

                $(this.$refs.modal_edit).modal({
                    backdrop:'static'
                });
            },
            addForm(context=null,data=null){
                if(context==null){
                    this.form_add.schema=this.list_schema[this.context]['DEF'];
                    this.form_add.context_def='';
                    this.form_add.parent_id=null;
                    if(this.form_add.schema.context.length==1){
                        this.form_add.context=this.form_add.schema.context[0];
                    }   

                }else{
                    this.form_add.schema=this.list_schema[this.context][context];
                    this.form_add.parent_id=data.id;
                    this.form_add.context_def=data.context;
                    this.form_add.context=context;
                }

                $(this.$refs.modal_add).modal({
                    backdrop:'static'
                });
            },
            transformSchema(arr=[]){
                return Object.entries(this.list_schema[this.context]).filter((el)=>{
                    return arr.includes(el[0]);
                });

            }
        },
        data:{
            tahun_list:<?=json_encode($tahun_list)?>,
            spliter:' <i class="fa fa-arrow-right" ></i> ',
            data:<?=json_encode($data)?>,
            context:'{{$context}}',
            form_add_indikator:{
                name:'',
                context:'',
                parent_context:'',
                parent_id:null,
                type_pemda:[],
                type:'',
                satuan:'',
                target:'',
                target1:'',
                target2:'',
                target3:'',
                target4:'',
            },
            form_add:{
                parent_id:null,
                context_def:'',
                context:null,
                schema:{
                    context:[],
                    use_ind:null,
                    sub:null
                },
            },
            form_delete:{
                uraian_old:'',
                uraian:'',
                context:'',
                id:''
            },
            form_edit:{
                uraian:'',
                context:'',
                id:''
            },
            list_schema:{
                SPM:{
                    DEF:{
                        name:'LAYANAN',
                        context:['LAYANAN'],
                        use_ind:true,
                        ind_context:[],
                    },
                    LAYANAN:{
                        name:'LAYANAN',
                        context:[],
                        use_ind:true,
                        ind_context:[],
                    }
                },
                RPJMN:{
                    DEF:{
                        name:'KONDISI SAAT INI',
                        context:['KONDISI_SAAT_INI'],
                        use_ind:true,
                        ind_context:[],
                    },
                    "KONDISI_SAAT_INI":{
                        name:'KONDISI SAAT INI',
                        context:['ISU_STRATEGIS'],
                        use_ind:true,
                        ind_context:[],
                    },
                    "ISU_STRATEGIS":{
                        name:'ISU STRATEGIS',
                        context:['ARAH_KEBIJAKAN'],
                        use_ind:true,
                        ind_context:[],
                    },
                    "ARAH_KEBIJAKAN":{
                        name:'ARAH KEBIJAKAN',
                        context:[],
                        use_ind:true,
                        ind_context:[],
                    }
                },
                SDGS:{
                    DEF:{
                        name:'GOAL',
                        context:['GOAL'],
                        use_ind:true,
                        ind_context:[],

                    },
                    GOAL:{
                        name:'Goal',
                        context:[],
                        use_ind:true,
                        ind_context:['PROGRAM']
                    }
                },
                URUSAN:{
                    DEF:{
                        name:'kinerja',
                        context:['KINERJA'],
                        use_ind:true
                    },
                    KINERJA:{
                        name:'kinerja',
                        context:[],
                        ind_context:['SUB KEGIATAN'],
                        use_ind:true
                    },
                    
                },
                RKP:{
                    DEF:{
                        name:'',

                        context:['MAJOR','PN'],     
                        use_ind:null,                   
                    },
                    MAJOR:{
                        name:'Major',
                        context:[],
                        use_ind:true,                   
       
                    },
                    PN:{
                        name:'Prioritas Nasional',
                        context:['PP'],
                        ind_context:[],

                        use_ind:false,                   

                        
                    },
                    PP:{
                        name:'Program Prioritas',
                        context:['KP'],  
                        use_ind:true,     
                        ind_context:['PROGRAM'],



                    },
                    KP:{
                        name:'Kegiatan Prioritas',

                        context:['PROP'], 
                        use_ind:true,   
                        ind_context:['KEGIATAN'],                


                    },
                    PROP:{
                        name:'Proyek Prioritas',
                        context:[], 
                        ind_context:['SUB KEGIATAN'],
                        use_ind:true,                   

                    }
                }
            }
        }
    })
</script>


@stop
