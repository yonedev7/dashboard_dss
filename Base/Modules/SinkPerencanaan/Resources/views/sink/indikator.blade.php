@extends('adminlte::page')
@section('content_header')
    <h4>Sinkronisasi RPJMN - {{$StahunRoute}}</h4>
    <p>{{$pemda->nama_pemda}}</p>
@stop

@section('content')
    <div class="container-fluid" id="sink-pemda">
        <div v-if="rpjmn_indikator.length==0" class="text-center bg-white rounded p-3">
            <h3>Data Tidak Tersedia</h3>
            <p>Belum Terdapat Matrix RPJMN yang Tertaging Dalam Nomenklatur RKPD</p>

        </div>
        <div class="card" v-if="i==page_active" v-for="item,i in rpjmn_indikator">
            <div class="card-header d-flex">
                <div class="mr-3 bg-primary rounded p-2 ">
                    <p class="align-self-center"><b>@{{(page_active+1)+' / '+(rpjmn_indikator.length)}}</b></p>
                </div>
               <div>
                <p class="m-0"><b>@{{item.context}} :</b> @{{item.keterangan_sumber}}</p>
               
               </div>

            </div>
            <div class="card-body">
               <div class="table-responsive">
                <table class="table table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th>IND</th>
                            <th>TARGET</th>
                            <th>SATUAN</th>
                            <th>KETERANGAN</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <p><small>@{{item.name}}</small></p>
                            </td>
                            <td>@{{item.target}}</td>
                            <td>@{{item.satuan}}</td>
                            <td v-html="item.keterangan"></td>
                        </tr>
                    </tbody>
                </table>
               </div>
               <hr>
              


                <h4>RKPD {{$StahunRoute}} <span> - <small>{{$pemda->nama_pemda}}</small></span></h4>
                <hr>
               <template v-for="itemk,ik in item.subkegiatan">

                <p class="sticky-top bg-white p-2 border-bottom" style="top:60px;"><span class="badge badge-primary">Sub Kegiatan</span> <b>@{{itemk.name}}</b>  <span class="badge bg-warning">Rp. @{{numParse(itemk.pagu)}}</span></p>
                <div class="table-responsive">
                 <table class="table table-bordered">
                     <thead class="thead-dark">
                         <tr>
                             <th>IND</th>
                             <th>TARGET</th>
                             <th>SATUAN</th>
                         </tr>
                     </thead>
                     <tbody>
                       <template v-if="!itemk.uuid">
                        <tr  class="bg-danger">

                            <td colspan="3"> 
                                <p class="text-center m-0">
                                    <b>Tidak Terdapat Kegiatan</b>
                                </p>
                            </td>
                        </tr>
                       </template>
                       <template v-else>
                        <tr v-if="itemk.indikator.length==0" class="bg-warning">
                            <td colspan="3">
                                <p class="text-center m-0">
                                    <b>Tidak Terdapat Indikator</b>
                                </p>
                            </td>
                        </tr>
                        <tr v-for="ind,id in itemk.indikator">
                            <td>@{{ind.name}}</td>
                            <td>@{{ind.target}}</td>
                            <td>@{{ind.satuan}}</td>
                        </tr>
                       </template>
                      
                        
                     </tbody>
                 </table>
                </div>
               </template>

               <div class="sticky-top bg-white pb-3 pt-2 sd" style="bottom:0;">
                    <div  v-if="isDraf()" class="bg-hightlight px-2 mb-2">
                        <b>DRAF </b>
                    </div>
                    <div v-else class="btn-group-sm">
                        <template v-if="item.rekomendasi.uuid">
                            <button v-if="item.rekomendasi.status==0" @click="item.rekomendasi.status=1" class="btn btn-primary">Lock Rekomendasi</button>
                            <button  v-if="item.rekomendasi.status==1" @click="item.rekomendasi.status=0"  class="btn btn-warning">UnLock Rekomendasi</button>
                        </template>

                    </div>
                    <h4 style="z-index:10;" class="bg-white mt-2" >REKOMENDASI <span> - <small>{{$pemda->nama_pemda}}</small></span></h4>
                    <p><small>@{{item.context}} - @{{item.name}}</small></p>
                    <hr>
                    <template v-if="item.rekomendasi.status==1">
                        <p v-html="item.rekomendasi.rekomendasi"></p>
                    </template>
                    <template v-else>
                        <textarea name="" v-model="item.rekomendasi.rekomendasi" @input="change_rekom(i)" class="form-control mb-2" id="" cols="30" rows="6"></textarea>
                    </template>


                    <div class="btn-group">
                        <button class="btn btn-primary" v-if="item.rekomendasi.status!=1" @click="submit_rekom(i)">Kirim</button>
                        <button class="btn  btn-info" v-if="(page_active+1)!=1" @click="page_active-=1"><i class="fa fa-arrow-left"></i></button>
                        <button class="btn btn-info" disabled>@{{(page_active+1)+' / '+(rpjmn_indikator.length)}} @{{see_drafted.length?'| Page Draf ('+see_drafted.join(' , ')+')':''}}</button>
                        <button class="btn  btn-info"  @click="page_active+=1"  v-if="(page_active+1)!=(rpjmn_indikator.length)"><i class="fa fa-arrow-right"></i></button>

                    </div>
               </div>
            </div>
        </div>
    </div>
@stop


@section('js')
<script>
    var Vapp=new Vue({
        el:'#sink-pemda',
        data:{
            page_active:0,
            rpjmn_indikator:<?=json_encode($ind_listed)?>,
            state_ind:<?=json_encode(array_map(function($el){return ['id'=>$el->id,'state'=>true];},$ind_listed))?>
        },
        created:function(){
           
        },
        methods: {
            change_rekom:function(i){
                console.log('recom draf ',i);   
                this.state_ind[i].state=false;
                console.log(this.state_ind,this.isDraf());
            },
            submit_rekom:function(i){
                console.log('jskjsk sjksj',i);
                if(!this.page_active==i){
                    
                }
                this.$isLoading(true);
                var self=this;

                req_ajax.post('{{$__url_rekom}}',{
                    data:this.rpjmn_indikator[i].rekomendasi
                }).then(res=>{
                    if(res.data.code==200){
                        self.state_ind[i].state=true;
                        self.rpjmn_indikator[i].rekomendasi=res.data.data;
                    }
                    
                }).finally(function(){
                    self.$isLoading(false);
                });
            },
            isDraf:function(){
                return this.state_ind[this.page_active].state==false;
            },
            numParse:function(val=0){
                if(val){
                return window.NumberFormat(parseFloat(val));

                }else{
                    return 0;
                }
            }
        },
        computed:{
            see_drafted:function(){
                var draf=[];
                this.state_ind.forEach((el,i)=>{
                    if(el.state==false){
                        draf.push(i+1);
                    }
                });

                return draf;
            }
        }
    })
</script>
@stop
@section('css')
<style>
    .bg-hightlight{
        background-color: aquamarine;
    }
    .sd::before{
        content: "";
        width: 100%;
        position: absolute;
        top:0px;
        height: 1px;
        background: transparent;
        background: #888;
        z-index: 1;
        box-shadow: 0px 30px 0px 0px transparent, 0px -10px 20px #888;
    }

</style>

@stop