@extends('adminlte::page')
@section('content_header')
    <h4>Sinkronisasi RPJMN - RPJMD {{$StahunRoute}}</h4>
    <p>{{$pemda->nama_pemda}}</p>
@stop

@section('content')
    <div class="container-fluid" id="app">
        <div class="table-responsive bg-white w-100">
            <table class="table table-bordered table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th colspan="4">RPJMN</th>
                        <th rowspan="2">Aksi</th>
                        <th colspan="4">RPJMD</th>

                    </tr>
                    <tr>
                        <th>Sumber</th>

                        <th>Nama</th>
                        <th>Target</th>
                        <th>Satuan</th>
                        <th>Sumber</th>
                        <th>Nama</th>
                        <th>Target</th>
                        <th>Satuan</th>
                    </tr>
                   
                    
                </thead>
                <tbody>
                    <template v-for="item,i in rpjmn">    
                    <tr >
                        <td ><p v-html="item.keterangan_sumber"></p></td>
                        <td>@{{item.name}}</td>
                        <td>@{{item.target}}</td>
                        <td>@{{item.satuan}}</td>
                        <td>
                            <button class="btn btn-primary btn-sm col-12" @click="sinkModal(i)" >Sink</button>
                        </td>
                        <template v-if="item.rpjmd[0]!=undefined">
                            <td v-html="item.rpjmd[0].keterangan_sumber"></td>
                            <td>@{{item.rpjmd[0].name}}</td>
                            <td>@{{item.rpjmd[0].target}}</td>
                            <td>@{{item.rpjmd[0].satuan}}</td>

                        </template>
                        <template v-else>
                            <td></td>
                            <td></td>

                            <td></td>
                            <td></td>
                        </template>

                    </tr>
                    <template v-for="ind,k in item.rpjmd"> 
                        <tr v-if="k>0">
                            <td colspan="5"></td>
                          
                             <td v-html="ind.keterangan_sumber"></td>
                            <td>@{{ind.name}}</td>
                            <td>@{{ind.target}}</td>
                            <td>@{{ind.satuan}}</td>

                        </tr>
                    </template>
                    </template>
                </tbody>
            </table>
        </div>
        <div ref="modal_sink" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-full" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="my-modal-title">Indikator RPJMD {{$pemda->nama_pemda}}</h5>
                        <button class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p class="m-0" v-if="sink_index>-1"><b> @{{rpjmn[sink_index].keterangan_sumber}}</b></p>
                        <p v-if="sink_index>-1">Sinkronisasi RPJMN - @{{rpjmn[sink_index].name}}</p>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead class="thead-dark">
                                    <tr>
                                        <th style="width:70px;">AKSI</th>
                                        <th>SUMBER</th>
                                        <th>NAMA</th>
                                        <th>TARGET</th>
                                        <th>SATUAN</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="item,i in rpjmd " v-if="sink_index>-1">
                                        <td>
                                            <template v-if="checkAdded(item.id)">
                                                <button class="btn btn-danger" @click="removeRpjmd(item)"><i class="fa fa-arrow-right"></i></button>
                                            </template>
                                            <template v-else>
                                                <button class="btn btn-primary" @click="addRpjmd(item)"><i class="fa fa-arrow-left"></i></button>
                                            </template>    
                                        </td>
                                        <td v-html="item.keterangan_sumber"></td>
                                        <td>@{{item.name}}</td>
                                        <td>@{{item.target}}</td>
                                        <td>@{{item.satuan}}</td>

                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop


@section('js')
<script>
    var Vapp= new Vue({
        el:'#app',
        data:{
            rpjmn: <?=json_encode($rpjmn)?>,
            rpjmd:<?=json_encode($rpjmd)?>,
            sink_index:-1,

        },
        methods: {
            sinkModal:function(index){
                this.sink_index=index;
                $(this.$refs.modal_sink).modal();
            },
            addRpjmd:function(item){
                this.rpjmn[this.sink_index].rpjmd.push(item);
                this.updateSink(this.sink_index);

            },
            removeRpjmd:function(item){
                var ids=this.rpjmn[this.sink_index].rpjmd.map((el,i)=>{
                    return el.id;
                });

                if(ids.includes(item.id)){
                    var index=ids.indexOf(item.id);
                    this.rpjmn[this.sink_index].rpjmd.splice(index,1);
                }else{

                }
                this.updateSink(this.sink_index);
            },
            updateSink(index){
                var self=this;
                self.$isLoading(true);
                req_ajax.post('{{route('api.mod-sink.sink.rpjmn-rpjmd.save',['tahun'=>$StahunRoute,'kodeurusan'=>$kodeurusan,'kodepemda'=>$pemda->kodepemda])}}',{
                    rpjmn_id:this.rpjmn[index].id,
                    rpjmd:this.rpjmn[index].rpjmd
                }).then(res=>{
                    if(res.data.code==200){
                        self.rpjmn[index].rpjmd=res.data.data;
                    }
                }).finally(function(){
                    self.$isLoading(false);
                });
            },
            checkAdded(id){
                var exist=this.rpjmn[this.sink_index].rpjmd.filter(el=>{
                   return el.id==id;
                });

                return exist.length>0?true:false;
            }
        },
    })

</script>


@stop