<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/sinkperencanaan', function (Request $request) {
    return $request->user();
});


Route::middleware('auth:api')->prefix('mod-sink/{tahun}')->group(function(){
    Route::prefix('rekomendasi')->group(function(){
        Route::post('save/{kodeurusan}/{kodepemda}','IndikatorController@saveRekom')->name('api.mod-sink.m.indikator.rekom.save');


    });
    
    Route::prefix('m-rpjmn/')->group(function(){
        Route::post('load-nomen-nest/{id_parent}','RpjmnController@loadNomen')->name('api.mod-sink.m.rpjmn.load-nest');
        Route::post('r-add','RpjmnController@addNomen')->name('api.mod-sink.m.rpjmn.add');
        Route::post('r-delete','RpjmnController@deleteNomen')->name('api.mod-sink.m.rpjmn.delete');
        Route::post('r-edit','RpjmnController@editNomen')->name('api.mod-sink.m.rpjmn.edit');
       
        Route::prefix('sink-nomen')->group(function(){
            Route::post('save','RpjmnController@sinkNomenTag')->name('api.mod-sink.sink.rpjmn.save');
            Route::post('sink-ind-nomen/{scope}','IndikatorController@indSinkNomen')->name('api.mod-sink.sink.nomen-ind');

        });

        Route::prefix('sink-rpjmd/{kodeurusan}/{kodepemda}')->group(function(){
            Route::post('save','RpjmnController@sinkRPJMD')->name('api.mod-sink.sink.rpjmn-rpjmd.save');
            

        });

       



    });
});