<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('dash/{tahun}/sistem-informasi/mod-sink')->middleware(['auth:web','bindTahun'])->group(function() {
    Route::get('/pusat/{context}','SumberIndikatorController@index')->name('mod-sink.master.pusat');
    Route::post('/pusat/{context}/add','SumberIndikatorController@store')->name('mod-sink.master.pusat.store');
    Route::delete('/pusat/{context}/delete','SumberIndikatorController@delete')->name('mod-sink.master.pusat.delete');
    Route::put('/pusat/{context}/update','SumberIndikatorController@update')->name('mod-sink.master.pusat.update');
    Route::post('/pusat/{context}/ind/add','SumberIndikatorController@addInd')->name('mod-sink.master.pusat.indikator.add');

    Route::get('/', 'IndikatorController@index')->name('mod-sink.index');
    Route::get('/2', 'IndikatorController@index2')->name('mod-sink.index2');


    Route::get('/mastering/{scopemda}', 'IndikatorController@master')->name('mod-sink.master');

    Route::get('/mastering-pemda/{kodepemda}', 'IndikatorController@master_pemda')->name('mod-sink.master.pemda');

    Route::post('/mastering/{scopemda}', 'IndikatorController@storeUpdate')->name('mod-sink.ui');
    Route::delete('/mastering/{scopemda}', 'IndikatorController@delete')->name('mod-sink.delete');



    Route::get('/m-rpjmn', 'RpjmnController@index')->name('mod-sink.m.rpjmn');
    Route::get('/m-rpjmn/map-rkpd', 'RpjmnController@index')->name('mod-sink.m.rpjmn.sink-rkpd');


    Route::get('/m-indikator-urusan/{kodeurusan}', 'UrusanController@index');
    Route::get('/m-rpjmd', 'RpjmdController@index');

    Route::prefix('sink-nomen')->group(function(){
    
        Route::get('rpjmn/{kodeurusan}','RpjmnController@nomenSink')->name('sink.rpjmn');
    
    });


    Route::prefix('sinkronisasi/{kodeurusan}/{kodepemda}')->group(function(){

        Route::get('/', 'SinkPerencanaanController@index')->name('mod-sink.sink.ind.index');
        Route::get('/rpjmd', 'SinkPerencanaanController@index_sink_rpjmd')->name('mod-sink.sink.ind.rpjmd');



    });





});
