<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('master.static_view', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('keterangan')->nullable();
            $table->bigInteger('prefer_dataset')->nullable()->unsingned();
            $table->mediumText('schema')->default('[]');

            $table->foreign('prefer_dataset')
            ->references('id')
            ->on('master.dataset')
            ->onDelete('cascade')
            ->onUpdate('cascade');


            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('master.static_view');

    }
};
