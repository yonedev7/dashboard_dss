<?php

namespace Modules\StaticViewModul\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Auth;
class StaticViewModulController extends Controller
{
    public static function index($tahun,$id,Request $request){
        if(Auth::check()){
            $user=Auth::User();
        }

       $meta= DB::table('master.static_view as sv')->where('id',$id_dataset)->first();

       if($meta){
           $id_dataset=$meta->prefer_dataset;
           $schema=json_decode($meta->schema??'{"view":""}',true);
            return view('staticviewmodul::'.$schema['view'])->with(['schema'=>$schema,'meta'=>$meta]);
       }else{
           return '';
       }

    }
}
