@php

    $sqlsipd=DB::table('dataset.'.$schema['schema']['sipd']['table'].' as sipd')
    $sqlgabung=DB::table($sqlsipd)

@endphp
<table class="table table-bordered">
    <thead>
        <tr>
            <th colspan="10">RKPD SIPD</th>
        </tr>
        <tr>
            <th>KODEPEMDA</th>
            <th>NAMA PEMDA</th>
            <th>KODE PROGRAM</th>
            <th>NAMA PROGRAM</th>
            <th>KODE KEGIATAN</th>
            <th>NAMA KEGIATAN</th>
            <th>KODE SUBKEGIATAN</th>
            <th>NAMA SUBKEGIATAN</th>
            <th>PAGU</th>
        </tr>
        <tr>
            <th colspan="10">RKPD FGD</th>
        </tr>
        <tr>
            <th>KODEPEMDA</th>
            <th>NAMA PEMDA</th>
            <th>KODE PROGRAM</th>
            <th>NAMA PROGRAM</th>
            <th>KODE KEGIATAN</th>
            <th>NAMA KEGIATAN</th>
            <th>KODE SUBKEGIATAN</th>
            <th>NAMA SUBKEGIATAN</th>
            <th>PAGU</th>
        </tr>
    </thead>
</table>