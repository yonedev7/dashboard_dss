<?php

namespace Modules\TematikModul\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Storage;
class SkemaHalamanController extends Controller
{
   public function save($tahun,$context=null,Request $request){
        $a=array_filter([$context,$request->schema_kode],function($e)
        {
            return $e;
        });
        Storage::put('skema_tampilan/'.$tahun.'/'.implode('_',$a).'.json',json_encode($request->shcema,true));
        return [
            'code'=>200,
            'payload'=>[
                'schema_key'=>$context,
                'schema_kode'=>$request->schema_kode
            ]
        ];

   }

}
