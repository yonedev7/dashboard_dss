<?php

namespace Modules\TematikModul\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use Auth;
class TematikModulController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index($tahun,$id)
    {
        return view('tematikmodul::index');
    }

    public function index_pendukung($tahun,$id=null)
    {
        $menus=DB::table('master.menus as m')->where([
            'type'=>2,
            'status'=>true
        ])->orderBy('index','asc')->get();
        if($id==null){
            $menu=$menus[0];
            $id=$menu->id;
        }else{
            $menu=DB::table('master.menus as m')->where([
                'type'=>2,
                'id'=>$id
            ])->first();
        }


        $data=DB::table('master.dataset')->where('id_menu',$id)->where('status',1)
        ->orderBy('index','asc')->get();
        
        $page_meta=[
            'title'=>'DATA PENDUKUNG',
            'keterangan'=>''
        ];
        foreach($data as $k=>$d){
            $tahun_x=$tahun;
            $data[$k]->tahun_exist=null;
            if(str_contains($d->table,'[TAHUN]')){
                do{
                    $tahun_x=-1;
                    $tb=str_replace('[TAHUN]',$tahun_x,$d->table);
                    $exist_table=DB::table('pg_tables')->where('schemaname','dataset')->where('tablename',$tb)->first();
                    if($exist_table){
                        $z=DB::table('dataset.'.$tb.' as d')
                        ->join('status_data.dts_'.$d->id.' as str','d.kodepemda','=','d.kodepemda')
                        ->select('d.tahun')
                        ->groupBy('d.tahun')
                        ->orderBy('d.tahun','desc')->first();
                        $data[$k]->tahun_exist=$z?$z->tahun:null;
                    }

                    if( $data[$k]->tahun_exist==null AND (($tahun-$tahun_x)>=5) ){
                        $data[$k]->tahun_exist=$tahun;
                    }

                }while( $data[$k]->tahun_exist==null);


            
                if($data[$k]->tahun_exist==null){
                    $data[$k]->tahun_exist=$tahun;
                }

            }else{
                $z=DB::table('dataset.'.$d->table.' as d')
                ->join('status_data.dts_'.$d->id.' as str','d.kodepemda','=','d.kodepemda')
                ->select('d.tahun')
                ->groupBy('d.tahun')
                ->orderBy('d.tahun','desc')
                ->first();
                $data[$k]->tahun_exist=$z?$z->tahun:$tahun;

            }
            $data[$k]->chart=[
                'chart'=>[
                    'type'=>'area',
                    'height'=>200,
                ],
                'title'=>[
                    'text'=>$d->name
                ],
                'subtitle'=>[
                    'text'=>'Loading...'
                ],
                'xAxis'=>[
                    'type'=>'category'
                ],
                'yAxis'=>[
                    'title'=>[
                        'text'=>'Jumlah Pemda'
                    ]
                ],
                'legend'=>[
                    'enabled'=>false
                ],
                'plotOptions'=>[
                    'series'=>[
                        'dataLables'=>[
                            'enabled'=>true,
                        ]
                    ]
                ],
                'series'=>[
                    [
                        'name'=>$d->name,
                        'data'=>[]
                    ]
                ]
                
            ];
        }

        return view('tematikmodul::index_pendukung')->with([
            'menus'=>$menus,
            'page_meta'=>$page_meta,
            'menu'=>$menu,
            'data'=>$data,
        ]);
    }

    public function show_list($tahun,$id)
    {
        $menu=DB::table('master.menus')->find($id);
        if($menu){
            if($menu->kode_parent=='PROFILE'){
                return redirect()->route('mod.profile.index',['tahun'=>$tahun]);
            }

            $data=DB::table('master.dataset')->where('id_menu',$id)->where('status',1)
            ->orderBy('index','asc')->get();
            $page_meta=[
                'title'=>$menu->title,
                'keterangan'=>$menu->keterangan
            ];

            if(Auth::check()){
                $analisa=DB::table('analisa.partials_data')->where('id_menu',$menu->id)->orderBy('index','asc')->get();

            }else{
                $analisa=[];
            }
            
            
            foreach($data as $k=>$d){
                $tahun_x=((int)$tahun)+1;
                $data[$k]->tahun_exist=null;
                if(str_contains($d->table,'[TAHUN]')){
                    do{
                        $tahun_x=$tahun_x-1;
                        $tb=str_replace('[TAHUN]',$tahun_x,$d->table);
                        $tbs='dts_'.$d->id.'_'.$tahun_x;
                        $exist_table=DB::table('pg_tables')->whereIn('schemaname',['dataset','status_data'])
                        ->whereIn('tablename',[$tb,$tbs])->get();
                        if(count($exist_table)>=2){
                            $z=DB::table('dataset.'.$tb.' as d')
                            ->join('status_data.'.$tbs.' as str',[['str.tahun','=','d.tahun'],['str.kodepemda','=','d.kodepemda'],['str.status','=',DB::raw(2)]])
                            ->select('d.tahun')
                            ->groupBy('d.tahun')
                            ->orderBy('d.tahun','desc')->first();
                            $data[$k]->tahun_exist=$z?$z->tahun:null;
                        }

                        if( $data[$k]->tahun_exist==null AND (($tahun-$tahun_x)>=5) ){
                            $data[$k]->tahun_exist=$tahun;
                        }

                    }while( $data[$k]->tahun_exist==null);


                
                    if($data[$k]->tahun_exist==null){
                        $data[$k]->tahun_exist=$tahun;
                    }

                }else{
                    $z=DB::table('dataset.'.$d->table.' as d')
                    ->join('status_data.dts_'.$d->id.' as str',[['str.tahun','=','d.tahun'],['str.kodepemda','=','d.kodepemda'],['str.status','=',DB::raw(2)]])
                    ->select('d.tahun')
                    ->groupBy('d.tahun')
                    ->orderBy('d.tahun','desc')
                    ->first();
                    $data[$k]->tahun_exist=$z?$z->tahun:$tahun;
                    

                }
                $data[$k]->chart=[
                    'chart'=>[
                        'type'=>'area',
                        'height'=>200,
                    ],
                    'title'=>[
                        'text'=>$d->name
                    ],
                    'subtitle'=>[
                        'text'=>'Loading...'
                    ],
                    'xAxis'=>[
                        'type'=>'category'
                    ],
                    'yAxis'=>[
                        'title'=>[
                            'text'=>'Jumlah Pemda'
                        ]
                    ],
                    'legend'=>[
                        'enabled'=>false
                    ],
                    'plotOptions'=>[
                        'series'=>[
                            'dataLables'=>[
                                'enabled'=>true,
                            ]
                        ]
                    ],
                    'series'=>[
                        [
                            'name'=>$d->name,
                            'data'=>[]
                        ]
                    ]
                    
                ];
            }
            return view('tematikmodul::dashboard.index')->with(['analisa'=>$analisa,'data'=>$data,'menu'=>$menu,'page_meta'=>$page_meta]);
        }
       
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('tematikmodul::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    
}
