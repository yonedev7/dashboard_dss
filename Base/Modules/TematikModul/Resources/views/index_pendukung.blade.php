@extends('adminlte::page')
@section('content')
<div class="parallax pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
    <div class="container">
        <section >
            <p><b>{!!$page_meta['keterangan']!!}</b></p>
        
        </section>
    </div>
</div>


<div id="app">
    <div class="p-3 bg-dark">
        <nav class="navbar navbar-expand-lg  p-0">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
              <ul class="navbar-nav">
                <template v-for="item in menus" >
                 <li class="nav-item active bg-primary" v-if="item.id==meta.id">
                    <a class="nav-link text-uppercase " href="#" > 
                    
                        <span >@{{item.title}}</span>
                    </a>
                </li>
                <li class="nav-item " v-else>
                    <a class="nav-link text-uppercase " :href="('{{route('mod.tematik.index_pendukung',['tahun'=>$StahunRoute,'id'=>'@?'])}}').yoneReplaceParam('@?',[item.id])" > 
        
                        <span >@{{item.title}}</span>
                    </a>
                </li>
                </template>
               
              </ul>
            </div>
          </nav>
    </div>
    <div class="parallax pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
        <div class="container">
            <section >
                <h3><b class="text-uppercase">@{{meta.title}}</b></h3>
                <p><b v-html="meta.keterangan"></b></p>
            </section>
        </div>
    </div>
    <div class="row no-gutters">
        <div class="col-md-3 pr-3 pt-3">
            <div class="" v-for="item,i in analisa"></div>

        </div>
        <div class="col-md-9 pr-3 pt-3">
            <div class="d-flex mb-3 ">
                <div class="flex  p-2  bg-white shadow rounded ">
                    <div class="d-flex flex-column  align-items-center ">
                        {{-- <h4 class="m-0 mr-2  p-1">Dataset : </h4> --}}
                    </div>
                    
                    <div class="d-flex flex-grow-1 align-items-center">
                        <div class="d-flex">
                            <span class="badge mr-2 bg-navy">Linkup Data</span>
                        <span class="badge mr-2 bg-primary">Tipe Bantuan</span>
                        <span class="badge mr-2 bg-success">Ketegori</span>
                        </div>
                    </div>
                </div>
            </div>
           
            <div v-for="item,i in data" class="shadow d-flex mb-3 rounded align-items-stretch" >
                <div class="d-flex  rounded p-2"  v-show="item.hover"  style=" background-color:#ffffff9e; transition: width 2s, height 4s; overflow:hidden; " >
                    <transition name="fade">
                    
                        <charts  v-if="item.hover" :options="item.chart"></charts>
                    </transition>

                </div>
                <div  class=" flex-grow-1 p-2 flex-column  d-flex rounded-right" style="background-image: url({{url('assets/img/bgcard.png')}}); transition: width 2s, height 4s; background-size:100% auto; position: relative;">
                    <div class="d-flex">
                        <span class="badge mr-2 bg-navy">@{{item.klasifikasi_1}}</span>
                        <template v-for="tag in buildTag(item.klasifikasi_2,'bg-primary')">
                            <span :class="'badge mr-2  '+tag.color">@{{tag.name}}</span> 
                        </template>
                        <template v-for="tag in buildTag(item.kategori,'bg-success')">
                            <span :class="'badge mr-2 '+tag.color">@{{tag.name}}</span> 
                        </template>
                    </div>
                    <div class="pt-2  flex-grow-1 d-flex">
                    
                        <div class="flex-grow-1 flex-column d-flex">
                            <div class="flex-grow-1  d-flex">
                                <div class="align-self-center">
                                    <h4 class="text-uppercase m-0"><b>@{{item.name}} Tahun @{{item.tahun_exist}}</b></h4>  
                                    <small>Update Terahir : @{{showDate(item.last_validate)}}</small>
                                    <p style="margin-bottom: 50px;" class="mt-1">@{{item.keterangan}}</p>
                                </div>
                            </div>
                            <div class="d-flex" style="height: 30px;">
                                <div class="d-flex justify-content-first">
                                    <a :href="('{{route('mod.dash.dataset.index',['tahun'=>'@?','id'=>'@?'])}}').yoneReplaceParam('@?',[item.tahun_exist||{{$StahunRoute}},item.id])" class="btn btn-primary  btn-sm self-align-center text-white "><i class="fa fa-arrow-right"></i> Detail
                                    </a> 
                                </div>
                            </div>
                        </div>

                        
                    </div>

                        
                    
                    
                    
                </div>
            </div>
        </div>
    </div>

</div>

@stop


@section('js')
<script>
    var app=new Vue({
        el:'#app',
        data:{
            meta:<?=json_encode($menu)?>,
            data:[],
            menus:<?=json_encode($menus)?>,
            analisa:[],
        },
        methods:{
            showDate:function(date){
                if(date){
                    return  moment(date).format('YYYY-MM-DD');

                }else{
                    return '-';
                }
             },
            hover:function(i,val){
                this.data[i].hover=val;
            },
            buildTag:function(data,color_class){
                data=JSON.parse(data||'[]');
                return data.map(el=>{
                    return {
                        name:el,
                        color:color_class
                    }
                });
            }
        },
        created:function(){
            var self=this;
            var data=<?=json_encode($data)?>;
            data.forEach((element,key) => {
                data[key]['hover']=true;
                data[key]['last_validate']=null;

                data[key]['chart']={
                    chart:{
                        type:'pie',
                        height:200,
                        width:300
                    },
                    title:{
                        text:'',
                        enabled:false
                    },
                    tooltip: {
                    pointFormat: '{point.percentage:.1f} %<br>Total: {point.y:.0f} PEMDA'
                    },
                    plotOptions: {
                        pie: {
                            dataLabels: {
                            enabled: true,
                            format: '{point.percentage:.1f} %<br>Total: {point.y:.0f} PEMDA',
                            distance: -10,
                            }
                        }
                    },
                    subtitle:{
                        text:'',
                        enabled:true
                    },
                    xAxis:{
                        type:'category'
                    },
                    series:[]
                }
            });
            this.data=data;

           setTimeout((ref) => {
            ref.data.forEach((el,key) => {
                var self=this;
                this.$isLoading(true);
                req_ajax.post(('{{route('api-web-mod.dataset.public.data.state',['tahun'=>'@?','id_dataset'=>'@?'])}}').yoneReplaceParam('@?',[el.tahun_exist||{{$StahunRoute}},el.id])).then(res=>{
                    if(res.status==200){
                        if(res.data['series']!=undefined){
                            ref.data[key].chart.series=res.data.series;
                            ref.data[key].last_validate=res.data.last_validate;
                            ref.data[key].chart.subtitle.text=res.data.series[0].name+' LONGLIST';
                        }
                    }
                }).finally(function(){
                    ref.$isLoading(false);
                });
            });
           }, 500,self);

           

        }

    });
</script>

@stop


