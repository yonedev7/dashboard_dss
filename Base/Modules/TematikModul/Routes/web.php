<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('dash/{tahun}/sistem-informasi/module-tematik')->name('mod.tematik-admin')->middleware(['auth:web','bindTahun'])->group(function() {
    Route::prefix('menus')->name('.menu')->group(function(){
        Route::get('/','MenuController@index')->name('.index');
    });
    

});

Route::prefix('session/{tahun}/module-tematik')->name('mod.tematik')->middleware(['bindTahun'])->group(function() {
    Route::get('tema/{id}','TematikModulController@show_list')->name('.index');
    Route::get('pendukung/{id?}','TematikModulController@index_pendukung')->name('.index_pendukung');


    

});
