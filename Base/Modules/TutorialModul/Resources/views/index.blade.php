@extends('adminlte::page')

@section('content')
<div class="parallax pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
    <div class="container">
        <section >
            <h3><b>Tutorial</b></h3>
        </section>
    </div>   
</div>
<div id="app" class="p-3">
    <div class="row mt-3">
        <div class="col-12 col-sm-6 col-md-3" v-for="item in files ">
            <div class="info-box mb-3 bg-dark">
            <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-play"></i></span>
            <div class="info-box-content">
            <span class="info-box-text text-uppercase">@{{item.name}}</span>
            <button class="btn btn-primary btn-sm" @click="play(item.name,item.link)"><i class="fa fa-play"></i>   Detail</button>

            </div>
            
            </div>
            
            </div>
       
    </div>
    <div id="modal-play" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-uppercase" id="my-modal-title">@{{modal.name}}</h5>
                    <button class="close" data-dismiss="modal" @click="modal.link=''" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <iframe v-if="modal.link" :src="modal.link" style="min-height:500px; width:100%;" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    var app=new Vue({
        el:'#app',
        data:{
            modal:{
                name:'',
                link:''
            },
            files:<?=json_encode($files)?>
        },
        methods:{
            play:function(name,link){
                this.modal.name=name;
                this.modal.link=link;
                $('#modal-play').modal({backdrop:'static'});

            }
        }
    })
</script>

@stop
