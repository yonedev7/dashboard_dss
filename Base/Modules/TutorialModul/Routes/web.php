<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('session/{tahun}/modul-tutorial')->name('mod.tutorial')->middleware(['auth:web','bindTahun'])->group(function(){
    Route::get('/', 'TutorialModulController@index')->name('.index');
});
