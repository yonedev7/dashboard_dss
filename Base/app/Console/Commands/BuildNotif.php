<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Notifications\SendNotificationSlack;
use DB;
use Storage;
use Carbon\Carbon;
class BuildNotif extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notif-slack:build';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Build Message For Sluck State Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {   
        $webhook=[
            'I'=>'https://hooks.slack.com/services/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',
            'II'=>'https://hooks.slack.com/services/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',
            'III'=>'https://hooks.slack.com/services/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',
            'IV'=>'https://hooks.slack.com/services/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',
            'TEST'=>'https://hooks.slack.com/services/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',
            'GENERAL'=>'https://hooks.slack.com/services/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',
        ];

        $webhook=config('laravel-slack.slack_webhook_sorces',$webhook);

        $tahun=date('Y');

        $message=[];

        \Slack::to('@yuan')->webhook($webhook['TEST'])->send("only for\nyuan");
        $dataset=DB::table('master.dataset')->where('status',1)
        ->where('id_menu','!=',null)
        ->get();
        $report=[
            'date'=>date('Y-m-d'),
            'builded'=>Carbon::now(),
            'data'=>[

            ]
        ];
        foreach($dataset as $dts){
            $table=$dts->table;
            $status='dts_'.$dts->id;
            if(str_contains($dts->table,'[TAHUN]')){
                $status.='_'.$tahun;
                $table=str_replace('[TAHUN]',$tahun,$table);
            }

            $data=DB::table('master.master_pemda as mp')
            ->join('dataset.'.$table.' as d',[['d.kodepemda','=','mp.kodepemda'],['d.tahun','=',DB::raw($tahun)]])
            ->leftJoin('status_data.'.$status.' as std','std.kodepemda','=','d.kodepemda')
            ->groupBy('mp.kodepemda')
            ->selectRaw("max(mp.regional_1) as regional,mp.kodepemda, max(mp.nama_pemda) as nama_pemda,max(case when (std.status is not null) then std.status else 0 end ) as status_data")->get()->toArray();
            
            if($data){
                $message=[
                    'dataset'=>$dts->name.' Tahun '.$tahun,
                    'keterisian'=>[
                        'I'=>[
                            '0'=>[
                                'count'=>count(array_filter($data,function($el){ return $el->status_data==0 AND $el->regional=='I';})),
                                'pemdas'=>array_map(function($el){ return $el->nama_pemda;},array_filter($data,function($el){ return $el->status_data==0 AND $el->regional=="I";}))
                            ],
                            '1'=>[
                                'count'=>count(array_filter($data,function($el){ return $el->status_data==1 AND $el->regional=='I';})),
                                'pemdas'=>array_map(function($el){ return $el->nama_pemda;},array_filter($data,function($el){ return $el->status_data==1 AND $el->regional=="I";}))
                            ],
                            '2'=>[
                                'count'=>count(array_filter($data,function($el){ return $el->status_data==2 AND $el->regional=='I';})),
                                'pemdas'=>array_map(function($el){ return $el->nama_pemda;},array_filter($data,function($el){ return $el->status_data==2 AND $el->regional=="I";}))
                            ],
                            
                        ],
                        'II'=>[
                            '0'=>[
                                'count'=>count(array_filter($data,function($el){ return $el->status_data==0 AND $el->regional=='II';})),
                                'pemdas'=>array_map(function($el){ return $el->nama_pemda;},array_filter($data,function($el){ return $el->status_data==0 AND $el->regional=="II";}))
                            ],
                            '1'=>[
                                'count'=>count(array_filter($data,function($el){ return $el->status_data==1 AND $el->regional=='II';})),
                                'pemdas'=>array_map(function($el){ return $el->nama_pemda;},array_filter($data,function($el){ return $el->status_data==1 AND $el->regional=="II";}))
                            ],
                            '2'=>[
                                'count'=>count(array_filter($data,function($el){ return $el->status_data==2 AND $el->regional=='II';})),
                                'pemdas'=>array_map(function($el){ return $el->nama_pemda;},array_filter($data,function($el){ return $el->status_data==2 AND $el->regional=="II";}))
                            ],
                            
                        ],
                        'III'=>[
                            '0'=>[
                                'count'=>count(array_filter($data,function($el){ return $el->status_data==0 AND $el->regional=='III';})),
                                'pemdas'=>array_map(function($el){ return $el->nama_pemda;},array_filter($data,function($el){ return $el->status_data==0 AND $el->regional=="III";}))
                            ],
                            '1'=>[
                                'count'=>count(array_filter($data,function($el){ return $el->status_data==1 AND $el->regional=='III';})),
                                'pemdas'=>array_map(function($el){ return $el->nama_pemda;},array_filter($data,function($el){ return $el->status_data==1 AND $el->regional=="III";}))
                            ],
                            '2'=>[
                                'count'=>count(array_filter($data,function($el){ return $el->status_data==2 AND $el->regional=='III';})),
                                'pemdas'=>array_map(function($el){ return $el->nama_pemda;},array_filter($data,function($el){ return $el->status_data==2 AND $el->regional=="III";}))
                            ],
                            
                        ],
                        'IV'=>[
                            '0'=>[
                                'count'=>count(array_filter($data,function($el){ return $el->status_data==0 AND $el->regional=='IV';})),
                                'pemdas'=>array_map(function($el){ return $el->nama_pemda;},array_filter($data,function($el){ return $el->status_data==0 AND $el->regional=="IV";}))
                            ],
                            '1'=>[
                                'count'=>count(array_filter($data,function($el){ return $el->status_data==1 AND $el->regional=='IV';})),
                                'pemdas'=>array_map(function($el){ return $el->nama_pemda;},array_filter($data,function($el){ return $el->status_data==1 AND $el->regional=="IV";}))
                            ],
                            '2'=>[
                                'count'=>count(array_filter($data,function($el){ return $el->status_data==2 AND $el->regional=='IV';})),
                                'pemdas'=>array_map(function($el){ return $el->nama_pemda;},array_filter($data,function($el){ return $el->status_data==2 AND $el->regional=="IV";}))
                            ],
                            
                        ],
                        'NONNUWAS'=>[
                            '0'=>[
                                'count'=>count(array_filter($data,function($el){ return $el->status_data==0 AND $el->regional==NULL;})),
                                'pemdas'=>array_map(function($el){ return $el->nama_pemda;},array_filter($data,function($el){ return $el->status_data==0 AND $el->regional==NULL;}))
                            ],
                            '1'=>[
                                'count'=>count(array_filter($data,function($el){ return $el->status_data==1 AND $el->regional==NULL;})),
                                'pemdas'=>array_map(function($el){ return $el->nama_pemda;},array_filter($data,function($el){ return $el->status_data==1 AND $el->regional==NULL;}))
                            ],
                            '2'=>[
                                'count'=>count(array_filter($data,function($el){ return $el->status_data==2 AND $el->regional==NULL;})),
                                'pemdas'=>array_map(function($el){ return $el->nama_pemda;},array_filter($data,function($el){ return $el->status_data==2 AND $el->regional==NULL;}))
                            ],

                        ]
    
                    ]
                ];
    
                Storage::disk('local')->put('report_data/dts_'.$dts->id.'/'.$report['date'].'.json',json_encode($message));
            }

            foreach($message['keterisian'] as $reg=>$dr){
                $state=false;
                $state_tact=false;
                if($reg!='NONNUWAS'){
                    $me="• *Regional ".$reg." ".$message['dataset']."*\n";
                    $me_tact="• *Regional ".$reg." ".$message['dataset']."*\n";
                   
                    foreach($dr as $status=>$d){
                        if($d['count']){
                            switch($status){
                                case 0:
                                $state=true;
                                $me.=" DATA BELUM TERVERIFIKASI : ".$d['count']." Pemda \n```";
                                $me.=ucwords(strtolower(str_replace('KABUPATEN','KAB.',str_replace('PROVINSI','PROV.',implode(",",$d['pemdas'])))))."```\n\n";
                                
                                break;
                                case 1:
                                $state_tact=true;

                                $me_tact.=" DATA  TERVERIFIKASI : ".$d['count']." Pemda \n```";
                                $me_tact.=ucwords(strtolower(str_replace('KABUPATEN','KAB.',str_replace('PROVINSI','PROV.',implode(",",$d['pemdas'])))))."```\n\n";



                                break;
                                case 2:
                                $state=true;
                                $me.=" DATA  TERVALIDASI : ".$d['count']." Pemda \n```";
                                $me.=ucwords(strtolower(str_replace('KABUPATEN','KAB.',str_replace('PROVINSI','PROV.',implode(",",$d['pemdas'])))))."```\n\n";


                                    break;
                            }
                            

                           
        
                        }
                    }
                }else{
                    $me_tact="NON NUWAS "." ".$message['dataset']."*\n";
                    foreach($dr as $status=>$d){
                        if($d['count']){
                            switch($status){
                                case 0:
                                $state_tact=true;
                                $me_tact.=" DATA BELUM TERVERIFIKASI : ".$d['count']." Pemda \n```";
                                $me_tact.=ucwords(strtolower(str_replace('KABUPATEN','KAB.',str_replace('PROVINSI','PROV.',implode(",",$d['pemdas'])))))."```\n\n";

                                break;
                                case 1:

                                $me_tact.=" BELUM TERVALIDASI : ".$d['count']." Pemda \n```";
                                $me_tact.=ucwords(strtolower(str_replace('KABUPATEN','KAB.',str_replace('PROVINSI','PROV.',implode(",",$d['pemdas'])))))."```\n\n";

                                $state_tact=true;
                                break;
                                case 2:
                                // $me_tact.=" DATA  TERVALIDASI : ".$d['count']." Pemda \n```";
                                //     break;
                                }
                        }
                    }
                }
                if($state){
                    if(!isset($webhook[$reg])){
                    }else{
                    \Slack::webhook($webhook[$reg])->send($me);

                    }
                }

                if($state_tact){
                    \Slack::webhook($webhook['GENERAL'])->send($me_tact);
                }


            }
         


        }

        return Command::SUCCESS;
    }
}
