<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Illuminate\Auth\AuthenticationException;
class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var string[]
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var string[]
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */

    // public function render($request, Throwable $exception)
    // {
        
    //     if($exception->getStatusCode()!=500){
    //         if (file_exists(app_path('../resources/views/errors/'.$exception->getStatusCode().'.blade.php'))) {
    //             return response()->view('errors.'.$exception->getStatusCode(), [], 500);
    //         }
    //     }


       
     
    //     return parent::render($request, $exception);
    // }

    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

     protected function unauthenticated($request, AuthenticationException $exception)
        {
        
            if ($exception->guards()[0]=='api' ) {
                return response()->json(
                    ['error' => 'Unauthenticated.']
                , 401);
            }

            return redirect()->guest(route('login'));
        }
        
}
