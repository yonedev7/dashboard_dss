<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Laravel\Passport\Passport;
use Illuminate\Http\Request;
use Auth;
use DB;
class LoginController extends Controller
{
    
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

     public function showLoginForm(){
        return view('auth.login');
     }

     

    public function login(Request $request){

        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($credentials)) {
            static::authenticate($request);
            return redirect()->intended('home');

        }else{
            return back()->withInput()->withErrors([
                'email' => 'The provided credentials do not match our records.',
            ]);

        }
    
    }

    public static function authenticate(Request $request)
    {
        $id_session=session()->getId();
        if(!$id_session){
            $request->session()->regenerate();
            $id_session=session()->getId();
        }
        do{
            $token_api=md5($id_session).rand(0,100);
        }while(DB::table('public.sessions')->where('api_token',$token_api)->first());   

        DB::table('public.sessions')->where('id',$id_session)->update(['api_token'=>$token_api]);


    }


    public function logout(){
        if(Auth::check()){
            Auth::logout();
            return redirect()->route('home');
        }
        return back();
    }

    

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
