<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
class CatalogCtrl extends Controller
{
    //

    public function index($tahun,Request $request){
        $updated=DB::table('master.data as dt')
        ->join('master.dataset_data as ddd','ddd.id_data','=','dt.id')
        ->selectRaw('max(dt.updated_at) as updated_at, count(distinct(dt.id::text)) as jumlah_datum')
        ->where('dt.name',DB::raw('NOT ILIKE'),'%test%')->first();
        if($updated){
            $jumlah_datum=$updated->jumlah_datum;
            $updated=$updated->updated_at;
        }

        $updated=Carbon::parse($updated)->format('d F Y');



        $data=DB::table('master.data as dt')
        ->selectRaw("dt.*,s.name as nama_sumber")
        ->where('dt.name',DB::raw('NOT ILIKE'),'%test%')
        ->leftJoin('master.validator_data as s','s.id','=','dt.id_validator')
        ->join('master.dataset_data as ddd','ddd.id_data','=','dt.id')
        ->orderBy('dt.id','desc')->get();
        
        return view('pages.catalog.index')->with(['data'=>$data,'last_updated'=>$updated,'jumlah_data'=>$jumlah_datum]);
    }
}
