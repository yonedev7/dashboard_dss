<?php

namespace App\Http\Controllers\DataNUWSP;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
class CapaianSPMCtrl extends Controller
{
    static function filter($tahun,$filter,$basis,$where_extends=[]){
        $where=[['d.tahun','=',$tahun]];
        switch($filter){
            case "kota":
                $where[]=['mp.stapem','=',2];
            break;
            case "kab":
                $where[]=['mp.stapem','=',3];

            break;
            case "provinsi":
                $where[]=['mp.stapem','=',1];
            break;
        }
        switch(strtolower($basis)){
            case 'longlist':
            $where[]=['mp.tahun_proyek','>',0];

            break;
            case 'sortlist':
            $where[]=['mp.tahun_proyek','=',$tahun];
            break;
        }
        if(count($where_extends)){
            foreach($where_extends as $w){
                $where[]=$w;
            }
        }
        return $where;
    }

    static $color_keterisian=[
        '0%-25%'=>'#ff5757',
        '26%-50%'=>'#f2be32',
        '51%-75%'=>'#7ed957',
        '76%-100%'=>'#5271fe'
    ];


    public function index($tahun,Request $request){
        $basis='nasional';
        if($request->basis){
            $basis=$request->basis;
        }

        if($request->meta){
            $index_page=explode('-',base64_decode($request->meta));

            $page_meta=config('data_tampilan.'.$index_page[0])[$index_page[1]]['submenu'][$index_page[2]];

        }else{
            return abort(404);
        }

        return view('data.spm.index')->with([
            'basis'=>$basis,
            'color_keterisian'=>static::$color_keterisian,
            'page_meta'=>$page_meta
           
        ]);

    }

    public function load_rekap($tahun,Request $request){
        $data=DB::table('dataset.spm_capaian_air_minum as d')
        ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
        ->selectRaw('sum(terlayani_jp) as capaian_jp,
        sum(terlayani_bjp) as capaian_bjp,
        sum(total_jumlah_rumah) as total_rumah ,
        sum(belum_terlayani_akses_air_minum) as blm_terlayani ,
        count(*) as jumlah_pemda,count(case when(mp.stapem<=1) then mp.kodepemda else null end) as pemda_pro,count(case when(mp.stapem>1) then mp.kodepemda else null end) as pemda_kota');
        $where=static::filter($tahun,$request->filter,$request->basis,[['d.status','=',1],['d.tw','=',4]]);
        if(count($where)){
            $data=$data->where($where);
        }
        $data=$data->first();

        $def=[  
            'jumlah_pemda_pro'=>0,
            'jumlah_pemda_kab'=>0,
            
            'blm_terlayani'=>0,
            'total_rumah'=>0,
            'capaian_bjp'=>0,
            'capaian_jp'=>0,
        ];

     
        if($data->pemda_pro){
            $def['jumlah_pemda_pro']+=(int)$data->pemda_pro;
        }
        if($data->pemda_kota){
            $def['jumlah_pemda_kab']+=(int)$data->pemda_kota;
        }
        if($data->blm_terlayani){
            $def['blm_terlayani']+=(int)$data->blm_terlayani;
        }
        if($data->total_rumah){
            $def['total_rumah']+=(int)$data->total_rumah;
        }
        if($data->capaian_bjp){
            $def['capaian_bjp']+=(int)$data->capaian_bjp;
        }
        if($data->capaian_jp){
            $def['capaian_jp']+=(int)$data->capaian_jp;
        }
        return $def;
    }

    public function load_provinsi($tahun,Request $request){
        $data=DB::table('dataset.spm_capaian_air_minum as d')
        ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
        ->leftJoin('master.master_pemda as prov','prov.kodepemda','=',DB::raw("left(d.kodepemda,3)"))
        ->selectraw("count(distinct(mp.kodepemda)) as value, max(prov.nama_pemda) as name, max(prov.kodepemda) as kodepemda")
        ->groupBy(DB::raw("prov.kodepemda"));
    
        $where=static::filter($tahun,$request->filter,$request->basis,[['d.status','=',1],['d.tw','=',4]]);
        if(count($where)){
            $data=$data->where($where);
            
        }
        $data=$data->get();

        $where=array_filter($where,function($el){
            return (str_contains($el[0],'mp.'));
          
        });

        
        foreach($data as $key=>$d){
            $jumlah_pemda=DB::table('master.master_pemda as mp')
            ->where('mp.kodepemda','ilike',$d->kodepemda.'%');
            if(count($where)){
                $jumlah_pemda=$jumlah_pemda->where($where);
            }
            $jumlah_pemda=$jumlah_pemda->count();
            $data[$key]->jumlah_pemda=$jumlah_pemda;

            $data[$key]->persentase=($d->value)?($d->value/$jumlah_pemda)*100:0;
            if($data[$key]->persentase>0 AND $data[$key]->persentase<=25){
                $data[$key]->color=static::$color_keterisian['0%-25%'];
            }else if( $data[$key]->persentase>25 AND  $data[$key]->persentase <= 50){
                $data[$key]->color=static::$color_keterisian['26%-50%'];
            }else if( $data[$key]->persentase>50 AND  $data[$key]->persentase <= 75){
                $data[$key]->color=static::$color_keterisian['51%-75%'];
            }
            else if( $data[$key]->persentase>75){
                $data[$key]->color=static::$color_keterisian['76%-100%'];
            }

            $data[$key]->id=(int)$d->kodepemda;

        }

        return $data;

    }

    public function load_kota($tahun,Request $request){
        $data=DB::table('dataset.spm_capaian_air_minum as d')
        ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
        ->selectraw("d.belum_terlayani_akses_air_minum,mp.kodepemda,mp.nama_pemda as name, d.total_jumlah_rumah,d.terlayani_bjp as terlayani_bjp,d.terlayani_jp as terlayani_jp ,
        (case when ((0+d.terlayani_jp)=0 OR (d.total_jumlah_rumah+0)=0) then 0 else((d.terlayani_jp/d.total_jumlah_rumah)*100) end  ) as value");
        $where=static::filter($tahun,$request->filter,$request->basis,[['d.status','=',1],['d.tw','=',4],['d.kodepemda','ilike',$request->kodepemda.'%']]);
        if(count($where)){
            $data=$data->where($where);
        }
        $data=$data->get();
       
        $color=[
            '80% - 100%'=>['text'=>'TINGGI','color'=>'#3ec996'],
            '60% - 79%'=>['text'=>'SEDANG','color'=>'#f57e16'],
            '0% - 59%'=>['text'=>'RENDAH','color'=>'#ff5757'],
        ];

        foreach($data as $key=>$d){
            $data[$key]->id=(int)$d->kodepemda;

            if($d->value>=80){
                $data[$key]->color=$color['80% - 100%']['color'];
                $data[$key]->text=$color['80% - 100%']['text'];
            }
            else if($d->value<80 and $d->value>=60){
                $data[$key]->color=$color['60% - 79%']['color'];
                $data[$key]->text=$color['60% - 79%']['text'];
            }
            else if($d->value<60){
                $data[$key]->color=$color['0% - 59%']['color'];
                $data[$key]->text=$color['0% - 59%']['text'];
            }else{
                $data[$key]->color='#ddd';
                $data[$key]->text='Tidak Tedefinisi';
            }
        }

        return ['data'=>$data,'colors'=>$color];
    }

    



    
}
