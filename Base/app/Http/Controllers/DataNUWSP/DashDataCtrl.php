<?php

namespace App\Http\Controllers\DataNUWSP;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Validator;
use Carbon\Carbon;
use Alert;
use Auth;
class DashDataCtrl extends Controller
{
    //

    public function update($tahun,$id,Request $request){
        $valid=Validator::make($request->all(),[
            'name'=>'required|string',
            'kode_khusus'=>'nullable|string',
            'definisi'=>'nullable|string',
            'tipe_data'=>'required|string|in:integer,string,float',
            'satuan'=>'required|string',
            'arah_nilai'=>'required|boolean',
            'fungsi_aggregasi'=>'required|string',
        ]);

        if($valid->fails()){
            Alert::error('',($valid->messages()->all()[0]));
            return back()->withInput();
        }else{

            $rentang=[
                "number"=>[
                    "m"=>null,
                    "val"=>[]
                ],
            "string"=>[
                "m"=>null,
                "val"=>[]]
            ];
            if($request->rentang){
                foreach($request->rentang as $r){

                }
            }
            $Tnow=Carbon::now();
            $data=[
                'name'=>$request->name,
                'kode_khusus'=>trim($request->kode_khusus),
                'definisi_konsep'=>$request->definisi,
                'tipe_data'=>$request->tipe_data,
                'satuan'=>$request->satuan,
                'arah_nilai'=>$request->arah_nilai,
                'fungsi_aggregasi'=>$request->fungsi_aggregasi,
                'fungsi_rentang_nilai'=>json_encode($rentang),
                'updated_at'=>$Tnow,
                'id_kategori'=>$request->id_kategori
            ];

            $in=DB::table('data')->where('id',$id)->update($data);

            if($in){
                Alert::success('','Entitas Data Berhasil Diubah!');
                return back();
            }else{
                Alert::info('Error','Server Tidak Menangapi');
                return back();
            }
        }

    }


    public function store($tahun,$id_sumber,Request $request){
        $valid=Validator::make($request->all(),[
            'name'=>'required|string',
            'kode_khusus'=>'nullable|string',
            'definisi'=>'nullable|string',
            'tipe_data'=>'required|string|in:integer,string,float',
            'satuan'=>'required|string',
            'arah_nilai'=>'required|boolean',
            'fungsi_aggregasi'=>'required|string',
        ]);

        if($valid->fails()){
            Alert::error('',($valid->messages()->all()[0]));
            return back()->withInput();
        }else{

            $rentang=[
                "number"=>[
                    "m"=>null,
                    "val"=>[]
                ],
            "string"=>[
                "m"=>null,
                "val"=>[]]
            ];
            if($request->rentang){
                foreach($request->rentang as $r){

                }
            }
            $Tnow=Carbon::now();
            $data=[
                'name'=>$request->name,
                'kode_khusus'=>trim($request->kode_khusus),
                'definisi_konsep'=>$request->definisi,
                'tipe_data'=>$request->tipe_data,
                'satuan'=>$request->satuan,
                'arah_nilai'=>$request->arah_nilai,
                'fungsi_aggregasi'=>$request->fungsi_aggregasi,
                'fungsi_rentang_nilai'=>json_encode($rentang),
                'created_at'=>$Tnow,
                'updated_at'=>$Tnow,
                'id_sumber_data'=>$id_sumber,
                'id_kategori'=>$request->id_kategori
            ];

            $in=DB::table('data')->insert($data);

            if($in){
                Alert::success('','Entitas Data Berhasil Ditambah!');

                return back();
            }else{
                Alert::info('Error','Server Tidak Menangapi');
                return back();
            }
        }
    }
}
