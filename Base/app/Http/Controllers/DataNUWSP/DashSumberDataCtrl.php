<?php

namespace App\Http\Controllers\DataNUWSP;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Validator;
Use Alert;

class DashSumberDataCtrl extends Controller
{
    //

    public function index($tahun){
        $data=DB::table('sumber_data as sd')
     
        ->selectRaw(
            "sd.*,
            (select count(*) from data where data.id_sumber_data=sd.id) as jumlah_jenis_data,
            (select count(*) from kategori_data where kategori_data.id_sumber_data=sd.id) as jumlah_kategori_data"
            )
        ->get();

       return view('dash.data-nuwsp.sumber-data.index')->with(['sumber_data'=>$data]);
    }

    public function view($tahun,$id){
        $data=DB::table('sumber_data')->where('id',$id)->first();
        if($data){
             $entitas=DB::table('data')->where(['id_sumber_data'=>$data->id])->get();

             return view('dash.data-nuwsp.sumber-data.detail')->with(['sumber_data'=>$data,'entitas'=>$entitas]);
        }

     }

    public function form($tahun){
        return view('dash.data-nuwsp.sumber-data.form');
    }

    public function store($tahun,Request $request){
        $valid=Validator::make($request->all(),[
            'name'=>'required|unique:sumber_data,name'
        ]);

        if($valid->fails()){
            return back()->with('errors', $valid->messages()->all()[0])->withInput();
        }else{
            $cr=DB::table('sumber_data')->insert([
                'name'=>$request->name,
                'keterangan'=>$request->keterangan
            ]);

            if($cr){
                return redirect()->route('dash.datanuwsp.sumberdata.index',['tahun'=>$tahun])
                ->withSuccess('Sumber Data Berhasil Ditambah!');
            }
        }
    }
}
