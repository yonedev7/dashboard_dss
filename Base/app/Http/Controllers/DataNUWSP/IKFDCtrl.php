<?php

namespace App\Http\Controllers\DataNUWSP;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
class IKFDCtrl extends Controller
{
    //
    static $page_meta=[
        'title'=>'INDEX KAPASITAS FISKAL DAERAH',
        'keterangan'=>'',
        'tahun'=>0,
        'tw'=>4
    ];

    static function filter($tahun,$filter,$basis,$where_extends=[]){
        $where=[['d.tahun','=',$tahun]];
        switch($filter){
            case "kota":
                $where=[['mp.stapem','=',2]];
            break;
            case "kab":
                $where=[['mp.stapem','=',3]];

            break;
            case "provinsi":
                $where=[['mp.stapem','=',1]];
            break;
        }
        switch(strtolower($basis)){
            case 'longlist':
            $where[]=['mp.tahun_proyek','>',0];

            break;
            case 'sortlist':
            $where[]=['mp.tahun_proyek','=',$tahun];
            break;
        }


        if(count($where_extends)){
            foreach($where_extends as $w){
                $where[]=$w;
            }
        }

        return $where;

    }

    public function load_rekap($tahun,Request $request){
        $data=DB::table('dataset.ikfd as d')
        ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
        ->selectRaw('d.kategori_ikfd,count(*) as jumlah_pemda,count(case when(mp.stapem<=1) then mp.kodepemda else null end) as pemda_pro,count(case when(mp.stapem>1) then mp.kodepemda else null end) as pemda_kota')
        ->groupBy('d.kategori_ikfd');
        $where=static::filter($tahun,$request->filter,$request->basis,[['d.status','=',1],['d.tw','=',4]]);
        if(count($where)){
            $data=$data->where($where);
        }
        $data=$data->get();

        $def=[  
            'jumlah_pemda_pro'=>0,
            'jumlah_pemda_kab'=>0,
            'SANGAT_TINGGI'=>0,
            'TINGGI'=>0,
            'SEDANG'=>0,
            'RENDAH'=>0,
            'SANGAT_RENDAH'=>0,
            'k1'=>0,
        ];

        foreach($data as $key=>$d){
            $def[strtoupper(str_replace(' ','_',$d->kategori_ikfd))]=(int)$d->jumlah_pemda;
            if($d->pemda_pro){
                $def['jumlah_pemda_pro']+=(int)$d->pemda_pro;
            }
            if($d->pemda_kota){
                $def['jumlah_pemda_kab']+=(int)$d->pemda_kota;
            }
        }
        return $def;
    }



    public function index($tahun,Request $request){
        $basis='nasional';
        if($request->basis){
            $basis=$request->basis;
        }
        $page_meta=static::$page_meta;
        $page_meta['tahun']=$tahun;
        $page_meta['tw']=4;
        $color=[
            '0%-25%'=>'#ff5757',
            '26%-50%'=>'#f2be32',
            '51%-75%'=>'#7ed957',
            '76%-100%'=>'#5271fe'
        ];

        return view('datadukung.ikfd.index')->with(['basis'=>$basis,'page_meta'=>$page_meta,'color'=>$color]);

    }

    public function load_provinsi($tahun,Request $request){
        $data=DB::table('dataset.ikfd as d')
        ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
        ->leftJoin('master.master_pemda as prov','prov.kodepemda','=',DB::raw("left(d.kodepemda,3)"))
        ->selectraw("count(distinct(mp.kodepemda)) as value, max(prov.nama_pemda) as name, max(prov.kodepemda) as kodepemda")
        ->groupBy(DB::raw("prov.kodepemda"));
    
        $where=static::filter($tahun,$request->filter,$request->basis,[['d.status','=',1],['d.tw','=',4]]);
        if(count($where)){
            $data=$data->where($where);
            
        }
        $data=$data->get();

        $where=array_filter($where,function($el){
            return (str_contains($el[0],'mp.'));
          
        });

        foreach($data as $key=>$d){
            $jumlah_pemda=DB::table('master.master_pemda as mp')
            ->where('mp.kodepemda','ilike',$d->kodepemda.'%');
            if(count($where)){
                $jumlah_pemda=$jumlah_pemda->where($where);
            }
            $jumlah_pemda=$jumlah_pemda->count();
            $data[$key]->jumlah_pemda=$jumlah_pemda;

            $data[$key]->persentase=($d->value)?($d->value/$jumlah_pemda)*100:0;
            if($data[$key]->persentase>0 AND $data[$key]->persentase<=25){
                $data[$key]->color='#ff5757';
            }else if( $data[$key]->persentase>25 AND  $data[$key]->persentase <= 50){
                $data[$key]->color='#f2be32';
            }else if( $data[$key]->persentase>50 AND  $data[$key]->persentase <= 75){
                $data[$key]->color='#7ed957';
            }
            else if( $data[$key]->persentase>75){
                $data[$key]->color='#5271fe';
            }

            $data[$key]->id=(int)$d->kodepemda;

        }

        return $data;

    }

    public function load_kota($tahun,Request $request){
        $data=DB::table('dataset.ikfd as d')
        ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
        ->selectraw("mp.kodepemda,mp.nama_pemda as name, d.kategori_ikfd,d.index_ikfd as value");
        $where=static::filter($tahun,$request->filter,$request->basis,[['d.status','=',1],['d.kodepemda','ilike',$request->kodepemda.'%']]);
        if(count($where)){
            $data=$data->where($where);
        }
        $data=$data->get();
        foreach($data as $key=>$d){
            $data[$key]->id=(int)$d->kodepemda;
            $data[$key]->color='#5271fe';

        }

        $color=[
            'SANGAT TINGGI'=>['text'=>'SANGAT TINGGI','color'=>'#3ec996'],
            'TINGGI'=>['text'=>'TINGGI','color'=>'#3d9970'],
            'SEDANG'=>['text'=>'SEDANG','color'=>'#f57e16'],
            'RENDAH'=>['text'=>'RENDAH','color'=>'#f8c10a'],
            'SANGAT RENDAH'=>['text'=>'SANGAT RENDAH','color'=>'#e3342f'],
        ];

        foreach($data as $key=>$d){
            $data[$key]->kategori_ikfd=strtoupper($d->kategori_ikfd);
            if(isset($color[$d->kategori_ikfd.''])){
                $data[$key]->color=$color[$d->kategori_ikfd.'']['color'];
                $data[$key]->text=$color[$d->kategori_ikfd.'']['text'];
            }else{
                $data[$key]->color='#ddd';
                $data[$key]->text='Tidak Tedefinisi';
            }
        }

        return ['data'=>$data,'colors'=>$color];
    }
}
