<?php

namespace App\Http\Controllers\DataNUWSP;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
class KinerjaBPKPCtrl extends Controller
{
    //

    static function filter($tahun,$filter,$basis,$where_extends=[]){
        $where=[['d.tahun','=',$tahun]];
        switch($filter){
            case "kota":
                $where[]=['mp.stapem','=',2];
            break;
            case "kab":
                $where[]=['mp.stapem','=',3];

            break;
            case "provinsi":
                $where[]=['mp.stapem','=',1];
            break;
        }
        switch(strtolower($basis)){
            case 'longlist':
            $where[]=['mp.tahun_proyek','>',0];

            break;
            case 'sortlist':
            $where[]=['mp.tahun_proyek','=',$tahun];
            break;
        }


        if(count($where_extends)){
            foreach($where_extends as $w){
                $where[]=$w;
            }
        }

        return $where;

    }

    static $color_kinerja=[
        '1'=>['text'=>'SEHAT','color'=>'#4bff70'],
        '2'=>['text'=>'KURANG SEHAT','color'=>'#f57e16'],
        '3'=>['text'=>'SAKIT','color'=>'#ff5757']
    ];

    static $color_fcr=[
        '100%'=>['text'=>'SUDAH FCR','color'=>'#4bff70'],
        '0-99%'=>['text'=>'BELUM FCR','color'=>'#ff5757']
    ];

    static $color_keterisian=[
        '0%-25%'=>'#ff5757',
        '26%-50%'=>'#f2be32',
        '51%-75%'=>'#7ed957',
        '76%-100%'=>'#5271fe'
    ];



    public function index($tahun,Request $request){
        $basis='nasional';
        if($request->basis){
            $basis=$request->basis;
        }

        if($request->meta){
            $index_page=explode('-',base64_decode($request->meta));
            $page_meta=config('data_tampilan.tematik')[$index_page[1]]['submenu'][$index_page[2]];
        }else{
            return abort(404);
        }

        return view('data.bpkp.index')->with([
            'basis'=>$basis,
            'color_keterisian'=>static::$color_keterisian,
            'color_kinerja'=>static::$color_kinerja,
            'color_fcr'=>static::$color_fcr,
            'page_meta'=>$page_meta
        ]);

    }

    public function load_rekap($tahun,Request $request){
        $data=DB::table('dataset.kinerja_bumdam_bpkp as d')
        ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
        ->selectRaw('d.kategori_penilaian_bpkp,count(*) as jumlah_pemda,count(case when(mp.stapem<=1) then mp.kodepemda else null end) as pemda_pro,count(case when(mp.stapem>1) then mp.kodepemda else null end) as pemda_kota')
        ->groupBy('d.kategori_penilaian_bpkp');
        $where=static::filter($tahun,$request->filter,$request->basis,[['d.status','=',1],['d.tw','=',4]]);
        if(count($where)){
            $data=$data->where($where);
        }
        $data=$data->get();

        $def=[  
            'jumlah_pemda_pro'=>0,
            'jumlah_pemda_kab'=>0,
            'K1'=>0,
            'K2'=>0,
            'K3'=>0,
            'K0'=>0,
        ];

        foreach($data as $key=>$d){
            $def[strtoupper('K'.$d->kategori_penilaian_bpkp)]=(int)$d->jumlah_pemda;
            if($d->pemda_pro){
                $def['jumlah_pemda_pro']+=(int)$d->pemda_pro;
            }
            if($d->pemda_kota){
                $def['jumlah_pemda_kab']+=(int)$d->pemda_kota;
            }
        }
        return $def;
    }


    public function load_provinsi($tahun,Request $request){
        $data=DB::table('dataset.kinerja_bumdam_bpkp as d')
        ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
        ->leftJoin('master.master_pemda as prov','prov.kodepemda','=',DB::raw("left(d.kodepemda,3)"))
        ->selectraw("count(distinct(mp.kodepemda)) as value, max(prov.nama_pemda) as name, max(prov.kodepemda) as kodepemda")
        ->groupBy(DB::raw("prov.kodepemda"));
    
        $where=static::filter($tahun,$request->filter,$request->basis,[['d.status','=',1],['d.tw','=',4]]);
        if(count($where)){
            $data=$data->where($where);
            
        }
        $data=$data->get();

        $where=array_filter($where,function($el){
            return (str_contains($el[0],'mp.'));
          
        });

        foreach($data as $key=>$d){
            $jumlah_pemda=DB::table('master.master_pemda as mp')
            ->where('mp.kodepemda','ilike',$d->kodepemda.'%');
            if(count($where)){
                $jumlah_pemda=$jumlah_pemda->where($where);
            }
            $jumlah_pemda=$jumlah_pemda->count();
            $data[$key]->jumlah_pemda=$jumlah_pemda;

            $data[$key]->persentase=($d->value)?($d->value/$jumlah_pemda)*100:0;
            if($data[$key]->persentase>0 AND $data[$key]->persentase<=25){
                $data[$key]->color=static::$color_keterisian['0%-25%'];
            }else if( $data[$key]->persentase>25 AND  $data[$key]->persentase <= 50){
                $data[$key]->color=static::$color_keterisian['26%-50%'];
            }else if( $data[$key]->persentase>50 AND  $data[$key]->persentase <= 75){
                $data[$key]->color=static::$color_keterisian['51%-75%'];
            }
            else if( $data[$key]->persentase>75){
                $data[$key]->color=static::$color_keterisian['76%-100%'];
            }

            $data[$key]->id=(int)$d->kodepemda;

        }

        return $data;

    }

    public function load_kota($tahun,Request $request){
        $data=DB::table('dataset.kinerja_bumdam_bpkp as d')
        ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
        ->selectraw("mp.kodepemda,mp.nama_pemda as name, d.kategori_penilaian_bpkp as value,d.nilai_kinerja_total,d.bobot_kinerja_keuangan,d.bobot_kinerja_pelayanan,d.bobot_kinerja_operasi,d.bobot_kinerja_sdm ");
        $where=static::filter($tahun,$request->filter,$request->basis,[['d.status','=',1],['d.kodepemda','ilike',$request->kodepemda.'%']]);
        if(count($where)){
            $data=$data->where($where);
        }

        $data=$data->get();
       

        $color=static::$color_kinerja;

        foreach($data as $key=>$d){
            $data[$key]->id=(int)$d->kodepemda;
            if(isset($color[$d->value.''])){
                $data[$key]->color=$color[$d->value.'']['color'];
                $data[$key]->text=$color[$d->value.'']['text'];
            }else{
                $data[$key]->color='#ddd';
                $data[$key]->text='Tidak Tedefinisi';
            }
        }

        return ['data'=>$data,'colors'=>$color];
    }

}
