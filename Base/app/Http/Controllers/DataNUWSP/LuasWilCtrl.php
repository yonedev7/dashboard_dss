<?php

namespace App\Http\Controllers\DataNUWSP;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
class LuasWilCtrl extends Controller
{
    //

    public function index($tahun,Request $request){
        if($request->meta){
            $index_page=explode('-',base64_decode($request->meta));

            $page_meta=config('data_tampilan.'.$index_page[0])[$index_page[1]]['submenu'][$index_page[2]];

        }else{
            return abort(404);
        }

        return view('data.bumdam.bisplan')->with([
            'basis'=>\_g_basis($request),
            'page_meta'=>$page_meta,
            'color_keterisisan'=>static::$color_keterisian
        ]);

    }

    static $color_keterisian=[
        '0%-25%'=>'#ff5757',
        '26%-50%'=>'#f2be32',
        '51%-75%'=>'#7ed957',
        '76%-100%'=>'#5271fe'
    ];



    public function load_provinsi($tahun,Request $request){
        $data=DB::table('dataset.bisnisplan_bumdam as d')
        ->join('master.master_pemda as mp','mp.kodepemda','=',DB::raw("left(d.kodepemda,3)"))
        ->selectRaw("mp.kodepemda,max(mp.nama_pemda) as name,sum(jsonb_array_length(doc_bisplan)) as jum_dokumen,count(distinct(d.kodepemda)) as value
        ")->groupBy('mp.kodepemda');
        $where=\_filter_l1($tahun,$request->filter,$request->basis,[['d.status','=',1],['d.tw','=',4]]);
        if(count($where)){
            $data=$data->where($where);
            
        }
        $data=$data->get();

        $where=array_filter($where,function($el){
            return (str_contains($el[0],'mp.'));
          
        });

        
        foreach($data as $key=>$d){
            $jumlah_pemda=DB::table('master.master_pemda as mp')
            ->where('mp.kodepemda','ilike',$d->kodepemda.'%');
            if(count($where)){
                $jumlah_pemda=$jumlah_pemda->where($where);
            }
            $jumlah_pemda=$jumlah_pemda->count();
            $data[$key]->jumlah_pemda=$jumlah_pemda;

            $data[$key]->persentase=($d->value)?($d->value/$jumlah_pemda)*100:0;
            if($data[$key]->persentase>0 AND $data[$key]->persentase<=25){
                $data[$key]->color=static::$color_keterisian['0%-25%'];
            }else if( $data[$key]->persentase>25 AND  $data[$key]->persentase <= 50){
                $data[$key]->color=static::$color_keterisian['26%-50%'];
            }else if( $data[$key]->persentase>50 AND  $data[$key]->persentase <= 75){
                $data[$key]->color=static::$color_keterisian['51%-75%'];
            }
            else if( $data[$key]->persentase>75){
                $data[$key]->color=static::$color_keterisian['76%-100%'];
            }

            $data[$key]->id=(int)$d->kodepemda;

        }

        return $data;  
    }


    public function load_kota($tahun,Request $request){
        $data=DB::table('dataset.bisnisplan_bumdam as d')
        ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
        ->selectRaw("mp.kodepemda,mp.nama_pemda as name, doc_bisplan as json_file,1 as value");
        $where=\_filter_l1($tahun,$request->filter,$request->basis,[['d.status','=',1],['d.tw','=',4],['d.kodepemda','ilike',$request->kodepemda.'%']]);
        if(count($where)){
            $data=$data->where($where);
            
        }
        $data=$data->get();

        foreach($data as $key=>$d){
            $data[$key]->json_file=$d->value?json_decode($d->json_file):[];
            $data[$key]->id=(int)$d->kodepemda;
            $data[$key]->color='#5271fe';
            $data[$key]->value='Miiliki Document Perencanaan';
            $data[$key]->y=0;
        }

        return ['data'=> $data,'column'=>[
            [
                'id'=>[5,2],
                'name'=>'Dokumen Rencana Bisnis',
                'field'=>'json_file',
                'type'=>'json',
                'type_handle'=>'link_download',
            ]

        ]];  
    }
}
