<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use DataTables;
class DatasetCtrl extends Controller
{
    //


    public function ajax_datax($tahun,$id_dataset,Request $request){
        
        $basis=$request->basis??'pemda';
        $dataset=DB::table('master.dataset')->where(['id'=>$id_dataset,'status'=>1])->first();
        if($dataset){
            

           if($basis=='pemda'){
            $kodepemda=$request->kodepemda??'0';
            $pemda=DB::table('master.master_pemda')->where('kodepemda',$kodepemda)->first();
            switch(strlen($kodepemda)){
                case 3:
                break;
                default:
                break;
                
            }

            return [
                'level_1'=>[
                    'builder'=>false,
                    'title'=>$pemda->nama_pemda,
                    'data_table'=>[],
                    'jumlah_terdata'=>0,
                    'jumlah_pemda'=>0,
                    'jumlah_terverifikasi'=>0,
                    'view'=>[],
                    'request_ajax'=>route('api-web.public.ajax_table',['tahun'=>$tahun,'id_dataset'=>$id_dataset,'basis'=>$basis,'kodepemda'=>$kodepemda])
                ],
                'level_2'=>[
                    'builder'=>false,
                    'title'=>NULL,
                    'jumlah_terdata'=>0,
                    'jumlah_pemda'=>0,
                    'jumlah_terverifikasi'=>0,
                    'data_table'=>[],
                    'view'=>[], 
                    'request_ajax'=>route('api-web.public.ajax_table',['tahun'=>$tahun,'id_dataset'=>$id_dataset,'basis'=>$basis,'kodepemda'=>'xxx','tw'=>'yyy'])
                ],
            ];
           }else{
            return [
                'level_1'=>[
                    'builder'=>false,
                    'title'=>'NASIONAL',
                    'jumlah_terdata'=>0,
                    'jumlah_pemda'=>0,
                    'jumlah_terverifikasi'=>0,
                    'data_table'=>[],
                    'view'=>[],
                    'request_ajax'=>route('api-web.public.ajax_table',['tahun'=>$tahun,'id_dataset'=>$id_dataset,'basis'=>$basis,'regional'=>null])
                ],
                'level_2'=>[
                    'builder'=>false,
                    'title'=>NULL,
                    'jumlah_terdata'=>0,
                    'jumlah_pemda'=>0,
                    'jumlah_terverifikasi'=>0,
                    'data_table'=>[],
                    'view'=>[], 
                    'request_ajax'=>route('api-web.public.ajax_table',['tahun'=>$tahun,'id_dataset'=>$id_dataset,'basis'=>$basis,'regonal'=>'xxx','tw'=>'yyy'])
                ],
            ];
           }
        }else{
            return [
                'title'=>'error',
                
            ];
        }

    }

    static function bindValue($header,$value,$level){
        $schema=$header->fungsi_rentang_nilai;
        $schema=json_decode($schema??'{}',true);
        if(isset($schema['tipe'])){
                switch($schema['tipe']){
                    case 'pilihan':
                        dd($schema);
                    break;
                    case 'min-max':
                        $value=number_format((float)$value,2,'.',',');
                    break;
                }
        }else{
            if(ctype_digit($value)){
                $value=number_format((float)$value,2,',','.');
            }else{
                switch($header->tipe_nilai){
                    case 'numeric':
                        $value=number_format((float)$value,2,',','.');
                    break;
                }
            }

        }

       
        return $value;
    }

    public function ajax_data($tahun,$id_dataset,Request $request){
        $dataset=DB::table('master.dataset')->find($id_dataset);
        $tw=$request->tw??4;
        $basis=$request->basis??'pemda';
        $regional=$request->regional??'';
        $pemda=$request->kodepemda_parent??'';
        $level=$request->level??1;
        $where=[];
        $where[]=['d.tahun','=',DB::raw($tahun)];
        $where[]=['d.tw','=',DB::raw($tw)];
        $join=[];
        $select=[];

        $login=Auth::guard('api')->check();
        $head= DB::table('master.dataset_data as dt')
        ->leftJoin('master.data as d','d.id','=','dt.id_data')
        ->where('id_dataset',$id_dataset);
        if(!$login){
           $head=$head->where('login',false);
        }
        $head=$head->selectRaw("d.*,dt.field_name as data,dt.tw_data,dt.tahun_data,(".$tahun." + (dt.tahun_data)) as tahun_record,dt.aggregasi,dt.title_aggregate");
        $head=$head->orderBy('index','asc')->get();
        
        $select=[];
        $groupby='';
        $header=[];
        foreach($head as $key=>$h){
            if($h->aggregasi){
                $hdef=['name'=>null,'data'=>null,'satuan'=>'','definisi_konsep'=>null,'tipe_nilai'=>null,'tw_data'=>0,'tahun_data'=>0,'cara_hitung'=>null];
                switch($h->aggregasi){
                    case 'SUM':
                      $hdef['query_select']="SUM(".$h->data.") as ".$h->data;
                    break;
                    case 'COUNT':
                        $hdef['query_select']="COUNT(".$h->data.") as ".$h->data;
                    break;
                    case 'AVERAGE':
                        $hdef['query_select']="AVERAGE(".$h->data.") as ".$h->data;
                    break;
                    case 'MIN':
                        $hdef['query_select']="MIN(".$h->data.") as ".$h->data;
                    break;
                    case 'MAX':
                        $hdef['query_select']="MAX(".$h->data.") as ".$h->data;
                    break;
                    case 'COUNT DISTINCT':
                        $hdef['query_select']="COUNT(DISTINCT(".$h->data.")) as ".$h->data;
                    break;
                }

                if(isset($hdef['query_select'])){
                    $h->fungsi_rentang_nilai=json_decode($h->fungsi_rentang_nilai);
                    $hdef['fungsi_rentang_nilai']=$h->fungsi_rentang_nilai;
                    $hdef['name']=$h->name;
                    $hdef['name_aggregate']=$h->title_aggregate;
                    $hdef['satuan']=$h->satuan;
                    $hdef['aggregasi']=$h->aggregasi;
                    $hdef['data']=$h->data;
                    $hdef['data_def']=$h->data.'_def';
                    $hdef['tw_data']=$h->tw_data;
                    $hdef['tahun_data']=$h->tahun_data;
                    $hdef['definisi_konsep']=$h->definisi_konsep;
                    $hdef['cara_hitung']=$h->cara_hitung;
                    $hdef['tipe_nilai']=$h->tipe_nilai;
                    $hdef['fungsi_aggregasi']=$h->fungsi_aggregasi;
                    $p=[];
                    if(isset($hdef['fungsi_rentang_nilai']->tipe)){
                        if(($hdef['fungsi_rentang_nilai']->tipe=='pilihan') and $hdef['aggregasi']=='COUNT'){
                            if($hdef['tipe_nilai']=='numeric'){
                                foreach($hdef['fungsi_rentang_nilai']->m as $m){
                                    $p[]=[
                                        'tag'=>$m->tag,
                                        'val'=>$m->val,
                                        'query_select'=>"COUNT(CASE WHEN (".$h->data."=".$m->val.") then 1 else null end)"
                                    ];
                                }
                            }else{
                                foreach($hdef['fungsi_rentang_nilai'] as $m){
                                    $p[]=[
                                        'tag'=>$m->tag,
                                        'val'=>$m->val,
                                        'query_select'=>"COUNT(CASE WHEN (".$h->data."='".$m->val."') then 1 else null end)"
                                    ];
                                }

                            }
                        }
                    }
                    if(count($p)==0){
                        $header[]=$hdef;
                    }

                    foreach($p as $key=>$pd){
                        $hdef['name_aggregate']=$h->title_aggregate.' - '.$pd['tag'];
                        $hdef['data']=$h->data.'_'.str_replace(' ','',$key);
                        $hdef['data_def']=$h->data.'_'.str_replace(' ','',$key).'_def';
                        $hdef['query_select']=$pd['query_select'].' as '.$hdef['data'];
                        $header[]=$hdef;
                    }
                    

                    
                }
            }
        }

        if($request->header){
            if($dataset->pusat){
            
                return $header;

            }else{
                if($request->level==1){
                return array_merge([['name'=>'kodepemda','data'=>'kodepemda','satuan'=>''],['name'=>'Nama Pemda','data'=>'nama_pemda','satuan'=>'']],$header,[['name'=>'Aksi','render'=>'function(a,s,item){return item.kodepemda}']]);

                }else{
                    return array_merge([['name'=>'kodepemda','data'=>'kodepemda','satuan'=>''],['name'=>'Nama Pemda','data'=>'nama_pemda','satuan'=>'']],$header);
                }
            }
        }


        
        switch($basis){
            case 'pemda':
                switch($level){
                    case 1:
                        $groupby='mp.kodepemda';
                        $where[]=['d.kodepemda','ilike',DB::raw("'".$pemda."%'")];
                        $join[]=['j'=>'master.master_pemda as mp','on'=>[['mp.kodepemda','=',DB::raw("left(d.kodepemda,3)")]]];
                        $select[]='mp.kodepemda';
                        $select[]='max(mp.nama_pemda) as nama_pemda';

                    break;
                    case 2:
                        $groupby='(d.kodepemda)';
                        $where[]=['d.kodepemda','ilike',DB::raw("'".$pemda."%'")];
                        $where[]=['d.kodepemda','in',['01402']];
                        $join[]=['j'=>'master.master_pemda as mp','on'=>[['mp.kodepemda','=','d.kodepemda']]];
                        $select[]='d.kodepemda';
                        $select[]='max(mp.nama_pemda) as nama_pemda';

                    break;       
                }
            
            break;
            case 'long_list':
            switch($level){
                case 1:
                    $groupby='mp.kodepemda';
                    $where[]=['d.kodepemda','ilike',DB::raw("'".$pemda."%'")];
                    $where[]=['d.kodepemda','in',['01402']];
                    $join[]=['j'=>'master.master_pemda as mp','on'=>[['mp.kodepemda','=',DB::raw("left(d.kodepemda,3)")]]];
                    $select[]='mp.kodepemda';
                    $select[]='max(mp.nama_pemda) as nama_pemda';


                break;
                case 2:
                    $groupby='mp.kodepemda';
                    $where[]=['mp.regional_1','ilike',DB::raw("'".$regional."%'")];
                    $join[]=['j'=>'master.master_pemda as mp','on'=>[['mp.kodepemda','=',DB::raw("left(d.kodepemda,3)")]]];
                    $select[]='mp.kodepemda';
                    $select[]='max(mp.nama_pemda) as nama_pemda';
                break;
                case 3:
                    $groupby='(d.kodepemda)';
                    $where[]=['d.kodepemda','ilike',$pemda.'%'];
                    $where[]=['d.kodepemda','ilike',DB::raw("'".$pemda."%'")];
                    $join[]=['j'=>'master.master_pemda as mp','on'=>[['mp.kodepemda','=','d.kodepemda']]];
                    $select[]='d.kodepemda';
                    $select[]='max(mp.nama_pemda) as nama_pemda';
            }

            break;
            case 'sort_list':
                switch($level){
                    case 1:
                        $groupby='mp.regional_1';
                    break;
                    case 2:
                        $groupby='left(d.kodepemda,3)';
                    break;
                    case 3:
                        $groupby='left(d.kodepemda)';
                    
                }
            break;
        }
        
        $select_data=[];
        $select_data=array_map(function($el){return $el['query_select'];},$header);

        $select=array_merge($select,$select_data);
        $data=DB::table('dataset.'.$dataset->table.' as d')->selectRaw(implode(',',$select))
            ->where($where);
        foreach($join as $j){
            $data=$data->leftJoin($j['j'],$j['on']);
        }

        $data=$data->groupBy($groupby)->orderBy($groupby,'asc');


        if($request->datatable){
            // return datatables()->of(DB::table(DB::raw('('.$data->toSql().') as x')))->toJson();
            return DB::table(DB::raw('('.$data->toSql().') as x'))->paginate(10);
        }else{
            return DB::table(DB::raw('('.$data->toSql().') as x'))->get();
        }

           


        

    }

    static function aggregate_select($aggregate,$h){
        switch($aggregate){
            case 'SUM':
                return "SUM(".$h->data.") as ".$h->data;
            break;
            case 'COUNT':
                return "SUM(".$h->data.") as ".$h->data;
            break;
            case 'AVERAGE':
                return "AVERAGE(".$h->data.") as ".$h->data;
            break;
            case 'MIN':
                return "MIN(".$h->data.") as ".$h->data;
            break;
            case 'MAX':
                return "MAX(".$h->data.") as ".$h->data;
            break;
            case 'COUNT DISTINCT':
                return "COUNT(DISTINCT(".$h->data.")) as ".$h->data;
            break;
        }

        return null;
    }

    public function ajax_table($tahun,$id_dataset,Request $request){
        $dataset=DB::table('master.dataset')->find($id_dataset);
        $tw=$request->tw??4;
        $login=Auth::check();
        if($request->header){
            $head= DB::table('master.dataset_data as dt')
            ->leftJoin('master.data as d','d.id','=','dt.id_data')
            ->where('id_dataset',$id_dataset);
            if(!$login){
               $head=$head->where('login',false);
            }
            $head=$head->selectRaw("d.*,dt.field_name as data")->orderBy('index','asc')->get(); 
            if(!$dataset->pusat){
                $head=array_merge([['data'=>'kodepemda','name'=>'Kodepemda'],['data'=>'nama_pemda','name'=>'Nama Pemda']],json_decode(json_encode($head)));
            }
            return $head;
        }

        if($request->rekap){
            if(!$dataset->pusat){
                return DB::table('dataset.'.$dataset->table.' as d')->where([
                    'tw'=>$tw,
                    'tahun'=>$tahun
                ])
                ->selectRaw("count(distinct(d.kodepemda)) as jumlah_data,
                count(distinct(case when status=1 then d.kodepemda else null end))  as terverifikasi")
                ->first();
            }else{
                return null;
            }
        }

        if($dataset){
            return datatables()->of(DB::table(DB::raw("(select mp.nama_pemda, d.* from dataset.".'"'.$dataset->table.'"'." as d left join master.master_pemda as mp on mp.kodepemda=d.kodepemda where status=1 and tahun=".$tahun." and tw=".$tw.") as x")))->toJson();

           if($dataset->pusat){
            //    return [
            //     "draw"=> 1,
            //     "recordsTotal"=> 57,
            //     "recordsFiltered"=> 57,
            //     "data"=> [
            //         [
            //             "data_2_plus0_4"=> 2,
            //             'q'=>$request->all()
                        
            //         ],
            //         [
            //             "data_2_plus0_4"=> 2
                        
            //         ],
                    
                       
                    
            //     ]
            //     ];
            return datatables()->of(DB::table(DB::raw("(select mp.nama_pemda, d.* from dataset.".'"'.$dataset->table.'"'." as d left join master.master_pemda as mp on mp.kodepemda=d.kodepemda where tahun=".$tahun." and tw=".$tw.") as x")))->toJson();

           }else{
            $basis=$request->basis??'pemda';
            if($basis=='pemda'){

            }else{
                
            }
           }
        }

    }


    public function ajax_chart($tahun,$id_dataset,Request $request){

    }

    public function index($tahun,$id_dataset,Request $request){
        $tw=$request->tw??4;
        if(in_array($request->basis??'pemda',['pemda','long_list','sort_list'])){
            $basis=$request->basis??'pemda';
            switch($basis){
                case 'pemda':
                    $pemda_count=514;
                    $nama_filter='BASIS PEMDA';
                break;
                case 'long_list':
                    $pemda_count=count(\_list_long());
                    $nama_filter='BASIS PROYEK - Long List';

                    
                break;
                case 'sort_list':
                    $pemda_count=count(\_list_sort($tahun));
                    $nama_filter='BASIS PROYEK - Sort List '.$tahun;

                break;
            }
        }


        $dataset=DB::table('master.dataset')->find($id_dataset);
        if($dataset){

            return view('dataset.index')->with(['tw'=>$tw,'dataset'=>$dataset,'basis'=>$basis,'nama_filter'=>$nama_filter,'pemda_count'=>$pemda_count]);
        

        }else{
            return abort(404);
        }
    }
    public function detail($tahun,$id_dataset){
        $dataset=DB::table('master.dataset')->find($id_dataset);
        if($dataset){
             return view('dataset.detail')->with(['id_dataset'=>$id_dataset,'dataset'=>$dataset]);

        }
    }

    public function get_dataset($tahun,$tw,$id_dataset,Request $request){

        $dataset=DB::table('master.dataset')->find($id_dataset);
        if($dataset){
        
            $dataset->list_data=[];
           array_map(function($el) use ($dataset){
                $dataset->list_data[$el->field_name]=$el;
                return 0;
            },json_decode(json_encode(DB::table('master.dataset_data as sd')
            ->selectRaw("
                sd.*,
                d.name,
                d.definisi_konsep,
                d.produsent_data,
                d.satuan,
                d.tipe_nilai,
                d.fungsi_rentang_nilai
            ")
            ->leftJoin('master.data as d','d.id','=','sd.id_data')
            ->where('sd.id_dataset',$id_dataset)->get())) );

            $dataset_data=DB::table('dataset.'.$dataset->table.' as dt')->selectRaw("dt.*,mp.nama_pemda")
            ->leftJoin('master.master_pemda as mp','mp.kodepemda','=','dt.kodepemda')
            ->where('dt.tahun',$tahun)->where('dt.tw',$tw)
            ->where('dt.kodepemda','ilike',$request->kodepemda.'%')
            ->where('dt.status',1)->get();
    
            $level=0;
            if(strlen($request->kodepemda)==1){
                $level=0;
            }else if(strlen($request->kodepemda)==3){
                $level=1;
            }
            else if(strlen($request->kodepemda)==5){
                $level=2;
            }
    
            $dataset_display=DB::table('master.dataset_display')->where([
                'id_dataset'=>$id_dataset,
                'tingkat'=>$level.''
            ])->first();
            if($dataset_display){
                $dataset_display=(array)($dataset_display);
                
                for($d=1;$d<5;$d++){
                    if($dataset_display['short_query_'.$d]){
                        $dataset_display['short_query_'.$d]=[
                            'html'=>null,
                            'schema'=>base64_encode($dataset_display['short_query_'.$d])
                        ];
                    }
                }
            }
    
            $data=[
                'tahun'=>$tahun,
                'tw'=>$tw,
                'meta'=>$dataset,
                'dataset'=>$dataset_data,
                'display'=> $dataset_display
    
            ];
    
            return $data;
        }
       
    }


    public function build_widget($tahun,$tw,Request $request){
        if($request->schema){
            $request['schema']=json_decode(base64_decode($request->schema));
            $widget=[];
            $widget['dataset']=[];
            $widget['series']=$request->schema->series;
            try{
                $query=$request->schema->sort_query;
                $query=str_replace('[TAHUN]',$tahun,$query);
                $query=str_replace('[TW]',$tw,$query);
                $data=DB::select($query);
                $widget['dataset']=$data;


            }catch(\Exception $e){
                return view('widget.error')->with('judul',$request['schema']->judul);
            }
                switch($request->schema->style){
                case 'CARD INFORMASI';
                    return view('widget.card_informasi')->with(['widget'=>$widget])->render();
                    
            
                break;
            }

        }


    }
}
