<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Models\User;
use Str;
class FilterCtrl extends Controller
{
    //

    static $def_permission=[
        'dts.[ID_DATASET]:view-data',
        'dts.[ID_DATASET]:edit-data',
        'dts.[ID_DATASET]:verified-data',
        'dts.[ID_DATASET]:validate-data'
    ];

    public function buildFieldSql($col,$type){
        $login=Auth::guard('web')->User()??Auth::guard('api')->User();
        $raw=false;
        $title='';
        if($col['login']){
            if(!$login){
                return false;
            }
        }

        if($type==1){
            // nasional priority
            if($col['priority']){
                if($col['aggregate_pusat']){
                        $title=$col['aggregate_pusat_title']??$col['name'];

                    if(str_contains('RAW:',$col['aggregate_pusat'])){
                        $bind='('.str_replace('@val',$col['field_name']).')';
                    }else{
                        $bind=$col['aggregate_pusat'].'('.str_replace('@val',$col['field_name']).')';
                    }

                    $raw=[$bind.' as '.$col['field_name'],"'".$col['aggregate_pusat_satuan']."' as ".$col['field_name']."_satuan",$col['aggregate_pusat_tipe_nilai']."' as ".$col['field_name']."_tipe_nilai"];

                }
            }else{
                return false;
            }

        }else if($type==2){
            if($col['priority']){
                    
                if($col['aggregate_pemda']){
                $title=$col['aggregate_pemda_title']??$col['name'];

                    if(str_contains('RAW:',$col['aggregate_pemda'])){
                        $bind='('.str_replace('@val',$col['field_name']).')';
                    }else{
                        $bind=$col['aggregate_pemda'].'('.str_replace('@val',$col['field_name']).')';
                    }
                    $raw=[$bind.' as '.$col['field_name'],"'".$col['aggregate_pemda_satuan']."' as ".$col['field_name']."_satuan",$col['aggregate_pemda_tipe_nilai']."' as ".$col['field_name']."_tipe_nilai"];
                }   

            }else{
                return false;
            }
            // pemda priority

        }else if($type==3){

            if($col['column_dash_pemda']){
                if($col['aggregate']){
                    $title=$col['aggregate_title']??$col['name'];

                    if(str_contains('RAW:',$col['aggregate'])){
                        $bind='('.str_replace('@val',$col['field_name']).')';
                    }else{
                        $bind=$col['aggregate'].'('.str_replace('@val',$col['field_name']).')';
                    }

                    $raw=[$bind.' as '.$col['field_name'],"'".$col['aggregate_satuan']."' as ".$col['field_name']."_satuan",$col['aggregate_tipe_nilai']."' as ".$col['field_name']."_tipe_nilai"];
                    
                }   

            }else{
                return false;
            }
            
            // table priority

        }

        return [$title,$raw];

    }

    public static function getPemissionsDataset(Request $request,$id_dataset=null){
        $My=Auth::guard('web')->id()??Auth::guard('api')->id();
        if($My){
            $My=User::find($My);
        }
        $permissions=DB::table('public.permissions as p');
        if($id_dataset){
        
           
           if(!$My->can('is_admin')){
                $pemit_model=DB::table('public.model_has_permissions')->where([
                    'model_type'=>'App\Models\User',
                    'model_id'=>DB::raw($My->id)
                ])->selectRaw('permission_id');

                $permis=DB::table('public.role_has_permissions as p')
                ->join('public.roles as r','r.id','=','p.role_id')
                ->join('public.model_has_roles as hr',[['hr.role_id','=','r.id'],['hr.model_type','=',DB::raw("'App\Models\User'")]])
                ->where('hr.model_id',$My->id)
                ->selectRaw('permission_id')
                ->union($pemit_model)->get()->pluck('permission_id')->toArray();
                
                $permissions=$permissions->whereIn('id',$permis)->where('name','ilike','dts.'.$id_dataset.':%')
                ->get()->pluck('name')->toArray();
           }else{
                $permissions=static::$def_permission;
                foreach($permissions as $k=>$p){
                    $permissions[$k]=str_replace('[ID_DATASET]',$id_dataset,$p);
                }
              
               
               
           }
           
        }else{
           if(!$My->can('is_admin')){
                $pemit_model=DB::table('public.model_has_permissions')->where([
                    'model_type'=>'App\Models\User',
                    'model_id'=>DB::raw($My->id)
                ])->selectRaw('permission_id');

                $permis=DB::table('public.role_has_permissions as p')
                ->join('public.roles as r','r.id','=','p.role_id')
                ->join('public.model_has_roles as hr',[['hr.role_id','=','r.id'],['hr.model_type','=',DB::raw("'App\Models\User'")]])
                ->where('hr.model_id',$My->id)
                ->selectRaw('p.permission_id')
                ->union($pemit_model)
                ->get()->pluck('permission_id')->toArray();
                
                $permissions=$permissions->whereIn('id',$permis)
                ->get()->pluck('name')->toArray();

           }else{

            $datasets=DB::table('master.dataset')->orderBy('index','asc')->get();
            $permissions_def=static::$def_permission;
            $permissions=[];
            foreach($datasets as $d){
                foreach($permissions_def as $k=>$p){
                    $permissions[]=str_replace('[ID_DATASET]',$d->id,$p);
                }
            }

           }
        }


        $non_regional=DB::table('public.roles as r')
        ->join('model_has_roles as mr','mr.role_id','=','r.id')
        ->selectRaw('r.name')
        ->where(
            [
                'mr.model_type'=>'App\Models\User',
                'mr.model_id'=>$My->id
            ]
        )->whereIn('r.name',['TACT-REGIONAL','PEMDA','BUMDAM'])->get()->pluck('name')->toArray();
       

        $permit=array_map(function($el){
            return explode(':',$el);
            
        },$permissions);

        $id_dataset=array_unique(array_map(function($el){
            return explode('.',$el[0])[1];
        },$permit));

        return [
            'permissions'=>$permissions,
            'dataset_id'=>array_values($id_dataset),
            'role_reg'=>$non_regional
        ];
    } 
    public static function buildFilterPemda(Request $request,$onlyNuwsp=false){
        $My=Auth::guard('web')->User()??Auth::guard('api')->User();
        if($My){
            $My=User::find($My->id);
        }
        
        $filter=[
            'regional'=>$request->filter_regional,
            'lokus'=>$request->filter_lokus,
            'sortlist'=>$onlyNuwsp?true:($request->filter_sortlist),
            'tipe_bantuan'=>$request->filter_tipe_bantuan,
            'jenis_pemda'=>$request->filter_jenis_pemda??3,
            'pemda'=>['=',null],
            'direct_pemda'=>false,
            'kodebu'=>[]
        ];

        $direct_pemda=false;
        $non_regional=[];
        if($My){
                if($My->lokus_basis){
                    $filter['lokus']=true;
                }

                $non_regional=DB::table('public.roles as r')
                ->join('model_has_roles as mr','mr.role_id','=','r.id')
                ->select('r.name')
                ->where(
                    [
                        'mr.model_type'=>'App\Models\User',
                        'mr.model_id'=>$My->id
                    ]
                )->whereIn('r.name',['TACT-REGIONAL','PEMDA','BUMDAM'])->get()->pluck('name')->toArray();
        }
        



        if(count($non_regional)){
            $filter['lokus']=true;
            if(in_array('TACT-REGIONAL',$non_regional)){
                if($My->regional){
                    $filter['regional']=$My->regional;
                }
            }else if(in_array('PEMDA',$non_regional)){
                if($My->kodepemda){
                    $filter['pemda']=['=',$My->kodepemda??'X'];
                    $filter['direct_pemda']=true;
    
                }

            }else if(in_array('BUMDAM',$non_regional)){
                if($My->kodebumdam){
                    $filter['pemda']=['=',$My->kodepemda??'X'];
                    $filter['direct_pemda']=true;
                    $filter['kodebu']=[$My->kodebumdam??0];
                }

            }

          
           
        }

        return $filter;
    }

    public static function filter_pemda($req=null,$lokus=false,$sortlist=null,$tipebantuan=null,$tipe_pemda=3,$kodepemda=['=',null]){


        $pemda=  DB::table('master.master_pemda as mp')->select('mp.kodepemda');
        if(($lokus OR $sortlist) OR $tipebantuan){
            $pemda=$pemda->join('master.pemda_lokus as lk','lk.kodepemda','=','mp.kodepemda');
        }
        if($tipe_pemda==2){
            $pemda=$pemda->where(DB::raw("length(mp.kodepemda)"),5);
        }

        if($tipe_pemda==1){
            $pemda=$pemda->where(DB::raw("length(mp.kodepemda)"),3);
        }

        if($tipe_pemda==3){
            $pemda=$pemda->where(DB::raw("length(mp.kodepemda)"),'>',1);
        }

        if($tipe_pemda==0){
            $pemda=$pemda->where(DB::raw("length(mp.kodepemda)"),1);
        }

        if($kodepemda[1]){
            $pemda=$pemda->where("mp.kodepemda",$kodepemda[0],$kodepemda[1]);
        }

        if($tipebantuan){
            if(is_array($tipebantuan)){

                $pemda=$pemda->whereIn('lk.tipe_bantuan',$tipebantuan);

            }else{
                $pemda=$pemda->where('lk.tipe_bantuan',$tipebantuan);
            }
        }
        if($sortlist){
            $pemda=$pemda->where('lk.tahun_proyek',$sortlist);
         }

        if($req){
           $pemda=$pemda->where('mp.regional_1',$req);
        }


        return array_values($pemda->groupBy('mp.kodepemda')->get()->pluck('kodepemda')->toArray());

    }

    
}
