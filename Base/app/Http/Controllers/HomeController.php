<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function rubah_tahun($tahun,Request $request){
        $url=base64_decode($request->pref);

        
        $url=str_replace('session/{tahun}','session/'.$request->tahun2,$url);
        $url=str_replace('dash/{tahun}','dash/'.$request->tahun2,$url);

        $url=preg_replace('/({\w+.?})/', '', $url);
        $url=preg_replace('/({\w+})/', '', $url);

        
        return redirect(url($url));

    }

    public function rubah_tahun_lokus($tahun,Request $request){
        session_start();
        $url=base64_decode($request->pref);
        
        $url=str_replace('session/{tahun}','session/'.$tahun,$url);
        $url=str_replace('dash/{tahun}','dash/'.$tahun,$url);
        $url=preg_replace('/({\w+.?})/', '', $url);
        $url=preg_replace('/({\w+})/', '', $url);

        session(['tahun_data'=>(int)$request->tahun2]);
        $_SESSION['tahun_data']=(int)$request->tahun2;

        $session_id=session()->getId(); 
        DB::table('sessions')->where('id',$session_id)->update(['access_tahun'=>$request->tahun2]);

        return redirect(url($url));


    }

    static function filter($tahun,$filter,$basis,$where_extends=[]){
        $where=[['d.tahun','=',$tahun]];
        switch($filter){
            case "kota":
                $where=[['mp.stapem','=',2]];
            break;
            case "kab":
                $where=[['mp.stapem','=',3]];

            break;
            case "provinsi":
                $where=[['mp.stapem','=',1]];
            break;
        }
        switch(strtolower($basis)){
            case 'longlist':
            $where[]=['mp.tahun_proyek','>',0];

            break;
            case 'sortlist':
            $where[]=['mp.tahun_proyek','=',$tahun];
            break;
        }


        if(count($where_extends)){
            foreach($where_extends as $w){
                $where[]=$w;
            }
        }

        return $where;

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function load_rekap_meta($tahun){
       



    }

    public function load_tematik($tahun,Request $request){
        $basis='nasional';
        if($request->basis){
            $basis=$request->basis;
        }

        if($request->meta){
            $index_page=explode('-',base64_decode($request->meta));
            $page_meta=config('data_tampilan.'.$index_page[0])[$index_page[1]];

        }else{
            return abort(404);
        }

        if(isset($page_meta['submenu'])){

        foreach($page_meta['submenu'] as $key=>$p){
            $page_meta['submenu'][$key]['link_render']=route($p['link'],[
                'tahun'=>'xxx','meta'=>base64_encode('tematik-'.$index_page[1].'-'.$key)
            ]);
            $page_meta['submenu'][$key]['load']=false;
            $page_meta['submenu'][$key]['rekap']=[
                'nasional'=>['pemda_pro'=>0,'pemda_kabkot'=>0],
                'longlist'=>['pemda_pro'=>0,'pemda_kabkot'=>0],
                'sortlist'=>['pemda_pro'=>0,'pemda_kabkot'=>0],
                'tahun'=>0
            ];
        }
        }else{
            $page_meta['submenu']=[];

        }


        return view('tematik.load')->with(['page_meta'=>$page_meta,'temaindex'=>$index_page[1]]);
        
    }

    public function load_pendukung($tahun,Request $request){
        $basis='nasional';
        if($request->basis){
            $basis=$request->basis;
        }

        if($request->meta){
            $index_page=explode('-',base64_decode($request->meta));
            $page_meta=config('data_tampilan.'.$index_page[0])[$index_page[1]];

        }else{
            return abort(404);
        }


        foreach($page_meta['submenu'] as $key=>$p){
            $page_meta['submenu'][$key]['link_render']=route($p['link'],[
                'tahun'=>'xxx','meta'=>base64_encode('pendukung-'.$index_page[1].'-'.$key)
            ]);
            $page_meta['submenu'][$key]['load']=false;
            $page_meta['submenu'][$key]['rekap']=[
                'nasional'=>['pemda_pro'=>0,'pemda_kabkot'=>0],
                'longlist'=>['pemda_pro'=>0,'pemda_kabkot'=>0],
                'sortlist'=>['pemda_pro'=>0,'pemda_kabkot'=>0],
                'tahun'=>0
            ];
        }


        return view('tematik.load_pendukung')->with(['page_meta'=>$page_meta,'temaindex'=>$index_page[1]]);
        
    }

    public function load_rekap($tahun,Request $request){
        $tahun_now=((int)$tahun+1);
        $exist=$tahun;
        if(!str_contains($request->table,'dataset.')){
            return null;
        }
        
        $count=0;
        if(str_contains($request->table,'[TAHUN]')){
            do{
                $tahun_now-=1;
                $tb=str_replace('[TAHUN]',$tahun_now,$request->table);
                $tb=explode('.',$tb);
                $tbex=(int)DB::table('pg_tables')->where('tablename','=',$tb[1])->where('schemaname','=',$tb[0])->count();
                if($tbex){
                    $count=DB::table(str_replace('[TAHUN]',$tahun_now,$request->table).' as d')
                    ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
                    ->where('d.status','=',1)
                    ->where('d.tw','=',4)
                    ->where('d.tahun','=',$tahun_now)
                    ->count();
                }else{
                    $count=0;
                }
                
            }while($count==0 AND ($tahun_now>=(((int)date('Y'))-5)));

            if($count==0){
                $request->table=str_replace('[TAHUN]',$tahun,$request->table);
                $exist=$tahun;
            }else{
                $request->table=str_replace('[TAHUN]',$tahun_now,$request->table);
                $exist=$tahun_now;

            }
        }else{
            do{
                $tahun_now-=1;
                $count=DB::table($request->table.' as d' )
                ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
                ->where('d.status','=',1)
                ->where('d.tw','=',4)
                ->where('d.tahun','=',$tahun_now)
                ->count();
               
                
            }while($count==0 AND ($tahun_now>=(((int)date('Y'))-5)));

            if($count==0){
                $exist=$tahun;
            }else{
                $exist=$tahun_now;
            }


        }

        if($count){
            $all=DB::table($request->table.' as d')
            ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
            ->where('d.status','=',1)
            ->where('d.tw','=',4)
            ->selectRaw("count(distinct(case when length(mp.kodepemda)=3 then mp.kodepemda else null end )) as pemda_pro,count(distinct(case when length(mp.kodepemda)>3 then mp.kodepemda else null end )) as pemda_kabkot");
            $whereall=\_filter_l1($exist,null,'nasional');
            if(count($whereall)){
                $all=$all->where($whereall);
            }
        
            $long=DB::table($request->table.' as d')
            ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
            ->where('d.status','=',1)
            ->where('d.tw','=',4)
            ->selectRaw("count(distinct(case when length(mp.kodepemda)=3 then mp.kodepemda else null end )) as pemda_pro,count(distinct(case when length(mp.kodepemda)>3 then mp.kodepemda else null end )) as pemda_kabkot");
            $wherelong=\_filter_l1($exist,null,'longlist');
            if(count($wherelong)){
                $long=$long->where($wherelong);
            }
    
            $sort=DB::table($request->table.' as d')
            ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
            ->where('d.status','=',1)
            ->where('d.tw','=',4)
            ->selectRaw("count(distinct(case when length(mp.kodepemda)=3 then mp.kodepemda else null end )) as pemda_pro,count(distinct(case when length(mp.kodepemda)>3 then mp.kodepemda else null end )) as pemda_kabkot");
            $wheresort=\_filter_l1($exist,null,'sortlist');
            if(count($wheresort)){
                $sort=$sort->where($wheresort);
            }

    
            return [
                'nasional'=>$all->first(),
                'longlist'=>$long->first(),
                'sortlist'=>$sort->first(),
                'tahun'=>$exist
    
    
            ];
        }else{

            return [
                'nasional'=>['pemda_pro'=>0,'pemda_kabkot'=>0],
                'longlist'=>['pemda_pro'=>0,'pemda_kabkot'=>0],
                'sortlist'=>['pemda_pro'=>0,'pemda_kabkot'=>0],
                'tahun'=>$exist
    
            ];

        }
       
    }

    public function index($tahun,Request $request)
    {

        $tema=config('data_tampilan.tematik');


        $pendukung=config('data_tampilan.pendukung');

        foreach($tema as $k=>$t){
            foreach($t['submenu'] as $ks=>$s){
                $tema[$k]['submenu'][$ks]['link_render']=route($s['link'],['tahun'=>'xxx']);
                $tema[$k]['submenu'][$ks]['rekap']=[
                    'tahun'=>$tahun,
                    'load'=>false,
                    'nasional'=>[
                        'pemda_pro'=>0,
                        'pemda_kabkot'=>0
                    ],
                    'sortlist'=>[
                        'pemda_pro'=>0,
                        'pemda_kabkot'=>0
                    ],
                    'longlist'=>[
                        'pemda_pro'=>0,
                        'pemda_kabkot'=>0
                    ],
                    
                ];
            }
        }
        foreach($pendukung as $k=>$t){
            foreach($t['submenu'] as $ks=>$s){
                $pendukung[$k]['submenu'][$ks]['link_render']=route($s['link'],['tahun'=>'xxx']);
                $pendukung[$k]['submenu'][$ks]['rekap']=[
                    'tahun'=>$tahun,
                    'load'=>false,
                    'nasional'=>[
                        'pemda_pro'=>0,
                        'pemda_kabkot'=>0
                    ],
                    'sortlist'=>[
                        'pemda_pro'=>0,
                        'pemda_kabkot'=>0
                    ],
                    'longlist'=>[
                        'pemda_pro'=>0,
                        'pemda_kabkot'=>0
                    ],
                    
                ];
            }
        }

        $dd=DB::table('master.dataset as  dts')
        ->join('master.menus as m','m.id','=','dts.id_menu')
        ->selectRaw('dts.*')
        ->where(['m.type'=>1])
        ->orderBy('m.id','asc')
        ->get();

        $dataset=[];
        foreach($dd as $key=>$d){
            $dataset[]=[
                'id_dataset'=>$d->id,
                'dataset'=>$d,
                'chart'=>[
                    'xAxis'=>[
                        "type"=>'category'
                    ],
                    'plotOptions'=>[
                        'area'=>[
                            "dataLabels"=>[
                                "enabled"=>true
                            ]
                        ]
                    ], 
                    "tooltip"=>[ 
                        "pointFormat"=>'{point.y:,0f} <br>Total: {point.jumlah_pemda:,0f} PEMDA'
                    ],
                    'title'=>[
                        'text'=>$d->name,
                    ],
                    'subtitle'=>[
                        'text'=>'',
                        'enabled'=>true
                    ],
                    'chart'=>[
                        'type'=>'area'
                    ],
                    'series'=>[]
                ]
            ];
        }

        if($request->test){
        return view('dashboard_s')->with(['tema_meta'=>$tema,'pendukung_meta'=>$pendukung,'req'=>$request,'datasets'=>$dataset]);

        }
        return view('dashboard')->with(['tema_meta'=>$tema,'pendukung_meta'=>$pendukung,'req'=>$request,'datasets'=>$dataset]);
        return view('pages.dashboard.index-1');
        return view('pages.dashboard.index');

    }
}
