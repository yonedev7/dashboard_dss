<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ProfileDaerahCtrl extends Controller
{
    static $sql=" select mp.kodepemda, 1 as status, xxxtahunxxx as tahun, 4 as tw, mp.nama_pemda, mp.tahun_proyek, mp.tipe_bantuan, ikfd.tahun as tahun_ikfd, ikfd.index_ikfd, ikfd.kategori_ikfd, tp.tipologi as tp_pemda, tp.opd_pelaksana as tp_opd, tp.tahun as tahun_tp from dataset.profile_pemda as pp left join master.master_pemda mp on mp.kodepemda=pp.kodepemda left join (select max(d1.kodepemda) as kodepemda, max(d1.tahun) as tahun, max(d1.index_ikfd) as index_ikfd, max(d1.kategori_ikfd) as kategori_ikfd from dataset.ikfd as d1 where d1.tahun<=xxxtahunxxx and d1.status=1 group by d1.kodepemda order by max(d1.tahun) desc ) as ikfd on ikfd.kodepemda =mp.kodepemda left join (select max(d2.kodepemda) as kodepemda, max(d2.tahun) as tahun, max(d2.tipologi) as tipologi, max(d2.opd_pelaksana) as opd_pelaksana from dataset.tipologi_pemda as d2 where d2.tahun<=xxxtahunxxx and d2.status=1  group by d2.kodepemda order by max(d2.tahun) desc ) as tp on tp.kodepemda=mp.kodepemda where pp.status =1 and pp.tw=4 and pp.tahun=xxxtahunxxx ";
    public function rekap($tahun,Request $request){

        $data=DB::table(DB::raw( replace(static::$sql,'xxxtahunxxx',$tahun)." as d "));
        if($request->tipe_bantuan){

        }

        if($request->basis){
            switch(strtoupper($request->basis)){
                case 'NASIONAL':

                break;
                case 'SORTLIST':
                    
                break;
                case 'LONGLIST':

                break;
            }
        }

        $data=$data->get();
    }

    public function get_data($tahun,Request $request){

    }

    public function index($tahun,Request $request){
        $basis='nasional';
        if($request->basis){
            $basis=$request->basis;
        }

        // if($request->meta){
        //     $index_page=explode('-',base64_decode($request->meta));

        //     $page_meta=config('data_tampilan.'.$index_page[0])[$index_page[1]];


        // }else{
        //     return abort(404);
        // }


        $data=DB::table(DB::raw('('.str_replace('xxxtahunxxx',$tahun,static::$sql).") as d "));

        if($request->tipe_bantuan){

        }

        if($request->basis){
            switch(strtoupper($request->basis)){
                case 'NASIONAL':
                    
                break;
                case 'SORTLIST':
                    $data=$data->where('d.tahun_proyek',$tahun);
                break;
                case 'LONGLIST':
                    $data=$data->where('d.tahun_proyek','>=',1);
                break;
            }
        }

        $data=$data->get();


        return view('profile-daerah.index')->with([
            'basis'=>$basis,
            'color_keterisian'=>[],
            'page_meta'=>['title'=>'a','keterangan'=>'ss'],
            'data'=>$data
           
        ]);
    
    }

    public function profile($tahun,$kodepemda){

      
        $pemda=DB::table('master.master_pemda')->where('kodepemda',$kodepemda)->first();
        $profile=DB::table('dataset.profile_pemda')->where([
            ['tahun','=',$tahun],
            ['tw','=',4],
            ['status','=',1],
            ['kodepemda','=',$kodepemda]
        ])->first();
        if($pemda and $profile){
            // dd($profile);
            return view('profile-daerah.detail')->with(['pemda'=>$pemda,'meta_profile'=>$profile]);
        }
    }


    static function buildParamater($params,$tahun,$pemda){
        $list=[
            '[dss_param_example]'=>view('profile.pemda_spm')->with(['tahun',$tahun,'pemda'=>$pemda,'pageId'=>"dss_example".rand(0,199) ])->render()
        ];
        $params_db=DB::table('dataset.param_profile')->whereIn('id',$params)->get();
        foreach($params_db as $k){
            switch($k->type){
                case 1:
                    $data=DB::table(str_replace('[tahun]',$tahun,$k->table))
                    ->where('kodepemda',$pemda->kodepemda)
                    ->selectRaw($k->field.' as data')
                    ->where('status',1);
                    if($k->last){
                        $data=$data->where('tahun','<=',$tahun);
                    }else{
                        $data=$data->where('tahun','=',$tahun);
                    }

                    $data=$data->first();
                    if($data){
                        $list['[dss_param_'.$k->id.']']=$data->data;
                    }else{
                        $list['[dss_param_'.$k->id.']']='';
                    }
                break;
                case 2:
                    $data=view($k->field)->with(['tahun',$tahun,'pemda'=>$pemda,'pageId'=>"dss_".$k->id.rand(0,199)])->render();
                    $list['[dss_param_'.$k->id.']']=$data;
                break;
            }
        }

        foreach($params as $k){
            if(!isset($list['[dss_param_'.$k.']'])){
                $list['[dss_param_'.$k.']']='';
            }
        }

        return $list;
    }

    public function render_content($tahun,Request $request){
       $re=[
        'content'=>$request->content,
        'list_param'=>[],
        'tahun'=>$tahun,
        'kodepemda'=>$request->kodepemda,
        'pemda'=>DB::table('master.master_pemda')->where('kodepemda',$request->kodepemda)->first()
       ];

       if($re['pemda']){
            preg_match_all('/\[(dss_param_\d+)\]/i',  $re['content'], $param_list);
            $param_list=array_map(function($el){
                return (int)str_replace('dss_param_','',$el);
            },array_unique($param_list[1]));
            $re['list_param']=$param_list;
            $params=static::buildParamater($re['list_param'],$re['tahun'],$re['pemda']);
            foreach($params as $k=>$v){
                $re['content']=str_replace($k,$v,$re['content']);
            }

       }else{
            $re['content']='';
       }
      
       return ($re);


    }
}
