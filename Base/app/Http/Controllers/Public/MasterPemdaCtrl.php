<?php

namespace App\Http\Controllers\Public;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
class MasterPemdaCtrl extends Controller
{

    function get_pemda(Request $request){
    
        $data=DB::table('master.master_pemda')->selectRaw('kodepemda,kodepemda_2,nama_pemda');
        if($request->q){
            $data=$data->where('nama_pemda','ilike','%'.$request->q.'%');
        }
        if($request->limit){
           $data=$data->limit($request->limit);
        }else{
            $data=$data->limit(10);
        }
        return [
            'data'=>$data->get()
        ];
    }
}
