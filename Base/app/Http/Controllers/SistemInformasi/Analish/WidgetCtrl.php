<?php

namespace App\Http\Controllers\SistemInformasi\Analish;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Storage;
use Carbon\Carbon;
use Auth;
use Alert;

class WidgetCtrl extends Controller{

    public function dashbord($tahun){
        $data_chart=DB::table('master.dashboard_user_chart')->where('id_user',Auth::User()->id)
        ->orderBy('updated_at','desc')->get();
        return view('sistem_informasi.analisa.dashbord')->with(['data_chart'=>$data_chart]);
    }
    public function save_dashbord($tahun,$kode_analisa,Request $request){
        $p=base64_decode($kode_analisa);
        $chart=json_decode($request->chart);
        $saver=json_decode(file_get_contents(storage_path('app/'.$p.'.json')),true);
        $saver['judul']=$request->judul;
        $saver['chart_schema']= $chart;
        $user=(Auth::guard('api')->User());
        $date=Carbon::now();
        Storage::put($p.'.json',json_encode($saver));
        $k=DB::table('master.dashboard_user_chart')->updateOrInsert([
            'kode'=>$kode_analisa,
            'id_user'=>$user->id
        ],[
            'title'=>$saver['judul'],
            'kode'=>$kode_analisa,
            'id_user'=>$user->id,
            'updated_at'=>$date
        ]);
        if($k){
            return $date;
        }



    }

    public function draw($tahun,$kode_analisa,Request $request){
        $p=base64_decode($kode_analisa);
        $saver=json_decode(file_get_contents(storage_path('app/'.$p.'.json')),true);
       
        $series_w=static::check_schema($tahun,$kode_analisa,'run',$request);

        return [
            'chart_list'=>$saver['chart_schema'],
            'series'=>$series_w['series']
        ];

    }

    public function canvas_chart($tahun,$kode_analisa,Request $request){
        $saver=static::check_schema($tahun,$kode_analisa,'run',$request);
        if($kode_analisa){
            if($saver){
                $series=$saver['series'];
                $data=$saver['data'];
                unset($saver['series']);
                unset($saver['data']);
                return view('sistem_informasi.analisa.widget_chart_3')->with(['kode_analisa'=>$kode_analisa,'datas'=>$data,'saver'=>$saver,'series'=>$series]);
            }
        }




    }

    public static function check_schema($tahun,$kode_analisa=null,$check_result='l',Request $request){
            $user=Auth::User();
            $saver=[];
            $data_l=[];

            if($request->schema){
                $data_l=$request->schema;
                foreach($data_l as $k=>$s){
                    $data_l[$k]['fungsi_rentang_nilai']=json_decode($s['fungsi_rentang_nilai']);
                    $data_l[$k]['collection_logic']=isset($s['collection_logic'])?$s['collection_logic']:[];
                }
            }
            if($kode_analisa){
                $kode_analisa=base64_decode($kode_analisa);
                $saver=file_get_contents(storage_path('app/'.$kode_analisa.'.json'));
                if($saver){
                    $saver=json_decode($saver,true);
                    if(count($data_l)){
                        $saver['selected_data_list']=$data_l;
                        $saver['judul']='';
                        $saver['filter_1']=$request->filter_1??2;
                        $saver['filter_2']=$request->filter_2??2;
                        $saver['tahun_pembuatan']=(int)$saver['tahun_pembuatan']??$tahun;
                        Storage::put($path.'.json',json_encode($saver));
                    }else{

                        
                        $request['schema']=$saver['selected_data_list'];
                        $data_l=$request['schema'];



                    }
                    if(!isset($saver['dinamic'])){
                        $saver['dinamic']=false;
                    }
                    if(!isset($saver['tahun_analisa'])){
                        $saver['tahun_analisa']=$saver['tahun_pembuatan'];
                    }

                    if($saver['dinamic']==true){

                        if(!isset($saver['tahun_analisa'])){
                            $saver['tahun_analisa']=$tahun;
                        }

                        foreach($data_l as $k=>$d){
                            $data_l[$k]['tahun']=$tahun+(((int)$saver['tahun_pembuatan'])-((int)$d['tahun']));
                        }
                    }


                }

                $kode_analisa=base64_encode($kode_analisa);
               
            }


            if(count($saver)==0){
                $id=$request->fingerprint();
                $date=Carbon::now()->format('d-m-Y');
                $time=Carbon::now()->format('h-i-s');
                $path='/schema-analisa/'.$user->id.'/'.$date.'/'.$time.'/'.$id;
                $saver=[
                    'selected_data_list'=>$data_l,
                    'judul'=>'',
                    'filter_1'=>$request->filter_1??2,
                    'filter_2'=>$request->filter_1??1,
                    'tahun_pembuatan'=>$tahun,
                ];
                $kode_analisa=base64_encode($path);
                Storage::put($path.'.json',json_encode($saver));
           }

        $list_dataset=[];
        $index_as_table=0;
        $list_group_aggre=[];
        $list_all_select=array_keys($data_l);
        foreach($data_l as $key=>$sc){

            if(!isset($list_dataset[$sc['id_dataset']])){
                $dts=DB::table('master.dataset')->find($sc['id_dataset']);
                $list_dataset[$sc['id_dataset']]=[
                    'name'=>$dts->name,
                    'table'=>$dts->table,
                    'field'=>[]
                ];
            }


            if(count($list_dataset[$sc['id_dataset']]['field'])==0){
                $list=DB::table('master.dataset_data as sda')
                ->join('master.data as sd','sda.id_data','=','sd.id')->where('sda.id_dataset',$sc['id_dataset'])
                ->select('sd.id','sd.name','sda.field_name')->get();
                foreach($list as $l){
                    $list_dataset[$sc['id_dataset']]['field'][$l->id]=[
                        'name'=>$l->name,
                        'field'=>$l->field_name,
                        
                    ];
                }
            }
            // membuat pengelompokan query berdasarkan tipe aggregasi
            if(!isset($list_group_aggre[$sc['tipe_aggregate']])){
                $list_group_aggre[$sc['tipe_aggregate']]=[
                    'tahun_list'=>[],
                    'tw_list'=>[],
                    'schema_query'=>[]
                ];
            }

            // membuat scope tahun dan tw yang akan di select
            $astable='d'.$sc['tipe_aggregate'].'_'.$sc['id_dataset'];
            if(!isset($list_group_aggre[$sc['tipe_aggregate']]['schema_query'][$sc['id_dataset']])){
                switch($sc['tipe_aggregate']){
                    case 2:
                        $al=3;
                    break;
                    case 1:
                        $al=1;
                    break;
                    default:
                        $al=5;
                    break;
                }
                $list_group_aggre[$sc['tipe_aggregate']]['schema_query'][$sc['id_dataset']]=[
                    'table'=>'dataset."'. $list_dataset[$sc['id_dataset']]['table'].'" as '.$astable,
                    'table_as'=>$astable,
                    'tahun_list'=>[],
                    'tw_list'=>[],
                    'al'=>$al,
                    'join_pemda'=>'left('.$astable.'.kodepemda,'.$al.')',
                    'query'=>null,
                    'select'=>[]
                ];
            }

            if(!in_array((int)$sc['tahun'],$list_group_aggre[$sc['tipe_aggregate']]['schema_query'][$sc['id_dataset']]['tahun_list'])){
                $list_group_aggre[$sc['tipe_aggregate']]['schema_query'][$sc['id_dataset']]['tahun_list'][]=(int)$sc['tahun'];
            }

            if(!in_array((int)$sc['tw'],$list_group_aggre[$sc['tipe_aggregate']]['schema_query'][$sc['id_dataset']]['tw_list'])){
                $list_group_aggre[$sc['tipe_aggregate']]['schema_query'][$sc['id_dataset']]['tw_list'][]=(int)$sc['tw'];
            }

            if(!isset($list_group_aggre[$sc['tipe_aggregate']]['schema_query'][$sc['id_dataset']]['select'][$key])){
                    $sss=null;
                    if($sc['tipe_schema']==1){
                        $sss="(case when (".$astable.".tahun=".$sc['tahun']." and ".$astable.".tw=".$sc['tw'].") 
                        then ".($astable.'.'.$list_dataset[$sc['id_dataset']]['field'][$sc['id_data']]['field'])." else null end)";

                    }else if($sc['tipe_schema']==2){
                        $sss="(case when (".$astable.".tahun=".$sc['tahun']." and ".$astable.".tw=".$sc['tw'].") and (".str_replace('valR',($astable.'.'.$list_dataset[$sc['id_dataset']]['field'][$sc['id_data']]['field']),str_replace('valR_',(($astable.'.'.$list_dataset[$sc['id_dataset']]['field'][$sc['id_data']]['field']).' and '.($astable.'.'.$list_dataset[$sc['id_dataset']]['field'][$sc['id_data']]['field'])),implode(' ',$sc['collection_logic']))).") 
                        then ".($astable.'.'.$list_dataset[$sc['id_dataset']]['field'][$sc['id_data']]['field'])." else null end)";
                    }
                    if($sss){
                        switch($sc['aggregate']){
                            case 'SUM';
                            $list_group_aggre[$sc['tipe_aggregate']]['schema_query'][$sc['id_dataset']]['select'][$key]=[
                                'name'=>$sc['title_aggregate'],
                                'q'=>'SUM('.$sss.') as '.$key
                            ];
                            break;
                            case 'MIN';
                            $list_group_aggre[$sc['tipe_aggregate']]['schema_query'][$sc['id_dataset']]['select'][$key]=[
                                'name'=>$sc['title_aggregate'],
                                'q'=>'MIN('.$sss.') as '.$key
                            ];
                            break;
                            case 'MAX';
                            $list_group_aggre[$sc['tipe_aggregate']]['schema_query'][$sc['id_dataset']]['select'][$key]=[
                                'name'=>$sc['title_aggregate'],
                                'q'=>'MAX('.$sss.') as '.$key
                            ];
                            break;
                            case 'AVERAGE';
                            $list_group_aggre[$sc['tipe_aggregate']]['schema_query'][$sc['id_dataset']]['select'][$key]=[
                                'name'=>$sc['title_aggregate'],
                                'q'=>'AVERAGE('.$sss.') as '.$key
                            ];
                            break;
                            case 'COUNT';
                            $list_group_aggre[$sc['tipe_aggregate']]['schema_query'][$sc['id_dataset']]['select'][$key]=[
                                'name'=>$sc['title_aggregate'],
                                'q'=>'COUNT('.$sss.') as '.$key
                            ];
                            break;
                            case 'COUNT DISTINCT';
                            $list_group_aggre[$sc['tipe_aggregate']]['schema_query'][$sc['id_dataset']]['select'][$key]=[
                                'name'=>$sc['title_aggregate'],
                                'q'=>'COUNT(DISTINCT('.$sss.')) as '.$key
                            ];
                            break;
                            default:
                            $list_group_aggre[$sc['tipe_aggregate']]['schema_query'][$sc['id_dataset']]['select'][$key]=[
                                'name'=>$sc['title_aggregate'],
                                'q'=>'MAX('.$sss.') as '.$key
                            ];

                            break;
                        }
                       
                    }
            }
        }
        $query=[];
        foreach($list_group_aggre as $tipe_ag=>$schema_q){
            foreach($list_group_aggre[$tipe_ag]['schema_query'] as $id_dts=>$quer){
                $select_this=[];
                foreach($list_all_select as $p){
                    if(isset($quer['select'][$p])){
                        $select_this[]=$quer['select'][$p]['q'];
                    }else{
                        $select_this[]='0 as '.$p;
                    }
                } 
                $query[$tipe_ag][]="(select ".$quer['join_pemda']." as kodepemda, ".implode(' ,',$select_this)." from ".$quer['table']." where ".($quer['table_as'].".tahun in (".implode(',',$quer['tahun_list']).")  and ".$quer['table_as'].".tw in (".implode(',',$quer['tw_list']).")")." group by ".$quer['join_pemda'].")";
            }
           
        }

        $final_query=[];
        foreach($query as $level=>$q){
            if(!isset($final_query[$level])){
                $final_query[$level]=[];
            }
            $union=implode(' UNION ',$q);
            $selall=[];
            foreach($list_all_select as $s){
                $selall[]="max(y.".$s.") as ".$s;
            }
            switch($level){
                case 2:
                    $al=3;
                break;
                case 1:
                    $al=1;
                break;
                default:
                    $al=5;
                break;
            }

            $final_query[$level]=array_merge($final_query[$level],DB::select('select (mp.kodepemda) as kodepemda,max(mp.nama_pemda) as name, '.implode(',',$selall)." from master.master_pemda as mp 
            left join (".$union.") as y on mp.kodepemda=y.kodepemda where length(mp.kodepemda)=".$al." group by mp.kodepemda"));
           
        }
        
        $series=[];
        foreach($data_l as $k=>$sc){
            
            $series[$k]=[
                'id'=>base64_encode($sc['key']),
                'type_of_dss_series'=>1,
                'name'=>$sc['tipe_aggregate']!=0?$sc['title_aggregate']:$sc['name'],
                'satuan'=>null,
                'level'=>$sc['tipe_aggregate'],
                'data'=>[],
                'type'=>'column',
                'xAxis'=>0,
                'dataSorting'=>[
                    "enabled"=>false
                ],
                "yAxis"=>0,
                "dataLabels"=> [
                    "enabled"=> true
                ],
                "visible"=>true,
            ];

            if(!isset($saver['data'][$sc['tipe_aggregate']]['header'])){
                $saver['data'][$sc['tipe_aggregate']]['header']=[];
            }
            if(!isset($saver['data'][$sc['tipe_aggregate']]['data'])){
                $saver['data'][$sc['tipe_aggregate']]['data']=[];
            }
            $saver['data'][$sc['tipe_aggregate']]['header'][$sc['key']]=$sc;
            foreach($final_query[$sc['tipe_aggregate']] as $l=>$d){
                $d=(array)$d;
                $d[$k]=(float)$d[$k];
                $saver['data'][$sc['tipe_aggregate']]['data'][$d['kodepemda']]=$d;
                $series[$k]['data'][]=[
                    'name'=>$d['name'],
                    'y'=>(float)$d[$k],
                    
                ];
            }
           
        }

        $series=array_values($series);

        $saver['series']=($series);
        


        // Storage::put(base64_decode($kode_analisa).'.json',json_encode($saver));
        
        if($check_result!=='run'){
            if($kode_analisa){
                return redirect()->route('dash.si.analisa.canvas',['tahun'=>$tahun,'kode_analisa'=>$kode_analisa]);
            }else{
                return redirect()->route('dash.si.analisa.wg_build',['tahun'=>$tahun,'kode_analisa'=>$kode_analisa]);
            }
        }else{
            return $saver;
        }


    }

    public function new_widget($tahun,$kode_analisa=null,Request $request){
        if($kode_analisa){
            $kode_analisa=base64_decode($kode_analisa);
            $saver=file_get_contents(storage_path('app/'.$kode_analisa.'.json'));
            if($saver){
                $saver=json_decode($saver,true);
            }
        }else{
            $saver=[
                'selected_data_list'=>[],
                'judul'=>'',
                'filter_1'=>2,
                'filter_2'=>1,
                'tahun_pembuatan'=>$tahun
            ];
        }

        return view('sistem_informasi.analisa.widget_chart')->with('saver',$saver);
    }

    public function listing_schema($tahun,Request $request){
        $tahun_mulai=$tahun-4;
        $tahun_selesai=$tahun;

        $dataset=DB::table('master.dataset as sdt')
        ->join('master.dataset_data as sda','sda.id_dataset','=','sdt.id')
        ->join('master.data as sd','sd.id','=','sda.id_data');
        if($request->q){

         $dataset=$dataset->where([
                ['sdt.name','ilike','%'.$request->q.'%'],
                ['sdt.status',true]
            ])
            ->orWhere([
            ['sd.name','ilike','%'.$request->q.'%'],
            ['sdt.status',true]
        ]);
        }else{
            $dataset=$dataset->where('sdt.status',true);
        }
        $dataset=$dataset->selectRaw("
        sdt.id as dt_id
        ")->groupBy('sdt.id')
        ->paginate(5);

        if($request->q){
            $dataset->appends(['q'=>$request->q]);
        }
        $id_dataset=[0];
        $selectRaw=[];
        if(count($dataset->items())){
            
            $id_dataset=array_map(function($el){
                return $el->dt_id;
            },($dataset->items()));
        }

        $data=(array)DB::table('master.dataset as sdt')
        ->join('master.dataset_data as sda','sda.id_dataset','=','sdt.id')
        ->join('master.data as sd','sd.id','=','sda.id_data')
        ->whereIn('sdt.id',$id_dataset)
        ->selectRaw("1 as tipe_schema,0 as have_child,sd.fungsi_rentang_nilai,sda.id_dataset,sdt.name as nama_dataset,sdt.interval_pengambilan,sdt.pusat,sda.id_data,sdt.table as table_record, sda.field_name,sd.name,sd.satuan,sd.definisi_konsep,sd.tipe_nilai")
        ->orderBy('sda.id_dataset','asc')
        ->orderBy('sda.index','asc')
        ->get()->toArray();

        
        foreach($data as $key=>$d){
            $selectRaw=[];
            $d=(array)$d;
            $data[$key]=$d;


            for($i=$tahun-5;$i<=$tahun;$i++){
                $tw=4;
                $selectRaw[]='sum(case when (dr.tahun='.$i.' and dr.tw='.$tw.') then 1 else 0 end) as tahun_'.$i.'_tw_'.$tw.'';
            
                $dex=str_replace('[TAHUN]',$i,$d['table_record']);
                $exist_table=DB::table('pg_tables')->where('schemaname','dataset')->where('tablename',$dex)->first();
                
                if($exist_table){
                    
                    $field_list=(array)DB::table('dataset.'.$dex.' as dr')->selectRaw(implode(',',$selectRaw))->first();
                    foreach($field_list as $fk=>$f){
                        $data[$key][$fk]=$f;
                    }
               
                }else{

                }
               
            }
           
            
           
        }

        return [
            'page_init'=>$dataset,
            'data'=>$data
        ];

    }


    public function build_json_schema_chart(){

    }

    public function render(){

    }
}
