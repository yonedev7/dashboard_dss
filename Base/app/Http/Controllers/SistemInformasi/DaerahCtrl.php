<?php

namespace App\Http\Controllers\SistemInformasi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;
use Alert;
use App\Http\Controllers\FilterCtrl;

class DaerahCtrl extends Controller
{
    
    public function index($tahun,Request $request){
        $basis=$request->basis??'nasional';
        $page_meta=[
            'title'=>'Lokus NUWSP',
            'keterangan'=>''
        ];
        return view('admin.daerah.index')->with(['basis'=>$basis,'page_meta'=>$page_meta,'req'=>$request->all()]);
    }


    public function update($tahun,$kodepemda,Request $request){
        switch($request->regional_1){
            case '0':
                $request['regional_2']=NULL;
            break;
            case '1':
                $request['regional_2']='SUMATRA';
            break;
            case '2':
                $request['regional_2']='SULAWESI';
            break;
            case '3':
                $request['regional_2']='KALIMANTAN';
            break;
            case '4':
                $request['regional_2']='JAWA';
            break;
            case '5':
                $request['regional_2']='BALI, NTT, MALUKU & PAPUA';
            break;
        }

        if($request->tipe_bantuan==0){
            $request['tipe_bantuan']=null;

        }
        if($request->regional_1==0){
            $request['regional_1']=null;

        }
        dd($request->regional_1);

        DB::table('master.master_pemda')->where('kodepemda',$kodepemda)->update([
            'regional_1'=>$request->regional_1,
            'regional_2'=>$request->regional_2,
            'tahun_proyek'=>$request->tahun_proyek,
            'tipe_bantuan'=>$request->tipe_bantuan,
        ]);

        Alert::success('','Berhasil Memperbarui data');
        return back();

    }


    public function load($tahun,Request $request){
        $filter=['regional'=>$request->filter_regional,'sortlist'=>$request->filter_sortlist,'lokus'=>false,'tipe_bantuan'=>$request->filter_tipe_bantuan];
        $pemdas=FilterCtrl::filter_pemda($filter['regional'],$filter['lokus'],$filter['sortlist'],$filter['tipe_bantuan']);

       return DB::table('master.master_pemda as pd')
       ->leftJoin('master.pemda_lokus as lk','lk.kodepemda','=','pd.kodepemda')
       ->selectRaw("pd.kodepemda,pd.nama_pemda,pd.regional_1,pd.regional_2,lk.tipe_bantuan,lk.tahun_proyek,lk.tahun_bantuan,lk.lokus")
       ->whereIn('pd.kodepemda',$pemdas)->get();
        

        
    }

}
