<?php

namespace App\Http\Controllers\SistemInformasi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
class HomeCtrl extends Controller
{
    //

    public function index($tahun,Request $request){
        $My=Auth::User();
        if($My->can('is_admin')){
            if($request->user){
                $My=DB::table('public.users')->find($request->user);

            }

        }
        return view('sistem_informasi.index')->with(['info_user'=>$My]);
    }
}
