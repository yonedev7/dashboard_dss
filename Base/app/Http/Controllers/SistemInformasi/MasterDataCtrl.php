<?php

namespace App\Http\Controllers\SistemInformasi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use Alert;
use Validator;
use Storage;
class MasterDataCtrl extends Controller
{
    static function schema_display($name){
        
    }

   
    public function json_get_data(Request $request){
            $data=DB::table('master.data as d')->selectRaw("0 as aggregasi,0 as login,'' as title_aggregate,d.*,v.name as nama_validator")
            ->leftJoin('master.validator_data as v','v.id','=','d.id_validator');
            $lim=30;
            if($request->q){
                $data=$data->where('d.name','ilike','%'.$request->q.'%');
            }
            if($request->id_validator){
                $lim=20;
                $data=$data->where('d.id_validator',$request->id_validator);
            }
            if($lim){
                $data=$data->limit($lim);
            }
            $data=$data->get();
            return response()->json([
                'data'=>$data,
            ],200);

    }

    
    static $file=[
        [
            'tag'=>'MS World (docx)',
             'val'=>'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
             'selected'=>0,
            'tipe'=>'file'
        ],
        [
            'tag'=>'MS Excel (xlxs)',
             'val'=>'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
             'selected'=>0,
            'tipe'=>'file'

        ],
        [
            'tag'=>'MS Power Point (pptx)',
             'val'=>'application/vnd.openxmlformats-officedocument.presentationml.presentation',
             'selected'=>0,
            'tipe'=>'file'

        ],
        [
            'tag'=>'PDF',
            'val'=>'application/pdf',
            'selected'=>0,
            'tipe'=>'file'


        ],
        [
            'tag'=>'Image',
            'val'=>'image/*',
            'selected'=>0,
            'tipe'=>'file'

        ],
    ];

    public function index($tahun){
        $data=DB::table('master.validator_data as vl')
        ->selectRaw("
            vl.id,
            max(vl.name) as name,
            count(data.id) as jumlah_data
        ")
        ->leftJoin('master.data as data',[['data.id_validator','=','vl.id']])
        ->orderBy('vl.created_at','DESC')
        ->groupBy('vl.id')
        ->get();

        foreach($data as $key=>$d){
            $data[$key]->link_detail=route('dash.si.schema.data.detail_validator',['tahun'=>$tahun,'id_validator'=>$d->id]);
            $data[$key]->link_delete=route('dash.si.schema.data.delete_validator',['tahun'=>$tahun,'id_validator'=>$d->id]);

        }

       
        return view('sistem_informasi.data.index')->with(['validators'=>$data]);
    }
    public function tambah_validator($tahun){
        return view('sistem_informasi.data.validator.tambah');
        
    }

    public function store_validator($tahun,Request $request){
        $valid=Validator::make($request->all(),[
            'nama'=>'required|string|unique:validator_data,name',
            'keterangan'=>'nullable|string'
        ]);

        if($valid->fails()){
            Alert::error('',$valid->errors()->first());
            return back();
        }else{
            $validator_data=DB::table('master.validator_data')->insert([
                'name'=>$request->nama,
                'keterangan'=>$request->keterangan
            ]);

            if($validator_data){
                Alert::success('','Berhasil Menambahkan validator data');
                return redirect()->route('dash.si.schema.data.index',['tahun'=>$tahun]);
            }
        }
    }

    public function detail_validator($tahun,$id,Request $request){
        
        if(is_numeric($id)){
            $validator_data=DB::table('master.validator_data')->find($id);
            if($validator_data){
                $data=DB::table('master.data')->where('id_validator',$validator_data->id)
                ->orderby('created_at','DESC')->get();
                foreach($data as $key=>$d){
                    
                    $data[$key]->link_edit=route('dash.si.schema.data.edit_data',['tahun'=>$tahun,'id_validator'=>$d->id_validator,'id_data'=>$d->id]);
                    $data[$key]->link_delete=route('dash.si.schema.data.delete_data',['tahun'=>$tahun,'id_validator'=>$d->id_validator,'id_data'=>$d->id]);
                }

                return view('sistem_informasi.data.validator.detail')->with(['validator_data'=>$validator_data,'data'=>$data]);

            }
            

        }else{

        }
        
    }

    public function tambah_data($tahun,$id,Request $request){
        if(is_numeric($id)){
            $validator_data=DB::table('master.validator_data')->find($id);
            if($validator_data){
                $file=static::$file;
                return view('sistem_informasi.data.validator.tambah_data')->with(['validator_data'=>$validator_data,'file_pilihan'=>$file]);

            }
        }
    }
    

    public function store_data($tahun,$id_validator,Request $request){
        if(is_numeric($id_validator)){
            $validator_data=DB::table('master.validator_data')->find($id_validator);
            if($validator_data){
              $valid=Validator::make($request->all(),[
                'nama'=>'required|string',
                'kode_khusus'=>'nullable|string',
                'tipe_data'=>'required|string',
                'jenis_data'=>'required|numeric',
                'arah_nilai'=>'required|numeric',
                'produsent_data'=>'required|string',


              ]);

              if($valid->fails()){
                  Alert::error('',$valid->errors()->first());
                  return back()->withInput();
              }else{

                do{
                    $table='data_'.md5(date('ymdhi'));
                }while(DB::table('master.data')->where('table',$table)->first());



                $fungsi_rentang_nilai=[];
                if($request->rentang_nilai_file??$request->rentang_nilai){
                    $tipe_data=$request->tipe_data;
                    if(in_array($tipe_data,['single-file','multy-file'])){
                        $sort_file=[];
                        foreach($request->rentang_nilai_file as $mim){
                            array_push($sort_file,$mim['val']);
                        }

                        $fungsi_rentang_nilai=[
                            'tipe'=>$tipe_data,
                            'm'=>$request->rentang_nilai_file,
                            'sort'=>implode(',',$sort_file)
                        ];
                    }else if($request->rentang_nilai_fungsi=='pilihan'){
                        $sort=[];

                        foreach($request->rentang_nilai as $mim){
                            array_push($sort,$mim['tag']);
                        }
                        $fungsi_rentang_nilai=[
                            'tipe'=>$request->rentang_nilai_fungsi,
                            'm'=>$request->rentang_nilai,
                            'sort'=>implode(' , ',$sort)
                        ];
                    }else if($request->rentang_nilai_fungsi=='min-max'){
                        $sort=[];

                        foreach($request->rentang_nilai as $mim){
                            array_push($sort,$mim['val']);
                        }

                        $m=-10000000;
                        foreach($sort as $kk=>$k){
                            $k=(float)$k;
                            
                            if($k>$m){
                                $m=$k;
                            }else{
                                Alert::error('','Rentang Nilai Tidak Sesuai');
                                return back()->withInput();
                            }
                        }

                        $fungsi_rentang_nilai=[
                            'tipe'=>$request->rentang_nilai_fungsi,
                            'm'=>$request->rentang_nilai,
                            'sort'=>$sort
                        ];
                    }

                }


                

                $fungsi_rentang_nilai=$fungsi_rentang_nilai!=[]?json_encode($fungsi_rentang_nilai):'{}';

                $data=DB::table('master.data')->insertGetId([
                    'name'=>$request->nama,
                    'table'=>$table,
                    'produsent_data'=>$request->produsent_data,                    
                    'satuan'=>$request->satuan,
                    'jenis_data'=>$request->jenis_data,
                    'tipe_nilai'=>$request->tipe_data,
                    'definisi_konsep'=>$request->definisi_konsep,
                    'fungsi_aggregasi'=>$request->tipe_aggregasi,
                    'kode'=>$request->kode_khusus,
                    'fungsi_rentang_nilai'=>$fungsi_rentang_nilai,
                    'id_validator'=>$id_validator,
                    'arah_nilai'=>$request->arah_nilai,
                    'cara_hitung'=>$request->cara_hitung

                ]);

                if($data){

                    $type='';
                    switch($request->tipe_data){
                        case 'numeric':
                            $type='float8';
                        break;
                        case 'string':
                            $type='varchar(195)';
                        break;
                        case 'text':
                            $type='text';
                        break;
                        case 'single-file':
                            $type='varchar(255)';
                        break;
                        case 'multy-file':
                            $type='text';
                        break;
                    }
                    $table='data_'.$data;
                    DB::table('master.data')->where('id',$data)->update([
                        'table'=>$table
                    ]);
                   
                    static::create_table($table,$type);

                    Alert::success('','Berhasil Menambahkan data');
                    return redirect()->route('dash.si.schema.data.detail_validator',['tahun'=>$tahun,'id_validator'=>$id_validator]);
                }

              }



            }
        }
    }


    static function create_table($table,$type){
        $table_data=[
            "CREATE TABLE IF NOT EXISTS data.".$table." (
                id bigserial NOT NULL,
                kodepemda varchar NULL,
                tahun int4 NULL,
                tw int2 NULL,
                id_dataset bigint,
                val ".$type." NULL,
                CONSTRAINT ".$table."_pk PRIMARY KEY (id),
                CONSTRAINT ".$table."_un UNIQUE (kodepemda, tahun, tw, id_dataset)
            );
            ",
            "CREATE TABLE IF NOT EXISTS data_konsensus.".$table." (
                id bigserial NOT NULL,
                kodepemda varchar NULL,
                tahun int4 NULL,
                tw int2 NULL,
                val ".$type." NULL,
                CONSTRAINT ".$table."_pk PRIMARY KEY (id),
                CONSTRAINT ".$table."_un UNIQUE (kodepemda, tahun, tw)
            );
            
            "
        ];
        $table_data_index=[
            "CREATE INDEX IF NOT EXISTS ".$table."_kodepemda_idx ON data.".$table." USING btree (kodepemda);",
            "CREATE INDEX IF NOT EXISTS  ".$table."_tahun_idx ON data.".$table." USING btree (tahun);",
            "CREATE INDEX IF NOT EXISTS  ".$table."_tw_idx ON data.".$table." USING btree (tw);",
            "CREATE INDEX IF NOT EXISTS  ".$table."_dataset_idx ON data.".$table." USING btree (id_dataset);",
            "CREATE INDEX IF NOT EXISTS ".$table."_kodepemda_idx ON data.".$table." USING btree (kodepemda);",
            "CREATE INDEX IF NOT EXISTS  ".$table."_tahun_idx ON data.".$table." USING btree (tahun);",
            "CREATE INDEX IF NOT EXISTS  ".$table."_tw_idx ON data.".$table." USING btree (tw);"
        ];

        $schema_saved='';
        foreach($table_data as $sql){
            DB::statement($sql);
            $schema_saved.=$sql."
";
        }

        foreach($table_data_index as $sql){
            DB::statement($sql);
            $schema_saved.=$sql."
";
        }

        Storage::disk('data_schema')->put(date('Y-m-d-h-i-s').'-'.'DDL-'.$table.'.sql',$schema_saved);
     
      
    }


    public function edit_data($tahun,$id,$id_data,Request $request){
        if(is_numeric($id_data)){
            $validator_data=DB::table('master.validator_data')->find($id);
            $data=DB::table('master.data')->where([
                ['id','=',$id_data],
                ['id_validator','=',$id],
            ])->first();

            if($data){
                $data->rentang_nilai_fungsi=0;
                $data->rentang_nilai_file=[];
                $data->rentang_nilai=[];

                $rentang_nilai=$data->fungsi_rentang_nilai?json_decode($data->fungsi_rentang_nilai,true):[];
                if(!$rentang_nilai){
                    $rentang_nilai=[
                        'tipe'=>$data->tipe_nilai,
                        'm'=>[],
                        'sort'=>''
                    ];
                }
                if(in_array($data->tipe_nilai,['single-file','multy-file'])){
                    $data->rentang_nilai_file=($rentang_nilai['m']);
                }else{
                    $data->rentang_nilai_fungsi=isset($rentang_nilai['tipe'])?$rentang_nilai['tipe']:null;
                    $data->rentang_nilai=isset($rentang_nilai['m'])?array_values($rentang_nilai['m']):[];
                }

                $file=static::$file;





                return view('sistem_informasi.data.validator.edit_data')->with(['validator_data'=>$validator_data,'file_pilihan'=>$file,'data'=>$data]);
            }
        }
    }


    public function update_data($tahun,$id,$id_data,Request $request){
        if(is_numeric($id)){
            $validator_data=DB::table('master.validator_data')->find($id);
            $data=DB::table('master.data')->where([['id','=',$id_data],['id_validator','=',$id]])->first();

            if($validator_data AND $data){
              $valid=Validator::make($request->all(),[
                'nama'=>'required|string',
                'kode_khusus'=>'nullable|string',
                'arah_nilai'=>'required|boolean',

              ]);

              if($valid->fails()){
                  Alert::error('',$valid->errors()->first());
                  return back()->withInput();
              }else{
                $fungsi_rentang_nilai=[];
                if($request->rentang_nilai_file??$request->rentang_nilai){
                    $tipe_data=$data->tipe_nilai;

                    if(in_array($tipe_data,['single-file','multy-file'])){
                        $sort_file=[];
                        $file_selected=[];
                        foreach($request->rentang_nilai_file as $key=>$mim){
                            if($mim['selected']==1 OR $mim['selected']=='true'){
                                array_push($sort_file,$mim['val']);
                                $file_selected[]=$mim;
                            }
                        }

                        $fungsi_rentang_nilai=[
                            'tipe'=>$tipe_data,
                            'm'=>$file_selected,
                            'sort'=>implode(',',$sort_file)
                        ];
                    }else if($request->rentang_nilai_fungsi=='pilihan'){
                        $sort=[];

                        foreach($request->rentang_nilai as $mim){
                            array_push($sort,$mim['tag']);
                        }
                        $fungsi_rentang_nilai=[
                            'tipe'=>$request->rentang_nilai_fungsi,
                            'm'=>$request->rentang_nilai,
                            'sort'=>implode(' , ',$sort)
                        ];
                    }else if($request->rentang_nilai_fungsi=='min-max'){
                        $sort=[];

                        foreach($request->rentang_nilai as $mim){
                            array_push($sort,(float)$mim['val']);
                        }
                        $m=-10000000;
                        foreach($sort as $kk=>$k){
                            $k=(float)$k;
                            
                            if($k>$m){
                                $m=$k;
                            }else{
                                Alert::error('','Rentang Nilai Tidak Sesuai');
                                return back()->withInput();
                            }
                        }
                        $fungsi_rentang_nilai=[
                            'tipe'=>$request->rentang_nilai_fungsi,
                            'm'=>$request->rentang_nilai,
                            'sort'=>$sort
                        ];
                    }

                }



                $fungsi_rentang_nilai=$fungsi_rentang_nilai!=[]?json_encode($fungsi_rentang_nilai):'{}';
                DB::beginTransaction();

                try {
                    $data=DB::table('master.data')->where('id',$id_data)
                    ->update([
                        'name'=>$request->nama,
                        'produsent_data'=>$request->produsent_data,                    
                        'satuan'=>$request->satuan,
                        'jenis_data'=>$request->jenis_data,
                        'definisi_konsep'=>$request->definisi_konsep,
                        'fungsi_aggregasi'=>$request->tipe_aggregasi,
                        'kode'=>$request->kode_khusus,
                        'fungsi_rentang_nilai'=>$fungsi_rentang_nilai,
                        'id_validator'=>$id,
                        'arah_nilai'=>$request->arah_nilai,
                        'cara_hitung'=>$request->cara_hitung
                    ]);

                    DB::commit();

                } catch (\Exception $e) {
                    DB::rollback();

                    Alert::error('','Server Tidak Dapat Memproses Data');

                    return back();
                }
                

                if($data){
                    Alert::success('','Berhasil Mengubah data');
                    return back();
                }

              }



            }
        }

    }

    public function delete_validator($tahun,$id_validator){
        $delete=DB::table('master.validator_data')->where([['id','=',$id_validator]])->delete();
        if($delete){
            Alert::success('','Berhasil Menghapus validator data');
            return back();
        }
    }

    public function delete_data($tahun,$id_validator,$id_data,Request $request){
        $delete=DB::table('master.data')->where([['id','=',$id_data],['id_validator','=',$id_validator]])->delete();
        if($delete){
            Alert::success('','Berhasil Menghapus data');
            return redirect()->route('dash.si.schema.data.detail_validator',['tahun'=>$tahun,'id_validator'=>$id_validator]);
        }else{
            Alert::error('','Gagal Menghapus data');
            return redirect()->route('dash.si.schema.data.detail_validator',['tahun'=>$tahun,'id_validator'=>$id_validator]);
        }
    }
}
