<?php

namespace App\Http\Controllers\SistemInformasi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Validator;
use Alert;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Storage;
use Str;
use Artisan;
use Carbon\Carbon;
use Auth;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class MasterDatasetCtrl extends Controller
{   


    static function shchema_display($name){
       return [
            'name' => $name,
            'schema' => [
              0 => [
                'name' => 'Schema Tampilan 1',
                'style' => 0,
                'judul' => '',
                'keterangan' => '',
                'tag' => [
                ],
                'scoped_time' => 1,
                'group' => [
                ],
                'where' => [
                ],
                'value' => [
                ],
              ],
              1 => [
                'name' => 'Schema Tampilan 2',
                'style' => 0,
                'judul' => '',
                'keterangan' => '',
                'tag' => [
                ],
                'scoped_time' => 1,
                'group' => [
                ],
                'where' => [
                ],
                'value' => [
                ],
              ],
              2 => [
                'name' => 'Schema Tampilan 3',
                'style' => 0,
                'judul' => '',
                'keterangan' => '',
                'tag' => [
                ],
                'scoped_time' => 1,
                'group' => [
                ],
                'where' => [
                ],
                'value' => [
                ],
              ],
              3 => [
                'name' => 'Schema Tampilan 4',
                'style' => 0,
                'judul' => '',
                'keterangan' => '',
                'tag' => [
                ],
                'scoped_time' => 1,
                'group' => [
                ],
                'where' => [
                ],
                'value' => [
                ],
              ],
            ],
        ];
    }
    static function build_query($col,$table){
        $col_display=[];
        if($col->style){
            $q=[]; 

           $col_display=[
                 "judul"=>$col->judul,
                "style"=>$col->style,
                "series"=>[],
                "select"=>[],
                "where"=>"",
                "table"=>'dataset.'.$table.' as dataset',
                "groupBy"=>"",
                "sort_query"=>""
            ];
            $qr=array_map(function($el){ 
                if($el->context!='operator'){
                    if($el->id_list!='nama_pemda'){
                        return 'dataset.'.$el->id_list;
                    }else{
                        return 'mpemda.'.$el->id_list;
                    }
                }else{
                    return $el->id_list;
                }
             }, $col->where);
            
           $col_display['where']=implode('',$qr);
             if($col->scoped_time){
               $col_display['where']=($col_display['where']?$col_display['where']+' AND ':'')."dataset.tahun=[TAHUN] AND dataset.tw=[TW]";
             }
            $qr=array_map(function($el){ 
                if($el->context!='operator'){
                    if($el->id_list!='nama_pemda'){
                        return 'dataset.'.$el->id_list;
                    }else{
                        return 'mpemda.'.$el->id_list;
                    }
                }else{
                    return $el->id_list;
                }
             }, $col->group);

           $col_display['groupBy']=implode('',$qr);

            foreach($col->value as $kv=>$val){               
                $qr=array_map(function($el){ 
                    if($el->context!='operator'){
                        if($el->id_list!='nama_pemda'){
                            return 'dataset.'.$el->id_list;
                        }else{
                            return 'mpemda.'.$el->id_list;
                        }
                    }else{
                        return $el->id_list;
                    }
                 }, $val->list);
                 $q=[
                     'query'=>implode('',$qr),
                     'as'=>strtolower(str_replace(' ','_',$val->name).' '),
                     'name'=>$val->name,
                    ]; 
                    $col_display['series'][$kv]=[
                        'name'=>$val->name,
                        'data'=>'this.dataset.map(function(el){return el.'.$q['as'].';}).join(",")'
                    ];
    
                    if(count($q)){
                       $col_display['select'][]=$q;
                    }
    
             
                }
                

               

               $col_display['sort_query']="select ".implode(',',array_map(function($el){return $el['query']." as ".$el['as']." ";},$col_display['select'])).' from '.$col_display['table']." ".($col_display['where']?' where '.$col_display['where']:'')." ".($col_display['groupBy']?' group by '.$col_display['groupBy']:'').";";
                
               return $col_display;
        }
    }

    
    static function build_sort_query($schema,$dataset){
        $display=[];
        try{
            foreach($schema->schema as $key=>$sch){
           
                $tam=$sch->schema;
    
                $select=[];
                $jenjang=$key;
                $listed=array_map(function($el){return $el->style;},$tam);
                if(max($listed)){
                    $display[$key]=[];
                    foreach($tam as $kt=>$col){
                        $ds=static::build_query($col,$dataset->table);
    
                        if($ds){
    
                            $display[$key][]=$ds;
                        }
                       
                     
                     }
                
                }
    
                
              
    
            }

        }catch(\Exception $e){
            return $e;
        }


       return $display;
    }

    public function update($tahun,$id_dataset,Request $request){
      
        if($request->datas){
            $request['datas']=json_decode($request->datas,true);
        }

        if($request->menus){
            $request['menus']=json_decode($request->menus,true);
        }
        if($request->kategori){
            $request['kategori']=json_decode($request->kategori,true);
        }

        if($request->metalinks){
            $request['metalinks']=json_decode($request->metalinks);
        }else{
            $request['metalinks']=[];
        }

        if($request->display){
            $request['display']=json_decode($request->display);
        }else{
            $request['display']=[];
        }

        if($request->scope){
            $request['scope']=json_decode($request->scope);
        }

        


        $valid=Validator::make($request->all(),[
            'datas'=>'array|required',
            'interval_pengambilan'=>'required|numeric',
            'status'=>'required||numeric',
            'name'=>'required|string',
            'tujuan'=>'required|string',
            'penangung_jawab'=>'required|string',
        ]);


        if($valid->fails()){
            Alert::error('',$valid->errors()->first());
            return back()->withInput();
        }

        $dataset=DB::table('master.dataset as dts')->where(['id'=>$id_dataset])->first();
        if($dataset){
            $display=static::build_sort_query($request['display'],$dataset);
            $create_database=false;
            $table_name=$dataset->table;
            $date=Carbon::now();
            $user=Auth::User();
            $columns=[];
            $datums_id=[];

            DB::beginTransaction();


          
            try {
                $data=[
                    'name'=>$request->name,
                    'table'=>$table_name,
                    'display_style'=>$request->display->style,
                    'id_menu'=>$request->id_menu,
                    'login'=>$request->login,
                    'pusat'=>$request->pusat??0,
                    'kategori'=>json_encode($request->kategori??[]),
                    'point'=>$request->point??0,
                    'penangung_jawab'=>$request->penangung_jawab,
                    'tujuan'=>$request->tujuan,
                    'methode_pengambilan'=>$request->methode_pengambilan,
                    'interval_pengambilan'=>$request->interval_pengambilan,
                    'status'=>$request->status,
                    'id_u_created'=>$user->id,
                    'id_u_updated'=>$user->id,
                    'updated_at'=>$date,
                    'created_at'=>$date,
                ];

                if(!$dataset->tanggal_publish){
                    $data['tanggal_publish']=$request->status?$date:null;
                }
                
                DB::table('master.dataset')->where('id',$dataset->id)->update($data);
                $id_dataset=$dataset->id;
                $list_kodepemda=[]; 

                foreach($request->menus??[] as $id_menu){
                    DB::table('master.dataset_tema')->insertOrIgnore([
                        'id_dataset'=>$dataset->id,
                        'id_tema'=>$id_menu
                    ]);
                }

                if(count($request->menus)){
                    DB::table('master.dataset_tema')->where('id_dataset',$id_dataset)->whereNotIn('id_tema',$request->menus)->delete();
                }   

                $scope=array_map(function($el){return $el->kodepemda;},$request->scope);
                $scope_insert=array_map(function($el)use ($id_dataset,$tahun, $user,$date){return ['kodepemda'=>$el->kodepemda,'id_dataset'=>$id_dataset,
                   'tahun'=>$tahun,'id_u_created'=>$user->id,'id_u_updated'=>$user->id,
                   'created_at'=>$date,
                   'updated_at'=>$date
               ];},$request->scope);
               
                if(count($scope)){
                   DB::table('master.dataset_scope')->insertOrIgnore($scope_insert);
                   DB::table('master.dataset_scope')->where('tahun',$tahun)->where('id_dataset',$id_dataset)->whereNotIn('kodepemda',$scope)->delete();
                }
            
               

            foreach($request->datas as $key=>$d){
                $k=null;
                


                if(isset($d['id_recorded'])){
                    DB::table('master.dataset_data')->where('id',$d['id_recorded'])->update([
                        'id_data'=>$d['id'],
                        'id_dataset'=>$id_dataset,
                        'tahun_data'=>$d['tahun_data'],
                        'field_name'=>$d['id_list'],
                        'tw_data'=>$d['tw_data'],
                        'aggregasi'=>$d['aggregasi'],
                        'title_aggregate'=>$d['title_aggregate'],
                        'login'=>$d['login'],
                        'index'=>$key,
                        'id_u_updated'=>$user->id,
                    ]);
                    $datums_id[]=$d['id_recorded'];
                    $k=$d['id_recorded'];
                }else{
                    if(!DB::table('master.dataset_data')->where([
                        ['id_data','=',$d['id']],
                        ['id_dataset','=',$id_dataset],
                        ['tahun_data','=',$d['tahun_data']],
                        ['tw_data','=',$d['tw_data']],
                    ])->first()){
                        $k= DB::table('master.dataset_data')->insertGetId([
                            'id_data'=>$d['id'],
                            'id_dataset'=>$id_dataset,
                            'tahun_data'=>$d['tahun_data'],
                            'field_name'=>$d['id_list'],
                            'tw_data'=>$d['tw_data'],
                            'aggregasi'=>$d['aggregasi'],
                            'title_aggregate'=>$d['title_aggregate'],
                            'login'=>$d['login'],
                            'index'=>$key,
                            'id_u_created'=>$user->id,
                            'id_u_updated'=>$user->id,
                        ]);
                    }
                    
                    if($k){
                    $datums_id[]=$k;

                    }
                }

                if($k){
                    $columns[$d['id_list']]=[
                        'id_data'=>$d['id'],
                        'name'=>$d['name'],
                        'aggregasi'=>$d['aggregasi'],
                        'satuan'=>$d['satuan'],
                        'id_dataset'=>$id_dataset,
                        'tahun_data'=>$d['tahun_data'],
                        'field_name'=>$d['id_list'],
                        'type'=>$d['tipe_nilai'],
                        'tw_data'=>$d['tw_data']
                    ];
                }
            }

                DB::table('master.dataset_data')->where('id_dataset',$id_dataset)->whereNotIn('id',$datums_id)->delete();
                $display=static::build_sort_query($request['display'],$dataset);
                foreach($display as $key=>$ds){
                    $data_q=[]; 
                    for($v=0;$v<4;$v++){
                        $dsc=$ds;
                        unset($dsc['schema']);
                        $data_q['short_query_'.($v+1)]=isset($dsc[$v]['judul'])?json_encode($dsc[$v]):null;
                    }
                    $data_q['schema']=json_encode($request->display->schema[$key]);
                    $data_q['id_dataset']=$id_dataset;
                    $data_q['tingkat']=$key;
                    DB::table('master.dataset_display')->updateOrInsert([
                        'id_dataset'=>$id_dataset,
                        'tingkat'=>$key,
                    ],
                    $data_q
                    );
                }


                $s=DB::table('master.dataset_display')->where([
                    'id_dataset'=>$dataset->id,
                    'tingkat'=>$key,
                ])->get();
            
                

            
            } catch (\Exception $e) {
                DB::rollback();
                dd($e);
                Alert::error('','Gagal Mengubah Dataset');
                return back()->withInput();

            }

            try{
                $schema=static::build_schema($tahun,$table_name,$columns);
                static::migrate_dataset($schema['name']);
                
            }catch(\Exception $e){
                DB::rollback();
                dd($e);


                Alert::error('','Gagal Mengubah Dataset');
                return back()->withInput();
            }
            

            if($request->status){   

            }

            DB::commit();
            
            Alert::success('','Berhasil Mengubah Dataset');
            return redirect()->route('dash.si.schema.dataset.index',['tahun'=>$tahun]);
        
        }
    }

    

    static function cl($num,$row=null) {
        
        $num++;

        $numeric = ($num - 1) % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval(($num - 1) / 26);
        if ($num2 > 0) {
            if($row!=null){
                return str_replace('@','A',(static::cl($num2-1). $letter).$row);
            }else{
                return str_replace('@','A',static::cl($num2-1) . $letter);
            }
            
        } else {
            if($row!=null){
             return str_replace('@','A',$letter.$row);
            }else{
                return str_replace('@','A',$letter);
            }
        }
    }



    public function download_excel($tahun,$id_dataset,Request $request){
       $tw= $request->tw??4;
        $dataset=DB::table('master.dataset')->find($id_dataset);
        if($dataset){
            $colom_schema=DB::table('master.dataset_data as sd')
            ->join('master.data as d','d.id','=','sd.id_data')
            ->selectRaw("d.name,d.satuan,sd.field_name,d.tipe_nilai,d.fungsi_rentang_nilai")
            ->where('sd.id_dataset',$id_dataset)->orderBy('sd.index','asc')->get();
        }
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load(storage_path('app/doc-template/export-dataset.xlsx'));
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('E2',Carbon::now()->format('Y-m-d'));
        // header
        $sheet->setCellValue('A1', base64_encode($dataset->id));
        $sheet->setCellValue('B1', $dataset->name);
        $sheet->setCellValue('C2', $tahun);
        $sheet->setCellValue('C3', $tw);

        $row=5;
        $sheet->setCellValue(static::cl(0,$row), 'ID_SISTEM');
        $sheet->setCellValue(static::cl(0,$row+1), 'ID_SISTEM');
        $sheet->setCellValue(static::cl(1,$row), 'PERINTAH SISTEM');
        $sheet->setCellValue(static::cl(1,$row+1), 'method');
        $sheet->setCellValue(static::cl(1,$row+2), 0);

        $sheet->setCellValue(static::cl(2,$row), 'STATUS VERIFIKASI');
        $sheet->setCellValue(static::cl(2,$row+1), 'status');
        $sheet->setCellValue(static::cl(2,$row+2), 1);

        $sheet->setCellValue(static::cl(3,$row), 'KODEPEMDA');
        $sheet->setCellValue(static::cl(3,$row+1), 'kodepemda');
        $sheet->setCellValue(static::cl(3,$row+2), 2);

        $sheet->setCellValue(static::cl(4,$row), 'NAMAPEMDA');
        $sheet->setCellValue(static::cl(4,$row+2), 3);
        $select=['dt.id',"'NONE' as ps","(case when status=1 then 'v' else null end) as st",'dt.kodepemda','mp.nama_pemda'];
        $styleH=[
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => [
                    'argb' => '00aaf393',
                ]
            ],
        ];
        $styleNum=[
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => [
                    'argb' => '00d3d3d3',
                ]
            ],
        ];
        $sheet->getStyle('B5:'.static::cl(4,$row+2))->applyFromArray($styleH);
        $sheet->getStyle('B7:'.static::cl(4,$row+2))->applyFromArray($styleNum);
        $colom=0;
        $pilihan_column_khusus=[];
        foreach($colom_schema as $colom=>$c){
            $select[]=$c->field_name;
            $sheet->setCellValue(static::cl($colom+5,$row),$c->name.' - '.$c->satuan);
            $sheet->getStyle(static::cl($colom+5,$row))->applyFromArray($styleH);
            $sheet->setCellValue(static::cl($colom+5,$row+1),$c->field_name);
            $sheet->setCellValue(static::cl($colom+5,$row+2),$colom+5);
            $sheet->getStyle(static::cl($colom+5,$row+2))->applyFromArray($styleNum);
            $sheet->getColumnDimension(static::cl($colom+5))->setWidth(120, 'pt');
            if($c->tipe_nilai=='numeric'){
                $sheet->getStyle(static::cl($colom+5))->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            }
            $c->fungsi_rentang_nilai=json_decode($c->fungsi_rentang_nilai??'{}',true);
            if(isset($c->fungsi_rentang_nilai['tipe'])){
                if($c->fungsi_rentang_nilai['tipe']=='pilihan'){
                    $sc_pil=[];
                    foreach($c->fungsi_rentang_nilai['m'] as $k=>$mp){
                        $sc_pil[base64_encode($mp['val'])]=$mp['tag'];
                    }
                    $pilihan_column_khusus[($colom+5)]=$sc_pil;
                         $validation = $spreadsheet->getActiveSheet()->getCell(static::cl($colom+5,8))
                        ->getDataValidation();
                        $validation->setType( \PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST );
                        $validation->setErrorStyle( \PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION );
                        $validation->setAllowBlank(false);
                        $validation->setShowInputMessage(true);
                        $validation->setShowErrorMessage(true);
                        $validation->setShowDropDown(true);
                        $validation->setErrorTitle('Input error');
                        $validation->setError('Value is not in list.');
                        $validation->setPromptTitle('Pick from list');
                        $validation->setPrompt('Please pick a value from the drop-down list.');
                        $validation->setFormula1('"'.implode(',',$sc_pil).'"');
                }
            }
        }

        $sheet->setCellValue(static::cl($colom+6,$row),'KETERANGAN');
        $sheet->setCellValue(static::cl($colom+6,$row+1),'keterangan');
        $sheet->setCellValue(static::cl($colom+6,$row+2),$colom+6);
        $sheet->getStyle(static::cl($colom+6,$row))->applyFromArray($styleH);
        $sheet->getStyle(static::cl($colom+6,$row+2))->applyFromArray($styleNum);
        $sheet->getColumnDimension(static::cl($colom+6))->setWidth(220, 'pt');



        $select[]='keterangan';




        
        $data=DB::table('dataset.'.$dataset->table.' as dt')->where('dt.tw',$tw)
        ->selectRaw(implode(',',$select))
        ->leftJoin('master.master_pemda as mp','mp.kodepemda','=','dt.kodepemda')
        ->where('dt.tahun',$tahun)->orderBy('dt.kodepemda')->get();

        $row+=3;
        $cd=5;
        $key=0;
        $stborder = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
        ];
        $r=[];
        $sistem_perintah=null;
        foreach($data as $key=>$d){
            $cd=0;
           
            foreach($d as $cdr=>$v){
                if(isset($pilihan_column_khusus[$cd])){
                    $sheet->setCellValue(static::cl($cd,$row+$key), isset($pilihan_column_khusus[$cd][base64_encode($v)])?$pilihan_column_khusus[$cd][base64_encode($v)]:null);
                }else{
                    $sheet->setCellValue(static::cl($cd,$row+$key), $v);
                }
                $cd++;
            }
           

        }
        $sistem_perintah=$sheet->getCell(static::cl(1,($row+0)))
                ->getDataValidation();


        $sistem_perintah->setSqref('B8:'.static::cl(1,count($data)+7));
        foreach($pilihan_column_khusus as $c=>$p){
            $v_d=$sheet->getCell(static::cl($c,8))
                ->getDataValidation();
            $v_d->setSqref(static::cl($c,8).':'.static::cl($c,count($data)+7));
        }

        $stborder = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
        ];
        $sheet->getStyle('B5:'.static::cl(count($select)-1,$row+$key))->applyFromArray($stborder);

        $sheet->setAutoFilter('B7:'.static::cl(count($select)-1,$row+$key));
        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'. urlencode(str_replace(' ','',$dataset->name).'-'.$tahun.'-'.$tw.'-'.Carbon::now()->format('d-m-y')).'.xlsx"');
        $writer->save('php://output');


    }

    public function upload_excel($tahun,$id_dataset,Request $request){
        $dataset=DB::table('master.dataset')->find($id_dataset);
        $id_user=Auth::User()->id;
        $now=Carbon::now();
        if($dataset){
            $colom_schema=DB::table('master.dataset_data as sd')
            ->join('master.data as d','d.id','=','sd.id_data')
            ->selectRaw("d.name,d.satuan,sd.field_name,d.tipe_nilai,d.fungsi_rentang_nilai")
            ->where('sd.id_dataset',$id_dataset)->orderBy('sd.index','asc')->get();
            $cs_r=[];
            foreach($colom_schema as $key=>$cs){
                $cs->fungsi_rentang_nilai=json_decode($cs->fungsi_rentang_nilai??'{}',true);
                if(isset($cs->fungsi_rentang_nilai['tipe'])){
                    if($cs->fungsi_rentang_nilai['tipe']=='pilihan'){
                        $pil=[];
                        foreach($cs->fungsi_rentang_nilai['m'] as $mp){
                            $pil[base64_encode($mp['tag'])]=$mp['val'];
                        }
                        $cs_r[$cs->field_name]=$pil;
                    }
                }
            }
            DB::beginTransaction();
            $data_row=0;
           try{
            $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($request->file);
            $sheet = $spreadsheet->getActiveSheet();
            $rows=$sheet->toArray();
            $id_file=base64_decode($rows[0][0]);
            if($dataset->id==$id_file){
                $field=[];
                foreach($rows[5] as $key=>$f){
                    if($key!=0){
                        if($f and $f!='method'){
                            $field[$f]=$key;
                        }
                    }
                }
                $kod_pem_reg='';
                $index_req=0;

                for($r=7;$r<count($rows);$r++){
                    $data=$field;
                    $d=false;
                    foreach($field as $f=>$rn){
                        if(isset($cs_r[$f])){
                            $data[$f]=isset($cs_r[$f][base64_encode($rows[$r][$rn])])?$cs_r[$f][base64_encode($rows[$r][$rn])]:null;
                        }else{
                            $data[$f]=$rows[$r][$rn];
                        }
                    }
                    if($data['kodepemda']!=$kod_pem_reg){
                        $kod_pem_reg=$data['kodepemda'];
                        $index_req=0;
                    }
                    $index_req++;

                    if(($rows[$r][1]!='NONE') AND  ($rows[$r][1]!='' AND $rows[$r][1]!=NULL) ){
                        $d=true;                        
                        $data['tw']=$rows[2][2];
                        $data['tahun']=$rows[1][2];
                        $data['updated_at']=$now;
                        $data['user_pengisi']=$id_user;
                        $data['index']=$index_req;
                        


                        
                        $data_row+=1;
                        if($dataset->jenis_dataset==0){
                            if($rows[$r][1]=='DELETE' AND $rows[$r][0]!=NULL){
                                DB::table('dataset.'.$dataset->table)->where('id',$rows[$r][0])->delete();
                            }else if($rows[$r][1]=='UPDATE'){
                                $data['status']=0;
                                DB::table('dataset.'.$dataset->table)->updateOrInsert(
                                    $rows[$r][0]!=NULL?['id'=>$rows[$r][0]]:['tahun'=>$data['tahun'],'tw'=>$data['tw'],'kodepemda'=>$data['kodepemda']],
                                    $data
                                );
                            }else if($rows[$r][1]=='VERIFIKASI'){
                                $data['status']=1;
                                if($rows[$r][0]!=NULL){
                                    unset($data['user_pengisi']);
    
                                }
                                $data['user_pengesah']=$id_user;
                                $data['tanggal_pengesahan']=$now;
                                DB::table('dataset.'.$dataset->table)->updateOrInsert(
                                    $rows[$r][0]!=NULL?['id'=>$rows[$r][0]]:['tahun'=>$data['tahun'],'tw'=>$data['tw'],'kodepemda'=>$data['kodepemda']],
                                    $data
                                );
                            }
                        }else{
                            if($rows[$r][1]=='DELETE' AND $rows[$r][0]!=NULL){
                                DB::table('dataset.'.$dataset->table)->where('id',$rows[$r][0])->delete();
                            }else if($rows[$r][1]=='UPDATE'){
                                $data['status']=0;
                                if($rows[$r][0]!=NULL){
                                    DB::table('dataset.'.$dataset->table)->updateOrInsert(
                                        ['id'=>$rows[$r][0]],
                                        $data
                                    );
                                }else{
                                    DB::table('dataset.'.$dataset->table)->insert(
                                        $data
                                    );
                                }
                                
                            }else if($rows[$r][1]=='VERIFIKASI' ){
                                $data['status']=1;
                                $data['user_pengesah']=$id_user;
                                $data['tanggal_pengesahan']=$now;
                                if($rows[$r][0]!=NULL){
                                    unset($data['user_pengisi']);
                                    DB::table('dataset.'.$dataset->table)->updateOrInsert(
                                        ['id'=>$rows[$r][0]],
                                        $data
                                    );
                                }else{
                                    DB::table('dataset.'.$dataset->table)->insert(
                                        $data
                                    );
                                }
                               
                               
                            }
                        }

                    }



                    

                }
            }else{
                
            }
            DB::commit();
           }catch(\Exception $e){
                DB::rollback();
               dd($e);
                Alert::error('','Gagal Melakukan Proccess');
               return back();
           }

          

           Alert::success('','Data Berhasil Dirubah '.$data_row.' rows data');
           return back();
        
        }
    }

    public function index_pusat($tahun){
        
        $dataset=DB::table('master.dataset')->where('pusat',0)
        ->where('status',1)->get();
        foreach($dataset as $key=>$dt){
                $dataset[$key]->link_download=route('dash.si.dataset.download_excel',['tahun'=>$tahun,'id_dataset'=>$dt->id]);
                $dataset[$key]->link_upload=route('dash.si.dataset.upload_excel',['tahun'=>$tahun,'id_dataset'=>$dt->id]);
               
                if($dt->jenis_dataset==0){
                    $data=DB::table('dataset.'.$dt->table." as rt")
                    ->where('tahun',$tahun)
                    ->selectRaw(
                        "
                        count(case when rt.tw=1 then true else null end) as count_data_1,
                        count(case when (rt.status=1 and rt.tw=1) then true else null end) as count_finish_1,
                        max(case when (rt.tw=1) then updated_at else null end ) last_update_data_1,
                        count(case when rt.tw=2 then true else null end) as count_data_2,
                        count(case when (rt.status=1 and rt.tw=2) then true else null end) as count_finish_2,
                        max(case when (rt.tw=2) then updated_at else null end ) last_update_data_2,
                        count(case when rt.tw=3 then true else null end) as count_data_3,
                        count(case when (rt.status=1 and rt.tw=3) then true else null end) as count_finish_3,
                        max(case when (rt.tw=3) then updated_at else null end ) last_update_data_3,
                        count(case when rt.tw=4 then true else null end) as count_data_4,
                        count(case when (rt.status=1 and rt.tw=4) then true else null end) as count_finish_4,
                        max(case when (rt.tw=4) then updated_at else null end ) last_update_data_4
                        "
                    )
                    ->first();
                }else{
                  

                    $data=DB::table(DB::raw("(select 
                    rt.kodepemda,
                    count(case when rt.tw=1 then true else null end) as count_1,
                    count(case when (rt.status=1 and rt.tw=1) then true else null end) as finish_1,
                    max(case when (rt.tw=1) then updated_at else null end ) update_1,
                    count(case when rt.tw=2 then true else null end) as count_2,
                    count(case when (rt.status=1 and rt.tw=2) then true else null end) as finish_2,
                    max(case when (rt.tw=2) then updated_at else null end ) update_2,
                    count(case when rt.tw=3 then true else null end) as count_3,
                    count(case when (rt.status=1 and rt.tw=3) then true else null end) as finish_3,
                    max(case when (rt.tw=3) then updated_at else null end ) update_3,
                    count(case when rt.tw=4 then true else null end) as count_4,
                    count(case when (rt.status=1 and rt.tw=4) then true else null end) as finish_4,
                    max(case when (rt.tw=4) then updated_at else null  end ) update_4 from dataset.".'"'.$dt->table.'"'." as rt where rt.tahun=".$tahun." group by rt.kodepemda) as d"))
                    ->selectRaw("
                    count(case when d.count_1 >0 then  1 else null end) as count_data_1,
                    count(case when (d.count_1 = finish_1) and (d.count_1 >0) then true else null end) as count_finish_1,
                    max(d.update_1)  as last_update_data_1,
                    count(case when d.count_2 >0 then  true else null end) as count_data_2,
                    count(case when d.count_2 = d.finish_2 and d.count_2 >0then true else null end) as count_finish_2,
                    max(d.update_2) as last_update_data_2,
                    count(case when d.count_3 >0 then  true else null end) as count_data_3,
                    count(case when d.count_3 = finish_3 and d.count_3 >0 then true else null end) as count_finish_3,
                    max(d.update_3) as last_update_data_3,
                    count(case when d.count_4 >0 then  true else null end) as count_data_4,
                    count(case when d.count_4 = d.finish_4 and d.count_4>0 then true else null end) as count_finish_4,
                    max(d.update_4) as last_update_data_4
                       
                    ")->first();
                    
                }

            
            $meta=[
                'count_data_1'=>'-',
                'count_finish_1'=>'-',
                'last_update_data_1'=>'-',
                'count_data_2'=>'-',
                'count_finish_2'=>'-',
                'last_update_data_2'=>'-',
                'count_data_3'=>'-',
                'count_finish_3'=>'-',
                'last_update_data_3'=>'-',
                'count_data_4'=>'-',
                'count_finish_4'=>0,
                'last_update_data_4'=>'-',

                'jenis_dataset'=>$dt->jenis_dataset,


            ];

            if($data){

                if($dt->interval_pengambilan==1){
                    $meta['count_data_4']=$data->count_data_4;
                    $meta['count_finish_4']=$data->count_finish_4;
                    $meta['last_update_data_4']=$data->last_update_data_4?Carbon::parse($data->last_update_data_4)->format('d F Y h:i'):'-';
                    $meta['jenis_dataset']=$dt->jenis_dataset;
                    $meta['link_detail']=route('dash.si.dataset_pusat.pilihan_pemda',['tahun'=>$tahun,'id_dataset'=>$dt->id]);

                }else{
                    $meta=[
                        'count_data_1'=>$data->count_data_1,
                        'count_finish_1'=>$data->count_finish_1,
                        'last_update_data_1'=>$data->last_update_data_1?Carbon::parse($data->last_update_data_1)->format('d F Y h:i'):'-',
                        'count_data_2'=>$data->count_data_2,
                        'count_finish_2'=>$data->count_finish_2,
                        'last_update_data_2'=>$data->last_update_data_2?Carbon::parse($data->last_update_data_2)->format('d F Y h:i'):'-',
                        'count_data_3'=>$data->count_data_3,
                        'count_finish_3'=>$data->count_finish_3,
                        'last_update_data_3'=>$data->last_update_data_3?Carbon::parse($data->last_update_data_3)->format('d F Y h:i'):'-',
                        'count_data_4'=>$data->count_data_4,
                        'count_finish_4'=>$data->count_finish_4,
                        'last_update_data_4'=>$data->last_update_data_4?Carbon::parse($data->last_update_data_4)->format('d F Y h:i'):'-',
                        'jenis_dataset'=>$dt->jenis_dataset,
    
                    ];
                    $meta['link_detail']=route('dash.si.dataset_pusat.pilihan_pemda',['tahun'=>$tahun,'id_dataset'=>$dt->id]);

                }
            }
       

        $dataset[$key]->meta=$meta;
       
        }


        return view('sistem_informasi.pusat.dataset.index')->with('dataset',$dataset);

    }

    public function pilihan_pemda_dataset($tahun,$id_dataset){
      
        $dataset=DB::table('master.dataset')->where('status',1)->where('id',$id_dataset)->first();

        if($dataset){
            $scope=DB::table('master.dataset_scope')->where('id_dataset',$id_dataset)->select('kodepemda')->get()->pluck('kodepemda');

            if(count($scope)){

            }else{
                if($dataset->jenis_dataset==0){
                    $pemdas=DB::table('master.master_pemda as mp')
                    ->leftJoin(DB::raw("dataset.".'"'.$dataset->table.'"'.' as rt'),'rt.kodepemda','=','mp.kodepemda')
                    ->selectRaw( "
                        mp.kodepemda,max(mp.nama_pemda) as nama_pemda,
                        count(case when rt.tw=1 then true else null end) as count_data_1,
                        count(case when (rt.status=1 and rt.tw=1) then true else null end) as count_finish_1,
                        max(case when (rt.tw=1) then updated_at else null end ) last_update_data_1,
                        count(case when rt.tw=2 then true else null end) as count_data_2,
                        count(case when (rt.status=1 and rt.tw=2) then true else null end) as count_finish_2,
                        max(case when (rt.tw=2) then updated_at else null end ) last_update_data_2,
                        count(case when rt.tw=3 then true else null end) as count_data_3,
                        count(case when (rt.status=1 and rt.tw=3) then true else null end) as count_finish_3,
                        max(case when (rt.tw=3) then updated_at else null end ) last_update_data_3,
                        count(case when rt.tw=4 then true else null end) as count_data_4,
                        count(case when (rt.status=1 and rt.tw=4) then true else null end) as count_finish_4,
                        max(case when (rt.tw=4) then updated_at else null end ) last_update_data_4
                    ")
                    ->where('mp.kodepemda','!=','0')
                    ->where('rt.tahun',$tahun);
                    
                    $rekap=$pemdas;
                    $pemdas=$pemdas->groupBy('mp.kodepemda')->get();

                }else{

                   $pemdas=DB::table('master.master_pemda as mp')->leftJoin(DB::raw("(select 
                    rt.kodepemda,
                    count(case when rt.tw=1 then true else null end) as count_1,
                    count(case when (rt.status=1 and rt.tw=1) then true else null end) as finish_1,
                    max(case when (rt.tw=1) then updated_at else null end ) update_1,
                    count(case when rt.tw=2 then true else null end) as count_2,
                    count(case when (rt.status=1 and rt.tw=2) then true else null end) as finish_2,
                    max(case when (rt.tw=2) then updated_at else null end ) update_2,
                    count(case when rt.tw=3 then true else null end) as count_3,
                    count(case when (rt.status=1 and rt.tw=3) then true else null end) as finish_3,
                    max(case when (rt.tw=3) then updated_at else null end ) update_3,
                    count(case when rt.tw=4 then true else null end) as count_4,
                    count(case when (rt.status=1 and rt.tw=4) then true else null end) as finish_4,
                    max(case when (rt.tw=4) then updated_at else null end ) update_4 from dataset.".'"'.$dataset->table.'"'." as rt where rt.tahun=".$tahun." group by rt.kodepemda) as d"),'d.kodepemda','=','mp.kodepemda')
                    ->selectRaw("
                        mp.kodepemda,
                        max(mp.nama_pemda) as nama_pemda,
                        count(case when d.count_1 >0 then  1 else null end) as count_data_1,
                        count(case when (d.count_1 = finish_1) and (d.count_1 >0) then true else null end) as count_finish_1,
                        max(d.update_1)  as last_update_data_1,
                        count(case when d.count_2 >0 then  true else null end) as count_data_2,
                        count(case when d.count_2 = d.finish_2 and d.count_2 >0then true else null end) as count_finish_2,
                        max(d.update_2) as last_update_data_2,
                        count(case when d.count_3 >0 then  true else null end) as count_data_3,
                        count(case when d.count_3 = finish_3 and d.count_3 >0 then true else null end) as count_finish_3,
                        max(d.update_3) as last_update_data_3,
                        count(case when d.count_4 >0 then  true else null end) as count_data_4,
                        count(case when d.count_4 = d.finish_4 and d.count_4>0 then true else null end) as count_finish_4,
                        max(d.update_4) as last_update_data_4
                    ")
                    ->where('mp.kodepemda','!=','0')
                    ->orderBy('mp.kodepemda','asc');
                    $pemdas=$pemdas->groupBy('mp.kodepemda')->get();

                    
                }
            }

            $rekap=[
                'count_data_1'=>0,
                'count_finish_1'=>0,
                'count_data_2'=>0,
                'count_finish_2'=>0,
                'count_data_3'=>0,
                'count_finish_3'=>0,
                'count_data_4'=>0,
                'count_finish_4'=>0,
            ];
            foreach($pemdas as $pd){
                $rekap['count_data_1']+=$pd->count_data_1;
                $rekap['count_finish_1']+=$pd->count_finish_1;
                $rekap['count_data_2']+=$pd->count_data_2;
                $rekap['count_finish_2']+=$pd->count_finish_2;
                $rekap['count_data_3']+=$pd->count_data_3;
                $rekap['count_finish_3']+=$pd->count_finish_3;
                $rekap['count_data_4']+=$pd->count_data_4;
                $rekap['count_finish_4']+=$pd->count_finish_4;

            }

        }
        return view('sistem_informasi.pusat.dataset.pemda_list')->with(['pemdas'=>$pemdas,'dataset'=>$dataset,'rekap'=>$rekap]);

    }


    public function detail($tahun,$id_dataset){
        $dataset=DB::table('master.dataset as dts')->where(['id'=>$id_dataset])->first();
        if($dataset){
            $datum=DB::table('master.dataset_data as dt')
            ->leftjoin('master.data as sd','dt.id_data','=','sd.id')
            ->selectRaw("sd.id as id,sd.name,sd.kode,sd.definisi_konsep,
            sd.cara_hitung,sd.methode_pemgambilan,sd.tipe_nilai,
            sd.satuan,dt.field_name as id_list,dt.tahun_data,dt.tw_data,
            sd.arah_nilai,sd.fungsi_aggregasi,sd.fungsi_rentang_nilai,
            dt.aggregasi,dt.title_aggregate,dt.login,
            dt.tw_data as step ,dt.id as id_recorded,concat('data_',sd.id) as id_list")
            ->orderBy('dt.index','asc')
            ->where('dt.id_dataset',$dataset->id)
            ->get();

            $scope=DB::table('master.dataset_scope as s')->where([
                's.id_dataset'=>$id_dataset,
                's.tahun'=>$tahun,
            ])
            ->selectRaw("concat(s.kodepemda,'_',s.tahun) as id_list,p.nama_pemda,p.kodepemda,p.kodepemda_2, ".$tahun." as tahun_data  ")
            ->leftJoin('master.master_pemda as p','p.kodepemda','=','s.kodepemda')->get();

            $themplate=[
                'all'=>[static::shchema_display('NASIONAL')],
                'jenjang'=>[static::shchema_display('NASIONAL'),static::shchema_display('PROVINSI')],
            ];
            $tema=DB::table('master.dataset_tema')->where('id_dataset',$dataset->id)->get()->pluck('id_tema');
            $dataset->menus=$tema;
            $display=DB::table('master.dataset_display')->where('id_dataset',$id_dataset)->get();
            foreach($display as $d){
                if($dataset->display_style==0){
                    if(isset($themplate['all'][(int)$d->tingkat])){
                        $themplate['all'][(int)$d->tingkat]=json_decode($d->schema);
                    }
                }else{
                    if(isset($themplate['jenjang'][(int)$d->tingkat])){
                        $themplate['jenjang'][(int)$d->tingkat]=json_decode($d->schema);
                    }
                }
            }



            return view('sistem_informasi.metadata.detail')->with(['dataset'=>$dataset,'datas'=>$datum,'scope'=>$scope,'themplate'=>$themplate,'menu_options'=>DB::table('master.tema')->get()]);
        }
    }

    public function form_dataset_daerah($tahun,$kodepemda){
        $pemda=DB::table('master.master_pemda')->where('kodepemda',$kodepemda)
        ->orWhere('kodepemda_2',$kodepemda)->first();
        if(!$pemda){
            return abord(404);
        }

        $dataset=DB::table('master.dataset as dts')->whereRaw("
            dts.status=true and ((select count(*) from master.dataset_scope as sc where sc.tahun=".$tahun." and sc.id_dataset=dts.id)=0 OR (select count(*) from master.dataset_scope as sc where sc.tahun=".$tahun." and sc.kodepemda='".$pemda->kodepemda."'  and sc.id_dataset=dts.id)>0 ) 
           ")->get();

          

        foreach($dataset as $key=>$dt){

           
                $data=DB::table('dataset.'.$dt->table." as rt")
                ->where('rt.kodepemda',$kodepemda)
                ->where('tahun',$tahun)
                ->selectRaw(
                    "
                    count(*) as count_data,
                    count(case when rt.status=1 then true else null end) as count_finish,
                    max(tanggal_pengisian) last_update_data
                    "
                )
                ->first();
                $meta=[
                    'count_data'=>0,
                    'count_finish'=>0,
                    'last_update_data'=>null,
                    'max_data'=>$dt->interval_pengambilan==0?4:1,
                    'jenis_dataset'=>$dt->jenis_dataset,
                    'link_detail'=>route('dash.si.dataset_daerah.list',['tahun'=>$tahun,'id_dataset'=>$dt->id,'kodepemda'=>$pemda->kodepemda])


                ];

                if($data){
                    $meta=[
                        'count_data'=>$data->count_data,
                        'count_finish'=>$data->count_finish,
                        'last_update_data'=>Carbon::parse($data->last_update_data)->format('d F Y h:i'),
                        'max_data'=>$dt->interval_pengambilan==0?4:1,
                        'jenis_dataset'=>$dt->jenis_dataset,
                        'link_detail'=>route('dash.si.dataset_daerah.list',['tahun'=>$tahun,'id_dataset'=>$dt->id,'kodepemda'=>$pemda->kodepemda])

                    ];
                }
           

            $dataset[$key]->meta=$meta;

        }

        return view('sistem_informasi.dataset.index')->with(['dataset'=>$dataset,'pemda'=>$pemda]);
    
    }

    public function form_dataset_pemda_detail($tahun,$kodepemda,$id_dataset){
        $pemda=DB::table('master.master_pemda')->where('kodepemda',$kodepemda)
        ->orWhere('kodepemda_2',$kodepemda)->first();
        if(!$pemda){
            return abord(404);
        }
        $dataset=DB::table('master.dataset')->where(['status'=>true,'id'=>$id_dataset])->first();
        if($dataset){
            $dataset_list_form=[];
            if($dataset->interval_pengambilan==0){
                for($tw=1;$tw<=4;$tw++){
                    $meta=[
                        'status'=>null,
                        'user_pengisi'=>0,
                        'keterangan'=>'',
                        'user_pengisi_req'=>'',
                        'tanggal_pengisian'=>null,
                        'tanggal_pengisian_f'=>'Belum Terdapat Record Pengisian Data'

                    ];
                    $d=DB::table('dataset.'.$dataset->table." as rt")
                    ->where('rt.kodepemda',$kodepemda)
                    ->where('tahun',$tahun)
                    ->where('tw',$tw)
                    ->selectRaw(
                        "
                        rt.*,
                        up.name as user_pengisi_req
                        "
                    )
                    ->leftJoin('users as up','up.id','=','rt.user_pengisi')
                    ->first();

                    if($d){
                        if($d->tanggal_pengisian){
                            $d->tanggal_pengisian_f=Carbon::parse($d->tanggal_pengisian)->format('d F Y h:i');
                        }else{
                            $d->tanggal_pengisian_f='Belum Terdapat Record Waktu Pengisian Data';
                        }

                        $meta=json_decode(json_encode($d),true);
                    }
                    $dataset_list_form[]=[
                        'dataset'=>$dataset,
                        'tahun'=>$tahun,
                        'tw'=>$tw,
                        'last_update_data'=>isset($meta['tanggal_pengisian_f'])?$meta['tanggal_pengisian_f']:'Belum Terdapat Record Pengisian Data',
                        'status'=>isset($meta['status'])?$meta['status']:null,
                        'link_detail'=>$meta['status']==1?route('dash.si.dataset_daerah.result_input',['tahun'=>$tahun,'kodepemda'=>$pemda->kodepemda,'id_dataset'=>$dataset->id,'tw'=>$tw]):route('dash.si.dataset_daerah.form_input',['tahun'=>$tahun,'kodepemda'=>$pemda->kodepemda,'id_dataset'=>$dataset->id,'tw'=>$tw]),
                        'link_verifikasi'=>route('dash.si.dataset_daerah.verifikasi_data',['tahun'=>$tahun,'kodepemda'=>$pemda->kodepemda,'id_dataset'=>$dataset->id,'tw'=>$tw]),
                        'meta'=>$meta
                    ];
                }
                return view('sistem_informasi.dataset.detail_pemda')->with(['dataset'=>$dataset,'list_dataset'=>$dataset_list_form,'pemda'=>$pemda]);

            }else{
                $tw=4;
                $meta=[
                    'status'=>null,
                    'user_pengisi'=>0,
                    'keterangan'=>'',
                    'user_pengisi_req'=>'',
                    'tanggal_pengisian'=>null,
                    'tanggal_pengisian_f'=>'Belum Terdapat Record Pengisian Data'

                ];
                $d=DB::table('dataset.'.$dataset->table." as rt")
                ->where('rt.kodepemda',$kodepemda)
                ->where('tahun',$tahun)
                ->where('tw',$tw)
                ->selectRaw(
                    "
                    rt.*,
                    up.name as user_pengisi_req
                    "
                )
                ->leftJoin('users as up','up.id','=','rt.user_pengisi')
                ->first();
             
                if($d){
                    if($d->tanggal_pengisian){
                        $d->tanggal_pengisian_f=Carbon::parse($d->tanggal_pengisian)->format('d F Y h:i');
                    }else{
                        $d->tanggal_pengisian_f='Belum Terdapat Record Waktu Pengisian Data';
                    }

                    $meta=(array)$d;
                }

                if($meta['status']==1){
                    return redirect()->route('dash.si.dataset_daerah.result_input',['tahun'=>$tahun,'kodepemda'=>$pemda->kodepemda,'id_dataset'=>$dataset->id,'tw'=>$tw]);
                }else{
                    if($dataset->jenis_dataset==1){
                        return redirect()->route('dash.si.dataset_daerah.form_input_multy',['tahun'=>$tahun,'kodepemda'=>$pemda->kodepemda,'id_dataset'=>$dataset->id,'tw'=>$tw]);
                    }else{
                        return redirect()->route('dash.si.dataset_daerah.form_input',['tahun'=>$tahun,'kodepemda'=>$pemda->kodepemda,'id_dataset'=>$dataset->id,'tw'=>$tw]);
                    }

                }
             




            }

            
       
        }

    }

    public function form_input_dataset_multy($tahun,$kodepemda,$id_dataset,$tw){
        $pemda=DB::table('master.master_pemda')->where('kodepemda',$kodepemda)
        ->orWhere('kodepemda_2',$kodepemda)->first();
        if(!$pemda){
            return abord(404);
        }
        $dataset=DB::table('master.dataset')->where(['status'=>true,'id'=>$id_dataset])->first();

        if($dataset){
            if($dataset->jenis_dataset==1){
                $data=DB::table('dataset.'.$dataset->table)->where('kodepemda',$pemda->kodepemda)
                ->where('tahun',$tahun)
                ->where('tw',$tw)->get();
                $datums=DB::table('master.dataset_data as dd')
                ->selectRaw("d.*,dd.tahun_data ,dd.tw_data,dd.field_name")
                ->leftJoin('master.data as d','d.id','=','dd.id_data')
                ->where('dd.id_dataset',$dataset->id)
                ->OrderBy('dd.index','asc')->get(); 
                
                foreach($datums as $key=>$datum){
                    $datum->rentang_nilai_fungsi=0;
                    $datum->rentang_nilai_file='[]';
                    $rentang_nilai=$datum->fungsi_rentang_nilai?json_decode($datum->fungsi_rentang_nilai,true):[];
                
                    if(in_array($datum->tipe_nilai,['single-file','multy-file'])){
                        $datum->rentang_nilai_file=json_encode($rentang_nilai['m']);
                    }else{
                        $datum->rentang_nilai_fungsi=isset($rentang_nilai['tipe'])?$rentang_nilai['tipe']:null;
                        $datum->rentang_nilai=isset($rentang_nilai['m'])?$rentang_nilai['m']:[];
                    }
                    $datums[$key]=$datum;
                }
                

                return view('sistem_informasi.dataset.form-multyple')->with(['datums'=>$datums,'pemda'=>$pemda,'dataset'=>$dataset,'values'=>$data,'tw'=>$tw]);
            }
        }

    }

    public function form_input_dataset($tahun,$kodepemda,$id_dataset,$tw){
        $pemda=DB::table('master.master_pemda')->where('kodepemda',$kodepemda)
        ->orWhere('kodepemda_2',$kodepemda)->first();
        if(!$pemda){
            return abord(404);
        }

        
        $dataset=DB::table('master.dataset')->where(['status'=>true,'id'=>$id_dataset])->first();
        if($dataset){
            if($dataset->interval_pengambilan==0){
                if($tw>4 OR $tw<1){
                    return abort(404);
                }
            }

            $datums=DB::table('master.dataset_data as dd')
            ->selectRaw("d.*,dd.tahun_data ,dd.tw_data,dd.field_name")
            ->leftJoin('master.data as d','d.id','=','dd.id_data')
            ->where('dd.id_dataset',$dataset->id)
            ->OrderBy('dd.index','asc')->get();
            
            foreach($datums as $key=>$datum){
                $datum->rentang_nilai_fungsi=0;
                $datum->rentang_nilai_file='[]';
                $rentang_nilai=$datum->fungsi_rentang_nilai?json_decode($datum->fungsi_rentang_nilai,true):[];
            
                if(in_array($datum->tipe_nilai,['single-file','multy-file'])){
                    $datum->rentang_nilai_file=json_encode($rentang_nilai['m']);
                }else{
                    $datum->rentang_nilai_fungsi=isset($rentang_nilai['tipe'])?$rentang_nilai['tipe']:null;
                    $datum->rentang_nilai=isset($rentang_nilai['m'])?$rentang_nilai['m']:[];
                }
                $datums[$key]=$datum;
            }

            $value=DB::table('dataset.'.$dataset->table)->where('kodepemda',$kodepemda)->where('tahun',$tahun)->where('tw',$tw)->first();
            if(!$value){
                $value=[];
                foreach($datums as $key=>$d ){
                    $value[$d->field_name]=null;
                }
            }


            return view('sistem_informasi.dataset.form')->with(['dataset'=>$dataset,'datums'=>$datums,'pemda'=>$pemda,'tw'=>$tw,'values'=>$value]);

        }

    }

    public function form_dataset_submit($tahun,$kodepemda,$id_dataset,$tw,Request $request){
        $dataset=DB::table('master.dataset')->find($id_dataset);
        if($dataset){
            $data=(array)$request->data;
            $data['kodepemda']=$kodepemda;
            $data['tahun']=$tahun;
            $data['tw']=$tw;
            $data['user_pengisi']=Auth::User()->id;
            $data['tanggal_pengisian']=Carbon::now();



            DB::table('dataset.'.$dataset->table)->updateOrInsert([
                'kodepemda'=>$kodepemda,
                'tahun'=>$tahun,
                'tw'=>$tw
            ],$data);


            return back();
        }
    }

    public function index($tahun){
        $dataset=DB::table('master.dataset as d')
        ->selectRaw("
            d.id,
            max(d.name) as name,
            max(d.tujuan) as tujuan,
            (d.jenis_dataset) as jenis_dataset,
            max(d.penangung_jawab) as penangung_jawab,
            (d.status) as status,
            max(d.tanggal_publish) as tanggal_publish,
            count(dt.id) as jumlah_data,
            count(dts.id) as jumlah_pemda_scope,
            d.jenis_distribusi

        ")
        ->leftjoin('master.dataset_data as dt','dt.id_dataset','=','d.id')
        ->leftjoin('master.dataset_scope as dts',[['dts.id_dataset','=','d.id'],['dts.tahun','=',DB::raw($tahun)]])

        ->groupBy('d.id')
        ->orderBy('d.tanggal_publish','asc')->get();
        foreach($dataset as $key=>$d){
            $dataset[$key]->tanggal_publish=Carbon::parse($dataset[$key]->tanggal_publish)->format('d F Y');
            $dataset[$key]->link_detail=route('dash.si.schema.dataset.detail_dataset',['tahun'=>$tahun,'id_dataset'=>$d->id]);
            $dataset[$key]->link_delete=route('dash.si.schema.dataset.delete_dataset',['tahun'=>$tahun,'id_dataset'=>$d->id]);
            $dataset[$key]->link_schema_up=route('dash.si.schema.dataset.schema_up',['tahun'=>$tahun,'id_dataset'=>$d->id]);


        }
        return view('sistem_informasi.metadata.index',['dataset'=>$dataset]);
    }

    public function delete($tahun,$id_dataset){
        $dataset=DB::table('master.dataset as dts')->where(['id'=>$id_dataset])->delete();
        if($dataset){
            Alert::success('','Berhasil Menghapus dataset');
            return back();
        }
    }


    public function get_datum($tahun,Request $request){
        $datum=DB::table('schema_data.datum_schema as datum')->leftJoin('schema_data.sumber_datum as sumber','datum.id_sumber_datum','sumber.id');
        $where=[];
        if($request->q){
            $where[]=['datum.name','=','%'.$request->q.'%'];
        }

        if($request->q){
            $where[]=['sumber.id','=',$request->id_sumber];
        }

        $datum=$datum->limit(10)->get();

        return $datum;
       
    }





    public static function nameVar($var,$m=1){
        if($m==0){
            $var=  preg_replace('/_+/', '_',(preg_replace('/[^\p{L}\p{N}\s]/u', '_',str_replace(' ','_', $var))));

        }else if($m==1){ 
            $var=  preg_replace('/\s+/', ' ',(preg_replace('/[^\p{L}\p{N}\s]/u', ' ', $var)));
            $var=str_replace(' ','',ucwords(strtolower(str_replace('_',' ',$var))));
        }

        return $var;

    }

    public static function migrate_dataset($file){
        Artisan::call('migrate --path=database/dataset_migrations/'.$file.' ');
        DB::table('public.migrations')->where('migration',str_replace('.php','',$file))->delete();
        
    }

    static function build_schema($tahun,$table,$datum,$jenis=0){
        $index_aggre=array_filter($datum,function($el){return $el['aggregasi']!=null and $el['aggregasi']!='0';});
        $index_aggre=array_keys($index_aggre);
        $file='<?php


        use Illuminate\Database\Migrations\Migration;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Support\Facades\Schema;
        
        class '.static::nameVar($table).' extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {';
                $file.= "
                    if(!Schema::hasTable('dataset.".$table."')){
                        Schema::create('dataset.".$table."',function(Blueprint $"."table){
                        $"."table->id();
                        $"."table->integer('index')->nullable()->default(0)->index();
                        $"."table->string('kodepemda',5);
                        $"."table->integer('tahun')->index();
                        $"."table->integer('tw')->index();
                        $"."table->integer('status')->default(0)->index();
                        $"."table->float('point')->default(0);            
                        ";
        
                        $file.="
                        $"."table->mediumText('keterangan')->nullable();
                        ".
                        "$"."table->bigInteger('user_pengisi')->unsigned();
                        ".
                        "$"."table->dateTime('tanggal_pengisian')->nullable();
                        ".
                        "$"."table->bigInteger('user_pengesah')->unsigned()->nullable();
                        ".
                        "$"."table->dateTime('tanggal_pengesahan')->nullable();
                        ".
                        "$"."table->bigInteger('user_pengesah_2')->unsigned()->nullable();
                        ".
                        "$"."table->dateTime('tanggal_pengesahan_2')->nullable();
                        ".
                        "$"."table->bigInteger('user_pengesah_3')->unsigned()->nullable();
                        ".
                        "$"."table->dateTime('tanggal_pengesahan_3')->nullable();
                        ".
                        "$"."table->dateTime('updated_at')->nullable();
                        ;";
                        foreach($datum as $d){
                            if($d['type']=='numeric'){
                                $file.="$"."table->double('".$d['field_name']."',25,3)->nullable()->comment('".$d['id_data'].'-'.substr($d['name'],0,25).'-'.$d['satuan'].'- tahun '.$d['tahun_data'].'- tw '.$d['tw_data']."');
                            ";
                            }elseif($d['type']=='string' OR ($d['type']=='single-file')){
                                $file.="$"."table->string('".$d['field_name']."')->nullable()->comment('".$d['id_data'].'-'.substr($d['name'],0,25).'-'.$d['satuan'].'- tahun '.$d['tahun_data'].'- tw '.$d['tw_data']."');
                            ";
                            }elseif($d['type']=='text' OR ($d['type']=='multy-file')){
                                $file.="$"."table->mediumText('".$d['field_name']."')->nullable()->comment('".$d['id_data'].'-'.substr($d['name'],0,25).'-'.$d['satuan'].'- tahun '.$d['tahun_data'].'- tw '.$d['tw_data']."');
                            ";
                            }
                            
                        }
                        if($jenis==0){
                            $file.="
                            $"."table->unique(['kodepemda','tahun','tw']);
                            $"."table->index(['kodepemda','tahun','tw','status']);
                            ";
                        }else{
                            

                        }

                        $file.="});

                    }

                    if(Schema::hasTable('dataset.".$table."')){
                        Schema::table('dataset.".$table."',function (Blueprint $"."table){
                            ";
                            $file.="if(!Schema::hasColumn('dataset.".$table."', 'updated_at')) {
                                $"."table->dateTime('updated_at')->nullable();
                            }
                            ";
                            foreach($datum as $d){
                                $file.="if(!Schema::hasColumn('dataset.".$table."', '".$d['field_name']."')) {
                                ";
                                if($d['type']=='numeric'){
                                    $file.="$"."table->double('".$d['field_name']."',25,3)->nullable()->comment('".$d['id_data'].'-'.substr($d['name'],0,25).'-'.$d['satuan'].'- tahun '.$d['tahun_data'].'- tw '.$d['tw_data']."');";
                                }elseif($d['type']=='string' OR ($d['type']=='single-file')){
                                    $file.="$"."table->string('".$d['field_name']."')->nullable()->comment('".$d['id_data'].'-'.substr($d['name'],0,25).'-'.$d['satuan'].'- tahun '.$d['tahun_data'].'- tw '.$d['tw_data']."');";
                                }elseif($d['type']=='text' OR ($d['type']=='multy-file')){
                                    $file.="$"."table->mediumText('".$d['field_name']."')->nullable()->comment('".$d['id_data'].'-'.substr($d['name'],0,25).'-'.$d['satuan'].'- tahun '.$d['tahun_data'].'- tw '.$d['tw_data']."');";
                                }
                            $file.="
                                }
                            ";
                            }
                            $file.="
                        });
                        ";
                        foreach($index_aggre as $i){
                            $file.="DB::statement('CREATE INDEX IF NOT EXISTS ".$i.'_idx'." ON dataset.".'"'.$table.'"'." USING btree (".$i.")');
                            ";
                        }
                        $file.="
                    }
                    
            }
                
            
            
            public function down(){
                
                   
                
            }
            
        }";
        
        $file_schema_named=date('Y_m_d_his').'_'.$table.'.php';
        Storage::disk('dataset')->put($file_schema_named,$file);
        return [
            'name'=>$file_schema_named,
            'path'=>Storage::disk('dataset')->url($file_schema_named)

        ];
    }

    public function tambah($tahun){
        $schema=[
            'all'=>[static::shchema_display('NASIONAL')],
            'jenjang'=>[static::shchema_display('NASIONAL'),static::shchema_display('PROVINSI')]
        ];
        

         return view('sistem_informasi.metadata.tambah')->with(['themplate'=>$schema,'menu_options'=>DB::table('master.tema')->get()]);
    }

    public function store($tahun,Request $request){

        if($request->datas){
            $request['datas']=json_decode($request->datas,true);
        }
        if($request->kategori){
            $request['kategori']=json_decode($request->kategori,true);
        }


        if($request->metalinks){
            $request['metalinks']=json_decode($request->metalinks);
        }

        if($request->menus){
            $request['menus']=json_decode($request->menus);
        }

        if($request->scope){
            $request['scope']=json_decode($request->scope);
        }

        if($request->display){
            $request['display']=json_decode($request->display);
        }


        $valid=Validator::make($request->all(),[
            'datas'=>'array|required',
            'interval_pengambilan'=>'required|numeric',
            'status'=>'required||numeric',
            'name'=>'required|string',
            'tujuan'=>'required|string',
            'penangung_jawab'=>'required|string',


        ]);



        if($valid->fails()){
            Alert::error('',$valid->errors()->first());
            return back()->withInput();
        }

        $table_name=strtolower(static::nameVar('dataset_'.$request->name.'  '.$request->penangung_jawab));
        $table_name_= substr($table_name, 0, 20);
        do{
           $texist= DB::table('master.dataset')->where('table',$table_name_)->first();
           if($texist){
                $table_name_=$table_name.'_'.rand(0,5);
           }
        }while($texist);

        if($table_name!=$table_name_){
            $table_name=$table_name_;
        }


       
        $date=Carbon::now();
        $user=Auth::User();
            $columns=[];

        DB::beginTransaction();



        try {
            $id_dataset=DB::table('master.dataset')->insertGetId([
                'name'=>$request->name,
                'display_style'=>$request->display->style,
                'id_menu'=>$request->id_menu,
                'login'=>$request->login??false,
                'pusat'=>$request->pusat??0,
                'kategori'=>json_encode($request->kategori??[]),
                'table'=>$table_name,
                'point'=>$request->point??0,
                'penangung_jawab'=>$request->penangung_jawab,
                'tujuan'=>$request->tujuan,
                'methode_pengambilan'=>$request->methode_pengambilan,
                'interval_pengambilan'=>$request->interval_pengambilan,
                'jenis_dataset'=>$request->jenis_dataset==1?true:false,
                'jenis_distribusi'=>$request->jenis_distribusi,
                'tanggal_publish'=>$request->tanggal_publish,
                'status'=>$request->status,
                'id_u_created'=>$user->id,
                'id_u_updated'=>$user->id,
                'updated_at'=>$date,
                'created_at'=>$date,
            ]);

            

         if($id_dataset){
             $scope=array_map(function($el){return $el->kodepemda;},$request->scope);
             $scope_insert=array_map(function($el)use ($id_dataset,$tahun, $user,$date){return ['kodepemda'=>$el->kodepemda,'id_dataset'=>$id_dataset,
                'tahun'=>$tahun,'id_u_created'=>$user->id,'id_u_updated'=>$user->id,
                'created_at'=>$date,
                'updated_at'=>$date
            ];},$request->scope);

                foreach($request->menus??[] as $id_menu){
                    DB::table('master.dataset_tema')->insertOrIgnore([
                        'id_dataset'=>$id_dataset,
                        'id_tema'=>$id_menu
                    ]);
                }

                if(count($request->menus)){
                    DB::table('master.dataset_tema')->where('id_dataset',$id_dataset)->whereNotIn('id_tema',$request->menus)->delete();
                }
            
             if(count($scope)){
                DB::table('master.dataset_scope')->insert($scope_insert);
                // DB::table('master.dataset_scope')->where('tahun',$tahun)->where('id_dataset',$id_dataset)->whereNotIn('kodepemda',$scope)->delete();
             }
           

            
            foreach($request->datas as $key=>$d){
                $k=false;
                
                if(!DB::table('master.dataset_data')->where([
                    ['id_data','=',$d['id']],
                    ['id_dataset','=',$id_dataset],
                    ['tahun_data','=',$d['tahun_data']],
                    ['tw_data','=',$d['tw_data']],
                ])->first()){
                    $k=DB::table('master.dataset_data')->insertGetId([
                        'id_data'=>$d['id'],
                        'id_dataset'=>$id_dataset,
                        'tahun_data'=>$d['tahun_data'],
                        'field_name'=>$d['id_list'],
                        'tw_data'=>$d['tw_data'],
                        'aggregasi'=>$d['aggregasi'],
                        'login'=>$d['login'],
                        'title_aggregate'=>$d['title_aggregate'],
                        'index'=>$key,
                        'id_u_created'=>$user->id,
                        'id_u_updated'=>$user->id,
                    ]);
                }

                if($k){
                    $columns[$d['id_list']]=[
                        'id_data'=>$d['id'],
                        'name'=>$d['name'],
                        'satuan'=>$d['satuan'],
                        'id_dataset'=>$id_dataset,
                        'tahun_data'=>$d['tahun_data'],
                        'aggregasi'=>$d['aggregasi'],
                        'login'=>$d['login'],
                        'title_aggregate'=>$d['title_aggregate'],
                        'field_name'=>$d['id_list'],
                        'type'=>$d['tipe_nilai'],
                        'tw_data'=>$d['tw_data']
                    ];
                }

                
            }


            $display=static::build_sort_query($request['display'],(Object)['id'=>$id_dataset,'table'=>$table_name]);
            foreach($display as $key=>$ds){
                $data_q=[]; 
                for($v=0;$v<4;$v++){
                    $dsc=$ds;
                    unset($dsc['schema']);
                    $data_q['short_query_'.($v+1)]=isset($dsc[$v]['judul'])?json_encode($dsc[$v]):null;
                }
                $data_q['schema']=json_encode($request->display->schema[$key]);
                $data_q['id_dataset']=$id_dataset;
                $data_q['tingkat']=$key;
                DB::table('master.dataset_display')->updateOrInsert([
                    'id_dataset'=>$id_dataset,
                    'tingkat'=>$key,
                ],
                $data_q
                );

            }

         }


        
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
            Alert::error('','Gagal Merubah Dataset');
            return back()->withInput();

        }

        try{
            $schema=static::build_schema($tahun,$table_name,$columns,$request->jenis_dataset);
            static::migrate_dataset($schema['name']);
        }catch(\Exception $e){
            DB::rollback();
            dd($e);
            Alert::error('','Gagal Merubah Dataset');
            return back()->withInput();
        }
        

        if($request->status){            
        
        }

        DB::commit();


        
        Alert::success('','Berhasil Menambahkan Dataset');
        return redirect()->route('dash.si.schema.dataset.index',['tahun'=>$tahun]);
        

   
    }

    public function form_dataset_submit_multy_row($tahun,$kodepemda,$id_dataset,$tw,Request $request){

            $ids=[];
            $dataset=DB::table('master.dataset')->find($id_dataset);
            $user=Auth::User();
            $date=Carbon::now();
            if($dataset){
                foreach ($request->values??[] as $key => $v) {
                    $v['user_pengisi']=$user->id;
                    $v['tanggal_pengisian']=$date;
                    $v['kodepemda']=$kodepemda;
                    $v['tw']=$tw;
                    $v['tahun']=$tahun;

                    if($v['id']){
                        $ids[]=$v['id'];
                        DB::table('dataset.'.$dataset->table)->updateOrInsert([
                                'id'=>$v['id'],
                                'kodepemda'=>$kodepemda,
                                'tw'=>$tw,
                                'tahun'=>$tahun,
                            ],
                            $v
                        );

                    }else{
                        
                        unset($v['id']);
                        $newid=DB::table('dataset.'.$dataset->table)->insertGetId($v);

                        $ids[]=$newid;
                    }

                }
                DB::table('dataset.'.$dataset->table)->where([
                    'kodepemda'=>$kodepemda,
                    'tahun'=>$tahun,
                    'tw'=>$tw
                ])->whereNotIn('id',$ids)->delete();
            }

           

            Alert::success('','Berhasil Meperbarui Data');
            return back();
            

    }

    public function schema_up($tahun,$id_dataset){
        $dataset=DB::table('master.dataset')->find($id_dataset);

        if($dataset){
            return view('sistem_informasi.metadata.schema_up')->with(['dataset'=>$dataset]);
        }

    }

}

