<?php

namespace App\Http\Controllers\SistemInformasi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
class RekapCtrl extends Controller
{
    //

    static function filter($tahun,$filter,$basis,$where_extends=[]){
        $where=[['d.tahun','=',$tahun]];
        switch($filter){
            case "kota":
                $where=[['mp.stapem','=',2]];
            break;
            case "kab":
                $where=[['mp.stapem','=',3]];

            break;
            case "provinsi":
                $where=[['mp.stapem','=',1]];
            break;
        }
        switch(strtolower($basis)){
            case 'longlist':
            $where[]=['mp.tahun_proyek','>',0];

            break;
            case 'sortlist':
            $where[]=['mp.tahun_proyek','=',$tahun];
            break;
        }

        if(count($where_extends)){
            foreach($$where_extends as $w){
                $where[]=$w;
            }
        }

        return $where;

    }

    public function index($tahun,Request $request){

    
        return view('sistem_informasi.rekap.index')->with('basis',$request->basis??'nasional');
    }

    public function load($tahun,Request $request){
        $table=DB::table('master.dataset')->get();
        $data=[];
        $table[]=(Object)[
            'name'=>'RKPD',
            'tujuan'=>'',
            'table'=>'program_kegiatan_rkpd',
            'jenis_dataset'=>0,
            'interval_pengambilan'=>1,
        ];
        if($tahun>=2022){
            $table[]=(Object)[
                'name'=>'Anggaran APBD Daerah',
                'tujuan'=>'',
                'table'=>'master_dpa_'.$tahun,
                'jenis_dataset'=>0,
                'interval_pengambilan'=>1,
            ];
        }
        foreach($table as $d){
            $def=[
                'data'=>[],
                'name'=>$d->name,
                'tahun'=>$tahun,
                'table'=>$d->table,
                'tujuan'=>$d->tujuan,
                'jenis_dataset'=>$d->jenis_dataset,
                'interval_pengambilan'=>$d->interval_pengambilan
            ];

            $qu=DB::table('dataset.'.$d->table.' as d' )
            ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
            ->selectRaw("
            d.tw,
            count(*) as count_row,
            count(distinct(mp.kodepemda)) as jumlah_pemda,
            count(distinct(case when status=0 then mp.kodepemda else null end))  as jumlah_pemda_not_ver
            ")->groupBy('d.tw');
            $where=static::filter($tahun,$request->filter,$request->basis);
            if(count($where)){
                $qu=$qu->where($where);
            }
            // $qu=$qu->where('d.tw',4);
            $qu=$qu->first();

           

            $def['data']=$qu??(object)['count_row'=>0,'jumlah_pemda'=>0,'jumlah_pemda_not_ver'=>0,'tw'=>1];
            $def['data']->jumlah_pemda_ver=$def['data']->jumlah_pemda-$def['data']->jumlah_pemda_not_ver;

            $data[]=$def;

        }

        return $data;
    }

    public function load_detail($tahun,Request $request){
        $qu=DB::table('dataset.'.$request->table.' as d' )
        ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
        ->selectRaw("
        d.tw,
        count(*) as count_row,
        count(distinct(mp.kodepemda)) as jumlah_pemda,
        count(distinct(case when status=0 then mp.kodepemda else null end))  as jumlah_pemda_not_ver
        ")->groupBy('d.tw');
        $where=static::filter($tahun,$request->filter,$request->basis);
        if(count($where)){
            $qu=$qu->where($where);
        }
        $qu=$qu->first();
        $rekap=$qu??(object)['count_row'=>0,'jumlah_pemda'=>0,'jumlah_pemda_not_ver'=>0,'tw'=>1];
        $rekap->jumlah_pemda_ver=$rekap->jumlah_pemda-$rekap->jumlah_pemda_not_ver;
        $data=DB::table('dataset.'.$request->table.' as d' )
        ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
        ->selectRaw("
            mp.kodepemda,
            max(mp.nama_pemda) as namapemda,
            min(d.status) as status
        ")->groupBy('mp.kodepemda')->orderBy('mp.kodepemda','asc');
        if(count($where)){
            $data=$data->where($where);
        }

        $data=$data->where('d.tw',$rekap->tw);
        $data=$data->get();

        return [
            'data'=>$data,
            'rekap'=>$rekap,
            'date'=>Carbon::now()->format('d F Y')
        ];


    }
}
