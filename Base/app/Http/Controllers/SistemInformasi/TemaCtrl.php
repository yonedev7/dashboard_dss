<?php

namespace App\Http\Controllers\SistemInformasi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Alert;
use Validator;
class TemaCtrl extends Controller
{
    //

    public function index($tahun){
        $data=DB::table('master.tema')->orderBy('index','asc')->get();
        return view('sistem_informasi.tema.index')->with('data',$data);
    }
    public function create($tahun){
        return view('sistem_informasi.tema.create');
    }

    public function update($tahun,$id_data,Request $request){
        $data=DB::table('master.tema')->find($id_data);
        if($data){
            $a=DB::table('master.tema')->where('id',$id_data)->update([
                'name'=>$request->name,
                'menu_group'=>$request->menu_group,
                'keterangan'=>$request->keterangan
            ]);
            if($a){
                Alert::success('','Data Berhasil Di perbarui');
            }
            return back();
        }
        
    }

    public function restructure($tahun,Request $request){
        foreach($request->data??[] as $key=>$data){
            $a=DB::table('master.tema')->where('id',$data->id)->update(['index'=>1+$key]);
        }
        Alert::success('','Data Berhasil Di perbarui');
        return back();
    }

    public function store($tahun,Request $request){

            $valid=Validator::make($request->all(),[
                'name'=>'required',
                
            ]);

            if($valid->fails()){
                Alert::error('',$valid->errors()->first());
                return back()->withInput();
            }

            $a=DB::table('master.tema')->insert([
                'name'=>$request->name,
                'index'=>10,
                'keterangan'=>$request->keterangan,
                'menu_group'=>$request->menu_group,
            ]);

            if($a){
                Alert::success('','Data Berhasil Di Tambahkan');
            }
            return redirect()->route('dash.si.menu.index',['tahun'=>$tahun]);
    }

    public function detail($tahun,$id_data){
        $data=DB::table('master.tema')->find($id_data);
        if($data){
            return view('sistem_informasi.tema.detail')->with('data',$data);
        }
    }

    public function delete($tahun,$id_data){
        $data=DB::table('master.tema')->where('id',$id_data)->delete();
        if($data){
            Alert::success('','Data Berhasil Dihapus');
        }else{

        }
        return back();
    }
}
