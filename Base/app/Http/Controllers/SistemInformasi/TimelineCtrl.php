<?php

namespace App\Http\Controllers\SistemInformasi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Auth;
use Validator;
class TimelineCtrl extends Controller
{
    //

    static function load_child($data_v,$kode,$tahun){
        $ss=DB::table('master.timeline')->selectRaw("
                kode as id,
                replace(kode,right(kode,3),'') as parent,
                id as id_data,
                concat  (kode,' - ',kegiatan) as name,persentase_pelaksanaan::float as completed,catatan_pelaksanaan,start,".'"end"'.",keterangan_tugas,kode,id_parent,
                replace('".route('api-web.public.timeline.update',['tahun'=>$tahun,'id'=>'xxx'])."','xxx',id::text) as link_update,
                replace('".route('api-web.public.timeline.delete',['tahun'=>$tahun,'id'=>'xxx'])."','xxx',id::text) as link_delete,
                replace('".route('api-web.public.timeline.add_child',['tahun'=>$tahun,'id'=>'xxx'])."','xxx',id::text) as link_add_child
                ")
                ->where('kode','ilike',$kode.'%')->where('kode','!=',$kode)->orderBy('id','asc')->orderBy('id_parent','asc')->get()->toArray();
            foreach ($ss as $key => $value) {
                # code...
                if($key==0){
                    $ss[$key]->parent=null;
                }
                $kode_dependen=explode('.',$value->kode);
                array_pop($kode_dependen);
                foreach($kode_dependen as $ky=>$k){
                    if($ky==1){
                        $kode_dependen[1]=$kode_dependen[0].'.'.$k;
                    }else if($ky>1){
                        $kode_dependen[$ky]=$kode_dependen[$ky-1].'.'.$k;
                    }
                }
                array_shift($kode_dependen);
                $ss[$key]->dependency=$kode_dependen;
                $ss[$key]->start=Carbon::parse($value->start)->setTimezone('UTC +7')->startOfDay();
                $ss[$key]->end=Carbon::parse($value->end)->setTimezone('UTC +7')->endOfDay();
                $child=DB::table('master.timeline')->where('kode','ilike',$kode.'%')->where('kode','!=',$kode)->count();
                
             }

        return array_merge($data_v,$ss);
    }

    public function load_data($tahun){
        $data=DB::table('master.timeline')->whereNull('id_parent')->get();
        $data_series=[];
        foreach ($data as $key => $d) {
            if(!isset($data_series[$d->id])){
                $data_series[$d->id]=[
                    'name'=>$d->kegiatan,
                    'id'=>$d->id,
                    'persentase_pelaksanaan'=>$d->persentase_pelaksanaan??0,
                    'catatan_pelaksanaan'=>$d->catatan_pelaksanaan??'',
                    'data'=>[]
                ];

                
            }
            $ss=static::load_child([],$d->kode,$tahun);

            $data_series[$d->id]['data']=$ss;
              
        }
        return array_values($data_series);
    }
    
    public function index($tahun){
        $date=Carbon::now();

        return view('sistem_informasi.timeline.index')->with(['date'=>$date]);

    }
    static function number_kode($id,$length){
        $kode=$id."";
        $prefix="";
        for($i=0;$i<($length-strlen($kode));$i++){
            $prefix.='0';
        }
        return $prefix.$kode;
    }

    public function store_child($tahun,$id,Request $request){
        $valid=Validator::make($request->all(),[
            'start'=>'date|required',
            'end'=>'date|required',
            'kegiatan'=>'string',
            'id_parent'=>'numeric'
        ]);

        $u=Auth::guard('api')->User();

        if($valid->fails()){
            return [];
        }

       $id= DB::table('master.timeline')->insertGetId([
            'kegiatan'=>$request->kegiatan,
            'keterangan_tugas'=>$request->keterangan_tugas,
            'start'=>$request->start,
            'end'=>$request->end,
            'id_parent'=>$id,
            'id_user'=>$u->id
        ]);
        $parent=DB::table('master.timeline')->find($request->id_parent);
        $kodeparent=$parent->kode;
        $kode=$kodeparent.'.'.static::number_kode($id,2);
        DB::table('master.timeline')->where('id',$id)->update(['kode'=>$kode]);
        
        static::valid_time($kode);
     

        return [];

    }

    static function valid_time($kode){
        $kode=explode('.',$kode);
        $kode_global=$kode;
        foreach($kode as $k){
             array_pop($kode_global);
            $k=implode('.',($kode_global));
            $lk=strlen($k)+3;
            $timemax=DB::table('master.timeline')->where('kode','ilike',$k.'%')->where('kode','!=',$k)->selectRaw('min(start) as start,max("end") as end,avg(case when(length(kode)='.$lk.') then persentase_pelaksanaan else null end ) as persentase_pelaksanaan')->first();
            DB::table('master.timeline')->where('kode',$k)->update([
                'start'=>$timemax->start,
                'end'=>$timemax->end,
                'persentase_pelaksanaan'=>$timemax->persentase_pelaksanaan
            ]);

        }
    }

    public function store($tahun,Request $request){
        $valid=Validator::make($request->all(),[
            'start'=>'date|required',
            'end'=>'date|required',
            'kegiatan'=>'string'
        ]);
        $u=Auth::guard('api')->User();

        if($valid->fails()){
            return [];
        }

       $id= DB::table('master.timeline')->insertGetId([
            'kegiatan'=>$request->kegiatan,
            'keterangan_tugas'=>$request->keterangan_tugas,
            'start'=>$request->start,
            'end'=>$request->end,
            'id_user'=>$u->id
        ]);
        $kode=static::number_kode($id,2);
        DB::table('master.timeline')->where('id',$id)->update(['kode'=>$kode]);



        $id2= DB::table('master.timeline')->insertGetId([
            'kegiatan'=>$request->kegiatan,
            'id_parent'=>$id,
            'keterangan_tugas'=>$request->keterangan_tugas,
            'start'=>$request->start,
            'end'=>$request->end,
            'id_user'=>$u->id
        ]);

        $kode=$kode.'.'.static::number_kode($id2,2);

        DB::table('master.timeline')->where('id',$id2)->update(['kode'=>$kode]);


        return [];
    }

    public function update($tahun,$id,Request $request){
        $data=$request->all();


        $data=DB::table('master.timeline')->where('id',$id)->update($data);
        $data=DB::table('master.timeline')->where('id',$id)->first();
        if($data){
           static::valid_time($data->kode);
        }
        return null;
        
    }
    public function delete($tahun,$id,Request $request){
        
    }
}
