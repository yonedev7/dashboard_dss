<?php

namespace App\Http\Controllers\Slack;


use Spatie\SlashCommand\Request;
use Spatie\SlashCommand\Response;
use Spatie\SlashCommand\Handlers\BaseHandler;
use App\Http\Controllers\Controller;
use Spatie\SlashCommand\Attachment;
use Spatie\SlashCommand\AttachmentField;
use Spatie\SlashCommand\AttachmentAction;
use DB;
class ListData extends BaseHandler
{
    public function canHandle(Request $request): bool
    {
        return true;
    }

    /**
     * Handle the given request. Remember that Slack expects a response
     * within three seconds after the slash command was issued. If
     * there is more time needed, dispatch a job.
     * 
     * @param \Spatie\SlashCommand\Request $request
     * 
     * @return \Spatie\SlashCommand\Response
     */
    public function handle(Request $request): Response
    {

        if($request->command=='status-data' ){
            $mes=[];
            $dataset=DB::table('master.dataset')->where('id_menu','!=',null)->orderBy('id_menu','asc')->get();
            return $this->respondToSlack('')
            ->withAttachment(Attachment::create()
                ->setColor('good')
                ->setText('List Dataset!')
                ->setFallback('/satatus-data good-message')
                ->setCallbackId('good-1')
                ->addAction(AttachmentAction::create('cool button', 'A Cool Button', 'button'))
            );

           

        }else{
           
        }
        
        return $this->respondToSlack("Hodor, hodor...".$request->text.' '.json_encode($request->all()));
    }
}
