<?php

namespace App\Http\Controllers\Tema;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use AUth;
use Validator;

// perencanaan penganggaran

class Tema1Ctrl extends Controller
{
    //

    // rpjmn_rpjmd
    static $basis='pemda';
    static $filter='p_n_k';


    


    public function anggaran($tahun,Request $request){
        $basis='nasional';
        if($request->basis){
            $basis=$request->basis;
        }

        return view('tematik.tema1.anggaran')->with(['basis'=>$basis]);

    }

    public function anggaran_program($tahun,Request $request){
        if($tahun>=2021){
            $data=DB::table('dataset.master_dpa_'.$tahun.' as d')
            ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
            ->selectraw("sum(d.total_harga) as y, (max(replace(replace(nama_program,'KABUPATEN/KOTA',''),'PROVINSI',''))) as name")
            ->groupBy(DB::raw("kode_program"));
    
            $where=static::filter($tahun,$request->filter,$request->basis);
            if(count($where)){
                $data->where($where);
            }
            $data=$data->get();
            foreach($data as $key=>$d){
                $data[$key]->y=(float)$d->y;
            }

        }else{
            $data=[];
        }
      

        return $data;

    }


    public function anggaran_kegiatan($tahun,Request $request){
        if($tahun>=2021){
            $data=DB::table('dataset.master_dpa_'.$tahun.' as d')
            ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
            ->selectraw("sum(d.total_harga) as y, (max(replace(replace(nama_giat,'KABUPATEN/KOTA',''),'PROVINSI',''))) as name")
            ->groupBy(DB::raw("d.kode_giat"))
            ->where('d.kode_program','ilike','1.03.03');

    
            $where=static::filter($tahun,$request->filter,$request->basis);
            if(count($where)){
                $data->where($where);
            }
            $data=$data->get();
            foreach($data as $key=>$d){
                $data[$key]->y=(float)$d->y;
            }

        }else{
            $data=[];
        }
      

        return $data;

    }

    public function anggaran_subkegiatan($tahun,Request $request){
        if($tahun>=2021){
            $data=DB::table('dataset.master_dpa_'.$tahun.' as d')
            ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
            ->selectraw("sum(d.total_harga) as y, d.kode_sub_kegiatan as  kodesubkegiatan, (max(replace(replace(nama_sub_kegiatan,'KABUPATEN/KOTA',''),'PROVINSI',''))) as name")
            ->groupBy(DB::raw("d.kode_sub_kegiatan"))
            ->where('d.kode_program','ilike','1.03.03');
            $where=static::filter($tahun,$request->filter,$request->basis);
            if(count($where)){
                $data->where($where);
            }
            $data=$data->get();
            foreach($data as $key=>$d){
                $data[$key]->y=(float)$d->y;
            }

        }else{
            $data=[];
        }
      

        return $data;

    }

    public function anggaran_subgiat_pemda_1($tahun,Request $request){
        $pemda=DB::table('master.master_pemda')->where('kodepemda',$request->kodepemda)->selectRaw("
            kodepemda as kode,
            nama_pemda as namapemda
        ")->first();
        if($tahun>=2021){
            $data=DB::table('dataset.master_dpa_'.$tahun.' as d')
            ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
            ->selectraw("
            sum(total_harga) as y,
            max(d.nama_akun_1) as name,
            max(d.kodepemda) as kodepemda,
            max(d.tahun) as tahun,
            max(d.kode_sub_kegiatan) as kodesub,
            d.kode_akun_1 as kodeakun
            ")->groupBy('d.kode_akun_1')->where('d.kodepemda',$request->kodepemda);
            $where=static::filter($tahun,$request->filter,$request->basis);
            if(count($where)){
                $data->where($where);
            }
            $data=$data->where('kode_sub_kegiatan','ilike',$request->kode.'%');
            $data= $data->get();

        }else{
            $data=[];
        }

        return [
            'pemda'=>$pemda,
            'data'=>$data
        ];


    }
    public function anggaran_subgiat_pemda_2($tahun,Request $request){
        $pemda=DB::table('master.master_pemda')->where('kodepemda',$request->kodepemda)->selectRaw("
            kodepemda as kode,
            nama_pemda as namapemda
        ")->first();
        if($tahun>=2021){
            $data=DB::table('dataset.master_dpa_'.$tahun.' as d')
            ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
            ->selectraw("
            sum(total_harga) as y,
            max(d.nama_akun_2) as name,
            max(d.kodepemda) as kodepemda,
            max(d.tahun) as tahun,
            max(d.kode_sub_kegiatan) as kodesub,
            d.kode_akun_2 as kodeakun
            ")->groupBy('d.kode_akun_2')->where('d.kodepemda',$request->kodepemda);
            $where=static::filter($tahun,$request->filter,$request->basis);
            if(count($where)){
                $data->where($where);
            }
            $data=$data->where('kode_sub_kegiatan','ilike',$request->kode.'%');
            $data=$data->where('d.kode_akun_1','ilike',$request->kodeakun.'%');
            $data= $data->get();

        }else{
            $data=[];
        }

        return [
            'pemda'=>$pemda,
            'data'=>$data
        ];
    }


    public function anggaran_subgiat_pemda_3($tahun,Request $request){
        $pemda=DB::table('master.master_pemda')->where('kodepemda',$request->kodepemda)->selectRaw("
            kodepemda as kode,
            nama_pemda as namapemda
        ")->first();
        if($tahun>=2021){
            $data=DB::table('dataset.master_dpa_'.$tahun.' as d')
            ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
            ->selectraw("
            sum(total_harga) as y,
            max(d.nama_akun_3) as name,
            max(d.kodepemda) as kodepemda,
            max(d.tahun) as tahun,
            max(d.kode_sub_kegiatan) as kodesub,
            d.kode_akun_3 as kodeakun
            ")->groupBy('d.kode_akun_3')->where('d.kodepemda',$request->kodepemda);
            $where=static::filter($tahun,$request->filter,$request->basis);
            if(count($where)){
                $data->where($where);
            }
            $data=$data->where('kode_sub_kegiatan','ilike',$request->kode.'%');
            $data=$data->where('d.kode_akun_2','ilike',$request->kodeakun.'%');
            $data= $data->get();

        }else{
            $data=[];
        }

        return [
            'pemda'=>$pemda,
            'data'=>$data
        ];
    }

    public function anggaran_subgiat_pemda_4($tahun,Request $request){
        $pemda=DB::table('master.master_pemda')->where('kodepemda',$request->kodepemda)->selectRaw("
            kodepemda as kode,
            nama_pemda as namapemda
        ")->first();
        if($tahun>=2021){
            $data=DB::table('dataset.master_dpa_'.$tahun.' as d')
            ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
            ->selectraw("
            sum(total_harga) as y,
            max(d.nama_akun_4) as name,
            max(d.kodepemda) as kodepemda,
            max(d.tahun) as tahun,
            max(d.kode_sub_kegiatan) as kodesub,
            d.kode_akun_4 as kodeakun
            ")->groupBy('d.kode_akun_4')->where('d.kodepemda',$request->kodepemda);
            $where=static::filter($tahun,$request->filter,$request->basis);
            if(count($where)){
                $data->where($where);
            }
            $data=$data->where('kode_sub_kegiatan','ilike',$request->kode.'%');
            $data=$data->where('d.kode_akun_3','ilike',$request->kodeakun.'%');
            $data= $data->get();

        }else{
            $data=[];
        }

        return [
            'pemda'=>$pemda,
            'data'=>$data
        ];
    }


    

    public function anggaran_subkegiatan_perdaerah($tahun,Request $request){
        if($tahun>=2021){
            $data=DB::table('dataset.master_dpa_'.$tahun.' as d')
            ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
            ->selectraw("
            mp.kodepemda,
            max(d.kode_sub_kegiatan) as kode,
            max(d.tahun) as tahun,
            max(mp.nama_pemda) as namapemda,
            sum(total_harga) as pagu,
            string_agg(distinct(skpd),' , ') as skpd
            ")->groupBy('mp.kodepemda');
            $where=static::filter($tahun,$request->filter,$request->basis);
            if(count($where)){
                $data->where($where);
            }
            $data=$data->where('kode_sub_kegiatan','ilike',$request->kode.'%');
            $data=$data->orderby(DB::raw($request->sort?$request->sort[0]:'mp.kodepemda'),$request->sort?$request->sort[1]:'asc');
            $data= $data->get();


        }else{
            $data=[];
        }
      

        return $data;

    }

    public function rekap_anggaran($tahun,Request $request){

        $data=DB::table('dataset.master_dpa_'.$tahun.' as d')
        ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
        ->selectraw("
        sum(d.total_harga) as pagu,
        count(distinct(case when (length(d.kodepemda)<5) then null  else d.kodepemda end)) as jumlah_pemda_kabkota,
        count(distinct(case when (length(d.kodepemda)<5) then d.kodepemda  else null end)) as jumlah_pemda_provinsi,
        count(distinct(kode_giat)) as jumlah_kegiatan,
        count(distinct(kode_sub_kegiatan)) as jumlah_subkegiatan
        ");
        $where=static::filter($tahun,$request->filter,$request->basis);
    	if(count($where)){
        $data->where($where);
        }
        $data=$data->where('kode_sub_kegiatan','ilike','1.03.03%');
        $data=$data->first();
    
        return $data;
    }


    public function index($tahun,Request $request){
        $temaindex=0;
        return view('tematik.index')->with(['menus'=>config('data_tampilan.tematik')[$temaindex],'temaindex'=>$temaindex]);
    }

    function rpjmn_rpjmd_index($tahun,Request $request){
        if($request->basis){
            static::$basis=$request->basis;
        }
        if($request->filter){
            static::$filter=$request->filter;
        }
        return view('tematik.per_peng.index')->with(['basis'=>static::$basis,'filter'=>static::$filter]);
    }
    
    
    static public function rpjmn_rpjmd_chart($tahun,$level,Reuqest $request){
        if($request->basis){
            static::$basis=$request->basis;
        }
        if($request->filter){
            static::$filter=$request->filter;
        }

    }

    static function filter($tahun,$filter,$basis,$where_extends=[]){
        $where=[['d.tahun','=',$tahun]];
        switch($filter){
            case "kota":
                $where=[['mp.stapem','=',2]];
            break;
            case "kab":
                $where=[['mp.stapem','=',3]];

            break;
            case "provinsi":
                $where=[['mp.stapem','=',1]];
            break;
        }
        switch(strtolower($basis)){
            case 'longlist':
            $where[]=['mp.tahun_proyek','>',0];

            break;
            case 'sortlist':
            $where[]=['mp.tahun_proyek','=',$tahun];
            break;
        }

        if(count($where_extends)){
            foreach($$where_extends as $w){
                $where[]=$w;
            }
        }

        return $where;

    }

    public function detail_subgiat_perpemda($tahun,Request $request){

        $data=DB::table('dataset.program_kegiatan_rkpd as d')
        ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
        ->selectraw("
        mp.kodepemda,
        max(mp.nama_pemda) as namapemda,
        sum(pagu) as pagu,
        string_agg(distinct(skpd),' , ') as skpd
        ")->groupBy('mp.kodepemda');
        $where=static::filter($tahun,$request->filter,$request->basis);
        if(count($where)){
            $data->where($where);
        }
        $data=$data->where('kodesubkegiatan','ilike',$request->kode.'%');
        $data=$data->orderby(DB::raw($request->sort?$request->sort[0]:'mp.kodepemda'),$request->sort?$request->sort[1]:'asc');
        return $data->get();

    }

    public function rekap_daerah($tahun,Request $request){

        $data=DB::table('dataset.program_kegiatan_rkpd as d')
        ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
        ->selectraw("
        sum(pagu) as pagu,
        count(distinct(case when (length(d.kodepemda)<5) then null  else d.kodepemda end)) as jumlah_pemda_kabkota,
        count(distinct(case when (length(d.kodepemda)<5) then d.kodepemda  else null end)) as jumlah_pemda_provinsi,
        count(distinct(kodekegiatan)) as jumlah_kegiatan,
        count(distinct(kodesubkegiatan)) as jumlah_subkegiatan
        ");
        $where=static::filter($tahun,$request->filter,$request->basis);
	  if(count($where)){
            $data->where($where);
        }
        $data=$data->where('kodesubkegiatan','ilike','1.03.03%');
        $data=$data->first();
      
        return $data;
    }

   


    public function program_kegiatan($tahun,Request $request){
        $basis='nasional';
        if($request->basis){
            $basis=$request->basis;
        }
        return view('tematik.per_peng.prokeg')->with('basis',$basis);
    }


    public function pagu_urusan($tahun,Request $request){
        $data=DB::table('dataset.program_kegiatan_rkpd as d')
        ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
        ->selectraw(" sum(pagu) as y, namaurusan as name")
        ->groupBy(DB::raw("namaurusan"));
        $where=static::filter($tahun,$request->filter,$request->basis);
        if(count($where)){
            $data->where($where);
        }
        $data=$data->get();
        foreach($data as $key=>$d){
            $data[$key]->y=(float)$d->y;
        }

        return $data;
    }

    public function pagu_sub_urusan($tahun,Request $request){

    }

    public function pagu_program($tahun,Request $request){
        // sub urusan air minum
        $data=DB::table('dataset.program_kegiatan_rkpd as d')
        ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
        ->selectraw("sum(pagu) as y, (max(replace(replace(namaprogram,'KABUPATEN/KOTA',''),'PROVINSI',''))) as name")
        ->groupBy(DB::raw("kodeprogram"))
        ->where('kodeurusan','ilike','1.03%')
        ;

        $where=static::filter($tahun,$request->filter,$request->basis);
        if(count($where)){
            $data->where($where);
        }
        $data=$data->get();
        foreach($data as $key=>$d){
            $data[$key]->y=(float)$d->y;
        }

        return $data;
    }

    public function pagu_giat_air($tahun,Request $request){
        // sub urusan air minum
        $data=DB::table('dataset.program_kegiatan_rkpd as d')
        ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
        ->selectraw("sum(pagu) as y, (max(namasubkegiatan)) as name,kodesubkegiatan")
        ->groupBy(DB::raw("(kodesubkegiatan)"))
        ->where('kodeprogram','ilike','1.03.03%')
        ;

        $where=static::filter($tahun,$request->filter,$request->basis);
        if(count($where)){
            $data->where($where);
        }

        $data=$data->get();
        foreach($data as $key=>$d){
            $data[$key]->y=(float)$d->y;
        }
        return $data;
    }

    public function program_air_minum($tahun,Request $request){

    }

   

    


}
