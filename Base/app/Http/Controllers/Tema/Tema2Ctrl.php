<?php

namespace App\Http\Controllers\Tema;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
class Tema2Ctrl extends Controller
{
    //
    static $basis='pemda';
    static $filter='p_n_k';
    static $menus=[
        'name'=>'Kerjasama Pengelolaan Air Minum',
        'keterangan'=>'',
        'icon'=>'',
        'sub'=>[
            [
                'name'=>'DDUB Air Minum Pemda',
                'keterangan'=>'',
                'icon'=>'',
                'link'=>'tematik.tm2.ddub',    
            ],
            [
                'name'=>'Tipologi Pemda',
                'keterangan'=>'',
                'icon'=>'',
                'link'=>'tematik.tm2.tipologi'
            ],
            [
                'name'=>'Tipologi Hubungan Pemda PDAM',
                'keterangan'=>'',
                'icon'=>'',
                'link'=>'tematik.tm2.hub_pemda'
            ],
            // [
            //     'name'=>'Sikronisasi',
            //     'keterangan'=>'',
            //     'icon'=>'',
            //     'link'=>'tematik.tm1.per_peng'
            // ],
            // [
            //     'name'=>'Gambaran Pagu Anggaran',
            //     'keterangan'=>'',
            //     'icon'=>'',
            //     'link'=>'tematik.tm1.per_peng'
            // ]
        ]

    ];

    public function index($tahun,Request $request){
        $temaindex=1;
        return view('tematik.index')->with(['menus'=>config('data_tampilan.tematik')[$temaindex],'temaindex'=>$temaindex]);
    }
    static function filter($tahun,$filter,$basis,$where_extends=[]){
        $where=[['d.tahun','=',$tahun]];
        switch($filter){
            case "kota":
                $where=[['mp.stapem','=',2]];
            break;
            case "kab":
                $where=[['mp.stapem','=',3]];

            break;
            case "provinsi":
                $where=[['mp.stapem','=',1]];
            break;
        }
        switch(strtolower($basis)){
            case 'longlist':
            $where[]=['mp.tahun_proyek','>',0];

            break;
            case 'sortlist':
            $where[]=['mp.tahun_proyek','=',$tahun];
            break;
        }


        if(count($where_extends)){
            foreach($where_extends as $w){
                $where[]=$w;
            }
        }

        return $where;

    }

    public function ddub($tahun,Request $request){
        $basis='nasional';
        if($request->basis){
            $basis=$request->basis;
        }

        $color=[
            '0%-25%'=>'#ff5757',
            '26%-50%'=>'#f2be32',
            '51%-75%'=>'#7ed957',
            '76%-100%'=>'#5271fe'
        ];
        if($request->meta){
            $index_page=explode('-',base64_decode($request->meta));

            $page_meta=config('data_tampilan.'.$index_page[0])[$index_page[1]]['submenu'][$index_page[2]];

        }else{
            return abort(404);
        }


     
        return view('tematik.tema2.ddub')->with(['basis'=>$basis,'page_meta'=>$page_meta,'color'=>$color]);
    }

    public function hub_pemda($tahun,Request $request){
        $basis='nasional';
        if($request->basis){
            $basis=$request->basis;
        }
        $color=[
            '0%-25%'=>'#ff5757',
            '26%-50%'=>'#f2be32',
            '51%-75%'=>'#7ed957',
            '76%-100%'=>'#5271fe'
        ];

        return view('tematik.tema2.hubungan_pemda_bumdam')->with(['basis'=>$basis,'color'=>$color]);

    }

    public function load_rekap_hubungan($tahun,Request $request){
        $data=DB::table('hub_pemda_pdam as d')
        ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
        ->selectRaw('d.tipologi_hubungan,count(*) as jumlah_pemda,count(case when(mp.stapem<=1) then mp.kodepemda else null end) as pemda_pro,count(case when(mp.stapem>1) then mp.kodepemda else null end) as pemda_kota')
        ->groupBy('d.tipologi_hubungan');
        $where=static::filter($tahun,$request->filter,$request->basis,[['d.status','=',1]]);
        if(count($where)){
            $data=$data->where($where);
        }
        $data=$data->get();

        $def=[  
            'jumlah_pemda_pro'=>0,
            'jumlah_pemda_kab'=>0,
            'k6'=>0,
            'k5'=>0,
            'k4'=>0,
            'k3'=>0,
            'k2'=>0,
            'k1'=>0,
        ];

        foreach($data as $key=>$d){
            $def['k'.$d->tipologi_hubungan]=(int)$d->jumlah_pemda;
            if($d->pemda_pro){
                $def['jumlah_pemda_pro']+=(int)$d->pemda_pro;
            }
            if($d->pemda_kota){
                $def['jumlah_pemda_kab']+=(int)$d->pemda_kota;
            }
        }
        return $def;
    }

    public function load_hubungan_kota($tahun,Request $request){
        $data=DB::table('hub_pemda_pdam as d')
        ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
        ->selectraw("mp.kodepemda,mp.nama_pemda as name, d.jenis_hubungan,d.tipologi_hubungan as value");
        $where=static::filter($tahun,$request->filter,$request->basis,[['d.status','=',1],['d.kodepemda','ilike',$request->kodepemda.'%']]);
        if(count($where)){
            $data=$data->where($where);
        }
        $data=$data->get();
        foreach($data as $key=>$d){
            $data[$key]->id=(int)$d->kodepemda;
            $data[$key]->color='#5271fe';

        }

        $color=[
            '1'=>['text'=>'TIDAK SEHAT DAN TERGANTUNG','color'=>'#e3342f'],
            '2'=>['text'=>'KURANG SEHAT DAN TERGANTUNG','color'=>'#f8c10a'],
            '3'=>['text'=>'KURANG SEHAT DAN SEMI TERGANTUNG','color'=>'#f57e16'],
            '4'=>['text'=>'SEHAT TAPI SEMI TERGANTUNG','color'=>'#3d9970'],
            '5'=>['text'=>'SEHAT TAPI TERGANTUNG PADA PEMDA','color'=>'#3ec996'],
            '6'=>['text'=>'SEHAT DAN MANDIRI','color'=>'#4bff70'],
        ];

        foreach($data as $key=>$d){
            if(isset($color[$d->value.''])){
                $data[$key]->color=$color[$d->value.'']['color'];
                $data[$key]->text=$color[$d->value.'']['text'];
            }else{
                $data[$key]->color='#ddd';
                $data[$key]->text='Tidak Tedefinisi';
            }
        }

        return ['data'=>$data,'colors'=>$color];
    }

    public function load_hubungan_pro($tahun,Request $request){
        $data=DB::table('hub_pemda_pdam as d')
        ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
        ->join('master.master_pemda as pro','pro.kodepemda','=',DB::raw("left(d.kodepemda,3)"))
        ->selectraw("pro.kodepemda ,max(pro.nama_pemda) as name,count(*) as value");
        $where=static::filter($tahun,$request->filter,$request->basis,[['d.status','=',1]]);
        if(count($where)){
            $data=$data->where($where);
        }
        $data=$data->groupBy('pro.kodepemda')->get();
        $da=[];
        $where=array_filter($where,function($el){
            return (str_contains($el[0],'mp.'));
          
        });

        foreach($data as $key=>$d){
            $jumlah_pemda=DB::table('master.master_pemda as mp')
            ->where('mp.kodepemda','ilike',$d->kodepemda.'%');
            if(count($where)){
                unset($where[0]);
                $jumlah_pemda=$jumlah_pemda->where($where);
            }
            $jumlah_pemda=$jumlah_pemda->count();
            $data[$key]->persentase=($d->value)?($d->value/$jumlah_pemda)*100:0;
            if($data[$key]->persentase>0 AND $data[$key]->persentase<=25){
                $data[$key]->color='#ff5757';
            }else if( $data[$key]->persentase>25 AND  $data[$key]->persentase <= 50){
                $data[$key]->color='#f2be32';
            }else if( $data[$key]->persentase>50 AND  $data[$key]->persentase <= 70){
                $data[$key]->color='#7ed957';
            }
            else if( $data[$key]->persentase>75){
                $data[$key]->color='#5271fe';
            }

            $data[$key]->id=(int)$d->kodepemda;

        }

        return $data;

    }

    public function ddub_ket_kota($tahun,Request $request){
        $data=DB::table('dataset.ddub as d')
        ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
        ->selectraw("
        max(d.ddub_total) as ddub_total,
        max(d.bantuan_stimulan) as bantuan_stimulan,
        max(d.bantuan_kinerja) as bantuan_kinerja,
        max(d.bantuan_pendamping) as bantuan_pendamping,
        count(distinct(mp.kodepemda)) as value, max(mp.nama_pemda) as name, (mp.kodepemda) as kodepemda")
        ->groupBy('mp.kodepemda')
        ->where('d.kodepemda','ilike',$request->kodepemda.'%');

    
        $where=static::filter($tahun,$request->filter,$request->basis);
        if(count($where)){
            $data=$data->where($where);
        }

        $data=$data->get();
        foreach($data as $key=>$d){
            $data[$key]->id=(int)$d->kodepemda;
            $data[$key]->color="#5271fe";
        }

        return $data;


    }

    public  function ddub_ket_pro($tahun,Request $request){
        $data=DB::table('dataset.ddub as d')
        ->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
        ->leftJoin('master.master_pemda as prov','prov.kodepemda','=',DB::raw("left(d.kodepemda,3)"))
        ->selectraw("count(distinct(mp.kodepemda)) as value, max(prov.nama_pemda) as name, max(prov.kodepemda) as kodepemda")
        ->groupBy(DB::raw("prov.kodepemda"));
    
        $where=static::filter($tahun,$request->filter,$request->basis);
        if(count($where)){
            $data=$data->where($where);
            
        }
        $data=$data->get();

        foreach($data as $key=>$d){
            $jumlah_pemda=DB::table('master.master_pemda as mp')
            ->where('mp.kodepemda','ilike',$d->kodepemda.'%');
            if(count($where)){
                unset($where[0]);
                $jumlah_pemda=$jumlah_pemda->where($where);
            }
            $jumlah_pemda=$jumlah_pemda->count();

            $data[$key]->persentase=($d->value)?($d->value/$jumlah_pemda)*100:0;
            if($data[$key]->persentase>0 AND $data[$key]->persentase<=25){
                $data[$key]->color='#ff5757';
            }else if( $data[$key]->persentase>25 AND  $data[$key]->persentase <= 50){
                $data[$key]->color='#f2be32';
            }else if( $data[$key]->persentase>50 AND  $data[$key]->persentase <= 70){
                $data[$key]->color='#7ed957';
            }
            else if( $data[$key]->persentase>75){
                $data[$key]->color='#5271fe';
            }

            $data[$key]->id=(int)$d->kodepemda;

        }

        return $data;


    }
}
