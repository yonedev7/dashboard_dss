<?php

namespace App\Http\Controllers\Tema;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Tema3Ctrl extends Controller
{
    //
    static $basis='pemda';
    static $filter='p_n_k';
    static $menus=[
        'name'=>'Capacity Building Dan Technical Assistance',
        'keterangan'=>'',
        'icon'=>'',
        'sub'=>[
            [
                'name'=>'Data Pelatihan PEMDA',
                'keterangan'=>'',
                'icon'=>'',
                'link'=>'tematik.tm1.program_kegiatan',    
            ],
            // [
            //     'name'=>'Target Kinerja',
            //     'keterangan'=>'',
            //     'icon'=>'',
            //     'link'=>null
            // ],
            // [
            //     'name'=>'Sikronisasi',
            //     'keterangan'=>'',
            //     'icon'=>'',
            //     'link'=>'tematik.tm1.per_peng'
            // ],
            // [
            //     'name'=>'Gambaran Pagu Anggaran',
            //     'keterangan'=>'',
            //     'icon'=>'',
            //     'link'=>'tematik.tm1.per_peng'
            // ]
        ]

    ];

    public function index($tahun,Request $request){
        $temaindex=2;
        return view('tematik.index')->with(['menus'=>config('data_tampilan.tematik')[$temaindex],'temaindex'=>$temaindex]);
    }
}
