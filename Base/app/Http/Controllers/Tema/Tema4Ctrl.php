<?php

namespace App\Http\Controllers\Tema;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Tema4Ctrl extends Controller
{
    //
    static $basis='pemda';
    static $filter='p_n_k';
    static $menus=[
        'name'=>'Capaian Full Cost Recovary',
        'keterangan'=>'',
        'icon'=>'',
        'sub'=>[
            [
                'name'=>'Capaian BUMDAM Tarif FCR',
                'keterangan'=>'',
                'icon'=>'',
                'link'=>'tematik.tm1.program_kegiatan',    
            ],
            [
                'name'=>'Data BPPSPAM Kinerja BUMDAM',
                'keterangan'=>'',
                'icon'=>'',
                'link'=>null
            ],
            // [
            //     'name'=>'Sikronisasi',
            //     'keterangan'=>'',
            //     'icon'=>'',
            //     'link'=>'tematik.tm1.per_peng'
            // ],
            // [
            //     'name'=>'Gambaran Pagu Anggaran',
            //     'keterangan'=>'',
            //     'icon'=>'',
            //     'link'=>'tematik.tm1.per_peng'
            // ]
        ]

    ];

    public function index($tahun,Request $request){
        $temaindex=3;
        return view('tematik.index')->with(['menus'=>config('data_tampilan.tematik')[$temaindex],'temaindex'=>$temaindex]);
    }
}
