<?php

namespace App\Http\Controllers\Tema;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Tema5Ctrl extends Controller
{
    //
    static $basis='pemda';
    static $filter='p_n_k';
    static $menus=[
        'name'=>'Profile Daerah & Badan Usaha Milik Daerah (Air Minum)',
        'keterangan'=>'',
        'icon'=>'',
        'sub'=>[
            [
                'name'=>'Jumlah Penduduk',
                'keterangan'=>'',
                'icon'=>'',
                'link'=>'tematik.tm1.program_kegiatan',    
            ],
            [
                'name'=>'Luas Wilayah',
                'keterangan'=>'',
                'icon'=>'',
                'link'=>null
            ],
            [
                'name'=>'Profile BUMDAM',
                'keterangan'=>'',
                'icon'=>'',
                'link'=>'tematik.tm1.per_peng'
            ],
            // [
            //     'name'=>'Gambaran Pagu Anggaran',
            //     'keterangan'=>'',
            //     'icon'=>'',
            //     'link'=>'tematik.tm1.per_peng'
            // ]
        ]

    ];

    public function index($tahun,Request $request){
        $temaindex=4;
        return view('tematik.index')->with(['menus'=>config('data_tampilan.tematik')[$temaindex],'temaindex'=>$temaindex]);
    }
}

