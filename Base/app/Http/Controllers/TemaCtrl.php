<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class TemaCtrl extends Controller
{
    //

    public function index($tahun,$id_tema){
        $tema=DB::table('master.tema')->find($id_tema);
        if($tema){
            return view('tema.index')->with('tema',$tema);
        }
       
    }

    public function ajax_dataset($tahun,$id_tema,Request $request){
        $where=[['dt.status','=',1],['dt.jenis_distribusi','=',0],['t.id_tema','=',$id_tema]];
        $orwhere=[['dt.status','=',1],['dt.jenis_distribusi','=',0],['t.id_tema','=',$id_tema]];
       $data= DB::table('master.dataset as dt')
        ->join('master.dataset_tema as t','t.id_dataset','=','dt.id')
       ->selectRaw("
       dt.kategori,dt.name,dt.tujuan,dt.penangung_jawab,dt.kode,
       replace('".route('dataset.index',['tahun'=>$tahun,'id_dataset'=>'xxx','tw'=>4,'basis'=>'pemda'])."','xxx',dt.id::text) as link_detail
       ");
       if($request->q){
           $where[]=['dt.name','ilike','%'.$request->q.'%'];
           $orwhere[]=['dt.name','ilike','%'.$request->q.'%'];
            $data=$data->where($where);
            $data=$data->orwhere($orwhere);
       }else{
           $data=$data->where('t.id_tema','=',$id_tema);
       }
       return $data->paginate(10);
    }
}
