<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Storage;
use Auth;
use Validator;
use Alert;

class UserController extends Controller
{
    /**
     * Display a listing of the users
     *
     * @param  \App\Models\User  $model
     * @return \Illuminate\View\View
     */
    public function index(User $model)
    {
        $my=Auth::User();
        $my->adminlte_image=$my->adminlte_image();

        return view('users.index')->with(['my'=>$my]);
    }

    public function update_ava(Request $request){
        if($request->image){
            $image = $request->image;  // your base64 encoded
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = str_random(10).'.'.'png';
            $st=Storage::put( 'public/account/'.Auth::User()->id.'/' . $imageName, base64_decode($image));
            if($st){
                $my=User::find(Auth::id());
                $my->profile_picture="storage/account/".Auth::User()->id.'/'.$imageName;
                $my->save();

                $my=User::find(Auth::id());
                $my->adminlte_image=$my->adminlte_image();

                return $my;
            }
        }
    }


    public function update_profile(Request $request){
        $my=User::find(Auth::id());
        $my->name=$request->name;
        $my->phone_number=$request->phone_number;
        $my->save();
        Alert::success('','Berhasil Merubah Profile');
        return back();



    }

    public function update_password(Request $request){
        $my=User::find(Auth::id());
        $valid=Validator::make($request->all(),[
            'old'=>'string|min:8',
            'new'=>'string|min:8',
            'new_conf'=>'string|min:8'
        ]);

        if($valid->fails()){
            Alert::error('','Password Kurag Dari 8 karakter');
            return back();
        }

        if(Hash::check($request->old, $my->password)){
            if($request->new==$request->new_conf){
                $my->password=Hash::make($request->new);
                $my->save();
                Auth::login($my);
                Alert::success('','Password Berhasil Dirubah');

            }else{
                Alert::error('','Password Baru Tidak Sesuai');

            }

        }else{
            Alert::error('','Password Lama Tidak Sesuai');
        }

       
        return back();



    }


}
