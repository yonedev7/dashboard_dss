<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use DB;
use Auth;
class StoreTokenSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $token=null;
        $session_stored=null;
        if(session()){
        $session_stored=DB::table('sessions')->find(session()->getId());

        }
        if($session_stored){
           if(Auth::guard('web')->check()){
                if($session_stored->api_token){

                }else{
                    $token=md5(session()->getId()).rand(0,100);
                    DB::table('sessions')->where(
                        'id',session()->getId()
                    )->update(['api_token'=>$token]);
                }
           }
            return $next($request);

        }else{
            return back();
        }
    }
}
