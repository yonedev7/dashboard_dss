<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use View;
use HelperC;
class TahunAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {   

        $sesTahun=$request->route('tahun');
        // if(session('tahun_data')){
        //     session(['tahun_data'=>date('Y')]); 
        // }

        View::share ('StahunRoute', $sesTahun);

            
        return $next($request);

    //     if(($session) && $session->access_tahun ){
    //         if($session->access_tahun!=$session->access_tahun){
    //             $sesTahun=$session->access_tahun;
    //             session(['access_tahun'=>$sesTahun]);
    //             return redirect()->route('home',['tahun'=>$sesTahun]);
    //         }

    //         // View::share( 'Stahun', $sesTahun );
    //        
    //         return $next($request);

    //     }else{
    //         HelperC::store_session('def');

    //         return back();
    //     }
    }
}
