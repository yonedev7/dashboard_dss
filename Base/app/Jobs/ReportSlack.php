<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Carbon\Carbon;
use DB;
use Spatie\SlackAlerts\Facades\SlackAlert;
class ReportSlack implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $tahun=date('Y');
        $table=[];
        $dataset=DB::table('master.dataset')->get();
        foreach($dataset as $d){
            if(str_contains('[TAHUN]',$d->table)){
                $table[]='dts_'.$d->id.'_'.$tahun;
            }else{
                $table[]='dts_'.$d->id;
            }
        }

        foreach($table as $tb){
            
        }

        \Slack::to('#finance')->send('Hey, finance channel! A new order was created just now!');
    }
}
