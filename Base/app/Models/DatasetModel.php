<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DatasetModel extends Model
{
    use HasFactory;
    protected $table="master.dataset";
    protected $fillable=['*'];
}
