<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use App\Models\UserTokenApiWeb;
use DB;
use JamesDordoy\LaravelVueDatatable\Traits\LaravelVueDatatableTrait;
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable,LaravelVueDatatableTrait;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */

    protected $dataTableColumns = [
        'id' => [
            'searchable' => false,
        ],
        'name' => [
            'searchable' => true,
        ],
        'email' => [
            'searchable' => true,
        ]
    ];
    
    protected $fillable = [
        'id',
        'name',
        'email',
        'password',
        'kodepemda',
        'kodebumdam',
        'regional',
        'lokus_basis',
        'profile_picture',
        'phone_number',
        'slack_account',
        'kodepemda'

    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'id',
        'remember_token',
        // 'profile_picture',
        // 'phone_number',
        // 'slack_account'

    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roleCheck($role,$guard='web'){
        $r=(DB::table('model_has_roles as hr')
        ->join('roles as r','r.id','=','hr.role_id')
        ->where('hr.model_type',DB::raw("'App\Models\User'"))
        ->where('r.name',$role)
        ->where('r.guard_name',$guard)
        ->select('r.name')
        ->where('hr.model_id',DB::raw($this->id))->first())?true:false;

        return $r;
    }

    public function _web_api_token(){
        
        return UserTokenApiWeb::get_token_web();
    }

    public function adminlte_image(){
        return asset($this->profile_picture??'assets/img/icons/icon-u.png');

        // return asset('assets/img/icons/icon-u.png');
    }

    public function adminlte_profile_url(){
        return route('dash.si.account.index',['tahun'=>date('Y')]);
    }

    
}
