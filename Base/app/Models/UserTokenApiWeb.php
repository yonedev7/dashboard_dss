<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Sanctum\HasApiTokens;

use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class UserTokenApiWeb extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    use HasRoles;
   

    protected $table='public.session_token';
    protected $fillable = [
        'id',
        'name',
        'email',
        'password',
        'kodepemda',
        'kodebumdam',
        'regional',
        'lokus_basis',
        'api_token'
    ];


    public static function get_token_web(){
        if(Auth::guard('web')->check()){
            $u=DB::table('public.sessions')->where('id',session()->getId())->first();
           return  $u?$u->api_token:'';
        }else{
            return '';
        }
    }




}
