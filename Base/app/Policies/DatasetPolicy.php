<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\DatasetModel;
class DatasetPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function input(User $user,DatasetModel $dts){
        return $user->getAllPermissions(['dts.'.$dts->id.':edit-data']);
    }
}
