<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;
use Carbon\Carbon;
use Auth;
use App\Models\User;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Models\Model' => 'App\Policies\ModelPolicy',

    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {

    
        Gate::before(function ($user, $ability) {
           if($ability!='non-admin'){
                if($user->hasRole('Super Admin')){
                    return true;
                }
                if($user->id==1){
                    return true;
                }
           }else{

           }
        });

        Gate::define('is_admin',function($user){
            if($user->hasRole('Admin')){
                return true;
            }else{
                return false;
            }
            
        });

        Gate::define('haveRoleTACT',function($user){
            if($user->hasRole('TACT-LG')){
                return true;
            }else{
                return false;
            }
            
        });

        Gate::define('non-admin',function ($user) {
            if($user->id==1){
                return false;
            }
            return !$user->hasRole(['Admin','Super Admin']);
        });

        Gate::define('authentificated-web',function($user){
            return ($user);
        });

        if (! $this->app->routesAreCached()) {
            Passport::routes();
            Passport::tokensExpireIn(now()->addDays(15));
            Passport::refreshTokensExpireIn(now()->addDays(30));
            Passport::personalAccessTokensExpireIn(now()->addMonths(6));
        }
        

       

    }


}
