<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;
use View;
use Auth;
use Session;
use Illuminate\Support\Facades\Event;
use DB;
use Illuminate\Http\Request;
use HelperC;
use Illuminate\Support\Facades\Route;

class MenuProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */

     static $menus=[
            [
                'name'=>'Perencanaan dan Anggaran',
                'icon'=>('assets/img/1.png'),
                'link'=>'tematik.load_tematik',
                'meta'=>('tematik-0')

            ],
            [
                'name'=>'Kerjasama Pengelolaan Air Minum',
                'icon'=>('assets/img/5.png'),
                'link'=>'tematik.load_tematik',

                'meta'=>('tematik-1')


            ],
            [
                'name'=>'CB & TA',
                'icon'=>('assets/img/4.png'),
                'link'=>'tematik.load_tematik',
                'meta'=>('tematik-2')


            ],
            [
                'name'=>'Capaian FCR',
                'icon'=>('assets/img/3.png'),
                'link'=>'tematik.load_tematik',
                'meta'=>('tematik-3')

            ],
            [
                'name'=>'Profil Daerah & BUMDAM',
                'icon'=>('assets/img/3.png'),
                'link'=>'tematik.load_tematik',
                'meta'=>('tematik-4')

            ]

     ];

    public function boot(){
        if(!isset($_SESSION['page_id'])){
            $_SESSION['page_id']=md5(date('si').'/'.rand(0,10));
        }
        View::share('SpageId',$_SESSION['page_id']);

        $menuUtama=config('data_tampilan.tematik');
        foreach($menuUtama as $key=>$me){
            $menuUtama[$key]['meta']='tematik-'.$key;
            if(!isset( $menuUtama[$key]['link'])){
                $menuUtama[$key]['link']='tematik.load_tematik';
            }

        }

        
        if(isset($_SERVER['REQUEST_URI']) and str_contains($_SERVER['REQUEST_URI'],'rubah-tahun')){

        }else{

            View::composer('*',function($view){
                $this->build();
            });
        }

        View::share('Smenus',$menuUtama);



         
    }



    public static function build()
    {
        $Stahun=session('tahun_data')?session('tahun_data'):date('Y');
        if(Auth::check()){
        //    $ses= DB::table('sessions')->where('id',session()->getId())->first();
        //     if($ses){
        //         if($ses->api_token==null){
        //             $token_api=null;
        //             do{
        //                 $id_session=session()->getId();
        //                 $token_api=md5($id_session).rand(0,100);
        //             }while(DB::table('public.sessions')->where('api_token',$token_api)->first()); 
        //             DB::table('sessions')->where('id',session()->getId())->update(['api_token'=>$token_api]);
        //         }
        //     }
        
        }
        
        $_SESSION['tahun_data']=$Stahun;
        $menuProfilePemda=[
            [
                'title'=>'GAMBARAN UMUM',
                'field'=>'gm',
                'content'=>'',
                'rendered'=>false,
                'sub'=>[
                    [
                        'title'=>'DEMOGRAFI',
                        'field'=>'gm_demografis',
                        'content'=>'',
                        'rendered'=>false,
                    ],
                    [
                        'title'=>'TINGKAT KEMISKIN',
                        'field'=>'gm_pen_miskin',
                        'content'=>'',
                        'rendered'=>false,
                    ],
                    [
                        'title'=>'AKSES AIR MINUM',
                        'field'=>'gm_akses_air_minum_rt',
                        'content'=>'',
                        'rendered'=>false,
                    ],
                    [
                        'title'=>'KAPASITAS FISKAL',
                        'field'=>'gm_index_ikfd',
                        'content'=>'',
                        'rendered'=>false,
                    ],
                    [
                        'title'=>'ANGGARAN DAN BELANJA DAERAH',
                        'field'=>'gm_apbd',
                        'content'=>'',
                        'rendered'=>false,
                    ]
        
                ]
            ],
            [
                'title'=>'PARTISIPASI DALAM NUWSP',
                'field'=>'kt',
                'content'=>'',
                'rendered'=>false,
                'sub'=>[
                    [
                        'title'=>'DANA HIBAH NUWSP',
                        'field'=>'kt_hibah',
                        'content'=>'',
                        'rendered'=>false,
                    ],
                    [
                        'title'=>'DANA DAERAH URUSAN BERSAMA',
                        'field'=>'kt_ddub',
                        'content'=>'',
                        'rendered'=>false,
                    ],
                    [
                        'title'=>'ALOKASI DANA',
                        'field'=>'kt_alokasi',
                        'content'=>'',
                        'rendered'=>false,
                    ],
                    [
                        'title'=>'KOMITMEN PROGRAM/KEGIATAN DILUAR DDUB',
                        'field'=>'kt_komitmen',
                        'content'=>'',
                        'rendered'=>false,
                    ],
                  
        
                ]
            ],
            [
                'title'=>'KELEMBAGAAN PENGELOLAAN AIR MINUM',
                'field'=>'kl',
                'content'=>'',
                'rendered'=>false,
                'sub'=>[
                    [
                        'title'=>'DINAS PENGELOLA',
                        'field'=>'kl_dinas',
                        'content'=>'',
                        'rendered'=>false,
                    ],
                    [
                        'title'=>'TIPOLOGI DINAS',
                        'field'=>'kl_tipologi',
                        'content'=>'',
                        'rendered'=>false,
                    ],
                    
        
                ]
            ],
            [
                'title'=>'PROGRAM KEGIATAN AIR MINUM (RKPD)',
                'field'=>'prokeg',
                'content'=>'',
                'rendered'=>false,
                'sub'=>[
                    [
                        'title'=>'TAHUN '.$Stahun,
                        'field'=>'prokeg_tahunan',
                        'content'=>'',
                        'rendered'=>false,
                    ],
                    [
                        'title'=>'TAHUN '.($Stahun-1),
                        'field'=>'prokeg_tahunan_min_1',
                        'content'=>'',
                        'rendered'=>false,
                    ],
                    
        
                ]
            ],
            [
                'title'=>'PEMENUHAN SPM',
                'field'=>'spm',
                'content'=>'',
                'rendered'=>false,
                'sub'=>[
                    [
                        'title'=>'TARGET SPM '.$Stahun,
                        'field'=>'spm_target',
                        'content'=>'',
                        'rendered'=>false,
                    ],
                    [
                        'title'=>'CAPAIAN SPM',
                        'field'=>'spm_capaian',
                        'content'=>'',
                        'rendered'=>false,
                    ],
                    
        
                ]
            ]
        
        
        ];
    
        $Ul=explode('/',$_SERVER['REQUEST_URI']);
        $CUR_=array_slice($Ul,1,count($Ul)>=4?10:2);
        $CUR_='/'.implode('/',$CUR_).'/';
        
        if(count(preg_grep('/\/dash\//',  [$CUR_]))){
            $Sadmin=true;
        }else{
            $Sadmin=false;
        }

        View::share('Sadmin', $Sadmin );

        $url_current=Route::current()?Route::current()->uri():'';

        $bind_parems=Route::current()?Route::current()->parameters:[];

        foreach($bind_parems??[] as $key=>$d){
            if($key!='tahun'){
                $url_current=str_replace('{'.$key.'}',$d,$url_current);
            }
        }
       


        $parm=[];
        foreach($_GET??[] as $k=>$d){
            $parm[]=(urlencode($k).'='.urlencode($d));       
        }

        if(count($parm)){
            $url_current.='?'.implode('&',$parm);
        }


    
        $StahunRoute=(Route::getFacadeRoot()->current()?Route::getFacadeRoot()->current()->parameters('tahun'):'0');
        $StahunRoute=(int)isset($StahunRoute['tahun'])?$StahunRoute['tahun']:date('Y');
        
        View::share('Scurent_url_bind', base64_encode($url_current) );
        View::share('SmenuProfilePemda', $menuProfilePemda);
        View::share ('Stahun', session('tahun_data')?session('tahun_data'):(int)date('Y'));
        View::share ('StahunRoute',$StahunRoute );
        if($Sadmin){
                 View::share('SenvDash', 'admin');
            
            config([
                'adminlte.menu'=>static::menuAdmin($StahunRoute,$Stahun,$url_current),
                'adminlte.layout_fixed_navbar' => false,
                'adminlte.layout_fixed_sidebar' => true,
            ]);
        }else{
            View::share('SenvDash', 'dashboard');

            config([
                'adminlte.menu'=>static::menuDashboard($StahunRoute,$Stahun,$url_current),
        
                ]);


        }




        // View::composer('adminlte::page',function($view) use($url_current,$Sadmin,$Stahun,$StahunRoute){
        //     Event::listen(BuildingMenu::class, function (BuildingMenu $event) use ($url_current,$Sadmin,$Stahun,$StahunRoute) {
        //         config([
        //             'adminlte.dashboard_url'=>route('home',['tahun'=>$Stahun])
        //         ]);
                
        //         $event->menu->menu=[];

        //         $event->menu->add(
                    // [
        //             'text' => 'Lokus '.$Stahun,
        //             'topnav_right'=>true,
        //             'submenu'=>[
        //                 [
        //                     'text'=>'Tahun '.($Stahun-2),
        //                     'url'=>route('rubah_tahun_lokus',['tahun'=>$StahunRoute,'tahun2'=>($Stahun-2),'pref'=>base64_encode($url_current)])
        //                 ],
        //                 [
        //                     'text'=>'Tahun '.($Stahun-1),
        //                     'url'=>route('rubah_tahun_lokus',['tahun'=>$StahunRoute,'tahun2'=>($Stahun-1),'pref'=>base64_encode($url_current)])
        //                 ],
        //                 [
        //                     'text'=>'Tahun '.($Stahun+1),
        //                     'url'=>route('rubah_tahun_lokus',['tahun'=>$StahunRoute,'tahun2'=>($Stahun+1),'pref'=>base64_encode($url_current)])
    
        //                 ],
        //                 [
        //                     'text'=>'Tahun '.($Stahun+2),
        //                     'url'=>route('rubah_tahun_lokus',['tahun'=>$StahunRoute,'tahun2'=>($Stahun+2),'pref'=>base64_encode($url_current)])
    
        //                 ]
        //             ]
        //         ]);

                

    
        //         // if(!Auth::check()){
        //         // $event->menu->add([
        //         //     'text' => 'Login',
        //         //     'topnav_right'=>true,
        //         //     'url'=>route('login'),
        //         //     'can'=>false
        //         // ]);
        //         //  }
    
    
        //         $event->menu->add(
        //             [
        //                 'text' => 'profile',
        //                 'url'  => 'http://my.domain.com/user/profile',
        //                 'icon' => 'fas fa-fw fa-user',
        //                 'topnav_user'=>true
        //             ],
        //         );
    
        //         $event->menu->add(
        //             [
        //                 'text' => 'Ubah Password',
        //                 'url'  => 'http://my.domain.com/user/profile',
        //                 'icon' => 'fas fa-fw fa-lock',
        //                 'topnav_user'=>true
        //             ],
                    
        //         );
    
        //         $event->menu->add(
        //             [
        //                 'text' => 'Admin',
        //                 'url'  => route('dash.si.index',['tahun'=>$Stahun]),
        //                 'icon' => "fas fa-toolbox",
        //                 'topnav_user'=>true
        //             ],
                    
        //         );
    
                
        //         if($Sadmin){
        //             $User=Auth::User();
    
        //           config([
        //                 'adminlte.layout_topnav'=>false,
        //                 'adminlte.layout_fixed_navbar' => true,
        //                 'adminlte.layout_fixed_sidebar' => true,
        //                 'adminlte.sidebar_collapse' => true,
        //                 'adminlte.sidebar_mini'=>'lg',
        //                 'adminlte.dashboard_url'=>route('home',['tahun'=>$StahunRoute])
        //             ]);
        //             $event->menu->add([
        //                 'text' => 'Dahshboard Analisa',
        //                 'url' => route('dash.si.my_dash',['tahun'=>$StahunRoute]),
        //                 'class'=>'btn btn-primary',
        //                 'can'=>'Accesss Analisa'
        //             ]);
        //             $event->menu->add([
        //                 'text' => 'Beranda',
        //                 'url' => route('home',['tahun'=>$StahunRoute]),
        //                 'class'=>'btn btn-primary'
        //             ]);
        //             $event->menu->add([
        //                 'text' => 'Rekap',
        //                 'url' => route('dash.si.rekap.index',['tahun'=>$StahunRoute]),
        //                 'can'=>'Accesss Rekap Data',
        //             ]);
        //             $event->menu->add([
        //                 'text' => 'Lokus',
        //                 'url' => route('dash.si.lokus.index',['tahun'=>$StahunRoute]),
        //                 'can'=>'Accesss Lokus',

        //             ]);
        //             $event->menu->add([
        //                 'text' => 'Analisa',
        //                 'url' => route('dash.si.analisa.wg_build',['tahun'=>$StahunRoute]),
        //                 'class'=>'',
        //                 'can'=>'Accesss Analisa'

        //             ]);
    

        //             if(($User->can('Access Schema Data') OR $User->can('Access Schema Dataset')) OR ($User->can('Access Parameter Profile'))){
        //                 $event->menu->add('Schema');
                    
        
        //                 $event->menu->add([
        //                     'text' => 'Data',
        //                     'url' => route('mod.dataset.schema-data.index',['tahun'=>$StahunRoute]),
        //                     'can'=>'Access Schema Data'

        //                 ]);
                    
        //                 $event->menu->add([
        //                     'text' => 'Dataset',
        //                     'url' => route('mod.dataset.schema.index',['tahun'=>$StahunRoute]),
        //                     'can'=>'Access Schema Dataset'

        //                 ]);
        //                 $event->menu->add([
        //                     'text' => 'Parameter Profile',
        //                     'url' => route('mod.parameter-profile.index',['tahun'=>$StahunRoute]),
        //                     'can'=>'Access Parameter Profile'
        //                 ]);
        //             }
                    
                   
        //             // $event->menu->add([
        //             //     'text' => 'Data Capaian',
        //             //     'url' => route('dash.si.sumberdata.index',['tahun'=>$Stahun]),
        //             // ]);
                    // $event->menu->add([
                    //     'text' => 'Form Monev',
                    //     'url' => 'admin/blog',
                    // ]);
        //             // $event->menu->add([
        //             //     'text' => 'Form Desk',
        //             //     'url' => 'admin/blog',
        //             // ]);

        //             $event->menu->add([
        //                 'text' => 'Kegiatan',
        //                 'url' => route('mod.agenda.index',['tahun'=>$Stahun]),
        //                 'can'=>'Access Kegiatan'
        //             ]);
                    // $event->menu->add([
                    //     'text' => 'Monev',
                    //     'url' => route('mod.monev-admin.form.index',['tahun'=>$Stahun]),
                    //     'can'=>'Access Monev'
                    // ]);
    
        //             if(($User->can('Access Dataset Pemda') OR $User->can('Access Dataset Pusat')) OR ($User->can('Access Profile Pemda'))){

        //             $event->menu->add('Pengisian Data');

        //             $event->menu->add([
        //                 'text' => 'Dataset PEMDA',
        //                 'url' => route('mod.dataset',['tahun'=>$StahunRoute,'basis'=>'nasional','meta'=>null]),
        //                 'can'=>'Access Dataset Pemda'
        //             ]);
    
        //             $event->menu->add([
        //                 'text' => 'Dataset PUSAT',
        //                 'url' => route('mod.dataset',['tahun'=>$StahunRoute,'basis'=>'nasional','meta'=>true]),
        //                 'can'=>'Access Dataset Pusat'

        //             ]);

        //             $event->menu->add([
        //                 'text' => 'Profile PEMDA',
        //                 'url' => route('dash.profile-pemda.index',['tahun'=>$StahunRoute]),
        //                 'can'=>'Access Profile Pemda'

        //             ]);
        //             }
                    
        //             $event->menu->add([
        //                 'text' => 'Timeline DSS',
        //                 'url' =>  route('dash.si.timeline.index',['tahun'=>$StahunRoute]),
        //                 'can'=>'Access Timeline Dss'
        //             ]);

        //             if(Auth::User()->can('init-superadmin')){

        //                 $event->menu->add('Users & Permission');

        //                 $event->menu->add([
        //                     'text' => 'Role',
        //                     'url' =>  route('mod.permission.role.index',['tahun'=>$Stahun]),
        //                     'can'=>'init-superadmin'
        //                 ]);
        //                 $event->menu->add([
        //                     'text' => 'Permision',
        //                     'url' =>  route('mod.permission.permis.index',['tahun'=>$Stahun]),
        //                     'can'=>'init-superadmin'
    
        //                 ]);
    
        //                 $event->menu->add([
        //                     'text' => 'Users',
        //                     'url' =>  route('mod.permission.user.index',['tahun'=>$Stahun]),
        //                     'can'=>'init-superadmin'
    
        //                 ]);
                        

        //             }
                   

                  
    
    
        
        //             // $event->menu->add('Database');
        
        //             // $event->menu->add([
        //             //     'text' => 'Data Capaian',
        //             //     'url' => 'admin/blog',
        //             // ]);
        //             // $event->menu->add([
        //             //     'text' => 'Form',
        //             //     'url' => 'admin/blog',
        //             // ]);
        
        //             // $event->menu->add([
        //             //     'text' => 'RKPD',
        //             //     'url' => 'admin/blog',
        //             // ]);
        
        //             // $event->menu->add([
        //             //     'text' => 'DPA',
        //             //     'url' => 'admin/blog',
        //             // ]);
        //             // $event->menu->add('Bot Crawler');
                    
        //             // $event->menu->add([
        //             //     'text' => 'RKPD',
        //             //     'url' => 'admin/blog',
        //             // ]);
        //             // $event->menu->add([
        //             //     'text' => 'SAT',
        //             //     'url' => 'admin/blog',
        //             // ]);
        
        //             // $event->menu->add('Output NUWSP');
        
        //             // $event->menu->add([
        //             //     'text' => 'DSS',
        //             //     'url' => 'admin/blog',
        //             // ]);
        
        //             // $event->menu->add([
        //             //     'text' => 'TACT',
        //             //     'url' => 'admin/blog',
        //             // ]);
        //         }else{
    
                  
        //         //    $menus= DB::table('master.tema')->where('menu_group','UTAMA')->orderBy('index','asc')->get();
        //         //    foreach($menus as $menu){
        //         //         $event->menu->add([
        //         //             'text' => $menu->name,
        //         //             'url' => route('tema.index',['tahun'=>$Stahun,'id_tema'=>$menu->id]),
        //         //         ]);
    
        //         //    }
        //         //    $menus_kpi= DB::table('master.tema')->where('menu_group','KPI-BANGDA')->orderBy('index','asc')->get();
        //         //     $subkpi=[];
        //         //    foreach($menus_kpi as $menu){
        //         //       $subkpi[] =[
        //         //             'text' => $menu->name,
        //         //             'url' => route('tema.index',['tahun'=>$Stahun,'id_tema'=>$menu->id]),
        //         //         ];
    
        //         //    }
                   
        //             // $event->menu->add([
        //             //     'text' => 'MONEV KPI',
        //             //     'submenu'=>$subkpi
        //             // ]);
                   
        //             // $event->menu->add([
        //             //     'text' => 'Profile',
        //             //     'url' => route('tema.index',['tahun'=>$Stahun,'id_tema'=>1]),
        //             // ]);
        //             // $event->menu->add([
        //             //     'text' => 'Analisa',
        //             //     'url' => route('tema.index',['tahun'=>$Stahun,'id_tema'=>1]),
        //             // ]);
    
    
                    
        //              $event->menu->add([
        //                 'text' => 'Katalog Metadata',
        //                 'url' => route('cat.index',['tahun'=>$Stahun]),
        //             ]);
    
    
        //             $event->menu->add([
        //                 'text' => 'Data Pendukung',
        //                 'url' => route('tematik.load_pendukung',['tahun'=>$Stahun,'meta'=>base64_encode('pendukung-0')]),
        //             ]);

        //             $event->menu->add([
        //                 'text' => 'Monev',
        //                 'url' => route('mod.monev.index',['tahun'=>$Stahun,'meta'=>base64_encode('monev-0')]),
        //                 'can'=>'authentificated-web',
        //             ]);

		// 			$event->menu->add([
        //                 'text' => 'Kegiatan',
        //                 'url' => route('mod.agenda.dash',['tahun'=>$Stahun,'kpi'=>'1']),
        //                 'can'=>'authentificated-web'
        //             ]);
    
        //             // $event->menu->add([
        //             //     'text' => 'Perencanaan Pusat',
        //             //     'submenu'=>[
        //             //         [
        //             //             'text'=>'RPJMN',
        //             //             'url'=>'#'
    
        //             //         ],
        //             //         [
        //             //             'text'=>'SDGS',
        //             //             'url'=>'#'
    
        //             //         ],
        //             //         [
        //             //             'text'=>'SPM',
        //             //             'url'=>'#'
    
        //             //         ],
        //             //         [
        //             //             'text'=>'NSPK',
        //             //             'url'=>'#'
    
        //             //         ],
        //             //         [
        //             //             'text'=>'Nomenklatur (PERMENDAGRI 90)',
        //             //             'url'=>'#'
    
        //             //         ],
        //             //         [
        //             //             'text'=>'PERMENDAGRI 18 Tahun 2020',
        //             //             'url'=>'#'
    
        //             //         ],
        //             //     ]
        //             // // ]);
    
        //             // $event->menu->add([
        //             //     'text' => 'Perencanaan & Anggaran Daerah',
        //             //     'url' => route('pd.index',['tahun'=>$Stahun]),
        //             // ]);
    
        //             // $event->menu->add([
        //             //     'text' => 'Data Sektoral',
        //             //     'url' => route('ds.index',['tahun'=>$Stahun]),
        //             // ]);
    
        //             // $event->menu->add([
        //             //     'text' => 'Refrensi',
        //             //     'url' => 'admin/blog',
        //             // ]);
    
        //             // $event->menu->add([
        //             //     'text' => 'Publikasi',
        //             //     'url' => 'admin/blog',
        //             // ]);
    
        //             // $event->menu->add([
        //             //     'text' => 'Profil',
        //             //     'url' => 'admin/blog',
        //             // ]);
    
        //             // $event->menu->add([
        //             //     'text' => 'Analisa',
        //             //     'url' => 'admin/blog',
        //             // ]);
    
        //         }
    
    
        //     });
        // });
    }

    static function menuDashboard($StahunRoute,$Stahun,$url_current){
        
        return [
            [
                'text' => 'Katalog Metadata',
                 'url' => route('cat.index',['tahun'=>$Stahun]),
            ],
            [
                'text' => 'Data Pendukung',
                 'url' => route('mod.tematik.index_pendukung',['tahun'=>$Stahun,'meta'=>base64_encode('pendukung-0')]),
            ],
            [
                'text' => 'Monev',
                'url' => route('mod.monev.index',['tahun'=>$Stahun,'meta'=>base64_encode('monev-0')]),
                'can'=>'authentificated-web',
            ],
            [
                'text' => 'Kegiatan',
                'url' => route('mod.agenda.dash',['tahun'=>$Stahun,'kpi'=>'1']),
                'can'=>'authentificated-web'
            ],
            [
                'text'=>'Dana Pendamping',
                'url'=>route('mod.levarance.pub.index',['tahun'=>date('Y')])
            ],
            [
                'text' => 'Tutorial',
                'url'=>route('mod.tutorial.index',['tahun'=>$Stahun]),
                'can'=>'authentificated-web'
            ],
            [
                'text' => 'Admin',
                'url'  => route('dash.si.index',['tahun'=>$Stahun]),
                'icon' => "fas fa-toolbox",
                'topnav_user'=>true
            ],
            [
                'text' => 'Data '.$StahunRoute,
                'topnav_right'=>true,
                'submenu'=>[
                    [
                        'text'=>'Tahun '.(((int)$StahunRoute)-2),
                        'url'=>route('rubah_tahun',['tahun'=>$StahunRoute,'tahun2'=>(((int)$StahunRoute)-2),'pref'=>base64_encode($url_current)])
                    ],
                    [
                        'text'=>'Tahun '.(((int)$StahunRoute)-1),
                        'url'=>route('rubah_tahun',['tahun'=>$StahunRoute,'tahun2'=>(((int)$StahunRoute)-1),'pref'=>base64_encode($url_current)])
                    ],
                    [
                        'text'=>'Tahun '.(((int)$StahunRoute)+1),
                        'url'=>route('rubah_tahun',['tahun'=>$StahunRoute,'tahun2'=>(((int)$StahunRoute)+1),'pref'=>base64_encode($url_current)])

                    ],
                    [
                        'text'=>'Tahun '.(((int)$StahunRoute)+2),
                        'url'=>route('rubah_tahun',['tahun'=>$StahunRoute,'tahun2'=>(((int)$StahunRoute)+2),'pref'=>base64_encode($url_current)])

                    ]
                ]
            ],
        ];
    }

    // $event->menu->add([
        //                 'text' => 'Katalog Metadata',
        //                 'url' => route('cat.index',['tahun'=>$Stahun]),
        //             ]);
    
    
        //             $event->menu->add([
        //                 'text' => 'Data Pendukung',
        //                 'url' => route('tematik.load_pendukung',['tahun'=>$Stahun,'meta'=>base64_encode('pendukung-0')]),
        //             ]);

        //             $event->menu->add([
        //                 'text' => 'Monev',
        //                 'url' => route('mod.monev.index',['tahun'=>$Stahun,'meta'=>base64_encode('monev-0')]),
        //                 'can'=>'authentificated-web',
        //             ]);

		// 			$event->menu->add([
        //                 'text' => 'Kegiatan',
        //                 'url' => route('mod.agenda.dash',['tahun'=>$Stahun,'kpi'=>'1']),
        //                 'can'=>'authentificated-web'
        //             ]);
    

    static function menuAdmin($StahunRoute,$Stahun,$url_current){
        $dataset_menu=[];
        if(Auth::check()){
            $my=Auth::User();
            $dataset_menu=[
                [
                    'text'=>'ADMIN DASHBOARD',
                    'icon'=>'nav-icon fas fa-tachometer-alt',
                    'url'=>route('dash.si.index',['tahun'=>date('Y')])
                ],
                [
                    'text'=>'PEMDA DASHBOARD',
                    'icon'=>'nav-icon fas fa-tachometer-alt',
                    'url'=>route('mod.profile.build.index',['tahun'=>date('Y')])
                ],
                [
                    'text'=>'TABLE CONTROL DATA UTAMA',
                    'icon'=>'nav-icon fa fa-check-double ',
                    'url'=>route('dss.table-control',['tahun'=>date('Y'),'context'=>'utama'])
                ],
                // [
                //     'text'=>'TABLE CONTROL DATA PENDUKUNG',
                //     'icon'=>'nav-icon fa fa-check-double ',
                //     'url'=>route('dss.table-control',['tahun'=>date('Y'),'context'=>'pendukung'])
                // ]
            ];


            if(count($my->getRoleNames())){
                $dataset_menu[]=[
                    'text'=>'DASHBOARD '.implode(', ',$my->getRoleNames()->toArray()),
                    'classes'=>'bg-dark px-2 rounded',
                    'topnav'=>true,
                    'icon'=>'nav-icon fas fa-tachometer-alt',
                    'url'=>route('dash.si.index',['tahun'=>date('Y')])
                ];
            }
           
            if($my->can('is_admin')){
                $jenis_menu=['','MENU UTAMA','MENU PENDUKUNG','MENU LAINYA'];
                $last_type=0;
                // $menus=DB::table('master.menus')->orderBy('type','asc')->orderBy('index','asc')->get();
            
                // foreach($menus as $k=>$menu ){
                //     $def=[
                //         'text'=>strtoupper($menu->title),
                //         'url'=>'#',
                //         'submenu'=>[
                            
                //         ],
                //         'icon'=>'text-dark fa fa-chart-pie'
        
                //     ];
                //     if($menu->type==1 && $menu->kode_parent!='PROFILE'){
                //         $def['submenu'][]=[
                //             'text'=>'REKAP TEMATIK',
                //             'icon'=>'nav-icon fa fa-chart-line',
                //             'url'=>'#'
                //         ];
                //     }
                //     $submenu=DB::table('master.dataset')->where('id_menu',$menu->id)->orderBy('index','asc')->get();
                //     foreach($submenu as $s=>$dts){
                //         $def['submenu'][]=[
                //             'text'=>strtoupper($dts->name),
                //             'id'=>$dts->id,
                //             'url'=>route('mod.dash.admin.index',['tahun'=>date('Y'),'id_dataset'=>$dts->id]),
                //             'icon'=>'text-success fa fa-chart-bar'
        
                //         ];
                //     }
                //     if($last_type!=$menu->type){
                //         $last_type=$menu->type;
                //         $dataset_menu[]=[
                //             'header'=>$jenis_menu[$menu->type],
                //             'classes'=>'border-primary border-top'
                //         ];
                //     }
                //     $dataset_menu[]=$def;
                // }
                // $menu=[];
                // foreach($dataset_menu as $m){
                //     if(is_string($m)){
                //         $menu[]=$m;
        
                //     }else{
                //         $menu[]=$m['text'];
                //         foreach($m['submenu'] as $s=>$dts){
                //             $menu[]=$dts;
                //         }
                //     }
                    
                // }

                if(true){
                
                    
                        $dataset_menu[]=[
                            'header'=>strtoupper('Penginputan / Prosessing Data Tematik'),
                            'classes'=>'border-primary border-top '
                        ];

                    

                        $dataset_menu[]= [
                            'text' => strtoupper('Tematik Utama'),
                            'url'  => route('mod.dataset.dataset.index',['tahun'=>date('Y')]),
                            'icon'=>'nav-icon fa fa-file',
                            // 'classes'=>'bg-success'
                        ];
                        
                        $dataset_menu[]=[
                                'text' => strtoupper('Tematik Pendukung'),
                                'url'  => route('mod.dataset.dataset.index_doc',['tahun'=>date('Y')]),
                                'icon'=>'nav-icon fa fa-file',
                                // 'classes'=>'bg-warning able'

                        ];
                        $dataset_menu[]= [
                            'text'=>'Sinkronisasi Air Minum',
                            'url'=>route('mod-sink.index',['tahun'=>date('Y')]),
                            'icon'=>'nav-icon fa fa-check'

                        ];

                        $dataset_menu[]=[
                                'text'=>'Evaluasi Dukungan PEMDA Terhadap Kerangka NUWSP',
                                'url'=>route('ev.mod.index',['tahun'=>date('Y')]),
                                'icon'=>'nav-icon fa fa-check'
                        ];

                    


                        $dataset_menu[]=[
                            'header'=>strtoupper('Penginputan / Prosessing Data Kegiatan Proyek'),
                            'classes'=>'border-primary border-top '
                        ];
                        
                        $dataset_menu[]= [
                            'text' => 'KEGIATAN',
                            'url' => route('mod.agenda.index',['tahun'=>date('Y')]),
                            'icon'=>'nav-icon fa fa-clock',

                        ];


            
                
                

                

                }


                $dataset_menu[]=[
                    'header'=>strtoupper('Setting'),
                    'classes'=>'border-primary border-top '
                ];

                
                $dataset_menu[]=[
                    'text'=>'LOKUS NUWSP',
                    'url' => route('dash.si.lokus.index',['tahun'=>date('Y')]),
                    'can'=>'Accesss Lokus',
                    'icon'=>'nav-icon fa fa-bullseye',

                ];
                // $dataset_menu[]=[
                //         'text' => 'Monev',
                //         'url' => route('mod.monev-admin.form.index',['tahun'=>$Stahun]),
                //         'can'=>'Access Monev'
                // ];
                
                $dataset_menu[]=[
                    "text"=>'USERS',
                    'icon'=>'nav-icon fa fa-user-circle',
                    // 'can'=>'non-admin',
                    'submenu'=>[
                        [
                            'text' => 'Role',
                            'url' =>  route('mod.permission.role.index',['tahun'=>date('Y')]),
                            'can'=>'init-superadmin',
                            'icon'=>'text-success fa fa-arrow-right'

                        ],
                        [
                            'text' => 'Permision',
                            'url' =>  route('mod.permission.permis.index',['tahun'=>date('Y')]),
                            'can'=>'init-superadmin',
                            'icon'=>'text-success fa fa-arrow-right'

                        ],
                        [
                            'text' => 'Users',
                            'url' =>  route('mod.permission.user.index',['tahun'=>date('Y')]),
                            'can'=>'init-superadmin',
                            'icon'=>'text-success fa fa-arrow-right'

                        ]
                    ]
                ];
                $dataset_menu[]=[
                    "text"=>'DATASET',
                    'icon'=>'nav-icon fa  fa-inbox',
                    // 'can'=>'non-admin',
                    'submenu'=>[
                        [
                            'text' => strtoupper('Master Entitas Data Schema'),
                            'url' => route('mod.dataset.schema-data.index',['tahun'=>date('Y')]),
                            'can'=>'Access Schema Data',
                            'icon'=>'text-success fa fa-arrow-right'

                        ],
                        [
                            'text' => strtoupper('Master Dataset Schema'),
                            'url' => route('mod.dataset.schema.index',['tahun'=>date('Y')]),
                            'can'=>'Access Schema Dataset',
                            'icon'=>'text-success fa fa-arrow-right'

                        ],
                    ]
                ];
                $dataset_menu[]=[
                    'text' => 'Data '.$StahunRoute,
                    'topnav_right'=>true,
                    'submenu'=>[
                            [
                                'text'=>'Tahun '.(((int)$StahunRoute)-2),
                                'url'=>route('rubah_tahun',['tahun'=>$StahunRoute,'tahun2'=>(((int)$StahunRoute)-2),'pref'=>base64_encode($url_current)])
                            ],
                            [
                                'text'=>'Tahun '.(((int)$StahunRoute)-1),
                                'url'=>route('rubah_tahun',['tahun'=>$StahunRoute,'tahun2'=>(((int)$StahunRoute)-1),'pref'=>base64_encode($url_current)])
                            ],
                            [
                                'text'=>'Tahun '.(((int)$StahunRoute)+1),
                                'url'=>route('rubah_tahun',['tahun'=>$StahunRoute,'tahun2'=>(((int)$StahunRoute)+1),'pref'=>base64_encode($url_current)])

                            ],
                            [
                                'text'=>'Tahun '.(((int)$StahunRoute)+2),
                                'url'=>route('rubah_tahun',['tahun'=>$StahunRoute,'tahun2'=>(((int)$StahunRoute)+2),'pref'=>base64_encode($url_current)])

                            ]
                        ]
                    ];
        

                return $dataset_menu;
            }else{
                $dataset_menu[]=[
                    'header'=>strtoupper('Penginputan / Prosessing Data Tematik'),
                    'classes'=>'border-primary border-top '
                ];
              
            }
        }

        $menu_2= 
           
            [
               
                [
                    'text' => 'Dashbord',
                    'url'  => route('home',['tahun'=>$Stahun]),
                    'icon' => "nav-icon fas fa-toolbox",
                    'topnav_user'=>true
                ],
                [
                    'text' => 'Dataset',
                    'url'  => route('mod.dataset.dataset.index',['tahun'=>$Stahun]),
                    'can'=>'non-admin'
                ],
                [
                    'text' => 'Dataset Pendukung',
                    'url'  => route('mod.dataset.dataset.index_doc',['tahun'=>$Stahun]),
                    'can'=>'non-admin'
                ],
                [
                    'text'=>'Evaluasi Dukungan PEMDA Terhadap Kerangka NUWSP',
                    'url'=>route('ev.mod.index',['tahun'=>date('Y')]),
                ],
                [
                    'text'=>'Sinkronisasi Air Minum',
                    'url'=>route('mod-sink.index',['tahun'=>date('Y')]),
                ],
                [
                    'text'=>'Master',
                    'can'=>'Kelola Master Aplikasi',
                    'submenu'=>[
                        [
                            'text'=>'Lokus',
                            'url' => route('dash.si.lokus.index',['tahun'=>$StahunRoute]),
                            'can'=>'Accesss Lokus'
                        ],
                        [
                            'text' => 'Role',
                            'url' =>  route('mod.permission.role.index',['tahun'=>$Stahun]),
                            'can'=>'init-superadmin'
                        ],
                        [
                            'text' => 'Permision',
                            'url' =>  route('mod.permission.permis.index',['tahun'=>$Stahun]),
                            'can'=>'init-superadmin'
                        ],
                        [
                            'text' => 'Users',
                            'url' =>  route('mod.permission.user.index',['tahun'=>$Stahun]),
                            'can'=>'init-superadmin'
                        ]

                    ],

                ],
                [
                    'text'=>'Schema',
                    'can'=>'Kelola Schema',
                    'submenu'=>[
                        [
                            'text' => 'Data',
                            'url' => route('mod.dataset.schema-data.index',['tahun'=>$StahunRoute]),
                            'can'=>'Access Schema Data'
                        ],
                        [
                            'text' => 'Dataset',
                            'url' => route('mod.dataset.schema.index',['tahun'=>$StahunRoute]),
                            'can'=>'Access Schema Dataset'
                        ],
                        [
                            'text' => 'Parameter Profile',
                            'url' => route('mod.parameter-profile.index',['tahun'=>$StahunRoute]),
                            'can'=>'Access Parameter Profile'

                        ]
                    ],
                    

                ],
              
                [
                    'text' => 'Monev',
                    'url' => route('mod.monev-admin.form.index',['tahun'=>$Stahun]),
                ],
                [
                    'text' => 'Penilaian KPI',
                    'url' => route('mod.kpi.index',['tahun'=>$Stahun]),
                    'can'=>'haveRoleTACT'
                ],
                [
                    'text' => 'Kegiatan',
                    'url' => route('mod.agenda.index',['tahun'=>$Stahun]),
                ],
                [
                    'text'=>'Form Input',
                    'can'=>'Access Form Input',
                    'submenu'=>[
                        [
                            'text' => 'Data PEMDA',
                            'url' => route('mod.dataset.index',['tahun'=>$StahunRoute,'basis'=>'nasional','meta'=>null]),
                            'can'=>'Access Dataset Pemda'

                        ],
                        [
                            'text' => 'Data Pusat',
                            'url' => route('mod.dataset.index',['tahun'=>$StahunRoute,'basis'=>'nasional','meta'=>'pusat']),
                            'can'=>'Access Dataset Pemda'

                        ],
                        [
                            'text' => 'Profile PEMDA',
                            'url' => route('dash.profile-pemda.index',['tahun'=>$StahunRoute]),
                            'can'=>'Access Profile Pemda'
                        ],
                        [
                            'text' => 'Profile BUMDAM',
                            'url' => route('dash.profile-pemda.index',['tahun'=>$StahunRoute]),
                            'can'=>'Access Profile Pemda'
                        ]
                    ]

                ],
                [
                'text' => 'Data '.$StahunRoute,
                'topnav_right'=>true,
                'submenu'=>[
                        [
                            'text'=>'Tahun '.(((int)$StahunRoute)-2),
                            'url'=>route('rubah_tahun',['tahun'=>$StahunRoute,'tahun2'=>(((int)$StahunRoute)-2),'pref'=>base64_encode($url_current)])
                        ],
                        [
                            'text'=>'Tahun '.(((int)$StahunRoute)-1),
                            'url'=>route('rubah_tahun',['tahun'=>$StahunRoute,'tahun2'=>(((int)$StahunRoute)-1),'pref'=>base64_encode($url_current)])
                        ],
                        [
                            'text'=>'Tahun '.(((int)$StahunRoute)+1),
                            'url'=>route('rubah_tahun',['tahun'=>$StahunRoute,'tahun2'=>(((int)$StahunRoute)+1),'pref'=>base64_encode($url_current)])

                        ],
                        [
                            'text'=>'Tahun '.(((int)$StahunRoute)+2),
                            'url'=>route('rubah_tahun',['tahun'=>$StahunRoute,'tahun2'=>(((int)$StahunRoute)+2),'pref'=>base64_encode($url_current)])

                        ]
                    ]
                ],
                [
                    'text'=>'',
                    'topnav_right'=>true,
                    'id'=>'btn-screenshoot',
                    'icon'=>'nav-icon fa fa-camera',
                    'url'=>'#'
                ]

            ];

        return array_merge($dataset_menu,$menu_2);
    }
}




