<?php
namespace App\SlashCommandHandlers;

use Spatie\SlashCommand\Request;
use Spatie\SlashCommand\Response;
use Spatie\SlashCommand\Handlers\BaseHandler;

class ListData extends BaseHandler
{
    public function canHandle(Request $request): bool
    {
        return starts_with($request->text, '/status-data');
    }

    public function handle(Request $request): Response
    {   
        $textWithoutRepeat = substr($request->text, 7);
        
        return $this->respondToSlack("You said {$textWithoutRepeat}");
    }
}