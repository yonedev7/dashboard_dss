
const puppeteer = require('puppeteer');
const { PendingXHR } = require('pending-xhr-puppeteer');
const { functionsIn } = require('lodash');
var md5 = require('md5');
const fs = require('fs');
const { Console } = require('console');



function delay(time) {
    return new Promise(function(resolve) { 
        setTimeout(resolve, time)
    });
} 

async function init(){
        browser = await puppeteer.launch(
      { 
          headless: false,
          timeout:6000000
        }
      
      );

    page =await  browser.newPage();
    pendingXHR = new PendingXHR(page);

    

 }


async function login(){

	if(access_key==null){
	     access_key='bab5d211e987836a9b6446dbf2748c74e667164e';
	}

	await page.goto('https://sipd.kemendagri.go.id/edb_gis/'+access_key+'/?m=dashboard', {
	    waitUntil: 'networkidle2',
	});

	logined=  await page.evaluate(function(){
	    const urlSearchParams = new URLSearchParams(window.location.search);
	    const params = Object.fromEntries(urlSearchParams.entries());
	    if(params.m!=undefined){
	        if(params.m=='login'){
	            return false;
	        }else{
	            return true;
	        }
	    }
	    
	    return false;
	});

	// login
	if(!logined){
	    await page.type('#username', account_user.username);
	    await    page.type('#password', account_user.password);
	    await Promise.all([
	        
	        page.click('[type="submit"]'),
	        page.waitForNavigation({
	                waitUntil: 'networkidle0',
	        })
	    ]);
	    delay(1000);

	     access_key= await page.evaluate(function(){
	           var url_action=window.location.href;
	            if(url_action){
	                url_action=url_action.split('/');
	                if(url_action.length>2){
	                    return url_action[2];
	                }
	            }
	            return url_action;
	        });
	        await login();
	}else{
	    console.log('logined');
	    return true;
	}

}



async function get_daerah(){
	daerah=await page.evaluate(function(){
        var kdd=[];
            $('.sidebar-item a').each(function(k,item){

            var link=$(item).attr('href');
            var urlSearchParams = new URLSearchParams(link);
            var params=Object.fromEntries(urlSearchParams.entries());
           var p=params.prov;
           var k=params.kabkot;
           
           
             if(p||k){
                if(k!='00'){
                    kdd.push(parseInt(p+''+k));

                }else{
                    kdd.push(parseInt(p));
                }
            }

        });

         kdd = kdd.filter(function(value,index,self){
           return self.indexOf(value) === index; 
        });
        
        return kdd;

    });

}


var optional={};
var access_key=null;
var dataset_rkpd={};
var status_rkpd;
var account_user={
    username:"subpkp@bangda.kemendagri.go.id",
    password:"bangdapkp"
};
var daerah=[];
var pendingXHR;
var browser;
var page;



for(var i of  process.argv){
	if(i.includes('--')){
		var b=i.replace('--','').split('=');
		optional[b[0]]=b[1];
	}
}

async function main(){
	await init();
	console.log('egine - ready');
	console.log('try to login');
	await login();
	console.log('login success');
	await get_daerah();
	console.log('listed pemda');
	var index_pemda=0;
	while(daerah.length>index_pemda){
		console.log('get data index '+index_pemda+' kodepemda '+daerah[index_pemda]+' Tahun '+optional.tahun);
		await get_data(daerah[index_pemda],optional.tahun);
		index_pemda++;
	}

}

main();

async function get_data(kodepemda,tahun){
	var env_load={
		prov:null,
		kabkot:null,
		levelAnggaran:null,
		tahun:tahun,
		access_key:access_key
	};

	if(kodepemda.length>2){
		env_load.prov=kodepemda.substring(0,2);
		env_load.kabkot=kodepemda.substring(2,4);
		env_load.levelAnggaran='provinsi';

	}else{

		env_load.prov=kodepemda;
		env_load.kabkot='00';
		env_load.levelAnggaran='kabkot';

	}

	await page.evaluate(function(p){
        status_rkpd=null;
        $.get("https://sipd.kemendagri.go.id/edb_gis/"+p.access_key+"/",{
            m:"dashboard",
            tahun:p.tahun,
            f: "get_jadwal_tahapan",
            prov:p.prov,
            kabkot:p.kabkot
        },function(res){
            status_rkpd=res;
        });

    },env_load);
    await pendingXHR.waitForAllXhrFinished();
    status_rkpd=await page.evaluate(function(){
        return status_rkpd;
    });
    if(status_rkpd.length){
        var last_status=status_rkpd[status_rkpd.length-1];
            tahapan_index=null;
            last_status.nama_sub_tahap=last_status.nama_sub_tahap.replace(/\s+/g,'');
            if(last_status.nama_sub_tahap.includes('PenetapanAwal')){
                tahapan_index=1;
            }else if(last_status.nama_sub_tahap.includes('RancanganRKPA')){
                tahapan_index=2;
            }else if(last_status.nama_sub_tahap.includes('MusrenbangRKPA')){
                tahapan_index=3;
            }else if(last_status.nama_sub_tahap.includes('RancanganAkhirRKPA')){
                tahapan_index=4;
            }else if(last_status.nama_sub_tahap.includes('Penetapan')){
                tahapan_index=5;
            }

            if(tahapan_index){
                env_load['id_tahapan']=last_status.max;
                env_load['tahapan']=tahapan_index;
                env_load['ang_log']=last_status.ang_log;
                env_load['log']=last_status.log;
            }
        
        var kode={
            kodebidang:null,
            kodeskpd:null,
            nama_skpd:null,
            kodeprogram:null,
            kodekegiatan:null,
            kodesubkegiatan:null,
        }


        if(true){
            // next - run
            // mengambil urusan
            await page.evaluate(function(p){
                urusan=null;
                $.get("https://sipd.kemendagri.go.id/edb_gis/"+p.access_key+"/",{
                    m:"dashboard",
                    tahun:p.tahun,
                    f: "perbidangUrusan",
                    prov:p.prov,
                    levelAnggaran:p.levelAnggaran,
                    kabkot:p.kabkot,
                    tahap:p.id_tahapan,
                    log:1
                   
                },function(res){
                    urusan=res;
                });
        
            },{env_load});
            await pendingXHR.waitForAllXhrFinished();
        
            dataset_rkpd['_dataset_per_urusan']=await page.evaluate(function(){
                return urusan;
            });
            delay(100);
            // selesai mengambil urusan
        }
    }
    console.log(env_load);
    console.log(status_rkpd);
    console.log(dataset_rkpd);

    process.exit();
}

