

var myenv=require('dotenv').config({
    path:__dirname+'/../../.env'
}).parsed;
var this_day=null;

p_op=((process.argv).slice(2,10));
egine_option={
    egine_name:'rkpd_sinkron'
};
for(index_op in p_op ){
    index_op=p_op[index_op].replace('--','').split('=');
    egine_option[index_op[0]]=index_op[1];
}

if(egine_option['egine_name']=='rkpd_sinkron'){
    egine_option['egine_name']='rkp_sinkron_'+egine_option['tahun'];
    if(egine_option['reverse']){
        egine_option['egine_name']+='-reverse';
    }
}


console.log(egine_option);


function trimx(text){
    if(text!=null){
        return text.replace(/\s+/g,' ');
    }else{
        return '';
    }
}

const puppeteer = require('puppeteer');
const { PendingXHR } = require('pending-xhr-puppeteer');
const { functionsIn, update } = require('lodash');
var md5 = require('md5');
const fs = require('fs');
const { Console, info } = require('console');
const { get } = require('https');

var dataset_rkpd={};
var access_key=null;
var status_rkpd;
var account_user={
    username:"subpkp@bangda.kemendagri.go.id",
    password:"bangdapkp"
};
var db_con={
    user:myenv.DB_USERNAME,
    password:myenv.DB_PASSWORD,
    database:myenv.DB_DATABASE,
    port:myenv.DB_PORT,
    host:myenv.DB_HOST
}



var access_data={
    tahun_data:egine_option['tahun']||2021,
    kodedaerah:egine_option['kodepemda']||"00",
    tahap:egine_option['tahap']||5
};


async function exist_name(ind){
    if(ind!='-' && ind!='' && ind && ind!='0'){
        return true;
    }else{
        return false;
    }
} 

const knex = require('knex')({
    client: 'pg',
    connection: db_con,
    userParams:{
        tahun:access_data.tahun_data
    }
});


var schema='rkpd_'+access_data.tahun_data;
var index_daerah=0;
var interasi=1;
var daerah=[];
async function get_sinkron(){
    var index_loop=0;
    while(daerah.length>index_loop){
        if(index_loop!=0){
            if(index_loop%100==0){
                await page.goto('https://sipd.kemendagri.go.id/edb_gis/'+access_key+'/?m=dashboard', {
                    waitUntil: 'networkidle2',
                });
            }
        }
        var kodepem=daerah[index_loop]+'';
        
        console.log('updated_db',kodepem);
        var ddd={
            prov:11,
            kab:'00',
            levelAnggaran:'provinsi'
            
        };

        if(kodepem.length>2){
       
                ddd.prov=kodepem.substring(0,2);
                ddd.kab=kodepem.substring(2,4);
                ddd.levelAnggaran='provinsi';
                ddd.tahun=egine_option['tahun']
        }else{
            ddd.prov=kodepem.substring(0,2);
            ddd.kab='00';
            ddd.levelAnggaran='provinsi';
            ddd.tahun=egine_option['tahun'];
        }

        await page.evaluate(function(dta){
            sink_rkpd=null;
            $.get("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/",{
                m:"dashboard",
                tahun:dta.tahun,
                f: "get_jadwal_tahapan",
                prov:dta.prov,
                kabkot:dta.kab
            },function(res){
                sink_rkpd=res;
            });
    
        },ddd);
        await pendingXHR.waitForAllXhrFinished();
        delay(200);
        sink_rkpd=await page.evaluate(function(){
            return sink_rkpd;
        });
        delay(100);
        
        if(sink_rkpd.length!=0){
            var thap= await getTahap(sink_rkpd[sink_rkpd.length-1].nama_sub_tahap);
             sink_rkpd=sink_rkpd[sink_rkpd.length-1];
            
            await knex.table(schema+'.sinkron_rkpd').select('id').where({
                kodepemda:'0'+kodepem
            }).then(async (k)=>{
                if(k.length){
                    await knex.table(schema+'.sinkron_rkpd').where({'id':k[0].id}).update({
                        'kodepemda':'0'+kodepem,
                        'log_sipd':sink_rkpd.log,
                        'tahun':egine_option['tahun'],
                        'log_ang_sipd':sink_rkpd.ang_log,
                        'kode_daerah_sipd':sink_rkpd.id_daerah,
                        'tahap':thap
                    });

                }else{
                    await knex.table(schema+'.sinkron_rkpd').insert({
                        'kodepemda':'0'+kodepem,
                        'log_sipd':sink_rkpd.log,
                        'tahun':egine_option['tahun'],
                        'log_ang_sipd':sink_rkpd.ang_log,
                        'kode_daerah_sipd':sink_rkpd.id_daerah,
                        'tahap':thap
                    });
                }
            })
        }
        index_loop++;

    }
}

var list_updated=[];
async function get_not_match(){
        var sql_raw="(select s.kodepemda,max(m.rpjmd_awal) as m_rpjmd_awal,max(m.rpjmd_ahir) as m_rpjmd_ahir,max(m.tahap) as m_tahap,max(m.log_sipd) as m_log_sipd,max(m.log_ang_sipd) as m_log_ang_sipd,max(s.rpjmd_awal) as s_rpjmd_awal,max(s.rpjmd_ahir) as s_rpjmd_ahir, max(s.tahap) as s_tahap,max(s.log_sipd) as s_log_sipd,max(s.log_ang_sipd) as s_log_ang_sipd from "+schema+".sinkron_rkpd as s left join "+schema+".meta_rkpd as m on m.kodepemda=s.kodepemda where (((s.tahap) <> (m.tahap)) or (m.log_sipd != s.log_sipd) or (m.log_ang_sipd != s.log_ang_sipd) ) or (m.log_sipd is null) or (m.log_ang_sipd <> m.log_ang_local) group by s.kodepemda ) as lrkpd";
  
        var yy=await knex.select('kodepemda').from(knex.raw(sql_raw)).then(async (kodepemda_list)=>{
             
              for(var i in kodepemda_list){
                  list_updated[i]=(kodepemda_list[i].kodepemda+'').slice(1,5);
              }

              if(egine_option['reverse']){
                  list_updated=list_updated.reverse();
              }
          });
}


async function  delay(time) {
    return new Promise(function(resolve) { 
        setTimeout(resolve, time)
    });
} 

var browser;
var page;  
var pendingXHR;  



 async function init(){
        browser = await puppeteer.launch(
      { 
          headless: true,
          timeout:6000000,
          args: ['--no-sandbox', '--disable-setuid-sandbox']
        }
      
      ); // default is true


    page =await  browser.newPage();
    pendingXHR = new PendingXHR(page);


 }

 async function login(){
       if(access_key==null){
             access_key='bab5d211e987836a9b6446dbf2748c74e667164e';
        }

        await page.goto('https://sipd.kemendagri.go.id/edb_gis/'+access_key+'/?m=dashboard', {
            waitUntil: 'networkidle2',
        });

        logined=  await page.evaluate(function(){
            const urlSearchParams = new URLSearchParams(window.location.search);
            const params = Object.fromEntries(urlSearchParams.entries());
            if(params.m!=undefined){
                if(params.m=='login'){
                    return false;
                }else{
                    return true;
                }
            }
            
            return false;
        });

        // login
        if(!logined){
            await page.type('#username', account_user.username);
            await    page.type('#password', account_user.password);
            await Promise.all([
                
                page.click('[type="submit"]'),
                page.waitForNavigation({
                        waitUntil: 'networkidle0',
                })
            ]);
            delay(1000);

             access_key= await page.evaluate(function(){
                   var url_action=window.location.href;
                    if(url_action){
                        url_action=url_action.split('/');
                        if(url_action.length>2){
                            return url_action[2];
                        }
                    }
                    return url_action;
                });
                await login();
        }else{
            console.log('logined');
            return true;
        }

    
     
}

var id_node='xxx'+egine_option['egine_name'];
var name_node=egine_option['egine_name'];

async function getTahap(nama){
    nama=nama.split(' -')[0].replace(/\s+/g,'').toUpperCase();
    switch(nama){
        case "RANCANGANAWAL":
            return 1;
        break;
        case "RANCANGAN":
            return 2;
        break;
        case "MUSRENBANG":
            return 3;
        break;
        case "RANCANGANAKHIR":
            return 4;
        break;
        case "PENETAPAN":
            return 5;
        break;
        default:
            return 6;
        break
    }
}

async function get_data(){    
   if(access_data.kodedaerah.length>2){
    access_data['kode_wil']={
        prov:access_data.kodedaerah.substring(0,2),
        kabkot:access_data.kodedaerah.substring(2,4),
        levelAnggaran:'kabkota'

    };
   

    }else{
        access_data['kode_wil']={
            prov:access_data.kodedaerah.substring(0,2),
            kabkot:"00",
            levelAnggaran:'provinsi'
        };
       
    }




    var next_step=true;

    await page.evaluate(function(dta){
        status_rkpd=null;
        $.get("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/",{
            m:"dashboard",
            tahun:dta.access_data.tahun_data,
            f: "get_jadwal_tahapan",
            prov:dta.access_data.kode_wil.prov,
            kabkot:dta.access_data.kode_wil.kabkot
        },function(res){
            status_rkpd=res;
        });

    },{access_data:access_data,access_key:access_key});
    await pendingXHR.waitForAllXhrFinished();

    status_rkpd=await page.evaluate(function(){
        return status_rkpd;
    },{access_data:access_data,access_key:access_key});

    await page.evaluate(function(dta){
        info_pemda=null;
        $.get("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/",{
            m:"dashboard",
            f: "get_info_pemda",
            prov:dta.access_data.kode_wil.prov,
            kabkot:dta.access_data.kode_wil.kabkot
        },function(res){
            info_pemda=JSON.parse(res);
        });

    },{access_data:access_data,access_key:access_key});
    await pendingXHR.waitForAllXhrFinished();

    info_pemda=await page.evaluate(function(){
        return info_pemda;
    },{access_data:access_data,access_key:access_key});

 
    if(status_rkpd.length==0){
        next_step=false;
    }


    // dataset_rkpd['semua_status']=status_rkpd;
    dataset_rkpd={
        'kodedaerah':access_data.kodedaerah,
        'tahun':access_data.tahun_data,
        'updated_at':new Date(),
        
    };
    if(next_step){

        if(status_rkpd[access_data.tahap-1]!=undefined){
            access_data['status_data']=status_rkpd[access_data.tahap-1];
            access_data['tahap']=access_data.tahap;

        }else{
            access_data['status_data']=status_rkpd[status_rkpd.length-1];
            access_data['tahap']=getTahap(status_rkpd[status_rkpd.length-1]['nama_sub_tahap']);
        }


        dataset_rkpd['status_data']=access_data['status_data'];
        dataset_rkpd['tahap']=access_data['tahap'];
        dataset_rkpd['dataset']=[];

    }
  
    db_req={};
    if(next_step){
        await knex.table('master.master_pemda').update({
            kode_sipd:dataset_rkpd.status_data.id_daerah,
            kode_bps:info_pemda.kodebps,
            kode_krisna:info_pemda.kodekrisna,
            domain_sipd:trimx(info_pemda.domainurl),
            api_key_sipd:trimx(info_pemda['apikey']||'')

        }).where(
            {
                kodepemda: '0'+dataset_rkpd.kodedaerah,
            
            }
        );

        if(info_pemda.visi.idperiode){
            await knex.select('id').from('rpjmd.visi').where({
                kodepemda:'0'+dataset_rkpd.kodedaerah,
                idperiode:trimx(info_pemda.visi.idperiode),
            }).then(async (idx)=>{
                if(idx.length){
                }else{
                    await knex.table('rpjmd.visi').insert({
                        kodepemda:'0'+dataset_rkpd.kodedaerah,
                        idperiode:trimx(info_pemda.visi.idperiode),
                        uraivisi:trimx(info_pemda.visi.uraivisi),
                        nama_kd:trimx(info_pemda.info.nama_kd)
                    });
                }
            });
        }
    }

  if(next_step){
    db_req['id_meta'] = await knex.select('id').from(schema+'.meta_rkpd').where(
        {
            kodepemda: '0'+dataset_rkpd.kodedaerah,
            tahap:dataset_rkpd.tahap,
            tahun:dataset_rkpd.tahun
        }
    ).then(async (idn)=>{
        if(!(idn.length)){
            newid=await knex(schema+'.meta_rkpd').insert(
                {
                    kodepemda:'0'+dataset_rkpd.kodedaerah,
                    tahap:dataset_rkpd.tahap,
                    tahun:dataset_rkpd.tahun,
                    kode_daerah_sipd:dataset_rkpd.status_data.id_daerah,
                    log_ang_sipd:dataset_rkpd.status_data.ang_log,
                    log_sipd:dataset_rkpd.status_data.log,
                    basis_nomenklatur:info_pemda.nomenklatur,
                    updated_at:dataset_rkpd.updated_at
                 }
            ).returning('id');
            return newid[0];
        }else{
            return (idn[0].id);
        }
    });

  }

   
  if(next_step){

    await page.evaluate(function(dta){
        dss_buffer=null;
        $.get("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/",{
            m:"dashboard",
            tahun:dta.access_data.tahun_data,
            f: "perbidangUrusan",
            prov:dta.access_data.kode_wil.prov,
            levelAnggaran:dta.access_data.levelAnggaran,
            kabkot:dta.access_data.kode_wil.kabkot,
            tahap:dta.access_data.status_data.max,
            log:1
           
        },function(res){
            dss_buffer=res;
        });

    },{access_data:access_data,access_key:access_key});
    await pendingXHR.waitForAllXhrFinished();

    dataset_rkpd['_dataset_per_urusan']=await page.evaluate(function(){
        return dss_buffer;
    },{access_data:access_data,access_key:access_key});
    delay(100);
  }else{
    dataset_rkpd['_dataset_per_urusan']=[];
  }


 


 

    index_urusan=0;
    while(dataset_rkpd['_dataset_per_urusan'].length>index_urusan){
        var mess='Egine :'+id_node+' ('+name_node+'), Iterasi : '+interasi+' -- ';
        if(egine_option['provinsi']){
        mess+=" "+(index_daerah+1)+'/'+daerah.length+' Daerah';
        }
        console.log(mess);
        console.log('-- RKPD '+access_data.kodedaerah+' Tahun '+access_data.tahun_data+' Tahap '+access_data.tahap+' -- '+(index_urusan/dataset_rkpd['_dataset_per_urusan'].length*100)+'%');
        dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd']=[];
        var ko_b=dataset_rkpd['_dataset_per_urusan'][index_urusan]['kodebidang'];
        if(true){
        var parent_data={
            tahun:access_data.tahun_data,
            prov:access_data.kode_wil.prov,
            kabkot:access_data.kode_wil.kabkot,
            kodebidang:dataset_rkpd['_dataset_per_urusan'][index_urusan].kodebidang,
            log:1,
            jadwal:access_data.status_data.max,
            tab:"bidang"
        };
        

        if(db_req['kode_bidang']!=dataset_rkpd['_dataset_per_urusan'][index_urusan].kodebidang){
            db_req['kode_bidang'] = await knex.select('kodebidang').from('master.master_urusan').where(
                {
                    kodebidang:dataset_rkpd['_dataset_per_urusan'][index_urusan].kodebidang
                }
            ).then(async (idn)=>{
                if(!(idn.length)){
                    newid=await knex('master.master_urusan').insert(
                        {
                            kodebidang:parent_data['kodebidang'],
                            nama_bidang:dataset_rkpd['_dataset_per_urusan'][index_urusan]['uraibidang'],

                        }
                    ).returning('kodebidang');
                    return newid[0];
                }else{
                    return (idn[0].kodebidang);
                }
            });
        }

        await page.evaluate(function(dta){
            dss_buffer=null;
            $.post("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/?m=dashboard&f=perbidang_skpd",{
                tahun:dta.tahun,
                prov:dta.prov,
                kodekot:dta.kabkot,
                kodebidang:dta.kodebidang,
                log:dta.log,
                jadwal:dta.jadwal,
                tab:dta.tab,
            },function(res){
                dss_buffer=res;
            });

        },parent_data);
        await pendingXHR.waitForAllXhrFinished();

        dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd']=await page.evaluate(function(){
            return dss_buffer;
        },{access_data:access_data,access_key:access_key});
        delay(100);

            index_skpd=0;
            while(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'].length>index_skpd){
                // ambil program skpd
                
                parent_data['id_skpd']=dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd].id_unit;
                parent_data['kode_skpd']=dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd].kode_skpd||'null';

                dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program']=[];

               
                if(db_req['kodeskpd']!=parent_data['kode_skpd']||db_req['kodebindang']!=parent_data['kodebidang']){
                    db_req['kodeskpd'] = await knex.select('kodeskpd').from('master.master_sipd_pd').where(
                        {
                            kodepemda: '0'+dataset_rkpd.kodedaerah,
                            // kodebidang:parent_data['kodebidang'],
                            kodeskpd:parent_data['kode_skpd'],
                            
                        }
                    ).then(async (idn)=>{
                        if(!(idn.length)){
                            newid=await knex('master.master_sipd_pd').insert(
                                {
                                    kodepemda: '0'+dataset_rkpd.kodedaerah,
                                    kodebidang:parent_data['kodebidang'],
                                    kodeskpd:parent_data['kode_skpd'],
                                    nama_skpd:trimx(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['nama_skpd']||'null'),
                                    updated_at:dataset_rkpd.updated_at
                                }
                            ).returning('kodeskpd');
                            return newid[0];
                        }else{
                            return (idn[0].kodeskpd);
                        }
                    });
                }
        
        
                await page.evaluate(function(dta){
                    dss_buffer=null;
                    $.post("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/?m=dashboard&f=program",{
                        tahun:dta.tahun,
                        prov:dta.prov,
                        kodekot:dta.kabkot,
                        kodebidang:dta.kodebidang,
                        skpd:dta.id_skpd,
                        log:dta.log,
                        jadwal:dta.jadwal,
                        tab:dta.tab,
                    },function(res){
                        dss_buffer=res;
                    });

                },parent_data);
                await pendingXHR.waitForAllXhrFinished();

                dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program']=await page.evaluate(function(){
                    return dss_buffer;
                },{access_data:access_data,access_key:access_key});
                delay(100);


                index_program=0;

                while( dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'].length>index_program){
                    // ambil kegiatan
                    parent_data['kode_program']=dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program].kode_program;

                    dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan']=[];
                    if(parent_data['kode_program'].length>200){
                        console.log('too longss------',{
                            kode:parent_data['kode_program'],
                            tag:2,
                            tahun_penambahan:access_data.tahun_data,
                            p:(''+dataset_rkpd.kodedaerah).length>3?false:true,
                            kode_urusan:parent_data['kodebidang'].split('.')[0],
                            kodebidang:parent_data['kodebidang'],
                            nama:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program].nama_program
                                   
                        });
                        process.exit();
                    }

                   if(db_req['kodeprogram']!=parent_data['kode_program']){
                        db_req['kodeprogram']=await knex.select('kode').from('master.master_nomenklatur_rkpd').where({
                            kode:parent_data['kode_program'],
                            tag:2,
                            p:(''+dataset_rkpd.kodedaerah).length>3?false:true,
                        }).then(async (dta)=>{
                            if(!(dta.length)){
                                return await knex('master.master_nomenklatur_rkpd').insert(
                                    {
                                        kode:parent_data['kode_program'],
                                        tag:2,
                                        tahun_penambahan:access_data.tahun_data,
                                        p:(''+dataset_rkpd.kodedaerah).length>3?false:true,
                                        kode_urusan:parent_data['kodebidang'].split('.')[0],
                                        kodebidang:parent_data['kodebidang'],
                                        nama:trimx(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program].nama_program)
                                    }
                                ).returning('kode');
                            }else{
                                return dta[0].kode;
                            }

                        });
                    }
            
                    await page.evaluate(function(dta){
                        dss_buffer=null;
                        $.post("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/?m=dashboard&f=kegiatan",{
                            tahun:dta.tahun,
                            prov:dta.prov,
                            kodekot:dta.kabkot,
                            kodebidang:dta.kodebidang,
                            skpd:dta.id_skpd,
                            log:dta.log,
                            program:dta.kode_program,
                            jadwal:dta.jadwal,
                            tab:dta.tab,
                        },function(res){
                            dss_buffer=res;
                        });
        
                    },parent_data);
                    await pendingXHR.waitForAllXhrFinished();
        
                    dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan']=await page.evaluate(function(){
                        return dss_buffer;
                    },{access_data:access_data,access_key:access_key});
                    
                    delay(100);

                    // ambil outcome
                    dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator']=[];
                    
                    await page.evaluate(function(dta){
                        dss_buffer=null;
                        $.post("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/?m=dashboard&f=indikator_prog",{
                            tahun:dta.tahun,
                            prov:dta.prov,
                            kodekot:dta.kabkot,
                            kodebidang:dta.kodebidang,
                            skpd:dta.kode_skpd,
                            log:dta.log,
                            program:dta.kode_program,
                            jadwal:dta.jadwal,
                            tab:dta.tab,
                        },function(res){
                            dss_buffer=res;
                        });
        
                    },parent_data);

                    await pendingXHR.waitForAllXhrFinished();
        
                    dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator']=await page.evaluate(function(){
                        return dss_buffer;
                    },{access_data:access_data,access_key:access_key});
                    delay(100);
                    index_program_ind=0;

                    while(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'].length>index_program_ind){
                        if(await exist_name(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'][index_program_ind].namaoutcome)){
                            var kodeindp=md5(dataset_rkpd.kodedaerah+'|'+dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'][index_program_ind].tahun_rpjmd_awal+'-'+dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'][index_program_ind].tahun_rpjmd_akhir+'-'+trimx(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'][index_program_ind].namaoutcome));
                            var rpjmd_awal=dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'][index_program_ind].tahun_rpjmd_awal;
                            var rpjmd_ahir=dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'][index_program_ind].tahun_rpjmd_akhir;
                            
                            if(db_req['rpjmd_awal']!=rpjmd_awal||db_req['rpjmd_ahir']!=rpjmd_ahir){
                                db_req['rpjmd_awal']=rpjmd_awal;
                                db_req['rpjmd_ahir']=rpjmd_ahir;
                                await knex.table(schema+".meta_rkpd").update({
                                    'rpjmd_awal':rpjmd_awal,
                                    'rpjmd_ahir':rpjmd_ahir

                                }).where({'id':db_req['id_meta']});
                            }

                           db_req['id_ip']=await knex.select('id').from('rpjmd.indikator_rpjmd').where(
                                {
                                    kodepemda:'0'+dataset_rkpd.kodedaerah,
                                    rpjmd_awal: db_req['rpjmd_awal'],
                                    rpjmd_ahir: db_req['rpjmd_ahir'],
                                    kodebidang:dataset_rkpd['_dataset_per_urusan'][index_urusan].kodebidang,
                                    kodeskpd:parent_data['kode_skpd'],
                                    kodeprogram:parent_data['kode_program'],
                                    kodeindikator:kodeindp,
                                    jenis:3
        
                                }
                            ).then(async (idn)=>{
                                if((idn.length==0)){
                                    var newid=await knex('rpjmd.indikator_rpjmd').insert(
                                        {
                                            kodepemda:'0'+dataset_rkpd.kodedaerah,
                                            rpjmd_awal: db_req['rpjmd_awal'],
                                            rpjmd_ahir: db_req['rpjmd_ahir'],
                                            kodebidang:parent_data['kodebidang'],
                                            kodeskpd:parent_data['kode_skpd'],
                                            kodeprogram:parent_data['kode_program'],
                                            kodeindikator:kodeindp,
                                            jenis:3,
                                            target_1:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'][index_program_ind].target_1,
                                            target_2:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'][index_program_ind].target_2,
                                            target_3:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'][index_program_ind].target_3,
                                            target_4:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'][index_program_ind].target_4,
                                            target_5:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'][index_program_ind].target_5,
                                            namaindikator:trimx(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'][index_program_ind].namaoutcome),
                                            satuan:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'][index_program_ind].satuanoutcome||'',
                                            updated_at:dataset_rkpd.updated_at
                                        }
                                    ).returning('id');
                                    return newid[0];
                                }else{
                                    var idxx=(idn[0].id);
                                    var target_f=(rpjmd_awal-access_data.tahun_data)+1;
                                    if(target_f>5){
                                        target_f=5;
                                    }else if(target_f<1){
                                        target_f=1;
                                    }
                                    if(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'][index_program_ind]['target_'+target_f]){
                                        await knex.select('target_'+target_f).from('rpjmd.indikator_rpjmd').where({
                                            id:idxx
                                        }).then(async (target)=>{
                                            if(target[0]['target_'+target_f]==null){
                                                var upin={};
                                                upin['target_'+target_f]=dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'][index_program_ind]['target_'+target_f];
                                                await knex('rpjmd.indikator_rpjmd').update(upin).where({
                                                    id:idxx
                                                });
                                            }
                                        });
                                    }
                                    return idxx;
                                }
                            });

                            await knex.select('id').from(schema+'.indikator_rpjmd').where({
                                id_meta:db_req['id_meta'],
                                id_indikator:db_req['id_ip']
                            }).then(async (idx)=>{
                                if((idx.length==0)){
                                    await knex.table('schema+'.indikator_rpjmd).insert({
                                        id_meta:db_req['id_meta'],
                                        id_indikator:db_req['id_ip']
                                    });
                                }
                                
                            });
    
                        }

                        
                        index_program_ind++;
                    }

                    index_kegiatan=0;

                    while(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'].length>index_kegiatan){
                        // ambil sub kegiatan
                        dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan']=[];
                        parent_data['kode_kegiatan']=dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan].kode_giat;
                        if(parent_data['kode_kegiatan'].length>200){
                            console.log('to longs-----',{
                                tag:3,
                                kode:parent_data['kode_kegiatan'],
                                tahun_penambahan:dataset_rkpd.tahun,
                                p:(''+dataset_rkpd.kodedaerah).length>3?false:true,
                                kode_urusan:parent_data['kodebidang'].split('.')[0],
                                kodebidang:parent_data['kodebidang'],
                                kodeprogram:parent_data['kode_program'],
                                nama:trimx(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan].nama_giat)
                            });
                            process.exit();
                        }



                        if(db_req['kodekegitan']!=parent_data['kode_kegiatan']){
                            db_req['kodekegitan']= await knex.select('id').from('master.master_nomenklatur_rkpd').where({
                                    kode:parent_data['kode_kegiatan'],
                                    tag:3,
                                    p:(''+dataset_rkpd.kodedaerah).length>3?false:true,
                                }).then(async (dta)=>{
                                    if(!(dta.length)){
                                    return await knex('master.master_nomenklatur_rkpd').insert(
                                            {
                                                tag:3,
                                                kode:parent_data['kode_kegiatan'],
                                                tahun_penambahan:dataset_rkpd.tahun,
                                                p:(''+dataset_rkpd.kodedaerah).length>3?false:true,
                                                kode_urusan:parent_data['kodebidang'].split('.')[0],
                                                kodebidang:parent_data['kodebidang'],
                                                kodeprogram:parent_data['kode_program'],
                                                nama:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan].nama_giat
                                            }
                                        ).returning('kode');
                                    }else{
                                        return dta[0].kode;
                                    }

                                });
                        }
                        
                    
                        await page.evaluate(function(dta){
                            dss_buffer=null;
                            
                            $.post("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/?m=dashboard&f=subkegiatan",{
                                tahun:dta.tahun,
                                prov:dta.prov,
                                kodekot:dta.kabkot,
                                kodebidang:dta.kodebidang,
                                skpd:dta.id_skpd,
                                log:dta.log,
                                program:dta.kode_program,
                                jadwal:dta.jadwal,
                                tab:dta.tab,
                                kegiatan:dta.kode_kegiatan
                            },function(res){
                                dss_buffer=res;
                            });
            
                        },parent_data);
                        await pendingXHR.waitForAllXhrFinished();
            
                        dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan']=await page.evaluate(function(){
                            return dss_buffer;
                        },{access_data:access_data,access_key:access_key});
                        delay(100);



                        // ambil indikator kegiatan
                        dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator']=[];

                        await page.evaluate(function(dta){
                            dss_buffer=null;
                            $.post("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/?m=dashboard&f=indikator_keg",{
                                tahun:dta.tahun,
                                prov:dta.prov,
                                kodekot:dta.kabkot,
                                kodebidang:dta.kodebidang,
                                skpd:dta.kode_skpd,
                                log:dta.log,
                                program:dta.kode_program,
                                jadwal:dta.jadwal,
                                tab:dta.tab,
                                kegiatan:dta.kode_kegiatan
                            },function(res){
                                dss_buffer=res;
                            });
            
                        },parent_data);
                        await pendingXHR.waitForAllXhrFinished();
            
                        dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator']=await page.evaluate(function(){
                            return dss_buffer;
                        },{access_data:access_data,access_key:access_key});
                        delay(100);

                        // xxxx
                        if(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator']==null){
                            dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator']=[];
                        }
                        index_kegiatan_ind=0;
                        while(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator'].length>index_kegiatan_ind){
                            if(await exist_name(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator'][index_kegiatan_ind].namaoutput)){
                                var kodeindp=md5(dataset_rkpd.kodedaerah+'|'+dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator'][index_kegiatan_ind].tahun_rpjmd_awal+'-'+dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator'][index_kegiatan_ind].tahun_rpjmd_akhir+'-'+trimx(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator'][index_kegiatan_ind].namaoutput));
                                var rpjmd_awal=dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator'][index_kegiatan_ind].tahun_rpjmd_awal;
                                var rpjmd_ahir=dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator'][index_kegiatan_ind].tahun_rpjmd_akhir;
                               
                               db_req['id_ik']= await knex.select('id').from('rpjmd.indikator_rpjmd').where(
                                    {
                                        kodepemda: '0'+dataset_rkpd.kodedaerah,
                                        rpjmd_awal:rpjmd_awal,
                                        rpjmd_ahir:rpjmd_ahir,
                                        kodebidang:dataset_rkpd['_dataset_per_urusan'][index_urusan].kodebidang,
                                        kodeskpd:parent_data['kode_skpd'],
                                        kodeprogram:parent_data['kode_program'],
                                        kodekegiatan:parent_data['kode_kegiatan'],
                                        kodeindikator:kodeindp,
                                        jenis:4,
                                    }
                                ).then(async (idn)=>{
                                    if(!(idn.length)){
                                        newid=await knex('rpjmd.indikator_rpjmd').insert(
                                            {
                                                kodepemda: '0'+dataset_rkpd.kodedaerah,
                                                rpjmd_awal:rpjmd_awal,
                                                rpjmd_ahir:rpjmd_ahir,
                                                kodeskpd:parent_data['kode_skpd'],
                                                jenis:4,
                                                kodebidang:parent_data['kodebidang'],
                                                kodeprogram:parent_data['kode_program'],
                                                kodekegiatan:parent_data['kode_kegiatan'],
                                                kodeindikator:kodeindp,
                                                target_1:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator'][index_kegiatan_ind].target_1,
                                                target_2:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator'][index_kegiatan_ind].target_2,
                                                target_3:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator'][index_kegiatan_ind].target_3,
                                                target_4:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator'][index_kegiatan_ind].target_4,
                                                target_5:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator'][index_kegiatan_ind].target_5,
                                                namaindikator:trimx(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator'][index_kegiatan_ind].namaoutput),
                                                satuan:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator'][index_kegiatan_ind].satuanoutput||'',
                                                updated_at:dataset_rkpd.updated_at
                                            }
                                        ).returning('id');


                                        return newid[0];
                                    }else{
                                        var idxx=(idn[0].id);
                                        var target_f=(rpjmd_awal-access_data.tahun_data)+1;
                                        if(target_f>5){
                                            target_f=5;
                                        }else if(target_f<1){
                                            target_f=1;
                                        }
                                        if(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator'][index_kegiatan_ind]['target_'+target_f]){
                                            await knex.select('target_'+target_f).from('rpjmd.indikator_rpjmd').where({
                                                id:idxx
                                            }).then(async (target)=>{
                                                if(target[0]['target_'+target_f]==null){
                                                    var upin={};
                                                    upin['target_'+target_f]=dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator'][index_kegiatan_ind]['target_'+target_f];
                                                    await knex('rpjmd.indikator_rpjmd').update(upin).where({
                                                        id:idxx
                                                    });
                                                }
                                            });
                                        }
                                        return idxx;
                                    }
                                });

                                await knex.select('id').from(schema+'.indikator_rpjmd').where({
                                    id_meta:db_req['id_meta'],
                                    id_indikator:db_req['id_ik']
                                }).then((idx)=>{
                                    if(!(idx.length)){
                                        knex.table('schema+'.indikator_rpjmd).insert({
                                            id_meta:db_req['id_meta'],
                                            id_indikator:db_req['id_ik']
                                        });
                                    }
                                    
                                });
                            }

                            index_kegiatan_ind++;
                        }

                        index_sub_kegiatan=0;

                        while(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'].length>index_sub_kegiatan){
                            // ambil indikator sub kegiatan
                            parent_data['kode_sub_kegiatan']=dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'][index_sub_kegiatan].kode_sub_giat;
                            dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'][index_sub_kegiatan]['_indikator']=[];
                           
                            if(parent_data['kode_sub_kegiatan'].length>200){
                                console.log('too longss------',{
                                    kode:parent_data['kode_sub_kegiatan'],
                                    tag:4,
                                    tahun_penambahan:dataset_rkpd.tahun,
                                    p:(''+dataset_rkpd.kodedaerah).length>3?false:true,
                                    kode_urusan:parent_data['kodebidang'].split('.')[0],
                                    kodebidang:parent_data['kodebidang'],
                                    kodeprogram:parent_data['kode_program'],
                                    kodekegiatan:parent_data['kode_kegiatan'],
                                    nama:trimx(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'][index_sub_kegiatan].nama_sub_giat)
                                    
                                });
                                process.exit();
                            }

                            
                            if(db_req['kode_sub_kegiatan']!=parent_data['kode_sub_kegiatan']){
                                db_req['kode_sub_kegiatan']=await knex.select('kode').from('master.master_nomenklatur_rkpd').where({
                                    kode:parent_data['kode_sub_kegiatan'],
                                    tag:4,
                                    p:(''+dataset_rkpd.kodedaerah).length>3?false:true,
                                }).then(async (dta)=>{
                                    if(!(dta.length)){
                                        var newid= await knex('master.master_nomenklatur_rkpd').insert(
                                            {
                                                kode:parent_data['kode_sub_kegiatan'],
                                                tag:4,
                                                tahun_penambahan:dataset_rkpd.tahun,
                                                p:(''+dataset_rkpd.kodedaerah).length>3?false:true,
                                                kode_urusan:parent_data['kodebidang'].split('.')[0],
                                                kodebidang:parent_data['kodebidang'],
                                                kodeprogram:parent_data['kode_program'],
                                                kodekegiatan:parent_data['kode_kegiatan'],
                                                nama:trimx(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'][index_sub_kegiatan].nama_sub_giat)
                                            }
                                        ).returning('kode');
                                        return newid[0];
                                    }else{
                                        return dta[0].kode;
                                    }

                                });
                            }

                            parent_data['id_sub_kegiatan']=await knex.select('id').from(schema+'.sub_kegiatan').where({
                                kodesubkegiatan:parent_data['kode_sub_kegiatan'],
                                kodeskpd:parent_data['kode_skpd'],
                                kodebidang:parent_data['kodebidang'],
                                kodeprogram:parent_data['kode_program'],
                                kodekegiatan:parent_data['kode_kegiatan'],
                                id_meta:db_req['id_meta'],
                            }).then(async (dta)=>{
                                if(!(dta.length)){
                                    var newid= await knex(schema+'.sub_kegiatan').insert(
                                        {
                                            id_meta:db_req['id_meta'],
                                            kodeskpd:parent_data['kode_skpd'],
                                            kodebidang:parent_data['kodebidang'],
                                            kodeprogram:parent_data['kode_program'],
                                            kodekegiatan:parent_data['kode_kegiatan'],
                                            kodesubkegiatan:parent_data['kode_sub_kegiatan'],
                                            pagu:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'][index_sub_kegiatan].sum,
                                        }
                                    ).returning('id');
                                    return newid[0];
                                  
                                }else{
                                    var newid= await knex(schema+'.sub_kegiatan').update(
                                        {
                                            id_meta:db_req['id_meta'],
                                            kodeskpd:parent_data['kode_skpd'],
                                            kodebidang:parent_data['kodebidang'],
                                            kodeprogram:parent_data['kode_program'],
                                            kodekegiatan:parent_data['kode_kegiatan'],
                                            kodesubkegiatan:parent_data['kode_sub_kegiatan'],
                                            pagu:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'][index_sub_kegiatan].sum,
                                        }
                                    ).where({id:dta[0].id}).returning('id');
                                    return newid[0];
                                }

                            });

                            
                        


                            await page.evaluate(function(dta){
                                dss_buffer=null;
                                $.post("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/?m=dashboard&f=indikator_sub_keg",{
                                    tahun:dta.tahun,
                                    prov:dta.prov,
                                    kodekot:dta.kabkot,
                                    kodebidang:dta.kodebidang,
                                    skpd:dta.kode_skpd,
                                    log:dta.log,
                                    program:dta.kode_program,
                                    jadwal:dta.jadwal,
                                    tab:dta.tab,
                                    kegiatan:dta.kode_kegiatan,
                                    subkegiatan:dta.kode_sub_kegiatan
                                },function(res){
                                    dss_buffer=res;
                                });
                
                            },parent_data);
                            await pendingXHR.waitForAllXhrFinished();
                
                            dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'][index_sub_kegiatan]['_indikator']=await page.evaluate(function(){
                                return dss_buffer;
                            },{access_data:access_data,access_key:access_key});

                            delay(100);

                            if( dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'][index_sub_kegiatan]['_indikator']==null){
                                dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'][index_sub_kegiatan]['_indikator']=[];
                            }

                            index_sub_kegiatan_ind=0;
                            while(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'][index_sub_kegiatan]['_indikator'].length>index_sub_kegiatan_ind){
                                if(await exist_name(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'][index_sub_kegiatan]['_indikator'][index_sub_kegiatan_ind].tolokukur_indikator )){
                                    var kodeindp=md5(dataset_rkpd.kodedaerah+'|'+access_data.tahun_data+'-'+trimx(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'][index_sub_kegiatan]['_indikator'][index_sub_kegiatan_ind].tolokukur_indikator));
                                    var rpjmd_awal=dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'][index_sub_kegiatan]['_indikator'][index_sub_kegiatan_ind].tahun;
                                    var rpjmd_ahir=dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'][index_sub_kegiatan]['_indikator'][index_sub_kegiatan_ind].tahun;
                                
                                    await knex.select('id').from(schema+'.indikator_sub').where(
                                        {
                                            id_meta:db_req['id_meta'],
                                            id_sub_kegiatan:parent_data['id_sub_kegiatan'],
                                            kodeskpd:parent_data['kode_skpd'],
                                            kodebidang:parent_data['kodebidang'],
                                            kodeprogram:parent_data['kode_program'],
                                            kodekegiatan:parent_data['kode_kegiatan'],
                                            kodesubkegiatan:parent_data['kode_sub_kegiatan'],
                                            kodeindikator:kodeindp,
                                            jenis:5,

                                        }
                                    ).then(async (idn)=>{
                                        if(!(idn.length)){
                                            newid=await knex(schema+'.indikator_sub').insert(
                                                {
                                                    id_meta:db_req['id_meta'],
                                                    id_sub_kegiatan:parent_data['id_sub_kegiatan'],
                                                    kodeskpd:parent_data['kode_skpd'],
                                                    kodebidang:parent_data['kodebidang'],
                                                    kodeprogram:parent_data['kode_program'],
                                                    kodekegiatan:parent_data['kode_kegiatan'],
                                                    kodesubkegiatan:parent_data['kode_sub_kegiatan'],
                                                    kodeindikator:kodeindp,
                                                    jenis:5,
                                                    pagu:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'][index_sub_kegiatan]['_indikator'][index_sub_kegiatan_ind].pagu_indikator,
                                                    namaindikator:trimx(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'][index_sub_kegiatan]['_indikator'][index_sub_kegiatan_ind].tolokukur_indikator),
                                                    satuan:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'][index_sub_kegiatan]['_indikator'][index_sub_kegiatan_ind].satuan_indikator||'',
                                                    updated_at:dataset_rkpd.updated_at
                                                }
                                            ).returning('id');
                                            return newid[0];
                                        }else{
                                            var idxx=(idn[0].id);
                                            
                                            return idxx;
                                        }
                                    });
                                }

                                index_sub_kegiatan_ind++;
                            }


                            index_sub_kegiatan++;

                        }


                        index_kegiatan++;
                    }


                    index_program++;
                }

                index_skpd++;

            }
        }

        index_urusan++;

    }
    // WHILE URUSAN

//  await client.connect();
// var dbo= await client.db('RKPD');
// dbo.collection(access_data.tahun_data+'').insertOne(dataset_rkpd, function(err, res) {
//     if (err) throw err;
//         console.log("1 document inserted");
// });
if(next_step){
   var pagu_tot=await knex.select("sum(pagu) as pagu_tot").table(schema+'.sub_kegiatan').where({
        id_meta:db_req['id_meta']
    }).then(async (pagu)=>{
        if(pagu.length){
            await knex.table(schema+'.meta_rkpd').where({
                id:db_req['id_meta']
            }).update({
                'log_ang_local':pagu[0].pagu_tot
            });

            return pagu[0].pagu_tot;
        }
    });

    console.log('total pagu didapat',pagu_tot);
}


var ddd=new Date();
var f_name=ddd.getFullYear()+'-'+ddd.getMonth();
var dir = __dirname+"/../../storage/app/public/"+f_name+'-'+md5(ddd.getUTCDay()+'+'+f_name)+"-RKPD-JSON";

 if (!fs.existsSync(dir)){
    fs.mkdirSync(dir);
}

fs.writeFileSync(dir+"/rkpd-"+access_data.tahun_data+"-"+access_data.kodedaerah+"-"+dataset_rkpd.tahap+".json", JSON.stringify(dataset_rkpd));

delay(100);
await page.reload({ waitUntil: ["networkidle0", "domcontentloaded"] });
delay(1000);

return 1;





//   await browser.close();
};








(async function main(){
    p_env=process.env;

  try{
   

    await init();
    await login();

    daerah=await page.evaluate(function(){
        var kdd=[];
            $('.sidebar-item a').each(function(k,item){

            var link=$(item).attr('href');
            var urlSearchParams = new URLSearchParams(link);
            var params=Object.fromEntries(urlSearchParams.entries());
           var p=params.prov;
           var k=params.kabkot;
           
           
             if(p||k){
                if(k!='00'){
                    kdd.push(parseInt(p+''+k));

                }else{
                    kdd.push(parseInt(p));
                }
            }

        });

         kdd = kdd.filter(function(value,index,self){
           return self.indexOf(value) === index; 
        });
        
        return kdd;

    });
    delay(100);
    await loop_check();
}catch(error){
    console.log(error);
    process.exit();
  }finally{
      console.log('proccess-down');
  }
})();

this_day=null;
async function loop_check(){
    var d=await  knex('tools.egine_log').select('log_2').where({
        'egine_id':id_node,
        'type':'RKPD-CRAW',
        'name':egine_option['egine_name'],

    }).then(async (idx)=>{
        if(idx.length){
            this_day=idx[0].log_2;
        }else{
            await knex.table('tools.egine_log').insert({
                'egine_id':id_node,
                'type':'RKPD-CRAW',
                'name':egine_option['egine_name'],
            });
        }
    });
    console.log('àssss',this_day);
    var day=new Date();
    var cd=day.getFullYear()+'-'+day.getMonth()+'-'+day.getDate();
    if(this_day!=cd){
        
        await get_sinkron();
        list_updated=[];
        this_day=cd;
        await knex.table('tools.egine_log').where({
            'egine_id':id_node,
            'type':'RKPD-CRAW',
            'name':egine_option['engine_name'],
        }).update({
            'log_2':cd
        });
    }
    await get_not_match();

    var index_updated=0;
   

    if(list_updated.length>index_updated){
        access_data.kodedaerah=list_updated[index_updated];
    
        await get_data();
        
        await knex.table('tools.egine_log').where({
            'egine_id':id_node,
            'type':'RKPD-CRAW',
            'name':egine_option['engine_name'],
        }).update({
            'log_1':list_updated[index_updated]
        });
        index_updated++;
    }else{
    delay(3000);
    await loop_check();

    }
}