CREATE OR REPLACE VIEW rkpd_2022.rkpd_2022_v1
AS SELECT sk.id,
    max(mr.kodepemda::text) AS kodepemda,
    max(mpd.nama_pemda::text) AS nama_pemda,
    max(mr.rpjmd_awal) AS rpjmd_awal,
    max(mr.rpjmd_ahir) AS rpjmd_ahir,
    max(mr.tahun) AS tahun_rkpd,
    max(mr.tahap) AS tahap_rkpd,
    max(sk.kodebidang::text) AS kodebidang,
    max(mu.nama_bidang::text) AS nama_bidang,
    max(sk.kodeskpd::text) AS kodeskpd,
    max(mspd.nama_skpd::text) AS nama_skpd,
    max(sk.kodeprogram::text) AS kodeprogram,
    row_number() OVER (PARTITION BY sk.id_meta, sk.kodebidang, sk.kodeskpd, sk.kodeprogram ORDER BY sk.id_meta, sk.kodebidang, sk.kodeskpd, sk.kodeprogram) AS index_p,
    max(mp.nama::text) AS nama_program,
    max(sk.kodekegiatan::text) AS kodekegiatan,
    row_number() OVER (ORDER BY sk.id_meta, sk.kodekegiatan) AS index_k,
    max(mk.nama::text) AS nama_kegiatan,
    max(sk.kodesubkegiatan::text) AS kodesubkegiatan,
    row_number() OVER (PARTITION BY sk.kodesubkegiatan ORDER BY sk.kodesubkegiatan) AS index_sk,
    max(msk.nama::text) AS nama_sub_kegiatan,
    max(sk.pagu) AS pagu
   FROM rkpd_2022.sub_kegiatan sk
     LEFT JOIN rkpd_2022.meta_rkpd mr ON mr.id = sk.id_meta
     LEFT JOIN master.master_urusan mu ON mu.kodebidang::text = sk.kodebidang::text
     LEFT JOIN master.master_pemda mpd ON mpd.kodepemda::text = mr.kodepemda::text
     LEFT JOIN master.master_sipd_pd mspd ON mspd.kodeskpd::text = sk.kodeskpd::text AND mspd.kodebidang::text = sk.kodebidang::text AND mspd.kodepemda::text = mr.kodepemda::text
     LEFT JOIN master.master_nomenklatur_rkpd mp ON mp.kode::text = sk.kodeprogram::text AND mp.tag = 2
     LEFT JOIN master.master_nomenklatur_rkpd mk ON mk.kode::text = sk.kodekegiatan::text AND mk.tag = 3
     LEFT JOIN master.master_nomenklatur_rkpd msk ON msk.kode::text = sk.kodesubkegiatan::text AND msk.tag = 4
  GROUP BY sk.id
  ORDER BY sk.id_meta, sk.kodebidang, sk.kodeskpd, sk.kodeprogram, sk.kodekegiatan, sk.kodesubkegiatan;