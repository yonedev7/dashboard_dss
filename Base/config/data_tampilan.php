<?php

return 
    [
        'tematik'=>[
            [
                'title'=>'RENCANA DAN ANGGARAN',
                'keterangan'=>'',
                'icon'=>('assets/img/1.png'),
                'submenu'=>[
                    [
                        'title'=>'Perencanaan Daerah',
                        'keterangan'=>'',
                        'table'=>'dataset.program_kegiatan_rkpd',
                        'link'=>'tematik.tm1.program_kegiatan'
                    ],
                    [
                        'title'=>'APBD Urusan Pekerjaan Umum',
                        'keterangan'=>'Data bersumber dari SIPD PUSDATIN untuk mengambarkan pengalokasian anggaran terkait kegiatan air minum daerah',
                        'table'=>'dataset.master_dpa_[TAHUN]',
                        'link'=>'tematik.tm1.anggaran'

                    ],
                    [
                        'title'=>'APBD Daerah',
                        'keterangan'=>'',
                        'table'=>'dataset.master_dpa_[TAHUN]',
                        'link'=>'tematik.tm1.anggaran'
                    ],
                    [
                        'title'=>'Rencana Bisnis  BUMDAM',
                        'keterangan'=>'',
                        'table'=>'dataset.bisnisplan_bumdam',
                        'link'=>'data.bisplan',
                    ],
                ]
            ],
            [
                'title'=>'KERJASAMA',
                'keterangan'=>'',
                'icon'=>('assets/img/5.png'),
                'submenu'=>[
                    [
                        'title'=>'DDUB Air Minum',
                        'keterangan'=>'',
                        'table'=>'dataset.ddub',
                        'link'=>'tematik.tm2.ddub',

                    ],
                    [
                        'title'=>'Subsidi PEMDA Kepada BUMDAM',
                        'keterangan'=>'',
                        'table'=>'dataset.penyertaan_modal',
                        'link'=>'data.penyertaan-modal'

                    ],
                    [
                        'title'=>'Tipologi Hubungan PEMDA DAN BUMDAM',
                        'keterangan'=>'',
                        'table'=>'dataset.hub_pemda_pdam',
                        'link'=>'tematik.tm2.hub_pemda',

                    ]
                ]
            ],
            [
                'title'=>'CB & TA',
                'keterangan'=>'',
                'icon'=>('assets/img/4.png'),
                'submenu'=>[
                    [
                        'title'=>'Tema Pelatihan',
                        'keterangan'=>'',
                        'table'=>'dataset.tema_pelatihan',
                        'link'=>'data.tema-pelatihan'

                    ],
                    [
                        'title'=>'Pelaksanaan Pelatihan',
                        'keterangan'=>'',
                        'table'=>'dataset.pelaksanaan_pelatihan',
                        'link'=>'data.pelatihan'

                    ]
                ]
            ],
            [
                'title'=>'CAPAIAN FCR',
                'keterangan'=>'',
                'icon'=>('assets/img/3.png'),

                'submenu'=>[
                    [
                        'title'=>'BUMDAM FCR',
                        'keterangan'=>'',
                        'table'=>'dataset.kinerja_bumdam_bpkp',
                        'link'=>'data.fcr',

                    ],
                    [
                        'title'=>'Audit BUMDAM SAT',
                        'keterangan'=>'',
                        'table'=>'dataset.sat',
                        'link'=>'data.sat',

                    ],
                    [
                        'title'=>'PENILAIAN KINERJA BUMDAM BPKP',
                        'keterangan'=>'',
                        'table'=>'dataset.kinerja_bumdam_bpkp',
                        'link'=>'data.bpkp-penilain',

                    ],
                    [
                        'title'=>'Capaian SPM',
                        'keterangan'=>'',
                        'table'=>'dataset.spm_capaian_air_minum',
                        'link'=>'data.spm'

                    ]
                    // [
                    //     'title'=>'Jumlah Rumah Tangga Terlayani Air Minum',
                    //     'keterangan'=>'',
                    //     'table'=>'dataset.ikfd',
                    //     'link'=>'data.ikfd',

                    // ],
                ]
            ],
            [
                'title'=>'PROFIL DAERAH & BUMDAM',
                'keterangan'=>'',
                'icon'=>('assets/img/2.png'),
                'link'=>'data.profile-pemda',
                'submenu'=>[]
            ]

        ],
        'pendukung'=>[
            [
                'title'=>'KEPENDUDUKAN',
                'keterangan'=>'',
                'icon'=>('assets/img/people.png'),

                'submenu'=>[
                    [
                        'title'=>'Jumlah Penduduk',
                        'keterangan'=>'',
                        'table'=>'dataset.jumlah_penduduk',
                        'link'=>'data.jumlah-penduduk'
                    ],
                   
                    [
                        'title'=>'Jumlah Penduduk Miskin',
                        'keterangan'=>'',
                        'table'=>'dataset.jumlah_penduduk_miskin',
                        'link'=>'data.penduduk-miskin'
                    ],
                
                ]
                

            ],
            [
                'title'=>'KEWILAYAHAN',
                'keterangan'=>'',
                'icon'=>('assets/img/map.png'),

                'submenu'=>[
                    [
                        'title'=>'Luas Wilayah',
                        'keterangan'=>'',
                        'table'=>'dataset.luas_wilayah',
                        'link'=>'data.luas-wilayah'
                    ],
                
                ]
                

            ],
            [
                'title'=>'EKONOMI',
                'keterangan'=>'',
                'icon'=>('assets/img/economic.png'),

                'submenu'=>[
                    [
                        'title'=>'Index Kapasitas Fiskal Daerah',
                        'keterangan'=>'',
                        'table'=>'dataset.ikfd',
                        'link'=>'data.ikfd',

                    ],
                
                ]
                

            ],
            [
                'title'=>'KELEMBAGAAN',
                'keterangan'=>'',
                'icon'=>('assets/img/economic.png'),

                'submenu'=>[
                    [
                        'title'=>'Tipologi Urusan',
                        'keterangan'=>'',
                        'table'=>'dataset.tipologi_pemda',
                        'link'=>'data.tipologi-urusan',

                    ],
                
                ]
                

            ],


        ]
];