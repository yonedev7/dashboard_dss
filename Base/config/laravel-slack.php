<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Slack Webhook URL
    |--------------------------------------------------------------------------
    |
    | Incoming Webhooks are a simple way to post messages from external sources
    | into Slack. You can read more about it and how to create yours
    | here: https://api.slack.com/incoming-webhooks
    |
    */

    'slack_webhook_url' => env('SLACK_WEBHOOK_URL', ''),
    'slack_webhook_sorces'=>[
        'I'=>'https://hooks.slack.com/services/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',
        'II'=>'https://hooks.slack.com/services/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',
        'III'=>'https://hooks.slack.com/services/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',
        'IV'=>'https://hooks.slack.com/services/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',
        'TEST'=>'https://hooks.slack.com/services/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',
        'GENERAL'=>'https://hooks.slack.com/services/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',
    ],

    /*
    |--------------------------------------------------------------------------
    | Default Channel
    |--------------------------------------------------------------------------
    |
    | If no recipient is specified the message will delivered to this channel.
    | You can set a default user by using '@' instead of '#'
    |
    */

    'default_channel' => '#general',

    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | The username that this integration will post as.
    | Leave null to use Slack's default.
    |
    */

    'application_name' => env('APP_NAME', null),

    /*
    |--------------------------------------------------------------------------
    | Application Image
    |--------------------------------------------------------------------------
    |
    | The user image that is used for messages from this integration.
    | Leave null to use Slack's default.
    | It should be a valid URL.
    |
    */

    'application_image' => null,

];
