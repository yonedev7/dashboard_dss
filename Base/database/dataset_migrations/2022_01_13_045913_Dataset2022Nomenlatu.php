<?php


        use Illuminate\Database\Migrations\Migration;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Support\Facades\Schema;
        
        class Dataset2022nomenlatu extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                    if(!Schema::hasTable('dataset.Dataset2022Nomenlatu')){
                        Schema::create('dataset.Dataset2022Nomenlatu',function(Blueprint $table){
                        $table->id();
                        $table->string('kodepemda',4);
                        $table->integer('tahun')->index();
                        $table->integer('tw')->index();
                        $table->integer('status')->default(0)->index();
                        $table->float('point')->default(0);            
                        
                        $table->mediumText('keterangan')->nullable();
                        $table->bigInteger('user_pengisi')->unsigned();
                        $table->dateTime('tanggal_pengisian')->nullable();
                        $table->bigInteger('user_pengesah')->unsigned()->nullable();
                        $table->dateTime('tanggal_pengesahan')->nullable();
                        $table->bigInteger('user_pengesah_2')->unsigned()->nullable();
                        $table->dateTime('tanggal_pengesahan_2')->nullable();
                        $table->bigInteger('user_pengesah_3')->unsigned()->nullable();
                        $table->dateTime('tanggal_pengesahan_3')->nullable();
                        $table->string('data_3_plus0_4')->nullable()->comment('3-Nomenklatur Bidang Urusan--- tahun 0- tw 4');
                        $table->string('data_4_plus0_4')->nullable()->comment('4-Nama Bidang Urusan--- tahun 0- tw 4');
                        $table->string('data_5_plus0_4')->nullable()->comment('5-Noemenklatur Program--- tahun 0- tw 4');
                        $table->string('data_6_plus0_4')->nullable()->comment('6-Nama Program Nomenklatur --- tahun 0- tw 4');
                        
                            $table->unique(['kodepemda','tahun','tw']);
                            $table->index(['kodepemda','tahun','tw','status']);
                            });

                    }

                    if(Schema::hasTable('dataset.Dataset2022Nomenlatu')){
                        Schema::table('dataset.Dataset2022Nomenlatu',function (Blueprint $table){
                            if(!Schema::hasColumn('dataset.Dataset2022Nomenlatu', 'data_3_plus0_4')) {
                                $table->string('data_3_plus0_4')->nullable()->comment('3-Nomenklatur Bidang Urusan--- tahun 0- tw 4');
                                }
                            if(!Schema::hasColumn('dataset.Dataset2022Nomenlatu', 'data_4_plus0_4')) {
                                $table->string('data_4_plus0_4')->nullable()->comment('4-Nama Bidang Urusan--- tahun 0- tw 4');
                                }
                            if(!Schema::hasColumn('dataset.Dataset2022Nomenlatu', 'data_5_plus0_4')) {
                                $table->string('data_5_plus0_4')->nullable()->comment('5-Noemenklatur Program--- tahun 0- tw 4');
                                }
                            if(!Schema::hasColumn('dataset.Dataset2022Nomenlatu', 'data_6_plus0_4')) {
                                $table->string('data_6_plus0_4')->nullable()->comment('6-Nama Program Nomenklatur --- tahun 0- tw 4');
                                }
                            
                        });
                    }
                    
            }
                
            
            
            public function down(){
                
                   
                
            }
            
        }