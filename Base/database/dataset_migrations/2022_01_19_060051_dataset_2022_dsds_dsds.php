<?php


        use Illuminate\Database\Migrations\Migration;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Support\Facades\Schema;
        
        class Dataset2022DsdsDsds extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                    if(!Schema::hasTable('dataset.dataset_2022_dsds_dsds')){
                        Schema::create('dataset.dataset_2022_dsds_dsds',function(Blueprint $table){
                        $table->id();
                        $table->string('kodepemda',5);
                        $table->integer('tahun')->index();
                        $table->integer('tw')->index();
                        $table->integer('status')->default(0)->index();
                        $table->float('point')->default(0);            
                        
                        $table->mediumText('keterangan')->nullable();
                        $table->bigInteger('user_pengisi')->unsigned();
                        $table->dateTime('tanggal_pengisian')->nullable();
                        $table->bigInteger('user_pengesah')->unsigned()->nullable();
                        $table->dateTime('tanggal_pengesahan')->nullable();
                        $table->bigInteger('user_pengesah_2')->unsigned()->nullable();
                        $table->dateTime('tanggal_pengesahan_2')->nullable();
                        $table->bigInteger('user_pengesah_3')->unsigned()->nullable();
                        $table->dateTime('tanggal_pengesahan_3')->nullable();
                        
                            $table->unique(['kodepemda','tahun','tw']);
                            $table->index(['kodepemda','tahun','tw','status']);
                            });

                    }

                    if(Schema::hasTable('dataset.dataset_2022_dsds_dsds')){
                        Schema::table('dataset.dataset_2022_dsds_dsds',function (Blueprint $table){
                            if(!Schema::hasColumn('dataset.dataset_2022_dsds_dsds', 'data_2_plus0_4')) {
                                
                                }
                            if(!Schema::hasColumn('dataset.dataset_2022_dsds_dsds', 'data_1_plus0_4')) {
                                
                                }
                            
                        });
                    }
                    
            }
                
            
            
            public function down(){
                
                   
                
            }
            
        }