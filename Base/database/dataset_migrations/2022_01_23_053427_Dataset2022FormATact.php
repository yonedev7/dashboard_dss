<?php


        use Illuminate\Database\Migrations\Migration;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Support\Facades\Schema;
        
        class Dataset2022formatact extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                    if(!Schema::hasTable('dataset.Dataset2022FormATact')){
                        Schema::create('dataset.Dataset2022FormATact',function(Blueprint $table){
                        $table->id();
                        $table->integer('index')->nullable()->default(0)->index();
                        $table->string('kodepemda',5);
                        $table->integer('tahun')->index();
                        $table->integer('tw')->index();
                        $table->integer('status')->default(0)->index();
                        $table->float('point')->default(0);            
                        
                        $table->mediumText('keterangan')->nullable();
                        $table->bigInteger('user_pengisi')->unsigned();
                        $table->dateTime('tanggal_pengisian')->nullable();
                        $table->bigInteger('user_pengesah')->unsigned()->nullable();
                        $table->dateTime('tanggal_pengesahan')->nullable();
                        $table->bigInteger('user_pengesah_2')->unsigned()->nullable();
                        $table->dateTime('tanggal_pengesahan_2')->nullable();
                        $table->bigInteger('user_pengesah_3')->unsigned()->nullable();
                        $table->dateTime('tanggal_pengesahan_3')->nullable();
                        $table->double('data_2_plus0_4',25,3)->nullable()->comment('2-Data test 2-SatuanA- tahun 0- tw 4');
                        $table->double('data_1_plus0_4',25,3)->nullable()->comment('1-Data test 1-SatuanA- tahun 0- tw 4');
                        
                            $table->unique(['kodepemda','tahun','tw']);
                            $table->index(['kodepemda','tahun','tw','status']);
                            });

                    }

                    if(Schema::hasTable('dataset.Dataset2022FormATact')){
                        Schema::table('dataset.Dataset2022FormATact',function (Blueprint $table){
                            if(!Schema::hasColumn('dataset.Dataset2022FormATact', 'data_2_plus0_4')) {
                                $table->double('data_2_plus0_4',25,3)->nullable()->comment('2-Data test 2-SatuanA- tahun 0- tw 4');
                                }
                            if(!Schema::hasColumn('dataset.Dataset2022FormATact', 'data_1_plus0_4')) {
                                $table->double('data_1_plus0_4',25,3)->nullable()->comment('1-Data test 1-SatuanA- tahun 0- tw 4');
                                }
                            
                        });
                    }
                    
            }
                
            
            
            public function down(){
                
                   
                
            }
            
        }