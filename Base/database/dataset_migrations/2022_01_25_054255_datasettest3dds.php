<?php


        use Illuminate\Database\Migrations\Migration;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Support\Facades\Schema;
        
        class Datasettest3dds extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                    if(!Schema::hasTable('dataset.datasettest3dds')){
                        Schema::create('dataset.datasettest3dds',function(Blueprint $table){
                        $table->id();
                        $table->integer('index')->nullable()->default(0)->index();
                        $table->string('kodepemda',5);
                        $table->integer('tahun')->index();
                        $table->integer('tw')->index();
                        $table->integer('status')->default(0)->index();
                        $table->float('point')->default(0);            
                        
                        $table->mediumText('keterangan')->nullable();
                        $table->bigInteger('user_pengisi')->unsigned();
                        $table->dateTime('tanggal_pengisian')->nullable();
                        $table->bigInteger('user_pengesah')->unsigned()->nullable();
                        $table->dateTime('tanggal_pengesahan')->nullable();
                        $table->bigInteger('user_pengesah_2')->unsigned()->nullable();
                        $table->dateTime('tanggal_pengesahan_2')->nullable();
                        $table->bigInteger('user_pengesah_3')->unsigned()->nullable();
                        $table->dateTime('tanggal_pengesahan_3')->nullable();
                        $table->dateTime('updated_at')->nullable();
                        ;$table->double('data_1_plus0_4',25,3)->nullable()->comment('1-Data test 1-SatuanA- tahun 0- tw 4');
                            $table->double('data_2_plus0_4',25,3)->nullable()->comment('2-Data test 2-SatuanA- tahun 0- tw 4');
                            });

                    }

                    if(Schema::hasTable('dataset.datasettest3dds')){
                        Schema::table('dataset.datasettest3dds',function (Blueprint $table){
                            if(!Schema::hasColumn('dataset.datasettest3dds', 'updated_at')) {
                                $table->dateTime('updated_at')->nullable();
                            }
                            if(!Schema::hasColumn('dataset.datasettest3dds', 'data_1_plus0_4')) {
                                $table->double('data_1_plus0_4',25,3)->nullable()->comment('1-Data test 1-SatuanA- tahun 0- tw 4');
                                }
                            if(!Schema::hasColumn('dataset.datasettest3dds', 'data_2_plus0_4')) {
                                $table->double('data_2_plus0_4',25,3)->nullable()->comment('2-Data test 2-SatuanA- tahun 0- tw 4');
                                }
                            
                        });
                        
                    }
                    
            }
                
            
            
            public function down(){
                
                   
                
            }
            
        }