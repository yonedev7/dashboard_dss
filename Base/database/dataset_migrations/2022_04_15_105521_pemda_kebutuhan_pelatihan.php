<?php


        use Illuminate\Database\Migrations\Migration;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Support\Facades\Schema;
        use Doctrine\DBAL\Types\FloatType;
        use Doctrine\DBAL\Types\Type;
        
        class PemdaKebutuhanPelatihan extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                    if (!Type::hasType('double')) {
                        Type::addType('double', FloatType::class);
                    }
                    if(!Schema::hasTable('dataset.pemda_kebutuhan_pelatihan')){
                        Schema::create('dataset.pemda_kebutuhan_pelatihan',function(Blueprint $table){
                        $table->id();
                        $table->integer('index')->nullable()->default(0)->index();
                        $table->string('kodepemda',5)->index('dts_99_idx_kodepemda');
                        $table->integer('tahun')->index('dts_99_idx_tahun');
                        $table->integer('tw')->index('dts_99_idx_tw');
                        $table->integer('status')->default(0)->index('dts_99_idx_status');
                        $table->float('point')->default(0);            
                        
                        $table->mediumText('keterangan')->nullable();
                        $table->bigInteger('user_pengisi')->unsigned();
                        $table->dateTime('tanggal_pengisian')->nullable();
                        $table->bigInteger('user_pengesah')->unsigned()->nullable();
                        $table->dateTime('tanggal_pengesahan')->nullable();
                        $table->bigInteger('user_pengesah_2')->unsigned()->nullable();
                        $table->dateTime('tanggal_pengesahan_2')->nullable();
                        $table->bigInteger('user_pengesah_3')->unsigned()->nullable();
                        $table->dateTime('tanggal_pengesahan_3')->nullable();
                        $table->dateTime('updated_at')->nullable();
                        ;$table->mediumText('tema_pelatihan')->nullable()->comment('272-Tema Pelatihan--');
                            $table->mediumText('jenis_pelatihan')->nullable()->comment('277-Jenis Pelatihan--');
                            $table->double('jumlah_peserta',25,3)->nullable()->comment('276-Jumlah Peserta Pelatihan-Orang');
                            
                            $table->index(['kodepemda','tahun','tw','status','dts_99_idx_composit']);
                            });

                    }

                    if(Schema::hasTable('dataset.pemda_kebutuhan_pelatihan')){
                        Schema::table('dataset.pemda_kebutuhan_pelatihan',function (Blueprint $table){
                            if(!Schema::hasColumn('dataset.pemda_kebutuhan_pelatihan', 'updated_at')) {
                                $table->dateTime('updated_at')->nullable();
                            }
                            if(!Schema::hasColumn('dataset.pemda_kebutuhan_pelatihan', 'tema_pelatihan')) {
                                $table->mediumText('tema_pelatihan')->nullable()->comment('272-Tema Pelatihan--');
                                    
                                    }else{$table->mediumText('tema_pelatihan')->nullable()->comment('272-Tema Pelatihan--')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.pemda_kebutuhan_pelatihan', 'jenis_pelatihan')) {
                                $table->mediumText('jenis_pelatihan')->nullable()->comment('277-Jenis Pelatihan--');
                                    
                                    }else{$table->mediumText('jenis_pelatihan')->nullable()->comment('277-Jenis Pelatihan--')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.pemda_kebutuhan_pelatihan', 'jumlah_peserta')) {
                                $table->double('jumlah_peserta',25,3)->nullable()->comment('276-Jumlah Peserta Pelatihan-Orang');
                                    
                                    }else{$table->double('jumlah_peserta',25,3)->nullable()->comment('276-Jumlah Peserta Pelatihan-Orang')->change();
                                        
                                }
                                
                        });
                        
                        if(Schema::hasTable('dataset.pemda_kebutuhan_pelatihan')){
                            Schema::table('dataset.pemda_kebutuhan_pelatihan',function (Blueprint $table){
                                $sm = Schema::getConnection()->getDoctrineSchemaManager();
                                $indexesFound = $sm->listTableIndexes('dataset.pemda_kebutuhan_pelatihan');
                                if(!array_key_exists('dts_99_idx_kodepemda', $indexesFound)){$table->index(['kodepemda'],'dts_99_idx_kodepemda'); }if(!array_key_exists('dts_99_idx_tahun', $indexesFound)){$table->index(['tahun'],'dts_99_idx_tahun'); }if(!array_key_exists('dts_99_idx_tw', $indexesFound)){$table->index(['tahun'],'dts_99_idx_tw'); }if(!array_key_exists('dts_99_idx_status', $indexesFound)){$table->index(['tahun'],'dts_99_idx_status'); }if(array_key_exists('dts_99_unique_single', $indexesFound)){$table->dropUnique('dts_99_unique_single'); }
                            });
                        }
                    }
                    
            }
                
            
            
            public function down(){
                
                   
                
            }
            
        }