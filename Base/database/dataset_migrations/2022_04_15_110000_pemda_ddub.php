<?php


        use Illuminate\Database\Migrations\Migration;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Support\Facades\Schema;
        use Doctrine\DBAL\Types\FloatType;
        use Doctrine\DBAL\Types\Type;
        
        class PemdaDdub extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                    if (!Type::hasType('double')) {
                        Type::addType('double', FloatType::class);
                    }
                    if(!Schema::hasTable('dataset.pemda_ddub')){
                        Schema::create('dataset.pemda_ddub',function(Blueprint $table){
                        $table->id();
                        $table->integer('index')->nullable()->default(0)->index();
                        $table->string('kodepemda',5)->index('dts_95_idx_kodepemda');
                        $table->integer('tahun')->index('dts_95_idx_tahun');
                        $table->integer('tw')->index('dts_95_idx_tw');
                        $table->integer('status')->default(0)->index('dts_95_idx_status');
                        $table->float('point')->default(0);            
                        
                        $table->mediumText('keterangan')->nullable();
                        $table->bigInteger('user_pengisi')->unsigned();
                        $table->dateTime('tanggal_pengisian')->nullable();
                        $table->bigInteger('user_pengesah')->unsigned()->nullable();
                        $table->dateTime('tanggal_pengesahan')->nullable();
                        $table->bigInteger('user_pengesah_2')->unsigned()->nullable();
                        $table->dateTime('tanggal_pengesahan_2')->nullable();
                        $table->bigInteger('user_pengesah_3')->unsigned()->nullable();
                        $table->dateTime('tanggal_pengesahan_3')->nullable();
                        $table->dateTime('updated_at')->nullable();
                        ;$table->double('ddub_perencanaan',25,3)->nullable()->comment('89-Perencanaan DDUB PEMDA-Rp.');
                            $table->double('ddub_realisasi',25,3)->nullable()->comment('93-Realisasi DDUB Pemda-Rp.');
                            $table->double('ddub_gap',25,3)->nullable()->comment('94-Gap Antara Perencanaan da-Rp.');
                            
                            $table->unique(['kodepemda','tahun','tw'],'dts_95_unique_single');
                            $table->index(['kodepemda','tahun','tw','status'],'dts_95_idx_composit');
                            });

                    }

                    if(Schema::hasTable('dataset.pemda_ddub')){
                        Schema::table('dataset.pemda_ddub',function (Blueprint $table){
                            if(!Schema::hasColumn('dataset.pemda_ddub', 'updated_at')) {
                                $table->dateTime('updated_at')->nullable();
                            }
                            if(!Schema::hasColumn('dataset.pemda_ddub', 'ddub_perencanaan')) {
                                $table->double('ddub_perencanaan',25,3)->nullable()->comment('89-Perencanaan DDUB PEMDA-Rp.');
                                    
                                    }else{$table->double('ddub_perencanaan',25,3)->nullable()->comment('89-Perencanaan DDUB PEMDA-Rp.')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.pemda_ddub', 'ddub_realisasi')) {
                                $table->double('ddub_realisasi',25,3)->nullable()->comment('93-Realisasi DDUB Pemda-Rp.');
                                    
                                    }else{$table->double('ddub_realisasi',25,3)->nullable()->comment('93-Realisasi DDUB Pemda-Rp.')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.pemda_ddub', 'ddub_gap')) {
                                $table->double('ddub_gap',25,3)->nullable()->comment('94-Gap Antara Perencanaan da-Rp.');
                                    
                                    }else{$table->double('ddub_gap',25,3)->nullable()->comment('94-Gap Antara Perencanaan da-Rp.')->change();
                                        
                                }
                                
                        });
                        
                        if(Schema::hasTable('dataset.pemda_ddub')){
                            Schema::table('dataset.pemda_ddub',function (Blueprint $table){
                                $sm = Schema::getConnection()->getDoctrineSchemaManager();
                                $indexesFound = $sm->listTableIndexes('dataset.pemda_ddub');
                                if(!array_key_exists('dts_95_idx_kodepemda', $indexesFound)){$table->index(['kodepemda'],'dts_95_idx_kodepemda'); }if(!array_key_exists('dts_95_idx_tahun', $indexesFound)){$table->index(['tahun'],'dts_95_idx_tahun'); }if(!array_key_exists('dts_95_idx_tw', $indexesFound)){$table->index(['tahun'],'dts_95_idx_tw'); }if(!array_key_exists('dts_95_idx_status', $indexesFound)){$table->index(['tahun'],'dts_95_idx_status'); }if(!array_key_exists('dts_95_idx_composit', $indexesFound)){$table->index(['kodepemda','tahun','tw','status'],'dts_95_idx_composit'); }if(array_key_exists('dts_95_unique_multy_extend', $indexesFound)){$table->dropUnique('dts_95_unique_multy_extend'); }if(!array_key_exists('dts_95_unique_single', $indexesFound)){$table->unique(['kodepemda','tahun','tw'],'dts_95_unique_single'); }
                            });
                        }
                    }
                    
            }
                
            
            
            public function down(){
                
                   
                
            }
            
        }