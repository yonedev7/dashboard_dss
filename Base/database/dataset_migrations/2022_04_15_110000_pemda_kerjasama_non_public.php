<?php


        use Illuminate\Database\Migrations\Migration;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Support\Facades\Schema;
        use Doctrine\DBAL\Types\FloatType;
        use Doctrine\DBAL\Types\Type;
        
        class PemdaKerjasamaNonPublic extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                    if (!Type::hasType('double')) {
                        Type::addType('double', FloatType::class);
                    }
                    if(!Schema::hasTable('dataset.pemda_kerjasama_non_public')){
                        Schema::create('dataset.pemda_kerjasama_non_public',function(Blueprint $table){
                        $table->id();
                        $table->integer('index')->nullable()->default(0)->index();
                        $table->string('kodepemda',5)->index('dts_97_idx_kodepemda');
                        $table->integer('tahun')->index('dts_97_idx_tahun');
                        $table->integer('tw')->index('dts_97_idx_tw');
                        $table->integer('status')->default(0)->index('dts_97_idx_status');
                        $table->float('point')->default(0);            
                        
                        $table->mediumText('keterangan')->nullable();
                        $table->bigInteger('user_pengisi')->unsigned();
                        $table->dateTime('tanggal_pengisian')->nullable();
                        $table->bigInteger('user_pengesah')->unsigned()->nullable();
                        $table->dateTime('tanggal_pengesahan')->nullable();
                        $table->bigInteger('user_pengesah_2')->unsigned()->nullable();
                        $table->dateTime('tanggal_pengesahan_2')->nullable();
                        $table->bigInteger('user_pengesah_3')->unsigned()->nullable();
                        $table->dateTime('tanggal_pengesahan_3')->nullable();
                        $table->dateTime('updated_at')->nullable();
                        ;$table->double('tahun_mulai_kerjasama',25,3)->nullable()->comment('271-Tahun Mulai Kerjasama--');
                            $table->mediumText('mitra_kerjasama')->nullable()->comment('278-Mitra Kerjasama Non Publi--');
                            $table->mediumText('bentuk_kegiatan_kerjasama')->nullable()->comment('279-Bentuk Kegiatan Kerjasama--');
                            $table->double('nilai_kontrak_kerjasama',25,3)->nullable()->comment('260-Nilai Kontrak-RUPIAH');
                            
                            $table->index(['kodepemda','tahun','tw','status','dts_97_idx_composit']);
                            });

                    }

                    if(Schema::hasTable('dataset.pemda_kerjasama_non_public')){
                        Schema::table('dataset.pemda_kerjasama_non_public',function (Blueprint $table){
                            if(!Schema::hasColumn('dataset.pemda_kerjasama_non_public', 'updated_at')) {
                                $table->dateTime('updated_at')->nullable();
                            }
                            if(!Schema::hasColumn('dataset.pemda_kerjasama_non_public', 'tahun_mulai_kerjasama')) {
                                $table->double('tahun_mulai_kerjasama',25,3)->nullable()->comment('271-Tahun Mulai Kerjasama--');
                                    
                                    }else{$table->double('tahun_mulai_kerjasama',25,3)->nullable()->comment('271-Tahun Mulai Kerjasama--')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.pemda_kerjasama_non_public', 'mitra_kerjasama')) {
                                $table->mediumText('mitra_kerjasama')->nullable()->comment('278-Mitra Kerjasama Non Publi--');
                                    
                                    }else{$table->mediumText('mitra_kerjasama')->nullable()->comment('278-Mitra Kerjasama Non Publi--')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.pemda_kerjasama_non_public', 'bentuk_kegiatan_kerjasama')) {
                                $table->mediumText('bentuk_kegiatan_kerjasama')->nullable()->comment('279-Bentuk Kegiatan Kerjasama--');
                                    
                                    }else{$table->mediumText('bentuk_kegiatan_kerjasama')->nullable()->comment('279-Bentuk Kegiatan Kerjasama--')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.pemda_kerjasama_non_public', 'nilai_kontrak_kerjasama')) {
                                $table->double('nilai_kontrak_kerjasama',25,3)->nullable()->comment('260-Nilai Kontrak-RUPIAH');
                                    
                                    }else{$table->double('nilai_kontrak_kerjasama',25,3)->nullable()->comment('260-Nilai Kontrak-RUPIAH')->change();
                                        
                                }
                                
                        });
                        
                        if(Schema::hasTable('dataset.pemda_kerjasama_non_public')){
                            Schema::table('dataset.pemda_kerjasama_non_public',function (Blueprint $table){
                                $sm = Schema::getConnection()->getDoctrineSchemaManager();
                                $indexesFound = $sm->listTableIndexes('dataset.pemda_kerjasama_non_public');
                                if(!array_key_exists('dts_97_idx_kodepemda', $indexesFound)){$table->index(['kodepemda'],'dts_97_idx_kodepemda'); }if(!array_key_exists('dts_97_idx_tahun', $indexesFound)){$table->index(['tahun'],'dts_97_idx_tahun'); }if(!array_key_exists('dts_97_idx_tw', $indexesFound)){$table->index(['tahun'],'dts_97_idx_tw'); }if(!array_key_exists('dts_97_idx_status', $indexesFound)){$table->index(['tahun'],'dts_97_idx_status'); }if(!array_key_exists('dts_97_idx_composit', $indexesFound)){$table->index(['kodepemda','tahun','tw','status'],'dts_97_idx_composit'); }if(array_key_exists('dts_97_unique_single', $indexesFound)){$table->dropUnique('dts_97_unique_single'); }
                            });
                        }
                    }
                    
            }
                
            
            
            public function down(){
                
                   
                
            }
            
        }