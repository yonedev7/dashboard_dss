<?php


        use Illuminate\Database\Migrations\Migration;
        use Illuminate\Database\Schema\Blueprint;
        use Illuminate\Support\Facades\Schema;
        use Doctrine\DBAL\Types\FloatType;
        use Doctrine\DBAL\Types\Type;
        
        class KinerjaBumdamBpkp extends Migration
        {
            /**
             * Run the migrations.
             *
             * @return void
             */
            public function up()
            {
                    if (!Type::hasType('double')) {
                        Type::addType('double', FloatType::class);
                    }
                    if(!Schema::hasTable('dataset.kinerja_bumdam_bpkp')){
                        Schema::create('dataset.kinerja_bumdam_bpkp',function(Blueprint $table){
                        $table->id();
                        $table->integer('index')->nullable()->default(0)->index();
                        $table->string('kodepemda',5)->index('dts_2_idx_kodepemda');
                        $table->integer('tahun')->index('dts_2_idx_tahun');
                        $table->integer('tw')->index('dts_2_idx_tw');
                        $table->integer('status')->default(0)->index('dts_2_idx_status');
                        $table->float('point')->default(0);            
                        
                        $table->mediumText('keterangan')->nullable();
                        $table->bigInteger('user_pengisi')->unsigned();
                        $table->dateTime('tanggal_pengisian')->nullable();
                        $table->bigInteger('user_pengesah')->unsigned()->nullable();
                        $table->dateTime('tanggal_pengesahan')->nullable();
                        $table->bigInteger('user_pengesah_2')->unsigned()->nullable();
                        $table->dateTime('tanggal_pengesahan_2')->nullable();
                        $table->bigInteger('user_pengesah_3')->unsigned()->nullable();
                        $table->dateTime('tanggal_pengesahan_3')->nullable();
                        $table->dateTime('updated_at')->nullable();
                        ;$table->double('roe',25,3)->nullable()->comment('3-ROE BUMDAM-%');
                            $table->double('nilai_roe',25,3)->nullable()->comment('30-Nilai ROE BUMDAM - BPKP--');
                            $table->double('rasio_operasi',25,3)->nullable()->comment('6-Rasio Operasi BUMDAM-%');
                            $table->double('nilai_rasio_operasi',25,3)->nullable()->comment('31-Nilai Rasio Operasi BUMDA--');
                            $table->double('rasio_kas',25,3)->nullable()->comment('7-Rasio Kas BUMDAM-%');
                            $table->double('nilai_rasio_kas',25,3)->nullable()->comment('32-Nilai Rasio Kas BUMDAM - --');
                            $table->double('evektifitas_penagihan',25,3)->nullable()->comment('8-Evektifitas Penagihan BUM-%');
                            $table->double('nilai_evektifitas_penagihan',25,3)->nullable()->comment('47-Nilai Evektifitas Penagih--');
                            $table->double('solvabilitas',25,3)->nullable()->comment('9-Solvabilitas BUMDAM-%');
                            $table->double('nilai_solvabilitas',25,3)->nullable()->comment('48-Nilai Solvabilitas BUMDAM--');
                            $table->double('bobot_kinerja_keuangan',25,3)->nullable()->comment('10-Bobot Kinerja - Bidang Ke--');
                            $table->double('cakupan_pelayanan',25,3)->nullable()->comment('11-Cakupan Pelayanan BUMDAM-%');
                            $table->double('nilai_cakupan_pelayanan',25,3)->nullable()->comment('34-Nilai Cakupan Pelayanan B--');
                            $table->double('pertumbuhan_pelanggan',25,3)->nullable()->comment('12-Pertumbuhan Pelanggan BUM-%');
                            $table->double('nilai_pertumbuhan_pelanggan',25,3)->nullable()->comment('35-Nilai Pertumbuhan Pelangg--');
                            $table->double('tingkat_penyelesaian',25,3)->nullable()->comment('13-Tingkat Penyelesaian Peng-%');
                            $table->double('nilai_tingkat_penyelesaian',25,3)->nullable()->comment('46-Nilai Tingkat Penyelesaia--');
                            $table->double('kualitas_air_pelanggan',25,3)->nullable()->comment('15-Kualitas Air Pelanggan BU-%');
                            $table->double('nilai_kualitas_air_pelanggan',25,3)->nullable()->comment('36-Nilai Kualitas Air Pelang--');
                            $table->double('konsumsi_air_domestik',25,3)->nullable()->comment('16-Konsumsi Air Domestik BUM-%');
                            $table->double('nilai_konsumsi_air_domestik',25,3)->nullable()->comment('49-Nilai Konsumsi Air Domest--');
                            $table->double('bobot_kinerja_pelayanan',25,3)->nullable()->comment('53-Bobot Kinerja - Bidang Pe--');
                            $table->double('effisiensi_produksi',25,3)->nullable()->comment('17-Effisiensi Produksi BUMDA-%');
                            $table->double('nilai_effisiensi_produksi',25,3)->nullable()->comment('37-Nilai Effisiensi Produksi--');
                            $table->double('tingkat_kehilangan_air',25,3)->nullable()->comment('18-Tingkat Kehilangan air BU-%');
                            $table->double('nilai_tingkat_kehilangan_air',25,3)->nullable()->comment('38-Nilai Tingkat Kehilangan --');
                            $table->double('jam_operasi_layanan',25,3)->nullable()->comment('19-Jam Operasi Layanan / har-Jam');
                            $table->double('nilai_jam_operasi_layanan',25,3)->nullable()->comment('39-Nilai Jam Operasi Layanan--');
                            $table->double('tekanan_sambungan',25,3)->nullable()->comment('20-Tekanan Sambungan Pelangg-%');
                            $table->double('nilai_tekanan_sambungan',25,3)->nullable()->comment('40-Nilai Tekanan Sambungan P--');
                            $table->double('pengganti_meter_air',25,3)->nullable()->comment('21-Penggantian Meter Air BUM-%');
                            $table->double('nilai_pengganti_meter_air',25,3)->nullable()->comment('41-Nilai Penggantian Meter A--');
                            $table->double('bobot_kinerja_operasi',25,3)->nullable()->comment('22-Bobot Kinerja - Bidang Op--');
                            $table->double('rasio_juml',25,3)->nullable()->comment('23-Rasio Juml Peg /1000 Plgn-%');
                            $table->double('nilai_rasio_juml',25,3)->nullable()->comment('43-Nilai Rasio Juml Peg /100--');
                            $table->double('ratio_dikalt_pegawai',25,3)->nullable()->comment('24-Ratio diklat pegawai/peni-%');
                            $table->double('nilai_ratio_diklat_pegawai',25,3)->nullable()->comment('44-Nilai Ratio diklat pegawa--');
                            $table->double('rasio_biaya_diklat',25,3)->nullable()->comment('25-Rasio Biaya Diklat terhad-%');
                            $table->double('nilai_rasio_biaya_diklat',25,3)->nullable()->comment('45-Nilai Rasio Biaya Diklat --');
                            $table->double('bobot_kinerja_sdm',25,3)->nullable()->comment('26-Bobot Kinerja - Bidang SD--');
                            $table->double('nilai_kinerja_total',25,3)->nullable()->comment('27-Total Nilai Kinerja BUMDA--');
                            $table->double('kategori_penilaian_bpkp',25,3)->nullable()->comment('28-Kategori Penilain Kinerja--');
                            $table->double('nilai_fcr',25,3)->nullable()->comment('280-Pemenuhan Tarif FCR-%');
                            
                            $table->unique(['kodepemda','tahun','tw'],'dts_2_unique_single');
                            $table->index(['kodepemda','tahun','tw','status'],'dts_2_idx_composit');
                            });

                    }

                    if(Schema::hasTable('dataset.kinerja_bumdam_bpkp')){
                        Schema::table('dataset.kinerja_bumdam_bpkp',function (Blueprint $table){
                            if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'updated_at')) {
                                $table->dateTime('updated_at')->nullable();
                            }
                            if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'roe')) {
                                $table->double('roe',25,3)->nullable()->comment('3-ROE BUMDAM-%');
                                    
                                    }else{$table->double('roe',25,3)->nullable()->comment('3-ROE BUMDAM-%')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'nilai_roe')) {
                                $table->double('nilai_roe',25,3)->nullable()->comment('30-Nilai ROE BUMDAM - BPKP--');
                                    
                                    }else{$table->double('nilai_roe',25,3)->nullable()->comment('30-Nilai ROE BUMDAM - BPKP--')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'rasio_operasi')) {
                                $table->double('rasio_operasi',25,3)->nullable()->comment('6-Rasio Operasi BUMDAM-%');
                                    
                                    }else{$table->double('rasio_operasi',25,3)->nullable()->comment('6-Rasio Operasi BUMDAM-%')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'nilai_rasio_operasi')) {
                                $table->double('nilai_rasio_operasi',25,3)->nullable()->comment('31-Nilai Rasio Operasi BUMDA--');
                                    
                                    }else{$table->double('nilai_rasio_operasi',25,3)->nullable()->comment('31-Nilai Rasio Operasi BUMDA--')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'rasio_kas')) {
                                $table->double('rasio_kas',25,3)->nullable()->comment('7-Rasio Kas BUMDAM-%');
                                    
                                    }else{$table->double('rasio_kas',25,3)->nullable()->comment('7-Rasio Kas BUMDAM-%')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'nilai_rasio_kas')) {
                                $table->double('nilai_rasio_kas',25,3)->nullable()->comment('32-Nilai Rasio Kas BUMDAM - --');
                                    
                                    }else{$table->double('nilai_rasio_kas',25,3)->nullable()->comment('32-Nilai Rasio Kas BUMDAM - --')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'evektifitas_penagihan')) {
                                $table->double('evektifitas_penagihan',25,3)->nullable()->comment('8-Evektifitas Penagihan BUM-%');
                                    
                                    }else{$table->double('evektifitas_penagihan',25,3)->nullable()->comment('8-Evektifitas Penagihan BUM-%')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'nilai_evektifitas_penagihan')) {
                                $table->double('nilai_evektifitas_penagihan',25,3)->nullable()->comment('47-Nilai Evektifitas Penagih--');
                                    
                                    }else{$table->double('nilai_evektifitas_penagihan',25,3)->nullable()->comment('47-Nilai Evektifitas Penagih--')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'solvabilitas')) {
                                $table->double('solvabilitas',25,3)->nullable()->comment('9-Solvabilitas BUMDAM-%');
                                    
                                    }else{$table->double('solvabilitas',25,3)->nullable()->comment('9-Solvabilitas BUMDAM-%')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'nilai_solvabilitas')) {
                                $table->double('nilai_solvabilitas',25,3)->nullable()->comment('48-Nilai Solvabilitas BUMDAM--');
                                    
                                    }else{$table->double('nilai_solvabilitas',25,3)->nullable()->comment('48-Nilai Solvabilitas BUMDAM--')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'bobot_kinerja_keuangan')) {
                                $table->double('bobot_kinerja_keuangan',25,3)->nullable()->comment('10-Bobot Kinerja - Bidang Ke--');
                                    
                                    }else{$table->double('bobot_kinerja_keuangan',25,3)->nullable()->comment('10-Bobot Kinerja - Bidang Ke--')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'cakupan_pelayanan')) {
                                $table->double('cakupan_pelayanan',25,3)->nullable()->comment('11-Cakupan Pelayanan BUMDAM-%');
                                    
                                    }else{$table->double('cakupan_pelayanan',25,3)->nullable()->comment('11-Cakupan Pelayanan BUMDAM-%')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'nilai_cakupan_pelayanan')) {
                                $table->double('nilai_cakupan_pelayanan',25,3)->nullable()->comment('34-Nilai Cakupan Pelayanan B--');
                                    
                                    }else{$table->double('nilai_cakupan_pelayanan',25,3)->nullable()->comment('34-Nilai Cakupan Pelayanan B--')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'pertumbuhan_pelanggan')) {
                                $table->double('pertumbuhan_pelanggan',25,3)->nullable()->comment('12-Pertumbuhan Pelanggan BUM-%');
                                    
                                    }else{$table->double('pertumbuhan_pelanggan',25,3)->nullable()->comment('12-Pertumbuhan Pelanggan BUM-%')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'nilai_pertumbuhan_pelanggan')) {
                                $table->double('nilai_pertumbuhan_pelanggan',25,3)->nullable()->comment('35-Nilai Pertumbuhan Pelangg--');
                                    
                                    }else{$table->double('nilai_pertumbuhan_pelanggan',25,3)->nullable()->comment('35-Nilai Pertumbuhan Pelangg--')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'tingkat_penyelesaian')) {
                                $table->double('tingkat_penyelesaian',25,3)->nullable()->comment('13-Tingkat Penyelesaian Peng-%');
                                    
                                    }else{$table->double('tingkat_penyelesaian',25,3)->nullable()->comment('13-Tingkat Penyelesaian Peng-%')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'nilai_tingkat_penyelesaian')) {
                                $table->double('nilai_tingkat_penyelesaian',25,3)->nullable()->comment('46-Nilai Tingkat Penyelesaia--');
                                    
                                    }else{$table->double('nilai_tingkat_penyelesaian',25,3)->nullable()->comment('46-Nilai Tingkat Penyelesaia--')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'kualitas_air_pelanggan')) {
                                $table->double('kualitas_air_pelanggan',25,3)->nullable()->comment('15-Kualitas Air Pelanggan BU-%');
                                    
                                    }else{$table->double('kualitas_air_pelanggan',25,3)->nullable()->comment('15-Kualitas Air Pelanggan BU-%')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'nilai_kualitas_air_pelanggan')) {
                                $table->double('nilai_kualitas_air_pelanggan',25,3)->nullable()->comment('36-Nilai Kualitas Air Pelang--');
                                    
                                    }else{$table->double('nilai_kualitas_air_pelanggan',25,3)->nullable()->comment('36-Nilai Kualitas Air Pelang--')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'konsumsi_air_domestik')) {
                                $table->double('konsumsi_air_domestik',25,3)->nullable()->comment('16-Konsumsi Air Domestik BUM-%');
                                    
                                    }else{$table->double('konsumsi_air_domestik',25,3)->nullable()->comment('16-Konsumsi Air Domestik BUM-%')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'nilai_konsumsi_air_domestik')) {
                                $table->double('nilai_konsumsi_air_domestik',25,3)->nullable()->comment('49-Nilai Konsumsi Air Domest--');
                                    
                                    }else{$table->double('nilai_konsumsi_air_domestik',25,3)->nullable()->comment('49-Nilai Konsumsi Air Domest--')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'bobot_kinerja_pelayanan')) {
                                $table->double('bobot_kinerja_pelayanan',25,3)->nullable()->comment('53-Bobot Kinerja - Bidang Pe--');
                                    
                                    }else{$table->double('bobot_kinerja_pelayanan',25,3)->nullable()->comment('53-Bobot Kinerja - Bidang Pe--')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'effisiensi_produksi')) {
                                $table->double('effisiensi_produksi',25,3)->nullable()->comment('17-Effisiensi Produksi BUMDA-%');
                                    
                                    }else{$table->double('effisiensi_produksi',25,3)->nullable()->comment('17-Effisiensi Produksi BUMDA-%')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'nilai_effisiensi_produksi')) {
                                $table->double('nilai_effisiensi_produksi',25,3)->nullable()->comment('37-Nilai Effisiensi Produksi--');
                                    
                                    }else{$table->double('nilai_effisiensi_produksi',25,3)->nullable()->comment('37-Nilai Effisiensi Produksi--')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'tingkat_kehilangan_air')) {
                                $table->double('tingkat_kehilangan_air',25,3)->nullable()->comment('18-Tingkat Kehilangan air BU-%');
                                    
                                    }else{$table->double('tingkat_kehilangan_air',25,3)->nullable()->comment('18-Tingkat Kehilangan air BU-%')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'nilai_tingkat_kehilangan_air')) {
                                $table->double('nilai_tingkat_kehilangan_air',25,3)->nullable()->comment('38-Nilai Tingkat Kehilangan --');
                                    
                                    }else{$table->double('nilai_tingkat_kehilangan_air',25,3)->nullable()->comment('38-Nilai Tingkat Kehilangan --')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'jam_operasi_layanan')) {
                                $table->double('jam_operasi_layanan',25,3)->nullable()->comment('19-Jam Operasi Layanan / har-Jam');
                                    
                                    }else{$table->double('jam_operasi_layanan',25,3)->nullable()->comment('19-Jam Operasi Layanan / har-Jam')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'nilai_jam_operasi_layanan')) {
                                $table->double('nilai_jam_operasi_layanan',25,3)->nullable()->comment('39-Nilai Jam Operasi Layanan--');
                                    
                                    }else{$table->double('nilai_jam_operasi_layanan',25,3)->nullable()->comment('39-Nilai Jam Operasi Layanan--')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'tekanan_sambungan')) {
                                $table->double('tekanan_sambungan',25,3)->nullable()->comment('20-Tekanan Sambungan Pelangg-%');
                                    
                                    }else{$table->double('tekanan_sambungan',25,3)->nullable()->comment('20-Tekanan Sambungan Pelangg-%')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'nilai_tekanan_sambungan')) {
                                $table->double('nilai_tekanan_sambungan',25,3)->nullable()->comment('40-Nilai Tekanan Sambungan P--');
                                    
                                    }else{$table->double('nilai_tekanan_sambungan',25,3)->nullable()->comment('40-Nilai Tekanan Sambungan P--')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'pengganti_meter_air')) {
                                $table->double('pengganti_meter_air',25,3)->nullable()->comment('21-Penggantian Meter Air BUM-%');
                                    
                                    }else{$table->double('pengganti_meter_air',25,3)->nullable()->comment('21-Penggantian Meter Air BUM-%')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'nilai_pengganti_meter_air')) {
                                $table->double('nilai_pengganti_meter_air',25,3)->nullable()->comment('41-Nilai Penggantian Meter A--');
                                    
                                    }else{$table->double('nilai_pengganti_meter_air',25,3)->nullable()->comment('41-Nilai Penggantian Meter A--')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'bobot_kinerja_operasi')) {
                                $table->double('bobot_kinerja_operasi',25,3)->nullable()->comment('22-Bobot Kinerja - Bidang Op--');
                                    
                                    }else{$table->double('bobot_kinerja_operasi',25,3)->nullable()->comment('22-Bobot Kinerja - Bidang Op--')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'rasio_juml')) {
                                $table->double('rasio_juml',25,3)->nullable()->comment('23-Rasio Juml Peg /1000 Plgn-%');
                                    
                                    }else{$table->double('rasio_juml',25,3)->nullable()->comment('23-Rasio Juml Peg /1000 Plgn-%')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'nilai_rasio_juml')) {
                                $table->double('nilai_rasio_juml',25,3)->nullable()->comment('43-Nilai Rasio Juml Peg /100--');
                                    
                                    }else{$table->double('nilai_rasio_juml',25,3)->nullable()->comment('43-Nilai Rasio Juml Peg /100--')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'ratio_dikalt_pegawai')) {
                                $table->double('ratio_dikalt_pegawai',25,3)->nullable()->comment('24-Ratio diklat pegawai/peni-%');
                                    
                                    }else{$table->double('ratio_dikalt_pegawai',25,3)->nullable()->comment('24-Ratio diklat pegawai/peni-%')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'nilai_ratio_diklat_pegawai')) {
                                $table->double('nilai_ratio_diklat_pegawai',25,3)->nullable()->comment('44-Nilai Ratio diklat pegawa--');
                                    
                                    }else{$table->double('nilai_ratio_diklat_pegawai',25,3)->nullable()->comment('44-Nilai Ratio diklat pegawa--')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'rasio_biaya_diklat')) {
                                $table->double('rasio_biaya_diklat',25,3)->nullable()->comment('25-Rasio Biaya Diklat terhad-%');
                                    
                                    }else{$table->double('rasio_biaya_diklat',25,3)->nullable()->comment('25-Rasio Biaya Diklat terhad-%')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'nilai_rasio_biaya_diklat')) {
                                $table->double('nilai_rasio_biaya_diklat',25,3)->nullable()->comment('45-Nilai Rasio Biaya Diklat --');
                                    
                                    }else{$table->double('nilai_rasio_biaya_diklat',25,3)->nullable()->comment('45-Nilai Rasio Biaya Diklat --')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'bobot_kinerja_sdm')) {
                                $table->double('bobot_kinerja_sdm',25,3)->nullable()->comment('26-Bobot Kinerja - Bidang SD--');
                                    
                                    }else{$table->double('bobot_kinerja_sdm',25,3)->nullable()->comment('26-Bobot Kinerja - Bidang SD--')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'nilai_kinerja_total')) {
                                $table->double('nilai_kinerja_total',25,3)->nullable()->comment('27-Total Nilai Kinerja BUMDA--');
                                    
                                    }else{$table->double('nilai_kinerja_total',25,3)->nullable()->comment('27-Total Nilai Kinerja BUMDA--')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'kategori_penilaian_bpkp')) {
                                $table->double('kategori_penilaian_bpkp',25,3)->nullable()->comment('28-Kategori Penilain Kinerja--');
                                    
                                    }else{$table->double('kategori_penilaian_bpkp',25,3)->nullable()->comment('28-Kategori Penilain Kinerja--')->change();
                                        
                                }
                                if(!Schema::hasColumn('dataset.kinerja_bumdam_bpkp', 'nilai_fcr')) {
                                $table->double('nilai_fcr',25,3)->nullable()->comment('280-Pemenuhan Tarif FCR-%');
                                    
                                    }else{$table->double('nilai_fcr',25,3)->nullable()->comment('280-Pemenuhan Tarif FCR-%')->change();
                                        
                                }
                                
                        });
                        
                        if(Schema::hasTable('dataset.kinerja_bumdam_bpkp')){
                            Schema::table('dataset.kinerja_bumdam_bpkp',function (Blueprint $table){
                                $sm = Schema::getConnection()->getDoctrineSchemaManager();
                                $indexesFound = $sm->listTableIndexes('dataset.kinerja_bumdam_bpkp');
                                if(!array_key_exists('dts_2_idx_kodepemda', $indexesFound)){$table->index(['kodepemda'],'dts_2_idx_kodepemda'); }if(!array_key_exists('dts_2_idx_tahun', $indexesFound)){$table->index(['tahun'],'dts_2_idx_tahun'); }if(!array_key_exists('dts_2_idx_tw', $indexesFound)){$table->index(['tahun'],'dts_2_idx_tw'); }if(!array_key_exists('dts_2_idx_status', $indexesFound)){$table->index(['tahun'],'dts_2_idx_status'); }if(!array_key_exists('dts_2_idx_composit', $indexesFound)){$table->index(['kodepemda','tahun','tw','status'],'dts_2_idx_composit'); }if(array_key_exists('dts_2_unique_multy_extend', $indexesFound)){$table->dropUnique('dts_2_unique_multy_extend'); }if(!array_key_exists('dts_2_unique_single', $indexesFound)){$table->unique(['kodepemda','tahun','tw'],'dts_2_unique_single'); }
                            });
                        }
                    }
                    
            }
                
            
            
            public function down(){
                
                   
                
            }
            
        }