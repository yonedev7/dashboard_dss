<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        DB::table('users')->updateOrInsert(['email'=>'dss@domain.com'],[
            'name'=>'dss',
            'email'=>'dss@domain.com',
            'password'=>'$2y$10$PVE6T8LXUMga8PAS/PDhsunppMwGcItxVvfChiwTviAKeIgCdPEBi',

        ]);

        DB::statement("CREATE SCHEMA IF NOT EXISTS master;");
        DB::statement("CREATE SCHEMA IF NOT EXISTS data_konsensus;");
        DB::statement("CREATE SCHEMA IF NOT EXISTS data;");
        DB::statement("CREATE SCHEMA IF NOT EXISTS dataset;");
        DB::statement("CREATE SCHEMA IF NOT EXISTS tools;");




    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
