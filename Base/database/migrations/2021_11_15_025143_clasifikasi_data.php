<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ClasifikasiData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('master.klasifikasi_data',function(Blueprint $table){
            $table->id();
            $table->bigInteger('id_kategori')->unsigned();
            $table->string('kode')->unique();
            $table->string('nama')->unique();
            $table->text('keterangan')->nullable();
            $table->timestamps();

            $table->foreign('id_kategori')
            ->references('id')
            ->on('master.kategori_klasifikasi_data')
            ->onDelete('cascade')
            ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('master.klasifikasi_data');

    }
}
