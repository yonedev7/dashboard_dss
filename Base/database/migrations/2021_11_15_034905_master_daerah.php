<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MasterDaerah extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('master.master_pemda',function(Blueprint $table){
            $table->string('kodepemda',5)->primary();
            $table->string('kodepemda_2',4)->nullable()->index();
            $table->string('kode_sipd')->nullable()->index();
            $table->string('kode_bps')->nullable()->index();
            $table->string('kode_krisna')->nullable()->index();
            $table->string('nama_pemda');
            $table->integer('stapem')->default(2);
            $table->string('regional_1')->nullable()->index();
            $table->string('regional_2')->nullable()->index();
            $table->string('domain_sipd')->nullable();
            $table->string('api_key_sipd')->nullable();

        });

        $pemda=DB::table('master.master_pemda_backup')->get();
        DB::table('master.master_pemda')->insert(json_decode(json_encode($pemda),true));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('master.master_pemda');

    }
}
