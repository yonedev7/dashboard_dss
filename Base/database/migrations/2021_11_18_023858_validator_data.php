<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ValidatorData extends Migration
{
   
    public function up()
    {

        Schema::create('master.validator_data', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->mediumText('keterangan')->nullable();
            $table->timestamps();
        });

        DB::table('master.validator_data')->insert([
            'name'=>'Belum Terdefinisi',
        ]);
    }

    public function down()
    {
        Schema::dropIfExists('master.validator_data');
    }
}
