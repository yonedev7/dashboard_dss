<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Data extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tahun=2021;
        Schema::create('master.data', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('table')->unique();
            $table->string('produsent_data')->nullable();
            $table->bigInteger('id_validator')->unsigned();
            $table->string('kode')->nullable();
            $table->mediumText('definisi_konsep')->nullable();
            $table->mediumText('cara_hitung')->nullable();
            $table->mediumText('methode_pemgambilan')->nullable();
            $table->string('tipe_nilai')->nullable();
            $table->boolean('jenis_data')->default(0);
            $table->string('satuan')->nullable();
            $table->integer('arah_nilai')->default(1);
            $table->string('fungsi_aggregasi')->nullable();
            $table->mediumText('fungsi_rentang_nilai')->default('{"number":{"m":null,"val:[]"},"string":{"m":null,"val:[]"}}');
            $table->foreign('id_validator')
            ->references('id')
            ->on('master.validator_data')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->timestamps();


        });

        DB::table('master.data')->insert([
            [
                "name"=>'Data test 1',
                "table"=>'data_1',
                "produsent_data"=>'DSS-NUWAS',
                "id_validator"=>1,
                "kode"=>"test.01",
                "tipe_nilai"=>'numeric',
                "jenis_data"=>0,
                "satuan"=>'SatuanA',
                "arah_nilai"=>1,
                "fungsi_rentang_nilai"=>'{}'

            ],
            [
                "name"=>'Data test 2',
                "table"=>'data_2',
                "produsent_data"=>'DSS-NUWAS',
                "id_validator"=>1,
                "kode"=>"test.02",
                "tipe_nilai"=>'numeric',
                "jenis_data"=>0,
                "satuan"=>'SatuanA',
                "arah_nilai"=>1,
                "fungsi_rentang_nilai"=>'{}'

            ]
        ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('master.data');

    }
}
