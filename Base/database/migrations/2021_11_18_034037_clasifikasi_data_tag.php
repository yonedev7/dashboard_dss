<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ClasifikasiDataTag extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('master.tag_data', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_data')->unsigned();
            $table->bigInteger('id_klasifikasi')->unsigned();
            $table->timestamps();

            $table->unique(['id_data','id_klasifikasi']);
            $table->foreign('id_data')
            ->references('id')
            ->on('master.data')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('id_klasifikasi')
            ->references('id')
            ->on('master.klasifikasi_data')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('master.tag_data');

    }
}
