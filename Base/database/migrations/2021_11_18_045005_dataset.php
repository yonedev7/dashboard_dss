<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Dataset extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('master.dataset', function (Blueprint $table) {
            $table->id();
            $table->string('penangung_jawab');
            $table->text('kategori')->default('[]');
            $table->boolean('pusat')->default(0);
            $table->boolean('login')->default(0);
            $table->boolean('display_style')->default(0);
            $table->bigInteger('id_menu')->nullable();
            $table->boolean('jenis_dataset')->default(1)->index();
            $table->string('name')->index();
            $table->string('kode')->nullable()->index();
            $table->string('table')->unique();
            $table->integer('interval_pengambilan')->default(1)->index();
            $table->float('point')->default(0)->index();
            $table->text('methode_pengambilan')->nullable();
            $table->mediumText('definisi_konsep')->nullable();
            $table->mediumText('tujuan')->nullable();
            $table->boolean('jenis_distribusi')->default(0);
            $table->dateTime('tanggal_publish')->nullable();
            $table->boolean('status')->default(1)->index();
            $table->bigInteger("id_u_created")->unsigned();
            $table->bigInteger("id_u_updated")->unsigned();
            $table->foreign('id_u_created')
            ->references('id')
            ->on('users')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->index(['kode','status','interval_pengambilan']);
            $table->index(['kode','status','jenis_dataset','jenis_distribusi']);
            $table->index(['kode','status','jenis_dataset']);
            $table->index(['kode','status','jenis_distribusi']);
            $table->index(['kode','point']);


            $table->foreign('id_u_updated')
            ->references('id')
            ->on('users')
            ->onDelete('cascade')
            ->onUpdate('cascade');


            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('master.dataset');

    }
}
