<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DatumDataset extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('master.dataset_data', function (Blueprint $table) {
            $table->id();
            $table->integer('index')->default(0);
            $table->bigInteger('id_dataset')->unsigned()->index();
            $table->bigInteger('id_data')->unsigned()->index();
            $table->string('aggregasi')->nullable();
            $table->string('title_aggregate')->nullable();
            $table->boolean('login')->defalut(0);
            $table->integer('tahun_data')->default(0);
            $table->integer('tw_data')->default(4);
            $table->string('kat')->nullable();
            $table->string('field_name')->index();
            $table->bigInteger('id_u_created')->unsigned();
            $table->bigInteger('id_u_updated')->unsigned();

            $table->timestamps();
            $table->unique(['id_dataset','id_data','tahun_data','tw_data']);
            $table->index(['id_dataset','field_name']);

            $table->foreign('id_data')
            ->references('id')
            ->on('master.data')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('id_u_created')
            ->references('id')
            ->on('users')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('id_u_updated')
            ->references('id')
            ->on('users')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('id_dataset')
            ->references('id')
            ->on('master.dataset')
            ->onDelete('cascade')
            ->onUpdate('cascade');

        });

        Schema::create('master.dataset_scope', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_dataset')->unsigned();
            $table->string('kodepemda',5);
            $table->integer('tahun');

            $table->bigInteger('id_u_created')->unsigned();
            $table->bigInteger('id_u_updated')->unsigned();

            $table->timestamps();
            $table->unique(['id_dataset','kodepemda','tahun']);

            $table->foreign('id_u_created')
            ->references('id')
            ->on('users')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('id_u_updated')
            ->references('id')
            ->on('users')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('id_dataset')
            ->references('id')
            ->on('master.dataset')
            ->onDelete('cascade')
            ->onUpdate('cascade');

        });
        
            
    }

    public function down()
    {
        Schema::dropIfExists('master.dataset_data');
        Schema::dropIfExists('master.dataset_scope');

        
    }
}
