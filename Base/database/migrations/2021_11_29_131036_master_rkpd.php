<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MasterRkpd extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('master.master_urusan',function(Blueprint $table){
            $table->string('kodebidang')->primary();
            $table->string('nama_bidang');
        });

        Schema::create('master.master_suburusan',function(Blueprint $table){
            $table->string('kodesuburusan')->primary();
            $table->string('kodebidang')->unsigned()->index();
            $table->string('nama_sub_bidang');
            $table->foreign('kodebidang')
            ->references('kodebidang')
            ->on('master.master_urusan')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            
        });

        Schema::create('master.master_sipd_pd',function(Blueprint $table){
            $table->id();
            $table->string('kodeskpd')->index();
            $table->string('kodebidang')->unsigned()->index();
            $table->string('kodepemda',5);
            $table->string('nama_skpd');
            $table->timestamps();

            $table->unique(['kodepemda','kodeskpd','kodebidang']);
            $table->foreign('kodebidang')
                ->references('kodebidang')
                ->on('master.master_urusan')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::create('master.master_nomenklatur_rkpd',function(Blueprint $table){
            $table->id();
            $table->string('tahun_penambahan',4);
            $table->text('nama')->index();
            $table->string('kode_urusan')->nullable()->index();
            $table->string('kodebidang')->unsigned()->index();
            $table->string('kodeprogram')->nullable();
            $table->string('kodekegiatan')->nullable();
            $table->string('kode')->index();
            $table->integer('tag')->index();
            $table->boolean('p')->default(0)->index();
            $table->unique(['kode','p']);
            $table->foreign('kodebidang')
                ->references('kodebidang')
                ->on('master.master_urusan')
                ->onDelete('cascade')
                ->onUpdate('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     
        Schema::dropIfExists('master.master_nomenklatur_rkpd');
        Schema::dropIfExists('master.master_suburusan');
        Schema::dropIfExists('master.master_sipd_pd');
        Schema::dropIfExists('master.master_urusan');



    }
}
