<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Rkpd2022 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        // $tahun=2022;
        // $connection='live';
        // $schema='rkpd_'.$tahun;
        
        // DB::statement("CREATE SCHEMA IF NOT EXISTS rkpd_".$tahun.";");
        // DB::statement("CREATE SCHEMA IF NOT EXISTS rpjmd;");

        // if(!Schema::connection($connection)->hasTable($schema.'.sinkron_rkpd')){
        //     Schema::connection($connection)->create($schema.'.sinkron_rkpd',function(Blueprint $table) use ($tahun,$schema){
        //         $table->id();
        //         $table->string('kodepemda',5)->unsigned()->index();
        //         $table->integer('kode_daerah_sipd')->nullable()->index();
        //         $table->integer('rpjmd_awal')->nullable();
        //         $table->integer('rpjmd_ahir')->nullable();
        //         $table->string('basis_nomenklatur')->nullable();
        //         $table->integer('tahun')->default($tahun)->index();
        //         $table->integer('tahap')->nullable()->index();
        //         $table->string('log_sipd')->nullable()->index();
        //         $table->double('log_ang_sipd',20,3)->nullable()->index();
        //         $table->timestamps();
        //         $table->softDeletes();
        //         $table->foreign('kodepemda')
        //         ->references('kodepemda')
        //         ->on('master.master_pemda')
        //         ->onDelete('cascade')
        //         ->onUpdate('cascade');

        //         $table->unique(['kodepemda','tahun','tahap']);

        //     });
        // }


        // if(!Schema::connection($connection)->hasTable($schema.'.meta_rkpd')){
        //     Schema::connection($connection)->create($schema.'.meta_rkpd',function(Blueprint $table) use ($tahun,$schema){
        //         $table->id();
        //         $table->string('kodepemda',5)->unsigned()->index();
        //         $table->integer('kode_daerah_sipd')->nullable()->index();
        //         $table->integer('rpjmd_awal')->nullable();
        //         $table->integer('rpjmd_ahir')->nullable();
        //         $table->string('basis_nomenklatur')->nullable();
        //         $table->integer('tahun')->default($tahun)->index();
        //         $table->integer('tahap')->nullable()->index();
        //         $table->string('log_sipd')->nullable();
        //         $table->double('log_ang_sipd',20,3)->nullable();
        //         $table->double('log_ang_local',20,3)->nullable()->index();
        //         $table->boolean('check_regional')->default(0);
        //         $table->boolean('check_pusat')->default(0);
        //         $table->boolean('publish')->default(0);
        //         $table->mediumText('note')->default(0);
        //         $table->boolean('confirmation_daerah')->default(0);
        //         $table->string('file_upload')->nullable();
        //         $table->timestamps();
        //         $table->softDeletes();
        //         $table->foreign('kodepemda')
        //         ->references('kodepemda')
        //         ->on('master.master_pemda')
        //         ->onDelete('cascade')
        //         ->onUpdate('cascade');

        //         $table->unique(['kodepemda','tahun','tahap']);

        //     });
        // }


        // if(!Schema::connection($connection)->hasTable($schema.'.sub_kegiatan')){
        //     Schema::connection($connection)->create($schema.'.sub_kegiatan',function(Blueprint $table) use ($tahun,$schema){
        //         $table->id();
        //         $table->bigInteger('id_meta')->unsigned()->index();
        //         $table->string('kodebidang')->unsigned()->index();
        //         $table->string('kodeskpd')->nullable()->index();
        //         $table->string('kodeprogram')->unsigned()->index();
        //         $table->string('kodekegiatan')->unsigned()->index();
        //         $table->string('kodesubkegiatan')->unsigned()->index();
        //         $table->float('persentase_pelaksanaan')->index()->default(0);
        //         $table->double('pagu',20,3)->nullable();
        //         $table->timestamps();
        //         $table->softDeletes();
        //         $table->unique(['id_meta','kodebidang','kodeskpd','kodeprogram','kodekegiatan','kodesubkegiatan'],'uniq_'.$schema.'_sub_kegiatan');
        //         $table->index(['id_meta','kodebidang'],'idx_'.$schema.'_bidang');
        //         $table->index(['id_meta','kodeprogram'],'idx_'.$schema.'_program');
        //         $table->index(['id_meta','kodeskpd'],'idx_'.$schema.'_skpd');
        //         $table->index(['id_meta','kodekegiatan'],'idx_'.$schema.'_kegiatan');
        //         $table->index(['id_meta','kodesubkegiatan'],'idx_'.$schema.'_subkegiatan');
        //         $table->index(['id_meta','kodebidang','kodeprogram'],'idx_'.$schema.'_bid_program');
        //         $table->index(['id_meta','kodebidang','kodeprogram','kodekegiatan'],'idx_'.$schema.'_bid_program_keg');
        //         $table->index(['id_meta','kodebidang','kodeskpd','kodeprogram','kodekegiatan'],'idx_'.$schema.'_dr_bid_program_keg');
        //         $table->index(['id_meta','kodebidang','kodeskpd','kodeprogram'],'idx_'.$schema.'_dr_bid_program');

        //         $table->foreign('id_meta')
        //         ->references('id')
        //         ->on($schema.'.meta_rkpd')
        //         ->onDelete('cascade')
        //         ->onUpdate('cascade');

               
        //     });

        // }

        // if(!Schema::connection($connection)->hasTable('rpjmd.indikator_rpjmd')){
        //     Schema::connection($connection)->create('rpjmd.indikator_rpjmd',function(Blueprint $table) use ($tahun,$schema){
        //         $table->id();
        //         $table->string('kodepemda',5)->index();
        //         $table->integer('rpjmd_awal')->nullable();
        //         $table->integer('rpjmd_ahir')->nullable();
        //         $table->string('kodebidang')->unsigned()->index();
        //         $table->string('kodeskpd')->nullable()->index();
        //         $table->string('kodeprogram')->nullable()->index();
        //         $table->string('kodekegiatan')->nullable()->index();
        //         $table->string('kodeindikator')->index();
        //         $table->text('namaindikator')->nullable()->index();
        //         $table->integer('jenis')->default(2);
        //         $table->string('target_1')->nullable();
        //         $table->string('capaian_1')->nullable();
        //         $table->string('target_2')->nullable();
        //         $table->string('capaian_2')->nullable();
        //         $table->string('target_3')->nullable();
        //         $table->string('capaian_3')->nullable();
        //         $table->string('target_4')->nullable();
        //         $table->string('capaian_4')->nullable();
        //         $table->string('target_5')->nullable();
        //         $table->string('capaian_5')->nullable();
        //         $table->string('satuan')->nullable()->index();
        //         $table->timestamps();
        //         $table->index(['kodepemda','rpjmd_awal','rpjmd_ahir'],'idx_'.$schema.'_ind_rpjmnd');
        //         $table->index(['kodepemda','rpjmd_awal','rpjmd_ahir','kodebidang'],'idx_'.$schema.'_ind_bidang');
        //         $table->index(['kodepemda','rpjmd_awal','rpjmd_ahir','kodeprogram'],'idx_'.$schema.'_ind_program');
        //         $table->index(['kodepemda','rpjmd_awal','rpjmd_ahir','kodeskpd'],'idx_'.$schema.'_ind_skpd');
        //         $table->index(['kodepemda','rpjmd_awal','rpjmd_ahir','kodekegiatan'],'idx_'.$schema.'_ind_kegiatan');
        //         $table->index(['kodepemda','rpjmd_awal','rpjmd_ahir','kodebidang','kodeprogram'],'_idx_'.$schema.'ind_bid_program');
        //         $table->index(['kodepemda','rpjmd_awal','rpjmd_ahir','kodebidang','kodeprogram','kodekegiatan'],'_idx_'.$schema.'ind_bid_program_keg');
        //         $table->index(['kodepemda','rpjmd_awal','rpjmd_ahir','kodebidang','kodeskpd','kodeprogram','kodekegiatan'],'_idx_'.$schema.'ind_dr_bid_program_keg');
        //         $table->index(['kodepemda','rpjmd_awal','rpjmd_ahir','kodebidang','kodeskpd','kodeprogram'],'idx_'.$schema.'_ind_dr_bid_program');
        //         $table->unique(['kodepemda','rpjmd_awal','rpjmd_ahir','kodebidang','kodeskpd','kodeprogram','kodekegiatan','kodeindikator'],'uniq_'.$schema.'_indikator');
        //         $table->foreign('kodepemda')
        //         ->references('kodepemda')
        //         ->on('master.master_pemda')
        //         ->onDelete('cascade')
        //         ->onUpdate('cascade');

        //     });

        // }

        // if(!Schema::connection($connection)->hasTable($schema.'.indikator_rpjmd')){
        //     Schema::connection($connection)->create($schema.'.indikator_rpjmd',function(Blueprint $table) use ($tahun,$schema){
        //         $table->id();
        //         $table->bigInteger('id_meta')->unsigned()->index();
        //         $table->bigInteger('id_indikator')->unsigned()->index();
        //         $table->unique(['id_meta','id_indikator']);
        //         $table->index(['id_meta','id_indikator']);
        //         $table->foreign('id_meta')
        //         ->references('id')
        //         ->on($schema.'.meta_rkpd')
        //         ->onDelete('cascade')
        //         ->onUpdate('cascade');

        //         $table->foreign('id_indikator')
        //         ->references('id')
        //         ->on('rpjmd.indikator_rpjmd')
        //         ->onDelete('cascade')
        //         ->onUpdate('cascade');
        //     });
        // }



        // if(!Schema::connection($connection)->hasTable($schema.'.indikator_sub')){
        //     Schema::connection($connection)->create($schema.'.indikator_sub',function(Blueprint $table) use ($tahun,$schema){
        //         $table->id();
        //         $table->bigInteger('id_meta')->unsigned()->index();
        //         $table->string('kodebidang')->unsigned()->index();
        //         $table->string('kodeskpd')->nullable()->index();
        //         $table->string('kodeprogram')->nullable()->index();
        //         $table->string('kodekegiatan')->nullable()->index();
        //         $table->string('kodesubkegiatan')->nullable()->index();
        //         $table->string('kodeindikator')->index();
        //         $table->integer('jenis')->default(5)->index();
        //         $table->text('namaindikator')->nullable();
        //         $table->double('pagu',20,3)->nullable()->index();
        //         $table->string('target')->nullable()->index();
        //         $table->string('capaian')->nullable()->index();
        //         $table->string('satuan')->nullable()->index();
        //         $table->timestamps();
              
        //         $table->unique(['id_meta','kodebidang','kodeskpd','kodeprogram','kodekegiatan','kodesubkegiatan','kodeindikator'],'uniq_'.$schema.'_indikator_sub');
        //         $table->index(['id_meta','kodebidang'],'idx_'.$schema.'ind_bidang');
        //         $table->index(['id_meta','kodeprogram'],'idx_'.$schema.'ind_program');
        //         $table->index(['id_meta','kodeskpd'],'idx_'.$schema.'ind_skpd');
        //         $table->index(['id_meta','kodekegiatan'],'idx_'.$schema.'ind_kegiatan');
        //         $table->index(['id_meta','kodesubkegiatan'],'idx_'.$schema.'ind_subkegiatan');
        //         $table->index(['id_meta','kodebidang','kodeprogram'],'idx_'.$schema.'ind_bid_program');
        //         $table->index(['id_meta','kodebidang','kodeprogram','kodekegiatan'],'idx_'.$schema.'ind_bid_program_keg');
        //         $table->index(['id_meta','kodebidang','kodeskpd','kodeprogram','kodekegiatan'],'idx_'.$schema.'ind_dr_bid_program_keg');
        //         $table->index(['id_meta','kodebidang','kodeskpd','kodeprogram'],'idx_'.$schema.'ind_dr_bid_program');
               
            

        //         $table->foreign('id_meta')
        //         ->references('id')
        //         ->on($schema.'.meta_rkpd')
        //         ->onDelete('cascade')
        //         ->onUpdate('cascade');

               

        //     });

        // }




    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tahun=2022;
        $connection='live';
        $schema='rkpd_'.$tahun;
        
        schema::connection($connection)->dropIfExists($schema.'.indikator_rpjmd');
        schema::connection($connection)->dropIfExists('rpjmd.indikator_rpjmd');
        $arr=['indikator_sub','sub_kegiatan','meta_rkpd','sinkron_rkpd'];
        foreach($arr as $i){
            Schema::connection($connection)->dropIfExists($schema.'.'.$i);
        }
        

    }
}
