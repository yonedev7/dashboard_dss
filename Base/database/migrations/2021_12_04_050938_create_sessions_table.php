<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sessions', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->foreignId('user_id')->nullable()->index();
            $table->string('ip_address', 45)->nullable();
            $table->text('user_agent')->nullable();
            $table->text('payload')->nulable();
            $table->integer('access_tahun')->nullable()->index();
            $table->string('kodepemda',5)->nullable()->index();
            $table->string('kodebidang')->nullable()->index();
            $table->string('kode_subbidang')->nullable()->index();
            $table->string('api_token')->nullable()->index();
            $table->integer('last_activity')->index();
            $table->index(['user_id','access_tahun']);
            $table->index(['user_id','api_token']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sessions');
    }
}
