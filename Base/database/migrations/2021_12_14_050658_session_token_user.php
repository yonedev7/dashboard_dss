<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SessionTokenUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
            DB::statement("CREATE VIEW public.session_token AS
            SELECT u.id,u.name,u.email,u.password,s.id as session_id,s.ip_address,s.user_agent,s.api_token,s.access_tahun,s.kodepemda,s.kodebidang,s.kode_subbidang FROM public.users AS u left join public.sessions as s on s.user_id = u.id");
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::statement("DROP VIEW  public.session_token");
    }
}
