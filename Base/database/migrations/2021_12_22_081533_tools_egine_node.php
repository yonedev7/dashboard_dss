<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ToolsEgineNode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('tools.egine_log', function (Blueprint $table) {
            $table->id();
            $table->string('egine_id')->nullable()->index();
            $table->string('type')->nullable()->index();
            $table->string('name')->nullable();
            $table->string('log_1')->nullable();
            $table->string('log_2')->nullable();
            $table->string('log_3')->nullable();
            $table->unique(['type','egine_id']);


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('tools.egine_log');
    }
}
