<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserApiInstansi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('public.user_api_public', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->boolean('status')->default(1);
            $table->string('kodepemda',5)->nullable();
            $table->string('kodebidang')->nullable();
            $table->string('kode_subbidang')->nullable();
            $table->string('api_token')->nullable();
            $table->string('host')->nullable();
            $table->string('url_last_get')->nullable();
            $table->string('ip')->nullable();
            $table->dateTime('last_active')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('public.user_api_public');

    }
}
