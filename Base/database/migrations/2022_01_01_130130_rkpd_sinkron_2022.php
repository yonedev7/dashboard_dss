<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RkpdSinkron2022 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        // $tahun=2022;
        // $connection='live';
        // $schema='rkpd_'.$tahun;
        
      

        // if(!Schema::connection($connection)->hasTable($schema.'.sinkron_rkpd')){
        //     Schema::connection($connection)->create($schema.'.sinkron_rkpd',function(Blueprint $table) use ($tahun,$schema){
        //         $table->id();
        //         $table->string('kodepemda',5)->unsigned()->index();
        //         $table->integer('kode_daerah_sipd')->nullable()->index();
        //         $table->integer('rpjmd_awal')->nullable();
        //         $table->integer('rpjmd_ahir')->nullable();
        //         $table->string('basis_nomenklatur')->nullable();
        //         $table->integer('tahun')->default($tahun)->index();
        //         $table->integer('tahap')->nullable()->index();
        //         $table->string('log_sipd')->nullable()->index();
        //         $table->double('log_ang_sipd',20,3)->nullable()->index();
        //         $table->timestamps();
        //         $table->softDeletes();
        //         $table->foreign('kodepemda')
        //         ->references('kodepemda')
        //         ->on('master.master_pemda')
        //         ->onDelete('cascade')
        //         ->onUpdate('cascade');

        //         $table->unique(['kodepemda','tahun','tahap']);

        //     });
        // }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        $tahun=2022;
        $connection='live';
        $schema='rkpd_'.$tahun;
        Schema::dropIfExists($schema.'.sinkron_rkpd');

    }
}
