<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DatasetDisplay extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('master.dataset_display', function (Blueprint $table) {
            $table->id();
            $table->biginteger('id_dataset')->unsigned();
            $table->string('tingkat')->index();
            $table->mediumText('schema')->nullable()->default('{}');
            $table->mediumText('short_query_1')->nullable();
            $table->mediumText('short_query_2')->nullable();
            $table->mediumText('short_query_3')->nullable();
            $table->mediumText('short_query_4')->nullable();
            $table->unique(['id_dataset','tingkat']);
            $table->foreign('id_dataset')
            ->references('id')
            ->on('master.dataset')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master.dataset_display');

        //
    }
}
