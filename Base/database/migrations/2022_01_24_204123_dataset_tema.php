<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DatasetTema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('master.dataset_tema', function (Blueprint $table) {
            $table->id();
            $table->biginteger('id_dataset')->unsigned();
            $table->biginteger('id_tema')->unsigned();
            $table->unique(['id_dataset','id_tema']);
            $table->foreign('id_dataset')
            ->references('id')
            ->on('master.dataset')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->foreign('id_tema')
            ->references('id')
            ->on('master.tema')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('master.dataset_tema');

    }
}
