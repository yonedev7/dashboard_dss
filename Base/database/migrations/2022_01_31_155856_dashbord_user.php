<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DashbordUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('master.dashboard_user_chart', function (Blueprint $table) {
            $table->id();
            $table->string('kode');
            $table->string('title')->nullable();
            $table->bigInteger('id_user')->unsigned()->index();
            $table->timestamps();

            $table->unique(['kode','id_user']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('master.dashboard_user_chart');
        
    }
}
