<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Timeline extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('master.timeline', function (Blueprint $table) {
            $table->id();
            $table->string('kode')->nullable()->index();
            $table->bigInteger('id_parent')->nullable()->unsigned();
            $table->text('kegiatan')->nullable();
            $table->float('persentase_pelaksanaan')->default(0);
            $table->text('keterangan_tugas')->nullable();
            $table->text('catatan_pelaksanaan')->nullable();
            $table->date('start')->nullable();
            $table->date('end')->nullable();
            $table->bigInteger('id_user')->unsigned()->index();
            $table->timestamps();
            $table->foreign('id_parent')
            ->references('id')
            ->on('master.timeline')
            ->onDelete('cascade')
            ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master.timeline');

    }
}
