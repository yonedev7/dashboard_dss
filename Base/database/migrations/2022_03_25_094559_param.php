<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Param extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('dataset.params_bind', function (Blueprint $table) {
            $table->id('id');
            $table->string('name')->unique();
            $table->text('keterangan')->nullable();
            $table->integer('type_scope')->detault(0);
            $table->integer('type')->detault(0);
            $table->bigInteger('id_dataset')->nullable()->unsigned();
            $table->bigInteger('id_field')->nullable()->unsigned();
            $table->text('field')->nullable();
            $table->boolean('last')->detault(0);
            $table->text('table')->nullable();
            $table->text('bind')->nullable()->detault('[]');
            $table->index(['table','field']);
            $table->index(['id_dataset','id_field']);
            $table->index(['bind']);

            $table->foreign('id_dataset')
            ->references('id')
            ->on('master.dataset')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            
            $table->foreign('id_field')
            ->references('id')
            ->on('master.dataset_data')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('dataset.params_bind');

       
    }
}
