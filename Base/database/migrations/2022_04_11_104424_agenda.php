<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Agenda extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('dataset.agenda', function (Blueprint $table) {
            $table->id('id');
            $table->string('name');
            $table->integer('tahun');
            $table->date('pel_awal');
            $table->date('pel_ahir');
            $table->text('keterangan')->nullable();
            $table->string('pelaksana')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('dataset.agenda');

    }
}
