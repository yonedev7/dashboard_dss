<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgendaFile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('dataset.agenda_dokumen_pendukung', function (Blueprint $table) {
            $table->id('id');
            $table->bigInteger('id_agenda')->unsigned();
            $table->string('name');
            $table->string('file');
            $table->string('kodepemda',5);
            $table->foreign('id_agenda')
                ->references('id')
                ->on('dataset.agenda')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->index(['id_agenda','kodepemda']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dataset.agenda_dokumen_pendukung');
    }
}
