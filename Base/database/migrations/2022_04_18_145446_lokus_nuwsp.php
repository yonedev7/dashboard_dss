<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LokusNuwsp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('master.pemda_lokus', function (Blueprint $table) {
            $table->id('id');
            $table->string('kodepemda',5)->unsigned()->index();
            $table->integer('tahun_bantuan')->nullable()->index();
            $table->string('tipe_bantuan')->index();
            $table->mediumText('lokus')->nullable();
            $table->integer('tahun_proyek')->nullable()->index();


            $table->foreign('kodepemda')
            ->references('kodepemda')
            ->on('master.master_pemda')
            ->onDelete('cascade')
            ->onUpdate('cascade');

        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('master.pemda_lokus');

    }
}
