<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MasterMenuDahsborad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('master.menus', function (Blueprint $table) {
        
            $table->id('id');
            $table->string('kode_parent',20)->index();
            $table->integer('type')->index()->default(0);
            $table->boolean('status')->default(false);
            $table->integer('index')->index()->default(0);
            $table->string('title')->nullable();
            $table->mediumText('keterangan')->nullable();
            $table->string('contoller')->nullable();


        
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('master.menus');

    }
}
