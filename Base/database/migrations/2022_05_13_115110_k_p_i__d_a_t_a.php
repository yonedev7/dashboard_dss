<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class KPIDATA extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('master.kpi_data_monev', function (Blueprint $table) {
            $table->id('id');
            $table->bigInteger('id_kpi')->unsigned();
            $table->bigInteger('id_data')->unsigned();
            $table->integer('index')->default(0);
            $table->boolean('input_allow')->default(true);
            $table->string('type_baseline')->default('all_years');
            $table->mediumText('keterangan')->nullable();

            $table->foreign('id_kpi')
            ->references('id')
            ->on('master.kpi')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('id_data')
            ->references('id')
            ->on('master.dataset')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('master.kpi_data_monev');

    }
}
