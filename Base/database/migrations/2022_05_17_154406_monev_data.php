<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MonevData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master.monev_data', function (Blueprint $table) {
            $table->id('id');
            $table->string('name');
            $table->bigInteger('id_data_base_1')->unsigned()->nullable()->index();
            $table->bigInteger('id_data_base_2')->unsigned()->nullable()->index();
            $table->bigInteger('id_data_base_3')->unsigned()->nullable()->index();
            $table->bigInteger('id_data_kesepakatan')->unsigned()->nullable()->index();
            $table->dateTime('tanggal_input_start')->nullable();
            $table->dateTime('tanggal_input_end')->nullable();
            $table->dateTime('tanggal_verifikasi_start')->nullable();
            $table->dateTime('tanggal_verifikasi_end')->nullable();
            $table->foreign('id_data_base_1')
                ->references('id')
                ->on('master.dataset')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('id_data_base_2')
                ->references('id')
                ->on('master.dataset')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('id_data_base_3')
                ->references('id')
                ->on('master.dataset')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('id_data_kesepakatan')
                ->references('id')
                ->on('master.dataset')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master.monev_data');

    }
}
