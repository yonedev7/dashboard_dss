<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SavedChart extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('analisa.saved_chart', function (Blueprint $table) {
            $table->id('id');
            $table->string('name');
            $table->bigInteger('id_user')->unsigned();
            $table->bigInteger('id_dataset')->index();
            $table->string('type');
            $table->mediumText('data')->default("{}");
            $table->timestamps();

            $table->foreign('id_user')
            ->references('id')
            ->on('public.users')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analisa.saved_chart');
    }
}
