<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PenilaianKpi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('analisa.penilaian_kpi', function (Blueprint $table) {
            $table->id('id');
            $table->bigInteger('id_kpi')->unsigned()->index();
            $table->string('kodepemda',5)->unsigned()->index();
            $table->boolean('terpenuhi')->default(true)->index();
            $table->integer('tahun')->default(0)->index();
            $table->boolean('verifikasi')->default()->index();
            $table->mediumText('keterangan')->nullable();
            $table->bigInteger('id_user')->unsigned();
            $table->unique(['kodepemda','tahun','id_kpi']);

            $table->timestamps();

            $table->foreign('id_kpi')
            ->references('id')
            ->on('master.kpi')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('id_user')
            ->references('id')
            ->on('public.users')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('kodepemda')
            ->references('kodepemda')
            ->on('master.master_pemda')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('analisa.penilaian_kpi');

    }
}
