<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MasterPdam extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('master.master_bu', function (Blueprint $table) {
            $table->id('id');
            $table->string('nama_bu')->index();
            $table->string('kategori_bu')->nullable()->deafult('BUMDAM');
            $table->string('alamat')->nullable();
            $table->string('long')->nullable();
            $table->string('profile_pic')->nullable();
            $table->string('lat')->nullable();
            $table->string('nomer_telpon',14)->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('master.master_bu');

    }
}
