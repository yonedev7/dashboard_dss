<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PdamPemda extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('master.master_bu_pemda', function (Blueprint $table) {
            $table->id('id');
            $table->bigInteger('id_bu')->unsigned()->index();
            $table->string('kodepemda',5)->unsigned()->index();
            $table->unique(['id_bu','kodepemda']);
            $table->foreign('id_bu')
            ->references('id')
            ->on('master.master_bu')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('kodepemda')
            ->references('kodepemda')
            ->on('master.master_pemda')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('master.master_bu_pemda');

    }
}
