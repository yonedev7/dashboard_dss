<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class StaticView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analisa.static_view', function (Blueprint $table) {
            $table->id('id');
            $table->string('title')->nullable();
            $table->string('param_1')->nullable();
            $table->string('param_2')->nullable();
            $table->string('param_3')->nullable();
            $table->string('param_4')->nullable();
            $table->string('param_5')->nullable();
            $table->string('param_6')->nullable();
            $table->string('param_7')->nullable();
            $table->string('param_8')->nullable();
            $table->string('param_9')->nullable();
            $table->string('param_10')->nullable();
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('analisa.static_view');

    }
}
