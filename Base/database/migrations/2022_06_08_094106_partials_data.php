<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PartialsData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('analisa.partials_data', function (Blueprint $table) {
            $table->id('id');
            $table->string('title')->nullable();
            $table->mediumText('keterangan')->nullable();
            $table->string('view_path')->nullable();
            $table->string('controller_path')->nullable();
            $table->string('route')->nullable();
            $table->string('url_static')->nullable();
            $table->string('param_2')->nullable();
            $table->string('param_3')->nullable();
            $table->string('param_4')->nullable();
            $table->string('param_5')->nullable();
            $table->string('param_6')->nullable();
            $table->string('param_7')->nullable();
           
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('analisa.partials_data');

    }
}
