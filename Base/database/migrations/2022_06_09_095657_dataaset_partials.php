<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataasetPartials extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('analisa.dataset_partials', function (Blueprint $table) {
            $table->id('id');
            $table->bigInteger('id_dataset')->unsigned();
            $table->bigInteger('id_partial')->unsigned();
            $table->unique(['id_dataset','id_partial']);

            $table->foreign('id_dataset')
            ->references('id')
            ->on('master.dataset')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('id_partial')
            ->references('id')
            ->on('analisa.partials_data')
            ->onDelete('cascade')
            ->onUpdate('cascade');

           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analisa.dataset_partials');

        //
    }
}
