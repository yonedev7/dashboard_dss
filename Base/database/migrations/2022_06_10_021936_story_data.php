<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class StoryData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('story_data.story_kegiatan', function (Blueprint $table) {
            $table->id('id');
            $table->bigInteger('id_dataset')->unsigned();
            $table->string('kegiatan')->nullable();
            $table->mediumText('keterangan')->nullable();
            $table->date('tanggal_pelaksanaan')->nullable();
            $table->mediumText('dokumentasi')->default('[]');


            $table->foreign('id_dataset')
            ->references('id')
            ->on('master.dataset')
            ->onDelete('cascade')
            ->onUpdate('cascade');

          

    
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('story_data.story_kegiatan');

        //
    }
}
