<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DatasteSandingan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('master.dataset_sandingan', function (Blueprint $table) {
            $table->id('id');
            $table->bigInteger('id_dataset')->unsigned();
            $table->bigInteger('id_dataset_sandingan')->unsigned();
            $table->unique(['id_dataset','id_dataset_sandingan']);

            $table->foreign('id_dataset')
            ->references('id')
            ->on('master.dataset')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('id_dataset_sandingan')
            ->references('id')
            ->on('master.dataset')
            ->onDelete('cascade')
            ->onUpdate('cascade');

           

           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('master.dataset_sandingan');

    }
}
