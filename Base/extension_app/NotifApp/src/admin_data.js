const { Buttons,Client,MessageMedia,List } = require('whatsapp-web.js');
var moment = require('moment');
var env;

const fs = require("fs");
var XLSX = require("xlsx");


const util = require('util');
const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);
const makeDir = util.promisify(fs.mkdir);
const dirExist= util.promisify(fs.exists);
const permitionFile=util.promisify(fs.chmod);

class HandleAdmin{
    PDF=null;
    Utarget=null;
    Conn=null;
    constructor(conDB,UserClient,puppet,env){
        this.Conn=conDB;
        this.Utarget=UserClient;
        this.PDF=puppet;
        env=env;
    }

    ListData=async function () {
        
    }
    delay=async function(time){
        return new Promise(function(resolve) { 
            setTimeout(resolve, time)
        });
    }

    BuildPath=async function(prefix_file,type,file_type){
        var date=new moment();
        var mime='pdf';
        switch(file_type){
            case 'pdf':
                mime='.pdf';
            break;
            case 'excel':
                mime='.xlsx';
            break;
        }

        var name=prefix_file+'-'+date.format('D-MM-yyyy')+mime;
        var tahun=date.format('yyyy');
        var target=__dirname+env.PDF_PATH+'/'+type+'/'+tahun+'/';
        var path_x=__dirname+'/..';
        var tg=target.replaceAll(__dirname+'/..','').split('/');
        for(var i =0;i<tg.length;i++){
            if(tg[i]){
                path_x+='/'+tg[i];
                if(!(await dirExist(path_x))){
                        await makeDir(path_x);
                }
            }
        };

        return {
            name:name,
            target:target,
            date:date
        }
    }

    SaveDoc=async function (target,name,PDF) {
        var path_x=__dirname+'/..';
        var tg=target.replaceAll(__dirname+'/..','').split('/');
        for(var i =0;i<tg.length;i++){
            if(tg[i]){
               if(!tg[i].includes('.')){
                    path_x+='/'+tg[i];
                    if(!(await dirExist(path_x))){
                            await makeDir(path_x);
                    }
               }
            }
        };
        
        await writeFile(target+name,PDF);
        return true;
        
    }
    getRekapPemda=async function (msg,tahun,kodepemda,file_type='pdf') {
        var meta_path=await this.BuildPath('profile-pemda-'+kodepemda+'-'+this.Utarget.roles.join('-'),'admin',file_type);
        const existfile=await dirExist(meta_path.target+meta_path.name);
        if(existfile){

            const media = MessageMedia.fromFilePath(meta_path.target+meta_path.name);
            msg.reply(media);

        }else{
                var params=[];
                this.Utarget.dataset.forEach((el,k)=>{
                    params.push(el.id);
                });
                var FILEBUILDED=await this.PDF.buildPdf(env.USER_HOST+'/dash/'+tahun+'/sistem-informasi/module-profile/pdf/pemda/'+kodepemda+'/?id_dataset='+encodeURI(params.join(',')),file_type);
                if(FILEBUILDED){

                   if(file_type=='pdf'){
                    await this.SaveDoc(meta_path.target,meta_path.name,FILEBUILDED);
                   }else if(file_type=='excel'){
                       await  XLSX.writeFile(FILEBUILDED, meta_path.target+meta_path.name,{ bookType:"xlsx", bookSST:false });
                   }
                    
                   const media = MessageMedia.fromFilePath(meta_path.target+meta_path.name);
                    msg.reply(media);
                }else{
                    msg.reply("Sistem Masih Dalam perbaikan!\nsilahkan mencoba beberapa saat lagi!");
                }
        }
        
    }

    Rekap=async function (msg,id_dataset=null,tahun=null) {
        var meta_path=await this.BuildPath('rekap-'+this.Utarget.roles.join('-'),'admin');
        if(tahun==null){
            tahun=(new Date()).getFullYear();
        }

        if(!id_dataset){
            const existfile=await dirExist(meta_path.target+meta_path.name);
            if(existfile){
                console.log('try get old data');

                const media = MessageMedia.fromFilePath(meta_path.target+meta_path.name);
                msg.reply(media);
            }else{
                console.log('try get new data');
                var PDF=await this.PDF.buildPdf(env.USER_HOST+'/dash/'+tahun+'/sistem-informasi/module-dataset/build-env');
                if(PDF){

                    await this.SaveDoc(meta_path.target,meta_path.name,PDF);
                    const media = MessageMedia.fromFilePath(meta_path.target+meta_path.name);
                    msg.reply(media);
                }else{
                    msg.reply("Sistem Masih Dalam perbaikan!\nsilahkan mencoba beberapa saat lagi!");
                }
            }
            
        }
    }
}

module.exports={
    HandleAdmin:HandleAdmin
}