

const team_number=[
    {
        name:'Pak Reza (DSS Team Leader)',
        regional:['I','II','III','IV'],
        utama:true,
        number:'6281288644609'
    },
    {
        name:'Yuan (DSS Programmer)',
        regional:['I','II','III','IV'],
        utama:false,
        number:'6287771079782'
    },
    {
        name:'Astri (DSS Team)',
        regional:['I'],
        utama:false,
        number:'6281298541743'
    },
    {
        name:'Andika (DSS Team)',
        regional:['II','III'],
        number:'6281513492616'
    },
    {
        name:'Nani (DSS Team)',
        regional:['IV'],
        utama:false,
        number:'6285656422053'
    },
    {
        name:'Sugeng (DSS Team)',
        regional:[],
        utama:false,
        number:'6285656422053'
    },
    {
        name:'Pak Anton (TACT-LG Team Leader)',
        regional:["I","II","III","IV"],
        utama:true,
        number:'62818737058'
    },
    {
        name:'Pak Riko (TACT-LG Rekap Data)',
        regional:["I","II","III","IV"],
        utama:true,
        number:'628128051351'
    },
    {
        name:'Pak Slamet (TACT-LG TA)',
        regional:["I","II","III","IV"],
        utama:true,
        number:'628119222936'
    },
    {
        name:'Pak Didi (TACT-LG TA)',
        regional:["I","II","III","IV"],
        utama:true,
        number:'6281389069408'
    },
    {
        name:'Pak Seif (TACT-LG TA)',
        regional:["I","II","III","IV"],
        utama:true,
        number:'6281384505018'
    },
    {
        name:'Pak Fredy (TACT-LG TA)',
        regional:["I","II","III","IV"],
        utama:true,
        number:'6285717004859'
    }
];

const team_group=[
    {
        name:'DSS Group Pengadegan',
        number:'6287802088980-1583734412'
    }
]


exports.default ={
    team_number:team_number,
    team_group:team_group
}