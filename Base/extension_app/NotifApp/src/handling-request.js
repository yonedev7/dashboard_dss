const { Buttons,Client,MessageMedia,List } = require('whatsapp-web.js');
const pg= require('pg')
var moment = require('moment');
const Vue=require('vue');
var blade = require('blade');
const fs = require("fs");
( blade.compile(fs.readFileSync("./src/template/rekap.jade").toString(),{filename:'920920920.html'},(err,tml)=>{
    console.log(tml,err);
}));

class Handle{
    client=null;
    constructor(){
        this.client = new pg.Client({
            user: 'postgres',
            host: '0.0.0.0',
            database: 'NUWSP2',
            password: 'Uba2013?',
            port: 5432,
            }
        )
        this.init();

    }
    getSender= async function(number){
       return (await this.client.query("select id,name from public.users where phone_number ilike '%"+(number.split('@')[0]||'')+"%'")).rows[0]||null;
    }
    
    init = async function (){
        await this.client.connect();
    }

    listPemdaLokus=async function (id_user) {
        var pm=null;
        var user=(await this.client.query('select * from public.users where id='+id_user)).rows[0]||null;
        var role=(await this.client.query("select r.name from public.roles as r join public.model_has_roles as hr on hr.role_id=r.id where hr.model_id="+id_user+"")).rows.map(el=>{
            return el.name;
        });

        if(role.includes('TACT-REGIONAL')){
            pm= (await this.client.query("select kodepemda from master.master_pemda where regional_1='"+user.regional+"'")).rows.map(el=>{return el.kodepemda;});
        }else{
            pm= (await this.client.query("select kodepemda from master.master_pemda where kodepemda <> '0'")).rows.map(el=>{return el.kodepemda;});
        }

        return {
            role:role,
            pemdas:pm
        }

    }
    
    listResponse= async function (method,obms) {
        var tahun=(new Date()).getFullYear();
        console.log('users' ,obms.from.split('@')[0]||'');
        if(method.includes('dts.')){
            var id_dataset=parseInt(obms.selectedRowId.replace('dts.',''));
            var dataset=(await this.client.query("select * from master.dataset where id="+id_dataset)).rows[0]||null;

            if(dataset){
                var table=dataset.table;
                var status='dts_'+dataset.id;
                if(dataset.table.includes('[TAHUN]')){
                    status+='_'+tahun;
                    table=table.replace('[TAHUN]',tahun);

                }

                var exist=(await this.client.query("select count(*) from pg_tables where tablename in ('"+status+"','"+table+"') and schemaname in ('dataset','status_data')")).rows[0];
                if(exist.count>=2){
                    var date=new Date();
                    var MyAccount=this.getSender(obms.from)||null;
                    var acc=this.listPemdaLokus(MyAccount.id||null);
                    console.log('acc',acc);
                    var data=(await this.client.query(`
                    select max(std.status) as status, mp.kodepemda, max(mp.nama_pemda) as name,max(d.id) as id_data,
max(case when (std.id is not null) then (case when std.status=2 then std.validate_date else  std.verified_date end) else d.tanggal_pengisian end ) as updated  
from  master.master_pemda as mp
left join dataset.`+table+` as d on (d.kodepemda=mp.kodepemda) 
left join status_data.`+status+` as std on (std.kodepemda =d.kodepemda and std.tahun=d.tahun) 
group by mp.kodepemda 
order by max(case when (std.id is not null) then (case when std.status=2 then std.validate_date else  std.verified_date end) else d.tanggal_pengisian end ) desc`)).rows;
                    var series={
                        'x':{
                            name:'Belum Memiliki Data',
                            y:data.filter(el=>{
                                return el.status==null && el.id_data==null;
                            }).map(el=>{
                                return {
                                    name:el.name,
                                }
                            }).length
                        },
                        '0':{
                            name:'Baru Terinput',
                            y:data.filter(el=>{
                                return el.status==null && el.id_data!=null;
                            }).map(el=>{
                                return {
                                    name:el.name,
                                }
                            }).length
                        },
                        '1':{
                            name:'Terverifikasi',
                            y:data.filter(el=>{
                                return el.status==1;
                            }).map(el=>{
                                return {
                                    name:el.name,
                                }
                            }).length
                        },
                        '2':{
                            name:'Tervalidasi',
                            y:data.filter(el=>{
                                return el.status==2;
                            }).map(el=>{
                                return {
                                    name:el.name,
                                }
                            }).length
                        }
                    }

                    var this_day={
                       
                        '0':{
                            name:'Baru Terinput',
                            data:data.filter(el=>{
                                return (el.status==null && el.updated &&  el.id_data!=null) && ((moment(el.updated).diff(moment(),'days')==0) );
                            }).map(el=>{
                                return el.name;
                                
                            }),
                            y:data.filter(el=>{
                                return (el.status==null && el.updated &&  el.id_data!=null) && ((moment(el.updated).diff(moment(),'days')==0) );
                            }).map(el=>{
                                return {
                                    name:el.name,
                                }
                            }).length
                        },
                        '1':{
                            name:'Terverifikasi',
                            data:data.filter(el=>{
                                return (el.status==1 && el.updated) && ((moment(el.updated).diff(moment(),'days')==0) );
                            }).map(el=>{
                                return el.name;
                                
                            }),
                            y:data.filter(el=>{
                                return (el.status==1 && el.updated) && ((moment(el.updated).diff(moment(),'days')==0) );
                            }).map(el=>{
                                return {
                                    name:el.name,
                                }
                            }).length
                        },
                        '2':{
                            name:'Tervalidasi',
                            data:data.filter(el=>{
                                return (el.status==2 && el.updated) && ((moment(el.updated).diff(moment(),'days')==0) );
                            }).map(el=>{
                                return el.name;
                            }),
                            y:data.filter(el=>{
                                return (el.status==2 && el.updated) && ((moment(el.updated).diff(moment(),'days')==0) );
                            }).map(el=>{
                                return {
                                    name:el.name,
                                }
                            }).length
                        }

                    }

                    var messs=Object.entries(this_day).filter((el,k)=>{
                        return el[1].y;
                    }).map((el,k)=>{
                        return el[1].name+' '+el[1].y+" Pemda \n"+el[1].data.join(', ');
                    }).join(" ");

                    if(messs){
                        obms.reply('Kondisi Update Tebaru '+dataset.name+' Tahun '+tahun);
                        obms.reply(messs);

                    }else{
                        console.log('tidak ada',series,this_day);
                        obms.reply('Tidak Terdapat  Update Data '+dataset.name+' Tahun '+tahun+" Hari Ini.! \n");
                    }


                }
            }
        }
        
    }
    
    Replay=async (method,obmes)=>{
        console.log(method);


        switch(method){
            case "/list-data":
                var dtsU=await this.client.query('select * from master.dataset where admin_menu=0');
                
                var dtsP=await this.client.query('select * from master.dataset where admin_menu=1');

                const productsList = new List(
                    "Pilih Dataset",
                    "Lihat Dataset",
                    [
                      {
                        title: "Dataset Utama",
                        rows: dtsU.rows.map(el=>{
                            
                                return {id:'dts.'+el.id,title:el.name};
                            })
                        
                      },
                      {
                        title: "Dataset Pendukung",
                        rows: dtsP.rows.map(el=>{
                            return {id:'dts.'+el.id,title:el.name};
                        }),
                      },
                    ],
                    "Mohon Pilih Dataset"
                  );
                  obmes.reply(productsList);

            break;
        }
    }
}



module.exports={
    Handle:Handle
}