const puppeteer = require('puppeteer');
var env;
const fs = require('fs');
const util = require('util');
const path = require('path');
const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);
const makeDir = util.promisify(fs.mkdir);
const dirExist= util.promisify(fs.exists);
const moment=require('moment');
var XLSX = require("xlsx");
const HtmlTableToJson = require('html-table-to-json');
const { table } = require('console');
const { delay } = require('lodash');
const { app } = require('firebase-admin');
const { FSWatcher } = require('chokidar');

const { Storage } = require('./storage');
const StorageEgine=new Storage();


class PDFEgine{

    logined=false;
    browser = null;
    page=null;
    env={};
    user={
        email:'',
        password:''
    }
    cookiesFilePath = __dirname+'/../session-dss';


    constructor(pup,account,env){
        this.env=env;
        this.user.email=account.email;
       this.user.password=account.password;
       this.init(pup);
       this.until=require('util');

    }

    delay = async function (time) {
        return new Promise(function(resolve) {
            setTimeout(resolve, time)
        });
    }

    createrPage=async function(){

        var page= await  this.browser.newPage();

        await this.appendSession(page);
        console.log('page create');
        return page;

    }

    init=async function(pup){

        this.browser =pup ;

        // var page=await this.createrPage();
        // await this.delay(3000);

        // await this.evalLogined(env.USER_HOST+'/dash/'+(new Date()).getFullYear()+'/sistem-informasi',0,page);
        var date=new moment();;
        var name='rekap-'+('xxx')+'-'+date.format('D-MM-yyyy')+'.pdf';
        var tahun=date.format('yyyy');
        // await this.delay(1000);
        // await  page.close();


        // var PDF=await this.buildPdf(env.USER_HOST+'/dash/'+tahun+'/sistem-informasi/module-dataset/build-env');
        // if(PDF){

        //     var path_x=__dirname+'/..';
        //     var tg=target.replaceAll(__dirname+'/..','').split('/');
        //     for(var i =0;i<tg.length;i++){
        //         if(tg[i]){
        //             path_x+='/'+tg[i];
        //             if(!(await dirExist(path_x))){
        //                     await makeDir(path_x);
        //             }
        //         }
        //     };


        //     await writeFile(target+name,PDF);
        // }

        return true;

    }

    appendSession=async function (page) {
        const cookiesString = await readFile(this.cookiesFilePath+"/cookiesString.json",(err,data)=>{
            if(err){
                return '[]';
            }else{

                return data;

            }
        });

        if(cookiesString){
             const cookies = JSON.parse(cookiesString);
            await page.setCookie(...cookies);

        }

        const sessionStorageString =( await readFile(this.cookiesFilePath+"/sessionStorageString.json",function(err){
            if(err){
                return '{}';
            }
        })||'[]');
        const sessionStorage = JSON.parse(sessionStorageString);

        const localStorageString = ( await readFile(this.cookiesFilePath+"/localStorageString.json",function(err){
            if(err){
                return '[]';
            }
        }))||'[]';
        const localStorage = JSON.parse(localStorageString);

        await page.evaluate((data) => {
            for (const [key, value] of Object.entries(data)) {
              sessionStorage[key] = value;
            }
          }, sessionStorage);
        await page.evaluate((data) => {
            for (const [key, value] of Object.entries(data)) {
              localStorage[key] = value;
            }
        }, sessionStorage);

    }

    savedSession=async function (page) {

        const cookies = JSON.stringify(await page.cookies());
        const sessionStorage = await page.evaluate(() =>JSON.stringify(sessionStorage));
        const localStorage = await page.evaluate(() => JSON.stringify(localStorage));
        // console.log('cookies',cookies);
        // console.log('sessionStorage',sessionStorage);
        // console.log('localStorage',localStorage);


        await writeFile(this.cookiesFilePath+"/cookiesString.json", cookies,function(err){
            if(err){
                console.log('err write',err);

            }
        });
        await writeFile(this.cookiesFilePath+"/sessionStorageString.json", sessionStorage,function(err){
            if(err){
                console.log('err write',err);

            }
        });
        await writeFile(this.cookiesFilePath+"/localStorageString.json", localStorage,function(err){
            if(err){
                console.log('err write',err);
            }
        });

        return true;

    }

    loadUrltoPDF=async function(url,user,dom_id,dom_html=null,path_file){
        var fr=path_file;
        var ext=path.extname(path_file);
       var file_path= await StorageEgine.put(fr.replace(ext,'.txt'),'redmee');
       if(!file_path){
        console.log('file path err',file_path);
        return false;
       }
        file_path=file_path.replace('.txt',ext);
        console.log('build pdf form-url',url,'--->',file_path);
        var page=await this.createrPage();
        var percobaan=null;
        try{

             percobaan=await this.evalLogined(url,0,page);

        }catch(err){
            
            console.log('percobaan login gagal --- ',err);
            return false;

        }

        console.log('logined',percobaan);

        if(percobaan){

            if(url.includes('?')){
                url+='&user='+user;
            }else{
                url+='?user='+user;
            }

            if(dom_html==null){
                await page.goto(url,{waitUntil: ['domcontentloaded','networkidle0']});
                this.delay(1000);
                var dom= await page.evaluate(function(dom_id){
                    if(document.querySelector(dom_id)){
                        if(app['activePrintMode']!=undefined){
                            return app.activePrintMode(dom_id);
    
                        }else{
    
                            return document.querySelector(dom_id).outerHTML;
                        }
    
                    }else{
                        return '';
                    }
                },dom_id);
                console.log('eval report ');
                console.log('URL=> ',url);
                console.log('SELECTOR=> ',dom_id);
                console.log('CONTENT=> ',dom);





            }else{
                console.log('FROM DOM ');
                dom=dom_html;
            }

            const pdfConfig = {
                path: file_path, // Saves pdf to disk.
                format: 'A4',
                printBackground: true,
                margin: { // Word's default A4 margins
                    top: '0cm',
                    bottom: '0cm',
                    left: '0cm',
                    right: '0cm'
                }
            };



            try{
                await page.setViewport({width:(1240-150),height:800-150});
                await page.setDefaultTimeout(120*1000);
                await page.setDefaultNavigationTimeout(120*1000);
                await page.goto(this.env.USER_HOST+'/print-canvas',{waitUntil: ['domcontentloaded','networkidle0']});
                var ev=await page.evaluate(function(dm){
                   if($('#content-canvas').html()!=undefined){
                            $('#content-canvas').html(dm);
                            $('.print_hidden').remove();
                            return $('html').html();
                   }else{
                    return false;
                   }

                },dom);
                console.log('set dom print');
                await this.delay(500);
                await page.emulateMediaType('print');
                var pdf = await page.pdf(pdfConfig);
                await page.close();

                return {
                    'file':pdf,
                    'path':file_path
                };

            }catch(err){
                console.log('print document gagal',err);
                page.close();
                return false;
            }

        }else{
            console.log('error');
        }


    }

    evalLogined=async function(path_tujuanx,percobaan=0,page){
        var logined=false;
         var langkah=percobaan;
        do{
          langkah+=1;
          var path_tujuan=this.env.USER_HOST+'/dash/2022/sistem-informasi';
          await page.goto(path_tujuan,{waitUntil: 'networkidle0'});

          var url= await page.evaluate(()=>{
              return window.location.href;
          });

          path_tujuan=path_tujuan.split('?')[0]||'xxxx';
          var url_new=url.split('?')[0]||'xxxx';

          if(url_new.includes(path_tujuan)){
              logined=true;
              console.log('login berhasil dan mencoba melakukan penyimpanan session');
              await this.savedSession(page);

          }else if(url.includes('/login')){
              console.log('gagal login pada percobaan ',langkah);
              console.log('mencoba melakukan login ulang');
              await page.type('[name="email"]',this.user.email||"");
              await page.type('[name="password"]',this.user.password||"");
              await page.click('button[type="submit"]')
              await page.waitForNavigation({waitUntil: 'networkidle2'});
          }

        }while(logined!=true && langkah<=4);

        return logined;

    }

    close=async function () {
        try{
            await this.browser.close();
        }catch(e){
            console.log('close browser error ',e);
        }finally{
            return true;
        }


    }



    buildPdf=async function(path,file_type){
        var page=await this.createrPage();

        var percobaan=null;
        try{
             percobaan=await this.evalLogined(path,0,page);
        }catch(err){
            console.log('percobaan login gagal --- ',err);
            page.close();
            return false;
        }

        if(percobaan){
            if(file_type=='pdf'){

                    const pdfConfig = {
                        path: 'url.pdf', // Saves pdf to disk.
                        format: 'A4',
                        printBackground: true,
                        margin: { // Word's default A4 margins
                            top: '0cm',
                            bottom: '0cm',
                            left: '0cm',
                            right: '0cm'
                        }
                    };
                try{
                    await page.setViewport({width:(1240-150),height:800-150});
                    await page.setDefaultTimeout(120*1000);
                    await page.setDefaultNavigationTimeout(120*1000);
                    await page.goto(path,{waitUntil: ['domcontentloaded','networkidle0']});
                    await this.delay(1000);
                    console.log('page emulated print');
                    await page.emulateMediaType('print');
                    var pdf = await page.pdf(pdfConfig);
                    await page.close();
                    return pdf;
                }catch(err){
                    console.log('print document gagal');
                    return false;
                }
            }else if(file_type=='excel'){
                var page=await this.createrPage();
                await page.goto(path,{waitUntil: ['domcontentloaded','networkidle0']});
                await this.delay(1000);
                var domhtmltable=await page.evaluate(function(){
                    var ObjectAkun=app.dataset;
                    var ObjectPemda=app.pemda;

                    var domtb=[];
                    ObjectAkun.forEach(async (el,k)=>{
                        if($('#dts_table_'+el.id).html()!=undefined){
                            domtb.push({
                                name:el.name,
                                id:el.id,
                                status_data:el.status_data,
                                pemda:ObjectPemda,
                                columns:el.columns,
                                dom_string:$('#dts_table_'+el.id).parent().html()
                            });
                        }
                    });



                    return domtb;
                });
                if(domhtmltable.length){
                var date=new Date();
                   var  workbook= XLSX.utils.book_new();
                   var sheMeta=domhtmltable.filter(el=>{
                        return el.status_data[0]!=null;
                   }).map(el=>{
                        return {
                            "Nama Dataset":el.name,
                            "Nama Pemda":el.pemda.nama_pemda,
                            "Status Data":el.status_data[1],
                            "Tanggal Pengambilan Data":date

                        }
                   })
                   var sheet = await XLSX.utils.json_to_sheet(sheMeta,{
                    'header':['Nama Dataset','Nama Pemda','Status Data','Tanggal Pengambilan Data'],
                    });
                    XLSX.utils.book_append_sheet(workbook, sheet,'Meta');
                    domhtmltable.forEach(async (el,k)=>{
                            var tableJson=HtmlTableToJson.parse(el.dom_string);
                            if(tableJson.results[0]!=undefined){
                                if(tableJson.results[0]!=undefined){
                                   if(tableJson.results[0].length){
                                        var options={header:el.columns.map(c=>{return (c.ag_title||c.name)+' ('+(c.ag_satuan||c.satuan)+')';})}
                                        var sheet = await XLSX.utils.json_to_sheet(tableJson.results[0],options);
                                        XLSX.utils.book_append_sheet(workbook, sheet, el.name.substring(0,31));
                                   }

                                }

                            }


                    });

                    return workbook;
                }else{
                    return false;
                }





            }
        } else{
            await page.close();
            return null;
        }

    }



}


module.exports={
    PDFEgine:PDFEgine
}
