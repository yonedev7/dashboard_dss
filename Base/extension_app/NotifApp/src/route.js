
const route={
    profile_pemda:'/session/[TAHUN]/mod-profile/build-env/[KODEPEMDA]',
    profile_bumdam:'/session/[TAHUN]/mod-mod/build-env/[KODEBUMDAM]',
    dts_content_public:'/session/[TAHUN]/mod-dataset/build-env/public/[ID_DATASET]',
    dts_content_admin:'/session/[TAHUN]/mod-dataset/build-env/admin/[ID_DATASET]',
    dts_rekap_admin:'/dash/[TAHUN]/sistem-informasi/module-dataset/build-env/[ID_DATASET]',
    dts_rekap_public:'/session/[TAHUN]/mod-dataset/build-env/rekap-public/[ID_DATASET]',
    dts_tugas:'/session/[TAHUN]/mod-dataset/build-env/tugas/[ID_USER]',
}



module.exports={
    route:route
}