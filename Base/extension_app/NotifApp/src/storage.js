const fs = require("fs");
const util = require('util');
const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);
const makeDir = util.promisify(fs.mkdir);
const dirExist= util.promisify(fs.exists);
const permitionFile=util.promisify(fs.chmod);
const path = require('node:path');
const { resolve } = require("path");

class Storage{
    root_path='';

    constructor(){
        this.root_path=path.resolve(process.env.APP_PATH,'storage');
    }

    getContent=async function (path_file) {
       try{
        var data=await  readFile(path_file);
        var ext=path.extname(path_file)||'.anu';
        switch(ext){
            case '.json':
            return JSON.parse(data);
            break;

            default:
            return (data);

            break;
        }

       }catch(e){
        return false;
       }

        
    }

    put=async function(path_file,file=''){
       var x= await this.makedir(path_file);
       if(x){
        try{
            var pr= path.join(this.root_path,path_file);
            await writeFile(pr,file);
            await fs.chmod(pr,511,function(err){
               if(err){
                console.log(err,'permission ',pr);
                return false;
               }else{
                console.log(pr,511,'done');

                return true;
               }
            });
            return pr;
        }catch(e){
            console.log(e);
            return false;
        }
       }else{
        return false;
       }
        

        
    }
    makedir=async function (path_file) {
        var sta_page=path.normalize(path.join(this.root_path,'/'+path_file));
        var path_on=sta_page.replaceAll(this.root_path,'').replaceAll('/{2+}','');

        var ext=path.extname(path_on)||'.anu';
      
        path_on=path_on.split('/');
       try{

        var path_ok=this.root_path;

        var path_dir=path_on.filter(e=>{
            return !e.includes(ext);
        });
        console.log('check path',path_dir.join('/'));

        if(!await dirExist(path.join(this.root_path,path_dir.join('/')))){
            await makeDir(path.join(this.root_path,path_dir.join('/')),{recursive:true});
        }
        for (var el of path_on) {
           
            if(!el.includes(ext)){
                if(el){
                    console.log('old path',path_ok);
                    path_ok=path.normalize(path_ok);
                    var ex=await dirExist(path_ok);
                    console.log('check dir',path_ok,' is ',ex,ext);
                    if(!(ex)){
                            console.log('make dir',path_ok);

                            await makeDir(path_ok);
                    }
                    path_ok+='/'+el;

                }
            }
          }
       
        return path_ok;
       }catch(e){
        return false;
       }
        
    }
}

module.exports={
    Storage:Storage
}