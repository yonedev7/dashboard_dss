const { Buttons,Client,MessageMedia,List } = require('whatsapp-web.js');
var moment = require('moment');
const fs = require("fs");
const { table } = require('console');

class User{
    Conn=null;
    constructor(conDB){
        this.Conn=conDB;
    }

    getDataset=async function(me,tahun) {
        console.log(me.pemissions);
           if(me.permissions.includes('*')){
            var dataset=(await this.Conn.query("select "+tahun+" as tahun_data,* from master.dataset as dts  ")).rows;

           }else{
            var dataset=(await this.Conn.query("select "+tahun+" as tahun_data,* from master.dataset as dts where id in ("+me.permissions.join(',')+")")).rows;
           }
            if(dataset.length){
                    dataset=dataset.filter(async (el,k)=>{
                            el['table_status']='dts_'+el.id;
                            el['tahun_data']=tahun;

                            if(el.table.includes('[TAHUN]')){
                                el.table=el.table.replaceAll('[TAHUN]',tahun);
                                el['table_status']='dts_'+el.id+'_'+tahun;
                            }

                            var exist_tb=(await this.Conn.query("select count(*) as c from pg_tables where tablename in ('"+el.table_status+"','"+el.table+"') and schemaname in ('dataset','status_data')")).rows[0]||{c:0};
                            if(parseInt(exist_tb.c)==2){
                                return true;
                            }else{
                                return false;
                            }
                    });

                    return {
                        dataset:dataset,
                        tahun_data:tahun
                    };
            }else{
                return {
                    dataset:[],
                    tahun_data:tahun
                };
            }

       
    }
    getPemda=async function (user) {
        if(user.roles.includes('TACT-REGIONAL')){
            var pemda=(await this.Conn.query("select mp.kodepemda from master.master_pemda as mp inner join master.pemda_lokus as lk on lk.kodepemda=mp.kodepemda where mp.regional_1='"+(user.regional||'xx')+"' order by mp.kodepemda asc")).rows;
           
        }else{
            console.log('all pemda');
            var pemda=(await this.Conn.query("select mp.kodepemda from master.master_pemda as mp inner join master.pemda_lokus as lk on lk.kodepemda=mp.kodepemda order by mp.kodepemda asc")).rows;
        }

        pemda=pemda.map(el=>{
            return el.kodepemda;
        });
        return pemda;
    }

    getBumdam=async function (user) {
        if(user.roles.includes('TACT-REGIONAL') || user.roles.includes('*')){
            var pemda=(await this.Conn.query("select mp.kodepemda from master.master_pemda as mp inner join master.pemda_lokus as lk on lk.kodepemda=mp.kodepemda where mp.regional_1='"+(user.regional||'xx')+"' order by mp.kodepemda asc")).rows;
           
        }else{
            var pemda=(await this.Conn.query("select mp.kodepemda from master.master_pemda as mp inner join master.pemda_lokus as lk on lk.kodepemda=mp.kodepemda order by mp.kodepemda asc")).rows;
        }

        pemda=pemda.map(el=>{
            return el.kodepemda;
        });
        return pemda;
    }

    getData=async function (number) {
        var C=(await this.Conn.query("select id,name,regional,email,phone_number from public.users where phone_number ilike '%"+(number.split('@')[0]||'')+"%'")).rows[0]||null;
        if(C){
            C['roles']=['Admin'];
            C['dataset']=[];
            
            if(C.phone_number){
                C.phone_number=JSON.parse(C.phone_number);
            }else{
                C.phone_number=[null,null,null];
            }

            C.number=number.split('@')[0]||null;
    
            var roles=(await this.Conn.query("select r.id,r.name from public.roles as r inner join model_has_roles as m on m.role_id=r.id  where m.model_id="+C.id)).rows;
            
            C.roles=roles.map(el=>{
                return el.name;
            });
            var roles_id=roles.map(el=>{
                return el.id;
            });


            if(roles_id.length){
                if(C.roles.includes('TACT-REGIONAL')){
                    C.roles.push('REGIONAL '+C.regional);
                }

                if(parseInt(C.id)!=1){
                    if(!C.roles.includes('Admin')){
                        var pemis_role=(await this.Conn.query("select p.name from permissions as p inner join public.role_has_permissions as rp on rp.permission_id=p.id where rp.role_id in ("+roles_id.join(',')+")")).rows;
                        var pemis_direct=(await this.Conn.query("select p.name from permissions as p inner join public.model_has_permissions as mp on mp.permission_id=p.id where mp.model_id = ("+C.id+")")).rows;
                        var permissions=pemis_role.concat(pemis_direct);
                            permissions=permissions.map(el=>{
                                return parseInt(((el.name||'').split(':')[0]||'').replaceAll('dts.',''));
                            });
                        permissions.sort();
                        C.permissions=permissions.filter((value, index, self)=>{
                            return self.indexOf(value) === index;
                        });

                    }else{
                        C.pemissions=['*'];
                    }

                }else{
                    C.permissions=['*'];
                }
                

            }else{
                if(C.roles.includes('Admin') || C.id==1){
                    C.roles=['*'];
                    C.dataset=[];
                    C.permissions=['*'];
                }else{
                    C.roles=[];
                    C.dataset=[];
                    C.permissions=['xxxx'];
                }
               
            }

        } 
        // console.log('have c',C);

        return C;
    }

}

module.exports={
    UserModel:User
}