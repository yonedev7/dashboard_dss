



console.log('=-----------');
var env=require('dotenv').config({
    path:__dirname+'/../../.env'
}).parsed;
process.env.APP_PATH=__dirname;
const path=require('path');
if(process.env.NODE_ENV==undefined){
    process.env.NODE_ENV=env.NODE_ENV||'production';
}

env['APP_PATH']=__dirname;
console.log(process.env.NODE_ENV);
console.log('ENV');
const { Client, LocalAuth,Buttons,MessageMedia,List } = require('whatsapp-web.js');
console.log('load wa module');
const { HandleAdmin } = require('./src/admin_data');
console.log('load admin module');

const { HandleDash } = require('./src/dash_data');
console.log('load dashboard module');

const { UserModel } = require('./src/user');
console.log('load user module');

const { route } = require('./src/route');
console.log('load rooting module');

const { team_number,team_group } = require('./src/contact').default;
console.log('load number module');

const { PDFEgine } = require('./src/pdf_egine');
const { Storage } = require('./src/storage');
const StorageEgine=new Storage();

const fs = require("fs");
const util = require('util');
const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);
const makeDir = util.promisify(fs.mkdir);
const dirExist= util.promisify(fs.exists);
const permitionFile=util.promisify(fs.chmod);

delay = async function (time) {
    return new Promise(function(resolve) { 
        setTimeout(resolve, time)
    });
}




String.prototype.replaceAll = function (target, payload) {
    let regex = new RegExp(target, 'g')
    return this.valueOf().replace(regex, payload)
};




// respond with "hello world" when a GET request is made to the homepage

function isCurrentUserRoot() {
    return process.getuid() == 0; // UID 0 is always root
 }




 
const pg= require('pg');


var qrcode = require('qrcode-terminal');


  

var client;
let wa_tahun_data=[];
var localEnvTahun='./storage/env_tahun.json';

var PDF=null;

async function sendHealty(){
   
        console.log('----------',state);
    
}

async function exitHandler(options,exitCode) {
   
    if (options.cleanup){
        if(options.exit){
            if(client){
                await client.pupBrowser.close();
            }
            await  PDF.close();
        }

    } 
    if (exitCode || exitCode === 0) {
        console.log(exitCode)
    }

    if (options.exit) {
        console.log('exit please');
        process.exit(0);
    };
}

//do something when app is closing
// process.stdin.resume();

// process.on('exit',  exitHandler.bind(null,{cleanup:true}));

// //catches ctrl+c event
// process.on('SIGINT',  exitHandler.bind(null, {exit:true,cleanup:true}));

// // catches "kill pid" (for example: nodemon restart)
// process.on('SIGUSR1',  exitHandler.bind(null, {exit:true,cleanup:true}));
// process.on('SIGUSR2',  exitHandler.bind(null, {exit:true,cleanup:true}));

// //catches uncaught exceptions
// process.on('uncaughtException',  exitHandler.bind(null, {exit:true,cleanup:true}));


const database=new pg.Client({
    user: env.DB_USERNAME,
    host: env.DB_HOST,
    database: env.DB_DATABASE||'NUWSP2',
    password: env.DB_PASSWORD||'Uba2013?',
    port: env.DB_PORT||5432,
    }
);

console.log('build schema connection database');


var kteam_utama=[];
team_number.forEach((el,key)=>{
    if(el.utama==true){
    kteam_utama.push(key);

    }
});


var state={
    'puppet':null,
    'ready':false,
    'auth_qr':null,
    'state':null,
    'battery_info':{
        battery:0,
        plugged:false
    }
}


var list_group=[
    '120363043191967264@g.us'
];

function notGroup(number){
    if(number.includes('@g.us')){
        return list_group.includes(number);
    }else{
        return true;
    }
}
async function savingEnvTahun(data){
    var path='tahun_data.json';
    await StorageEgine.put(path,JSON.stringify(data));
    
}

async function getEnvTahun(){
    var data=await StorageEgine.getContent('tahun_data.json');
    if(data){
        return data;
    }else{
        return [];
    }
}
console.log('load function module set tahun');
var config_puput=
    {
        headless: true,
        args:[
        '--no-sandbox',
        '--disable-setuid-sandbox',
        '--disable-dev-shm-usage',
        '--disable-accelerated-2d-canvas',
        '--no-first-run',
        '--no-zygote',
        '--disable-gpu'
        ]
    };

    if((process.env.NODE_ENV||'')=='development'){
         config_puput=
        {
            headless: true,
           
        };
    }

async function init(){
    // await sendHealty();
    wa_tahun_data=await getEnvTahun();
    console.log(wa_tahun_data,'load env tahun');
    await database.connect();
    console.log('database connection');
    var Sender=new UserModel(database);
    if(!client){
        client=new Client({
            authStrategy: new LocalAuth(),
            puppeteer:config_puput
        });

    }
    // console.log('User Model initial');

   

    client.on('qr', (qr) => {
        console.log('QR RECEIVED', qr);
        qrcode.generate(qr,{small: true});
        state.ready=false;
        state.auth_qr=qr;
         sendHealty();
    });

    client.on('authenticated', async () => {    
            state.ready=false;
            state.auth_qr=null;
          
         sendHealty();

    });

    client.on('change_state', async (state) => {    
        state.state=state;
       
        sendHealty();

    });

    client.on('auth_failure', async (state) => {    
        state.state='Auth Fails';
        state.ready=false;
        state.auth_qr=null;
        fs.rmSync('./.wwebjs_auth/', { recursive: true, force: true })
        
        sendHealty();


        
    });

    client.on('change_battery',async (batteryInfo)=>{
        state.battery_info=batteryInfo;
        sendHealty();

    });

    client.on('disconnected',async ()=>{
        state.auth_qr=null;
        state.ready=false;
        state.state=null;
        sendHealty();
    });
    
    client.on('ready', () => {
        var pup=null;
        while(pup==null){
            if(client.pupBrowser){
                pup=client.pupBrowser;
                console.log('pup exist build egine pdf');
                PDF=new PDFEgine(client.pupBrowser,{email:env.USER_EMAIL,password:env.USER_PASSWORD},env);
            }
        }
        state.ready=true;
        state.auth_qr=null;
        state.state=null;
        console.log('client ready');
        sendHealty();

    });
    
    client.on('message', async (msg) => {
        sendHealty();


        try{
            var me=await Sender.getData(msg.from.trim());
        // if(await msg.acceptGroupV4Invite()){
        //     msg.reply("Hallo Saya DSS BOT\n Siap Membantu anda dalam penyediaan data air minum :)");
        // }
        if(me && (!msg.isForwarded) && notGroup('msg.from')){
            console.log('me');
            var tahun_data=wa_tahun_data[me.from]!=undefined?wa_tahun_data[me.from]:parseInt((new Date()).getFullYear());

            var params=msg.body.split(' ').slice(0,3);

            if((params[0]||'').includes('!set-tahun')){
                if(!isNaN(parseInt(params[1]||'xxx'))){
                    wa_tahun_data[msg.from]=parseInt(params[1]);
                    await savingEnvTahun(wa_tahun_data);
                    msg.reply('Tahun Data ('+me.name+') Telah Dirubah Pada Time Frame '+wa_tahun_data[msg.from]);
                }
            }else if((params[0]||'').includes('!help')){
                var body=msg.body.replaceAll('!help','').trim();
                team_number.forEach(async (el,k)=>{
                    var message="Hallo "+el.name+"\n"+me.name+" (+"+(me.number.split('@')[0]||'')+") ("+me.roles.join(' - ')+"), meminta bantuan!";

                    if(me.regional && el.number!=me.number){
                        if(kteam_utama.includes(k)){
                            await client.sendMessage(el.number+'@c.us',message);
                            await msg.forward(el.number+'@c.us');
                        }
                    }
                    else if((el.number!=me.number && me.roles.includes('TACT-REGIONAL')) && (el.regional.includes(me.regional||"X"))){
                        await client.sendMessage(el.number+'@c.us',message);
                        await msg.forward(el.number+'@c.us');
                    }else{
                        if(kteam_utama.includes(k)){
                            await client.sendMessage(el.number+'@c.us',message);
                            await msg.forward(el.number+'@c.us');
                        }

                    }
                    
                });

                team_group.forEach(async (el)=>{
                    var message="Hallo "+el.name+"\n"+me.name+" (+"+(me.number.split('@')[0]||'')+") ("+me.roles.join(' - ')+"), meminta bantuan!";

                    if(true){
                        await client.sendMessage(el.number+'@g.us',message);
                        await msg.forward(el.number+'@g.us');
                    }
                    
                });

                msg.reply("OK "+me.name+", Team DSS akan Segera Mengubungi Anda!");

            }else if((params[0]||'').includes('!team-pusat')){
                const TeamList = new List(
                    "Hubungi Team DSS",
                    "Lihat Kontak",
                    [
                      {
                        title: "Kontak  Team",
                        rows: team_number.filter((el,k)=>{
                            if(me.regional==null){
                                if(kteam_utama.includes(k)){
                                    return true;
                                }
                            }else if((el.number!=me.number && me.roles.includes('TACT-REGIONAL')) && (el.regional.includes(me.regional||'X'))){
                                return true;
                            }else if(me.roles.length){
                                if(kteam_utama.includes(k)){
                                    return true;
                                }
                            }
                            else{
                                return false;
                            }
                        }).map(el=>{    
                            return {id:'team.'+el.number,title:el.name};
                        })
                      },
                      
                    ],
                    "Mohon Pilih Kontak"
                  );

                  msg.reply(TeamList);

            }
            else if(msg.type=='list_response'){
                if(msg.selectedRowId.includes('team.')){
                    var number=msg.selectedRowId.replaceAll('team.','');
                    console.log('nomer di tuju',number);
                    var team=team_number.filter(el=>{
                        return el.number==number;
                    });
                    console.log(team,'team')
                    if(team.length){
                        msg.reply(""+team[0].name+" - klik +"+team[0].number+" Untuk Menghubungi");
                    }
                }else if(msg.selectedRowId.includes('.rekap-pemda') && msg.selectedRowId.includes('admin.')){
                    var kodepemda=msg.selectedRowId.split('.');
                    var tahun=null;
                    kodepemda.forEach(el=>{
                        if(el.includes('_')){
                            console.log(el,'ss');
                            tahun=el.split('_')[1];
                            
                        }
                    });

                    file_type="pdf";
                    kodepemda=(kodepemda[kodepemda.length-1]||'');
                    console.log('kodepemda dapetnya',kodepemda);
                    if(kodepemda.includes('0ft0')){
                        file_type="excel";
                        kodepemda=kodepemda.replaceAll('0ft0','');
                        console.log(kodepemda,'kodepemnda');
                    }
                    var a= (await Sender.getDataset(me,tahun_data));
                    if(a.dataset.length){
                        msg.reply("Mohon Menunggu....\nPembuatan File Membutuhkan Waktu Beberapa Menit, Jika Dalam 15 Menit Tidak Mendapatkan Report Silahkan Hubungi Penyedia Layanan");

                    }else{
                        msg.reply("Anda Tidak Memiliki Role Untuk Melakukan Perintah Ini");
                    }
                    me.dataset=a.dataset;
                    var Egine=new HandleAdmin(database,me,PDF,env);
                    Egine.getRekapPemda(msg,tahun,kodepemda,file_type);
                    
                }
                
                // await Egine.listResponse(msg.selectedRowId,msg);
        
            }else if(((params[0]||'').includes('/'))){
                switch((params[0]||'')){
                    case '/list-tugas':
                        if((params[1]||'')=='help'){
                            msg.reply("Format Pesan \n"+'1. '+"\/list-tugas : Memberikan List Tugas Pada Semua Dataset  \n"+"2. \/list-tugas pilih-dataset : Memberikan Tugas List Tugas Pada Dataset Tertentu");
                        }else{
                        
                        }
                    break;
                    case '/admin-data':
                        console.log('params',params,'me',me);
                    if((params[1]||'')=='help'){
                        msg.reply("Format Pesan \n"+'1. '+"\/admin-data : Memberikan Status Keterisian Pada Dataset Pilihan \n"+"2. \/admin-data build-excel : Mencetak Status keterisian Dalam Format Excel Pada Pilihan Dataset");
                    }else{
                        var def=false;
                        
                        var Egine=new HandleAdmin(database,me,PDF,env);

                        switch((params[1]||'')){
                            case 'rekap':
                                return await Egine.Rekap(msg,null,(params[2]||null));
                            break;
                            case 'rekap-pemda':
                                var kodepemdas= await Sender.getPemda(me);
                                if(kodepemdas.length){
                                    var pemda=(await database.query("select * from master.master_pemda as mp where kodepemda in ('"+(kodepemdas.join("','"))+"') order by mp.regional_1 asc, mp.nama_pemda asc")).rows;
                                    var list={};
                                    var surfix='';
                                    if((params[2]||'').toLocaleLowerCase()=='excel'){
                                        surfix='0ft0';
                                    }
                                    pemda.forEach(el=>{
                                        var t=el.regional_2||'Diluar Regional';
                                        var jk=el.regional_1||'Lainya';
                                        if(list[jk]==undefined){
                                            list[jk]={
                                                title:'Regional '+jk,
                                                rows:[
                                                    {
                                                        title:el.nama_pemda,
                                                        id:'admin.dts.*_'+tahun_data+'.rekap-pemda.'+el.kodepemda+surfix
                                                    }
                                                ]
                                            }
                                        }else{
                                            if(list[jk]['rows']!=undefined){
                                                list[jk].rows.push({
                                                    title:el.nama_pemda,
                                                    id:'admin.dts.*_'+tahun_data+'.rekap-pemda.'+el.kodepemda+surfix
                                                });
                                            }else{

                                                console.log('rows no detect',jk,list);
                                            }
                                          
                                        }
                                    });

                                    console.log('listnya',Object.keys(list));

    
                                    const l_pemda = new List(
                                        "Pilih Rekap PEMDA Tahun "+tahun_data,
                                        "Pilihan Sesuai Role",
                                        Object.values(list),
                                        "Pilih Rekap PEMDA"
                                      );
    
                                      msg.reply(l_pemda);
                                }else{
                                    msg.reply('Tidak Terdapat Pemda Yang Dapat Dipilih');
                                }     
                               

                            break;
                            default:
                                var a= (await Sender.getDataset(me,tahun_data));
                                me.dataset=a.dataset;

                               var dataset_list_utama=(me.dataset||[]).filter(el=>{
                                    return el.admin_menu==0;
                                }).map(el=>{
                                    return {
                                        id:("admin.dts."+el.id+"_"+tahun_data+".rekap"),
                                        title:(el.name+' Tahun '+tahun_data)
                                    }
                                });
                                
    
                               var dataset_list_pendukung=(me.dataset||[]).filter(el=>{
                                    return el.admin_menu!=0;
                                }).map(el=>{
                                    
                                    return {
                                        id:"admin.dts."+el.id+"_"+tahun_data+".rekap",
                                        title:el.name+' Tahun '+tahun_data+''
                                    }
                                });
    
                               var  list_message = new List(
                                    "Pilih Dataset Untuk Melihat Data Tahun "+tahun_data,
                                    "Pilihan Sesuai Role",
                                    [
                                      {
                                        title: "Dataset Utama",
                                        rows: dataset_list_utama
                                      },
                                      {
                                        title: "Dataset Pendukung",
                                        rows: dataset_list_pendukung
                                      }
                                      
                                    ],
                                    "Pilih Dataset Rekap"
                                  );
                                  msg.reply(list_message);
                                // pilih dataset rekap admin.dts.[ID]_[TAHUN].rekap
                            break;
                            
                        }
                        return true;

                    }

                       
                    break;
                    case '/dash-data':
                    if((params[1]||'')=='help'){
                    
                        msg.reply("Format Pesan \n"+'1. '+"\/dash-data : Memberikan Status Keterisian Data Pemda Yang Telah Tevalidasi Pada Dataset Pilihan \n"+"2. \/dash-data profile-pemda : Mencetak Profile Pemda  Dalam Format PDF Pada Pilihan Pemda\n"+"3. \/dash-data profile-bumdam : Mencetak Profile Bumdam  Dalam Format PDF Pada Pilihan Bumdam\n"+"4. \/dash-data laporan-monev : Mencetak Laporan Ketercapaian KPI  Dalam Format PDF Pada Pilihan KPI\n");
                    
                    }else{
                        switch((params[1]||'')){
                            case 'profile-pemda':
                                var kodepemdas= await Sender.getPemda(me);

                                var pemda=(await database.query("select * from master.master_pemda as mp where in ('"+kodepemdas.join(',')+"') order by mp.kodepemda asc")).rows;
                                var list={};
                                var surfix='';
                                if((params[2]||'')=='excel'){
                                    surfix='_excel';
                                }

                                pemda.forEach(el=>{
                                    if(el.kodepemda.length<5){
                                        list[el.kodepemda]={
                                            title:el.nama_pemda,
                                            rows:[
                                                {
                                                    title:el.nama_pemda,
                                                    id:'dash.dts.*_'+tahun_data+'.profile-pemda.'+el.kodepemda+surfix
                                                }
                                            ]
                                        }
                                    }else{
                                        list[el.kodepemda].rows.push({
                                            title:el.nama_pemda,
                                            id:'dash.dts.*_'+tahun_data+'.profile-pemda.'+el.kodepemda+surfix
                                        });
                                    }
                                });

                                const l_pemda = new List(
                                    "Pilih Profile PEMDA Tahun "+tahun_data,
                                    "Pilihan Sesuai Role",
                                    Object.values(list),
                                    "Pilih Profile PEMDA"
                                  );

                                  msg.reply(l_pemda);
                                // pilih dataset  list-pemda-profile rekap dash.dts.[ID].profile-bumdam.[KODEPEMDA]
    
                            break;
                            case 'profil-bumdam':
                                // pilih dataset list-bumdam-profile dash.dts.[ID].profile-bumdam.[KODEBUMDAM]
    
                            break;
    
                            case 'laporan-monev':
                               var dataset_list_utama=me.dataset.filter(el=>{
                                    return el.admin_menu==0;
                                }).map(el=>{
                                    return {
                                        id:("dash.dts."+el.id+"_"+tahun_data+".rekap"),
                                        title:(el.name+' Tahun '+tahun_data)
                                    }
                                });
                               var dataset_list_pendukung=me.dataset.filter(el=>{
                                    return el.admin_menu!=0;
                                }).map(el=>{
                                    
                                    return {
                                        id:"dash.dts."+el.id+"_"+tahun_data+".rekap",
                                        title:el.name+' Tahun '+tahun_data+''
                                    }
                                });
                               var  list_message = new List(
                                    "Pilih Dataset Untuk Melihat Data Tahun "+tahun_data,
                                    "Pilihan Sesuai Role",
                                    [
                                      {
                                        title: "Dataset Utama",
                                        rows: dataset_list_utama
                                      },
                                      {
                                        title: "Dataset Pendukung",
                                        rows: dataset_list_pendukung
                                      }
                                      
                                    ],
                                    "Pilih Dataset Rekap"
                                  );
                                  msg.reply(list_message);
                                // pilih dataset kpi dash.dts.[ID].laporan-monev
    
                            break;
                            case 'rekap':
                                var Egine=new HandleDash(database,me,PDF);

                                // pilih dataset kpi dash.dts.[ID].laporan-monev
    
                            break;
                            default: 
                               var dataset_list_utama=me.dataset.filter(el=>{
                                    return el.admin_menu==0;
                                }).map(el=>{
                                    return {
                                        id:("dash.dts."+el.id+"_"+tahun_data+".rekap"),
                                        title:(el.name+' Tahun '+tahun_data)
                                    }
                                });
                               var dataset_list_pendukung=me.dataset.filter(el=>{
                                    return el.admin_menu!=0;
                                }).map(el=>{
                                    
                                    return {
                                        id:"dash.dts."+el.id+"_"+tahun_data+".rekap",
                                        title:el.name+' Tahun '+tahun_data+''
                                    }
                                });
                               var  list_message = new List(
                                    "Pilih Dataset Untuk Melihat Data Tahun "+tahun_data,
                                    "Pilihan Sesuai Role",
                                    [
                                      {
                                        title: "Dataset Utama",
                                        rows: dataset_list_utama
                                      },
                                      {
                                        title: "Dataset Pendukung",
                                        rows: dataset_list_pendukung
                                      }
                                      
                                    ],
                                    "Pilih Dataset Rekap"
                                  );
                                  msg.reply(list_message);
                                // pilih dataset rekap dash.dts.[ID].rekap
    
                            break;
                        }
                    }
                    break;
                   
                }

              
    
            }else{
                switch((msg.body||'').toLowerCase()){
                    case "!ping":
                        msg.reply("Hallo "+me.name+" ("+me.roles.join(' - ')+") Ada Yang Bisa Kami Bantu?");
                    break;
                    
                    
                }
            }
            }else{
                if(msg.from.includes('@g.us')){
                    // msg.reply('DSS-BOT Izin Bergabung! :)');
                }else{
                    msg.reply('Nomer Anda ('+(msg.from.split('@')[0]||'')+') Belum Terdaftar!');
                }
            }
        }catch(e){
            console.log(e,'client err');
            
        }
       if(msg.id.fromMe==false){
        await client.sendSeen(msg.id.id);
       } 


    });
    console.log('add event listener for browser whatsapp');
    try{
        console.log('initials browser to start all egine');
        var egine_count=30;

        
       var init= await client.initialize();
        
    }catch(err){
        if(client.pupBrowser){
            await client.pupBrowser.close();
        }
        console.log('whatapp errooor','initialized reload');
        


    }finally{

    }


}




SaveDoc=async function (target,name,PDF) {
    var path_x=__dirname;
    var tg=target.replaceAll(__dirname+'/..','').split('/');
    for(var i =0;i<tg.length;i++){
        if(tg[i]){
           if(!tg[i].includes('.')){
                path_x+='/'+tg[i];
                if(!(await dirExist(path_x))){

                     await makeDir(path_x);
                }
           }
        }
    };
    
    await writeFile(target+name,PDF);
    return true;
    
}

    const express = require('express')
    console.log('load module express');

    const app = express();
    var http = require('http');
    console.log('load module http');

    const bodyParser = require('body-parser');
    app.use(bodyParser.json())

    var urlencodedParser = bodyParser.urlencoded({ extended: false });

    app.post('/send-wa/',urlencodedParser,async (req,res)=>{
       if(state.ready){
            if(req.body.type=='from-url'){  
                console.log('get-data');
                var check_file=await dirExist(path.join(process.env.APP_PATH,'storage',req.body.name_file));
                if(check_file){
                    try{
                        var media = MessageMedia.fromFilePath(path.join(process.env.APP_PATH,'storage',req.body.name_file));
                        if(media){
                            await client.sendMessage(req.body.number,media);
                        }
                        res.json({code:200,data:{
                            part_file:__dirname+'/'+req.body.name_file
                        }});
                   }catch(e){
                        res.json({code:500,data:{
                            err:e,
                            part_file:__dirname+'/'+req.body.name_file
                        }});
                    }
                
                   

                }else{
                var file=await  PDF.loadUrltoPDF(req.body.url,req.body.user,req.body.dom_selected,req.body.dom_html,req.body.name_file);
                
                if(file){
                
                    try{
                        console.log('builder file done----',file.path,req.body.number);
                        await delay(1000);
                        var media = MessageMedia.fromFilePath(file.path,{filename:'Rekap DSS.pdf'});
                        if(media){
                            await client.sendMessage(req.body.number,media);
                        }
                        res.json({code:200,data:{
                            part_file:__dirname+'/'+req.body.name_file
                        }});
                        return true;
                    }catch(e){
                        res.json({code:500,data:{
                            err:e,
                            part_file:__dirname+'/'+req.body.name_file
                        }});
                    }
                        
                }else{
                    res.json({code:500,data:[]}); 
                }
            }
              

            }else{
                res.json({code:500,data:{
                    part_file:__dirname+'/'+req.body.name_file
                }});
            }
           

       }else{
            res.json({code:500,data:[]});
       }
    });
    app.post('/healty',async (req,res)=>{
        res.send(JSON.stringify(state));
    });

    app.post('/test',async (req,res)=>{
        console.log('get request test',req.body);
        res.send('aa');
    });

    var port= process.env.PORT || 3110;
    console.log('express server up', 'try set port ',port);
    app.set('port',port);
    http.createServer(app).listen(app.get('port'), async function(){
        console.log("Express server listening on port ", app.get('port'));
        console.log('up server');
        await init();

    });



// console.log('test',Egine.listResponse('dts.120',{from:'6287771079782',selectedRowId:'dts.120'}));


