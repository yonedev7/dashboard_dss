/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components_final/showcase_dash.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components_final/showcase_dash.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  props: {
    link: String,
    isAggre: {
      type: Boolean,
      "default": true
    },
    options: {
      type: Object,
      "default": function _default() {
        return {
          scope: ''
        };
      }
    },
    statusData: Number
  },
  data: function data() {
    return {
      varian_filter: [{
        key: 'scope',
        value: '',
        label: 'Nasional'
      }, {
        key: 'scope',
        value: 'LONGLIST',
        label: 'Longlist'
      }, {
        key: 'scope',
        value: 'SORTLIST',
        label: 'Sortlist'
      }, {
        key: 'regional',
        value: 'I',
        label: 'Regional I'
      }, {
        key: 'regional',
        value: 'II',
        label: 'Regional II'
      }, {
        key: 'regional',
        value: 'III',
        label: 'Regional III'
      }, {
        key: 'regional',
        value: 'IV',
        label: 'Regional IV'
      }],
      title: '',
      subtitle: '',
      charts: [],
      deviasi: 0,
      capaian: {
        value: 0,
        tahun: 0,
        jumlah_pemda: 0,
        jumlah_pemda_kabkot: 0,
        jumlah_pemda_provinsi: 0
      },
      options_final: {
        scope: ''
      },
      satuan_deviasi: 'Rp.',
      satuan_deviasi_pos: 'prefix',
      deviasi_percentage: 0
    };
  },
  mounted: function mounted() {
    if (this.options) {
      this.options_final = this.options;
    }

    this.loadData();
  },
  methods: {
    isClikedScope: function isClikedScope(x) {
      if (this.options_final[x.key] != undefined) {
        return this.options_final[x.key] == x.value;
      } else {
        return false;
      }
    },
    numberFormat: function numberFormat(num) {
      return window.NumberFormat(parseInt(num));
    },
    addScope: function addScope(x) {
      this.options_final = {};
      this.options_final[x.key] = x.value;
      this.loadData();
    },
    loadData: function loadData() {
      var self = this;
      this.$isLoading(true);
      var dtsr = this.options_final;

      if (this.statusData) {
        dtsr.status_data = this.statusData;
      }

      req_ajax.post(this.link, dtsr).then(function (res) {
        self.deviasi = res.data.dev;
        self.title = res.data.title;
        self.subtitle = res.data.subtitle;
        self.capaian = res.data.capaian;
        self.deviasi_percentage = res.data.dev_percentage;
        self.charts = res.data.charts;
      })["finally"](function () {
        self.$isLoading(false);
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components_final/showcase_dash.vue?vue&type=template&id=4f6c2e60&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components_final/showcase_dash.vue?vue&type=template&id=4f6c2e60& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function render() {
  var _vm = this,
      _c = _vm._self._c;

  return _c("div", {
    staticClass: "d-flex flex-column bg-white w-100 rounded mt-2"
  }, [_c("h4", {
    staticClass: "bg-secondary p-2 rounded"
  }, [_vm._v(_vm._s(_vm.title))]), _vm._v(" "), _vm.isAggre ? _c("div", {
    staticClass: "btn-group W-100 self-align-center"
  }, _vm._l(_vm.varian_filter, function (btn, i) {
    return _c("button", {
      key: i + "btn",
      "class": "btn " + (_vm.isClikedScope(btn) ? "btn-warning" : "btn-primary"),
      on: {
        click: function click($event) {
          return _vm.addScope(btn);
        }
      }
    }, [_vm._v(_vm._s(btn.label))]);
  }), 0) : _vm._e(), _vm._v(" "), _c("div", {
    staticClass: "d-flex w-100 border-bottom"
  }, [_c("div", {
    staticClass: "p-2 text-left mr-1 flex-grow-1"
  }, [_c("h4", {}, [_vm._v("Capaian")]), _vm._v(" "), _c("p", {
    staticClass: "mb-0"
  }, [_vm.satuan_deviasi_pos == "prefix" ? _c("small", [_vm._v(_vm._s(_vm.satuan_deviasi))]) : _vm._e(), _vm._v("  " + _vm._s(_vm.numberFormat(_vm.capaian.value)) + " "), _vm.satuan_deviasi_pos == "surfix" ? _c("small", [_vm._v(_vm._s(_vm.satuan_deviasi))]) : _vm._e()]), _vm._v(" "), _c("small", {}, [_vm._v("Tahun " + _vm._s(_vm.capaian.tahun) + " (" + _vm._s(_vm.capaian.jumlah_pemda) + " Pemda - " + _vm._s(_vm.capaian.jumlah_pemda_kabkot) + " Kab/Kota, " + _vm._s(_vm.capaian.jumlah_pemda_provinsi) + " Provinsi )")])]), _vm._v(" "), _c("div", {
    staticClass: "bg-white p-2 text-right"
  }, [_c("h4", {}, [_c("small", [_vm._v("Persentase Peningkatan")]), _c("b", [_vm._v(" " + _vm._s(_vm.deviasi_percentage) + "%")])]), _vm._v(" "), _c("p", {}, [_vm._v("Deviasi "), _vm.satuan_deviasi_pos == "prefix" ? _c("small", [_vm._v(_vm._s(_vm.satuan_deviasi))]) : _vm._e(), _vm._v("  " + _vm._s(_vm.numberFormat(_vm.deviasi)) + " "), _vm.satuan_deviasi_pos == "surfix" ? _c("small", [_vm._v(_vm._s(_vm.satuan_deviasi))]) : _vm._e()])])]), _vm._v(" "), _vm._l(_vm.charts, function (item, i) {
    return _c("div", {
      key: i
    }, [_c("charts", {
      attrs: {
        options: item
      }
    })], 1);
  })], 2);
};

var staticRenderFns = [];
render._withStripped = true;


/***/ }),

/***/ "./resources/js/components_final/showcase_dash.vue":
/*!*********************************************************!*\
  !*** ./resources/js/components_final/showcase_dash.vue ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _showcase_dash_vue_vue_type_template_id_4f6c2e60___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./showcase_dash.vue?vue&type=template&id=4f6c2e60& */ "./resources/js/components_final/showcase_dash.vue?vue&type=template&id=4f6c2e60&");
/* harmony import */ var _showcase_dash_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./showcase_dash.vue?vue&type=script&lang=js& */ "./resources/js/components_final/showcase_dash.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _showcase_dash_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _showcase_dash_vue_vue_type_template_id_4f6c2e60___WEBPACK_IMPORTED_MODULE_0__.render,
  _showcase_dash_vue_vue_type_template_id_4f6c2e60___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components_final/showcase_dash.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components_final/showcase_dash.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components_final/showcase_dash.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_showcase_dash_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./showcase_dash.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components_final/showcase_dash.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_showcase_dash_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components_final/showcase_dash.vue?vue&type=template&id=4f6c2e60&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components_final/showcase_dash.vue?vue&type=template&id=4f6c2e60& ***!
  \****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_showcase_dash_vue_vue_type_template_id_4f6c2e60___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_showcase_dash_vue_vue_type_template_id_4f6c2e60___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_showcase_dash_vue_vue_type_template_id_4f6c2e60___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./showcase_dash.vue?vue&type=template&id=4f6c2e60& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components_final/showcase_dash.vue?vue&type=template&id=4f6c2e60&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ normalizeComponent)
/* harmony export */ });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent(
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */,
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options =
    typeof scriptExports === 'function' ? scriptExports.options : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) {
    // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () {
          injectStyles.call(
            this,
            (options.functional ? this.parent : this).$root.$options.shadowRoot
          )
        }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functional component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection(h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing ? [].concat(existing, hook) : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
/*!***********************************!*\
  !*** ./resources/js/dss-final.js ***!
  \***********************************/
window.v_com_dts_showcase_dash = (__webpack_require__(/*! ./components_final/showcase_dash.vue */ "./resources/js/components_final/showcase_dash.vue")["default"]);
})();

/******/ })()
;