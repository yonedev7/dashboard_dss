/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
 window.l_ = require('lodash');
require('./bootstrap');
//  window.SockFirebuilder=require('./firebase').default.SockFire;


window.Vue = require('vue').default;
// import VueExcelEditor from 'vue-excel-editor';
// Vue.use(VueExcelEditor)

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

// import BEditableTable from 'bootstrap-vue-editable-table';
// Vue.component('BEditableTable',BEditableTable);
import VueTextareaAutosize from 'vue-textarea-autosize'
Vue.use(VueTextareaAutosize)

import vSelect from "vue-select";
import VueAppend from 'vue-append'
Vue.use(VueAppend)
window.Highcharts = require('highcharts')
require('./chart_them');
window.moment = require('moment');
import VueNumeric from 'vue-numeric'

Vue.use(VueNumeric)

import InputMask from 'vue-input-mask';

Vue.component('v-number-i',VueNumeric)
Vue.component('input-mask', InputMask)
// import HighchartOfflineExport from 'highcharts/modules/offline-exporting'

import Gantt from "highcharts/modules/gantt";
import HighchartsVue from 'highcharts-vue'
import exportingInit from 'highcharts/modules/exporting'
import HighchartsMapModule from 'highcharts/modules/map'
import stockInit from 'highcharts/modules/stock'
import HighchartMakeCluster from 'highcharts/modules/marker-clusters'
import HighExport from 'highcharts/modules/export-data'
require('highcharts/highcharts-more')(Highcharts);

// import FlowyPlugin from "@hipsjs/flowy-vue";
import {ind,ind_kab} from './mapgeo.js';
exportingInit(Highcharts)
HighchartsMapModule(Highcharts)
stockInit(Highcharts)
Gantt(Highcharts);
HighExport(Highcharts);
HighchartMakeCluster(Highcharts);
// HighchartOfflineExport(HighchartOfflineExport);
window.html2canvas = require('html2canvas');


Highcharts.maps['ind']=ind;
Highcharts.maps['ind_kab']=ind_kab;

Vue.use(HighchartsVue, {tagName: 'charts'});
import DataTable from 'laravel-vue-datatable';
Vue.use(DataTable);
// import { VuejsDatatableFactory } from 'vuejs-datatable';


// Vue.use( VuejsDatatableFactory );
window.yone_XLSX = require("xlsx");
import { Readable } from 'stream';
yone_XLSX.stream.set_readable(Readable)
// import Sortable fromno 'vue-sortable'
// Vue.use(Sortable)
// Vue.use(FlowyPlugin);
Vue.component("v-select", vSelect);
import draggable from 'vuedraggable';
Vue.component('draggable',draggable);
import TextareaAutosize from 'vue-textarea-autosize'
 
Vue.use(TextareaAutosize)
window.DeltaToHtml = require('quill-delta-to-html').QuillDeltaToHtmlConverter

import loading from 'vuejs-loading-screen'

Vue.use(loading,{'tagName':'v-loading'});

// window.quillTable = require('quill-table');


//  window.QuillBetterTable=require('quill-better-table');


 import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
//  import Mention from '@ckeditor/ckeditor5-mention/src/mention';
//  import Essentials from '@ckeditor/ckeditor5-essentials/src/essentials';
//  import UploadAdapter from '@ckeditor/ckeditor5-adapter-ckfinder/src/uploadadapter';
//  import Autoformat from '@ckeditor/ckeditor5-autoformat/src/autoformat';
//  import Bold from '@ckeditor/ckeditor5-basic-styles/src/bold';
//  import Italic from '@ckeditor/ckeditor5-basic-styles/src/italic';
//  import BlockQuote from '@ckeditor/ckeditor5-block-quote/src/blockquote';
//  import EasyImage from '@ckeditor/ckeditor5-easy-image/src/easyimage';
//  import Heading from '@ckeditor/ckeditor5-heading/src/heading';
//  import Image from '@ckeditor/ckeditor5-image/src/image';
//  import ImageCaption from '@ckeditor/ckeditor5-image/src/imagecaption';
//  import ImageStyle from '@ckeditor/ckeditor5-image/src/imagestyle';
//  import ImageToolbar from '@ckeditor/ckeditor5-image/src/imagetoolbar';
//  import ImageUpload from '@ckeditor/ckeditor5-image/src/imageupload';
//  import Link from '@ckeditor/ckeditor5-link/src/link';
//  import List from '@ckeditor/ckeditor5-list/src/list';
//  import Paragraph from '@ckeditor/ckeditor5-paragraph/src/paragraph';
//  import Alignment from '@ckeditor/ckeditor5-alignment/src/alignment'; 



//  ClassicEditor.builtinPlugins=[
//     // "Mention",
//     // Essentials,
//     // UploadAdapter,
//     // Autoformat,
//     // Bold,
//     // Italic,
//     // BlockQuote,
//     // EasyImage,
//     // Heading,
//     // Image,
//     // ImageCaption,
//     // ImageStyle,
//     // ImageToolbar,
//     // ImageUpload,
//     // Link,
//     // List,
//     // Paragraph,
//     // Alignment   
//  ];
 window.CKClassicEditor= ClassicEditor;

import CKEditor from '@ckeditor/ckeditor5-vue2';
 Vue.use( CKEditor );


 import ToggleButton from 'vue-js-toggle-button'
 
Vue.use(ToggleButton)

window.clean_text=(text,max_char=0)=>{
    var result='';
    if(text!=null && text!='null'){
        text=text.replace( /(<([^>]+)>)/ig, '');
        if(max_char!=0){
            if(text[max_char]==' '){
                max_char-=1;
            }
            var length=text.length;
            if(length>max_char){
                result='...';
            }
            text=text.substring(0, max_char);
            
        }
        return text+result;
    }else{
        return '';
    }    

}
window.Swal=require('sweetalert2');

import Timeline from 'timeline-vuejs'
Vue.component('VTimeline',Timeline);
window.NumberFormat=function (number){
    return  (number).toLocaleString('en-US', {
        style: 'decimal',
      });
    
}
import {
    // Directives
    VTooltip,
    VClosePopper,
    // Components
    Dropdown,
    Tooltip,
    Menu
  } from 'floating-vue'
  
  Vue.directive('tooltip', VTooltip)
  Vue.directive('close-popper', VClosePopper)
  
  Vue.component('VDropdown', Dropdown)
  Vue.component('VTooltip', Tooltip)
  Vue.component('VMenu', Menu)

// window.filesystem = require('level-filesystem')
// window.fs = require('browserify-fs').default;
// window.ExcelJS=require('exceljs');

window.onlyUnique=(value, index, self) =>{
    return self.indexOf(value) === index;
}
import VueFormulate from '@braid/vue-formulate'
Vue.use(VueFormulate);

window.scrollToDom=function(dom){
    $('html, body').animate({
        scrollTop: $(dom).offset().top
    }, 2000);
}

window._path = require('path');

String.prototype.yoneReplaceParam=function(regex,replace){
    var index=0;
    return this.replaceAll(regex,function(p,i){
        if(replace[index]!=undefined){
            var res=replace[index];
            index++;
            return res ;
        }else{
            index++;

            return p;
        }
        
    });
}

String.prototype.toCapitalize=function(){
        var str=(this+''||'').toLowerCase().trim();
        str=str.split(' ');
        str=str.map(el=>{
            var fs=el.split('').map((l,k)=>{
                if(k==0){
                    return l.toUpperCase();
                }else{
                    return l;
                }
            });
            return fs.join('');
            

        });

        return str.join(' ');
    
        
}

import { Cropper } from 'vue-advanced-cropper';
Vue.component(Cropper);
import VuePhoneNumberInput from 'vue-phone-number-input';
Vue.component('vue-phone-number-input', VuePhoneNumberInput);

window.domtoimage = require('dom-to-image');
Vue.component('DtsRkpd',require('./components/page_builder/rkpd.vue').default);
Vue.component('v-show-meta-data-dataset',require('./components/column_dataset.vue').default);

Vue.component('v-img-ava-croper',require('./components/avacrop.vue').default);
Vue.component('v-dts-rkpd',require('./components/dataset/rkpd.vue').default);

Vue.component('v-wa-sender',require('./components/dataset/whatsapp_sended.vue').default);

Vue.component('dash-com-add',require('./components/dataset/dash/com_add_component.vue').default);
Vue.component('dash-markdown',require('./components/dataset/dash/com_markdown.vue').default);
Vue.component('dash-com-edit',require('./components/dataset/dash/com_edit.vue').default);

Vue.component('v-show-cluster',require('./components/dataset/display_cluster.vue').default);

Vue.component('dash-display',require('./components/dataset/dash/com_display.vue').default);
Vue.component('dash-data',require('./components/dataset/dash/com_data.vue').default);

Vue.component('dash-col',require('./components/dataset/dash/com_col.vue').default);
Vue.component('dash-row',require('./components/dataset/dash/com_row.vue').default);
Vue.component('dash-pre',require('./components/dataset/dash/com_pre.vue').default);


Vue.component('dts-edit-pelatihan',require('./components/dataset/input_dataset/pelatihan.vue').default);


Vue.component('v-dash-dts-keterisian-chart',require('./components/dataset/dash_keterisian.vue').default);

// form --------
Vue.component('vi-pelatihan',require('./components/dataset/form/pelatihan.vue').default);

Vue.component('eval-berita-acara',require('./../../Modules/EvaluasiDukungan/Resources/views/admin/components/berita_acara.vue').default);

Vue.component('eval-form-1',require('./../../Modules/EvaluasiDukungan/Resources/views/admin/components/form_1.vue').default);
Vue.component('eval-form-1-data',require('./../../Modules/EvaluasiDukungan/Resources/views/admin/components/form_1_data.vue').default);

Vue.component('eval-form-2',require('./../../Modules/EvaluasiDukungan/Resources/views/admin/components/form_2.vue').default);
Vue.component('eval-form-3',require('./../../Modules/EvaluasiDukungan/Resources/views/admin/components/form_3.vue').default);
Vue.component('eval-form-4',require('./../../Modules/EvaluasiDukungan/Resources/views/admin/components/form_4.vue').default);
Vue.component('eval-form-5',require('./../../Modules/EvaluasiDukungan/Resources/views/admin/components/form_5.vue').default);
Vue.component('eval-form-6',require('./../../Modules/EvaluasiDukungan/Resources/views/admin/components/form_6.vue').default);

Vue.component('chart-eval-form-1',require('./../../Modules/EvaluasiDukungan/Resources/views/admin/components/chart/form_1.vue').default);

Vue.component('chart-eval-form-2',require('./../../Modules/EvaluasiDukungan/Resources/views/admin/components/chart/form_2.vue').default);
Vue.component('chart-eval-form-3',require('./../../Modules/EvaluasiDukungan/Resources/views/admin/components/chart/form_3.vue').default);
Vue.component('chart-eval-form-4',require('./../../Modules/EvaluasiDukungan/Resources/views/admin/components/chart/form_4.vue').default);


var VueHtml2pdf =require('vue-html2pdf').default;
Vue.component('vhtml2pdf',VueHtml2pdf);

import VueExcelEditor from 'vue-excel-editor'
import Vue from "vue";
    
Vue.use(VueExcelEditor)





