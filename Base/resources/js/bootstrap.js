const { default: axios } = require('axios');

window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.Popper = require('popper.js').default;
    var $ = require( 'jquery' );
    global.$ = global.jQuery = $;
    require('bootstrap');
    require('admin-lte');
    $(function(){
        $('.loading-wrapper').css('display','block');
    });



   
    require('bootstrap4-toggle');
    require( 'datatables.net-bs4' ).default;
    require( 'jszip' );
    require( 'pdfmake' );
    require( 'datatables.net-autofill-dt' );
    require( 'datatables.net-buttons-dt' );
    require( 'datatables.net-buttons/js/buttons.colVis.js' );
    require( 'datatables.net-buttons/js/buttons.html5.js' );
    require( 'datatables.net-buttons/js/buttons.print.js' );
    require( 'datatables.net-colreorder-dt' );
    require( 'datatables.net-datetime' );
    require( 'datatables.net-fixedcolumns-dt' );
    require( 'datatables.net-fixedheader-dt' );
    require( 'datatables.net-keytable-dt' );
    require( 'datatables.net-responsive-dt' );
    require( 'datatables.net-rowgroup-dt' );
    require( 'datatables.net-rowreorder-dt' );
    require( 'datatables.net-scroller-dt' );
    require( 'datatables.net-searchbuilder-dt' );
    require( 'datatables.net-searchpanes-bs4' );
    require( 'datatables.net-select-dt' );
    require( 'datatables.net-staterestore-dt' );

} catch (e) {
    console.log(e);
}



window._def_a_con = require('axios');

window.req_ajax;
window.qs = require('querystring');

 window.initial_ajax=(params)=>{
    window.req_ajax= window._def_a_con.create({
        baseURL:params.base,
        headers:{
            Authorization:'Bearer '+params.token,
        },
        validateStatus:function (status) {
            if(status==401){
                console.log('Token Expired');
                
                return true;
            }else if(status==500){

                return true;

            }else if(status==200){
                return true;
            }

            return true;
        }

    });
}
