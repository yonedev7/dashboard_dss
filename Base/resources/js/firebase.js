import { initializeApp } from "firebase/app";
import { getMessaging, getToken } from "firebase/messaging";
class SockFire{
    app=null;
    messaging=null;
    permission=false;
    firebaseConfig = {
        apiKey: "AIzaSyDivFBdl7c7FoRQk8_J2bmYYrMMsSBoWH8",
        authDomain: "test-fire-7251f.firebaseapp.com",
        projectId: "test-fire-7251f",
        storageBucket: "test-fire-7251f.appspot.com",
        messagingSenderId: "757213176203",
        appId: "1:757213176203:web:3768324d3490fd44e08f8e",
        measurementId: "G-DMLR9BH9H8"
    };
    constructor(){
         this.app = initializeApp(this.firebaseConfig);
         this.messaging = getMessaging(this.app);

    }

    requestPemission= async ()=>{
        return await Notification.requestPermission().then((permission) => {
            if (permission === 'granted') {
               this.permission=true;
            }else{
                this.permission=false;
            }
            return this;
        });
    }

    refreshToken= async ()=>{
        getToken(this.messaging, { vapidKey: this.firebaseConfig.apiKey  }).then((currentToken) => {
            if (currentToken) {
                console.log('current_token',currentToken);
              
            } else {
                 this.requestPemission();
                 this.refreshToken();

              // Show permission request UI
              console.log('No registration token available. Request permission to generate one.');
              // ...
            }
          }).catch((err) => {
            this.permission=false;

            // ...
          });

          return this;
    }
}
export default{
    SockFire:SockFire
}



// Initialize Firebase Cloud Messaging and get a reference to the service



