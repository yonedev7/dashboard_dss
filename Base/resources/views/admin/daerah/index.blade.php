@extends('adminlte::page')


@section('content')
<script>
    var page_meta=<?=json_encode($page_meta)?>;
</script>
<div class="parallax"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
    <div class="container">
        <section >
            <h3><b>{{$page_meta['title']}}</b></h3>
            <p><b>{!!$page_meta['keterangan']!!}</b></p>
           
        </section>
      
    </div>
   
</div>
<div class="card">
    <div class="card-body">
    @include('sistem_informasi.partials.filter',['req'=>$req])
    </div>
</div>  


<div class="card" id="table-data">
    <div class="card-body">
        <div class="table-responsive">
            <div class="text-center" v-if="loading">
                <div class="spinner-border text-primary" role="status">
                <span class="sr-only">Loading...</span>
                </div>
                <div class="spinner-border text-secondary" role="status">
                <span class="sr-only">Loading...</span>
                </div>
                <div class="spinner-border text-success" role="status">
                <span class="sr-only">Loading...</span>
                </div>
                <div class="spinner-border text-danger" role="status">
                <span class="sr-only">Loading...</span>
                </div>
                <div class="spinner-border text-warning" role="status">
                <span class="sr-only">Loading...</span>
                </div>
            
            </div>
        <table class="table table-bordered table-striped" v-if="loading==false" id="table-data-show">
            <thead class="thead-dark">
                <tr>
                    <th>AKSI</th>
                    <th>Kodepemda</th>
                    <th>Nama Pemda</th>
                    <th>Regional</th>
                    <th>Regional Wilayah</th>
                    <th>Tahun Kontrak</th>
                    <th>Jenis Bantuan</th>
                    <th>Tahun Proyek</th>  
                    <th>Lokus</th>    
                </tr>    
            </thead>
            <tbody class="">
                <tr v-for="item,k in data" v-bind:class="item.tahun_proyek=={{$Stahun}}?'bg-success':(item.tahun_proyek>0?'bg-warning':'')">
                    <td>
                        <div class="btn-group">
                            <button class="btn btn-primary btn-sm" @click="editParse(item)">Edit</button>
                        </div>    
                    </td>

                    <td>@{{item.kodepemda}}</td>
                    <td>@{{item.nama_pemda}}</td>
                    <td>@{{item.regional_1}}</td>
                    <td>@{{item.regional_2}}</td>
                    <td>@{{load_tahun_proyek(item.tahun_bantuan)}}</td>
                    <td>@{{item.tipe_bantuan}}</td>
                    <td>@{{(item.tahun_proyek)}}</td>
                    <td>@{{(item.lokus)}}</td>


                </tr>    
            </tbody>
            </table>
        </div>
        
    </div>
    <div id="modal-edit" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
           <form v-bind:action="modal.path_real" method="post">
            @csrf
            @method('PUT')
            <div class="modal-content">
                <div class="modal-header">
                    <h5><b>@{{modal.nama_pemda}} (@{{modal.kodepemda}})</b></h5></div>
                <div class="modal-body">
                    <div class="form-group">
                    <label for="">Regional</label>
                           <select name="regional_1" class="form-control" id=""  v-model="modal.regional">
                            <option value="0">TIDAK MASUK REGIONAL</option>
                            <option value="I">REGIONAL 1 - SUMATRA</option>
                            <option value="II">REGIONAL 2 - SULAWESI</option>
                            <option value="III">REGIONAL 3 - KALIMANTAN</option>
                            <option value="IV">REGIONAL 4 - JAWA</option>  
                            <option value="v">REGIONAL 5 - BALI,NTT,MALUKU & PAPUA</option>  

                            </select>                     
                    </div>
                    <div class="form-group">
                        <label for="">Tahun Proyek</label>
                        <select name="tahun_proyek" v-model="modal.tahun_proyek" id="" class="form-control">
                            <option v-bind:value="0">Bukan Lokus NUWAS</option>
                            <option value="1">LONGLIST</option>
                            <option v-bind:value="tahun-4">@{{tahun-4}}</option>
                            <option v-bind:value="tahun-3">@{{tahun-3}}</option>
                            <option v-bind:value="tahun-2">@{{tahun-2}}</option>
                            <option v-bind:value="tahun-1">@{{tahun-1}}</option>
                            <option v-bind:value="tahun">@{{tahun}}</option>
                            <option v-bind:value="tahun+1">@{{tahun+1}}</option>
                            <option v-bind:value="tahun+2">@{{tahun+2}}</option>
                            <option v-bind:value="tahun+3">@{{tahun+3}}</option>
                            <option v-bind:value="tahun+4">@{{tahun+4}}</option>

                        </select>    
                    </div>
                    <div class="form-group" v-if="modal.tahun_proyek>2000">
                        <label for="">Tipe Bantuan</label>
                        <select name="tipe_bantuan" id="" class="form-control"  v-model="modal.tipe_bantuan">
                            <option value="0">TIDAK ADA BANTUAN</option>
                            <option value="STIMULAN">STIMULAN</option>
                            <option value="PENDAMPINGAN">PENDAMPINGAN</option>
                            <option value="BERBASIS KINERJA">BASIS KINERJA</option>
                            <option value="BERBASIS KINERJA & PENDAMPINGAN">BASIS KINERJA & PENDAMPINGAN</option>
                            <option value="BERBASIS KINERJA & STIMULAN">BASIS KINERJA & STIMULAN</option>
                            <option value="PENDAMPINGAN & STIMULAN">BASIS KINERJA & STIMULAN</option>
                            <option value="PENDAMPINGAN & STIMULAN & BERBASIS KINERJA">BASIS KINERJA & STIMULAN & PENDAMPINGAN</option>
                        </select>
                        <p v-if="modal.tipe_bantuan" class="mt-2 rounded p-2 bg-success">*@{{bantuan[modal.tipe_bantuan]}}</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success btn-sm" type="submit">Update</button>    
                </div>


             </div>
            </form>
        </div>
</div>
</div>

@stop

@section('js')
<script>
    var table_data=new Vue({
        el:'#table-data',
        watch:{
            'modal.tahun_proyek':function(){
                if(this.modal.tahun_proyek==0){
                    this.modal.tipe_bantuan=0;
                    this.modal.regional_1=0;
                    this.modal.regional_2=null;
                }
            }
        },
        data:{
            data:[],
            filter:{
                filter_sortlist:"{{$req['filter_sortlist']??''}}",
                filter_tipe_bantuan:"{{$req['filter_tipe_bantuan']??''}}",
                filter_regional:"{{$req['filter_regional']??''}}",
            },
            basis:'{{$basis}}',
            tahun:{{$StahunRoute}},
            loading:false,
            bantuan:{
                'STIMULAN':'Bantuan stimulan untuk pemerintah Daerah yang kapasitas BUMDAMnya masih rendah',
                'PENDAMPINGAN':'Insentif untuk mendorong Pemerintah Daerah dan BUMDAM yang memilki kapasitas dan kelayakan untuk memanfaatkan sumber-sumber pendanaan non pemerintah',
                'BERBASIS KINERJA':'Bantuan untuk meningkatkan efisiensi operasional BUMDAM, mencakup kategori NRW dan EE'

            },
            modal:{
                path:'{{route('dash.si.lokus.update',['tahun'=>$StahunRoute,'kodepemda'=>'xxxx'])}}',
                path_real:'',
                kodepemda:'',
                nama_pemda:'',
                rigional:'',
                tahun_proyek:{{$StahunRoute}},
                tipe_bantuan:''
            }
        },
        created(){
            this.load();
        },
        methods:{
            editParse:function(data){
                this.modal.kodepemda=data.kodepemda;
                this.modal.regional=data.regional_1;
                this.modal.nama_pemda=data.nama_pemda;
                this.modal.tahun_proyek=data.tahun_proyek;
                this.modal.tipe_bantuan=data.tipe_bantuan;
                this.modal.path_real=this.modal.path.replace('xxxx',data.kodepemda);
                $('#modal-edit').modal();
            },
            react:function(data){
                if(data.method=='filter_change'){
                    this.filter=data.data.filter;
                    this.load();
                }
            },
            load_tahun_proyek:function(tahun){
                if(tahun>2000){
                    return tahun;
                }else if(tahun>0){
                    return 'LONGLIST';
                }
            },
            load:function(){
                if(this.datatable){
                    this.datatable.destroy();
                    this.datatable=null;

                }
                this.loading=true;
                req_ajax.post('{{route('api-web.lokus.load-data',['tahun'=>$StahunRoute])}}',this.filter
                   ).then(res=>{
                    table_data.loading=false;
                    table_data.data=res.data;
                    setTimeout(() => {
                        table_data.datatable=$('#table-data-show').DataTable();
                    }, 500);
                })
            }
        }
    })    
</script>


@stop
