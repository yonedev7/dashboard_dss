<div class="modal fade" id="dash-data-form-view" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content ">
        <form v-bind:action="endpoint" method="POST">
            @csrf
            @method('PUT')
            <input type="hidden" name="form-add-entity" value="1">
            <div class="modal-header">
                <h4 class="modal-title">Ubah Entitas</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="form-group">
                    <label for="">Nama Entitas*</label>
                    <input type="text" class="form-control" name="name" v-model="name"    required>
                </div>
                <div class="form-group">
                    <label for="">Definisi / Konsep</label>
                    <textarea name="definisi" class="form-control"  v-model="definisi"></textarea>
                </div>
                <div class="form-group">
                    <label for="">Kode Khusus</label>
                    <input type="text" class="form-control" name="kode_khusus" v-model="kode_khusus"  >
                </div>
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="">Tipe Data*</label>
                            <select name="tipe_data" class="form-control" v-model="tipe_data" required>
                                <option value="integer" >Integer</option>
                                <option value="float"  >Float</option>
                                <option value="string"  >String</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="">Arah Nilai*</label>
                            <select name="arah_nilai" class="form-control" v-model="arah_nilai" required>
                                <option value="1"  >Positif</option>
                                <option value="0"  >Negatif</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="">Satuan*</label>
                            <select name="satuan" class="form-control" v-model="satuan" required>
                                <option value="Orang" >Orang</option>
                                <option value="Keluarga" >Keluarga</option>
                                <option value="Sambungan Rumah" >Sambungan Rumah</option>
                            </select>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="">Tipe Agregasi</label>
                    <select name="fungsi_aggregasi" class="form-control" v-model="fungsi_aggregasi"  required>
                        <option value="non" >Non</option>
                        <option value="sum"  >Sum</option>
                        <option value="count" >Count</option>
                        <option value="min" >Min</option>
                        <option value="max" >Max</option>
                        <option value="average" >Average</option>
                    </select>
                </div>
              </div>
              <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah</button>
              </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>

  @push('js')
  <script>
       var V_entitas_edit=new Vue({
            el:'#dash-data-form-view',
            data:{
                name:null,
                tipe_data:null,
                arah_nilai:null,
                definisi:null,
                satuan:null,
                kode_khusus:null,
                fungsi_aggregasi:'non',
                endpoint:'{{route('dash.datanuwsp.data.update',['tahun'=>$Stahun,'id'=>'xxx'])}}',
            },
            methods:{
                show:function(item){
                    this.name=item.name;
                    this.tipe_data=item.tipe_data;
                    this.arah_nilai=item.arah_nilai;
                    this.kode_khusus=item.kode_khusus;
                    this.definisi=item.definisi_konsep;

                    this.satuan=item.satuan;
                    this.fungsi_aggregasi=item.fungsi_aggregasi;
                    var url_def='{{route('dash.datanuwsp.data.update',['tahun'=>$Stahun,'id'=>'xxx'])}}';
                    this.endpoint=url_def.replace(/xxx/g,item.id);
                    $(this.$el).modal();
                }
            }
        });
  </script>
@endpush
