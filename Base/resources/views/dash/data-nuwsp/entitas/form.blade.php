<div class="modal fade" id="dash-data-form" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content ">
        <form action="{{route('dash.datanuwsp.data.store',['tahun'=>$Stahun,'id_sumber'=>$sumber_data->id])}}" method="POST">
            @csrf
            <input type="hidden" name="form-add-entity" value="1">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Entitas</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="form-group">
                    <label for="">Nama Entitas*</label>
                    <input type="text" class="form-control" name="name" value="{{old('name')}}"    required>
                </div>
                <div class="form-group">
                    <label for="">Definisi / Konsep</label>
                    <textarea name="definisi" class="form-control">{{nl2br(old('definisi'))}}</textarea>
                </div>
                <div class="form-group">
                    <label for="">Kode Khusus</label>
                    <input type="text" class="form-control" name="kode_khusus" value="{{old('kode_khusus')}}" >
                </div>
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="">Tipe Data*</label>
                            <select name="tipe_data" class="form-control" required>
                                <option value="integer" {{old('tipe_data')=='integer'??'selected'}}>Integer</option>
                                <option value="float"  {{old('tipe_data')=='float'??'selected'}}>Float</option>
                                <option value="string"  {{old('tipe_data')=='string'??'selected'}}>String</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="">Arah Nilai*</label>
                            <select name="arah_nilai" class="form-control" required>
                                <option value="1"  {{old('arah_nilai')??'selected'}}>Positif</option>
                                <option value="0"  {{old('arah_nilai')??'selected'}}>Negatif</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="">Satuan*</label>
                            <select name="satuan" class="form-control" required>
                                <option value="Orang"  {{old('satuan')=='Orang'??'selected'}} >Orang</option>
                                <option value="Keluarga" {{old('satuan')=='Keluarga'??'selected'}} >Keluarga</option>
                                <option value="Sambungan Rumah" {{old('Sambungan Rumah')=='Orang'??'selected'}}>Sambungan Rumah</option>
                            </select>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="">Tipe Agregasi</label>
                    <select name="fungsi_aggregasi" class="form-control" required>
                        <option value="non" {{old('fungsi_aggregasi')=='non'??'selected'}}>Non</option>
                        <option value="sum" {{old('fungsi_aggregasi')=='sum'??'selected'}} >Sum</option>
                        <option value="count" {{old('fungsi_aggregasi')=='count'??'selected'}}>Count</option>
                        <option value="min" {{old('fungsi_aggregasi')=='min'??'selected'}}>Min</option>
                        <option value="max" {{old('fungsi_aggregasi')=='max'??'selected'}}>Max</option>
                        <option value="average" {{old('fungsi_aggregasi')=='average'??'selected'}}>Average</option>
                    </select>
                </div>
              </div>
              <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Tambah</button>
              </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>