@extends('adminlte::page')
@section('content_header')
    <h3>Sumber Data {{$sumber_data->name}}</h3>

@endsection
@section('content')
<div class=" row" id="con">
   <div class="col-12">
    <div  class="card" >
        <div class="card-header">
            <div class="row">
                <div class="col-6">
                    <button  onclick="$('#dash-data-form').modal()" class="btn btn-primary">Tambah Entitas</button>
                    <input type="checkbox" onchange="V_sumber.table=($(this).prop('checked')?false:true);console.log(V_sumber.table)"  data-toggle="toggle" data-on="Kategori" data-off="Kategori">
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div v-bind:class="(table?'col-12':'col-8')">
                    <h5><b>Entitas Data</b></h5>
                    <hr>
                    <table class="table table-bordered">
                        <thead>
                            <tr class="bg-navy">
                                <th>Kategori</th>
                                <th>Kode Khusus</th>
                                <th>Nama Entitas</th>
                                <th>Definisi / Konsep</th>
                                <th>Satuan</th>
                                <th>Arah Nilai</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                                <tr v-for="(item,i) in data_entitas">
                                    <td></td>
                                    <td>@{{item.kode_khusus}}</td>
                                    <td>@{{item.name}}</td>
                                    <td>@{{item.definisi_konsep}}</td>
                                    <td>@{{item.satuan}}</td>
                                    <td>@{{item.arah_nilai?'Positif':'Negatif'}}</td>
                                    <td>
                                        <div class="btn-group">
                                            <button v-on:click="edit_entitas(item)" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i></button>
                                            <button class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                                        </div>

                                    </td> 
                                </tr>
                        </tbody>
                    </table>

                </div>
                <div v-bind:class="(table?'d-none':'col-4')">
                    <h5><b>Kategori</b></h5>
                    <hr>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Aksi</th>

                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            
        @endsection
        </div>
    </div>
   </div>
</div>
    
@include('dash.data-nuwsp.entitas.form')
@include('dash.data-nuwsp.entitas.detail')

@section('js')
    <script>
        var V_sumber=new Vue({
            el:"#con",
            data:{
                table:true,
                data_entitas:<?=json_encode($entitas)?>,
                data_kategori:[],
                
            },
            methods:{
                edit_entitas:function(item){

                    V_entitas_edit.show(item);
                }
            },
            watch:{
                
            }
        });

       
    </script>
@endsection