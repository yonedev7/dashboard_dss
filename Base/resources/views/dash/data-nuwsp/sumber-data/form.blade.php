@extends('adminlte::page')
@section('content_header')
    <h3>Tambah Sumber Data</h3>
@endsection

@section('content')
<form action="{{route('dash.datanuwsp.sumberdata.store',['tahun'=>$Stahun])}}" method="post">
    @csrf
    <div class="card">
        <div class="card-body">
            <div class="form-group">
                <label for="">Nama Sumber Data</label>
                <input type="text" class="form-control" required name="name">
            </div>

            <div class="form-group">
                <label for="">Keterangan</label>
                <textarea name="keterangan" class="form-control"></textarea>
            </div>
            
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Tambah</button>
        </div>
    </div>
    
</form>
@endsection