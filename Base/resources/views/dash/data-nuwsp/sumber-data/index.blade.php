@extends('adminlte::page')
@section('content_header')
    <h3>Sumber Data</h3>
@endsection
@section('content')
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-6">
                    <a href="{{route('dash.datanuwsp.sumberdata.form',['tahun'=>$Stahun])}}" class="btn btn-primary">Tambah Sumber Data</a>
                </div>
            </div>
        </div>
        <div class="card-body" id="tb-1">
            
            <v-data-table
                :headers="headers"
                :items="desserts"
                :items-per-page="5"
                class="elevation-1"
            ></v-data-table>

            {{-- <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Sumber Data</th>
                        <th>Jumlah jenis Data</th>
                        <th>Jumlah Kategori Data</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($sumber_data as $key=>$sd)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$sd->name}}</td>
                            <td>{{$sd->jumlah_jenis_data}}</td>
                            <td>{{$sd->jumlah_kategori_data}}</td>
                            <td>
                                <div class="btn-group">
                                    <a href="{{route('dash.datanuwsp.sumberdata.view',['tahun'=>$Stahun,'id'=>$sd->id])}}" class="btn btn-success"><i class="fa fa-arrow-right"></i></a>
                                    <a href="{{route('dash.datanuwsp.sumberdata.view',['tahun'=>$Stahun,'id'=>$sd->id])}}" class="btn btn-primary"><i class="fa fa-eye"></i></a>

                                    <a href="" class="btn btn-danger"><i class="fa fa-trash"></i></a>

                                </div>
                            </td>


                        </tr>
                    @endforeach
                    
                </tbody>
            </table> --}}
        </div>
    </div>
    @endsection


@section('js')
    <script>
        V_sumber_data=new Vue({
            el:"#tb-1",
            data:{
                headers: [
          {
            text: 'Dessert (100g serving)',
            align: 'start',
            sortable: false,
            value: 'name',
          },
          { text: 'Calories', value: 'calories' },
          { text: 'Fat (g)', value: 'fat' },
          { text: 'Carbs (g)', value: 'carbs' },
          { text: 'Protein (g)', value: 'protein' },
          { text: 'Iron (%)', value: 'iron' },
        ],
        desserts: [
          {
            name: 'Frozen Yogurt',
            calories: 159,
            fat: 6.0,
            carbs: 24,
            protein: 4.0,
            iron: '1%',
          },
          {
            name: 'Ice cream sandwich',
            calories: 237,
            fat: 9.0,
            carbs: 37,
            protein: 4.3,
            iron: '1%',
          },
          {
            name: 'Eclair',
            calories: 262,
            fat: 16.0,
            carbs: 23,
            protein: 6.0,
            iron: '7%',
          },
          {
            name: 'Cupcake',
            calories: 305,
            fat: 3.7,
            carbs: 67,
            protein: 4.3,
            iron: '8%',
          },
          {
            name: 'Gingerbread',
            calories: 356,
            fat: 16.0,
            carbs: 49,
            protein: 3.9,
            iron: '16%',
          },
          {
            name: 'Jelly bean',
            calories: 375,
            fat: 0.0,
            carbs: 94,
            protein: 0.0,
            iron: '0%',
          },
          {
            name: 'Lollipop',
            calories: 392,
            fat: 0.2,
            carbs: 98,
            protein: 0,
            iron: '2%',
          },
          {
            name: 'Honeycomb',
            calories: 408,
            fat: 3.2,
            carbs: 87,
            protein: 6.5,
            iron: '45%',
          },
          {
            name: 'Donut',
            calories: 452,
            fat: 25.0,
            carbs: 51,
            protein: 4.9,
            iron: '22%',
          },
          {
            name: 'KitKat',
            calories: 518,
            fat: 26.0,
            carbs: 65,
            protein: 7,
            iron: '6%',
          },
        ],
      
            }
        });

    </script>
@endsection