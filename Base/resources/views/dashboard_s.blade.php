@extends('adminlte::page')


@section('content')

<div id="app">
    <div class="py-2  mb-3"  style="background-image: url({{url('assets/img/bgcard.png')}}); transition: width 2s, height 4s; background-size:100% auto; position: relative; border-bottom:5px solid #fcda1a">
       <div class="container">
        <h5 class="text-center"><b>KONDISI DATA AIR MINUM {{$StahunRoute}}</b></h5>
        <hr>
        <div class="form-group">
            <label for="">Cari Data</label>
            <input type="text" class="form-control" v-model="search.q">
        </div> 
       </div>
    </div>
    
   <div class="container">
  
   </div>
   
   <div class="d-flex flex-column ">
    <div class=" d-flex   mb-2" style="max-width: 100vw; overflow-x:scroll;" v-for="row,index in com_dataset">
        <div class="d-flex"  v-for="item,i in row">
            <div class="d-flex bg-white shadow ">
                <div class=" parallax" style=" background-image: url('{{url('assets/img/back-w2.gif')}}'); ">
                    <charts :options="item.chart_pie" ></charts>
                </div>
                <div class="flex-grow-1 d-flex" >
                    <charts :options="item.chart" ></charts>
                </div>
            </div>
        </div>
   </div>
   <div v-if="com_dataset.length ==0">
            <p class="text-center">Tidak Terdapat Data <span><i class="" v-html="com_s_info"></i></span></p>
   </div>
   </div>
</div>

@stop


@section('js')
<script>
    var app=new Vue({
        el:'#app',
        created:function(){
            var self=this;
            var datasets=<?=json_encode($datasets)?>;
            datasets.forEach((el,i)=>{

                datasets[i].chart.chart['height']=400;
                datasets[i].chart.chart['type']='area';
                datasets[i].chart_pie={
                    chart:{
                        type:'pie',
                        height:400,
                        width:200
                    },
                    xAxis:{
                        type:'category'
                    },
                    tooltip: {
                        pointFormat: '{point.percentage:.1f} %<br>Total: {point.y:.0f} PEMDA'
                    },
                    plotOptions: {
                        pie: {
                            dataLabels: {
                            enabled: true,
                            format: '{point.percentage:.1f} %<br>Total: {point.y:.0f} PEMDA',
                            distance: -10,
                            }
                        }
                    },
                    title:{
                        text:'',
                        enabled:false
                    },
                    subtitle:{
                        text:'',
                        enabled:true
                    },
                    series:[]
                }
               

            });


            this.datasets=datasets;
            this.$forceUpdate();
            setTimeout((ref) => {
                ref.datasets.forEach((el,i)=>{
                req_ajax.post(('{{route('api-web-mod.dataset.public.data.series',['tahun'=>$StahunRoute,'id_dataset'=>'@?'])}}'.yoneReplaceParam('@?',[el.id_dataset]))).then(res=>{
                    if(res.status==200){
                        var ser=[];
                        if(res.data.data[0]!=undefined){
                            ref.datasets[i].chart.series=res.data.data[0].series;
                        }else{
                            ref.datasets[i].chart.series=[];
                        }
                        console.log(res.data);
                        ref.datasets[i].chart.subtitle.text=res.data.pemda_list.length+' PEMDA LONGLIST';
                    }
                });

                req_ajax.post(('{{route('api-web-mod.dataset.public.data.state',['tahun'=>$StahunRoute,'id_dataset'=>'@?'])}}'.yoneReplaceParam('@?',[el.id_dataset]))).then(res=>{
                    if(res.status==200){
                        var ser=[];
                        ref.datasets[i].chart_pie.series=res.data.series;
                        ref.datasets[i].chart_pie.subtitle.text=res.data.series[0].name+' LONGLIST TAHUN {{$StahunRoute}}';

                    }
                });
            });
                
            }, 500,self);
        },
        computed:{
            com_s_info:function(){
                return (this.search.q?' Untuk "'+this.search.q+'"':"");
            },
            com_dataset:function(){
                var dta=this.datasets.filter(el=>{
                    if((this.search.q||'')==''){
                        return true && el.chart.series.length;
                    }else{
                        return (el.dataset.name||'').toUpperCase().includes((this.search.q||'').toUpperCase()) && el.chart.series.length;

                    }
                });

                dta=this.chunk(dta,2);

                return dta;
            }
        },
        methods:{

            chuckSplit:function(input,size){
                var chunked=[];
                var x=Math.ceil(input.length / size);
                if(x){
                    return this.chunk(input,x);
                }else{
                    return [input];
                }
              },  
              chunk:function(input,size){
                  var chunked=[];
                  Array.from({length: Math.ceil(input.length / size)}, (val, i) => {
                      chunked.push(input.slice(i * size, i * size + size))
                  });
  
                  return chunked;
              },
            nFormat:function(val){
                return NumberFormat(val);
            },
            nSelisih:function(i,array){
                if(i==0){
                    yf=array[0].y||0;
                    y=array[i].y||0;
                    per=Math.ceil(((y-yf)/yf)*100);


                }else{
                    yf=array[i-1].y||0;
                    y=array[i].y||0;
                    per=Math.ceil(((y-yf)/yf)*100);

                }

                return [yf,y,per,(y-yf)];
            }
        },
        data:{
            search:{
                q:''
            },
            datasets:[],
            capaian_sr:{
                chart_map:{
                    chart:{
                        type:'map'
                    },
                    title:{
                        text:'Capaian SR Tahun '
                    },
                    series:[
                        {
                            name:'Provinsi',
                            mapData:Highcharts.maps['ind'],

                        }
                    ]
                },
                chart_line:{
                    chart:{
                        type:'line'
                    },
                    title:{
                        text:'Capaian SR'
                    },
                    xAxis:{
                        type:'category',
                        minorTickLength: 0,
                        tickLength: 0,
                        lineWidth: 0,
                        minorGridLineWidth: 0,
                        lineColor: 'transparent',
                    },
                    yAxis:{
                        visible:false,
                    },
                    plotOptions:{
                        series:{
                            dataLabels:{
                                enabled:true
                            },
                        }
                    },
                    series:[
                        {
                            name:'Jumlah SR',
                            type:'area',
                            color:'#4285f4',
                            data:[
                                {
                                    name:'2019',
                                    y:10
                                },
                                {
                                    name:'2020',
                                    y:5
                                },
                                {
                                    name:'2021',
                                    y:9
                                }
                            ]
                        },
                        {
                            name:'Jumlah Pemda',
                            type:'line',
                            color:'#34a854',
                            data:[
                                {
                                    name:'2019',
                                    y:10
                                },
                                {
                                    name:'2020',
                                    y:13
                                },
                                {
                                    name:'2021',
                                    y:3
                                }
                            ]
                        }
                    ]
                }
    }
}
});

</script>
@stop