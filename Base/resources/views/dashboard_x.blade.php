@extends('adminlte::page')


@section('content')
<style>
  .carousel-control-prev-icon,#slider-one .carousel-control-next-icon{
    width:50px;
    height:50px;
  }
  .max-height-500{
    max-height: 500px;

  }
  .scoll-y{
    overflow-y: scroll;
  }
</style>

{{-- <div id="slider-dss" class="carousel slide parallax" data-ride="carousel" style=" background-image: url('{{url('assets/img/back-t.png')}}')">
  <div class="carousel-inner pt-5 " style="height:400px;">
    <div class="carousel-item active"  >
        <div class=" justify-content-center  row" >
          <div class="col-4 text-center">
            <h1><b style="font-size:100px;">&bdquo;</b></h1>
            <p style="font-size:20px;"><b>Aplikasi DSS-NUWSP yang telah dibangun oleh konsultan DSS sebagai bentuk implementasi project dengan menyajikan data dan informasi yang dikelola dalam sistem berbasis web.</b></p>
          </div>
        </div>
          
    </div>
    <div class="carousel-item  justify-content-center  row"  >
      <div class=" justify-content-center row">
        <div class="col-4 text-center">
        <h1><b style="font-size:100px;">&bdquo;</b></h1>

        <p style="font-size:20px;"><b>Data dan Informasi berbasis web tersebut dikelola dalam bentuk technical assistance untuk menyediakan input utama memformulasikan langkah strategis yang diambil setiap DPIU</b></p>
      
      </div>
      </div>
        
    </div>
    <div class="carousel-item  justify-content-center row "     >
      <div class="justify-content-center row">
        <div class="col-4 text-center">
          <h1><b style="font-size:100px;">&bdquo;</b></h1>
          <p style="font-size:20px;"><b>Maksud : Memberikan bantuan kepada Subdit Perumahan dan Kawasan Permukiman Ditjen Bina Pembangunan Daerah dalam pengembangan sistem informasi untuk kebutuhan pelaksanaan program NUWSP tahun 2021 – 2022.</b></p>

        </div>
        <div class="col-4 text-center">
          <h1><b style="font-size:100px;">&bdquo;</b></h1>
          <p style="font-size:20px;"><b>Tujuan : memfasilitasi pelaksanaan  tugas dan fungsi Subdit Perumahan dan Kawasan Permukiman pada Ditjen Bina Pembangunan Daerah, Kementerian Dalam Negeri</b></p>

        </div>
      </div>
        
    </div>
  </div>

    <a class="carousel-control-prev " href="#slider-dss" role="button" data-slide="prev">
      <span class=" bg-primary p-3  rounded-circle"  aria-hidden="true">
        <i class="fa fa-arrow-left"></i>
      </span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#slider-dss" role="button" data-slide="next">
      <span class=" p-3  bg-primary rounded-circle" aria-hidden="true">
        <i class="fa fa-arrow-right"></i>
      </span>
      <span class="sr-only">Next</span>
    </a>
</div>
  --}}
<div id="sr-data" class="parallax" style=" background-image: url('{{url('assets/img/back-w2.gif')}}')">
    @php
        $basis='nasional';
    @endphp
    <div class="bg-primary">
      <p class="p-3 m-0 text-center"><b>CAPAIAN SAMBUNGAN RUMAH (SR) AIR MINUM DAERAH @{{tahun}}</b></p>
    </div>
   
    <div  class="" >

        <charts   :options="chartOptions" :constructor-type="'mapChart'" class=" pb-3"></charts> 

        <div class=" justify-content-center d-flex ">
          <div class="card col-6 p-0 mb-0" v-if="color_list!={}">
            <div class="card-body p-1 text-center">
              <p class="mb-0"><b>Capaian SR</b></p>
              <ul class="list-inline mb-2">
                <li class="list-inline-item" v-for="item,key in color_list">
                  <i class="fa fa-circle" v-bind:style="'color:'+item.color"></i> @{{item.text}} @{{key}} 
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class=" justify-content-center d-flex  pb-3">
         
          <div class="input-group mb-3 col-6">
            <div class="input-group-prepend">
              <div class="btn-group">
                <button class="btn-primary btn" @click="basis='nasional'"><i class="fa fa-arrow-up"></i> NASIONAL</button>
                <button class="btn-warning btn" @click="basis='longlist'"><i class="fa fa-arrow-up"></i> LONGLIST</button>
                <button class="btn-success btn" @click="basis='sortlist'"><i class="fa fa-arrow-up"></i> SORTLITS</button>
              </div>
            </div>
            <select v-model="visible" class="custom-select" placeholder="" aria-label="" aria-describedby="basic-addon1">
                <option value="0">Kota / Kab</option>
                <option value="1">Provinsi</option>
            </select>
          </div>
         
        </div>
    

    </div>

</div>
<div style="height: 20px;" class="bg-yellow"></div>

<div id="rekap-data-tema">
  <div class="row">
    <div class="table-renponsive col-6 pt-3 " >
      <h5 class="text-center p-2"><b>DATA UTAMA</b></h5>
      <div class="scoll-y max-height-500">
        <table class="table">
          <template v-for="tema,mk in metas"> 
          <tr>
            <td colspan="6" class="bg-primary text-center">
              <h4><b>@{{tema.title}}</b></h4>
      
            </td>
          </tr>
          <tr v-for="item,sk in tema.submenu">
            <td>
              <div v-if="item.rekap.load" class="spinner-border text-primary" role="status">
                <span class="sr-only">Loading...</span>
              </div>
            </td>
            <td> 
            <h5><b> @{{item.title}}</b></h5>
            <p>Tahun Data @{{item['rekap']!=undefined?item.rekap.tahun:''}}</p>
            </td>
            <td> 
              <h5><b>NASIONAL</b></h5>
              PROVINSI: <b> @{{item.rekap.nasional.pemda_pro}}</b> PEMDA - KOTA/KAB : <b> @{{item.rekap.nasional.pemda_kabkot}}</b>  PEMDA
            </td>
            <td> 
              <h5><b>LONGLIST</b></h5>
              PROVINSI:  <b> @{{item.rekap.longlist.pemda_pro}}</b> PEMDA - KOTA/KAB : <b>@{{item.rekap.longlist.pemda_kabkot}}</b> PEMDA
            </td>
            <td> 
              <h5><b>SORTLIST {{$Stahun}}</b></h5>
              PROVINSI:  <b> @{{item.rekap.sortlist.pemda_pro}}</b> PEMDA - KOTA/KAB : <b> @{{item.rekap.sortlist.pemda_kabkot}}</b> PEMDA
            </td>
            <td>
              <a v-bind:href="item.link_render.replace('xxx',item.rekap.tahun)+'?meta='+btoa_ren('tematik'+'-'+mk+'-'+sk)" v-if="!item.rekap.load" class="btn btn-primary rounded-circle"><i class="fa fa-arrow-right"></i></a>
            </td>
          </tr>
          </template>
      </table>
      </div>
    </div>
    <div class="table-renponsive col-6 pt-3 ">
      <h5 class="text-center p-2"><b>DATA PENDUKUNG</b></h5>
      <div class="scoll-y max-height-500">
        <table class="table">
          <template v-for="tema,mk in pendukung_metas"> 
          <tr>
            <td colspan="6" class="bg-primary text-center">
              <h4><b>@{{tema.title}}</b></h4>
      
            </td>
          </tr>
          <tr v-for="item,sk in tema.submenu">
            <td>
              <div v-if="item.rekap.load" class="spinner-border text-primary" role="status">
                <span class="sr-only">Loading...</span>
              </div>
            </td>
            <td> 
            <h5><b> @{{item.title}}</b></h5>

            <p>Tahun Data @{{item['rekap']!=undefined?item.rekap.tahun:''}}</p>
            </td>
            <td> 
              <h5><b>NASIONAL</b></h5>
              PROVINSI: <b> @{{item.rekap.nasional.pemda_pro}}</b> PEMDA - KOTA/KAB : <b> @{{item.rekap.nasional.pemda_kabkot}}</b>  PEMDA
            </td>
            <td> 
              <h5><b>LONGLIST</b></h5>
              PROVINSI:  <b> @{{item.rekap.longlist.pemda_pro}}</b> PEMDA - KOTA/KAB : <b>@{{item.rekap.longlist.pemda_kabkot}}</b> PEMDA
            </td>
            <td> 
              <h5><b>SORTLIST {{$Stahun}}</b></h5>
              PROVINSI:  <b> @{{item.rekap.sortlist.pemda_pro}}</b> PEMDA - KOTA/KAB : <b> @{{item.rekap.sortlist.pemda_kabkot}}</b> PEMDA
            </td>
            <td>
              <a v-bind:href="item.link_render.replace('xxx',item.rekap.tahun)+'?meta='+btoa_ren('pendukung'+'-'+mk+'-'+sk)" v-if="!item.rekap.load" class="btn btn-primary rounded-circle"><i class="fa fa-arrow-right"></i></a>

            </td>
          </tr>
          </template>
      </table>
      </div>
      
    </div>
  </div>
</div>
  





@stop

@section('js')

<script>

  var app_sr=new Vue({
    el:"#sr-data",
    created(){
          
      this.init();
      console.log('ref  ',this.ch_p);
      },
      methods:{
        init:function(){
          req_ajax.post('{{route('api-web.public.data.rekap.load_rekap',['tahun'=>$Stahun])}}',{
            'table':'dataset.spm_capaian_air_minum'
          }).then((res)=>{
            app_sr.tahun=res.data.tahun;
            app_sr.chartOptions.title.text=('BASIS '+app_sr.basis).toUpperCase();
            req_ajax.post(('{{route('api-web.public.data.spm.per_kota',['tahun'=>'xxx'])}}').replace('xxx',app_sr.tahun),{
                    basis:app_sr.basis,
                    filter:app_sr.filter,
                    kodepemda:'0'
                }).then((res)=>{
                  app_sr.color_list=res.data.colors;
                    app_sr.chartOptions.series[1].data=res.data.data.filter((el)=>{
                      return el.id>100;
                    });
                    app_sr.chartOptions.series[0].data=res.data.data.filter((el)=>{
                      return el.id<100;
                    });
                  });
            });
        }
      },
    data:{
      ch_p:{},
      ch_k:{},
      keterangan:'',
      basis:'nasional',
      tahun:{{$Stahun}},
      filter:'provinsi dan kota / kab',
      visible:0,
      color_list:{},
      chartOptions:{
        title:{
          text:'BASIS',
          style: {
              display: 'block'
          }
        },
        subtitle:{
          text:'Nasional Tahun 2022',
          style: {
              display: 'none'
          }
        },
        chart:{
          height:500,
          backgroundColor:'#6c757d00'
        },
        tooltip: {
              formatter:function(){
                  return this.point.text?'<b>'+this.point.name+'</b><br>'+this.point.text+' '+parseFloat(this.point.value).toFixed(2)+'%':'<b>'+this.point.name+' 0% '+'</b>';
              }
          },
         
      

        series:[
          {
            name:'provinsi',
            type:'map',
            mapData:Highcharts.maps['ind'],
            joinBy:['id','id'],
            visible:true,
            showInLegend:false,
            data:[],
            dataLabels: {
                enabled: false,
                color: '#FFFFFF',
                format: '{point.name}'
            },
          },
          {
            name:'kabkota',
            type:'map',
            mapData:Highcharts.maps['ind_kab'],
            joinBy:['id','id'],
            showInLegend:false,
            visible:true,
            data:[]
          },
         
          
        ]
      },
  
    },
    watch:{
      basis:function(){
        this.init();
      },
      visible:function(val){
       if(val==0){
          this.chartOptions.series[1].visible=true;
          this.chartOptions.series[0].dataLabels.enabled=false;


        }else{
          this.chartOptions.series[1].visible=false;
          this.chartOptions.series[0].dataLabels.enabled=true;


        }

        //  this.chartOptions.series[0].data=Object.assign(this.chartOptions.series[0].data,JSON.parse(JSON.stringify(this.chartOptions.series[0].data)));
        // this.chartOptions.series[1].data=Object.assign(this.chartOptions.series[1].data,JSON.parse(JSON.stringify(this.chartProvOptions.series[1].data)));
        
      }

     
    }
  });

  var rekap=new Vue({
    el:'#rekap-data-tema',
    created(){
      this.loadRekapMeta(this.index_meta[0],this.index_meta[1]);
      if(this.pendukung_metas){
        this.loadRekapPendukungMeta(this.index_meta[0],this.index_meta[1]);
      }

    //  for(var i in this.metas){
    //    var t1=i!=0?i:1.2;

    //    for(var s in this.metas[i].submenu){
    //       var t2=s!=0?s:1.2;
    //       var time=((t1*t2)*300)>1000?1000+(t1*t2):((t1*t2)*300);
    //        console.log('time',time);
    //     setTimeout(
    //       this.loadRekapMeta(i,s)
    //       , 500*((i>0?i:1)*(s>0?s:1)));
         
    //    }
    //  }
    },
    methods:{
      btoa_ren:function(text){
        return window.btoa(text);
      },
      next_index_meta:function(){
        if(this.metas[this.index_meta[0]]!=undefined && this.metas[this.index_meta[1]]['submenu'][this.index_meta[1]+1]!=undefined){
          this.index_meta=[this.index_meta[0],this.index_meta[1]+1];
          this.loadRekapMeta(this.index_meta[0],this.index_meta[1]);
        }else if(this.metas[this.index_meta[0]+1]!=undefined){
          this.index_meta=[this.index_meta[0]+1,0];
          this.loadRekapMeta(this.index_meta[0],this.index_meta[1]);
        }else{

        }
      },
      next_index_pendukung:function(){

        if(this.pendukung_metas[this.index_pendukung[0]]!=undefined && this.pendukung_metas[this.index_pendukung[1]]['submenu'][this.index_pendukung[1]+1]!=undefined){
          this.index_pendukung=[this.index_pendukung[0],this.index_pendukung[1]+1];

         this.loadRekapPendukungMeta(this.index_pendukung[0],this.index_pendukung[1]);

        }else if(this.pendukung_metas[this.index_pendukung[0]+1]!=undefined){
        this.index_pendukung=[this.index_pendukung[0]+1,0];
         this.loadRekapPendukungMeta(this.index_pendukung[0],this.index_pendukung[1]);

        }else{

        }

      },
      loadRekapMeta:async function(index,indexsub){
        console.log('met',index,indexsub);

        if(this.metas[index].submenu.length && this.metas[index]['submenu'][indexsub]!=undefined){
          this.metas[index]['submenu'][indexsub]['rekap']['load']=true;
          req_ajax.post('{{route('api-web.public.data.rekap.load_rekap',['tahun'=>$Stahun])}}',{
            'table':this.metas[index]['submenu'][indexsub].table
          }).then((res)=>{
            rekap.metas[index]['submenu'][indexsub]['rekap']['load']=false;
            rekap.metas[index]['submenu'][indexsub]['rekap']=res.data;
            rekap.next_index_meta();
          });
        }else{
          rekap.next_index_meta();
        }
      },
      loadRekapPendukungMeta:async function(index,indexsub){
        console.log('pen',index,indexsub);
        if(this.pendukung_metas[index].submenu.length && this.pendukung_metas[index]['submenu'][indexsub]!=undefined){
          this.pendukung_metas[index]['submenu'][indexsub]['rekap']['load']=true;
          req_ajax.post('{{route('api-web.public.data.rekap.load_rekap',['tahun'=>$Stahun])}}',{
            'table':this.pendukung_metas[index]['submenu'][indexsub].table
          }).then((res)=>{
            rekap.pendukung_metas[index]['submenu'][indexsub]['rekap']['load']=false;
            rekap.pendukung_metas[index]['submenu'][indexsub]['rekap']=res.data;
            rekap.next_index_pendukung();
          });
        }else{
          rekap.next_index_pendukung();

        }

      },
    },
    data:{
      index_meta:[0,0],
      index_pendukung:[0,0],
      metas:<?=json_encode($tema_meta)?>,
      pendukung_metas:<?=json_encode($pendukung_meta)?>,

    }
  })
 
</script>

@stop