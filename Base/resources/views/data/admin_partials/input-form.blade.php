<script>
  Vue.component('yone-input-form',{
        props:{
            showindex:Number,
            columns:Array,
            data:Object,
            index_row:Number,
            name:String,
            datasetpemda:Boolean,
            'optionpemda':Function,
            showme:Function,
            maxindex:Number,
            addnew:Function,

        },
        data:()=>{
            return {
                noBind:['kodepemda','keterangan','status'],
                ref_timeout:null,
                ref_timeout_index:null,
                url_iframe:null,
                bu_select:null,
                loading:false,

            }
        },
        watch:{
            'bu_select':function(val){
                if(!val){
                    val='@';
                }

                val=val.split('@');
                this.data.kodebu=parseInt(val[0]);
                this.data.kodepemda=val[1]+'';
                this.UpdateBu();
            },
        },
        computed:{
            com_option_pemda:function(){
                    if(this.datasetpemda){
                        return this.optionpemda(this.data.kodepemda);
                    }else{
                        return this.optionpemda(this.data.kodebu+'@'+this.data.kodepemda);
                    }
                },
        },
        methods:{
            formatAngka:function(angka){
                    var number_string =(angka+'');
                    var split   		= number_string.split('.');
                    split[0]        = (split[0]+'').replace(/[^,\d]/g, '').toString();
                    var sisa     		= split[0].length % 3,
                    rupiah     		= split[0].substr(0, sisa),
                    ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

                    if(ribuan){
                        var separator = sisa ? '.' : '';
                        rupiah += separator + ribuan.join('.');
                    }
                    if(split[1]!=undefined){
                        split[1]=this.formatAngka(split[1]);
                    }
                    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                    return rupiah;
            },
            valueBind:function(column){
                var val_='0';
                if(!(['single_file','multy_file']).includes(column['type'])){
                    if(column['bind_view']!=undefined && column['bind_view'].length){
                        var index=-1;
                        if(this.data[column.key]!=null){
                            do{
                                index+=1;
                                if(column['bind_view'].length<=index){
                                    index-=1;
                                    logic=true;
                                }else{
                                    logic_=column['bind_view'][index].logic.replace(/#val/g,'val_xxx');
                                    var val_xxx=this.data[column.key];
                                    logic=eval(logic_);
                                
                                }
                            }while((logic!=true));
                            
                            if(column['bind_view'].length){
                                val_=column['bind_view'][index]['tag'];
                                val_=val_.replace(/\[val\]/g,this.data[column.key]);
                            }else{

                                val_= this.data[column.key];
                                if(column.tipe_nilai=='numeric'){
                                    val_=this.formatAngka(this.data[column.key])||0;
                                }else{
                                    val_=this.data[column.key]||'';
                                }
                            }

                        }else{
                            val_=this.data[column.key]||'';
                        }

                    
                    }else{
                        if(column.tipe_nilai=='numeric'){
                            val_=this.formatAngka(this.data[column.key])||0;
                        }else{
                            val_=this.data[column.key]||'';
                        }
                    }
                }else{

                    if(this.data[column.key]){
                        try{
                            val_=JSON.parse(this.data[column.key]);
                        }catch(err){
                            val_=Array();
                        }
                        val_=val_.map((el)=>{
                            return `<span onclick="{{$init['component']['id']}}.showIframe('{{url('')}}`+el.path+`')" class=" badge bg-navy  cursor-clickable">`+el.name+`</span>`;
                        });
                        val_=val_.join(' ');
                    }else{
                        val_='';
                    }

                }
                if(column.key=='kodepemda'){
                    if(val_){
                        val_=parseInt(val_)+'';
                    }
                }

                return val_+(column['satuan']!=undefined?' ('+column.satuan+')':'');
                
            },
            requestIndex:function(index){
                this.showme(index);
            },
            newRow:function(){
                this.addnew();
                var self=this;
                setTimeout((self) => {
                self.showme(self.maxindex);
                    
                }, 100,self);
            },
            changeActiveCelIndex:function(col,row,key){
                if(this.changeActiveCel){
                    this.changeActiveCel(col,row,key);

                }
            },
            UpdateBu:function(){
                    var pemda=this.optionpemda(this.data.kodebu+'@'+this.data.kodepemda,'bu').filter(el=>{
                        return (el.kodebu+'')==(this.data.kodebu+'') && el.kodepemda==this.data.kodepemda;
                    });

                    if(pemda.length){
                        this.data.kodepemda=pemda[0].kodepemda;
                        this.data.name=pemda[0].nama_pemda;
                        this.data.tipe_bantuan=pemda[0].tipe_bantuan;
                        this.data.regional_1=pemda[0].regional_1;
                        this.data.regional_2=pemda[0].regional_2;
                        this.data.kodebu=pemda[0].kodebu;
                        this.data.nama_bu=pemda[0].nama_bu;


                    }else{

                        this.data.kodepemda=null;
                        this.data.name=null;
                        this.data.tipe_bantuan=null;
                        this.data.regional_1=null;
                        this.data.regional_2=null;
                        this.data.kodebu=null;
                        this.data.nama_bu=null;
                    }

                    this.changeActiveCelIndex(-1,-1,null);
                },

            UpdatePemda:function(){
                var pemda=this.optionpemda(this.data.kodepemda).filter(el=>{
                    return (el.kodepemda+'')==(this.data.kodepemda+'');
                });

                if(pemda.length){
                    this.data.kodepemda=pemda[0].kodepemda;
                    this.data.name=pemda[0].name;
                    this.data.tipe_bantuan=pemda[0].tipe_bantuan;
                    this.data.regional_1=pemda[0].regional_1;
                    this.data.regional_2=pemda[0].regional_2;
                }else{

                    this.data.kodepemda=null;
                    this.data.name=null;
                    this.data.tipe_bantuan=null;
                    this.data.regional_1=null;
                    this.data.regional_2=null;

                }

            },
            upload_file:function(ev,row,key){
                    var files= ev.target.files;
                    var formData = new FormData();
                    var index=0
                    ev.target.files.forEach(el=>{
                        formData.append('file['+index+']',el);
                        formData.append('name['+index+']',el.name);
                    });
                    var self=this;
                    self.loading=true;
                    req_ajax.post('{{route('api-web-mod.dataset.upload_file',['tahun'=>$StahunRoute,'id_dataset'=>$init['component']['id_dataset'],'tw'=>$init['component']['tw']])}}',formData,{
                            headers: {
                            'Content-Type': 'multipart/form-data'
                            }
                    }).then(res=>{
                    self.loading=false;

                        this.data[key]=JSON.stringify(res.data);
                        this.changeActiveCelIndex(-1,-1,null);
                    }).catch(err=>{
                    self.loading=false;
                    });
            },
        },
        template:`
        <div class="modal " v-bind:id="'modal-form-yone-'+index_row" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-full" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">@{{datasetpemda?data.name:data.name+' - '+(data.nama_bu||'')}}</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
               <div class="row">
                <div class="col-md-6" v-for="column,index_column in columns" v-if="column.editable">
                    <div class="form-group">
                       
                        <label>@{{column.label}}</label> 
                        <template v-if="column.type=='options'" >
                                <select  class="form-control"  v-bind:id="'input-c-'+index_column+'-r-'+index_row" v-model="data[column.key]"   v-bind:name="'data['+index_row+']['+column.key+']'" >
                                    <option v-bind:value="op.value" v-for="op in column.options">@{{op.tag}}</option>
                                </select>
                            </template>
                            <template v-if="column.type=='input-value'&&column.tipe_nilai=='numeric'">
                                <template  v-if="column.key=='kodebu'" && datasetpemda >
                                    <v-select @input="UpdateBu"  v-bind:id="'input-c-'+index_column+'-r-'+index_row"  :options="com_option_pemda" class="form-control" label="name" :reduce="l => l.kodebu_integrate" v-model="bu_select" ></v-select>
                                </template>
                                <template v-else>
                                <input type="number" step="0.001"  v-bind:id="'input-c-'+index_column+'-r-'+index_row"   v-bind:name="'data['+index_row+']['+column.key+']'"  class="form-control" v-model="data[column.key]">
                                </template>
                            </template>
                            <template v-if="column.tipe_nilai=='string'&&column.type=='input-value'">
                                <template  v-if="column.key=='kodepemda' && datasetpemda" >
                   
                                <v-select @input="UpdatePemda" v-bind:id="'input-c-'+index_column+'-r-'+index_row"  :options="com_option_pemda" class="form-control" label="name" :reduce="l => l.kodepemda" v-model="data.kodepemda" ></v-select>
                                </template>
                                <template  v-else-if="column.key=='kodepemda' && (!datasetpemda)" >
                                    @{{data.kodepemda}}
                                </template>
                                
                                <template  v-else>
                                    <textarea-autosize   v-bind:id="'input-c-'+index_column+'-r-'+index_row" 
                                    class="form-control"
                                    placeholder="Type something here..."
                                    :min-height="50"
                                    :max-width="'100%'"
                                    v-bind:name="'data['+index_row+']['+column.key+']'" 
                                    v-model="data[column.key]" 
                                    />
                                </template>
                            </template>
                        
                            <template v-if="column.type=='single_file'"  >
                                <input type="file"  v-bind:id="'input-c-'+index_column+'-r-'+index_row"  class="custom-file-input1" v-bind:accept="column.file_accept.join(',')"   style="" @change="upload_file($event,index_row,column.key)">  
                                <input type="hidden" v-bind:name="'data['+index_row+']['+column.key+']'" v-model="data[column.key]">    
                            </template>
                            <template v-if="column.type=='multy_file'"  >
                                <input type="file"  v-bind:id="'input-c-'+index_column+'-r-'+index_row"  class="custom-file-input1"  v-bind:accept="column.file_accept.join(',')"   multiple style="" @change="upload_file($event,index_row,column.key)">  
                                <input type="hidden" v-bind:name="'data['+index_row+']['+column.key+']'" v-model="data[column.key]">    
                            </template>  
                            <div class="progress" v-if="loading">
                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="loading.." style="width: 100%"></div>
                            </div> 
                            <div v-if="noBind.includes(column.key)!=true" class="d-flex">
                                <small class="text-success" v-html="valueBind(column)"></small>
                            </div>
                    </div>
                </div> 
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" @click="newRow"  v-if="maxindex==index_row" data-dismiss="modal">Row Baru</button>
                <button type="button" class="btn btn-success" v-if="(index_row>0)" @click="requestIndex(showindex - 1)"  data-dismiss="modal"><i class="fa fa-arrow-left"></i> </button>
                <button type="button" class="btn btn-success"  v-if="maxindex > index_row"  @click="requestIndex(showindex + 1)"  data-dismiss="modal"><i class="fa fa-arrow-right"></i></button>


            </div>
          </div>
        </div>
      </div>

           
        
        `
});


</script>