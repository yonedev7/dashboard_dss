<script>

Vue.component('input-row', {
    props: {
        data:Object,
        column:Object,
        index_row:Number,
        index_column:Number,
        ajaxw:Array,
        copyrowr:Function,
        datasetpemda:Boolean,
        'optionpemda':Function,
        active_cell_index:Array,
        'updateTrigger':Function,
        'changeActiveCel':Function

    },
    data:()=>{
        return {
            ref_timeout:null,
            ref_timeout_index:null,
            url_iframe:null,
            loading:false,
            bu_select:null,
            pemda_list:[]

        }
    },
    created:function(){
    },
    computed:{
        com_option_pemda:function(){
            if(this.datasetpemda){
                return this.optionpemda(this.data.kodepemda);
            }else{
                return this.optionpemda(this.data.kodebu+'@'+this.data.kodepemda);
            }
        },
        bind_value_check:function(){
            var val_='0';
            if(!(['single_file','multy_file']).includes(this.column['type'])){
                if(this.column['bind_view']!=undefined && this.column['bind_view'].length){
                    var index=-1;
                    if(this.data[this.column.key]!=null){
                        do{
                            index+=1;
                            if(this.column['bind_view'].length<=index){
                                index-=1;
                                logic=true;
                            }else{
                                logic_=this.column['bind_view'][index].logic.replace(/#val/g,'val_xxx');
                                var val_xxx=this.data[this.column.key];
                                logic=eval(logic_);
                            
                            }
                        }while((logic!=true));
                        
                        if(this.column['bind_view'].length){
                            val_=this.column['bind_view'][index]['tag'];
                            val_=val_.replace(/\[val\]/g,this.data[this.column.key]);
                        }else{

                            val_= this.data[this.column.key];
                            if(this.column.tipe_nilai=='numeric'){
                                val_=this.formatAngka(this.data[this.column.key])||0;
                            }else{
                                val_=this.data[this.column.key]||'';
                            }
                        }

                    }else{
                        val_=this.data[this.column.key]||'';
                    }
            
                
                }else{
                    if(this.column.tipe_nilai=='numeric'){
                        val_=this.formatAngka(this.data[this.column.key])||0;
                    }else{
                        val_=this.data[this.column.key]||'';
                    }
                }
            }else{

                if(this.data[this.column.key]){
                    try{
                        val_=JSON.parse(this.data[this.column.key]);
                    }catch(err){
                        val_=Array();
                    }
                    val_=val_.map((el)=>{
                        return `<span onclick="{{$init['component']['id']}}.showIframe('{{url('')}}`+el.path+`')" class=" badge bg-navy  cursor-clickable">`+el.name+`</span>`;
                    });
                    val_=val_.join(' ');
                }else{
                    val_='';
                }

            }
            if(this.column.key=='kodepemda'){
                if(val_){
                    val_=parseInt(val_)+'';
                }
            }

            return val_+(this.column['satuan']!=undefined?' ('+this.column.satuan+')':'');
        }
    },
    template: `
    <div>
            <div class="progress" v-if="loading">
                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="loading.." style="width: 100%"></div>
            </div>
            
            <div v-bind:style="(column.editable==true?((active_cell_index[0]==index_column&&active_cell_index[1]==index_row)?false:true):true)?'display:block':'display:none'">
            <p><b v-html="bind_value_check" @click="copy(index_row,index_column)"></b></p>
            <p><span v-if="column.editable==true" @click="changeActiveCelIndex(index_column,index_row,column.key)" class="badge bg-primary cursor-clickable">Click Untuk Mengedit</span> 
                <span v-if="column.editable==true && !(['status','keterangan'].includes(column.key)) && (data[column.key]!=null && data[column.key]!='') "  @click="copy(index_row,index_column)" class="badge badge-success  cursor-clickable">Copy</span></p>
            </div>
            <div v-bind:style="(column.editable==true?((active_cell_index[0]==index_column&&active_cell_index[1]==index_row)?true:false):false)?'display:block':'display:none' ">
               
                <template v-if="column.type=='options'" >
                    <select  class="form-control"  v-bind:id="'input-c-'+index_column+'-r-'+index_row" v-model="data[column.key]"   v-bind:name="'data['+index_row+']['+column.key+']'" >
                        <option v-bind:value="op.value" v-for="op in column.options">@{{op.tag}}</option>
                    </select>
                </template>
                <template v-if="column.type=='input-value'&&column.tipe_nilai=='numeric'">
                    <template  v-if="column.key=='kodebu'" && datasetpemda >
                        <v-select @input="UpdateBu"  v-bind:id="'input-c-'+index_column+'-r-'+index_row"  :options="com_option_pemda" class="form-control" label="name" :reduce="l => l.kodebu_integrate" v-model="bu_select" ></v-select>
                    </template>
                    <template v-else>
                    <input type="number" step="0.001"  v-bind:id="'input-c-'+index_column+'-r-'+index_row"   v-bind:name="'data['+index_row+']['+column.key+']'"  class="form-control" v-model="data[column.key]">
                    </template>
                </template>

                <template v-if="column.tipe_nilai=='string'&&column.type=='input-value'">
                   <template  v-if="column.key=='kodepemda' && datasetpemda" >
                   
                    <v-select @input="UpdatePemda" v-bind:id="'input-c-'+index_column+'-r-'+index_row"  :options="com_option_pemda" class="form-control" label="name" :reduce="l => l.kodepemda" v-model="data.kodepemda" ></v-select>
                    </template>
                    <template  v-else-if="column.key=='kodepemda' && (!datasetpemda)" >
                        @{{data.kodepemda}}
                    </template>
                   
                    <template  v-else>
                        <textarea-autosize   v-bind:id="'input-c-'+index_column+'-r-'+index_row" 
                        class="form-control"
                        placeholder="Type something here..."
                        :min-height="50"
                        :max-width="'100%'"
                        v-bind:name="'data['+index_row+']['+column.key+']'" 
                        v-model="data[column.key]" 
                        />
                    </template>
                    
                </template>
                
               
                <template v-if="column.type=='single_file'"  >
                    <input type="file"  v-bind:id="'input-c-'+index_column+'-r-'+index_row"  class="custom-file-input1" v-bind:accept="column.file_accept.join(',')"   style="" @change="upload_file($event,index_row,column.key)">  
                    <input type="hidden" v-bind:name="'data['+index_row+']['+column.key+']'" v-model="data[column.key]">    
                </template>
                <template v-if="column.type=='multy_file'"  >
                    <input type="file"  v-bind:id="'input-c-'+index_column+'-r-'+index_row"  class="custom-file-input1"  v-bind:accept="column.file_accept.join(',')"   multiple style="" @change="upload_file($event,index_row,column.key)">  
                    <input type="hidden" v-bind:name="'data['+index_row+']['+column.key+']'" v-model="data[column.key]">    
                </template>
            </div>

           
    </div>`,
  
    methods:{
        copy:function(index_row,index_column){
            console.log(this);
            if(this.copyrowr){
                console.log(this.copyrowr(index_row,index_column));
            }

            // this.copy_row_r(index_row,index_column);
        },
        UpdatePemda:function(){
           
            var pemda=this.optionpemda(this.data.kodepemda).filter(el=>{
                return (el.kodepemda+'')==(this.data.kodepemda+'');
            });

            if(pemda.length){
                this.data.kodepemda=pemda[0].kodepemda;
                this.data.name=pemda[0].name;
                this.data.tipe_bantuan=pemda[0].tipe_bantuan;
                this.data.regional_1=pemda[0].regional_1;
                this.data.regional_2=pemda[0].regional_2;
            }else{

                this.data.kodepemda=null;
                this.data.name=null;
                this.data.tipe_bantuan=null;
                this.data.regional_1=null;
                this.data.regional_2=null;

            }

            this.changeActiveCelIndex(-1,-1,null);

        },
        UpdateBu:function(){
            var pemda=this.optionpemda(this.data.kodebu+'@'+this.data.kodepemda,'bu').filter(el=>{
                return (el.kodebu+'')==(this.data.kodebu+'') && el.kodepemda==this.data.kodepemda;
            });

            if(pemda.length){
                this.data.kodepemda=pemda[0].kodepemda;
                this.data.name=pemda[0].nama_pemda;
                this.data.tipe_bantuan=pemda[0].tipe_bantuan;
                this.data.regional_1=pemda[0].regional_1;
                this.data.regional_2=pemda[0].regional_2;
                this.data.kodebu=pemda[0].kodebu;
                this.data.nama_bu=pemda[0].nama_bu;


            }else{

                this.data.kodepemda=null;
                this.data.name=null;
                this.data.tipe_bantuan=null;
                this.data.regional_1=null;
                this.data.regional_2=null;
                this.data.kodebu=null;
                this.data.nama_bu=null;
            }

            this.changeActiveCelIndex(-1,-1,null);
        },
        changeActiveCelIndex:function(col,row,key){
            if(this.changeActiveCel){
                this.changeActiveCel(col,row,key);

            }
        },
        formatAngka:function(angka){
                var number_string = (angka+'').replace(/[^,\d]/g, '').toString(),
                split   		= number_string.split(','),
                sisa     		= split[0].length % 3,
                rupiah     		= split[0].substr(0, sisa),
                ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
    
                // tambahkan titik jika yang di input sudah menjadi angka ribuan
                if(ribuan){
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                }
    
                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                return rupiah;
        },
        upload_file:function(ev,row,key){
               var files= ev.target.files;
               var formData = new FormData();
               var index=0
               ev.target.files.forEach(el=>{
                   formData.append('file['+index+']',el);
                   formData.append('name['+index+']',el.name);
               });
               var self=this;
               self.loading=true;
               req_ajax.post('{{route('api-web-mod.dataset.upload_file',['tahun'=>$StahunRoute,'id_dataset'=>$init['component']['id_dataset'],'tw'=>$init['component']['tw']])}}',formData,{
                    headers: {
                    'Content-Type': 'multipart/form-data'
                    }
               }).then(res=>{
               self.loading=false;

                this.data[key]=JSON.stringify(res.data);
                this.changeActiveCelIndex(-1,-1,null);
            }).catch(err=>{
               self.loading=false;
            });
        },
        getMeta:function(val){
            var self=this;
            self.data.name=null;
            self.data.tahun_proyek=null;
            self.data.tipe_bantuan=null;
            self.data.regional_1=null;
            self.data.regional_2=null;
            if((val.length==3||val.length==5)||val.length==1){
                req_ajax.post(('{{route('api-web-mod.dataset.meta_pemda',['tahun'=>$StahunRoute,'kodepemda'=>'xxxx'])}}').replace('xxxx',val),{
                    basis:{{$init['component']['id']}}.basis
                }).then(function(res){
                    if(res.data){
                        self.ref_timeout=null;
                        self.data.name=res.data.nama_pemda;
                        self.data.tahun_proyek=res.data.tahun_proyek;
                        self.data.tipe_bantuan=res.data.tipe_bantuan;
                        self.data.regional_1=res.data.regional_1;
                        self.data.regional_2=res.data.regional_2;


                    }else{
                        self.data.name=null;
                        self.data.tahun_proyek=null;
                        self.data.tipe_bantuan=null;
                        self.data.regional_1=null;
                        self.data.regional_2=null;


                    }
                });
            
                
            }else{
            
               
                self.data.name=null;
                self.data.tahun_proyek=null;
                self.data.tipe_bantuan=null;
                self.data.regional_1=null;
                self.data.regional_2=null;
            
           }
        }
    },
    watch: {
       'bu_select':function(val){
                if(!val){
                    val='@';
                }

            val=val.split('@');
            this.data.kodebu=parseInt(val[0]);
            this.data.kodepemda=val[1]+'';
            this.UpdateBu();
       },
        'active_cell_index':function(val){
            if(this.index_column==val[0] && this.index_column==val[1]){
               
            }
        },
        data: {
            handler: function(newValue,oldVue) {
               if(newValue){
                if (this.updateTrigger) {
                    
                    if(this.data.method!='delete'){
                        this.data.method='update';
                        if(!newValue.koepemda!=oldVue.kodepemda){
                            this.updateTrigger(newValue,newValue.uuid_row,oldVue,this.index_row);
                            if(this.ref_timeout_index){
                                clearTimeout(this.ref_timeout_index);
                                this.ref_timeout=null;
                            }
                            var self=this;
                            
                            if(this.active_cell_index[2]!='kodepemda'){
                                this.ref_timeout_index=setTimeout((ref)=>{
                                    ref.changeActiveCelIndex(-1,-1,null);
                                },2000,self);
                            }
                           
                        }
                    }
                }
               }
            },
            deep: true
        },
    }
});
</script>