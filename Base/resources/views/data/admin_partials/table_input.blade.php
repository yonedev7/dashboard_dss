@php
    $init=[
        'component'=>[
            'name'=>isset($init['component']['name'])?$init['component']['name']:'',
            'id'=>isset($init['component']['id'])?$init['component']['id']:'table_list',
            'basis'=>isset($init['component']['basis'])?$init['component']['basis']:'NASIONAL',
            'tw'=>isset($init['component']['tw'])?$init['component']['tw']:4,
            'id_dataset'=>isset($init['component']['id_dataset'])?$init['component']['id_dataset']:0,
            'tahun'=>isset($init['component']['tahun'])?$init['component']['tahun']:0,
            'pemda_list'=>isset($init['component']['pemda_list'])?$init['component']['pemda_list']:[],
            'dataset'=>isset($init['component']['dataset'])?$init['component']['dataset']:[],
            'def_kodepemda'=>isset($init['component']['def_kodepemda'])?$init['component']['def_kodepemda']:'',
            'def_name'=>isset($init['component']['def_name'])?$init['component']['def_name']:'',
            'def_tahun_proyek'=>isset($init['component']['def_tahun_proyek'])?$init['component']['def_tahun_proyek']:0,
            'def_tipe_bantuan'=>isset($init['component']['def_tipe_bantuan'])?$init['component']['def_tipe_bantuan']:null,
            'def_regional_1'=>isset($init['component']['def_regional_1'])?$init['component']['def_regional_1']:null,
            'def_regional_2'=>isset($init['component']['def_regional_2'])?$init['component']['def_regional_2']:null,


        ],
        'map_table'=>[
            'submit_url'=>isset($init['map_table']['submit_url'])?$init['map_table']['submit_url']:null,
            'ajax_data'=>isset($init['map_table']['ajax_data'])?$init['map_table']['ajax_data']:'',
            'columns'=>isset($init['map_table']['columns'])?$init['map_table']['columns']:[],
        ]
];



@endphp
@push('css')
{{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.17.1/extensions/sticky-header/bootstrap-table-sticky-header.min.css"> --}}

<style>
 .table-excel td{
    padding:0px;
    height:calc(1.6em + 0.75rem + 2px);
 }
 .table-excel td p{
    margin:10px;

 }
 .cursor-clickable{
    cursor: pointer;
 }

 .tableFixHead {
    height: {{($init['component']['def_kodepemda'])?700:600}}px;
}
.tableFixHead thead th {
    position: sticky;
    top: 0;
    z-index: 10;
}
.tableFixHead  thead tr.second-header th{
    top:65px;
    z-index: 8;
}
.vs__dropdown-toggle{
    background:#fff;
}

.tableFixHead thead th.roll-column {
    position: sticky;
    left: 0;
    z-index: 11!important;
}

.tableFixHead  th.right-roll-column {
    position: sticky;
    right: -200px;
    z-index: 11!important;
    background:#6c757d !important;
    transition: right 2s;
}

.tableFixHead  td.right-roll-column:hover{
     right:0px;
}


.tableFixHead  td.right-roll-column {
    position: sticky;
    right: -200px;
    z-index: 7!important;
    background: #d9d8df;
}

.tableFixHead thead tr.first-header th.roll-column {
    position: sticky;
    left: 0;
    z-index: 12!important;
}

.tableFixHead thead tr.first-header th{
    background:transparent!important;
}
.tableFixHead thead{
    background:#fff;
    position: sticky;
    top:0;
    z-index: 10;
}

.tableFixHead tbody td.roll-column{
    position: sticky;
    left: 0;
    z-index: 7;
}


.fixed_header tbody {
        display: block;
        width: 100%;
        overflow: auto;
        height: 100px;
      }

 .table-excel td .form-control{
    border-radius:0px!important;

 }
 .table-excel td .form-control{
     box-shadow: none;
 }

.table-excel tbody{
    max-width: 300px;
    overflow: scroll;
}

.custom-file-input1 {
  color: transparent;
}
.custom-file-input1::-webkit-file-upload-button {
  visibility: hidden;
}
.custom-file-input1::before {
  content: 'Select some files';
  color: black;
  display: inline-block;
  background: -webkit-linear-gradient(top, #f9f9f9, #e3e3e3);
  border: 1px solid #999;
  border-radius: 3px;
  padding: 5px 8px;
  width:100%;
  outline: none;
  white-space: nowrap;
  -webkit-user-select: none;
  cursor: pointer;
  text-shadow: 1px 1px #fff;
  font-weight: 700;
  font-size: 10pt;
}
.custom-file-input1:hover::before {
  border-color: black;
}
.custom-file-input1:active {
  outline: 0;
}
.custom-file-input1:active::before {
  background: -webkit-linear-gradient(top, #e3e3e3, #f9f9f9); 
}

</style>
@endpush


<div id="{{$init['component']['id']}}" class="bootstrap-table">
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Delete Data Row @{{modal_delete.row+1}}</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>@{{modal_delete.kodepemda}} - @{{modal_delete.name}}</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" @click="handleDelete(modal_delete.row)">Hapus</button>
            </div>
          </div>
        </div>
      </div>
      <template v-for="(item,row) in data" > 
        <yone-input-form  :datasetpemda="dataset.klasifikasi_1.toUpperCase()=='DATA PEMDA'"  :showindex="desk_data.index" :maxindex="max_index" :columns="columns" :name="'yone-form-input-'+row"  :index_row="row" :data="item"  :showme="deskMode" :addnew="addRow" :optionpemda="singlePemdaList"  />
      </template>

      

    <div class="table-responsive tableFixHead bg-white table-excel" >
        <table class="table table-bordered  " >
            <thead class="thead-dark ">
                <tr class="first-header">
                    <th v-bind:colspan="10" class="bg-white roll-column" style="background: #fff!important">
                           
                            <div class="d-flex">
                                <div class="d-flex p-3">
                                    (@{{row_update.length}}) Data Siap Untuk Dilakukan Perubahan 
                                </div>
                                <div class="d-flex p-2">
                                         <form action="{{$init['map_table']['submit_url']?$init['map_table']['submit_url']:route('mod.dataset.update',['tahun'=>$init['component']['tahun'],'id_dataset'=>$init['component']['id_dataset'],'tw'=>$init['component']['tw']])}}" method="post">
                                            <div class="btn-group">
                                                    <button  @click="addRow()" v-if="data.length==0||pemda==null"  type="button" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> Tambah</button>
                                                    <button  v-if="row_update.length" type="submit" class="btn btn-success btn-xs">Update</button>
                                                    <button  v-if="data.length" type="button" @click="deskMode" class="btn btn-primary bg-navy btn-xs">Desk Mode</button>

                                            </div>
                                           
                                            @method('PUT')
                                            @csrf
                                            
                                            <template v-for="item,key in data_update">
                                                <input type="hidden" v-bind:name="'data['+key+'][tw]'"  v-bind:value="tw">
                                                <input type="hidden" v-bind:name="'data['+key+'][tahun]'"  v-bind:value="tahun">
                                                <template v-for="[kob,ob] of Object.entries(item)">
                                                    <input type="hidden" v-bind:name="'data['+key+']['+kob+']'" v-bind:value="ob">
                                                </template>
                                            </template>
                                        </form>
                                        
                                </div>
                                <div class="flex-grow-1 p-2">
                                    <input type="text" class="form-control" placeholder="Cari Data" v-model="search">
                                </div>
                            </div>
                    
                    </th>
                    <th v-bind:colspan="(columns.length+3)-10"></th>
                </tr>
                <tr class="second-header">
                    <th class="">NO. ROW </th>
                    <th class="" style="min-width:{{$init['component']['def_kodepemda']?100:55}}px;">AKSI </th>
                    <th class="">HANDLE UPDATE </th>
                    <th v-for="c,ck in columns" v-bind:class="(datasetpemda?(c.key=='name'?'roll-column':''):(c.key=='nama_bu'?'roll-column':''))+' '+(c.key=='keterangan'?'right-roll-column':'')" v-bind:style="c.editable?'min-width:300px;':'min-width:100px;'">
                        <p class="text-uppercase">@{{c.label}} @{{c.stuan?(c['satuan']!='-'?'('+(c.satuan)+')':''):''}}</p>
                        <a v-if="(c.editable) && (c.key!='kodepemda')" @click="infoColumn(c)" href="javascript:void(0)" class=" btn btn-sm btn-primary rounded-circle" ><i class="fa fa-info"></i></a>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="item,row in data"  v-if="(item.method!='delete') && (item.display!=0)"  v-bind:class="uuid_last==item.uuid_row?'bg-warning':''">
                    <td class="p-3 ">
                       <b> @{{row+1}}.</b>
                    </td>
                    <td class="d-flex flex-column">
                        <div class="btn-group">
                            <button @click="showModalDelete(row)" type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                            <button @click="deskMode(row)" type="button" class="btn btn-primary bg-navy btn-sm">Desk</button>
                        
                        </div>
                        <div class=" d-flex btn-group">
                            <template v-if="pemda">
                                {{-- <button @click="addRow(row,0)"  type="button" class="btn btn-success btn-sm"><i class="fa fa-arrow-up"></i> Plus</button> --}}
                                <button @click="addRow(row,1)" type="button" class="btn btn-success btn-sm">Plus <i class="fa fa-arrow-down"></i></button>
                               
                            </template>
                        </div>
                        <div class="d-flex btn-group">
                            <template v-if="pemda">
                                <button @click="moveRow(row,-1)" v-if="row!=0"  type="button" class="btn btn-primary btn-sm"><i class="fa fa-arrow-up"></i> Move</button>
                                <button @click="moveRow(row,1)"  v-if="data[row+1]!=undefined" type="button" class="btn btn-primary btn-sm">Move <i class="fa fa-arrow-down"></i></button>
                            </template>
                        </div>
                    </td>
                    <td class="text-center p-2 " >
                        <template v-if="row_update.indexOf(item.uuid_row)>-1">
                            <button type="button" v-if="(item.kodepemda||'').length && item.name" class="btn btn-success btn-sm rounded-circle" disabled><i class="fa fa-check "></i></button>
                            <div  v-if="(item.kodepemda||'') && item.name==null" class="spinner-border text-primary" role="status">
                                <span class="sr-only">Loading...</span>
                              </div>

                        </template>
                    </td>
                    <td  v-bind:class="(datasetpemda?(col.key=='name'?'roll-column':''):(col.key=='nama_bu'?'roll-column':''))+' '+(col.editable!=true?'bg-secondary':'')+' '+(col.key=='keterangan'?'right-roll-column ':'')"  v-for="col,c in columns"  >
                        <input-row :copyrowr="copyRow" :datasetpemda="dataset.klasifikasi_1.toUpperCase()=='DATA PEMDA'" :optionpemda="singlePemdaList"  :index_column="c" :index_row="row" :ajaxw="ajaxw" :data="item" :change-active-cel="changeActiveCell" :column="col" :active_cell_index="active_cell_index" :update-trigger="handleUpdate"/>
                    </td>
                </tr>
                <tr v-if="loading">
                    <td v-bind:colspan="10" class=" p-3 roll-column">
                        <div class="spinner-border text-primary" role="status">
                            <span class="sr-only">Loading...</span>
                          </div>
                          <div class="spinner-border text-secondary" role="status">
                            <span class="sr-only">Loading...</span>
                          </div>
                          <div class="spinner-border text-success" role="status">
                            <span class="sr-only">Loading...</span>
                          </div>
                          <div class="spinner-border text-danger" role="status">
                            <span class="sr-only">Loading...</span>
                          </div>
                    </td>
                   <td v-bind:colspan="(columns.length+3)-10"></td>
                </tr>
            </tbody>
        </table>
    </div>
    <pre id="json">
    </pre>

    <div class="modal fade" id="modal-info-column" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">@{{modal_info_column.label}} (@{{modal_info_column.satuan}})</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            
            
              <p><b>Statistik Dasar</b></p>
              <table class="table table-bordered table-striped">
                  <tr v-for="item,k in modal_info_column.show_stat">
                      <td>
                        <template  v-if="item.p=='bold'" ><b v-html="item.label"></b></template>
                        <template  v-if="item.p!='bold'"  v-html="item.label"></template>
                      </td>
                      <td>
                        @{{item.value}}
                      </td>
                    </tr>

              </table>
              <hr>
              <p><b class="text-success">Definisi/Konsep : </b></p>
              <p>@{{modal_info_column.definisi}}</p>
              <p><b class="text-success">Penangung Jawab : </b>@{{modal_info_column.penangung_jawab}}</p>
              <p><b class="text-success">Tipe Nilai  : </b>@{{modal_info_column.tipe_nilai}}</p>
              <p><b class="text-success">Tipe Pengisian  : </b>@{{modal_info_column.type}}</p>
              <p><b class="text-success">Validator  : </b>@{{modal_info_column.validator}}</p>
              <p><b class="text-success">Data Klasifikasi  : </b>@{{modal_info_column.classification?"YA":'TIDAK'}}</p>

            </div>
            <div class="modal-footer">
              
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade" v-if="url_iframe" v-bind:id="'modal-iframe-row'" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title">@{{url_iframe}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <iframe class="full-width" style="height:40vh;width:100%;" v-bind:src="url_iframe" v-bind:title="url_iframe"></iframe>  
            </div>
        </div>
  
</div>


@push('js_push')
@include('data.admin_partials.input-form')
@include('data.admin_partials.input-row')


  <script>


    var {{$init['component']['id']}}=new Vue({
        el:"#{{$init['component']['id']}}",
        created(){
            this.datasetpemda=this.dataset.klasifikasi_1.toUpperCase()=='DATA PEMDA';
            this.init();
        },
        computed:{
           max_index:function(){
               return this.data.length - 1;
           },
          
        },
        data:{
            datasetpemda:false,
            desk_data:{
                index:-1,
            },
            dataset:<?=json_encode($init['component']['dataset']??[])?>,
            search:'',
            url_iframe:null,
            uuid_last:'',
            pemda_list:<?=json_encode($init['component']['pemda_list'])?>,
            ajaxw:[],
            loading:false,
            curent_page:1,
            filter:'provinsi kab/kota',
            basis:'{{$init['component']['basis']}}',
            tahun:{{$init['component']['tahun']}},
            tw:{{$init['component']['tw']}},
            columns:<?=json_encode($init['map_table']['columns'])?>,
            active_cell_index:['x','x',null],  
            data:[
            ],
            pemda:<?=isset($page_meta['pemda'])?json_encode($page_meta['pemda']):'null'?>,
            data_require_field:['kodepemda','name'],
            row_update:[],
            index_update:[],
            data_update:[],
            modal_delete:{
                kodepemda:'',
                name:'',
                row:0
            },
            modal_info_column:{
                name:'',
                satuan:'',
                definisi:'',
                validator:'',
                producent_data:'',
                type:'',
                tipe_nilai:'',
                key:'',
                bind_view:[],
                file_accept:[],
                classification:false,
                show_stat:[]
            }
        },
        watch:{
           'search':function(val,old){
                this.data.forEach((el,k)=>{
                    console.log(el);
                    if(el.method!='delete'){
                        var val_array=Object.values(el);
                        val_array=val_array.join(' ');
                        if(val_array.toLowerCase().includes(val.toLowerCase())){
                            this.data[k].display=1;
                        }else{
                            this.data[k].display=0;
                        }
                        
                    }
                });
           },
           'active_cell_index':function(val,old){
            if(val[0]!=-1){
                    setTimeout((col,row)=>{
                        $('#input-c-'+col+'-r-'+row).focus();
                    },200,val[0],val[1]);
                }
           }
            
        },
        methods:{
            copyRow:function(row,col){
                var data_def=this.data[row];
                var data={
                    'method':'update',
                    'display':1,
                    'tw':4,
                    'status':0,
                    'uuid_row':this.uuid(),
                    'index':row+1,
                    'kodepemda':null,
                    'kodebu':null,
                    'name':null,
                    'tipe_bantuan':null,
                    'tahun_proyek':null,
                    'regional_1':null,
                    'id':null,
                    'regional_2':null,
                };
                var def={
                    kodepemda:'{{$init['component']['def_kodepemda']}}',
                    name:'{{$init['component']['def_name']}}',
                    tipe_bantuan:'{{$init['component']['def_tipe_bantuan']}}',
                    tahun_proyek:{{$init['component']['def_tahun_proyek']}},
                    regional_1:'{{$init['component']['def_regional_1']}}',
                    regional_2:'{{$init['component']['def_regional_2']}}',
                }
                    
                    for(const [key, value] of Object.entries(def)) {
                        data[key]=value;
                    };

                    this.columns.forEach((el,k)=>{
                    if(el.key){
                        if(data[el.key]==undefined){
                            data[el.key]=(def[el.key]!=undefined?def[el.key]:null);
                        }

                        if(k<=col){
                            if(!(['id','method','status','uuid_row','display','tw','index']).includes(el.key)){
                                data[el.key]=data_def[el.key];
                            }
                        }
                    }
                });

                this.addRow(row,1,data);
            },
            change_index:function(val,old){
                if(this.dataset.jenis_dataset){
                    for(var i=0;i<this.data.length;i++){
                        this.data[i].index=i;
                    } 
                }
               
            },
            singlePemdaList:function(code='',context){
                var self=this;   
                if((!this.dataset.jenis_dataset) && this.dataset.klasifikasi_1.toUpperCase()=='DATA PEMDA'){
                    return this.pemda_list.filter(el=>{
                        var exist=this.data.filter(d=>{
                            return (el.kodepemda==d.kodepemda)?(el.kodepemda==code?false:true):false;
                        });
                        if(exist.length){
                            return false;
                        }else{
                            return true;
                        }
                    });

                }else if((!this.dataset.jenis_dataset) && this.dataset.klasifikasi_1.toUpperCase()=='DATA BUMDAM'){
                    if(code==''){
                        code=='@';
                    }
                    return this.pemda_list.filter(el=>{
                        var kode=code.split('@');
                        var exist=this.data.filter(d=>{
                            return ((el.kodebu==d.kodebu) && (el.kodepemda==d.kodepemda))?(((el.kodebu==kode[0]) && (el.kodepemda==kode[1]))?false:true):false;
                        });
                        if(exist.length){
                            return false;
                        }else{
                            return true;
                        }
                     });

                }else{
                    return this.pemda_list;
                }
               
            },
            deskMode:function(nextIndex=null){
                if(nextIndex!=null){
                    this.desk_data.index=nextIndex;
                    if(this.desk_data.index >= this.data.length){
                        this.desk_data.index=(this.data.length - 1);
                    }
                }

                console.log('request',this.desk_data.index);

                if(this.data[this.desk_data.index]!=undefined){
                    $('.modal').modal('hide');
                    $('#modal-form-yone-'+this.desk_data.index).modal();

                }else{
                     if(this.desk_data.index>=0){
                         $('.modal').modal('hide');
                         $('#modal-form-yone-'+this.desk_data.index).modal();

                    }else{
                        this.desk_data.index=0;
                        this.deskMode();
                    }
                }
            },
            updateMeta:function(data){
                
            },
            showIframe:function(url){
                this.url_iframe=url;
                $('#modal-iframe-row').modal();
            },
            changeActiveCell:function(col,row,key=null){
                this.activeCell(col,row,key);
            },
            infoColumn:function(c){
                this.modal_info_column.label=c.label;
                this.modal_info_column.satuan=c.satuan;
                this.modal_info_column.definisi=c.definisi_konsep;
                this.modal_info_column.type=c.type;
                this.modal_info_column.key=c.key;
                this.modal_info_column.tipe_nilai=c.tipe_nilai;
                this.modal_info_column.validator=c['validator']||'';
                this.modal_info_column.classification=c['classification']||false;
                this.modal_info_column.producent_data=c['producent_data']||'';
                this.modal_info_column.bind_view=c['bind_view']||[];
                this.modal_info_column.show_stat=[];
                var self=this;
                var value_list=this.data.map(el=>{
                    if( el[c.key]!=null){
                        return c.tipe_nilai=='numeric'?parseFloat(el[c.key]||0):el[c.key];
                    }else{
                        return null;
                    }
                });
                value_list=value_list.filter((el)=>{
                    return el!=null;
                })

                if(c.tipe_nilai=='numeric' && !(c['classification']!=undefined?c['classification']:false)){
                    var state=[
                        {
                            label:'Nilai Sum',
                            p:'bold',
                            value:this.formatAngka(this.getSum(value_list))

                        },
                        {
                            label:'Nilai Max',
                            p:'bold',

                            value:this.formatAngka(this.getMax(value_list))

                        },
                        {
                            label:'Nilai Min',
                            p:'bold',
                            value:this.formatAngka(this.getMin(value_list))

                        },
                        {
                            label:'Nilai Rata Rata',
                            p:'bold',
                            value:this.formatAngka(this.getAverage(value_list))

                        },
                        {
                            label:'Nilai Median',
                            p:'bold',
                            value:this.formatAngka(this.getMedian(value_list))

                        },
                        
                    ];
                    this.modal_info_column.show_stat=state;

                }else{
                    if(['single_file','multy_file'].includes(c.type)){

                    }else{

                        var Frequency=this.getFrequencies(value_list);
                        var state=[
                            {
                                label:'Frequency Nilai',
                                p:'bold',
                                value:null

                            }
                        ];
                        Frequency.forEach(el=>{
                            state.push({
                                label:'Frequensi '+(c['bind_view']!=undefined?self.bind_value(c['bind_view'],el.label):el.label),
                                p:'',
                                value:this.formatAngka(el.value)
                            })
                        });
                         this.modal_info_column.show_stat=state;



                    }


                    
                }

                $('#modal-info-column').modal();
            },
            getFrequencies:function(arr) {
            let obj = {};
            for (let i = 0; i < arr.length; i++) {
                let element = arr[i];
                if (obj['#kkk#'+element] !== undefined) {
                obj['#kkk#'+element]['value']+=1;
                }
                else {
                obj['#kkk#'+element] = {
                    value:1,
                    label:element,
                    'p':''    
                };
                }
             }
                return Object.values(obj);
            },
            getAverage:function(arr){
                var sum=this.getSum(arr);
                return sum/arr.length;
                
            },
            getSum:function(arr){
                var sum=0;
                arr.forEach(el=>{
                    sum+=parseFloat(el);
                });
                return sum;
            },
            getMin:function(arr){
                return Math.min(...arr);
            },
            getMax:function(arr){
                return Math.max(...arr);
            },
            getMedian:function(arr){
                let middle = Math.floor(arr.length / 2);
                 arr = [...arr].sort((a, b) => a - b);
                return arr.length % 2 !== 0 ? arr[middle] : (arr[middle - 1] + arr[middle]) / 2;
            },
            moveRow:function(pointer,position){
                const toIndex = (pointer+position)<0?0:(pointer+position);
                const element = this.data.splice(pointer, 1)[0];
                this.data.splice(toIndex, 0, element);
                this.uuid_last=this.data[toIndex].uuid_row;
                index_update=this.row_update.indexOf(this.uuid_last);
                if(index_update>-1){
                    this.data_update[index_update].index=toIndex;
                }
            },
            uuid:function(){
                var dt = new Date().getTime();
                var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                    var r = (dt + Math.random()*16)%16 | 0;
                    dt = Math.floor(dt/16);
                    return (c=='x' ? r :(r&0x3|0x8)).toString(16);
                });
                return uuid;

            },
            activeCell:function(row,column,key=null){
                this.active_cell_index=[row,column,key];
            },
            init:function(){
                this.data=[];
                this.curent_page=1;
                this.load();
            },
            load:function(){    
                var self=this;
                this.loading=true;
                req_ajax.post('{{$init['map_table']['ajax_data']}}',{
                    basis:this.basis,
                    filter:this.filter,
                    page:this.curent_page,
                    kodepemda:this.pemda?this.pemda.kodepemda:null
                }).then((res)=>{
                    self.loading=false;
                    self.curent_page=res.data.curent_page;
                    res.data.data.forEach((el,rkey) => {
                        el.method='none';
                        el.display=1;
                        el.tahun=self.tahun;
                        el.tw=self.tw;
                        el['uuid_row']=self.uuid();
                        el.index=((self.curent_page-1)*res.data.pagiante_count)+rkey;
                        self.data.push(el);
                    });

                    console.log(self.curent_page,res.data.last_page);
                    if(self.curent_page<res.data.last_page){
                        self.curent_page+=1;
                        self.load();
                    }else{
                        self.loading=false;
                    }
                });

            },
            react:function(data){
                if(data.method=='filter_change'){
                    this.filter=data.data.filter;
                    this.init();
                }

            },
            bind_value:function(bind_value,val){
                var val_='0';
                   
                    var index=-1;
                    if(val!=null){
                       if(bind_value.length){
                        do{
                            index+=1;
                            if(bind_value.length<=index){
                                index-=1;
                                logic=true;
                            }else{
                                logic_=bind_value[index].logi.replace(/#val/g,'val_xxx');
                                var val_xxx=val;
                                logic=eval(logic_);
                                
                            }
                        }while((logic!=true));

                          if(bind_value[index]!=undefined){
                            val_=bind_value[index]['tag'];
                          }
                            val_=val_.replace(/\[val\]/g,val);
                       }else{
                           val_=val;
                       }

                    }else{
                        val_=val||'';
                    }
                    
                  
                    return val_;
                
            },
            formatAngka:function(angka){
                var number_string = (angka+'').replace(/[^,\d]/g, '').toString(),
                split   		= number_string.split(','),
                sisa     		= split[0].length % 3,
                rupiah     		= split[0].substr(0, sisa),
                ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
    
                // tambahkan titik jika yang di input sudah menjadi angka ribuan
                if(ribuan){
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                }
    
                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                return rupiah;
             },
            addRow:function(pointer=null,add_index=null,def_data=null){
                if(def_data){
                    var data=def_data;
                }else{
                    var data={
                    'method':'none',
                    'display':1,
                    'tw':4,
                    'status':0,
                    'uuid_row':this.uuid(),
                    'index':0,
                    'kodepemda':null,
                    'kodebu':null,
                    'name':null,
                    'tipe_bantuan':null,
                    'tahun_proyek':null,
                    'regional_1':null,
                    'regional_2':null,
                    
                };
                var def={
                    kodepemda:'{{$init['component']['def_kodepemda']}}',
                    name:'{{$init['component']['def_name']}}',
                    tipe_bantuan:'{{$init['component']['def_tipe_bantuan']}}',
                    tahun_proyek:{{$init['component']['def_tahun_proyek']}},
                    regional_1:'{{$init['component']['def_regional_1']}}',
                    regional_2:'{{$init['component']['def_regional_2']}}',
                }
                for(const [key, value] of Object.entries(def)) {
                        data[key]=value;
                    };

                this.columns.forEach(el=>{
                    if(el.key){
                        if(data[el.key]==undefined){
                            data[el.key]=(def[el.key]!=undefined?def[el.key]:null);
                        }
                    }
                });
                }
                if(pointer!=null){
                    var index_request=(pointer+add_index)<0?0:(pointer+add_index);
                    data.index=index_request;
                    this.data.splice(index_request,0,data);
                     this.uuid_last=this.data[index_request].uuid_row;

                }else{
                 data.index=this.data.length;
                 this.data.push(data);
                 this.uuid_last=data.uuid_row;


                }
                this.change_index();
            },
            showModalDelete:function(index){
                if(this.data[index]!=undefined){
                    var data=this.data[index];
                    this.modal_delete.kodepemda=data.kodepemda;
                    this.modal_delete.name=data.name;
                    this.modal_delete.row=index;
                    $('#modal-delete').modal();
                }
            },
            handleDelete:function(index){
                if(this.data[index]!=undefined){
                    var uuid=this.data[index].uuid_row;
                    if(this.data[index].id==null){
                        var index_row=this.row_update.indexOf(uuid);
                        if(index_row>-1){
                            this.row_update.splice(index_row,1);
                            this.data_update.splice(index_row,1);
                        }
                        this.data.splice(index,1);
                    }else{
                        this.data[index].method='delete';
                        var index_row=this.row_update.indexOf(uuid);
                        if(index_row>-1){
                            this.data_update[index_row].method='delete';
                        }else{
                            this.row_update.push(uuid);
                            this.data_update.push(this.data[index]);
                        }
                        
                    }
                    $('#modal-delete').modal('hide');
                    this.change_index();

                }
            },
            handleUpdate:function(data,uuid,old=null,index_row=-1){
                console.log(data,index_row);
            var index_update=this.row_update.indexOf(uuid);
              if(data.method!='delete'&&data.method!='none'){
                 

                var validator_dum=true;
                    this.data_require_field.forEach(el=>{
                    validator_dum=validator_dum&&(!(['',0,null].includes(data[el])));
                });
                self=this;
               
                var require_v_editable=this.columns.map(function(el){
                    if(el.editable){
                        if(el.key!='status'){
                            return el.key;
                        }
                    }
                });

                var validator_data=false;

                require_v_editable.forEach(el=>{
                   if(!self.data_require_field.includes(el)){
                       if(data[el]!=undefined){
                            if(data[el]!=null||data[el]!=''){
                                validator_data=true;
                            }
                       }
                   }
                });

                if(validator_data==false&&validator_dum==true){
                    if(data.id){
                        var exist=false;
                        var index_exist=0;
                        this.index_update.forEach(el=>{
                            if(el.uuid==uuid){
                                exist=true;
                            }else{
                                index_exist+=1;
                            }
                        });
                        if(exist){
                            this.index_update[index_exist]={
                                uuid:uuid,
                                id:data.id,
                                index:data.index
                            }
                        }else{
                            this.index_update.push({
                                uuid:uuid,
                                id:data.id,
                                index:data.index
                            });

                        }
                            
                    }else{
                        var index_exist=this.row_update.indexOf(uuid);
                        if(index_exist>-1){
                            this.data_update[index_exist].index=data.index;
                        }
                        
                        

                    }
                
                }

              

                if(validator_dum&&validator_data){
                    if(index_update>-1){
                        this.data_update[index_update]=data;
                    }else{
                        this.data_update.push(data);
                        this.row_update.push(uuid);
                    }
                }else{
                    if(index_update>-1){
                        this.data_update.splice(index_update,1);
                        this.row_update.splice(index_update,1);
                    }else{

                    }
                    
                }
              }else{
                if(index_update>-1){
                    this.data_update.splice(index_update,1);
                    this.row_update.splice(index_update,1);
                }else{

                }


              }
              this.change_index();

            },
            
        }
    });

  </script>



@endpush