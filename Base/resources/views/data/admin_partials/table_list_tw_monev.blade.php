@php
    $init=[
        'component'=>[
            'name'=>isset($init['component']['name'])?$init['component']['name']:'',
            'id'=>isset($init['component']['id'])?$init['component']['id']:'table_list',
            'basis'=>isset($init['component']['basis'])?$init['component']['basis']:'NASIONAL',
            'nasional'=>isset($init['component']['nasional'])?$init['component']['nasional']:false

            
        ],
        'map_table'=>[
            'monev'=>isset($init['map_table']['monev'])?$init['map_table']['monev']:false,
            'ajax_data'=>isset($init['map_table']['ajax_data'])?$init['map_table']['ajax_data']:''
        ]
    ]

@endphp


<div id="{{$init['component']['id']}}" class="p-3">
    <div v-if="loading" class=" ">
        <div class="spinner-border text-primary" role="status">
            <span class="sr-only">Loading...</span>
          </div>
          <div class="spinner-border text-secondary" role="status">
            <span class="sr-only">Loading...</span>
          </div>
          <div class="spinner-border text-success" role="status">
            <span class="sr-only">Loading...</span>
          </div>
          <div class="spinner-border text-danger" role="status">
            <span class="sr-only">Loading...</span>
          </div>
          <div class="spinner-border text-warning" role="status">
            <span class="sr-only">Loading...</span>
          </div>
          <div class="spinner-border text-info" role="status">
            <span class="sr-only">Loading...</span>
          </div>
    <hr>

    </div>
    <div  class="table-responsive">
        <table class="table table-bordered table-striped" id="table-show">
            <thead class="thead-dark">
                <tr>
                    <th rowspan="2">Kode</th>
                    <th rowspan="2">Nama Dataset</th>
                    <th rowspan="2">Penangung Jawab</th>
                    <th colspan="3" class="bg-primary">Status Data</th>
                </tr>
                <tr>
                    <th>
                        Pemda Terdata
                    </th>
                    <th>
                        Pemda Terverifikasi
                    </th>
                    <th>
                        Aksi
                    </th>
                   
                </tr>
                <tr>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>4</th>
                    <th>5</th>
                    <th>6</th>

                  



                </tr>
            </thead>
            <tbody>
             
            </tbody>
        </table>
    </div>
   
</div>


@push('js_push')
<script>

   

    var table_show=null;
    
    var {{$init['component']['id']}}=new Vue({
        el:'#{{$init['component']['id']}}',
        data:{
            loading:false,
            ref_datatable:null,
            filter:'provinsi kab / kota',
            data:[],
            current_page:1,
            basis:'{{$init['component']['basis']}}'
        },
        created:function(){
            this.init();
        
        },
        watch:{
           
        },
        methods:{
            drawTable:function(){
                if(window.table_show){
                    window.table_show.clear().draw();
                    window.table_show.rows.add(this.data);
                    window.table_show.draw();
                }
                
            },
            init:function(){
                this.current_page=1;
                this.data=[];
                self=this;
                setTimeout(function(a){
                    a.load();
                    window.table_show=$('#table-show').DataTable({
                        columns:[
                            {
                                data:'id',
                                
                            },
                            {

                                data:'name',
                                
                            },
                            {
                                data:'penangung_jawab',
                                
                            },
                           
                            {
                            data:'keterisian.tw_4.terdata'
                                
                            },
                            {
                            data:'keterisian.tw_4.terverifikasi'
                                
                            },
                            {
                            data:'keterisian.tw_4.link',
                            type:'html',
                            render:function(data, type, row, meta){
                                return '<a href="'+data+'" class="btn btn-success btn-sm"><i class="fa fa-arrow-right"></a>';
                            }
                                
                            },
                        
                        ]
                });
                a.drawTable();
                },1000,self);
            },
            // load:function(){
            //     this.loading=true;
            //     var self=this;
            //     req_ajax.post('{{$init['map_table']['ajax_data']}}',{
            //         filter:this.basis,
            //         basis:this.basis,
            //         page:this.current_page,
            //         meta:{{$init['component']['nasional']?'true':'false'}},
            //     }).then(res=>{
            //          self.loading=false;
            //          self.data=res.data.data;
            //          if(self.current_page!=res.data.last_page){
            //              self.current_page+=1;
            //              self.load_next();
            //          }
            //     });
            // },
            load:function(){
                var self=this;
                this.loading=true;
                req_ajax.post('{{$init['map_table']['ajax_data']}}',{
                    filter:this.basis,
                    basis:this.basis,
                    page:this.current_page,
                    meta:{{$init['component']['nasional']?'true':'false'}},
                }).then(res=>{
                    self.loading=false;
                    res.data.data.forEach(element => {
                        self.data.push(element);  
                    });
                    self.drawTable();
                     if(self.current_page!=res.data.last_page){
                         self.current_page+=1;
                         self.load();
                     }
                });

            }
        }
    });


 </script>
@endpush