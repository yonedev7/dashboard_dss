@extends('adminlte::page')


@section('content')
<script>
    var page_meta=<?=json_encode($page_meta)?>;
</script>
<div class="parallax"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
    <div class="container">
        <section >
            <h3><b>{{$page_meta['title']}}</b></h3>
            <p><b>{!!$page_meta['keterangan']!!}</b></p>
           
        </section>
      
    </div>
   
</div>
@include('tematik.filter')
@include('data.partials.filter_jenis_daerah',['init'=>[
    'component'=>[
        'id'=>'filter_bispan',
        'name'=>$page_meta['title'],
        'target_react'=>['l1'],
        'basis'=>$basis,
    ],
   
]])
@include('data.partials.map_prov_dan_kabkot',['init'=>[
    'component'=>[
        'id'=>'l1',
        'name'=>$page_meta['title'],
        'basis'=>$basis,
        'width'=>12
    ],
    'map_provinsi'=>[
        'ajax_data'=>route('api-web.public.data.renbis.load_provinsi',['tahun'=>$StahunRoute]),
        'legenda'=>$color_keterisisan,
        'target_react'=>['l1']
    ],
    'map_kabkot'=>[
        'ajax_data'=>route('api-web.public.data.renbis.load_kota',['tahun'=>$StahunRoute]),
        'legenda'=>[],
        'target_react'=>['table_data']
    ]
   
]]);

@include('data.partials.table_data',['init'=>[
    'component'=>[
        'id'=>'table_data',
        'name'=>$page_meta['title'],
        'target_react'=>['l1'],
        'basis'=>$basis,
    ],
   
]])
@stop