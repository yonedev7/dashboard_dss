<div class="row no-gutters" style="z-index: 0;">
    <div class="col-4 {{strtoupper($basis)=='NASIONAL'?'bg-secondary':' bg-primary'}} p-3 pt-5" >
        <h4><b>NASIONAL</b></h4>
        <a href="{{url()->current().'?basis=nasional&meta='.(isset($_GET['meta'])?$_GET['meta']:'')}}" class="btn btn-success"><i class="fa fa-arrow-down"></i></a>
    </div>
    <div class="col-4  {{strtoupper($basis)=='LONGLIST'?'bg-secondary efault':' bg-success'}} p-3 pt-5">
        <h4><b>LONGLIST</b></h4>
        <a  href="{{url()->current().'?basis=longlist&meta='.(isset($_GET['meta'])?$_GET['meta']:'')}}" class="btn btn-warning text-dark"><i class="fa fa-arrow-down text-dark"></i></a>

    </div>
    <div class="col-4  {{strtoupper($basis)=='SORTLIST'?'bg-secondary':' bg-warning'}} p-3 pt-5">
        <h4><b>SORTLIST {{$Stahun}}</b></h4>
        <a  href="{{url()->current().'?basis=sortlist&meta='.(isset($_GET['meta'])?$_GET['meta']:'')}}" class="btn btn-primary text-white"><i class="fa fa-arrow-down text-white"></i></a>
    </div>
 
</div>