@php
    $init=[
        'component'=>[
            'id'=>isset($init['component']['id'])?$init['component']['id']:'filter',
            'target_react'=>isset($init['component']['target_react'])?$init['component']['target_react']:[],

        ]
];
@endphp
<div id="{{$init['component']['id']}}">
    <div>
        <div class="form-group p-3" >
        <div class="col-4">
            <label for="">Filter</label>
            <select name="" class="form-control" id="" v-model="filter">
                <option value="provinsi dan kota / kab">Semua</option>
                <option value="provinsi">Provinsi</option>
                <option value="kota">Kota</option>
                <option value="kab">Kab</option>
            </select>
        </div>
        </div>
    </div>

    <div style="min-height:20px;" class="bg-dark text-center p-1"><b>BASIS DATA {{strtoupper($basis)}} - (PEMDA @{{filter.toUpperCase()}}) TAHUN {{$StahunRoute}}</b></div>
</div>


@push('js_push')

<script>
 var {{$init['component']['id']}} =new Vue({
        el:'#{{$init['component']['id']}}',
        data:{
            filter:'provinsi dan kota / kab',
            target_react:<?=json_encode($init['component']['target_react'],true)?>
        },
        watch:{
            "filter":function(val){
                var self=this;
                setTimeout(function(self){
                 self.target_react.forEach(element => {
                    window[element].react({
                        'component':'{{$init['component']['id']}}',
                        'method':'filter_change',
                        'data':{
                            'filter':self.filter

                        }    
                    });
                 });
                },500,self);

            }
        }
    });

</script>
@endpush