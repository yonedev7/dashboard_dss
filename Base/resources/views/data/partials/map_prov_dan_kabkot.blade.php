@php
$init=[
    'component'=>[
        'id'=>isset($init['component']['id'])?$init['component']['id']:'APP_'.(int)rand(1,100),
        'name'=>isset($init['component']['name'])?$init['component']['name']:'',
        'tahun'=>isset($init['component']['tahun'])?$init['component']['tahun']:date('Y'),
        'width'=>isset($init['component']['width'])?$init['component']['width']:12,
        'basis'=>isset($init['component']['basis'])?$init['component']['basis']:'nasional'

    ],
    'map_provinsi'=>[
        'legenda'=>isset($init['map_provinsi']['legenda'])?$init['map_provinsi']['legenda']:[],
        'ajax_data'=>isset($init['map_provinsi']['ajax_data'])?$init['map_provinsi']['ajax_data']:'',
    ],
    'map_kabkot'=>[
        'legenda'=>isset($init['map_kabkot']['legenda'])?$init['map_kabkot']['legenda']:[],
        'ajax_data'=>isset($init['map_kabkot']['ajax_data'])?$init['map_kabkot']['ajax_data']:'',
        'target_react'=>isset($init['map_kabkot']['target_react'])?$init['map_kabkot']['target_react']:'',
    ],
    'initial'=>[
    ]
];



@endphp

<div id="{{$init['component']['id']}}" class="full-width">
    <div class="card parallax mb-0"  style=" background-image: url('{{url('assets/img/back-w2.gif')}}');">
        <div class="card-body parallax" style="backdrop-filter:blur(1px);">
            <div class="row no-gutter">
                <div v-bind:class="chart_kota.title.text?'col-6':'col-12'">
                    <charts :constructor-type="'mapChart'" :ref="ref_one"  :options="chart_provinsi" :callback="chartCallback"></charts>
                    @if($init['map_provinsi']['legenda'])
                    <div class="card rounded-0" >
                        <div class="card-body">
                            <ul class="list-inline">
                                @foreach ($init['map_provinsi']['legenda'] as $key=> $c)
                                <li class="list-inline-item">
                                    <i class="fa fa-circle" style="color:{{$c}}"></i> {{$key}}
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif
                </div>
                <div v-if="chart_kota.title.text" class="col-6">
                    <charts  :constructor-type="'mapChart'" class="" :options="chart_kota"></charts>
                    @if($init['map_kabkot']['legenda'])

                        <div class="card rounded-0" >
                            <div class="card-body">
                                <ul class="list-inline">
                                    @foreach ($init['map_kabkot']['legenda'] as $key=> $c)
                                    <li class="list-inline-item">
                                        <i class="fa fa-circle" style="color:{{$c}}"></i> {{$key}}
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                     @endif

                </div>
            </div>
        </div>
    </div>
</div>

@push('js_push')
<script>

var {{$init['component']['id']}}=new Vue({
        el:'#{{$init['component']['id']}}',
        created:function(){
            this.init();
        },
        computed:{
            getTitle:function(){
                return '{{($init['component']['name'])}}'.toUpperCase();
            }
        },
        methods:{
            react:function(data){
                if(data.method=='filter_change'){
                    console.log(data.component,'--',data.method);
                    console.log(data.data);
                    this.filter=data.data.filter;
                    this.chart_kota.subtitle.text=this.filter+' - Tahun '+this.tahun;
                    this.chart_provinsi.subtitle.text=this.filter+' - Tahun '+this.tahun;
                    this.init();
                }
            },
            detail_per_kota:function(kodepemda,name){
                req_ajax.post(('{{$init['map_kabkot']['ajax_data']}}').replace('xxxtahunxxx','{{$init['component']['tahun']}}'),{
                    basis:this.basis,
                    filter:this.filter,
                    kodepemda:kodepemda
                }).then((res)=>{
                    {{$init['component']['id']}}.chart_kota.title.text=''+(page_meta.title).toUpperCase()+' '+name+' BASIS '+l1.basis.toUpperCase();

                    {{$init['component']['id']}}.table.name=page_meta.title+' '+name+' BASIS '+l1.basis.toUpperCase();
                    {{$init['component']['id']}}.chart_kota.series[0].data=[{
                        id:parseInt(kodepemda),
                        value:1,
                        color:'#fff'
                        
                    }];
                    {{$init['component']['id']}}.chart_kota.series[1].data=(res.data.data);
                   
                    setTimeout(function(){
                        {{$init['component']['id']}}.ref_one.reflow();

                    },600);

                    var self=this;
                        setTimeout(function(self,res){
                            console.log(self,res);
                        self.target_react_kabkot.forEach(element => {
                            window[element].react({
                                'component':'{{$init['component']['id']}}',
                                'method':'build_table',
                                'data':{
                                    'column':(res.data.column),
                                    'data':(res.data.data),


                                }    
                            });
                        });
                        },500,self,res);

                });

            },
            chartCallback:function(event){
                this.ref_one=event;
                this.ref_one.reflow();
            },
            formatAngka:function(angka){
                var number_string = (angka+'').toString(),
                split   		= number_string.split('.'),
                sisa     		= split[0].length % 3,
                rupiah     		= split[0].substr(0, sisa),
                ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
                // tambahkan titik jika yang di input sudah menjadi angka ribuan
                if(ribuan){
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                }

    
                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;

                return rupiah;

            },
            init:function(){
                this.chart_kota.title.text='';
                this.chart_provinsi.subtitle.text=this.filter.toUpperCase()+' - TAHUN {{$StahunRoute}}';
                this.chart_kota.subtitle.text=this.filter.toUpperCase()+' - TAHUN {{$StahunRoute}}';

                req_ajax.post(('{{$init['map_provinsi']['ajax_data']}}').replace('xxxtahunxxx','{{$init['component']['tahun']}}'),{
                    basis:this.basis,
                    filter:this.filter
                }).then((res)=>{
                    {{$init['component']['id']}}.chart_provinsi.series[0].data=res.data;
                });
                this.chart_provinsi.title.text='KETERISIAN '+(page_meta.title.toUpperCase())+' BASIS '+this.basis.toUpperCase();
            }
        },
        data:{
            ref_one:{},
            filter:'provinsi dan kota / kab',
            basis:'{{$init['component']['basis']}}',
            tahun:'{{$init['component']['tahun']}}',
            table:{
                title:'',
                data:[]
            },
            
            chart_kota:{
                colors:[],
                title:{
                    text:'KETERISIAN  '+('{{($init['component']['name'])}}').toUpperCase()+'  PER PROVINSI'
                },
                subtitle:{
                    text:'provinsi dan kota / kab Tahun {{$StahunRoute}}'
                },
                chart:{
                    height:500,
                    backgroundColor:'#ffffff00'
                },
                tooltip: {
                    formatter:function(){
                        return this.point.text?'<b>'+this.point.name+'</b><br>'+this.point.text:'<b>'+this.point.name+'</b>';
                    }
                },
             
                series:[
                    {
                        name:'KETERISIAN PER PROVINSI',
                        data:[],
                        type:'map',
                        joinBy:['id','id'],
                        mapData:Highcharts.maps['ind'],
                        allAreas:false,
                        dataLabels: {
                            enabled: false,
                            format: '{point.text} '
                        },
                        tooltip: {
                           enabled:false
                        },
                        point:{
                            events:{
                               
                            }
                        },
                        showInLegend:false,
                        data:[]
                    },
                    {
                        name:'KETERISIAN PER KOTA/KAB',
                        data:[],
                        type:'map',
                        joinBy:['id','id'],
                        mapData:Highcharts.maps['ind_kab'],
                        allAreas:false,
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}'
                        },
                        
                        point:{
                            events:{
                               
                            }
                        },
                        showInLegend:false,
                        data:[]
                    },
                ]
            },
            target_react_kabkot:<?=json_encode($init['map_kabkot']['target_react'])?>,
            chart_provinsi:{
                ref:null,
                title:{
                    text:'KETERISIAN '+('{{($init['component']['name'])}}').toUpperCase()+' PER PROVINSI'
                },
                subtitle:{
                    text:'provinsi dan kota / kab Tahun {{$StahunRoute}}'
                },
                chart:{
                    height:500,
                    backgroundColor:'#ffffff00'

                },
                tooltip: {
                    enabled:true,
                    formatter:function(){
                        return this.point.name+': <b>'+this.point.value+'/'+this.point.jumlah_pemda+'</b> PEMDA <br> Persentase : '+this.point.persentase.toFixed(2)+'%';
                    }
                },
                series:[
                    {
                        name:'KETERISIAN PER PROVINSI',
                        data:[],
                        type:'map',
                        joinBy:['id','id'],
                        mapData:Highcharts.maps['ind'],
                        dataLabels: {
                            enabled: true,
                            format: '{point.name} : <br>{point.value} Pemda'
                        },
                        point:{
                            events:{
                                click:function(){
                                    {{$init['component']['id']}}.detail_per_kota(this.kodepemda,this.name);
                                }
                            }
                        },
                        showInLegend:false,
                        data:[]
                    }
                ]
            }
         
         
        }
    });
    

</script>
@endpush