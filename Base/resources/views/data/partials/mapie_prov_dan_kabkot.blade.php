@php
$init=[
    'component'=>[
        'id'=>isset($init['component']['id'])?$init['component']['id']:'APP_'.(int)rand(1,100),
        'name'=>isset($init['component']['name'])?$init['component']['name']:'',
        'tahun'=>isset($init['component']['tahun'])?$init['component']['tahun']:date('Y'),
        'width'=>isset($init['component']['width'])?$init['component']['width']:12,
        'basis'=>isset($init['component']['basis'])?$init['component']['basis']:'nasional'

    ],
    'map_provinsi'=>[
        'legenda'=>isset($init['map_provinsi']['legenda'])?$init['map_provinsi']['legenda']:[],
        'ajax_data'=>isset($init['map_provinsi']['ajax_data'])?$init['map_provinsi']['ajax_data']:'',
        'pie'=>[
            'column'=>isset($init['map_provinsi']['pie']['column'])?$init['map_provinsi']['pie']['column']:[]
        ]
    ],
    'map_kabkot'=>[
        'legenda'=>isset($init['map_kabkot']['legenda'])?$init['map_kabkot']['legenda']:[],
        'ajax_data'=>isset($init['map_kabkot']['ajax_data'])?$init['map_kabkot']['ajax_data']:'',
        'target_react'=>isset($init['map_kabkot']['target_react'])?$init['map_kabkot']['target_react']:'',
        'pie'=>[
            'column'=>isset($init['map_kabkot']['ajax_data']['pie'])?$init['map_kabkot']['ajax_data']['pie']:''
        ]
    ],
    'initial'=>[
    ]
];



@endphp

<div id="{{$init['component']['id']}}" class="full-width">
    <div class="card parallax mb-0"  style=" background-image: url('{{url('assets/img/back-w2.gif')}}');">
        <div class="card-body parallax" style="backdrop-filter:blur(1px);">
            <div class="row no-gutter">
                <div v-bind:class="chart_kota.title.text?'col-6':'col-12'">
                    <charts :constructor-type="'mapChart'" :ref="ref_one"  :options="chart_provinsi" :callback="chartCallback"></charts>
                    @if($init['map_provinsi']['legenda'])
                    <div class="card rounded-0" >
                        <div class="card-body">
                            <ul class="list-inline">
                                @foreach ($init['map_provinsi']['legenda'] as $key=> $c)
                                <li class="list-inline-item">
                                    <i class="fa fa-circle" style="color:{{$c}}"></i> {{$key}}
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif
                </div>
                <div v-if="chart_kota.title.text" class="col-6">
                    <charts  :constructor-type="'mapChart'" class="" :options="chart_kota"></charts>
                    @if($init['map_kabkot']['legenda'])

                        <div class="card rounded-0" >
                            <div class="card-body">
                                <ul class="list-inline">
                                    @foreach ($init['map_kabkot']['legenda'] as $key=> $c)
                                    <li class="list-inline-item">
                                        <i class="fa fa-circle" style="color:{{$c}}"></i> {{$key}}
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                     @endif

                </div>
            </div>
        </div>
    </div>
</div>

@push('js_push')
<script>

Highcharts.seriesType('mappie', 'pie', {
        center: null, // Can't be array by default anymore
        states: {
            hover: {
                halo: {
                    size: 5
                }
            }
        },
        linkedMap: null, // id of linked map
        dataLabels: {
            enabled: false
        }
    }, {
        init: function () {
            Highcharts.Series.prototype.init.apply(this, arguments);
            // Respond to zooming and dragging the base map
            Highcharts.addEvent(this.chart.mapView, 'afterSetView', () => {
                this.isDirty = true;
            });
        },
        render: function () {
            const series = this,
                chart = series.chart,
                linkedSeries = chart.get(series.options.linkedMap);
            Highcharts.seriesTypes.pie.prototype.render.apply(
                series,
                arguments
            );
            if (series.group && linkedSeries && linkedSeries.is('map')) {
                series.group.add(linkedSeries.group);
            }
        },
        getCenter: function () {
            const options = this.options,
                chart = this.chart,
                slicingRoom = 2 * (options.slicedOffset || 0);
            if (!options.center) {
                options.center = [null, null]; // Do the default here instead
            }
            // Handle lat/lon support
            if (options.center.lat !== undefined) {
                const projectedPos = chart.fromLatLonToPoint(options.center),
                    pixelPos = chart.mapView.projectedUnitsToPixels(
                        projectedPos
                    );

                options.center = [pixelPos.x, pixelPos.y];
            }
            // Handle dynamic size
            if (options.sizeFormatter) {
                options.size = options.sizeFormatter.call(this);
            }
            // Call parent function
            const result = Highcharts.seriesTypes.pie.prototype.getCenter
                .call(this);
            // Must correct for slicing room to get exact pixel pos
            result[0] -= slicingRoom;
            result[1] -= slicingRoom;
            return result;
        },
        translate: function (p) {
            this.options.center = this.userOptions.center;
            this.center = this.getCenter();
            return Highcharts.seriesTypes.pie.prototype.translate.call(this, p);
        }
    });

var {{$init['component']['id']}}=new Vue({
        el:'#{{$init['component']['id']}}',
        created:function(){
            this.init();
        },
        computed:{
            getTitle:function(){
                return '{{($init['component']['name'])}}'.toUpperCase();
            }
        },
        methods:{
            react:function(data){
                if(data.method=='filter_change'){
                    console.log(data.component,'--',data.method);
                    console.log(data.data);
                    this.filter=data.data.filter;
                    this.chart_kota.subtitle.text=this.filter+' - Tahun '+this.tahun;
                    this.chart_provinsi.subtitle.text=this.filter+' - Tahun '+this.tahun;
                    this.init();
                }
            },
            detail_per_kota:function(kodepemda,name){
                req_ajax.post(('{{$init['map_kabkot']['ajax_data']}}').replace('xxxtahunxxx','{{$init['component']['tahun']}}'),{
                    basis:this.basis,
                    filter:this.filter,
                    kodepemda:kodepemda
                }).then((res)=>{
                    {{$init['component']['id']}}.chart_kota.title.text=''+page_meta.title+' '+name+' BASIS '+l1.basis.toUpperCase();

                    {{$init['component']['id']}}.table.name=page_meta.title+' '+name+' BASIS '+l1.basis.toUpperCase();
                    {{$init['component']['id']}}.chart_kota.series[0].data=[{
                        id:parseInt(kodepemda),
                        value:1,
                        color:'#fff'
                        
                    }];
                    {{$init['component']['id']}}.chart_kota.series[1].data=(res.data.data);
                   
                    setTimeout(function(){
                        {{$init['component']['id']}}.ref_one.reflow();

                    },600);

                    var self=this;
                        setTimeout(function(self,res){
                            console.log(self,res);
                        self.target_react_kabkot.forEach(element => {
                            window[element].react({
                                'component':'{{$init['component']['id']}}',
                                'method':'build_table',
                                'data':{
                                    'column':(res.data.column),
                                    'data':(res.data.data),


                                }    
                            });
                        });
                        },500,self,res);

                });

            },
            chartCallback:function(event){
                this.ref_one=event;
                this.ref_one.reflow();
            },
            formatAngka:function(angka){
                var number_string = (angka+'').toString(),
                split   		= number_string.split('.'),
                sisa     		= split[0].length % 3,
                rupiah     		= split[0].substr(0, sisa),
                ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
                // tambahkan titik jika yang di input sudah menjadi angka ribuan
                if(ribuan){
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                }

    
                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;

                return rupiah;

            },
            init:function(){
                this.chart_kota.title.text='';
                this.chart_provinsi.subtitle.text=this.filter.toUpperCase()+' - TAHUN {{$StahunRoute}}';
                this.chart_kota.subtitle.text=this.filter.toUpperCase()+' - TAHUN {{$StahunRoute}}';
                var self=this;
                req_ajax.post(('{{$init['map_provinsi']['ajax_data']}}').replace('xxxtahunxxx','{{$init['component']['tahun']}}'),{
                    basis:this.basis,
                    filter:this.filter
                }).then((res)=>{
                    {{$init['component']['id']}}.chart_provinsi.series[0].data=res.data;
                    var maxVotes = res.data.reduce((max, row) => Math.max(max, row[5]), 0);
                    var refmap={{$init['component']['id']}}.ref_one;
                    setTimeout(function(refmap,maxVotes){
                        refmap.series[0].points.forEach(function (state){
                        if (!state.id) {
                            return; // Skip points with no data, if any
                        }
                        var dtpie=[]
                        self.provinsi_op.pie.column.forEach(c=>{
                            var dt={
                                name:c.name,
                                y:parseFloat(state[c.field]),
                                value:parseFloat(state[c.field]),
                            }
                            dtpie.push(dt);
                        });

                        var pieOffset = state.pieOffset || {},
                        centerLat = parseFloat(state.properties.latitude),
                        centerLon = parseFloat(state.properties.longitude);
                        console.log(refmap);
                       
                        console.log(
                        refmap.addSeries({
                            type:'pie',
                            name:state.name,
                            linkedMap:Highcharts.map(['ind']),
                            zIndex: 6,
                            sizeFormatter: function () {
                                const zoomFactor = refmap.mapView.zoom / refmap.mapView.minZoom;

                                return Math.max(
                                    this.chart.chartWidth / 45 * zoomFactor, // Min size
                                    this.chart.chartWidth /
                                        11 * zoomFactor * refmap.sumVotes / maxVotes
                                );
                            },
                            center: {
                                lat: centerLat + (pieOffset.lat || 0),
                                lon: centerLon + (pieOffset.lon || 0)
                            },
                            data:dtpie

                        }));
                        });
                    },1500,refmap,maxVotes);
                    // refmap.series[1]={
                    //     name:(self.page_meta.title.toUpperCase()),
                    //     data:self.chart_provinsi.series[0].data.map(function(el){
                    //          var dtpie={};
                    //          self.provinsi_op.pie.column.forEach(c=>{
                    //             dtpie[c.f]
                    //          });
                    //     });
                    // }
                });
                this.chart_provinsi.title.text='KETERISIAN '+(page_meta.title.toUpperCase())+' BASIS '+this.basis.toUpperCase();
            }
        },
        data:{
            ref_one:{},
            ref_two:{},
            provinsi_op:{
                pie:{
                    column:<?=json_encode($init['map_provinsi']['pie']['column'])?>
                }
            },
            filter:'provinsi dan kota / kab',
            basis:'{{$init['component']['basis']}}',
            tahun:'{{$init['component']['tahun']}}',
            table:{
                title:'',
                data:[]
            },
            
            chart_kota:{
                colors:[],
                title:{
                    text:'KETERISIAN  '+('{{($init['component']['name'])}}').toUpperCase()+'  PER PROVINSI'
                },
                subtitle:{
                    text:'provinsi dan kota / kab Tahun {{$StahunRoute}}'
                },
                chart:{
                    height:500,
                    backgroundColor:'#ffffff00'
                },
                tooltip: {
                    formatter:function(){
                        return this.point.text?'<b>'+this.point.name+'</b><br>'+this.point.text:'<b>'+this.point.name+'</b>';
                    }
                },
             
                series:[
                    {
                        name:'KETERISIAN PER PROVINSI',
                        data:[],
                        type:'map',
                        joinBy:['id','id'],
                        mapData:Highcharts.maps['ind'],
                        allAreas:false,
                        dataLabels: {
                            enabled: false,
                            format: '{point.text} '
                        },
                        tooltip: {
                           enabled:false
                        },
                        point:{
                            events:{
                               
                            }
                        },
                        showInLegend:false,
                        data:[]
                    },
                    {
                        name:'KETERISIAN PER KOTA/KAB',
                        data:[],
                        type:'map',
                        joinBy:['id','id'],
                        mapData:Highcharts.maps['ind_kab'],
                        allAreas:false,
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}'
                        },
                        
                        point:{
                            events:{
                               
                            }
                        },
                        showInLegend:false,
                        data:[]
                    },
                ]
            },
            target_react_kabkot:<?=json_encode($init['map_kabkot']['target_react'])?>,
            chart_provinsi:{
                ref:null,
                title:{
                    text:'KETERISIAN '+('{{($init['component']['name'])}}').toUpperCase()+' PER PROVINSI'
                },
                subtitle:{
                    text:'provinsi dan kota / kab Tahun {{$StahunRoute}}'
                },
                chart:{
                    height:500,
                    backgroundColor:'#ffffff00'

                },
                tooltip: {
                    formatter:function(){
                        return this.point.name+': <b>'+this.point.value+'</b> PEMDA';
                    }
                },
                series:[
                    {
                        name:'KETERISIAN PER PROVINSI',
                        data:[],
                        type:'map',
                        joinBy:['id','id'],
                        mapData:Highcharts.maps['ind'],
                        dataLabels: {
                            enabled: true,
                            format: '{point.name} : <br>{point.value} Pemda'
                        },
                        point:{
                            events:{
                                click:function(){
                                    {{$init['component']['id']}}.detail_per_kota(this.kodepemda,this.name);
                                }
                            }
                        },
                        showInLegend:false,
                        data:[]
                    }
                ]
            }
         
         
        }
    });
    

</script>
@endpush