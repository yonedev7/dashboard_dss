@php
$init=[
    'component'=>[
        'id'=>isset($init['component']['id'])?$init['component']['id']:'APP_'.(int)rand(1,100),
        'name'=>isset($init['component']['name'])?$init['component']['name']:'',
        'tahun'=>isset($init['component']['tahun'])?$init['component']['tahun']:date('Y'),
        'width'=>isset($init['component']['width'])?$init['component']['width']:12,
        'basis'=>isset($init['component']['basis'])?$init['component']['basis']:'nasional'

    ]
];
@endphp

<div id="{{$init['component']['id']}}">
    <div class="table-responsive" v-if="display">
        <table class="table-bordered table-striper table">
            <thead>
                <tr>
                    <th>Kodepemda</th>
                    <th>Nama Pemda</th>
                    <th v-for="item,k in column">
                        @{{item.name}} 
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="item,k in data">
                    <td>@{{item.kodepemda}}</td>
                    <td>@{{item.name}}</td>
                    <td v-for="c,ck in column">
                        <template v-if="c.type=='numeric'">
                            @{{formatAngka(item[c.field])}} (@{{(c.satuan!=undefined)?c.satuan:'-'}})
                        </template>
                        <template v-if="c.type=='string'">
                            @{{item[c.field]}}
                        </template>
                        <template v-if=" c.type_handle=='link_download'">
                                <a v-bind:href="'{{url('')}}/'+lk.path" download="" class="btn btn-primary btn-sm" v-for="lk,klk in item[c.field]">
                                   @{{klk+1}}. Download @{{lk.name}}
                                </a>
                        </template>
                        <template v-if="c.type_handle=='link_direct'">
                            <a v-bind:href="'{{url('')}}/'+lk.path"  class="btn btn-primary btn-sm" v-for="lk,klk in item[c.field]">
                                @{{klk+1}}. @{{lk.name}}
                             </a>
                        </template>
                    </td>

                </tr>
            </tbody>
        </table>
    </div>

</div>

@push('js_push')
<script>
    var {{$init['component']['id']}}=new Vue({
        el:'#{{$init['component']['id']}}',
        data:{
            display:0,
            data:[],
            column:[]
        },
        methods:{
            formatAngka:function(angka){

                var number_string = (parseFloat(angka).toFixed(2)+'').toString(),
                split   		= number_string.split('.'),
                sisa     		= split[0].length % 3,
                rupiah     		= split[0].substr(0, sisa),
                ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
                // tambahkan titik jika yang di input sudah menjadi angka ribuan
                if(ribuan){
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                }


                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;

                return rupiah;

                },
            react:function(data){
                if(data.method=='build_table'){
                    this.data=data.data.data;

                    if(data.data.data){
                        this.display=true;
                        this.column=data.data.column;

                        this.data=data.data.data;


                    }else{
                        this.data=[];
                        this.display=false;

                    }
                }
            }
        }
    })
</script>
@endpush