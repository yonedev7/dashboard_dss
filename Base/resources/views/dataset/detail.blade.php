@extends('adminlte::page')

@section('content')
<style>
    .lite-snniper{
        width:100%!important;
        border-radius: 10px!important;
        height:10px!important;

    }
    
</style>

<div class="parallax"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;"">
    <div class="container">
        <section >
            <h3><b>TEMA A</b></h3>
            <p><b>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quaerat vitae, ea assumenda officiis quod repudiandae nostrum quos, inventore porro non illo vel obcaecati voluptatum, consequatur amet. Voluptates qui a alias.</b></p>
           
        </section>
      
    </div>
   

    
</div>

<div class="row no-gutters" style="z-index: 0;">
    <div class="col-4 bg-primary p-3 pt-5" >
        <h4><b>BASIS PEMDA</b></h4>
        <p><b>Seluruh Pemda</b></p>
    </div>
    <div class="col-4 bg-success p-3 pt-5">
        <h4><b>BASIS PROYEK</b></h4>
        <p><b>Longlist</b></p>
    </div>
    <div class="col-4 bg-warning p-3 pt-5   ">
        <h4><b>BASIS PROYEK</b></h4>
        <p><b>Sort list {{$Stahun}}</b></p>
    </div>
</div>
<div id="app-dataset-{{$SpageId}}" class="mt-4">
 
    <div class="container-fluid">
        <h3 class="text-uppercase pt-3">DATASET {{$dataset->name}}</h3>
        <p>Taggal Update</p>
        <hr>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">TW</label>
                    <select name="" id="" class="form-control" v-model="tw">
                        <option value="1">TW1</option>
                        <option value="2">TW2</option>
                        <option value="3">TW3</option>
                        <option value="4">TW4</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row" v-if="item.active" v-for="(item,keyl) in level">
            <div class="col-md-12">
                <h3>@{{item.named_lavel}} </h3>
            </div>
            <div v-for="(dis,keyd) in item.display" class="col-md-12">
                <div class="row">
                    <div class="col-md-12" v-if="['short_query_1','short_query_2','short_query_3','short_query_4'].includes(keyd)">
                        
                            <div v-if="dis && dis.schema!=null" class="row">
                                <div v-bind:id="'level_'+keyl+'_html_'+keyd" class="col-md-6" >
                                    v-if="dis.html" <div class="card">
                                        <div class="card-body" style="min-height:400px;" v-html="dis.html"></div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div> 
           </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4><b>Tahun @{{tahun}} TW@{{tw}}</b></h4>
                    </div>
                    <div class="card-body table-responsive">
                        <div v-if="item.builded==false">
                            <table class="table table-bordered">
                                <thead class="thead-dark">
                                    <tr>
                                        <th> <div class="spinner-grow lite-snniper text-secondary" role="status">
                                        <span class="sr-only">Loading...</span>
                                        </div></th>
                                        <th> <div class="spinner-grow lite-snniper text-secondary" role="status">
                                        <span class="sr-only">Loading...</span>
                                        </div></th>
                                        <th> <div class="spinner-grow lite-snniper text-secondary" role="status">
                                        <span class="sr-only">Loading...</span>
                                        </div></th>
                                        <th> <div class="spinner-grow lite-snniper text-secondary" role="status">
                                        <span class="sr-only">Loading...</span>
                                        </div></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td> <div class="spinner-grow lite-snniper-2 text-secondary" role="status">
                                            <span class="sr-only">Loading...</span>
                                            </div></td>
                                            <td> <div class="spinner-grow lite-snniper-2 text-secondary" role="status">
                                            <span class="sr-only">Loading...</span>
                                            </div></td>
                                            <td> <div class="spinner-grow lite-snniper-2 text-secondary" role="status">
                                            <span class="sr-only">Loading...</span>
                                            </div></td>
                                            <td> <div class="spinner-grow lite-snniper-2 text-secondary" role="status">
                                            <span class="sr-only">Loading...</span>
                                            </div></td>
                    
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div v-if="item.builded==true">
                            <table v-bind:id="'table-level-'+keyl" class="table table-bordered">
                                
                                <thead class="thead-dark">
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th class="with-info" v-for="(colom,keyc) in item.meta.list_data">
                                            <span @click="alert('click')"><i class="fa fa-info-circle"></i> Informasi Kolom</span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th >Kode PEMDA</th>
                                        <th >Nama PEMDA</th>
                                        <th class="with-info" v-for="(colom,keyc) in item.meta.list_data">@{{colom.name}} (@{{colom.satuan}}) -Tahun  @{{parseInt(item.tahun)+parseInt(colom.tahun_data)}} TW@{{colom.tw_data}}
                                        
                                        </th>
                                    </tr>
                                    <tr>
                                        <th>1</th>
                                        <th>2</th>
                                        <th class="with-info" v-for="(colom,keyc) in item.meta.list_data">@{{keyc+3}}
                    
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                    
                                </tbody>
                            </table>
                    
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('js')
<script>
    var app_{{$SpageId}}=new Vue({
        el:"#app-dataset-{{$SpageId}}",
        data:{
            tw:4,
            tahun:{{$Stahun}},
            dataset:<?=json_encode($dataset)?>,            
            level:{
                l0:{
                    builded:false,
                    named_lavel:'NASIONAL',
                    active:0,
                    display:[],
                    dataset:[],
                    tahun:0,
                    tw:0,
                    meta:[],
                    table:null,
                    widget:[
                        {
                            builded:false,
                            html:null,
                            active:0
                        },
                        {
                            builded:false,
                            html:null,
                            active:0
                        },
                        {
                            builded:false,
                            html:null,
                            active:0
                        },
                        {
                            builded:false,
                            html:null,
                            active:0
                        },
                    ]

                },
                l1:{
                    builded:false,
                    named_lavel:'PER-PROVINSI',
                    active:0,
                    display:[],
                    dataset:[],
                    tahun:0,
                    tw:0,
                    meta:[],
                    table:null,
                    widget:[
                        {
                            builded:false,
                            html:null,
                            active:0
                        },
                        {
                            builded:false,
                            html:null,
                            active:0
                        },
                        {
                            builded:false,
                            html:null,
                            active:0
                        },
                        {
                            builded:false,
                            html:null,
                            active:0
                        },
                    ]


                },
                l2:{   
                    builded:false,
                    named_lavel:'KAB/KOTA',
                    active:0,
                    display:[],
                    dataset:[],
                    tahun:0,
                    tw:0,
                    meta:[],
                    table:null,
                    widget:[
                        {
                            builded:false,
                            html:null,
                            active:0
                        },
                        {
                            builded:false,
                            html:null,
                            active:0
                        },
                        {
                            builded:false,
                            html:null,
                            active:0
                        },
                        {
                            builded:false,
                            html:null,
                            active:0
                        },
                    ]
                }
            }
        },
        methods:{
            init:function(){
                setTimeout(() => {
                    app_{{$SpageId}}.get_level_0();
                }, 100);
            },
            get_level_0:function(kodepemda=0){
                this.level.l0.active=true;
                this.level.l0.builded=false;
                if(this.level.l0.table!=null){
                    app_{{$SpageId}}.level.l0.table.destroy();
                    this.level.l0.table=null;
                }

                window.req_ajax.get('public/get-dataset-detail/'+this.tahun+'/'+this.tw+'/'+this.dataset.id+'?'+qs.stringify({kodepemda:kodepemda})).then(function(res){
                   var named_old=app_{{$SpageId}}.level.l0.named_lavel;
                    app_{{$SpageId}}.level.l0.meta=res.data.meta;
                    app_{{$SpageId}}.level.l0.dataset=res.data.dataset;
                    app_{{$SpageId}}.level.l0.display=res.data.display;
                    app_{{$SpageId}}.level.l0.tahun=res.data.tahun;
                    app_{{$SpageId}}.level.l0.tw=res.data.tw;
                    app_{{$SpageId}}.level.l0.builded=true;
                    app_{{$SpageId}}.level.l0.active=1;
                });
                
            },
            build_level_0:function(){

            }

        },
        watch:{
            'tw':function(){
                this.init();
            },
            'level.l0.builded':async function(val,old){
                if(val==1){
                    


                    var op_table={
                        data:this.level.l0.dataset,
                        pageLength:10,
                        lengthChange:false,
                        "retrieve": true,
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ],
                        columns:[
                            {
                                data:'kodepemda',
                                render:function(d,a,item){
                                    return parseInt(item.kodepemda)
                                }
                            },
                            {
                                data:'nama_pemda'
                            }
                        ]
                    };

                    for(var f in this.level.l0.meta.list_data){
                       op_table.columns.push({data:f});
                    }
                    
                    await setTimeout(() => {
                         app_{{$SpageId}}.level.l0.table=$('#table-level-l0').DataTable(op_table);

                    },200);

                    var d=1;
                    while(d<5){
                        var dp=this.level.l0.display['short_query_'+d];
                        if(dp && dp.schema){
                            this.level.l0.widget[d-1].active=true;
                            url=('{{url('api/')}}'+'/public/build-widget-dataset/'+this.tahun+'/'+this.tw+'?'+qs.stringify({schema:dp.schema}));
                            $('#level_l0_html_short_query_'+d).load(url,function( response, status, xhr ) {
                            if ( status == "error" ) {
                                var msg = "Sorry but there was an error: ";
                                console.log(msg);
                            }
                            });
                            // await window.req_ajax.get().then(async function(res) {
                            //     app_{{$SpageId}}.level.l0.display['short_query_'+d].html=res.data.html;
                                
                               
                            //  });
                        }
                        d++;
                    }
                  
                   
                   

                    
                }
             }
        }
        
    });
    app_{{$SpageId}}.init();

</script>
<script id="script-a"></script>

@stop

{{-- if(val==1){
    if(this.level.l0.table!=null){
        this.level.l0.table.detroy();
        this.level.l0.table=null;
    }
    var op_table={
        data:this.level.l0.dataset,
        columns:[
            {
                data:'kodepemda'
            },
            {
                data:'nama_pemda'
            }
        ]
    };
    this.level.l0.meta.list_data.each(function(el){
        console.log(el)
    });

    this.level.l0.table=$('#table-level-0').DataTable() --}}

