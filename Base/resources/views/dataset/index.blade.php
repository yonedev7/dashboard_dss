@extends('adminlte::page')



@section('content')
<div id="app_{{$SpageId}}">
    @php
    @endphp
    <div class="parallax"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
        <div class="container">
            <section >
                <h3><b>{{$dataset->name}} - {{$Stahun}}</b></h3>
                <p><b>{!!$dataset->tujuan!!}</b></p>
                <div class="btn-group">
                    <button disabled class="btn btn-warning btn-sm" style="opacity: 1;">Terbarui Pada : 28 Nov 2022</button>
                    <button class="btn btn-dark bg-navy btn-sm" @click="info_show">Informasi Tambahan</button>
                </div>
            </section>
          
        </div>
       
    
        
    </div>
    
    <div class="row no-gutters" v-if="dataset.pusat!=1" style="z-index: 0;">
        <div class="col-4 bg-primary p-3 pt-5" >
            <h4><b>BASIS PEMDA</b></h4>
            <p><b>Seluruh Pemda</b></p>
            <a href="{{route('dataset.index',['tahun'=>$Stahun,'id_dataset'=>$dataset->id,'tw'=>$tw,'basis'=>'pemda'])}}" class="btn btn-success"><i class="fa fa-arrow-down"></i></a>
        </div>
        <div class="col-4 bg-success p-3 pt-5">
            <h4><b>BASIS PROYEK</b></h4>
            <p><b>Long List</b></p>
            <a  href="{{route('dataset.index',['tahun'=>$Stahun,'id_dataset'=>$dataset->id,'tw'=>$tw,'basis'=>'long_list'])}}"class="btn btn-warning text-dark"><i class="fa fa-arrow-down text-dark"></i></a>
        </div>
        <div class="col-4 bg-warning p-3 pt-5   ">
            <h4><b>BASIS PROYEK</b></h4>
            <p><b>Sort List {{$Stahun}}</b></p>
            <a href="{{route('dataset.index',['tahun'=>$Stahun,'id_dataset'=>$dataset->id,'tw'=>$tw,'basis'=>'sort_list'])}}" class="btn btn-primary "><i class="fa fa-arrow-down text-white"></i></a>
        </div>
    </div>
    <div class="parallax pt-3"  v-if="dataset.pusat!=1" style=" background-image: url('{{url('assets/img/back-t-2.png')}}');">
        <div class="container">
            <h3 class="text-center "><b>{{$nama_filter.' - '.$pemda_count}} Pemda</b></h3>
        </div>
    
    </div>
    @if($dataset->interval_pengambilan==0)
    <div class="container">
        <div class="row">
            <div class="col-4">
             <div class="form-group">
                 <label for="">TW</label>
                 <select name="" class="form-control" id="">
                     <option value="1">TW I</option>
                     <option value="2">TW II</option>
                     <option value="3">TW III</option>
                     <option value="4">TW IV</option>
                 </select>
             </div>
            </div>
        </div>
     </div>
    
    @endif
</div>

   
<div id="lev_1" v-if="builder">
    <div class="row no-gutters" v-if="load_table==true && pusat!=true">
        <div class="col-12 text-center pt-2" style="border-top:2px solid #000"><H4><b>@{{title}}</b></H4></div>
        <div class="col-6 p-3 text-center bg-dark">
            <h4>Terdata</h4>
            <p>@{{jumlah_terdata}} / @{{jumlah_pemda}} Pemda</p>
        </div>
        <div class="col-6 p-3 text-center bg-secondary">
            <h4>Terverifikasi</h4>
            <p>@{{
            jumlah_terverifikasi}} / @{{jumlah_terdata}} Pemda</p>
        </div>
    </div>
    <div v-if="load_table==false" class="p-3 text-center ">
        <div class="spinner-border text-navy" role="status">
            <span class="sr-only">Loading...</span>
          </div>
        <div class="spinner-border text-primary" role="status">
            <span class="sr-only">Loading...</span>
          </div>
          <div class="spinner-border text-warning" role="status">
            <span class="sr-only">Loading...</span>
          </div>
         
          <div class="spinner-border text-danger" role="status">
            <span class="sr-only">Loading...</span>
          </div>
    </div>
    <section class="container">
      <div class="card ">
          <div class="card-body">
            <div class="table-responsive">
                <table id="lev_1_table" class="table table-bordered" style="width:100%;">
                    <thead>
                        <tr>
                            <th  v-for=" h in table.header"  @click="info_column(h)">
                                  <i class="fa fa-info-circle"></i> Info Data
                            </th>
                        </tr>
                      <tr>
                         
                          <th v-for=" h in table.header">
                               @{{h.name}}
                          </th>
                      </tr>
                    </thead>
                    <tbody>
                        <tr v-for="item in table.data">
                           <td v-for="h in table.header">@{{item[h.data]==undefined?'':item[h.data]}}</td>
                        </tr>
                    </tbody>
                  </table>
            </div>
          </div>
      </div>
    </section>

    <div class="modal fade" id="modal-info-l1" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Info</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <p for="">Kode : @{{info_header.kode??'-'}}</p>
                    <label for="">Nama</label>
                    <p>@{{info_header.name}}</p>
                    <label for="">Satuan</label>
                    <p>@{{info_header.satuan}}</p>
                    <label for="">Tipe Nilai</label>
                    <p>@{{info_header.tipe_nilai}}</p>
                    <label for="">Definisi / Konsep</label>
                    <p v-html="info_header.definisi_konsep"></p>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

</div>
<div id="lev_2" v-if="builder">
    <div class="row no-gutters">
        <div class="col-12 text-center pt-2" style="border-top:2px solid #000"><H4><b>@{{level_2.title}}</b></H4></div>
        <div class="col-6 p-3 text-center bg-dark">
            <h4>Terdata</h4>
            <p>@{{level_2.jumlah_terdata}} / @{{level_2.jumlah_pemda}} Pemda</p>
        </div>
        <div class="col-6 p-3 text-center bg-secondary">
            <h4>Terverifikasi</h4>
            <p>@{{level_2.jumlah_terverifikasi}} / @{{level_2.jumlah_terdata}} Pemda</p>
        </div>
    </div>
</div>
<div id="lev_3" v-if="level_3['title']!=undefined"></div>

    <div id="modal-info-dataset"  class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Informasi Tambahan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <label for="">Interval Pengambilan</label>
                <p>{{$dataset->interval_pengambilan==1?'Tahunan':'Triwulanan'}}</p>
                <label for="">Penangung Jawab</label>
                <p>{{$dataset->penangung_jawab}}</p>
              </div>
            
          </div>
        </div>
      </div>



@stop


@section('js')

<script>
    var app_l_1=new Vue({
        el:'#lev_1',
        data:{
            pusat:{{$dataset->pusat?1:0}},
            path_data:'{{route('api-web.public.ajax_data',['tahun'=>$Stahun,'id_dataset'=>$dataset->id])}}',
            builder:false,
            load_table:false,
            load_chard:false,
            tw:{{$tw}},
            basis:'{{$basis}}',
            title:'NASIONAL',
            chart:[],
            data:[],
            table:{
                header:[],
                data:[],
                ajax_path:'{{route('api-web.public.ajax_table',['tahun'=>$Stahun,'id_dataset'=>$dataset->id,'basis'=>$basis,'level'=>1])}}',
                ajax_path_header:'{{route('api-web.public.ajax_table',['tahun'=>$Stahun,'id_dataset'=>$dataset->id,'header'=>true,'basis'=>$basis,'level'=>1])}}',
                ajax_path_rekap:'{{route('api-web.public.ajax_table',['tahun'=>$Stahun,'id_dataset'=>$dataset->id,'rekap'=>true,'basis'=>$basis,'level'=>1])}}',
                table_object:null
            },
            info_header:{
                name:'',
                satuan:'',
                definisi_kosep:'',
                tipe_nilai:'',
                kode:'',
            },
            jumlah_pemda:0,
            jumlah_terdata:0,
            jumlah_terverifikasi:0,
        },
        methods:{
            info_column:function(item){
                this.info_header=item;
                $('#modal-info-l1').modal();
            },
            init_table:function(){
                $.post(this.table.ajax_path_rekap,function(res){
                    app_l_1.jumlah_pemda=514;
                    app_l_1.jumlah_terdata=res.jumlah_data;
                    app_l_1.jumlah_terverifikasi=res.terverifikasi;
                });

                req_ajax.post(this.path_data,{'header':true,'basis':this.basis,level:1}).then(function(res){
                    app_l_1.table.header=res.data;
                    setTimeout(() => {
                        
                        app_l_1.table_draw();
                        // app_l_1.table.table_object=$('#lev_1_table').DataTable({
                        //     "ajax":{
                        //         url:app_l_1.path_data,
                        //         type:'POST',
                        //         data:{
                        //             tw:app_l_1.tw,
                        //             basis:app_l_1.basis,
                        //             level:1,
                        //             datatable:1

                        //         },
                        //         'beforeSend': function (request) {
                        //             request.setRequestHeader("Authorization", 'Bearer '+dss{{$SpageId}}.token);
                        //         }
                                
                        //     },
                        //     serverSide:true,
                        //     columns:app_l_1.table.header.map((el)=>{
                        //         return {data:el.data};
                        //     })
                        // });
                    }, 500);
                    app_l_1.load_table=true;
                    app_l_1.load_table=true;

                });               
            },
            clean:function(){
                this.builder=false;
            },
            table_draw:function(){
                req_ajax.post(this.path_data,{'basis':this.basis,level:1}).then(function(res){
                    app_l_1.table.data=res.data;
                    setTimeout(() => {
                        app_l_1.table.table_object=$('#lev_1_table').DataTable();
                        
                    }, 100);
                });

            },
            init:function(){
                app_l_2.clean();
                this.builder=true; 
                @if($dataset->pusat)
                 this.init_table();
                @else
                this.init_table();
                // $.post('{{route('api-web.public.ajax_table',['tahun'=>$Stahun,'id_dataset'=>$dataset->id])}}')
                @endif
            }
        }
    });
    var app_l_2=new Vue({
        el:'#lev_2',
        data:{
            builder:false,
            title:'NASIONAL',
            chart:[],
            jumlah_pemda:0,
            jumlah_terdata:0,
            jumlah_terverifikasi:0

        },
        methods:{
            clean:function(){
                this.builder=false;
            },
            init:function(){
                

            }
        }
    });


    var app=new Vue({
        el:'#app_{{$SpageId}}',
        data:{
            jumlah_scope_pemda:{{$pemda_count}},
            dataset:<?=json_encode($dataset)?>,
            basis:'{{$basis}}',
            tw:{{$tw}},
        },
        watch:{
            tw:function(val){
                app_l_2.tw=val;
                app_l_1.init();
            }
        },
        methods:{
            
            info_show:function(){
                $('#modal-info-dataset').modal();
            },
            schema_builder:function(){
               app_l_1.init();
               
            },
            
           
        }
    });

    app.schema_builder();

</script>



@stop