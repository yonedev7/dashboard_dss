<table class="table tabl-bordered">
    <thead class="bg-primary">
        <tr>
            <th>Kode PEMDA</th>
            <th>Nama Pemda</th>
            <th>Status Verifikasi</th>

            <th>Pagu</th>
            <th>Jumlah Kegiatan</th>
            <th>Jumlah Sub Kegiatan</th>



        </tr>
    </thead>
    <tr>
        <td >1101</td>
        <td >KABUPATEN ACEH SELATAN</td>
        <td >Belum Terverifikasi</td>
        <td >IDR 1.900.000.000</td>
        <td >1</td>
        <td >2</td>
        </tr>
        <tr>
        <td>1102</td>
        <td>KABUPATEN ACEH TENGGARA</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 49.750.000.000</td>
        <td>1</td>
        <td>6</td>
        </tr>
        <tr>
        <td>1104</td>
        <td>KABUPATEN ACEH TENGAH</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 5.950.004.206</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>1105</td>
        <td>KABUPATEN ACEH BARAT</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 4.678.000.000</td>
        <td>1</td>
        <td>13</td>
        </tr>
        <tr>
        <td>1106</td>
        <td>KABUPATEN ACEH BESAR</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 6.468.000.000</td>
        <td>1</td>
        <td>6</td>
        </tr>
        <tr>
        <td>1108</td>
        <td>KABUPATEN ACEH UTARA</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 1.018.077.000</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>1116</td>
        <td>KABUPATEN ACEH TAMIANG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 14.685.224.981</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>1171</td>
        <td>KOTA BANDA ACEH</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 24.448.126.000</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>1172</td>
        <td>KOTA SABANG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 894.118.423</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>1174</td>
        <td>KOTA LANGSA</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 3.688.520.000</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>1202</td>
        <td>KABUPATEN TAPANULI UTARA</td>
        <td>Belum Terverifikasi</td>
        <td>4747260936.99</td>
        <td>1</td>
        <td>4</td>
        </tr>
        <tr>
        <td>1204</td>
        <td>KABUPATEN NIAS</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 2.200.000.000</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>1205</td>
        <td>KABUPATEN LANGKAT</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 6.970.000.000</td>
        <td>1</td>
        <td>5</td>
        </tr>
        <tr>
        <td>1206</td>
        <td>KABUPATEN KARO</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 1.100.000.000</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>1207</td>
        <td>KABUPATEN DELI SERDANG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 32.700.480.000</td>
        <td>1</td>
        <td>8</td>
        </tr>
        <tr>
        <td>1208</td>
        <td>KABUPATEN SIMALUNGUN</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 27.162.700.000</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>1209</td>
        <td>KABUPATEN ASAHAN</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 21.942.705.344</td>
        <td>1</td>
        <td>4</td>
        </tr>
        <tr>
        <td>1210</td>
        <td>KABUPATEN LABUHANBATU</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 1.271.900.000</td>
        <td>1</td>
        <td>4</td>
        </tr>
        <tr>
        <td>1211</td>
        <td>KABUPATEN DAIRI</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 18.950.000.000</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>1272</td>
        <td>KOTA PEMATANGSIANTAR</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 10.400.000.000</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>1273</td>
        <td>KOTA SIBOLGA</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 378.074.550</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>1274</td>
        <td>KOTA TANJUNG BALAI</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 2.200.000.000</td>
        <td>1</td>
        <td>5</td>
        </tr>
        <tr>
        <td>1275</td>
        <td>KOTA BINJAI</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 5.000.000.000</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>1276</td>
        <td>KOTA TEBING TINGGI</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 1.000.000.000</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>1277</td>
        <td>KOTA PADANGSIDIMPUAN</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 485.000.000</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>1302</td>
        <td>KABUPATEN SOLOK</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 10.505.000.000</td>
        <td>1</td>
        <td>10</td>
        </tr>
        <tr>
        <td>1303</td>
        <td>KABUPATEN SIJUNJUNG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 2.510.000.000</td>
        <td>1</td>
        <td>14</td>
        </tr>
        <tr>
        <td>1304</td>
        <td>KABUPATEN TANAH DATAR</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 9.186.000.000</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>1306</td>
        <td>KABUPATEN AGAM</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 11.629.830.000</td>
        <td>1</td>
        <td>5</td>
        </tr>
        <tr>
        <td>1372</td>
        <td>KOTA SOLOK</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 1.473.085.000</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>1373</td>
        <td>KOTA SAWAHLUNTO</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 4.376.757.500</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>1375</td>
        <td>KOTA BUKITTINGGI</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 170.580.000</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>1376</td>
        <td>KOTA PAYAKUMBUH</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 101.000.000</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>1401</td>
        <td>KABUPATEN KAMPAR</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 3.900.000.000</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>1402</td>
        <td>KABUPATEN INDRAGIRI HULU</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 19.067.873.200</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>1403</td>
        <td>KABUPATEN BENGKALIS</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 57.535.850.000</td>
        <td>1</td>
        <td>10</td>
        </tr>
        <tr>
        <td>1404</td>
        <td>KABUPATEN INDRAGIRI HILIR</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 13.060.364.000</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>1471</td>
        <td>KOTA PEKANBARU</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 28.896.018.716</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>1472</td>
        <td>KOTA DUMAI</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 19.800.000.000</td>
        <td>1</td>
        <td>4</td>
        </tr>
        <tr>
        <td>1501</td>
        <td>KABUPATEN KERINCI</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 5.300.000.000</td>
        <td>1</td>
        <td>4</td>
        </tr>
        <tr>
        <td>1502</td>
        <td>KABUPATEN MERANGIN</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 11.500.000.000</td>
        <td>1</td>
        <td>6</td>
        </tr>
        <tr>
        <td>1504</td>
        <td>KABUPATEN BATANGHARI</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 9.307.426.000</td>
        <td>1</td>
        <td>5</td>
        </tr>
        <tr>
        <td>1508</td>
        <td>KABUPATEN BUNGO</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 2.558.735.000</td>
        <td>1</td>
        <td>7</td>
        </tr>
        <tr>
        <td>1571</td>
        <td>KOTA JAMBI</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 103.226.710.000</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>1601</td>
        <td>KABUPATEN OGAN KOMERING ULU</td>
        <td>Belum Terverifikasi</td>
        <td>5000000.0</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>1603</td>
        <td>KABUPATEN MUARA ENIM</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 6.109.345.000</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>1604</td>
        <td>KABUPATEN LAHAT</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 77.175.000.000</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>1606</td>
        <td>KABUPATEN MUSI BANYUASIN</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 3.020.000.000</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>1610</td>
        <td>KABUPATEN OGAN ILIR</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 4.750.000.000</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>1671</td>
        <td>KOTA PALEMBANG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 35.000.000.000</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>1673</td>
        <td>KOTA LUBUK LINGGAU</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 1.293.750.000</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>1701</td>
        <td>KABUPATEN BENGKULU SELATAN</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 46.880.000.000</td>
        <td>1</td>
        <td>8</td>
        </tr>
        <tr>
        <td>1702</td>
        <td>KABUPATEN REJANG LEBONG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 13.480.120.000</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>1709</td>
        <td>KABUPATEN BENGKULU TENGAH</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 4.383.903.183</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>1771</td>
        <td>KOTA BENGKULU</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 4.104.340.000</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>1801</td>
        <td>KABUPATEN LAMPUNG SELATAN</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 6.786.727.170</td>
        <td>1</td>
        <td>4</td>
        </tr>
        <tr>
        <td>1802</td>
        <td>KABUPATEN LAMPUNG TENGAH</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 22.084.980.133</td>
        <td>1</td>
        <td>4</td>
        </tr>
        <tr>
        <td>1803</td>
        <td>KABUPATEN LAMPUNG UTARA</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 674.336.744</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>1806</td>
        <td>KABUPATEN TANGGAMUS</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 16.078.144.600</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>1871</td>
        <td>KOTA BANDAR LAMPUNG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR -</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>1901</td>
        <td>KABUPATEN BANGKA</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 1.800.000.000</td>
        <td>1</td>
        <td>5</td>
        </tr>
        <tr>
        <td>1902</td>
        <td>KABUPATEN BELITUNG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 10.996.608.250</td>
        <td>1</td>
        <td>4</td>
        </tr>
        <tr>
        <td>1971</td>
        <td>KOTA PANGKAL PINANG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 5.617.769.500</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>2172</td>
        <td>KOTA TANJUNG PINANG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 29.558.195.200</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>3201</td>
        <td>KABUPATEN BOGOR</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 20.501.000.000</td>
        <td>1</td>
        <td>4</td>
        </tr>
        <tr>
        <td>3202</td>
        <td>KABUPATEN SUKABUMI</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 162.630.369.000</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>3204</td>
        <td>KABUPATEN BANDUNG</td>
        <td>Belum Terverifikasi</td>
        <td>11272849650.45</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>3207</td>
        <td>KABUPATEN CIAMIS</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 27.555.000.000</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>3208</td>
        <td>KABUPATEN KUNINGAN</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 154.257.069.492</td>
        <td>1</td>
        <td>6</td>
        </tr>
        <tr>
        <td>3209</td>
        <td>KABUPATEN CIREBON</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 8.439.046.003</td>
        <td>1</td>
        <td>4</td>
        </tr>
        <tr>
        <td>3210</td>
        <td>KABUPATEN MAJALENGKA</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 40.200.917.000</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>3211</td>
        <td>KABUPATEN SUMEDANG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 32.971.994.000</td>
        <td>1</td>
        <td>4</td>
        </tr>
        <tr>
        <td>3212</td>
        <td>KABUPATEN INDRAMAYU</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 649.999.932</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>3213</td>
        <td>KABUPATEN SUBANG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 10.558.000.000</td>
        <td>1</td>
        <td>4</td>
        </tr>
        <tr>
        <td>3217</td>
        <td>KABUPATEN BANDUNG BARAT</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 6.506.452.945</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>3273</td>
        <td>KOTA BANDUNG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 18.736.768.375</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>3274</td>
        <td>KOTA CIREBON</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 1.250.493.500</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>3276</td>
        <td>KOTA DEPOK</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 2.663.160.000</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>3277</td>
        <td>KOTA CIMAHI</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 6.666.976.300</td>
        <td>1</td>
        <td>4</td>
        </tr>
        <tr>
        <td>3279</td>
        <td>KOTA BANJAR</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 4.100.000.000</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>3301</td>
        <td>KABUPATEN CILACAP</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 7.186.625.000</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>3302</td>
        <td>KABUPATEN BANYUMAS</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 9.180.000.000</td>
        <td>1</td>
        <td>5</td>
        </tr>
        <tr>
        <td>3305</td>
        <td>KABUPATEN KEBUMEN</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 13.503.685.000</td>
        <td>1</td>
        <td>4</td>
        </tr>
        <tr>
        <td>3306</td>
        <td>KABUPATEN PURWOREJO</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 17.178.000.000</td>
        <td>1</td>
        <td>4</td>
        </tr>
        <tr>
        <td>3308</td>
        <td>KABUPATEN MAGELANG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 390.510.000</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>3309</td>
        <td>KABUPATEN BOYOLALI</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 5.725.793.000</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>3310</td>
        <td>KABUPATEN KLATEN</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 1.668.237.254</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>3311</td>
        <td>KABUPATEN SUKOHARJO</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 1.290.000.000</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>3314</td>
        <td>KABUPATEN SRAGEN</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 4.884.999.500</td>
        <td>1</td>
        <td>5</td>
        </tr>
        <tr>
        <td>3318</td>
        <td>KABUPATEN PATI</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 25.725.000.000</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>3322</td>
        <td>KABUPATEN SEMARANG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 27.010.549.000</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>3325</td>
        <td>KABUPATEN BATANG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 3.239.879.739</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>3326</td>
        <td>KABUPATEN PEKALONGAN</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 18.374.776.462</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>3329</td>
        <td>KABUPATEN BREBES</td>
        <td>Belum Terverifikasi</td>
        <td>IDR -</td>
        <td>1</td>
        <td>6</td>
        </tr>
        <tr>
        <td>3373</td>
        <td>KOTA SALATIGA</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 2.416.792.000</td>
        <td>1</td>
        <td>5</td>
        </tr>
        <tr>
        <td>3375</td>
        <td>KOTA PEKALONGAN</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 2.030.999.000</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>3376</td>
        <td>KOTA TEGAL</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 816.000.000</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>3402</td>
        <td>KABUPATEN BANTUL</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 8.333.089.440</td>
        <td>1</td>
        <td>8</td>
        </tr>
        <tr>
        <td>3404</td>
        <td>KABUPATEN SLEMAN</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 37.410.744.000</td>
        <td>1</td>
        <td>5</td>
        </tr>
        <tr>
        <td>3501</td>
        <td>KABUPATEN PACITAN</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 5.624.000.000</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>3502</td>
        <td>KABUPATEN PONOROGO</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 1.275.000.000</td>
        <td>1</td>
        <td>7</td>
        </tr>
        <tr>
        <td>3504</td>
        <td>KABUPATEN TULUNGAGUNG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 126.022.000.000</td>
        <td>1</td>
        <td>21</td>
        </tr>
        <tr>
        <td>3505</td>
        <td>KABUPATEN BLITAR</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 12.209.491.000</td>
        <td>1</td>
        <td>6</td>
        </tr>
        <tr>
        <td>3506</td>
        <td>KABUPATEN KEDIRI</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 11.581.734.950</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>3507</td>
        <td>KABUPATEN MALANG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 12.744.281.000</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>3508</td>
        <td>KABUPATEN LUMAJANG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 6.229.046.400</td>
        <td>1</td>
        <td>18</td>
        </tr>
        <tr>
        <td>3509</td>
        <td>KABUPATEN JEMBER</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 19.390.347.537</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>3513</td>
        <td>KABUPATEN PROBOLINGGO</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 22.782.116.000</td>
        <td>1</td>
        <td>7</td>
        </tr>
        <tr>
        <td>3514</td>
        <td>KABUPATEN PASURUAN</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 7.195.231.000</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>3515</td>
        <td>KABUPATEN SIDOARJO</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 180.117.314.300</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>3521</td>
        <td>KABUPATEN NGAWI</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 3.975.000.000</td>
        <td>1</td>
        <td>5</td>
        </tr>
        <tr>
        <td>3524</td>
        <td>KABUPATEN LAMONGAN</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 38.777.000.000</td>
        <td>1</td>
        <td>6</td>
        </tr>
        <tr>
        <td>3525</td>
        <td>KABUPATEN GRESIK</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 33.028.001.880</td>
        <td>1</td>
        <td>7</td>
        </tr>
        <tr>
        <td>3526</td>
        <td>KABUPATEN BANGKALAN</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 27.812.006.665</td>
        <td>1</td>
        <td>6</td>
        </tr>
        <tr>
        <td>3527</td>
        <td>KABUPATEN SAMPANG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 23.758.791.255</td>
        <td>1</td>
        <td>11</td>
        </tr>
        <tr>
        <td>3573</td>
        <td>KOTA MALANG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 6.003.830.000</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>3574</td>
        <td>KOTA PROBOLINGGO</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 10.000.000</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>3575</td>
        <td>KOTA PASURUAN</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 3.521.161.000</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>3578</td>
        <td>KOTA SURABAYA</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 293.924.659</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>3579</td>
        <td>KOTA BATU</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 11.527.517.167</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>3601</td>
        <td>KABUPATEN PANDEGLANG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 1.247.900.000</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>3602</td>
        <td>KABUPATEN LEBAK</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 14.696.068.250</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>3603</td>
        <td>KABUPATEN TANGERANG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 48.903.200.000</td>
        <td>1</td>
        <td>4</td>
        </tr>
        <tr>
        <td>3671</td>
        <td>KOTA TANGERANG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 2.829.768.317</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>5101</td>
        <td>KABUPATEN JEMBRANA</td>
        <td>Belum Terverifikasi</td>
        <td>IDR -</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>5102</td>
        <td>KABUPATEN TABANAN</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 32.706.083.200</td>
        <td>1</td>
        <td>7</td>
        </tr>
        <tr>
        <td>5103</td>
        <td>KABUPATEN BADUNG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 7.413.898.982</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>5104</td>
        <td>KABUPATEN GIANYAR</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 3.508.000.000</td>
        <td>1</td>
        <td>4</td>
        </tr>
        <tr>
        <td>5105</td>
        <td>KABUPATEN KLUNGKUNG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 3.303.362.712</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>5106</td>
        <td>KABUPATEN BANGLI</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 7.997.000.000</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>5107</td>
        <td>KABUPATEN KARANGASEM</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 15.924.376.000</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>5108</td>
        <td>KABUPATEN BULELENG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 3.415.163.500</td>
        <td>1</td>
        <td>4</td>
        </tr>
        <tr>
        <td>5171</td>
        <td>KOTA DENPASAR</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 4.510.173.400</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>5201</td>
        <td>KABUPATEN LOMBOK BARAT</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 12.518.802.400</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>5202</td>
        <td>KABUPATEN LOMBOK TENGAH</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 22.766.640.000</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>5203</td>
        <td>KABUPATEN LOMBOK TIMUR</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 17.291.313.459</td>
        <td>1</td>
        <td>8</td>
        </tr>
        <tr>
        <td>5208</td>
        <td>KABUPATEN LOMBOK UTARA</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 6.933.289.529</td>
        <td>1</td>
        <td>4</td>
        </tr>
        <tr>
        <td>5271</td>
        <td>KOTA MATARAM</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 70.460.074</td>
        <td>1</td>
        <td>4</td>
        </tr>
        <tr>
        <td>5307</td>
        <td>KABUPATEN SIKKA</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 48.634.553.120</td>
        <td>1</td>
        <td>5</td>
        </tr>
        <tr>
        <td>5308</td>
        <td>KABUPATEN ENDE</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 8.060.000.000</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>5309</td>
        <td>KABUPATEN NGADA</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 47.698.910.600</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>5310</td>
        <td>KABUPATEN MANGGARAI</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 83.438.179.300</td>
        <td>1</td>
        <td>8</td>
        </tr>
        <tr>
        <td>5315</td>
        <td>KABUPATEN MANGGARAI BARAT</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 7.519.565.000</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>6110</td>
        <td>KABUPATEN MELAWI</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 33.753.500.000</td>
        <td>1</td>
        <td>9</td>
        </tr>
        <tr>
        <td>6112</td>
        <td>KABUPATEN KUBU RAYA</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 12.805.415.710</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>6171</td>
        <td>KOTA PONTIANAK</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 5.510.722.590</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>6172</td>
        <td>KOTA SINGKAWANG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 5.356.740.000</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>6203</td>
        <td>KABUPATEN KAPUAS</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 19.644.823.000</td>
        <td>1</td>
        <td>13</td>
        </tr>
        <tr>
        <td>6204</td>
        <td>KABUPATEN BARITO SELATAN</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 13.927.986.000</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>6205</td>
        <td>KABUPATEN BARITO UTARA</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 4.282.694.325</td>
        <td>1</td>
        <td>4</td>
        </tr>
        <tr>
        <td>6271</td>
        <td>KOTA PALANGKARAYA</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 933.873.988</td>
        <td>1</td>
        <td>4</td>
        </tr>
        <tr>
        <td>6301</td>
        <td>KABUPATEN TANAH LAUT</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 6.424.081.600</td>
        <td>1</td>
        <td>5</td>
        </tr>
        <tr>
        <td>6303</td>
        <td>KABUPATEN BANJAR</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 8.045.205.150</td>
        <td>1</td>
        <td>5</td>
        </tr>
        <tr>
        <td>6305</td>
        <td>KABUPATEN TAPIN</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 1.839.000.000</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>6308</td>
        <td>KABUPATEN HULU SUNGAI UTARA</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 4.482.435.000</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>6372</td>
        <td>KOTA BANJARBARU</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 5.970.440.000</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>6409</td>
        <td>KABUPATEN PENAJAM PASER UTARA</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 14.004.000.000</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>6471</td>
        <td>KOTA BALIKPAPAN</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 15.500.000.000</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>6472</td>
        <td>KOTA SAMARINDA</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 28.250.000.000</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>6474</td>
        <td>KOTA BONTANG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 29.100.000.000</td>
        <td>1</td>
        <td>8</td>
        </tr>
        <tr>
        <td>6501</td>
        <td>KABUPATEN BULUNGAN</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 13.700.000.000</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>6571</td>
        <td>KOTA TARAKAN</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 10.500.000.000</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>7101</td>
        <td>KABUPATEN BOLAANG MONGONDOW</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 13.645.000.000</td>
        <td>1</td>
        <td>4</td>
        </tr>
        <tr>
        <td>7102</td>
        <td>KABUPATEN MINAHASA</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 13.035.338.022</td>
        <td>1</td>
        <td>7</td>
        </tr>
        <tr>
        <td>7103</td>
        <td>KABUPATEN KEPULAUAN SANGIHE</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 1.500.000.000</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>7106</td>
        <td>KABUPATEN MINAHASA UTARA</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 6.900.000.000</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>7172</td>
        <td>KOTA BITUNG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 5.700.000.000</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>7173</td>
        <td>KOTA TOMOHON</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 8.010.055.617</td>
        <td>1</td>
        <td>4</td>
        </tr>
        <tr>
        <td>7201</td>
        <td>KABUPATEN BANGGAI</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 4.963.500.000</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>7202</td>
        <td>KABUPATEN POSO</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 7.674.097.450</td>
        <td>1</td>
        <td>5</td>
        </tr>
        <tr>
        <td>7203</td>
        <td>KABUPATEN DONGGALA</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 15.504.800.000</td>
        <td>1</td>
        <td>5</td>
        </tr>
        <tr>
        <td>7271</td>
        <td>KOTA PALU</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 11.333.593.500</td>
        <td>1</td>
        <td>4</td>
        </tr>
        <tr>
        <td>7302</td>
        <td>KABUPATEN BULUKUMBA</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 11.196.512.110</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>7303</td>
        <td>KABUPATEN BANTAENG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 3.957.933.300</td>
        <td>1</td>
        <td>4</td>
        </tr>
        <tr>
        <td>7304</td>
        <td>KABUPATEN JENEPONTO</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 5.937.000.000</td>
        <td>1</td>
        <td>5</td>
        </tr>
        <tr>
        <td>7305</td>
        <td>KABUPATEN TAKALAR</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 7.426.815.000</td>
        <td>1</td>
        <td>6</td>
        </tr>
        <tr>
        <td>7306</td>
        <td>KABUPATEN GOWA</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 19.965.772.400</td>
        <td>1</td>
        <td>5</td>
        </tr>
        <tr>
        <td>7307</td>
        <td>KABUPATEN SINJAI</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 50.084.000.000</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>7308</td>
        <td>KABUPATEN BONE</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 16.117.544.000</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>7309</td>
        <td>KABUPATEN MAROS</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 5.560.247.935</td>
        <td>1</td>
        <td>3</td>
        </tr>
        <tr>
        <td>7310</td>
        <td>KABUPATEN PANGKAJENE KEPULAUAN</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 2.729.200.000</td>
        <td>1</td>
        <td>2</td>
        </tr>
        <tr>
        <td>7311</td>
        <td>KABUPATEN BARRU</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 4.266.000.000</td>
        <td>1</td>
        <td>4</td>
        </tr>
        <tr>
        <td>7312</td>
        <td>KABUPATEN SOPPENG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 547.200.000</td>
        <td>1</td>
        <td>1</td>
        </tr>
        <tr>
        <td>7313</td>
        <td>KABUPATEN WAJO</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 2.637.000.000</td>
        <td>1</td>
        <td>6</td>
        </tr>
        <tr>
        <td>7314</td>
        <td>KABUPATEN SIDENRENG RAPPANG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 16.505.000.000</td>
        <td>1</td>
        <td>13</td>
        </tr>
        <tr>
        <td>7315</td>
        <td>KABUPATEN PINRANG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 9.870.821.548</td>
        <td>1</td>
        <td>4</td>
        </tr>
        <tr>
        <td>7316</td>
        <td>KABUPATEN ENREKANG</td>
        <td>Belum Terverifikasi</td>
        <td>IDR 10.858.635.650</td>
        <td>1</td>
        <td>4</td>
        </tr>
   </table>