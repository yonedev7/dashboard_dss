<table class="table table-bordered">
                <tbody>
                    <thead class="bg-primary">
                        <tr>
                            <th>Kode PEMDA</th>
                            <th>Nama Pemda</th>
                            <th>Status Verifikasi</th>

                            <th>Pagu</th>
                            <th>Jumlah Kegiatan</th>
                            <th>Jumlah Sub Kegiatan</th>



                        </tr>
                    </thead>
                    <tr>
                        <td>1101</td>
                        <td>KABUPATEN ACEH SELATAN</td>
                        <td>Belum Terverifikasi</td>

                        <td>IDR 948.096.732</td>
                        <td>1</td>
                        <td>21</td>
                    </tr>
            
                    <tr>
                        <td>1102</td>
                        <td>KABUPATEN ACEH TENGGARA</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 3.032.000.000</td>
                        <td>1</td>
                        <td>4</td>
                        </tr>
                        <tr>
                        <td>1104</td>
                        <td>KABUPATEN ACEH TENGAH</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 16.450.850.000</td>
                        <td>1</td>
                        <td>21</td>
                        </tr>
                        <tr>
                        <td>1105</td>
                        <td>KABUPATEN ACEH BARAT</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 1.850.000.000</td>
                        <td>1</td>
                        <td>13</td>
                        </tr>
                        <tr>
                        <td>1106</td>
                        <td>KABUPATEN ACEH BESAR</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 10.725.900.004</td>
                        <td>1</td>
                        <td>6</td>
                        </tr>
                        <tr>
                        <td>1108</td>
                        <td>KABUPATEN ACEH UTARA</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 15.436.710.500</td>
                        <td>1</td>
                        <td>3</td>
                        </tr>
                        <tr>
                        <td>1116</td>
                        <td>KABUPATEN ACEH TAMIANG</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 6.550.000.000</td>
                        <td>1</td>
                        <td>6</td>
                        </tr>
                        <tr>
                        <td>1171</td>
                        <td>KOTA BANDA ACEH</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 24.041.119.343</td>
                        <td>1</td>
                        <td>7</td>
                        </tr>
                        <tr>
                        <td>1172</td>
                        <td>KOTA SABANG</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 680.000.000</td>
                        <td>1</td>
                        <td>3</td>
                        </tr>
                        <tr>
                        <td>1174</td>
                        <td>KOTA LANGSA</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 45.250.373</td>
                        <td>1</td>
                        <td>2</td>
                        </tr>
                        <tr>
                        <td>1202</td>
                        <td>KABUPATEN TAPANULI UTARA</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 785.833.870</td>
                        <td>1</td>
                        <td>3</td>
                        </tr>
                        <tr>
                        <td>1204</td>
                        <td>KABUPATEN NIAS</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 9.987.063.000</td>
                        <td>1</td>
                        <td>2</td>
                        </tr>
                        <tr>
                        <td>1205</td>
                        <td>KABUPATEN LANGKAT</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 1.463.400.000</td>
                        <td>1</td>
                        <td>7</td>
                        </tr>
                        <tr>
                        <td>1206</td>
                        <td>KABUPATEN KARO</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 450.000.000</td>
                        <td>1</td>
                        <td>4</td>
                        </tr>
                        <tr>
                        <td>1207</td>
                        <td>KABUPATEN DELI SERDANG</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 16.519.098.052</td>
                        <td>1</td>
                        <td>7</td>
                        </tr>
                        <tr>
                        <td>1208</td>
                        <td>KABUPATEN SIMALUNGUN</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 45.500.000.000</td>
                        <td>1</td>
                        <td>4</td>
                        </tr>
                        <tr>
                        <td>1209</td>
                        <td>KABUPATEN ASAHAN</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 3.096.540.000</td>
                        <td>1</td>
                        <td>3</td>
                        </tr>
                        <tr>
                        <td>1210</td>
                        <td>KABUPATEN LABUHANBATU</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 5.891.800.000</td>
                        <td>1</td>
                        <td>4</td>
                        </tr>
                        <tr>
                        <td>1211</td>
                        <td>KABUPATEN DAIRI</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 1.250.000.000</td>
                        <td>1</td>
                        <td>4</td>
                        </tr>
                        <tr>
                        <td>1271</td>
                        <td>KOTA MEDAN</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 10.200.000.000</td>
                        <td>1</td>
                        <td>5</td>
                        </tr>
                        <tr>
                        <td>1272</td>
                        <td>KOTA PEMATANGSIANTAR</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 5.720.000.000</td>
                        <td>1</td>
                        <td>1</td>
                        </tr>
                        <tr>
                        <td>1273</td>
                        <td>KOTA SIBOLGA</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 1.425.000.000</td>
                        <td>1</td>
                        <td>2</td>
                        </tr>
                        <tr>
                        <td>1274</td>
                        <td>KOTA TANJUNG BALAI</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 1.950.000.000</td>
                        <td>1</td>
                        <td>8</td>
                        </tr>
                        <tr>
                        <td>1275</td>
                        <td>KOTA BINJAI</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 300.000.000</td>
                        <td>1</td>
                        <td>2</td>
                        </tr>
                        <tr>
                        <td>1276</td>
                        <td>KOTA TEBING TINGGI</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 6.500.000.000</td>
                        <td>1</td>
                        <td>4</td>
                        </tr>
                        <tr>
                        <td>1277</td>
                        <td>KOTA PADANGSIDIMPUAN</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR -</td>
                        <td>1</td>
                        <td>1</td>
                        </tr>
                        <tr>
                        <td>1302</td>
                        <td>KABUPATEN SOLOK</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 14.125.236.000</td>
                        <td>1</td>
                        <td>1</td>
                        </tr>
                        <tr>
                        <td>1303</td>
                        <td>KABUPATEN SIJUNJUNG</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 1.348.071.352</td>
                        <td>1</td>
                        <td>3</td>
                        </tr>
                        <tr>
                        <td>1304</td>
                        <td>KABUPATEN TANAH DATAR</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 12.892.609.600</td>
                        <td>1</td>
                        <td>2</td>
                        </tr>
                        <tr>
                        <td>1305</td>
                        <td>KABUPATEN PADANG PARIAMAN</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 5.830.750.000</td>
                        <td>1</td>
                        <td>7</td>
                        </tr>
                        <tr>
                        <td>1306</td>
                        <td>KABUPATEN AGAM</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 32.720.000.000</td>
                        <td>1</td>
                        <td>5</td>
                        </tr>
                        <tr>
                        <td>1371</td>
                        <td>KOTA PADANG</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR -</td>
                        <td>1</td>
                        <td>1</td>
                        </tr>
                        <tr>
                        <td>1372</td>
                        <td>KOTA SOLOK</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR -</td>
                        <td>1</td>
                        <td>3</td>
                        </tr>
                        <tr>
                        <td>1373</td>
                        <td>KOTA SAWAHLUNTO</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 3.800.000.000</td>
                        <td>1</td>
                        <td>3</td>
                        </tr>
                        <tr>
                        <td>1375</td>
                        <td>KOTA BUKITTINGGI</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 3.197.191.000</td>
                        <td>1</td>
                        <td>3</td>
                        </tr>
                        <tr>
                        <td>1376</td>
                        <td>KOTA PAYAKUMBUH</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 1.914.034.820</td>
                        <td>1</td>
                        <td>1</td>
                        </tr>
                        <tr>
                        <td>1401</td>
                        <td>KABUPATEN KAMPAR</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 1.650.000.000</td>
                        <td>1</td>
                        <td>8</td>
                        </tr>
                        <tr>
                        <td>1403</td>
                        <td>KABUPATEN BENGKALIS</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 63.914.231.500</td>
                        <td>1</td>
                        <td>5</td>
                        </tr>
                        <tr>
                        <td>1404</td>
                        <td>KABUPATEN INDRAGIRI HILIR</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR -</td>
                        <td>1</td>
                        <td>2</td>
                        </tr>
                        <tr>
                        <td>1471</td>
                        <td>KOTA PEKANBARU</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 17.863.339.600</td>
                        <td>1</td>
                        <td>3</td>
                        </tr>
                        <tr>
                        <td>1472</td>
                        <td>KOTA DUMAI</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 13.300.000.000</td>
                        <td>1</td>
                        <td>4</td>
                        </tr>
                        <tr>
                        <td>1501</td>
                        <td>KABUPATEN KERINCI</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 7.000.000.000</td>
                        <td>1</td>
                        <td>3</td>
                        </tr>
                        <tr>
                        <td>1502</td>
                        <td>KABUPATEN MERANGIN</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 11.932.827.500</td>
                        <td>1</td>
                        <td>4</td>
                        </tr>
                        <tr>
                        <td>1504</td>
                        <td>KABUPATEN BATANGHARI</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 4.237.437.000</td>
                        <td>1</td>
                        <td>8</td>
                        </tr>
                        <tr>
                        <td>1508</td>
                        <td>KABUPATEN BUNGO</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 799.086.200</td>
                        <td>1</td>
                        <td>5</td>
                        </tr>
                        <tr>
                        <td>1601</td>
                        <td>KABUPATEN OGAN KOMERING ULU</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 3.500.460.800</td>
                        <td>1</td>
                        <td>4</td>
                        </tr>
                        <tr>
                        <td>1603</td>
                        <td>KABUPATEN MUARA ENIM</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 9.878.486.366</td>
                        <td>1</td>
                        <td>5</td>
                        </tr>
                        <tr>
                        <td>1604</td>
                        <td>KABUPATEN LAHAT</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 85.142.500.000</td>
                        <td>1</td>
                        <td>3</td>
                        </tr>
                        <tr>
                        <td>1606</td>
                        <td>KABUPATEN MUSI BANYUASIN</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 27.575.638.800</td>
                        <td>1</td>
                        <td>3</td>
                        </tr>
                        <tr>
                        <td>1701</td>
                        <td>KABUPATEN BENGKULU SELATAN</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 8.097.374.000</td>
                        <td>1</td>
                        <td>5</td>
                        </tr>
                        <tr>
                        <td>1702</td>
                        <td>KABUPATEN REJANG LEBONG</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 3.104.621.521</td>
                        <td>1</td>
                        <td>5</td>
                        </tr>
                        <tr>
                        <td>1801</td>
                        <td>KABUPATEN LAMPUNG SELATAN</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 2.123.937.400</td>
                        <td>1</td>
                        <td>5</td>
                        </tr>
                        <tr>
                        <td>1802</td>
                        <td>KABUPATEN LAMPUNG TENGAH</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 12.528.426.700</td>
                        <td>1</td>
                        <td>4</td>
                        </tr>
                        <tr>
                        <td>1901</td>
                        <td>KABUPATEN BANGKA</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 16.957.385.670</td>
                        <td>1</td>
                        <td>5</td>
                        </tr>
                        <tr>
                        <td>1902</td>
                        <td>KABUPATEN BELITUNG</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 5.979.684.783</td>
                        <td>1</td>
                        <td>5</td>
                        </tr>
                        <tr>
                        <td>5315</td>
                        <td>KABUPATEN MANGGARAI BARAT</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 6.686.760.000</td>
                        <td>1</td>
                        <td>2</td>
                        </tr>
                        <tr>
                        <td>9103</td>
                        <td>KABUPATEN JAYAPURA</td>
                        <td>Belum Terverifikasi</td>
                        <td>IDR 6.238.808.000</td>
                        <td>1</td>
                        <td>1</td>
                        </tr>
                    
                </tbody>
            </table>