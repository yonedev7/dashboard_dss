@extends('adminlte::page')
@section('content_header')


@endsection
@section('content')
<div id="app" >

    <div class="parallax mb-5 pt-3"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
        <div class="container">
            <section class="">
                <h3><b>KATALOG METADATA - </b> <small>{{number_format($jumlah_data,0,',','.')}} Katalog Data Terinput</small></h3>
                <p><b>Informasi Terkait Standar dan Meta Data </b></p>
                <p>Metadata adalah informasi dalam bentuk struktur dan format yang baku untuk menggambarkan Data, menjelaskan Data, serta memudahkan pencarian, penggunaan, dan pengelolaan informasi Data</p>
                <p>Komponen Standar Data Disusun Oleh Walidata yang Terdiri Dari : Konsep, Definisi, Klasifikasi, Ukuran dan Satuan</p>
                <p>Lebih Lanjut Dapat Dibaca Pada <a href="https://www.bps.go.id/website/fileMenu/Perpres-No-39-Tahun-2019.pdf">PERPRES NO.39 Tahun 2019</a>  </p>
                <p><span class="badge badge-warning p-1">Terbarui Pada : @{{updated}}</span></p>
               <div class="row ">
                <div class=" col-md-12 " >
                    <div class="input-group mb-3 input-group-lg" style="z-index:99;position: absolute; width:50%; bottom:-50px; right:0px;">
                        <input type="text" v-model="q" class="form-control input-xl" placeholder="Cari Katalog Data" aria-label="Cari Katalog Data" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                        <button @click="get_data" class="btn btn-primary btn-xl" type="button"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
            </div>
               </div>
            </section>
          
        </div>
       
    
        
    </div>
    <div class="card" data-app style="width:100%">
        <div class="card-body">
            <table class="table table-bordered" style="width:100%;" id="table-catalog">
                <thead>
                    <tr class="bg-primary">
                        <th v-for="item in table_datum.headers">@{{item.text}}</th>
                        
                    </tr>
                    <tr>
                        <th v-for="(item,key) in table_datum.headers">@{{key+1}}</th>

                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
   
</div>
@stop


@section('js')
<script>
    var v_app=new Vue({
        el:'#app',
        watch:{
            q:function(){
                this.get_data();
            }
        },
        data:{
            q:'',
            updated:'{{$last_updated}}',
            table_datum:{
                headers:[
                    {
                        text:'#index',
                        data:'id'
                    },
                    {
                        text:'Validator',
                        data:'nama_sumber',

                    },
                    {
                        text:'Kode Referensi',
                        data:'kode',

                    },
                    {
                        text:'Nama Data',
                        data:'name',
                        sortable:false,


                    },
                    {
                        text:'Tipe Nilai',
                        data:'tipe_nilai',
                        render:function(a,s,item){
                            
                            return (item.tipe_nilai||'').toUpperCase();
                        }

                    },
                    {
                        text:'Arah Nilai',
                        data:'arah_nilai',
                        render:function(a,s,item){
                            return item.arah_nilai==0?'NETRAL':(item.arah_nilai==1?'POSITIF':'NEGATIF');
                        }

                    },
                    {
                        text:'Satuan',
                        data:'satuan',
                        sortable:false,


                    },
                    {
                        text:'Definisi / Konsep',
                        data:'definisi_konsep',
                        sortable:false,


                    },
                    {
                        text:'Cara Hitung',
                        data:'cara_hitung',
                        sortable:false,


                    },
                   
                   
                   
                ],
                datasets:<?=json_encode($data)?>,
                table:null


            },
          
    },
    methods:{
                init:function(){
                   this.table_datum.table= $('#table-catalog').DataTable({
                        data:v_app.table_datum.datasets,
                        columns:v_app.table_datum.headers,
                        dom: 'tp'
                    })
                },
                get_data:function(){
                    this.table_datum.table.search(this.q).draw();
                },
                rentang_show:function(val){
                    if(val['tipe']!==undefined){
                        switch(val.tipe){
                            case 'pilihan':
                            return val['sort'];
                            break;
                            case 'min-max':
                                return (val.sort.join(' - '));
                            break;
                            default:
                            return '';
                            break;
                        }
                    }else{
                        return '-';

                    }
                },
            }
        }
    
    );
    v_app.init();   
</script>

@stop