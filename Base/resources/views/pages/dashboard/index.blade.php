@extends('adminlte::page')

@section('content')
<div id="dashboard">
    <v-parallax  src="https://cdn.vuetifyjs.com/images/parallax/material.jpg">
        <v-card  elevation="1" class="mb-4">
            <v-card-title>KPI BANGDA</v-card-title>  
            <v-card-subtitle>Data Record </v-card-subtitle>  
            <v-card class="d-flex flex-row mb-6" >
                <v-card  class=" col-3 red" >
                    <v-card-subtitle>40 Pemda meningkatkan dukungan finansial kepada PDAM melalui peningkatan kontribusi modalnya, pemberian hibah, atau penyediaan jaminan bagi PDAM untuk mendapatkan pembiayaan non-publik </v-card-subtitle>
                </v-card>
                <v-card  class="col-3" >
                    <v-card-subtitle>20 Pemda/PDAM mendapatkan Bantuan Program Pendamping sebagai hasil utilisasi pembiayaan non-publik untuk pembangunan infrastruktur</v-card-subtitle>
                </v-card>
                <v-card  class="col-3" >
                    <v-card-subtitle>200 Pemda Berpartisipasi Aktif Dalam Program Pelatihan dan Peningkatan Kapasitas</v-card-subtitle>
                </v-card>
                <v-card  class="col-3" >
                    <v-card-subtitle>50 PDAM telah mencapai tarif yang full cost recovery</v-card-subtitle>
                </v-card>
             </v-card>  
    
        </v-card>
    
    </v-parallax>

    <v-parallax  src="https://cdn.vuetifyjs.com/images/parallax/material.jpg">
        <v-card   elevation="1" class="mb-4">
            <v-card-title>Keterisian Data Perencanaan</v-card-title>  
            <v-card-subtitle>Data Record </v-card-subtitle>  
            <v-card class="d-flex flex-row mb-6" >
                <v-card class="col-4">
                    <v-card-title>Indikator RPJMN IV</v-card-title>
                    <v-card-subtitle>76 Indikator</v-card-subtitle>
                    <v-card-title>Indikator RPJMN IV</v-card-title>
                    <v-card-subtitle>76 Indikator</v-card-subtitle>
                </v-card>
                <v-card  class=" col-4 red" >
                    <highcharts  :options="chart_RPJMD" ></highcharts>
                </v-card>
                <v-card  class="col-4" >
                    <highcharts :options="chart_RKPD" ></highcharts>
                </v-card>
               
              
             </v-card>  
    
        </v-card>
    
    </v-parallax>
</div>
@stop

@section('js')
    <script>
        v_dash=new Vue({
            el:'#dashboard',
            vuetify: new Vuetify(),
            data:{
                chart_RPJMD: {
            chart: {
                height:300,
                type:'pie',
                
            },
            title:{
                    text:'Pelaporan RPJMD'
                },
                plotOptions: {
                pie: {
                    size: '100%',
                    center: ['50%', '50%'] ,
                    dataLabels: {
                distance: -1,}
                }
            },
            series: [
                {
                    data: [
                        {
                            name:'Terinput',
                            y:3,
                            color:'#53e3d2'
                            
                        },
                        {
                            name:'Belum Terinput',
                            y:3,
                            color:'#ff6c6b',

                          
                        }
                    ] // sample data
                }
            ]
        },
        chart_RKPD: {
            chart: {
                height:300,
                type:'pie',
               
                
            },
            title:{
                    text:'Pelaporan RKPD'
                },
            series: [
                {
                    name:'Daerah',
                    data: [
                        {
                            name:'Terinput',
                            y:3,
                            color:'#53e3d2'
                            
                        },
                        {
                            name:'Belum Terinput',
                            y:3,
                            color:'#ff6c6b',

                          
                        }
                    ] // sample data
                }
            ]
        },
        chart_DPA: {
            chart: {
                height:300,
                type:'pie',
                
            },
            title:{
                    text:'Pelaporan DPA'
                },
            series: [
                {
                    data: [
                        {
                            name:'Terinput',
                            y:3,
                            color:'#53e3d2'
                            
                        },
                        {
                            name:'Belum Terinput',
                            y:3,
                            color:'#ff6c6b',

                          
                        }
                    ] // sample data
                }
            ]
        }
            }

        });
    </script>

@stop