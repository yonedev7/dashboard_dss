@extends('adminlte::page')


@section('content')
<style>
    .list-group-item.direct:hover{
        cursor: pointer;
        -webkit-transform: scale(1.1); 
    }
</style>
@php

@endphp



<div class="parallax"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
    <div class="container">
        <section >
            <h2><b>PROFILE PEMDA</b></h2>
            <h3><b>{{$pemda->nama_pemda}}</b> {{$Stahun}}</h3>
            <p></p>
        </section>
      
    </div>
   
</div>
<div class=" d-flex">
    <div class="col-3 bg-gray" style="height:70vh;" id="side-menu-profile">
        <div class="btn-group mb-2 mt-3" v-if="print">
            <button class="btn btn-primary" onclick="print_profile('content_printed')"><i class="fa fa-print"></i> Print</button>
        </div>
        <ul class="list-group text-dark font-weight-bold">
            <li class="list-group-item direct" v-for="item,key in listed" @click="bind_scroll(key)">
                @{{item.title}}
            </li>
            
        </ul>
    </div>
    <div class="flex-grow-1" id="content_printed">
        <div class=" p-3 " id="content_inner_print"  style="height:70vh; overflow-y:scroll">
             <section id="bab-x" class="pg_break d-flex ml-5  flex-column p-3" style="min-height:100vh; background-size: cover; background-image: url('{{url('assets/img/backcover.png')}}');">
                <div class="flex-grow-1 d-flex">
                    <div class="align-self-center mb-4 col-12">
                        <div class="text-center text-primary">
                            <h1 style="font-size:80px;"><b>PROFIL</b></h1>
                            <h1 style="font-size:65px;" class="text-primary"><b> {{$pemda->nama_pemda}}</b></h1>
                            <h2><b>BIDANG AIR MINUM PERKOTAAN<br>LINGKUP NUWSP TAHUN {{$Stahun}}</b></h2>
                            <p >{{($pemda->regional_1?'Regional '.$pemda->regional_1:'').' '.($pemda->regional_2?'Wilayah '.$pemda->regional_2:'')}}</p>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-center mb-3" >
                    <div class="d-flex justify-content-center">
                        <div class="mr-2 d-flex">
                            <img src="{{asset('assets/img/brand/logonuwas.png')}}" class="align-self-center" style="max-width:80px" alt="">
                        </div>
                        <div class="mr-2 d-flex">
                            <img src="{{asset('assets/img/brand/logo.png')}}" class="align-self-center" style="max-width:80px" alt="">
                        </div>
                        <div class="flex-grow-1 p-2 ">
                            <h4 class=" m-0"><b>DATA SUPORT SYSTEM {{$Stahun}}</b></h4>
                            <h4  class=" m-0"><b>NASIONAL URBAN WATER SUPLAY PROJECT</b></h4>
    
                        </div>
                        
                    </div>
                </div>
                <div class="d-flex justify-content-center mb-3 border-dark border-top pt-3" >
                    <div class="d-flex justify-content-center">
                        <div class="mr-2 d-flex">
                            <img src="{{asset('assets/img/brand/logokemen.png')}}" class="align-self-center" style="max-width:50px" alt="">
                        </div>
                        <div class="flex-grow-1 p-2 ">
                            <h4 class=" m-0"><b>DITJEN BINA PEMBANGUNAN DAERAH</b></h4>
                            <h4  class=" m-0"><b>KEMENTERIAN DALAM NEGERI</b></h4>
    
                        </div>
                        
                    </div>
                </div>
    
                
            </section>
            <section v-bind:id="'bab-'+(key)" class="pg_break  ml-5 mr-4 " v-for="item,key in listed">
                <div class="d-flex mb-4 border-bottom border-dark" v-if="key!=0">
                    <div class="mr-2 d-flex">
                        <img src="{{asset('assets/img/brand/logo.png')}}" class="align-self-center" style="max-width:50px" alt="">
                    </div>
                    <div class="flex-grow-1 p-2 ">
                        <p class=" m-0"><b>PROFILE {{$pemda->nama_pemda}} {{$Stahun}}</b></p>
                    </div>
                </div>
                <div class="text-center mb-4 mt-4 border-bottom border-dark" v-if="key==0">
                    <h4><b>PROFIL {{$pemda->nama_pemda}}</b></h4>
                    <h5><b>BIDANG AIR MINUM PERKOTAAN</b></h5>
                    <h5><b>LINGKUP NUWSP TAHUN {{$Stahun}}</b></h5>
                    <p >{{($pemda->regional_1?'Regional '.$pemda->regional_1:'').' '.($pemda->regional_2?'Wilayah '.$pemda->regional_2:'')}}</p>

                </div>
                <span v-bind:id="'bab-'+(key)+'-scroll'"></span>
                <h4 class="bg-primary p-3 rounded mb-4"><b>@{{(key+1)+'. '+item.title}}</b></h4>
                    <div class="ml-4" v-bind:id="'bab-'+(key)+'-content'" v-if="item.rendered" v-append.sync="item.content"></div>
                    <div class="ml-4">
                        <template v-for="sub,ks in item.sub">
                             <h5  class="bg-info p-3 rounded mb-4"><b>@{{(key+1)+'.'+(ks+1)+' '+sub.title}}</b></h5>
                                <div class="ml-4" v-bind:id="'bab-'+(key)+'-'+ks+'-content'" v-if="sub.rendered"  v-append.sync="sub.content"></div>
                        </template>
                    </div>
               
            </section>
          
            
        </div>
    </div>

</div>

@stop

@push('css')
<link rel="stylesheet" href="{{asset('css/print_profile.css')}}">
@endpush
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.print/1.6.2/jQuery.print.min.js" integrity="sha512-t3XNbzH2GEXeT9juLjifw/5ejswnjWWMMDxsdCg4+MmvrM+MwqGhxlWeFJ53xN/SBHPDnW0gXYvBx/afZZfGMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
   function print_profile(id_dom){
       
            // var printContents = document.getElementById('content_inner_print').innerHTML;
			// var originalContents = document.body.innerHTML;

			// document.body.innerHTML = printContents;

			// window.print();

			// document.body.innerHTML = originalContents;

            $("#content_inner_print").print({
        	globalStyles: true,
        	mediaPrint: true,
        	stylesheet: null,
        	noPrintSelector: ".no-print",
        	iframe: true,
        	append: null,
        	prepend: null,
        	manuallyCopyFormValues: true,
        	deferred: $.Deferred(),
        	timeout: 6000,
        	title: 'PROFIL {{$pemda->nama_pemda}}',
        	doctype: '<!doctype html>'
	});
   
   }

  

   var side_menu=new Vue({
       el:'#side-menu-profile',
       data:{
           listed:[],
           print:false
       },
       methods:{
           bind_scroll:function(bab){
                $('#content_inner_print').animate({
                        scrollTop: ($("#bab-"+bab+'-scroll')[0].offsetTop-$('#content_printed').offset().top)-50
                }, 1500);
           }
       }
   });

   var content_profile=new Vue({
       el:'#content_printed',
       data:{
        listed:<?=json_encode($SmenuProfilePemda)?>,
        content_sementara:[],
        data:<?=json_encode($meta_profile)?>,
        ajax_load:[],
        deleted:[]
       },
       methods:{
            load:function(){
                return new Promise((resolve, reject) => {
                    this.listed.forEach((el,key,arr) => {
                        this.listed[key].content=this.data[el.field];

                        el.sub.forEach((elsub,keysub,arr2)=>{
                        if(this.data[elsub.field]){
                            this.listed[key].sub[keysub].content=this.data[elsub.field];
                        }else{
                            this.deleted.push([key,keysub]);
                        }
                        if((arr.length-1)==key && (arr2.length-1)==keysub ){
                            this.content_sementara=Object.assign(this.content_sementara,this.listed);
                            resolve(this.listed);
                        }
                        
                        });
                    });
                });
            },
            checkSubNull:function(){
                return new Promise((resolve, reject) => {
                    if(this.deleted.length==0){
                        resolve(this.listed);

                    }
                    this.deleted.reverse().forEach((el,key,arr)=>{
                        this.content_sementara[el[0]].sub.splice(el[1],1);
                        if((this.deleted.length-1)==key){
                            this.listed=Object.assign(this.listed,this.content_sementara);
                            resolve(this.listed);
                        }
                    });
                });
            },
            finalCheck:function(){
                return new Promise((resolve,reject)=>{

                    this.listed=this.listed.filter(el=>{
                        if(el){
                            return el;
                        }else{
                            return fasle;
                        }
                    });
                    let selt=this;
                    this.listed.forEach((el,key,arr) => {
                        if(el.sub.length==0 && (el.content==''|| el.content==null) ){
                            this.listed[key]=false;   
                        }
                        if((arr.length-1)==key){
                            this.listed=this.listed.filter(el=>{
                                return el;
                            });
                            resolve(this.listed);

                        }
                    });
                });

            },
            listLoad:function(){
                return new Promise((resolve,reject)=>{
                    var listed_load=[];
                    this.listed.forEach((el,k,ar)=>{
                        listed_load.push([k]);
                        el.sub.forEach((el2,k2,ar2)=>{
                            listed_load.push([k,k2]);
                            if((ar.length-1)==k && (ar2.length-1)==k2){
                                resolve(listed_load);
                            }
                        });
                    });
                });
            },
            loadContent:function(listed_load){
                selt=this;

                return new Promise(async (resolve,reject)=>{
                    for(k in listed_load){
                        el=listed_load[k];
                        await req_ajax.post('{{route('api-web.mod-profilepemdamodule.load_profile_content',['tahun'=>$Stahun])}}',{
                            kodepemda:'{{$pemda->kodepemda}}',
                            content:el.length<2? selt.listed[el[0]].content:selt.listed[el[0]].sub[el[1]].content
                        }).then((res)=>{
                            if(el.length==1){
                                selt.listed[el[0]].content=res.data.content;
                                selt.listed[el[0]].rendered=true;
                               
                            }else{
                                selt.listed[el[0]].sub[el[1]].content=res.data.content;
                                selt.listed[el[0]].sub[el[1]].rendered=true;
                               

                            }
    
                        });

                        if((listed_load.length-1)==k){
                            setInterval(() => {
                                window.side_menu.print=true;
                                resolve(true);
                                
                            }, 1000);
                        }
                    }
                });

            }

            
       },
       created:function(){
        window.side_menu.listed=this.listed;
        this.content_sementara=this.listed;
        let selt=this;
        this.load().then(function(res){
            console.log('1');

            selt.checkSubNull().then(res2=>{
                console.log('2');

                selt.finalCheck().then(res3=>{
                    console.log('3');

                    selt.listLoad().then(res4=>{
                        console.log('4');
                        selt.loadContent(res4).then(res5=>{
                            
                        });
                    });
                });
            });
        });


       },
       watch:{
           listed:function(){
               window.side_menu.listed=this.listed;
           }
       }
   });
</script>

@stop