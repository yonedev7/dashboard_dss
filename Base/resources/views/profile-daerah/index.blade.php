@extends('adminlte::page')


@section('content')
<script>
    var page_meta=<?=json_encode($page_meta)?>;
    
</script>

<div class="parallax"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
    <div class="container">
        <section >
            <h3><b>{{$page_meta['title']}}</b></h3>
            <p><b>{!!$page_meta['keterangan']!!}</b></p>
           
        </section>
      
    </div>
   
</div>
@include('tematik.filter')

<div class="card">
    <div class="card-body">
        <div class="table-responsive">
            <table id="table-list" class="table table-bordered table-striped align-middle">
                <thead>
                    <tr class="align-middle">
                        <th rowspan="3" class="align-middle">Kodepemda</th>
                        <th rowspan="3" class="align-middle">Nama PEMDA</th>
                        <th colspan="8" class="align-middle">Profil PEMDA</th>
                        <th rowspan="3" class="align-middle">Detail Profile PEMDA</th>
                    </tr>
                    <tr >
                        <th colspan="3" class="align-middle">Data IKFD</th>
                        <th colspan="3" class="align-middle">Data Tipologi Urusan</th>
                        <th colspan="2" class="align-middle">Bantuan NUWAS</th>
                    </tr>
                     <tr>
                        <th>Nilai</th>
                        <th>Kategori</th>
                        <th>Tahun Data</th> 

                        <th>Kategori</th>
                        <th>OPD Pelaksana</th>
                        <th>Tahun Data</th>  
                        
                        <th>Tipe Bantuan</th>
                        <th>Tahun Proyek</th>  

                    </tr>
                    <tr class=" align-middle font-weight-bold bg-secondary">
                        <td>1</td>
                        <td>2</td>
                        <td>3</td>
                        <td>4</td>
                        <td>5</td>
                        <td>6</td>
                        <td>6</td>
                        <td>8</td>
                        <td>9</td>
                        <td>10</td>
                        <td>11</td>

                    </tr>
                    
                </thead>
                <tbody>
                    @foreach($data as $key=>$d)
                    @php
                    // dd($d);
                    @endphp
                    <tr class=" font-weight-bold">
                        <td>{{$d->kodepemda}}</td>
                        <td>{{$d->nama_pemda}}</td>
                        <td>{{$d->index_ikfd}}</td>
                        <td>{{$d->kategori_ikfd}}</td>
                        <td>{{$d->tahun_ikfd}}</td>
                        <td>{{$d->tp_pemda}}</td>
                        <td>{{$d->tp_opd}}</td>
                        <td>{{$d->tahun_tp}}</td>
                        <td>{{$d->tipe_bantuan}}</td>
                        <td>{{$d->tahun_proyek<2?'':$d->tahun_proyek}}</td>
                        <td><a href="{{route('data.profile-pemda-detail',['tahun'=>$Stahun,'kodepemda'=>$d->kodepemda])}}" class="btn btn-primary btn-sm"><i class="fa fa-file"></i></a></td>
                    </tr>

                    @endforeach
                 </tbody>
            </table>
        </div>
    </div>
</div>

@stop

@section('js')
<script>
$('#table-list').DataTable();
</script>

@stop