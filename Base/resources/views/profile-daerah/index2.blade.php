@extends('adminlte::page')

@section('content_header')
    <h3>Perencanaan Dan Pengangaran </h3>

@endsection
@section('content')

<div id="KPI-bangda">
    <v-parallax  src="https://cdn.vuetifyjs.com/images/parallax/material.jpg">
        <v-card   elevation="1" class="mb-4">
            <v-card-title>Keterisian</v-card-title>  
            <v-card-subtitle>Data Record </v-card-subtitle>  
            <v-card class="d-flex flex-row mb-6" >
                <v-card  class=" col-4 red" >
                    <highcharts  :options="chart_RPJMD" ></highcharts>
                </v-card>
                <v-card  class="col-4" >
                    <highcharts :options="chart_RKPD" ></highcharts>
                </v-card>
                <v-card  class="col-4" >
                    <highcharts :options="chart_DPA" ></highcharts>
                </v-card>
              
             </v-card>  
    
        </v-card>
    
    </v-parallax>
    <div class="container" >
      
        
        <v-card   elevation="1" class="mb-4">
            <v-card class="d-flex flex-row mb-6" >
               
                <v-card  class="col-4" elevation="1" >
                    <v-card-subtitle>RKPD Program / Kegiatan Komulatif</v-card-subtitle>
                    <p class="text-center ">20.3000 Program</p>  
                    <p class="text-center">20.3000 Kegiatan</p>   
    
    
    
                </v-card>
                <v-card  class="col-4" elevation="1" >
                    <v-card-subtitle>RKPD Pagu Komulatif</v-card-subtitle>
                    <p class="text-center">20.3000.002.000</p>   
                    
    
    
    
                </v-card>
                <v-card  class="col-4" elevation="1" >
                    <v-card-subtitle>DPA Komulatif</v-card-subtitle>
                    <p class="text-center">20.3000.002.000</p>   
                    
    
                </v-card>
              
             </v-card>  
           
        
        </v-card>
    
    </div>
    <v-parallax  src="https://cdn.vuetifyjs.com/images/parallax/material.jpg">
        <v-card   elevation="1" class="mb-4">
            <v-card-title>Statistik Dasar Per-Daerah</v-card-title>
            <v-card class="d-flex flex-row mb-6" >
            
                <v-card  class="col-4" elevation="1" >
                    <v-card-subtitle>RKPD - Program / Kegiatan Terbanyak</v-card-subtitle>
                    <v-list-item three-line>
                        <v-list-item-content>
                        <v-list-item-title>Jakarta Selatan</v-list-item-title>
                        <v-list-item-subtitle>
                            10 Program
                        </v-list-item-subtitle>
                        <v-list-item-subtitle>
                            300 Kegaiatan
                        </v-list-item-subtitle>
                        
                        </v-list-item-content>
                    </v-list-item>
                    <v-list-item three-line>
                        <v-list-item-content>
                        <v-list-item-title>Jakarta Selatan</v-list-item-title>
                        <v-list-item-subtitle>
                            10 Program
                        </v-list-item-subtitle>
                        <v-list-item-subtitle>
                            300 Kegaiatan
                        </v-list-item-subtitle>
                        
                        </v-list-item-content>
                    </v-list-item>
                    <v-list-item three-line>
                        <v-list-item-content>
                        <v-list-item-title>Jakarta Selatan</v-list-item-title>
                        <v-list-item-subtitle>
                            10 Program
                        </v-list-item-subtitle>
                        <v-list-item-subtitle>
                            300 Kegaiatan
                        </v-list-item-subtitle>
                        
                        </v-list-item-content>
                    </v-list-item>
                    
                

                </v-card>
                <v-card class="col-4" elevation="1">
                    <v-card-subtitle>RKPD - Pagu Tertinggi</v-card-subtitle>
                    <v-list-item>
                        <v-list-item-content>
                        <v-list-item-title>Jakarta Selatan</v-list-item-title>
                        <v-list-item-subtitle>
                            5000.0000 (Rp.) - Pagu
                        </v-list-item-subtitle>
                        
                        
                        </v-list-item-content>
                    </v-list-item>
                    <v-list-item>
                        <v-list-item-content>
                        <v-list-item-title>Jakarta Selatan</v-list-item-title>
                        <v-list-item-subtitle>
                            5000.0000 (Rp.) - Pagu
                        </v-list-item-subtitle>
                        
                        
                        </v-list-item-content>
                    </v-list-item>
                    <v-list-item>
                        <v-list-item-content>
                        <v-list-item-title>Jakarta Selatan</v-list-item-title>
                        <v-list-item-subtitle>
                            5000.0000 (Rp.) - Pagu
                        </v-list-item-subtitle>
                        
                        
                        </v-list-item-content>
                    </v-list-item>
                </v-card>
                <v-card  class="col-4" elevation="1" >
                    <v-card-subtitle>DPA Tertinggi</v-card-subtitle>
                    <v-list-item>
                        <v-list-item-content>
                        <v-list-item-title>Jakarta Selatan</v-list-item-title>
                        <v-list-item-subtitle>
                            5000.0000 (Rp.)
                        </v-list-item-subtitle>
                        
                        
                        </v-list-item-content>
                    </v-list-item>
                    <v-list-item>
                        <v-list-item-content>
                        <v-list-item-title>Jakarta Selatan</v-list-item-title>
                        <v-list-item-subtitle>
                            5000.0000 (Rp.)
                        </v-list-item-subtitle>
                        
                        
                        </v-list-item-content>
                    </v-list-item>
                    <v-list-item>
                        <v-list-item-content>
                        <v-list-item-title>Jakarta Selatan</v-list-item-title>
                        <v-list-item-subtitle>
                            5000.0000 (Rp.) 
                        </v-list-item-subtitle>
                        
                        
                        </v-list-item-content>
                    </v-list-item>

                </v-card>
            
            </v-card>  

        </v-card>
    </v-parallax>

    <div class="container">
        <v-card>
            <v-card-title>Detail Data</v-card-title>
          
              <v-card>
                <v-card-title>
                    <v-text-field
                      v-model="search"
                      append-icon="fa fa-search"
                      label="Pencarian"
                      single-line
                      hide-details
                    ></v-text-field>
                  </v-card-title>
                <v-data-table
                :headers="data_detail.header"
                :items="data_detail.data"
                :items-per-page="5"
                class="elevation-1"
                mobile-breakpoint="0"
            >
           
            </v-data-table>
              </v-card>
        </v-card>
    </div>
      
    
      
    
           
    
    </div>
    
</div>

@stop

@section('js')
<script>
    Vue.use(HighchartsVue);

    V_KPI_BANGDA=new Vue({
        el:"#KPI-bangda",
        vuetify: new Vuetify(),
        data :{
        data_detail:{
            header:[
                {
            text: 'Nama Derah',
            align: 'start',
            sortable: false,
            value: 'name',
          },
          { text: 'Data RPJMD', value: 'pel_RPJMD' },
          { text: 'Data RKPD', value: 'pel_RKPD' },
          { text: 'Data DPA', value: 'pel_DPA' },
            ],
            data:[
               {
                   name:'Kob. Aceh Barat',
                   pel_RPJMD:'Tersedia',
                   pel_RKPD:'Tersedia',
                   pel_DPA:'Tidak Tersedia',
               },
               {
                   name:'Kob. Aceh Selatan',
                   pel_RPJMD:'Tersedia',
                   pel_RKPD:'Tersedia',
                   pel_DPA:'Tidak Tersedia',
               },
               {
                   name:'Kob. Tapanuli Utara',
                   pel_RPJMD:'Tersedia',
                   pel_RKPD:'Tersedia',
                   pel_DPA:'Tidak Tersedia',
               }
            ]
        },
        chart_RPJMD: {
            chart: {
                height:300,
                type:'pie',
                
            },
            title:{
                    text:'Pelaporan RPJMD'
                },
                plotOptions: {
                pie: {
                    size: '100%',
                    center: ['50%', '50%'] ,
                    dataLabels: {
                distance: -1,}
                }
            },
            series: [
                {
                    data: [
                        {
                            name:'Terinput',
                            y:3,
                            color:'#53e3d2'
                            
                        },
                        {
                            name:'Belum Terinput',
                            y:3,
                            color:'#ff6c6b',

                          
                        }
                    ] // sample data
                }
            ]
        },
        chart_RKPD: {
            chart: {
                height:300,
                type:'pie',
               
                
            },
            title:{
                    text:'Pelaporan RKPD'
                },
            series: [
                {
                    name:'Daerah',
                    data: [
                        {
                            name:'Terinput',
                            y:3,
                            color:'#53e3d2'
                            
                        },
                        {
                            name:'Belum Terinput',
                            y:3,
                            color:'#ff6c6b',

                          
                        }
                    ] // sample data
                }
            ]
        },
        chart_DPA: {
            chart: {
                height:300,
                type:'pie',
                
            },
            title:{
                    text:'Pelaporan DPA'
                },
            series: [
                {
                    data: [
                        {
                            name:'Terinput',
                            y:3,
                            color:'#53e3d2'
                            
                        },
                        {
                            name:'Belum Terinput',
                            y:3,
                            color:'#ff6c6b',

                          
                        }
                    ] // sample data
                }
            ]
        }
        
    },
    methods:{
        pushDt:function(){
            this.chartOptions.series[0].data.push(Math.random(0,199));
        }
    }
  
});

</script>
@stop