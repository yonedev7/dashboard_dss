@extends('adminlte::page')


@section('content')
    <table class="table table-bordered">
        <thead>
            <th>NAME</th>
            <th>URL SCHEMA</th>
            <th>METHOD</th>

        </thead>
        <tbody>
            @foreach($routes as $route)
            <tr>
                <td>
                    @if(gettype($route->getName())=='string')
                    {{$route->getName()}}
                    @else
                        -
                    @endif
                    </td>
                
                <td>
                    @if(gettype($route->uri())=='string')
                        {{$route->uri()}}
                    @else
                        @php
                            dd('uri',$route->uri());
                        @endphp
                    @endif
                </td>
                
                <td>
                    @if(gettype($route->getAction()['uses'])=='string')
                    {{($route->getAction()['uses'])}}
                    @else
                        -
                    @endif
                    </td>

              
            </tr>
            @endforeach 
        </tbody>
    </table>
@stop

@section('js')
<script>
    $('.table').DataTable();
</script>
@stop