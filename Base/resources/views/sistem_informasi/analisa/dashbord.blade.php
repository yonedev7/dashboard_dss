@extends('adminlte::page')

@section('content_header')
<h3><b>Dashboard {{Auth::User()->name}}</b></h3>
@stop

@section('content')
   <div class="p-2" style="background: #f1f1f1">
    @foreach($data_chart as $item)
    <h4 class="text-center"><b>{{$item->title}}</b></h4>
    <hr>
    <div class="mb-2" id="app-chart-{{md5($item->kode)}}">
        <div  v-if="loaded==false" class="text-center">
            <div class="spinner-grow text-primary" role="status">
                <span class="sr-only">Loading...</span>
              </div>
              <div class="spinner-grow text-secondary" role="status">
                <span class="sr-only">Loading...</span>
              </div>
              <div class="spinner-grow text-success" role="status">
                <span class="sr-only">Loading...</span>
              </div>
              <div class="spinner-grow text-danger" role="status">
                <span class="sr-only">Loading...</span>
              </div>
              <div class="spinner-grow text-warning" role="status">
                <span class="sr-only">Loading...</span>
              </div>
              <div class="spinner-grow text-info" role="status">
                <span class="sr-only">Loading...</span>
              </div>
          </div> 
        <div class="card parallax" v-if="loaded" style=" background-image: url('{{url('assets/img/back.png')}}')" v-for="chart,key_chart in chart_list">
            <div class="card-body">
              <charts v-if="loaded" :options="chart.options"></charts>
              
            </div>
           
        </div>
        
    </div>
    @endforeach
</div>
@stop

@section('js')
<script>
     @foreach($data_chart as $item)
        var app_{{md5($item->kode)}}=new Vue({
            el:'#app-chart-{{md5($item->kode)}}',
            data:{
                loaded:false,
                series:[],
                chart_list:[]
            },
            methods:{
                init:function(){
                    req_ajax.post('{{route('api-web.public.analisa.draw',['tahun'=>$Stahun,'kode_analisa'=>$item->kode])}}').then(function(res){
                        if(res){
                            app_{{md5($item->kode)}}.series=res.data.series;
                            app_{{md5($item->kode)}}.chart_list=res.data.chart_list;
                            app_{{md5($item->kode)}}.loaded=true;
                            app_{{md5($item->kode)}}.builder();


                        }
                    });
                },
                builder:function(){
                    for( var chart_key in this.chart_list){ 

                        for(var se in this.chart_list[chart_key].options.series){
                           var id=this.chart_list[chart_key].options.series[se].id;
                            for(so in this.series){
                                if(this.series[so].id==id){
                                    this.chart_list[chart_key].options.series[se].data=this.series[so].data;
                                }
                            }
                        }
                    }
                }
            }
            
        });
        app_{{md5($item->kode)}}.init();

     @endforeach

</script>
@stop