@extends('adminlte::page')

@section('content_header')


@stop

@section('content')
<div id="app-widget">
    <div class="card">
        <div class="card-body">
            <div v-bind:class="'fade '+(step==1?'in show':'')"  v-if="step==1">
                <h3>Pilihan Data</h3>
                <hr>
                <div class="form-group">
                    <form v-on:submit.prevent="load()">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" v-model="q" placeholder="Cari Data" aria_label="Cari Data" aria_describedby="basic_addon2">
                        <div class="input-group-append">
                          <button class="btn btn-outline-secondary" @click="load()" type="button"><i class="fa fa-search"></i></button>
                        </div>
                      </div>
                    </form>
                </div>
              <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th rowspan="2" style="width:100px;">Nama Data</th>
                            <th rowspan="2">Tipe Nilai</th>
                            <th rowspan="2">Satuan</th>
                            <th colspan="4">{{$Stahun-4}}</th>
                            <th colspan="4">{{$Stahun-3}}</th>
                            <th colspan="4">{{$Stahun-2}}</th>
                            <th colspan="4">{{$Stahun-1}}</th>
                            <th colspan="4">{{$Stahun}}</th>
                        </tr>
                        <tr>
                            <th>TW1</th>
                            <th>TW2</th>
                            <th>TW3</th>
                            <th>TW4</th>
                            <th>TW1</th>
                            <th>TW2</th>
                            <th>TW3</th>
                            <th>TW4</th>
                            <th>TW1</th>
                            <th>TW2</th>
                            <th>TW3</th>
                            <th>TW4</th>
                            <th>TW1</th>
                            <th>TW2</th>
                            <th>TW3</th>
                            <th>TW4</th>
                            <th>TW1</th>
                            <th>TW2</th>
                            <th>TW3</th>
                            <th>TW4</th>    
                        </tr>
                    </thead>
                    <tbody>
                            <template v-for="(item,key) in com_data_list_group">
                            <tr v-if="item.render_named_dt">
                                <td  class="bg bg-primary" colspan="23" >@{{item.nama_dataset}}</td>
                            </tr>
                            <tr>
                               
                                <td  style="width:100px;">@{{item.name}}</td>
                                <td>@{{item.tipe_nilai}}</td>
                                <td>@{{item.satua}}</td>
                                <template v-for="th in tahun_list">
                                <template v-for="tw in tw_list">
                                        <td>
                                            <input  v-if="item['tahun_'+th+'_tw_'+tw]" v-bind:checked="check_select(item.id_dataset,item.id_data,th,tw)" @change=checkbox_ch($event,key,th,tw) type="checkbox" v-bind:id="'dt'+key+'tahun_'+th+'_tw_'+tw">
                                            <span v-else>_</span>
                                        </td>
                                    </template>
    
                                </template>
                            </tr>
                            </template>
                    </tbody>
                </table>
              </div>
              <div class="form-group">
                  <button class="btn btn-primary" v-if="show_step_2" @click="step=2"><i class="fa fa-arrow-right"></i></button>
              </div>
            </div>
            <div  v-bind:class="'fade '+(step==2?'in show':'')"  v-if="step==2">
                <h3>Tampilan</h3>
                <hr>
                <div class="form-group">
                    <label for="">Tipe Aggregasi</label>
                    <select name="" id="" class="form-control" v-model="tampilan.aggregasi">
                        <option v-bind:value="item.val" v-for="item,k in tampilan.pilihan_aggregasi">@{{item.text}}</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">FILTER 1 - SCOPE</label>
                    <select name="" id="" class="form-control" v-model="tampilan.filter_scope">
                        <option v-bind:value="item.val" v-for="item,k in tampilan.pilihan_filter_scope">@{{item.text}}</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">FILTER 2 - JENIS PEMDA</label>
                    <select name="" id="" class="form-control" v-model="tampilan.filter_pemda">
                        <option v-bind:value="item.val" v-for="item,k in tampilan.pilihan_filter_pemda">@{{item.text}}</option>
                    </select>
                </div>

                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th style="width:100px;">AKSI</th>
                                <th>NAMA DATASET</th>
                                <th>NAMA DATA</th>
                                <th>SATUAN</th>
                                <th>TIPE NILAI</th>
                                <th>TAHUN</th>
                                <th>TW</th>
                                <th>RENTANG NILAI</th>
                                <th style="min-width:150px;" >COLLECTION LOGIC</th>
                                <th style="min-width:200px;" >LEVEL AGGREGASI</th>
                                <th v-if="tampilan.aggregasi!=0" style="min-width:100px;" >TIPE AGGREGASI</th>
                                <th v-if="tampilan.aggregasi!=0"  style="min-width:300px;">NAMA SETELAH AGGREGASI</th>
                            </tr>
                        </thead>  
                        <tbody>
                            <tr v-for="it,k in selected_data_list">
                                <td scope="col">
                                    <div class="btn-group">
                                    <button class="btn btn-danger btn-sm" @click="hapus_selected_data(k)"><i class="fa fa-trash"></i></button>
                                    <button @click="buat_turunan(k)" v-if="(it.have_child==0||(it.fungsi_rentang_nilai!=null?it.fungsi_rentang_nilai.tipe=='min-max':false)) && it.tipe_schema==1" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="bottom" title="Jabarkan Setiap Pilihan"><i class="fa fa-arrow-down"></i> <span>Cluster</span></button>
                                </div>
                                </td>
                                <td>
                                    @{{it.nama_dataset}}
                                </td>
                                <td>
                                    @{{it.name}}
                                </td>
                                <td>
                                    @{{it.satuan}}
                                </td>
                                <td>
                                    @{{it.tipe_nilai}}
                                </td>
                                <td>                                                
                                    @{{it.tahun}}
                                </td>
                                <td>
                                    @{{it.tw}}
                                </td>
                               
                                <td>
                                    <template v-if="it.fungsi_rentang_nilai && it.tipe_schema==1">
                                        <p class="mb-0"><b>@{{it.fungsi_rentang_nilai.tipe}}</b></p>
                                        <p v-html="(it.fungsi_rentang_nilai.m.map(function(el){return el.val+'=>'+el.tag;}).join(' ,, ')).replace(/,,/g,'<br>')"></p>
                                    </template>
                                    <p v-if="it.fungsi_rentang_nilai==null|| it.tipe_schema==2">-</p>
                                </td>
                                <td>
                                   <template v-if="it.fungsi_rentang_nilai!=null">
                                        <template v-if="it.fungsi_rentang_nilai.tipe=='pilihan'">
                                            <template v-if="it.collection_logic.length>0 ">
                                                <p>@{{it.collection_logic.join(' ')}}</p>
                                            </template>
                                            <p v-if="it.collection_logic.length==0">-</p>
                                        </template>
                                        <template v-if="it.fungsi_rentang_nilai.tipe=='min-max'">
                                            <div class="input-group mb-3" v-if="it.collection_logic.length>0">
                                                <input v-model="it.collection_logic[0]" v-bind:type="it.tipe_nilai=='numeric'?'number':'text'" class="form-control" placeholder="Min" aria-label="Min" aria-describedby="basic-addon2">
                                                <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2">@{{it.collection_logic[1]}}</span>
                                                </div>
                                                <input v-model="it.collection_logic[2]" v-bind:type="it.tipe_nilai=='numeric'?'number':'text'" class="form-control" placeholder="Max" aria-label="Max" aria-describedby="basic-addon2">
                                            </div>
                                            
                                            <p v-if="it.collection_logic.length==0">-</p>
                                        </template>
                                   </template>
                                </td>
                                <td>
                                    <select  name="" class="form-control" id="" v-model="it.tipe_aggregate">
                                        <option v-bind:value="pa.val" v-for="pa,kpa in tampilan.pilihan_aggregasi">@{{pa.text}}</option>
                                    </select>
                                </td>
                                <td >
                                    <select v-if="it.tipe_aggregate!=0" name="" id="" class="form-control" v-model="it.aggregate" >
                                        <option value="0">NONE</option>
                                        <option v-bind:value="ka+1" v-for="(ag,ka) in tampilan.pilihan_aggregasi_data" v-if="ag.tipe_policy.includes(it.tipe_nilai)">@{{ag.text}}</option>

                                    </select>
                                </td>
                                <td v-if="tampilan.aggregasi!=0">
                                    <input type="text" class="form-control" v-model="it.title_aggregate">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="btn-group">
                    <button class="btn btn-info" @click="step=1"><i class="fa fa-arrow-left"></i></button>
                    <button class="btn btn-primary"  v-if="show_step_3" @click="submit_schema()"><i class="fa fa-arrow-right"></i></button>
                </div>
            </div>
        </div>
    </div>
    <form  id="form-schema" v-if="show_step_3"  action="{{route('dash.si.analisa.check_schema',['tahun'=>$Stahun])}}" method="post">
        @csrf
        <input type="hidden" name="filter_1" v-bind:value="tampilan.filter_scope">
        <input type="hidden" name="filter_2" v-bind:value="tampilan.filter_pemda">
        <template v-for="item,key in selected_data_list">
            <input type="hidden" v-bind:name="'schema['+key+'][key]'" v-bind:value="key">
            <input type="hidden" v-bind:name="'schema['+key+'][id_dataset]'" v-bind:value="item.id_dataset">
            <input type="hidden" v-bind:name="'schema['+key+'][id_data]'" v-bind:value="item.id_data">
            <input type="hidden" v-bind:name="'schema['+key+'][tahun]'" v-bind:value="item.tahun">
            <input type="hidden" v-bind:name="'schema['+key+'][tahun_dinamic]'" v-bind:value="item.tahun_dinamic">
            <input type="hidden" v-bind:name="'schema['+key+'][tw]'" v-bind:value="item.tw">
            <input type="hidden" v-bind:name="'schema['+key+'][aggregate]'" v-bind:value="item.aggregate">
            <input type="hidden" v-bind:name="'schema['+key+'][tipe_aggregate]'" v-bind:value="item.tipe_aggregate">
            <input type="hidden" v-bind:name="'schema['+key+'][have_child]'" v-bind:value="item.have_child">
            <input type="hidden" v-bind:name="'schema['+key+'][tipe_schema]'" v-bind:value="item.tipe_schema">
            <input type="hidden" v-bind:name="'schema['+key+'][fungsi_rentang_nilai]'" v-bind:value="JSON.stringify(item.fungsi_rentang_nilai||{})">
            <input type="hidden" v-for="l,kl in item.collection_logic" v-bind:name="'schema['+key+'][collection_logic]['+kl+']'" v-bind:value="item.collection_logic[kl]">
            <input type="hidden" v-bind:name="'schema['+key+'][title_aggregate]'" v-bind:value="item.title_aggregate">
        </template>  
    </form>
</div>
@stop


@section('js')
    <script>
        var app=new Vue({
            el:'#app-widget',
            data:{
                q:'',
                data_list:[{
                    name:'',
                    nama_dataset:'xx'
                }],
                tahun_list:[{{$Stahun-4}},{{$Stahun-3}},{{$Stahun-2}},{{$Stahun-1}},{{$Stahun}}],
                tw_list:[1,2,3,4],
                index_selected_list:1,
                selected_data_list:<?=json_encode($saver['selected_data_list'])?>,
                step:1,
                show_step_3:false, 
                show_step_2:false,         
                tampilan:{
                    filter_scope:<?=$saver['filter_1']?>,
                    pilihan_filter_scope:[
                        {
                            text:'NASIONAL',
                            val:1
                        },
                        {
                            text:'LONG LIST NUWSP',
                            val:2
                        },
                        {
                            text:'SORT LIST NUWSP {{$Stahun}}',
                            val:3
                        }
                    ],
                    filter_pemda:<?=$saver['filter_1']?>,
                    pilihan_filter_pemda:[
                        {
                            text:'KABKOTA',
                            val:1
                        },
                        {
                            text:'KABKOTA DAN PROVINSI',
                            val:2
                        },
                        {
                            text:'PROVINSI',
                            val:3
                        }
                    ],
                    aggregasi:1,
                    pilihan_aggregasi_data:[
                        {
                            text:'SUM',
                            tipe_policy:['numeric']
                        },
                        {
                            text:'MAX',
                            tipe_policy:['numeric','string']
                        },
                        {
                            text:'MIN',
                            tipe_policy:['numeric','string']
                        },
                        {
                            text:'AVERAGE',
                            tipe_policy:['numeric']
                        },
                        {
                            text:'COUNT',
                            tipe_policy:['numeric','string']
                        }
                    ],
                    pilihan_aggregasi:[
                        {
                            'text':'Tidak Beragregasi',
                            val:0,
                            'groupBy':['dtas.kodepemda'],
                            'joinPemda':['mp.kodepemda','=','dtas.kodepemda'],
                            'orderBy':[['mp.kodepemda','asc'],['dtas.tw','asc']]

                        },
                        {
                            'text':'Tingkat Provinsi',
                            'groupBy':['mp.kodepemda'],
                            val:2,

                            'joinPemda':['mp.kodepemda','=','left(dtas.kodepemda,3)'],
                            'orderBy':[['mp.kodepemda','asc'],['dtas.tw','asc']]
                        },
                        {
                            'text':'Tingkat Nasional',
                            'groupBy':['mp.kodepemda'],
                            val:1,
                            'joinPemda':['mp.kodepemda','=','left(dtas.kodepemda,1)'],
                            'orderBy':[['mp.kodepemda','asc'],['dtas.tw','asc']]
                        }

                    ]
                }
            },
            watch:{
                step:function(val){
                    if(val==2){
                        this.data_list_sort();
                        console.log('sort ok');
                        this.check_req_aggre();
                        console.log('aggre ok');

                    }
                },
                "selected_data_list":{
                    deep:true,
                    handler:function(v){
                        
                        this.check_req_aggre();
                        
                    }
                }
                
            },
            computed:{
                com_data_list_group:function(){
                    var data_new_list=[];
                    var old_group=null;
                    this.data_list.forEach(el => {

                        if(old_group!=el.id_dataset){
                            el['render_named_dt']=true;
                            old_group=el.id_dataset;
                        }else{
                            el['render_named_dt']=false;
                        }

                         data_new_list.push(el);


                    });
                    return data_new_list;

                }
            },
            mounted:function(){
               this.data_list=[];
            },
            methods:{
                data_list_sort:function(){
                    this.selected_data_list = Object.keys(this.selected_data_list).sort().reduce(
                                (obj, key) => { 
                                    obj[key] = app.selected_data_list[key]; 
                                    return obj;
                                }, 
                            {}
                    );
                    if(Object.keys(this.selected_data_list).length==0){
                        this.show_step_3=false;
                        this.show_step_2=false;
                    }else{
                        this.show_step_2=true;
                    }

                    this.index_selected_list=1;
                },
                hapus_selected_data:function(key){
                    if(this.selected_data_list[key]!=undefined){
                        delete (this.selected_data_list[key]);
                        this.data_list_sort();
                    }
                },
                submit_schema:function(){
                    $('#form-schema').submit();
                },
                check_req_aggre:function(){
                    var a=true;
                    for(var k in this.selected_data_list){
                        if(this.selected_data_list[k]!=0){
                            if(((this.selected_data_list[k].title_aggregate!=null)&&(this.selected_data_list[k].aggregate>0))!=true){
                                a=false;
                            }
                        }
                    }
                    this.show_step_3=a;
                },
              
                check_select:function(id_dataset,id_data,tahun,tw){
                    var key='dataset_'+id_dataset+'_s_data_'+id_data+'_s_tahun_'+tahun+'_s_tw_'+tw;
                    if(this.selected_data_list[key]!=undefined){
                        return true;
                    }else{
                        return false;
                    }

                },
                load:function(){
                    req_ajax.post('{{route('api-web.public.analisa.listing_schema',['tahun'=>$Stahun])}}',{q:this.q}).then(function(res){
                        app.data_list=res.data.data;
                    });
                    this.check_req_aggre();
                },
                buat_turunan:function(key){
                    var source=this.selected_data_list[key];
                    if(source!=undefined){
                        source=JSON.parse(JSON.stringify(source));
                        if(source.fungsi_rentang_nilai==null){

                        }else if(source.fungsi_rentang_nilai.tipe=='pilihan'){
                            for(var i in source.fungsi_rentang_nilai.m){
                                if(source.tipe_nilai=='numeric'){
                                    var logic=["valR",'=',source.fungsi_rentang_nilai.m[i].val];
                                }else{
                                    var logic=["valR",'=',"'"+source.fungsi_rentang_nilai.m[i].val+"'"];
                                }
                                if(source.title_aggregate){
                                    t_a=source.title_aggregate+' (Pilihan '+source.fungsi_rentang_nilai.m[i].tag+')';
                                }else{
                                    t_a=source.title_aggregate;
                                }
                                if(source.name){
                                    t_n=source.name+' (Pilihan '+source.fungsi_rentang_nilai.m[i].tag+')';
                                }else{
                                    t_n=source.name;
                                }

                                var data={
                                    id_data:source.id_data,
                                    id_dataset:source.id_dataset,
                                    name:t_n,
                                    nama_dataset:source.nama_dataset,
                                    satuan:source.satuan,
                                    tipe_nilai:source.tipe_nilai,
                                    definisi_konsep:source.definisi_konsep,
                                    fungsi_rentang_nilai:source.fungsi_rentang_nilai,
                                    tipe_schema:2,
                                    collection_logic:logic,
                                    have_child:1,
                                    tipe_aggregate:source.tipe_aggregate,
                                    tahun:source.tahun,
                                    tahun_dinamic:{{$Stahun}}-source.tahun,
                                    tw:source.tw,
                                    display:true,
                                    aggregate:null,
                                    title_aggregate:t_a
                                }
                                this.selected_data_list[key+'_'+i]=data;
                            }

                            this.selected_data_list[key].have_child=1;

                           


                        }else if(source.fungsi_rentang_nilai.tipe=='min-max'){
                                if(source.tipe_nilai=='numeric'){
                                    var logic=[0,'< valR_ <',0];
                                }
                                t_a=source.title_aggregate;
                                t_n=source.name;
                                var data={
                                    id_data:source.id_data,
                                    id_dataset:source.id_dataset,
                                    name:t_n,
                                    nama_dataset:source.nama_dataset,
                                    satuan:source.satuan,
                                    tipe_nilai:source.tipe_nilai,
                                    definisi_konsep:source.definisi_konsep,
                                    fungsi_rentang_nilai:source.fungsi_rentang_nilai,
                                    tipe_schema:2,
                                    collection_logic:logic,
                                    have_child:source.have_child+1,
                                    tahun:source.tahun,
                                    tipe_aggregate:source.tipe_aggregate,

                                    tahun_dinamic:{{$Stahun}}-source.tahun,
                                    tw:source.tw,
                                    display:true,
                                    aggregate:null,
                                    title_aggregate:t_a
                                }
                                this.selected_data_list[key+'_'+data.have_child]=data;

                                this.selected_data_list[key].have_child=source.have_child+1;

                           


                        }

                        this.data_list_sort();
                        

                    }
                },
                checkbox_ch:function(ev,index,tahun,tw){
                    var source=this.data_list[index];

                    if(source!=undefined){
                        var fn=null;
                        if(source.fungsi_rentang_nilai!=null){
                            fn=JSON.parse(source.fungsi_rentang_nilai);
                        }
                        if(fn['tipe']!=undefined){
                            fn={
                                tipe:fn.tipe,
                                m:fn.m
                            }
                        }else{
                            fn=null;
                        }

                        var data={
                            id_data:source.id_data,
                            id_dataset:source.id_dataset,
                            name:source.name,
                            nama_dataset:source.nama_dataset,
                            satuan:source.satuan,
                            tipe_nilai:source.tipe_nilai,
                            definisi_konsep:source.definisi_konsep,
                            fungsi_rentang_nilai:fn,
                            tipe_schema:source.tipe_schema,
                            collection_logic:[],
                            have_child:source.have_child,
                            tipe_aggregate:2,
                            tahun:tahun,
                            tw:tw,
                            display:true,
                            aggregate:null,
                            title_aggregate:null
                        }

                        var key='dataset_'+data.id_dataset+'_s_data_'+data.id_data+'_s_tahun_'+data.tahun+'_s_tw_'+data.tw;

                        if($(ev.target).prop('checked')){

                            if(this.selected_data_list[key]==undefined){
                                this.selected_data_list[key]=data;

                            }
                        }else{
                            console.log('delete',key);
                            if(this.selected_data_list[key]!=undefined){
                                delete this.selected_data_list[key];
                            }

                        }
                    }
                    this.data_list_sort();                
                }

            }
        });
        app.load();
    </script>

@stop