@extends('adminlte::page')

@section('content_header')

@stop

@section('content')
<div class=""></div>

<div id="app-widget">
    <div class="col-md-12">
        <p>Tahun Analisa @{{meta.tahun_analisa}} <span><small>@{{meta.dinamic?'Tahun Dinamic':'Tahun Static'}}</small></span></p>
        <hr>
        <h4 v-if="edit_title==false"><b>@{{meta.judul?meta.judul:'No Title'}}</b> <small><span class="badge badge-success" @click="edit_title=true"><i class="fa fa-pen"></i></span></small></h4>
        <form v-on:submit.prevent="edit_title=false">
        <input type="text" v-model="meta.judul" class="form-control" v-if="edit_title" placeholder="Judul Component">
        </form>

    </div>
    <div class="card">
        <div class="card-body" style="height:calc(100vh - 70px);">
            <p v-if="saving_ready">....loading to saving...</p>

            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                  <div class="navbar-nav">
                    <a class="nav-item nav-link active" href="javascript:void(0)" onclick="$('#modal_save').modal()"> Simpan Pada Dashboard</a>
                    <a class="nav-item nav-link active" href="javascript:void(0)" @click="show_modal_add()"> <i class="fa fa-plus"></i> Chart</a>
                    <a class="nav-item nav-link active" href="javascript:void(0)"> <i class="fa fa-eye"></i> Seris Data</a>
                  </div>
                </div>
            </nav>
            <div class="p-3" style="background: #f1f1f2; height:calc(100vh - 200px); overflow-y:scroll">
                <div class="card parallax" style=" background-image: url('{{url('assets/img/back.png')}}')" v-for="chart,key_chart in chart_list">
                    <div class="card-body">
                    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                          </button>
                          <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                            <div class="navbar-nav">
                              <a class="nav-item nav-link active" href="javascript:void(0)" @click="show_modal_yAxis(key_chart)"> Y-Axis</a>          
                                <a class="nav-item nav-link active" href="javascript:void(0)" @click="show_modal_chart_series(key_chart)"> Series</a>          
                            </div>
                          </div>
                            
                    </nav>
                      <charts :options="chart.options"></charts>
                    </div>
                </div>
                <img src="{{url('assets/img/DSSChartCanva.gif')}}" v-if="chart_list.length==0" style="width:100%" alt="">
            </div>
           
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="modal_add_chart">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Tambah Chart</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="">Judul</label>
                    <input type="text" class="form-control" v-model="modal.add_chart.title">
                </div>
                <div class="form-group">
                    <label for="">Level Aggregate</label>
                    <select name="" class="form-control" id="" v-model="modal.add_chart.level">
                        <option v-bind:value="l.val" v-for="l in modal.add_chart.option_level">@{{l.text}}</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Tipe Chart Default</label>
                    <select name="" class="form-control"  v-model="modal.add_chart.type" >
                        <option v-bind:value="type" v-for="type in list_type_chart">@{{type}}</option>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" v-if="modal.add_chart.title" @click="add_chart();"><i class="fa fa-plus"></i></button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade" tabindex="-1" role="dialog" id="modal_y_axis">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Y - Axis @{{meta.title}} @{{modal.yAxis.index_chart}}</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Sudut</th>
                            <th>Aksi</th>

                        </tr>
                    </thead>
                    <tbody>
                            <tr v-for="y,k in modal.yAxis.axis">
                                <td>
                                    <input type="text" class="form-control" v-model="y.title.text">
                                </td>
                                <td>
                                    <button type="button" v-bind:class="'btn-sm btn btn-'+(y.opposite?'primary':'danger')" @click="y.opposite?y.opposite=false:y.opposite=true;">@{{y.opposite?'Ya':'Tidak'}}</button>
                                </td>
                                <td>

                                </td>
                            </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" @click="add_yAxis(modal.yAxis.index_chart)" ><i class="fa fa-plus"></i></button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" tabindex="-1" role="dialog" id="modal_save">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Simpan Pada Dashboard</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <p><b>Simpan "@{{meta.title}}" Pada Dashboard Sistem Informasi</b></p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" @click="save_dashbord()" >Simpan</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade" tabindex="-1" role="dialog" id="modal_chart_series">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Series @{{meta.title}}</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Tipe Chart</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <template v-if="chart_list[(modal.chart_series.index_chart)]!=undefined">
                            <tr  v-for="item,key in chart_list[modal.chart_series.index_chart].option_series">
                                <td>
                                    <input type="text" class="form-control" v-model="item.name">
                                </td>
                                <td>
                                    <select name="" class="form-control" v-model="item.type" >
                                        <option v-bind:value="ty" v-for="ty,k in list_type_chart">@{{ty}}</option>
                                    </select>
                                </td>
                                <td>
                                    <button class="btn btn-primary btn-sm" @click="add_chart_series(modal.chart_series.index_chart,key)"><i class="fa fa-plus"></i></button>
                                </td>
                            </tr>
                        </tbody>
                        </template>
                </table>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

      
</div>
@stop

@section('js')
<script>
var app=new Vue({
    el:'#app-widget',
    data:{
        saving_ready:false,
        meta:<?=json_encode($saver)?>,
        edit_title:false,
        series:<?=json_encode($series)?>,
        chart_list:[],
        list_type_chart:['column','pie','line','bar'],
        modal:{
            add_chart:{
                title:'',
                type:'column',
                level:2,
                option_level:[],
            },
            yAxis:{
                index_chart:-1, 
                axis:[]
            },
            chart_series:{
                index_chart:-1,
                list:[]
            },
            edit_chart:{

            }
        },
        def_options:{
            title:{
                 text:'',
            },  
            subtitle:{
                enabled:false,
                text:''
            },
            chart:{
                backgroundColor:'#ffffffb8',
                type:'column',
            
            },
            yAxis:[],
            xAxis:{
                type:'category',
                title:{
                    text:'Nama Pemda'
                },
                min: 1,
                max: 4,
                crosshair: true,
                scrollbar: {
                    enabled:true
                }
                
            },
            series:[]
        }

    },
    methods:{
        data_saver:function(){
            var j=JSON.parse(JSON.stringify(this.chart_list));
            for(chart in j){
                for(var i in j[chart].options.series){
                    j[chart].options.series[i].data=[];
                    j[chart].option_series=[];

                }

            };

            return JSON.stringify(j);

       },
        add_chart:function(){
            var op=JSON.parse(JSON.stringify(this.def_options));
            op.title.text=this.modal.add_chart.title;
            op.chart.type=this.modal.add_chart.type;
            op.subtitle.text='DSS ('+(this.meta.tahun_analisa)+')';
            op.subtitle.enabled=true;
            var level=this.modal.add_chart.level;
            var chart={
                level:this.modal.add_chart.level,
                option_series:this.series.filter(function(s){ return s.level==level;}),
                options:op
            };
            this.chart_list.push(chart);
            this.modal.add_chart.title='';

            $('.modal').modal('hide');
        },
        show_modal_add:function(){
            var object1={};
            this.series.map(function(el){
                switch(parseInt(el.level)){
                    case 1:
                    l= {
                        text:'NASIONAL',
                        val:1
                    };
                    break;
                    case 2:
                    l= {
                        text:'PROVINSI',
                        val:2
                    };
                    break;
                    case 3:
                    l= {
                        text:'KAB/KOT',
                        val:3
                    };
                    break;
                }
                object1[l.val]=l;
                return l;
            });
            this.modal.add_chart.option_level=Object.values(object1);
            this.modal.add_chart.level= this.modal.add_chart.option_level[0].val;
            
            
            $('#modal_add_chart').modal();
        },
        save_dashbord:function(){
            this.saving_ready=true;
            this.edit_title=false;
            $('.modal').modal('hide');

            req_ajax.post('{{route('api-web.public.analisa.save_dashbord',['tahun'=>$Stahun,'kode_analisa'=>$kode_analisa])}}',{'judul':this.meta.judul,chart:this.data_saver()}).then(function(res){
                if(res.data){
                    app.saving_ready=false;  
                      
                }
            });
        },
        show_modal_yAxis:function(index_chart){
            this.modal.yAxis.index_chart=index_chart;
            this.modal.yAxis.axis=this.chart_list[index_chart].options.yAxis;

            $('#modal_y_axis').modal();
        },
        add_yAxis:function(index_chart){
            this.chart_list[index_chart].options.yAxis.push({
                title:{
                    text:''
                },
                opposite:false
            });

            this.modal.yAxis.axis=this.chart_list[index_chart].options.yAxis;

        },
        con_yAxis:function(index){
            return chart_list[index].options.yAxis;
        },
        draw_yAxis:function(index_chart){
          this.chart_list[index_chart].yAxis=JSON.parse(JSON.stringify( this.modal.yAxis.axis));
            $('.modal').modal('hide');
        },
        show_modal_chart_series:function(index_chart){
            this.modal.chart_series.index_chart=index_chart;
            this.modal.chart_series.list=this.chart_list[index_chart].option_series;
            $('#modal_chart_series').modal();
        },
        add_chart_series:function(index_chart,index_series){
            this.chart_list[index_chart].option_series[index_series].yAxis=index_chart;
            var ser=JSON.parse(JSON.stringify(this.chart_list[index_chart].option_series[index_series]));
            this.chart_list[index_chart].options.series.push(ser);
            $('.modal').modal('hide');
        },
    },
    computed:{
      
    }
    

});
</script>

@stop