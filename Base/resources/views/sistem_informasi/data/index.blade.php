@extends('adminlte::page')

@section('content_header')
{{ Breadcrumbs::render('dash.validator') }}
    <a href="{{route('dash.si.schema.data.tambah_validator',['tahun'=>$Stahun])}}" class="btn btn-success">Tambah Validator Data</a>
@stop
@section('content')
  <div class="card" id="app_{{$SpageId}}">
    <div class="card-body">
      <table id="table-validator-{{$SpageId}}" class="table table-bordered">
        <thead class="bg-primary">
          <tr>
            <th>Nama Validator</th>
            <th>Jumlah Datum</th>
            <th>Aksi</th>
            

          </tr>
        </thead>
      </table>
    </div>
  </div>
@stop

@section('js')
<script>
  var app_table;
   var A{{$SpageId}}=new Vue({
     el:'#app_{{$SpageId}}',
     
     data:{
       table_validator:null,
       dataset:<?=json_encode($validators)?>,
     },
   
     watch:{
      dataset:function(){
        this.redraw_table();
      }
     },
     methods:{
       redraw_table:function(){
        
        this.table_validator.clear().draw(); 
        this.table_validator.rows.add(this.dataset).draw();
       },
       init:function(){

          this.table_validator=$('#table-validator-{{$SpageId}}').DataTable( {
            paging: false,
            autoWidth: false,
            process:1,
            data: window.A{{$SpageId}}.dataset,
            columns: [
                { 
                    data: 'name',
                    name: 'Nama Validator',
                },
                { 
                    data: 'jumlah_data',
                    name: 'Jumlah Data',
                    type:'html',
                    render:(display,a,item)=>{
                      return item.jumlah_data+' Data';
                    }
                },
                {
                  data:'',
                  name:'Aksi', 
                  sortable:false,
                  render:(display,a,item)=>{
                    return '<div class="btn-group"><a href="'+item.link_detail+'" class="btn btn-circle btn-primary"><i class="fa fa-arrow-right"></i></a></div>';
                  }
                  
                }
                
              
            ],
          
        });
       }
     }
   });
   A{{$SpageId}}.init();

</script>

@stop