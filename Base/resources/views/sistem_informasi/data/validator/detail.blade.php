@extends('adminlte::page')

@section('content_header')
<h3><b>Validator Data : {{$validator_data->name}}</b></h3>
<a href="{{route('dash.si.schema.data.tambah_data',['tahun'=>$Stahun,'id_validator'=>$validator_data->id])}}" class="btn btn-success ">Tambah Data</a>
@stop
@section('content')
  <div class="card" id="app_{{$SpageId}}">
    <div class="card-body">
      <table id="table-validator-{{$SpageId}}" class="table table-bordered">
        <thead class="bg-primary">
          <tr>
            <th>Kode</th>
            <th>Produsen Data</th>
            <th>Nama Data</th>
            <th>Satuan</th>
            <th>Definisi / Konsep</th>
            <th>Tipe Nilai</th>
            <th>Rentang Nilai</th>
            <th>Aksi</th>
          </tr>
        </thead>
      </table>
    </div>
    <div class="modal" tabindex="-1" role="dialog" id="modal-delete">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Hapus Schema Data</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <label for="">Nama</label>
              <p>@{{editedItem.name}}</p>
            </div>
            <div class="modal-footer">
                <form v-bind:action="editedItem.link_delete" method="post">
                    @method('delete')
                    @csrf

              <button type="submit" class="btn btn-primary">Hapus</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </form>
            </div>
          </div>
        </div>
      </div>

  </div>
@stop

@section('js')
<script>
  var app_table;
   var A{{$SpageId}}=new Vue({
     el:'#app_{{$SpageId}}',
     
     data:{
       table_validator:null,
       dataset:<?=json_encode($data)?>,
       editedItem:{

       }
     },
   
     watch:{
      dataset:function(){
        this.redraw_table();
      }
     },
     methods:{
       redraw_table:function(){
        
        this.table_validator.clear().draw(); 
        this.table_validator.rows.add(this.dataset).draw();
       },
       deletedItem:function(id){
            var index=0;
            while(this.dataset[index].id!=id){    
                index++;
            }


            if(this.dataset[index]!=undefined){
                console.log(this.dataset[index]);
                this.editedItem=this.dataset[index];
                $('#modal-delete').modal();
            }

            
       },
       init:()=>{
           
          this.table_validator=$('#table-validator-{{$SpageId}}').DataTable( {
            paging: false,
            autoWidth: false,
            process:1,
            data: window.A{{$SpageId}}.dataset,
            columns: [
                { 
                    data: 'kode',
                    name: 'kode',
                },
                { 
                    data: 'produsent_data',
                    name: 'Produsent Data',
                },
                { 
                    data: 'name',
                    name: 'Nama',
                },
                { 
                    data: 'satuan',
                    name: 'Satuan',
                },
                { 
                    sortable:false,
                    type:'html',
                    render:function(a,s,item){
                       return clean_text(item.definisi_konsep,50)!=''?clean_text(item.definisi_konsep,50):'-';
                    }
                },
                { 
                    data: 'tipe_nilai',
                    name: 'Tipe Nilai',
                },
                { 
                    name: 'rentang_nilaif',
                    sortable:false,
                    type:'html',
                    render:(a,b,item)=>{
                      var r=JSON.parse(item.fungsi_rentang_nilai);

                      if(r['tipe']!=undefined){
                        switch(r['tipe']){
                          case 'min-max':
                            return r['sort'][0]+' - '+r['sort'][1]
                            break;
                          case 'pilihan':
                          return r['sort'].replace(/,/g,' , ');
                          default:
                          var c=[];
                           var b=r['sort'].replace(/\s+/,'').split(',');
                           b=b.filter(onlyUnique)
                           for(d of b){
                             switch(d){
                              case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                                c.push('Ms World');
                                break;
                                case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                                c.push('Ms Excel');
                                break;

                                case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
                                c.push('Ms Power Point')
                                break;
                                case 'application/pdf':
                                c.push('PDF')
                                break;
                                case 'image/*':
                                c.push('image')
                                break;

                             }
                           }
                           return c.join(', ');

                          break;
                        }
                      }
                        return '-';
                    }

                },
                
                {
                  data:'',
                  name:'Aksi', 
                  sortable:false,
                  render:(display,a,item)=>{
                    return '<div class="btn-group"><a href="'+item.link_edit+'" class="btn btn-circle btn-primary"><i class="fa fa-pen"></i></a><button onclick="A{{$SpageId}}.deletedItem('+item.id+')"  class="btn btn-circle btn-danger"><i class="fa fa-trash"></i></button></div>';
                  }
                  
                }
                
              
            ],
          
        });
       }
     }
   });
   A{{$SpageId}}.init();

</script>

@stop