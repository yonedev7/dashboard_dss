@extends('adminlte::page',['breadcrums_page'=>'dash.validator.tambah'])

@section('content_header')

<h1>Tambah Validator</h1>
@stop
@section('content')
   <div id="app">
        <form action="{{route('dash.si.schema.data.store_validator',['tahun'=>$Stahun])}}" method="post">
            @csrf
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        <label for="">Nama Validator</label> 
                        <input type="text" name="nama" class="form-control" required value="{{old('nama')}}">   
                    </div>
                    <div class="form-group">
                        <label for="">Keterangan</label> 
                        <textarea name="keterangan" class="form-control" id="" cols="30" rows="10">{!!old('keterangan')!!}</textarea>  
                    </div>
        
                </div> 
                <div class="card-footer">
                    <button class="btn btn-success" type="submit">Tambah</button>
                </div> 
            </div>
        </form>
   </div>
@stop

@section('js')
<script>
    var v_app=new Vue({
        el:'#app',
        vuetify:new Vuetify(),
        data:{
            headers:[
                {
                    text:'Sumber Datum',
                },
                { text: 'Jumlah Kategori', value: 'kategori_count' },
            ],
            dataset:[

            ]
        }
    });
</script>

@stop