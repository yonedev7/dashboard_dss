@extends('adminlte::page')

@section('content_header')

<h1>Tambah Data - Validator {{$validator_data->name}}</h1>
@stop

@section('content')
   <div id="app_{{$SpageId}}">
    <form action="{{route('dash.si.schema.data.store_data',['tahun'=>$Stahun,'id_validator'=>$validator_data->id])}}" method="post">
        @csrf
        <div class="card card-primary">
            <div class="card-body">
               <div class="row">
                   <div class="col-md-4">
                   
                        <div class="form-group">
                            <label for="">Kode Khusus</label> 
                            <input type="text" name="kode_khusus" v-model="data.kode_khusus"  class="form-control"  value="{{old('kode_khuus')}}">   
                        </div>
                        <div class="form-group">
                            <label for="">Nama Data*</label> 
                            <input type="text" name="nama"  v-model="data.name" class="form-control" required value="{{old('name')}}">   
                        </div>
                        <div class="form-group">
                            <label for="">Produsent Data*</label> 
                            <input type="text" name="produsent_data" v-model="data.produsent_data"  class="form-control"  value="{{old('produsent_data')}}">   
                        </div>
                        <div class="form-group">
                            <label for="">Definisi / Konsep*</label> 
                            <textarea name="definisi_konsep" class="form-control" id="" cols="30" v-model="data.definisi_konsep" rows="10"></textarea>  
                        </div>
                   </div>
                   <div class="col-md-4">
                    
                    <div class="form-group">
                        <label for="">Jenis Data*</label> 
                        <select name="jenis_data" class="form-control" required id="" v-model="data.jenis_data">
                            <option value="0">Qualitatif</option>
                            <option value="1">Quantitatif</option>

                           
                        </select>   
                    </div>
                    <div class="form-group">
                        <label for="">Tipe Nilai*</label> 
                        <select name="tipe_data" class="form-control" id="" required  v-model="data.tipe_data">
                            <option value="numeric">Numeric</option>
                            <option value="string">String (196 Char)</option>
                            <option value="text">Text (500 Char)</option>
                            <option value="single-file">Single File</option>
                            <option value="multy-file">Multy File</option>
                        </select>   
                    </div>
                    <div class="form-group">
                        <label for="">Arah Nilai*</label> 
                        <select name="arah_nilai" class="form-control" id=""  required v-model="data.arah_nilai">
                            <option value="1">Positif</option>
                            <option value="0">Netral</option>
                            <option value="-1">Negatif</option>
                        </select>   
                    </div>

                    <div class="form-group">
                        <label for="">Satuan*</label> 
                        <input type="text" required class="form-control" v-model="data.satuan" name="satuan">
                    
                    </div>
                    <div class="form-group">
                        <label for="">Tipe Aggregasi</label> 
                        <select name="tipe_aggregasi" class="form-control" id="" required  v-model="data.tipe_aggregasi">
                            <option value="0">NONE</option>
                            <option value="SUM">SUM</option>
                            <option value="COUNT">COUNT</option>
                            <option value="AVARAGE">AVERAGE</option>
                            <option value="MIN">MIN</option>
                            <option value="MAX">MAX</option>
                            <option value="MEAN">MEAN - Dengan Average Sementara</option>
                        </select>   
                    </div>
                   </div>
                   <div class="col-md-4">
                    <div class="form-group" >
                        <div v-if="!['single-file','multy-file'].includes(data.tipe_data)">
                        <label for="">Rentang Nilai</label> 
                        <select name="rentang_nilai_fungsi" class="form-control" id="" v-model="data.rentang_nilai_fungsi">
                            <option value="0">None</option>
                            <option v-if="data.tipe_data=='numeric'" value="min-max">Min-Max</option>
                            <option value="pilihan">Pilihan</option>
                        </select>
                    </div>

                        <div class="table-responsive">
                            <div v-if="['single-file','multy-file'].includes(data.tipe_data)">
                                <template  data-app >  
                                 
     
                                 <table  class="table table-bordered">
                                     <thead>
                                         <tr>
                                             <th>Check</th>
                                             <th>Tag</th>
                                             <th>Nilai</th>
                                     </thead>
                                     <tbody>
                                         <tr v-for="(item,index) in table_file.dataset">
                                             <td>
                                                 <input v-bind:checked="item.selected?true:false" v-if="data.tipe_data=='single-file'" name="s_f"  @click="file_selected($event,idx)" type="radio">
                                                 <input v-bind:checked="item.selected?true:false" v-if="data.tipe_data!='single-file'" name="s_f[]"   @click="file_selected($event,idx)" type="checkbox">
                                             </td>
                                             <td>@{{item.tag}}
                                                <input   type="hidden"  v-bind:name="'rentang_nilai_file['+index+'][tag]'" v-model="i.tag" required>
                                                <input   type="hidden"  v-bind:name="'rentang_nilai_file['+index+'][val]'"   v-model="i.val" required>
                                                <input   type="hidden"  v-bind:name="'rentang_nilai_file['+index+'][tipe]'"   v-model="data.tipe_data" required>
                                                <input  type="hidden"  v-bind:name="'rentang_nilai['+index+'][selected]'"   v-model="i.selected" required>
                                            </td>
                                             <td>@{{item.val}}</td>
                                            
     
                                         </tr>
                                     </tbody>
                                 </table>
                               </template>
                               
                                   
                              
                             </div>
                             <div  data-app  v-if="['pilihan','min-max'].includes(data.rentang_nilai_fungsi) && ['numeric','string','text'].includes(data.tipe_data)" class="mt-3">
                                <div v-if="data.rentang_nilai_fungsi=='pilihan'">
                                    <button @click="show_dialog_datum_numeric_add()" type="button" class="btn btn-primary btn-sm mb-1"><i class="fa fa-plus"></i> Tambah Pilihan</button>
                                </div>     
                                
                                <table  class="table table-bordered">
                                         <thead>
                                             <tr>
                                                 <th>Tag</th>
                                                 <th>Nilai</th>
                                                 <th>Aksi</th>
                                             </tr>
                                         </thead>
                                         <tbody>
                                             <tr v-for="item in table_rentang.dataset">
                                                 <td>@{{item.tag}}</td>
                                                 <td>@{{item.val}}
                                                    <input  v-if="item.selected"  type="hidden"  v-bind:name="'rentang_nilai['+index+'][tag]'" v-model="item.tag" required>
                                                    <input  v-if="item.selected"  type="hidden"  v-bind:name="'rentang_nilai['+index+'][val]'"   v-model="item.val" required>
                                                    <input  v-if="item.selected"  type="hidden"  v-bind:name="'rentang_nilai['+index+'][val]'"   v-model="item.val" required>
                                                    <input  v-if="item.selected"  type="hidden"  v-bind:name="'rentang_nilai['+index+'][tipe]'"   v-model="data.rentang_nilai_fungsi" required>
                                                    <input  v-if="item.selected"   type="hidden"  v-bind:name="'rentang_nilai['+index+'][selected]'"   value="1" required>
                                                </td>
                                                 <td>
                                                     <div class="btn-group">
                                                         <button type="button" @click="editItem(item)" class="btn btn-sm btn-primary"><i class="fa fa-pen"></i></button>
                                                         <button v-if="data.rentang_nilai_fungsi=='pilihan'"  type="button" @click="deleteItem(item)" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                                        
                                                        </div>
                                                 </td>
     
                                             </tr>
                                         </tbody>
                                     </table>
                                 
     
                               
                             </div> 
                            </div>  
                    </div>

                   </div>
               </div>
               <div class="row">
                   <div class="col-12">
                       <div class="form-group">
                           <label for="">Cara Hitung</label>
                           <textarea name="cara_hitung" v-model="data.cara_hitung" class="form-control" id="" cols="30" rows="10"></textarea>  

                       </div>
                   </div>
               </div>

               
    
            </div> 
            <div class="card-footer">
                <button class="btn btn-success" type="submit">Tambah</button>
            </div> 
        </div>
    </form>

    <div class="modal" tabindex="-1" role="dialog" id="modal-datum-numeric-edit">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Ubah Nilai</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                  <label for="Tag">Tag</label>
                  <p v-if="data.rentang_nilai_fungsi!='pilihan'">@{{editedItem.tag}}</p>
                  <input v-if="data.rentang_nilai_fungsi=='pilihan'" type="text" class="form-control" v-model="editedItem.tag">
              </div>

              <div class="form-group">
                <label for="Tag">Nilai</label>
                <input v-if="data.tipe_data=='numeric'" type="number" class="form-control" step="0.00001" v-model="editedItem.val">
                <textarea v-if="data.tipe_data=='text'" name="" id=""  class="form-control" v-model="editedItem.val" cols="30" rows="5"></textarea>
                <input v-if="data.tipe_data=='string'" type="text" class="form-control" v-model="editedItem.val">


            </div>
            </div>
            <div class="modal-footer">
              <button type="button" v-if="editedItem.tag && editedItem.val" class="btn btn-primary" class="btn btn-primary" @click="save()">Save changes</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

      <div class="modal" tabindex="-1" role="dialog" id="modal-datum-numeric-add">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Tambah Pilihan</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                  <label for="Tag">Tag</label>
                  <input type="text" class="form-control"  v-model="editedItem.tag">
              </div>

              
              <div class="form-group">
                <label for="Tag">Nilai</label>
                <input v-if="data.tipe_data=='numeric'" type="number" class="form-control" step="0.00001" v-model="editedItem.val">
                <textarea v-if="data.tipe_data=='text'" name="" id=""  class="form-control" v-model="editedItem.val" cols="30" rows="5"></textarea>
                <input v-if="data.tipe_data=='string'" type="text" class="form-control" v-model="editedItem.val">


                </div>
            </div>
            <div class="modal-footer">
              <button type="button" v-if="editedItem.tag && editedItem.val" class="btn btn-primary" @click="save()">Save changes</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

      <div class="modal" tabindex="-1" role="dialog" id="modal-datum-numeric-delete">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Hapus Pilihan</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                  <label for="Tag">Tag</label>
                  <p>@{{editedItem.tag}}</p>
              </div>

              
              <div class="form-group">
                <label for="Tag">Nilai</label>
                <p>@{{editedItem.val}}</p>



                </div>
            </div>
            <div class="modal-footer">
              <button type="button" v-if="editedItem.tag && editedItem.val" class="btn btn-primary" @click="deleteItemConfirm()">Hapus</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
</div>



</div>

@stop

@section('js')
<script>
    var app_{{$SpageId}}=new Vue({
        el:'#app_{{$SpageId}}',
        data:{
            dialogDelete:false,
            dialog:false,
            editedIndex: -1,
            editedItem: {
                tag: '',
                val: null,
                tipe:null,
                selected:1,
            },
            defaultItem: {
                tag: '',
                val: null,
                tipe:null,
                selected:1,
            },
            data:{
                name:'{{old('name')}}',
                produsent_data:'{{old('produsent_data')}}',
                cara_hitung:{!!'"'.(old('cara_hitung')?_o_string_js(old('cara_hitung')):'').'"'!!},
                kode_khusus:'{{old('kode_khusus')}}',
                definisi_konsep:{!!'"'.(old('definisi_konsep')?_o_string_js(old('definisi_konsep')):'').'"'!!},
                rentang_nilai_fungsi:'{!!old('rentang_nilai_fungsi')??0!!}',
                satuan:'{{old('satuan')}}',
                jenis_data:{{old('jenis_data')?1:0}},
                tipe_aggregasi:'{{old('fungsi_aggregasi')?old('fungsi_aggregasi'):'0'}}',
                arah_nilai:{{old('arah_nilai')??1}},
                tipe_data:"{{old('tipe_data')??'numeric'}}",
                saved_rentang_pilihan:{!!old('rentang_nilai_fungsi')=='pilihan'?json_encode(old('rentang_nilai_fungsi')):'[]'!!},
                saved_rentang_min_max:{!!old('rentang_nilai')=='min-max'?json_encode(old('rentang_nilai')):'[]'!!},
                saved_single_file:{!!json_encode($file_pilihan)!!},
                saved_multy_file:{!!json_encode($file_pilihan)!!},
            },
            table_file:{
                singleSelect:false,
                header:[
                {
                    text:'Jenis File',
                    value:'tag'
                }],
                dataset:{!!json_encode($file_pilihan)!!},
                file_type_old:{!!(old('rentang_nilai_file'))?json_encode(old('rentang_nilai_file')):'[]'!!}
               
            },
            table_rentang:{
                table:null,
                dataset:{!!(old('rentang_nilai_fungsi'))?json_encode(old('rentang_nilai_fungsi')):'[]'!!}
            }
        },
        methods:{
            file_render:function(val){
                if(val!='numeric'){
                    if(this.data.rentang_nilai_fungsi=='min-max'){
                        this.data.rentang_nilai_fungsi='0';
                    }
                }
                if(['single-file','multy-file'].includes(val)){
                    for(var i in this.table_file.dataset){
                        this.table_file.dataset[i].selected=false;
                    }

                    if(val=='single-file'){
                        if(this.table_file.file_type_old.length){
                            this.table_file.file_type_old=[this.table_file.file_type_old[0]];
                        }
                    }

                    for(var i in this.table_file.dataset){
                        var tag=this.table_file.dataset[i].tag;
                        for(var ii in this.table_file.file_type_old){
                            if(this.table_file.file_type_old[ii].tag==tag){
                                this.table_file.dataset[i].selected=true;
                            }
                        }   
                    }
                }
            },
            initial:function(){
                if(this.table_rentang.table){
                    this.table_rentang.table.destroy();
                    this.table_rentang.table=null;
                }
                this.table_rentang.table=$('#table-rentang').DataTable({
                    paging: false,
                    autoWidth: false,
                    process:1,
                    sort:false,
                    serach:false,
                    data:[],
                    columns:[
                        {
                            data:'tag',

                        },
                        {
                            data:'val',

                        },
                        {
                            render:()=>{
                                return '';
                            }

                        }
                    ]
                });

            },
            draw_table_rentang:function(data){
                this.table_rentang.table.clear().draw();
                this.table_rentang.table.rows.add(data).draw();
            },
            rentang_nilai_saved:function(){
                if(this.data.rentang_nilai_fungsi=='min-max'){
                    this.data.saved_rentang_min_max=this.table_rentang.dataset
                }else if(this.data.rentang_nilai_fungsi=='pilihan'){
                    this.data.saved_rentang_pilihan=this.table_rentang.dataset;
                }
            },
            editItem (item) {
                this.editedIndex = this.table_rentang.dataset.indexOf(item)
                this.editedItem = Object.assign({}, item)
                this.show_dialog_datum_numeric_edit()
            },
            show_dialog_datum_numeric_edit:function(){
                $('#modal-datum-numeric-edit').modal();
            },
            file_selected:function(event,index){
                
                
                if($(event.target).prop('checked')){
                    this.table_file.dataset[index].selected=true;
                }else{
                    this.table_file.dataset[index].selected=false;
                }

                if(this.data.tipe_data=='single-file'){
                    for(var i in this.table_file.dataset){
                        if(i==index){
                        }else{
                            this.table_file.dataset[i].selected=false;

                        }
                     }
                }

                for(var i in this.table_file.dataset){
                    if(this.table_file.dataset[i].selected==true){
                        if(this.table_file.file_type_old.indexOf(this.table_file.dataset[i])==-1){
                            if(this.data.tipe_data=='single-file'){
                                this.table_file.file_type_old=[this.table_file.dataset[i]];
                            }else{
                                this.table_file.file_type_old.push(this.table_file.dataset[i]);
                            }
                            
                        }
                        
                    }

                }
                console.log(this.table_file.file_type_old);


                if(this.data.tipe_data=='single-file'){
                    if(this.table_file.file_type_old.length){
                        this.table_file.file_type_old=[this.table_file.file_type_old[this.table_file.file_type_old.length-1]];
                    }
                }

                
            },
            show_dialog_datum_numeric_add:function(){
                $('#modal-datum-numeric-add').modal();
                this.editedIndex=-1;
                this.editedItem = Object.assign({}, this.defaultItem);

            },

            close_dialog_datum_numeric_edit:function(){
                $('#modal-datum-numeric-edit').modal('hide');
                $('#modal-datum-numeric-add').modal('hide');

            },

            deleteItem (item) {
                this.editedIndex = this.table_rentang.dataset.indexOf(item)
                this.editedItem = Object.assign({}, item)
                this.dialogDelete = true
                $('#modal-datum-numeric-delete').modal();
            },

            deleteItemConfirm () {
                this.table_rentang.dataset.splice(this.editedIndex, 1)
                this.closeDelete()
            },

            close () {
                this.dialog = false
                this.$nextTick(() => {
                this.editedItem = Object.assign({}, this.defaultItem)
                this.editedIndex = -1
                })
            },

            closeDelete () {
                $('#modal-datum-numeric-delete').modal('hide');
                this.$nextTick(() => {
                this.editedItem = Object.assign({}, this.defaultItem)
                this.editedIndex = -1
                })
            },

            save () {
                if (this.editedIndex > -1) {
                    if(this.data.tipe_data=='numeric'){
                        this.editedItem.val=parseFloat(this.editedItem.val);
                    }
                    if(this.editItem.val!=NaN){
                        Object.assign(this.table_rentang.dataset[this.editedIndex], this.editedItem)
                    }
                
                } else {
                    if(this.data.tipe_data=='numeric'){
                        this.editedItem.val=parseFloat(this.editedItem.val);
                    }
                    if(this.editItem.val!=NaN){
                        this.table_rentang.dataset.push(this.editedItem)
                    }
                
                }
                this.close_dialog_datum_numeric_edit();
            
            },
        },
        watch:{
           
            'data.tipe_data':function(val){
                this.file_render(val);
            },
            dialog (val) {
                val || this.close()
            },
            dialogDelete (val) {
                val || this.closeDelete()
            },
            'table_rentang.dataset':function(val){
                if(val){
                    switch(this.data.rentang_nilai_fungsi){
                        case 'min-max':
                            this.data.saved_rentang_min_max=val;
                        break;
                        case 'pilihan':
                            this.data.saved_rentang_pilihan=val;
                        break;
                    }
                }
            },
            'data.rentang_nilai_fungsi':function(val){
                if(val){
                    switch(this.data.rentang_nilai_fungsi){
                        case 'min-max':
                           
                            if(this.data.saved_rentang_min_max.length){
                                this.table_rentang.dataset=this.data.saved_rentang_min_max;
                            }else{
                                this.table_rentang.dataset=[
                                    {
                                        tag:'Minimum Nilai',
                                        val:0,
                                        tipe:'min-max',
                                        selected:1,



                                    },
                                    {
                                        tag:'Maximal Nilai',
                                        val:0,
                                        tipe:'min-max',
                                        selected:1,

                        
                                    }
                            
                                ];
                            }
                        break;
                        case 'pilihan':
                        

                        if(this.data.saved_rentang_min_max.length){
                                this.table_rentang.dataset=this.data.saved_rentang_pilihan;
                            }else{
                                this.table_rentang.dataset=[
                                    
                                ];
                            }

                            
                        break;
                            
                       
                    }

                    setTimeout(() => {
                        app_{{$SpageId}}.initial();
                       setTimeout(() => {
                           console.log(app_{{$SpageId}}.table_rentang.dataset.length);
                           if(app_{{$SpageId}}.table_rentang.dataset.length){
                                app_{{$SpageId}}.draw_table_rentang(JSON.parse(JSON.stringify(app_{{$SpageId}}.table_rentang.dataset)));                           }
                        
                       }, 1000);
                    }, 300);
                }
            }
        },
        created:function(){
            this.file_render(this.data.tipe_data);
        }
       
    });
</script>

@stop