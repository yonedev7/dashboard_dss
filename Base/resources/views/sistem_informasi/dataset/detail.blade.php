@extends('adminlte::page')

@section('content_header')
<h3 class=""><b>{{$pemda->nama_pemda}}  - Tahun {{$Stahun}} </b></h3>
<p><b>Dataset</b> {{$dataset->name}} - {{$dataset->penangung_jawab}}</p>
    
@stop

@section('content')
<div id="app_{{$SpageId}}" class="content">
    <div class="card" data-app>
        <div class="card-body">
                <v-data-table
                  :headers="headers"
                  :items="desserts"
                  sort-by="calories"
                  class="elevation-1 table-bordered"
                >
                <template v-slot:item.keterisian_render='{ item }'>
                 <div class="mb-1 mt-1">
                  <p>@{{item.meta.count_data+' TW Terisi Dari '+item.meta.max_data }}</p>
                  <div class="progress">
                    
                    <div v-bind:class="'progress-bar w-'+(item.meta.count_data==0?0:parseInt((item.meta.count_data/item.meta.max_data)*100))" role="progressbar" v-bind:aria-valuenow="(item.meta.count_data==0?0:parseInt((item.meta.count_data/item.meta.max_data)*100))" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>  
                 </div>
                 </template>
                 <template v-slot:item.validasi_render='{ item }'>
                  <div class="mb-1 mt-1">
                   <p>@{{item.meta.count_finish+' TW Data Telah Tervalidasi Dari '+item.meta.max_data }}</p>
                   <div class="progress">
                    <div v-bind:class="'progress-bar w-'+(item.meta.count_finish==0?0:parseInt((item.meta.count_finish/item.meta.max_data)*100))" role="progressbar" v-bind:aria-valuenow="(item.meta.count_finish==0?0:parseInt((item.meta.count_finish/item.meta.max_data)*100))" aria-valuemin="0" aria-valuemax="100"></div>
                     
                   </div>  
                  </div>
                  </template>
                  <template v-slot:item.status_render='{ item }'>
                    <div v-if="item.status==1">Tervalidasi</div>
                    <div v-if="item.status!=1">Belum Tervalidasi</div>

                    </template>
                    
                  


                </template>
                  <template v-slot:top>
                    <v-toolbar
                      flat
                    >
                      <v-toolbar-title>Dataset</v-toolbar-title>
                      <v-divider
                        class="mx-4"
                        inset
                        vertical
                      ></v-divider>
                      <v-spacer></v-spacer>
                     
                       
              
                        
                  
                    </v-toolbar>
                  </template>
                  <template v-slot:item.actions="{ item }">
                   <v-btn icon v-bind:class="'text-center'" v-bind:href="item.link">
                      <v-icon
                        small
                        class="mr-2"

                      >
                      mdi-arrow-right-bold-hexagon-outline
                      </v-icon>
                   </v-btn>
                   
                  </template>
                 
                </v-data-table>
        </div>
    </div>
   
</div>

@stop


@section('js')
<script>
    var v_app_{{$SpageId}}=new Vue({
        el:'#app_{{$SpageId}}',
        vuetify:new Vuetify(),
        data:{
      dialog: false,
      dialogDelete: false,
      headers: [
        {
          text:'Tahun',
          value:'tahun'
        },
        {
          text: 'TW',
          sortable: true,
          value: 'tw',
        },
        {
            text:'Status',
            value:'status_render'
        },
        
        {
          text: 'User Penginput',
          value: 'meta.user_pengisi_req',
        },
        {
          text: 'Terahir di Perbarui',
          value: 'meta.tanggal_pengisian_f',
        },
        { text: 'Aksi', value: 'actions', sortable: false },
      ],
      desserts: [],
      editedIndex: -1,
      editedItem: {
        id:null,
        name: '',
        calories: 0,
        fat: 0,
        carbs: 0,
        protein: 0,
    
      },
      defaultItem: {
        id:null,
        name: '',
        calories: 0,
        fat: 0,
        carbs: 0,
        protein: 0,
 

      },
    },

    computed: {
      formTitle () {
        return this.editedIndex === -1 ? 'New Item' : 'Edit Item'
      },
    },

    watch: {
      dialog (val) {
        val || this.close()
      },
      dialogDelete (val) {
        val || this.closeDelete()
      },
    },

    created () {
      this.initialize()
    },

    methods: {
      initialize () {
        this.desserts = <?=json_encode($list_dataset)?>;
      },
      editItem (item) {
        console.log('itr',item);
        this.editedIndex = this.desserts.indexOf(item)
        this.editedItem = Object.assign({}, item)
        this.dialog = true
      },

      deleteItem (item) {
        this.editedIndex = this.desserts.indexOf(item)
        this.editedItem = Object.assign({}, item)
        this.dialogDelete = true
      },

      deleteItemConfirm () {
        this.desserts.splice(this.editedIndex, 1)
        this.closeDelete()
      },

      close () {
        this.dialog = false
        this.$nextTick(() => {
          this.editedItem = Object.assign({}, this.defaultItem)
          this.editedIndex = -1
        })
      },

      closeDelete () {
        this.dialogDelete = false
        this.$nextTick(() => {
          this.editedItem = Object.assign({}, this.defaultItem)
          this.editedIndex = -1
        })
      },

      save () {
        if (this.editedIndex > -1) {
          Object.assign(this.desserts[this.editedIndex], this.editedItem)
        } else {
          this.desserts.push(this.editedItem)
        }
        this.close()
      },
    },
    });
</script>

@stop