@extends('adminlte::page')

@section('content_header')
<h3 class=""><b>{{$pemda->nama_pemda}}  - Tahun {{$Stahun}} </b></h3>
<p><b>Dataset {{$dataset->name}}</b></p>
    
@stop

@section('content')
<div id="app_{{$SpageId}}" class="content">
    <div class="card" data-app>
        <div class="card-body">
              <table class="table table-bordered" id="table-dataset">
                <thead class="thead-dark">
                  <tr>
                    <th> Nama</th>
                    <th> Status Form</th>
                    <th> Tahun</th>
                    <th> TW</th>
                    <th>Terahir Diperbarui</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
              </table>
        </div>
    </div>
   
</div>

@stop


@section('js')
<script>
    var v_app_{{$SpageId}}=new Vue({
        el:'#app_{{$SpageId}}',
        data:{
          dataset:<?=json_encode($list_dataset)?>,
          table:null
        },
        methods:{
          init:function(){
            this.table=$('#table-dataset').DataTable({
              data:v_app_{{$SpageId}}.dataset,
              columns:[
                {
                  data:'dataset.name'
                },
                {
                    data:"status",
                    render:function(s,a,item){
                        return item.status!=null?(item.status==0?'Belum Tervalidasi':'Sudah Tervalidasi'):'Belum melakukan Pengisian';
                    }
                },
                {
                  data:'tahun'
                },
                {
                  data:'tw',
                  render:function(s,a,item){
                      return 'TW'+item.tw;
                  }
                },
               
                {
                  data:'meta.last_update_data',
                  render:function(s,a,item){
                    return item.last_update_data||'-';
                  }
                },
                {
                  render:function(s,a,item){
                    return '<div class="btn-group"><a  href="'+item.link_detail+'" class="btn btn-primary btn-sm"><i class="fa fa-arrow-right"></i></a><a  href="'+item.link_verifikasi+'" class="btn btn-primary bg-navy btn-sm"><i class="fa fa-check"></i></a></div>';
                  }
                }
              ]
            });
          }
        }

    });
    v_app_{{$SpageId}}.init();
</script>

@stop