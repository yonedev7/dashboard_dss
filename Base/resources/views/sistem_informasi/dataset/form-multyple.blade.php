@extends('adminlte::page')

@section('content_header')

    
@stop

@section('content')
<style>
    .hover-bind:hover{
        transform: scale(1.5); 
        cursor: context-menu;
    }
    .v-select.form-control{
        padding:0px;
       
    }
    .v-select.form-control .vs__dropdown-toggle{
        border:none;
    }
</style>

<div id="app_{{$SpageId}}">
    <form action="{{route('dash.si.dataset_daerah.submit_data_multy_row',['tahun'=>$Stahun,'kodepemda'=>$pemda->kodepemda,'id_dataset'=>$dataset->id,'tw'=>$tw])}}" method="post">
        @csrf
        @method('PUT')
        <div class="card card-primary" >
            <div class="card-header with-border">
                <h3 class="text-center"><b>Form Input Dataset "{{$dataset->name}}" - {{$dataset->penangung_jawab}} Tahun {{$Stahun}} - TW {{$tw}}</h3>
                    <p class="text-center">{{$pemda->nama_pemda}}</p>
            </div>
            <div class="card-body">
                <label for="">Tujuan</label>
                <p>{!!$dataset->tujuan!!}</p>
                <hr>
                <button style="position:fixed; top:20%; right:50px;" type="button" class="with-border-primary mb-1 btn btn-xl btn-success btn-circle" @click="add_data"><i class="fa fa-plus"></i> Data</button>
    
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Aksi</th>
                                <th v-for="sd in datums">@{{sd.name}}</th>
                            </tr>
                            
    
                        </thead>
                        <tbody>
                            <tr v-for="(d,key) in values">
                                <td>@{{key+1}} <input type="hidden" v-model="d.id" v-bind:name="'values['+key+'][id]'"></td>
                                <td><button type="button" @click="show_modal_hapus(key)" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button></td>
                                <td v-for="item in datums">
                                    <input class="form-control"  v-if="item.tipe_nilai=='numeric' && item.rentang_nilai_fungsi=='min-max' && item.rentang_nilai_fungsi " v-bind:min="item.rentang_nilai[0].val" v-bind:max="item.rentang_nilai[1].val" type="number"  v-bind:name="'values['+key+']['+item.field_name+']'" v-model="d[item.field_name]" class="form-control" step="0.001" aria-label="Username" aria-describedby="basic-addon1">
                                    <v-select  v-if="item.rentang_nilai_fungsi=='min-max' " v-model="d[item.field_name]" v-bind:name="'values['+key+']['+item.field_name+']'" label="text" :reduce="option => option.value"  class="form-control" :options="pilihan_option(item.rentang_nilai)" ></v-select>
                                    <input  v-if="(!item.rentang_nilai_fungsi) && item.tipe_nilai=='numeric'"  type="number"  v-bind:name="'values['+key+']['+item.field_name+']'" v-model="d[item.field_name]" class="form-control" step="0.001" aria-label="Username" aria-describedby="basic-addon1">
                                    <input  v-if="(!item.rentang_nilai_fungsi) && item.tipe_nilai=='sigle-file'" type="file"  v-bind:name="'values['+key+']['+item.field_name+']'"  accept="" class="form-control" step="0.001" aria-label="Username" aria-describedby="basic-addon1">
                                    <input  v-if="(!item.rentang_nilai_fungsi) && item.tipe_nilai=='multy-file'" type="file" multiple="true"  accept=""  v-bind:name="item.field_name+'[]'"  class="form-control" step="0.001" aria-label="Username" aria-describedby="basic-addon1">
                                    <input  v-if="(!item.rentang_nilai_fungsi) && item.tipe_nilai=='string'" type="text"  v-bind:name="'values['+key+']['+item.field_name+']'" v-model="d[item.field_name]" class="form-control" step="0.001" aria-label="Username" aria-describedby="basic-addon1">
                                    <textarea v-if="(!item.rentang_nilai_fungsi) && item.tipe_nilai=='text'" v-bind:name="item.field_name"id="" cols="30" rows="10" class="form-control"></textarea>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            <div class="modal modal-primary fade" id="modal-info" tabindex="-1" role="dialog"  aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLongTitle">Info</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                        <p><b>@{{info.name}}</b></p>
                        <hr>
                        <label for="">Satuan</label>
                        <p>@{{info.satuan}} (Tipe Nilai : @{{info.tipe_nilai_ren}})</p>
                        <hr>
                        <label for="">Definisi / Konsep</label>
                       <p> @{{info.definisi_konsep}}</p>
                    </div>
                   
                  </div>
                </div>
              </div>
             <input  type="hidden" v-for="(val,name) in values" v-bind:name="'data['+name+']'" v-bind:value="val" >
        </div>
        
    </form>

    <div class="modal fade" tabindex="-1" role="dialog" id="modal-hapus-data">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Hapus Data</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Hapus Data Row @{{form_hapus.row}}.</p>
            </div>
            <div class="modal-footer">
              <button type="button" @click="hapus_data" class="btn btn-danger"><i class="fa fa-trash"></i></button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
</div>
@stop


@section('js')
<script>
    var app_{{$SpageId}}=new Vue({
        el:'#app_{{$SpageId}}',
       
        data:{
            form_hapus:{
                row:0
            },
            selected:'',
            info:{
                name:null,
                definisi_konsep:null,
                tipe_nilai:null,
                tipe_nilai_ren:null,
                satuan:null,

            },
            themplate_data:{},
            tahun:{{$Stahun}},
            tw:{{$tw}},
            datums:<?=json_encode($datums)?>,
            values:<?=$values?json_encode($values):'{}'?>
        },
        created () {
            this.themplate_data.id=null;
            this.themplate_data['kodepemda']='{{$pemda->kodepemda}}';
            this.themplate_data['tw']={{$tw}};
            this.themplate_data['tahun']={{$Stahun}};
            for(i of this.datums){
                this.themplate_data[i.field_name]=null;
            }   
        },
        watch:{
            'value':{
                handle:function(val,old){
                    console.log(val);
                },
                deep:true
            }
        },
        methods:{
            hapus_data:function(){

                this.values.splice(this.form_hapus.row-1,1);
                console.log(this.values,this.form_hapus.row-1);
                $('#modal-hapus-data').modal('hide');


            },
            show_modal_hapus:function(index){
                this.form_hapus.row=(1+index);
                $('#modal-hapus-data').modal();
            },
            pilihan_option:function(pilihan){
                return pilihan.map(function(el){return {text:el.tag,value:el.val}});
            },
            add_data:function(){
                this.values.push(JSON.parse(JSON.stringify(this.themplate_data)));
            },
            getset:function(f){
               return eval('app_{{$SpageId}}.values.'+f);
            },
            pilihan_option:function(pilihan){
                return pilihan.map(function(el){return {text:el.tag,value:el.val}});
            },
            init:function(){
                for(var i in this.datums){
                }
            },
            show_info:function(item){
                this.info=item;
                this.tipe_nilai_ren='';
                this.info.tipe_nilai_ren=(this.info.tipe_nilai+'').replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });

                $('#modal-info').modal();
            }
        }
    });

    app_{{$SpageId}}.init();


</script>
@stop