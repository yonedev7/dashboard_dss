@extends('adminlte::page')

@section('content_header')

    
@stop

@section('content')
<style>
    .hover-bind:hover{
        transform: scale(1.5); 
        cursor: context-menu;
    }
    .v-select.form-control{
        padding:0px;
       
    }
    .v-select.form-control .vs__dropdown-toggle{
        border:none;
    }
</style>

<form action="{{route('dash.si.dataset_daerah.submit_data',['tahun'=>$Stahun,'kodepemda'=>$pemda->kodepemda,'id_dataset'=>$dataset->id,'tw'=>$tw])}}" method="post">
    @csrf
    @method('PUT')
    <div class="card card-primary" id="app_{{$SpageId}}">
        <div class="card-header with-border">
            <h3 class="text-center"><b>Form Input Dataset "{{$dataset->name}}" - {{$dataset->penangung_jawab}} Tahun {{$Stahun}} - TW {{$tw}}</h3>
                <p class="text-center">{{$pemda->nama_pemda}}</p>
        </div>
        <div class="card-body">
            <label for="">Tujuan</label>
            <p>{!!$dataset->tujuan!!}</p>
            <hr>
            <div class="row" style="width:100%">
                <div v-for="(item,key) in datums" class="col-md-6">
                    <div class=" ">
                        <label for="">@{{(key+1)}}. <span class="hover-bind" @click="show_info(item)" ><i class="fa fa-info-circle"></i></span>  @{{item.name}}</label>
                        <div v-if="item.tipe_nilai!='text'" class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text" id="basic-addon1">Tahun @{{item.tahun_data+tahun}}</span>
                              <span class="input-group-text" id="basic-addon1">TW @{{item.tw_data}}</span>
                            </div>
                        
                            <input class="form-control"  v-if="item.tipe_nilai=='numeric' && item.rentang_nilai_fungsi=='min-max' && item.rentang_nilai_fungsi " v-bind:min="item.rentang_nilai[0].val" v-bind:max="item.rentang_nilai[1].val" type="number"  v-bind:name="item.field_name" v-model="values[item.field_name]" class="form-control" step="0.001" aria-label="Username" aria-describedby="basic-addon1">
                            
                            <v-select  v-if="item.rentang_nilai_fungsi=='min-max' " v-model="values[item.field_name]" v-bind:name="item.field_name" label="text" :reduce="option => option.value"  class="form-control" :options="pilihan_option(item.rentang_nilai)" ></v-select>
                            <v-select  v-if="item.rentang_nilai_fungsi=='pilihan' " v-model="values[item.field_name]" v-bind:name="item.field_name" label="text" :reduce="option => option.value"  class="form-control" :options="pilihan_option(item.rentang_nilai)" ></v-select>
                           
                            <input  v-if="(!item.rentang_nilai_fungsi) && item.tipe_nilai=='numeric'"  type="number"  v-bind:name="item.field_name" v-model="values[item.field_name]" class="form-control" step="0.001" aria-label="Username" aria-describedby="basic-addon1">
                            <input  v-if="(!item.rentang_nilai_fungsi) && item.tipe_nilai=='sigle-file'" type="file"  v-bind:name="item.field_name"  accept="" class="form-control" step="0.001" aria-label="Username" aria-describedby="basic-addon1">
                            <input  v-if="(!item.rentang_nilai_fungsi) && item.tipe_nilai=='multy-file'" type="file" multiple="true"  accept=""  v-bind:name="item.field_name+'[]'"  class="form-control" step="0.001" aria-label="Username" aria-describedby="basic-addon1">
                            <input  v-if="(!item.rentang_nilai_fungsi) && item.tipe_nilai=='string'" type="text"  v-bind:name="item.field_name" v-model="values[item.field_name]" class="form-control" step="0.001" aria-label="Username" aria-describedby="basic-addon1">
                            <textarea v-if="(!item.rentang_nilai_fungsi) && item.tipe_nilai=='text'" v-bind:name="item.field_name"id="" cols="30" rows="10" class="form-control"></textarea>
                            <br>
                         
                           
                            <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon1">@{{item.satuan}}</span>
                            </div>
                              
                        </div>
                        <div v-if="item.tipe_nilai=='text'">
                        <p>Tahun: @{{item.tahun_data+tahun}} - TW @{{item.tw_data}}</p>
                        <textarea  v-bind:name="item.field_name" v-model="values[item.field_name]" class="form-control" rows="10"></textarea>
                        </div>
                        
    
                    </div>
    
                </div>
            </div>
        </div>
        <div class="card-footer">
            <button class="btn btn-primary" type="submit">Submit</button>
        </div>
    
        <div class="modal modal-primary fade" id="modal-info" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">Info</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    <p><b>@{{info.name}}</b></p>
                    <hr>
                    <label for="">Satuan</label>
                    <p>@{{info.satuan}} (Tipe Nilai : @{{info.tipe_nilai_ren}})</p>
                    <hr>
                    <label for="">Definisi / Konsep</label>
                   <p> @{{info.definisi_konsep}}</p>
                </div>
               
              </div>
            </div>
          </div>
         <input  type="hidden" v-for="(val,name) in values" v-bind:name="'data['+name+']'" v-bind:value="val" >
    </div>
</form>
@stop


@section('js')
<script>
    var app_{{$SpageId}}=new Vue({
        el:'#app_{{$SpageId}}',
       
        data:{
            selected:'',
            info:{
                name:null,
                definisi_konsep:null,
                tipe_nilai:null,
                tipe_nilai_ren:null,
                satuan:null,

            },
            tahun:{{$Stahun}},
            tw:{{$tw}},
            datums:<?=json_encode($datums)?>,
            values:<?=$values?json_encode($values):'{}'?>
        },
        created () {
            for(i in this.datums){
                // if(this.values[this.datums[i].field_name]==undefined){
                //     this.values[this.datums[i].field_name]=null;
                // }
            }   
        },
        watch:{
            'value':{
                handle:function(val,old){
                    console.log(val);
                },
                deep:true
            }
        },
        methods:{
            getset:function(f){
               return eval('app_{{$SpageId}}.values.'+f);
            },
            pilihan_option:function(pilihan){
                return pilihan.map(function(el){return {text:el.tag,value:el.val}});
            },
            init:function(){
                for(var i in this.datums){
                }
            },
            show_info:function(item){
                this.info=item;
                this.tipe_nilai_ren='';
                this.info.tipe_nilai_ren=(this.info.tipe_nilai+'').replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });

                $('#modal-info').modal();
            }
        }
    });

    app_{{$SpageId}}.init();


</script>
@stop