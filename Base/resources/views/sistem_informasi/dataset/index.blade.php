@extends('adminlte::page')

@section('content_header')
<h3 class=""><b>{{$pemda->nama_pemda}}  - Tahun {{$Stahun}} </b></h3>
    
@stop

@section('content')
<div id="app_{{$SpageId}}" class="content">
    <div class="card" data-app>
        <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="table-dataset">
                <thead class="thead-dark">
                  <tr>
                    <th> Nama</th>
                    <th> Status Pengisian</th>
                    <th> Status Verifikasi</th>
                    <th>Terahir Diperbarui</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
              </table>
            </div>
        </div>
    </div>
   
</div>

@stop


@section('js')
<script>
    var v_app_{{$SpageId}}=new Vue({
        el:'#app_{{$SpageId}}',
        data:{
          dataset:<?=json_encode($dataset)?>,
          table:null
        },
        methods:{
          init:function(){
            this.table=$('#table-dataset').DataTable({
              data:v_app_{{$SpageId}}.dataset,
              columns:[
                {
                  data:'name'
                },
                {
                  data:'(item.meta.count_finish!=0?(item.meta.count_finish/item.meta.max_data)*100:0).toFixed(2)',
                  type:'html',
                 render:function(s,a,item){
                  if(item.meta.jenis_dataset){
                    return item.meta.count_data+ ' Data Terisi';
                   }else{
                   return item.meta.count_data+' Dataset Terisi / '+item.meta.max_data+' Dataset <br>'+
                   '<div class="progress" style="height:8px;">'+
                      '<div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="'+(item.meta.count_finish!=0?(item.meta.count_finish/item.meta.max_data)*100:0).toFixed(2)+'" aria-valuemin="0" aria-valuemax="100" style="width: '+(item.meta.count_finish!=0?(item.meta.count_finish/item.meta.max_data)*100:0).toFixed(2)  +'%"></div>'+
                    '</div>';
                  }
                }
                },
                {
                  data:'(item.meta.count_finish!=0?(item.meta.count_finish/item.meta.max_data)*100:0).toFixed(2)',
                  type:'html',
                 render:function(s,a,item){
                   return item.meta.count_finish+' Terverifikasi / '+item.meta.count_data+' Dataset Terisi <br>'+
                   '<div class="progress" style="height:8px;">'+
                      '<div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="'+((item.meta.count_finish!=0&&item.meta.count_data!=0)?(item.meta.count_finish/item.meta.count_data)*100:0).toFixed(2)+'" aria-valuemin="0" aria-valuemax="100" style="width: '+((item.meta.count_finish!=0&&item.meta.count_data!=0)?(item.meta.count_finish/item.meta.count_data)*100:0).toFixed(2)  +'%"></div>'+
                    '</div>';
                 }
                },
                {
                  data:'meta.last_update_data',
                  render:function(s,a,item){
                    return item.meta.last_update_data||'-';
                  }
                },
                {
                  render:function(s,a,item){
                    return '<a  href="'+item.meta.link_detail+'" class="btn btn-primary btn-sm"><i class="fa fa-arrow-right"></i></a>';
                  }
                }
              ]
            });
          }
        }

    });
    v_app_{{$SpageId}}.init();
</script>

@stop