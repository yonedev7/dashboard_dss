@extends('adminlte::page')

@section('content_header')
    <h4>Selamat Datang Di Sistem Informasi Air Minum</h4>

@stop

@section('content')
<div id="app">
    <div class="d-flex p-3">
        <div class="col-md-6  col-md-offset-3 bg-white p-3 shadow rounded">
            <div class="form-group">
                <label for="">Subcribe Dataset</label>
                <br>
                <button class="btn btn-danger" @click="showForm"><i class="fa fa-bell"></i>   Subcribe</button>
                <p><small class="text-center text-capitalize">
                    Tambahkan Dataset pada halaman desk Dashboard anda untuk melihat pergerakan data sesuai pilihan, anda juga akan mendapatkan notifikasi melalui jaringan whatsapp untuk setiap perubahan dataset yang anda subcribe 😉
                </small></p>
            </div>
        </div>
    </div>
    <div ref="modal_form_subcribe" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
        <div class="modal-dialog  modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="my-modal-title">List Dataset Subcribe</h5>
                    <button class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead class="bg-primary">
                                <tr>
                                    <th>Nama Dataset</th>
                                    <th style="width:150px">Status Subcribe</th>
                                </tr>
                            </thead>
                            <tbody>
                                <template>
                                    <tr v-for="item in list_dataset">
                                        <td>
                                            <p>@{{item.name}}</p>
                                            <small v-html="item.tujuan"></small>
                                        </td>
                                        <td>
                                            <toggle-button @change="updateSub(item)" v-model="item.subscribe_status"></toggle-button>
                                        </td>
                                    </tr>
                                </template>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="p-3 container-printed">
        <p class="for-printed" >@{{date}}</p>
        <div class="row">
            <div class="col-md-6 mt-2 "  v-for="item in list_subcribe">
                <div class="bg-white rounded shadow p-2">
                    <v-dash-dts-keterisian-chart :id_content="buildIdComponent(item)" :regional="item.regional_only"   link="{{route('api-web-mod.dataset.data.series.column.keterisian',['tahun'=>'@?','id_dataset'=>'@?'])}}" :tahun="{{$StahunRoute}}" :dataset="item" ></v-dash-dts-keterisian-chart>

                </div>
                
            </div>
        </div>
    </div>

</div>

@stop


@section('js')
    <script>
        var app=new Vue({
            el:'#app',
            data:{
                date:moment().format('DD MMMM YYYY h:m a'),
                autho_reload:6000,
                list_dataset:[],
            },
            created:function(){
                this.getDatasetSubribe();
            },
            watch:{
                list_dataset:function(){

                }
            },
            computed:{
                list_subcribe:function(){
                    return this.list_dataset.filter(el=>{
                        return el.subscribe_status;
                    });
                }
            },
            methods:{
                buildIdComponent:function(item){
                    return 'id_'+item.id+'_'+moment().format('DDMMYYY');
                },
                updateSub:function(item){
                    var self=this;
                    self.$isLoading(true);
                    req_ajax.post('{{route('api-web-mod.dataset.update-subcribe',['tahun'=>$StahunRoute])}}',{
                        id_dataset:item.id,
                        status:item.subscribe_status
                    }).then(res=>{
                        Object.assign(self.list_dataset,res.data);
                        
                    }).finally(function(){
                        setTimeout((ref) => {
                            ref.$isLoading(false);
                        
                            ref.getDatasetSubribe();
                            
                        }, 1000,self);
                    });

                        
                },
                getDatasetSubribe:function(){
                    var self=this;
                    self.$isLoading(true);

                    req_ajax.post('{{route('api-web-mod.dataset.get-subcribe',['tahun'=>$StahunRoute])}}',{user:{{$info_user->id}}}).then(res=>{
                        self.list_dataset=res.data;
                    }).finally(function(){
                        self.$isLoading(false);

                    });
                },
                showForm:function(){
                    $(this.$refs.modal_form_subcribe).modal({
                        backdrop:'static'
                    });
                }
            }
        })
    </script>

@stop