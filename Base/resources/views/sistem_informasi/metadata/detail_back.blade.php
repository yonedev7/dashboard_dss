@extends('adminlte::page')

@section('content_header')
<style>
  .btn-xxs{
    font-size: 8px!important;
    width:20px;
    height: 20px;
    padding: 0;
  }
</style>
<h3 id="h_a"><b>Dataset : {{name||'<?=$dataset->name?>'}}</b></h3>
@stop
@section('content')
<div class="card" id="app_{{$SpageId}}">
  <div class="card-body">
    <div v-if="step==1">
      <h4>META DATASET</h4>
      <hr>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="">Kode</label>
              <input type="text" class="form-control" v-model="meta.kode">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="">Nama</label>
              <input type="text" class="form-control" v-model="meta.name">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="">Jenis Dataset</label>
              <select name="" id="" class="form-control" v-model="meta.jenis_dataset">
                <option value="0">Capaian</option>
                <option value="1">Existing</option>

              </select>
          </div>
        </div>
        
        <div class="col-md-6">
          <div class="form-group">
            <label for="">Interval Pengambilan</label>
            <select name="" id="" class="form-control" v-model="meta.interval_pengambilan">
              <option value="-1">Tahunan</option>
              <option value="0">Triwulanan</option>
            </select>
          </div>
        </div>
       
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="">Tujuan</label>
             <textarea name=""  class="form-control" v-model="meta.tujuan" id="" cols="30" rows="10"></textarea>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="">Status Dataset</label>
            <select name="" id="" class="form-control" v-model="meta.status">
              <option value="1">Publish</option>
              <option value="0">Draf</option>
            </select>
          </div>
          <div class="form-group">
            <label for="">Tanggal Publish</label>
            <input type="date" class="form-control" v-model="meta.tanggal_publish">

          </div>

        </div>
      </div>
      <hr>

      <div class="row">
        <div class="col-md-12">
          <div class="btn-group">
              <button type="button" @click="step=2" class="btn btn-primary"><i class="fa fa-arrow-right"></i> Schema List Data </button>
          </div>
        </div>
      </div>
    </div>
    <div v-if="step==2">
      <h4>SCHEMA LIST DATA</h4>
      <hr>
      <div class="row">
        <div class="col-md-6 table-responsive">
          <p><b>DATASET</b></p>

          
  
          <table class="table table-bordered">
            <thead class="thead-dark">
              <tr>
                <th></th>
                <th>
                  No.
                </th>
                <th>
                  Kode
                </th>
                <th>
                  Nama Dataset
                </th>
                <th>
                  Satuan
                </th>
                <th>
                  Tahun
                </th>
                <th>
                  TW
                </th>
                <th>
                  Aksi
                </th>
              </tr>
            </thead>
            <tbody  >
           

                <tr  v-for="(item,key) in meta.datas" :key="item.id_list">
                  <td scope="row">
                    <i class="fas fa-arrows-alt item-sr">
                  </td>
                  <td>@{{key+1}}.</td>
                  <td>@{{item.kode}}</td>
                  <td>@{{item.name}}</td>
                  <td>@{{item.satuan}}</td>
                  <td>@{{item.tahun_data>=0?'+'+item.tahun_data:item.tahun_data}} Tahun</td>
                  <td>@{{item.tw_data}}</td>
                  <td>
                  <div class="btn-group">
                    <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-pen"></i></button>
                    <button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                  </div>
                  </td>
                </tr>
            

          </table>
        </div>
        <div class="col-md-6 table-responsive">
          <p><b>SCHEMA DATA</b></p>
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Cari Schema Data" v-model="sumber.datas.query">
          </div>
          <table class="table table-bordered">
            <thead>
              <tr>
              <th>
                Kode
              </th>
              <th>
                Validator
              </th>
              <th>
                Nama Dataset
              </th>
              <th>
                Satuan
              </th>
              <th>Aksi</th>
              </tr>
            </thead>
            <tbody >
               <tr class="item-r" v-for="item,key in sumber.datas.data"  > 
                <td>@{{item.kode}}</td>
                <td>@{{item.nama_validator}}</td>
                <td>@{{item.name}}</td>
                <td>@{{item.satuan}}</td>
                <td>
                  <button type="button" @click="tf_form_add_data(key)" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i></button>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <hr>

      <div class="row">
        <div class="col-md-12">
        
          <div class="btn-group">
            <button type="button" @click="step=1" class="btn btn-info"><i class="fa fa-arrow-left"></i> Meta Dataset </button>
              <button type="button" @click="step=3" class="btn btn-primary"><i class="fa fa-arrow-right"></i> Scope PEMDA </button>
          </div>
        </div>
      </div>
    </div>
    <div v-if="step==3">
      <h4>SCOPE PEMDA - Tahun {{$Stahun}}</h4>
      <hr>
      <p class="bg-yellow p-2">Dengan tidak mengsikan Scope PEMDA maka dataset ini dapat diisi oleh seluruh PEMDA.</p>
      <div class="row">
        <div class="col-md-6">
          <p>Total Pemda: @{{meta.pemda_list.length}}</p>
          <table class="table table-bordered">
            <thead class="thead-dark">
              <tr>
                <th>No.</th>
                <th>PEMDA</th>
                <th>Tahun</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <tr v-for="(item,key) in meta.pemda_list">
                <td>@{{key+1}}</td>
                <td>@{{item.nama_pemda}}</td>
                <td>@{{item.tahun_data}}</td>
                <td>
                  <div class="btn-group">
                    <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-pen"></i></button>
                    <button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>  
        <div class="col-md-6">
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Cari Pemda" v-model="sumber.pemda_list.query">
          </div>
          <table class="table table-bordered">
            <thead>
              <tr>
                <th>KODEPEMDA</th>
                <th>PEMDA</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <tr v-for="(item,key) in sumber.pemda_list.data">
                <td>@{{item.kodepemda_2}}</td>
                <td>@{{item.nama_pemda}}</td>
                <td>
                  <button class="btn btn-sm btn-primary" @click="tf_pemda_form_add(key)" ><i class="fa fa-plus"></i></button>
                </td>
              </tr>
            </tbody>
          </table>
        </div>  
      </div>
      <hr>
      <div class="row">
        <div class="col-md-12">
          <div class="btn-group">
            <button type="button" @click="step=2" class="btn btn-info"><i class="fa fa-arrow-left"></i> Schema List Data </button>
            <button type="button" @click="step=4" class="btn btn-primary"><i class="fa fa-arrow-right"></i> Gaya Tampilah </button>
          </div>
        </div>
      </div>
    </div>
    <div v-if="step==4">
      <h4>GAYA TAMPILAN</h4>
      <hr>
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label for="">Gaya Tampilan</label>
            <select name="" class="form-control" v-model="display.style" id="">
              <option value="0">TABULASI KESELURUHAN</option>
              <option value="1">TABULASI BERJENJANG</option>
            </select>
          </div>
          <p><b>Berikan Tampilan Informasi</b></p>
          <div> 
           
            <div v-for="(item,keys) in display.schema">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th colspan="4">@{{item.name}}</th>
                  </tr>
                  <tr>
                    <th v-for="(colom,keyc) in item.schema">@{{colom.name}}</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                   <td v-for="(colom,keyc) in item.schema">
                     <div class="form-group">
                       <label for="">Judul</label>
                       <input type="text" class="form-control" v-model="colom.judul">
                     </div>
                    <div class="form-group">
                      <select name=""  class="form-control" id="" v-model="colom.style">
                        <option value="0">NONE</option>
                        <option value="COLUMN">CHART COLUMN</option>
                        <option value="LINE">CHART LINE</option>
                        <option value="BAR">CHART BAR</option>
                        <option value="MAP">CHART MAP</option>
                      </select>
                    </div>
                    <hr>
                    <div class="form-group">
                      <label for="">Values</label>
                       <div class="col-md-12">
                        <button class="btn btn-success btn-sm" @click="tambah_value(keys,keyc)"><i class="fa fa-plus"></i> Value</button>
                        <hr>
                        <div v-for="(value_schema,keyv) in colom.value">
                          <div class="form-group">
                            <label for="">Judul Value @{{keyv+1}}   <span><button @click="hapus_value(keys,keyc,keyv)" class="btn btn-xxs btn-danger"><i class="fa fa-trash"></i></button></span></label>
                            <input type="text" class="form-control" v-model="value_schema.name">
                          </div>
                          <p>Query Value @{{keyv+1}}</p>
                          <div class="row no-gutters">
                            <div class="col-md-8 ">
                              <div class="p-1 mb-1 bg-dark">
                                <span v-bind:class="'badge  m-1 '+(wt.context=='operator'?'badge-success':'badge-primary')" v-for="(wt,keyw) in value_schema.list">@{{wt.id_list}} <button @click="hapus_op(keys,keyc,'value',keyv,keyw)" class="btn btn-xxs btn-danger"><i class="fa fa-trash"></i></button></span>
                              </div>
                              <p style="font-size:8px;">
                              @{{value_schema.list.map(function(elm){return elm.id_list}).join(' ')}}
                              </p>
                            </div>
                            <div class="col-md-4">
                              <div class="btn-group">
                                  <button class="btn btn-sm btn-success" @click="show_source_operator(keys,keyc,'value',keyv)"></i>Op</button>
                                  <button class="btn btn-sm btn-primary" @click="show_source_data(keys,keyc,'value',keyv)"></i>Data</button>
                              </div>
                            </div>
                           </div>
                           <hr>
                         </div>
                        
                        </div>
                     
                    </div>
                   <hr>
                    <div class="form-group">
                      <label for="">Where </label>
                       <div class="col-md-12">
                        <div class="row no-gutters">
                          <div class="col-md-8 ">
                            <div class="p-1 mb-1 bg-dark">
                              <span  v-bind:class="'badge  m-1 '+(wt.context=='operator'?'badge-success':'badge-primary')"  v-for="(wt,keyw) in colom.where">@{{wt.id_list}} <button @click="hapus_op(keys,keyc,'where',null,keyw)" class="btn btn-xxs btn-danger"><i class="fa fa-trash"></i></button></span>
                            </div>
                             <p style="font-size:8px;">@{{colom.where.map(function(elm){return elm.id_list}).join(' ')}}</p>
                            
                          </div>
                          <div class="col-md-4">
                            <div class="btn-group">
                                <button class="btn btn-sm btn-success" @click="show_source_operator(keys,keyc,'where',null)" ></i>Op</button>
                                <button class="btn btn-sm btn-primary"  @click="show_source_data(keys,keyc,'where',null)"></i>Data</button>
                            </div>
                          </div>
                         </div>
                       </div>
                     
                    </div>
                    <hr>
                    <div class="form-group">
                      <label for="">Group By </label>
                       <div class="col-md-12">
                        <div class="row no-gutters">
                          <div class="col-md-8 ">
                            <div class="p-1 mb-1 bg-dark">
                              <span v-bind:class="'badge  m-1 '+(wt.context=='operator'?'badge-success':'badge-primary')" v-for="(wt,keyw) in colom.group">@{{wt.id_list}} <button @click="hapus_op(keys,keyc,'group data',null,keyw)" class="btn btn-xxs btn-danger"><i class="fa fa-trash"></i></button></span>
                            </div>
                            <p style="font-size:8px;">@{{colom.group.map(function(elm){return elm.id_list}).join(' ')}}</p>
                            
                          </div>
                          <div class="col-md-4">
                            <div class="btn-group">
                                <button class="btn btn-sm btn-success" @click="show_source_operator(keys,keyc,'group data',null)" ></i>Op</button>
                                <button class="btn btn-sm btn-primary" @click="show_source_data(keys,keyc,'group data',null)"></i>Data</button>
                            </div>
                          </div>
                         </div>
                       </div>
                     
                    </div>
                    <hr>  
                    <div class="form-group">
                      <label for="keterangan"> keteragan
                       
                      </label>
                      <textarea name="" class="form-control" id="" cols="30" rows="10"></textarea>
                    </div>
                   </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <hr>
        <div class="row">
          <div class="col-md-12">
            <form action="{{route('dash.si.schema.dataset.update',['tahun'=>$Stahun,'id_dataset'=>$dataset->id])}}" method="post" v-if="form_sub" id="form-sub">
              @csrf
              @method('PUT')
              <input type="hidden" name="name" v-model="meta.name">
              <input type="hidden" name="kode" v-model="meta.kode">
              <input type="hidden" name="tujuan" v-model="meta.tujuan">
              <input type="hidden" name="status" v-model="meta.status">
              <input type="hidden" name="jenis_dataset" v-model="meta.jenis_dataset">
              <input type="hidden" name="tanggal_publish" v-model="meta.tanggal_publish">
              <input type="hidden" name="interval_pengambilan" v-model="meta.interval_pengambilan">
              <input type="hidden" name="datas" v-model="com_datas">
              <input type="hidden" name="scope" v-model="com_scope">
              <input type="hidden" name="display" v-model="com_display">



            </form>
            
            <div class="btn-group">
              <button type="button" @click="step=3" class="btn btn-info"><i class="fa fa-arrow-left"></i> Scope PEMDA </button>
              <button type="button"  @click="submit" class="btn btn-primary"><i class="fa fa-arrow-right"></i> Update </button>
            </div>
          </div>
        </div>
      
    </div>
  </div>

  <div class="modal" tabindex="-1" role="dialog" id="modal-add-datalist">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Tambah Schema Data</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
         <div class="form-group">
           <label for="">Nama</label>
           <p>@{{sumber.datas.form_add.name}} -  @{{sumber.datas.form_add.satuan}}</p>
         </div>
         <div class="form-group">
          <label for="">Definisi Konsep</label>
          <p>@{{sumber.datas.form_add.definisi_konsep}}</p>
        </div>

        <div class="form-group">
          <label for="">Tahun Data</label>
          <select name="" class="form-control" id="" v-model="sumber.datas.form_add.tahun_data">
            <option  v-for="i in tahun_list()" v-bind:value="i.val">@{{i.val>=0?'+'+i.val:i.val}} Tahun</option>
          </select>
        </div>
        <div class="form-group">
          <label for="">TW Data</label>
          <select name="" class="form-control" id="" v-model="sumber.datas.form_add.tw_data">
            <option value="" v-for="i in tw_list()" v-bind:value="i.val">TW @{{i.val}}</option>
          </select>
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" @click="tf_form_add_t_datas">Add</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" tabindex="-1" role="dialog" id="modal-add-pemdalist">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Tambah  Scope PEMDA</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
         <div class="form-group">
           <label for="">Nama</label>
           <p>@{{sumber.pemda_list.form_add.nama_pemda}} </p>
         </div>
         <div class="form-group">
          <p>Pengisian Dataset Tahun @{{sumber.pemda_list.form_add.tahun_data}}</p>
        </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" @click="tf_pemda_form_pemda_list">Add</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" tabindex="-1" role="dialog" id="modal-add-op">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Tambah Operator Pada @{{display.form_op.context}}</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
         <P>Jenjang <b>@{{display.form_op.nama_jenjang}}</b> - <b>@{{display.form_op.nama_schema}}</b></P>
         <hr>
         <table class="table table-bordered" id="table-op">
           <thead>
             <tr>
               <th>Nama</th>
               <th>Aksi</th>
             </tr>
           </thead>
           <tbody>
             
           </tbody>
         </table>
        </div>
       
      </div>
    </div>
  </div>
  <div class="modal" tabindex="-1" role="dialog" id="modal-add-data">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Tambah Data Pada @{{display.form_data.context}}</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
         <P>Jenjang <b>@{{display.form_data.nama_jenjang}}</b> - <b>@{{display.form_data.nama_schema}}</b></P>
         <hr>
          <div class="table-responsive">
            <table class="table table-bordered" id="table-data">
              <thead>
                <tr>
                 <th>Kode</th>
                  <th>Nama</th>
                  <th>Satuan</th>
   
                  <th>Tahun</th>
                  <th>TW</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                
              </tbody>
            </table>
          </div>
        </div>
       
      </div>
    </div>
  </div>
 
</div>
@stop

@section('js')
<script>
  var h_a=new Vue({
    el:'#h_a',
    data:{
      name:''
    }
  });
  var app_{{$SpageId}}=new Vue({
    el:'#app_{{$SpageId}}',
    data:{
      form_sub:false,
      named_table:'dts.',
      step:4,
      display:{
        form_op:{
          table:null,
          nama_jenjang:'',
          nama_schema:'',
          context:'',
          nama_value:'',
          index_jenjang:-1,
          index_schema:-1,
          index_value:null,
          op:{}
        },
        form_data:{
          table:null,
          nama_jenjang:'',
          nama_schema:'',
          context:'',
          nama_value:'',
          index_jenjang:-1,
          index_schema:-1,
          index_value:null,
          data:{}
        },
        style:0, 
        judul:'',
        keterangan:'',
        schema_all:[
          {
            name:'NASIONAL',
            schema:[
              {
              name:'Schema Tampilan 1',
              style:0, 
              judul:'',
            keterangan:'',
              tag:[],
              group:[],
              where:[],
              value:[],
            },
            {
              name:'Schema Tampilan 2',
              style:0, 
              judul:'',
            keterangan:'',
              tag:[],
              group:[],
              where:[],
              value:[],
            },
            {
              name:'Schema Tampilan 3',
              style:0, 
              judul:'',
            keterangan:'',
              tag:[],
              group:[],
              where:[],
              value:[],
            },
            {
              name:'Schema Tampilan 4',
              style:0, 
              judul:'',
            keterangan:'',
              tag:[],
              group:[],
              where:[],
              value:[],
            }
          ]
          }
          
        ],
        schema_jenjang:[
            {
              name:'NASIONAL',
              schema:[
                {
                  name:'Schema Tampilan 1',
                  style:0, 
                judul:'',
          keterangan:'',
                  tag:[],
                  group:[],
                  where:[],
                  value:[],
                },
                {
                  name:'Schema Tampilan 2',
                  style:0, 
                judul:'',
          keterangan:'',
                  tag:[],
                  group:[],
                  where:[],
                  value:[],
                },
                {
                  name:'Schema Tampilan 3',
                  style:0, 
                judul:'',
          keterangan:'',
                  tag:[],
                  group:[],
                  where:[],
                  value:[],
                },
                {
                  name:'Schema Tampilan 4',
                  style:0, 
                judul:'',
          keterangan:'',
                  tag:[],
                  group:[],
                  where:[],
                  value:[],
                }
              ]
            },
            {
              name:'PROVINSI',
              schema:[
                {
                  name:'Schema Tampilan 1',
                  style:0, 
                judul:'',
          keterangan:'',
                  tag:[],
                  group:[],
                  where:[],
                  value:[],
                },
                {
                  name:'Schema Tampilan 2',
                  style:0, 
                judul:'',
          keterangan:'',
                  tag:[],
                  group:[],
                  where:[],
                  value:[],
                },
                {
                  name:'Schema Tampilan 3',
                  style:0, 
                judul:'',
          keterangan:'',
                  tag:[],
                  group:[],
                  where:[],
                  value:[],
                },
                {
                  name:'Schema Tampilan 4',
                  style:0, 
                judul:'',
          keterangan:'',
                  tag:[],
                  group:[],
                  where:[],
                  value:[],
                }
              ]
            },
            {
              name:'KAB/KOTA',
              schema:[
                {
                  name:'Schema Tampilan 1',
                  style:0, 
                  judul:'',
                  keterangan:'',
                  tag:[],
                  group:[],
                  where:[],
                  value:[],
                },
                {
                  name:'Schema Tampilan 2',
                  style:0, 
                judul:'',
                keterangan:'',
                  tag:[],
                  group:[],
                  where:[],
                  value:[],
                },
                {
                  name:'Schema Tampilan 3',
                  style:0, 
                judul:'',
          keterangan:'',
                  tag:[],
                  group:[],
                  where:[],
                  value:[],
                },
                {
                  name:'Schema Tampilan 4',
                  style:0, 
                judul:'',
          keterangan:'',
                  tag:[],
                  group:[],
                  where:[],
                  value:[],
                }
              ]
            }
          ],
          schema:[],
          data:[
            {
              id_list:'nama_pemda',
              name:'Nama Pemda',
              satuan:'-',
              definisi_konsep:'-',
              cara_hitung:'-',
              tahun_data:0,
              tw_data:4,
              kode:'',
              fungsi_rentang_nilai:'{}',
              fungsi_aggregasi:0
            },
            {
              id_list:'kodepemda',
              name:'Kode Pemda',
              satuan:'-',
              definisi_konsep:'-',
              cara_hitung:'-',
              tahun_data:0,
              tw_data:4,
              kode:'',

              fungsi_rentang_nilai:'{}',
              fungsi_aggregasi:0
            },
            {
              id_list:'tanggal_pengisian',
              name:'Tanggal Pengisian',
              satuan:'-',
              definisi_konsep:'-',
              cara_hitung:'-',
              tahun_data:0,
              tw_data:4,
              kode:'',
              fungsi_rentang_nilai:'{}',
              fungsi_aggregasi:0
            }
        ],
        operator:[
            {
              id_list:'CONCAT',
              name:'CONCATINATE'
            },
            {
              id_list:'STRING_AGG',
              name:'CONCATINATE ROW'
            },
            {
              id_list:'(',
              name:'OPEN PARENTASES'
            },
            {
              id_list:')',
              name:'CLOSE PARENTASES'
            },
            {
              id_list:'SUM',
              name:'SUM'
            },
            {
              id_list:'MAX',
              name:'MAX'
            },
            {
              id_list:'MIN',
              name:'MIN'
            },
            {
              id_list:'AVERAGE',
              name:'AVARAGE'
            },
            {
              id_list:'OR',
              name:'OR'
            },
            {
              id_list:'AND',
              name:'AND'
            },
            {
              id_list:'<>',
              name:'NOT EQUAL'
            },
            {
              id_list:'=',
              name:'EQUAL'
            },
            {
              id_list:'<',
              name:'LES THEN'
            } ,
            {
              id_list:'>',
              name:'MORE THEN'
            },
            {
              id_list:'DISTINCT',
              name:'DISTINCT'
            },
            {
              id_list:'|',
              name:'SPLITER'
            },
            {
              id_list:'%',
              name:'More Charakter'
            },
            {
              id_list:'ilike',
              name:'MATCH BY CHARACTER'
            }                         
          ]
        },
      meta:{
        name:'{{$dataset->name}}',
        kode:'{{$dataset->kode}}',
        jenis_dataset:{{$dataset->jenis_dataset??0}},
        status:{{$dataset->status??0}},
        tanggal_publish:'{{$dataset->tanggal_publish??''}}',
        interval_pengambilan:{{$dataset->interval_pengambilan??0}},
        tujuan:'{{isset($dataset->tujuan)?_o_string_js($dataset->tujuan):''}}',
        options:{
          handle:'.item-sr'
        },
        pemda_list:[],
        datas:[
          {
            id:0,
            id_list:'11111',
            name:'dsdsdss',
            tahun_data:-1,
            tw_data:4,
            kode:'sds',
            satuan:'sdsd'

          },
          {
            id:0,
            id_list:'sdsss',
            name:'2',
            tahun_data:-1,
            tw_data:4,
            kode:'sds',
            satuan:'sdsd'

          }
        ],
      },
      sumber:{
        datas:{
          form_add:{},
          query:'',
          token:null,
          data:[]
        },
        pemda_list:{
          data:[],
          form_add:{},
          token:null,
          query:''
        }
        
      }
    },
    watch:{
      'sumber.datas.query':function(){
        this.sumber_datas_ajax();
      },
      'meta.name':function(val){
        window.h_a.name=val;
      },
      'sumber.pemda_list.query':function(){
        this.sumber_pemda_ajax();
      },
      'display.style':function(val,old){
        console.log('display change ',val,' from ',old);
        this.change_display(val,old);
      }

    },
    computed:{
      com_datas:function(){
        return JSON.stringify(this.meta.datas);
      },
      com_scope:function(){
        return JSON.stringify(this.meta.pemda_list.map(function(elm){return elm.kodepemda}));
      },
      com_display:function(){
        var display={
          style:this.display.style,
          shcema:this.display.schema
        };
        return JSON.stringify(display);
      }
    },
    methods:{
      submit:function(){
        this.form_sub=true;
        setTimeout(() => {
          $('#form-sub').submit(); 
        }, 1000);
        
      },
      hapus_value:function(index_jenjang,index_schema,index_value){
        this.display.schema[index_jenjang].schema[index_schema].value.splice(index_value,1);
      },
      hapus_op:function(index_jenjang,index_schema,context,index_value,index_op){
        switch(context){
          case 'value':
            this.display.schema[index_jenjang].schema[index_schema].value[index_value].list.splice(index_op,1);
          break;
          case 'where':
            this.display.schema[index_jenjang].schema[index_schema].where.splice(index_op,1);
          break;
          case 'group data':
            this.display.schema[index_jenjang].schema[index_schema].group.splice(index_op,1);
          break;
        }
      },
      tambah_value:function(index_jenjang,index_schema){
        var th_value={
          name:'',
          list:[]
        }
        this.display.schema[index_jenjang].schema[index_schema].value.push(th_value);
      },
      show_source_data:function(index_jenjang,index_schema,context,index_value=null){
       
       this.display.form_data.index_value=index_value;
       this.display.form_data.context=context;
       this.display.form_data.nama_jenjang=this.display.schema[index_jenjang].name;
       this.display.form_data.nama_schema=this.display.schema[index_jenjang].schema[index_schema].name;
       $('#modal-add-data').modal();
       if(this.display.form_data.table!=null){
         this.display.form_data.table.clear().draw();
         this.display.form_data.table.destroy();
         this.display.form_data.table=null;
       }
       if(this.display.form_data.table==null){
       
         var def=window.app_{{$SpageId}}.display.data;
         for(var i in def){
           def[i].index_jenjang=index_jenjang;
           def[i].index_schema=index_schema;
           def[i].context=context;
           def[i].index_value=index_value;
         }
         

         this.display.form_data.table=$('#table-data').DataTable({
             data:def,
             pageLength:5,
             lengthChange:false,
             columns:[
               {
                data:'kode'
               },
               {
                 data:'name',
                 type:'html',
                 render:function(s,a,item){
                   return '<b>'+item.name+'</b>'+' / '+item.id_list+'';
                 }
               },
               {
                data:'satuan'
               },
               {
                
                 data:'tahun_data',
                 type:'html',
                 render:function(s,a,item){
                   return item.tahun_data>=0?'+'+item.tahun_data:item.tahun_data;
                 }
               },
               {
                
                data:'tw_data',
                type:'html',
                render:function(s,a,item){
                  return 'TW'+item.tw_data;
                }
               },
                
               {
                 sortable:false,
                 type:'html',
                 render:function(s,a,item,y){
                   return '<button class="btn btn-primary btn-sm" onclick="app_{{$SpageId}}.add_data('+item.index_jenjang+','+item.index_schema+',\''+item.context+'\','+index_value+','+y.row+')"><i class="fa fa-plus"></i></button>';
                 }
               }
             ]
           });
         }
       

     },
      show_source_operator:function(index_jenjang,index_schema,context,index_value=null){
       
        this.display.form_op.index_value=index_value;
        this.display.form_op.context=context;
        this.display.form_op.nama_jenjang=this.display.schema[index_jenjang].name;
        this.display.form_op.nama_schema=this.display.schema[index_jenjang].schema[index_schema].name;
        $('#modal-add-op').modal();
        if(this.display.form_op.table!=null){
          this.display.form_op.table.clear().draw();
          this.display.form_op.table.destroy();
          this.display.form_op.table=null;
        }
        if(this.display.form_op.table==null){
       
          var def=window.app_{{$SpageId}}.display.operator;
          for(var i in def){
            def[i].index_jenjang=index_jenjang;
            def[i].index_schema=index_schema;
            def[i].context=context;
            def[i].index_value=index_value;
          }
          

          this.display.form_op.table=$('#table-op').DataTable({
              data:def,
              pageLength:5,
              lengthChange:false,
              columns:[
                {
                  data:'name',
                  type:'html',
                  render:function(s,a,item){
                    return '<b>'+item.name+'</b>'+' / '+item.id_list+'';
                  }
                },
                {
                  sortable:false,
                  type:'html',
                  render:function(s,a,item,y){
                    return '<button class="btn btn-primary btn-sm" onclick="app_{{$SpageId}}.add_operator('+item.index_jenjang+','+item.index_schema+',\''+item.context+'\','+index_value+','+y.row+')"><i class="fa fa-plus"></i></button>';
                  }
                }
              ]
            });
          }
        

      },
      add_operator:function(index_jenjang,index_schema,context,index_value=null,index){
        var op=this.display.operator[index];
          op.context='operator';
          switch(context){
            case 'where':
              this.display.schema[index_jenjang].schema[index_schema].where.push(JSON.parse(JSON.stringify(op)));
            break;
            case 'group data':
              this.display.schema[index_jenjang].schema[index_schema].group.push(JSON.parse(JSON.stringify(op)));
            break;
            case 'value':
              this.display.schema[index_jenjang].schema[index_schema].value[index_value].list.push(JSON.parse(JSON.stringify(op)));
            break;
          }
          
        $('#modal-add-op').modal('hide');
      },
      add_data:function(index_jenjang,index_schema,context,index_value=null,index){
        var op=this.display.data[index];
          op.context='data';
          switch(context){
            case 'where':
              this.display.schema[index_jenjang].schema[index_schema].where.push(JSON.parse(JSON.stringify(op)));
            break;
            case 'group data':
              this.display.schema[index_jenjang].schema[index_schema].group.push(JSON.parse(JSON.stringify(op)));
            break;
            case 'value':
              this.display.schema[index_jenjang].schema[index_schema].value[index_value].list.push(JSON.parse(JSON.stringify(op)));
            break;
          }
          
        $('#modal-add-data').modal('hide');
      },
      change_display:function(val,old){
        if(old==0){
          this.display.schema_all=this.display.schema;
        }else{
          this.display.schema_jenjang=this.display.schema;
        }

        if(val==0){
          this.display.schema=this.display.schema_all;
        }else{
          this.display.schema=this.display.schema_jenjang;
        }

      },
      init_diaplay:function(){
        if(this.display.style==0){
          this.display.schema=this.display.schema_all;
        }else{
          this.display.schema_all=this.display.schema_jenjang
        }

        for(var i in this.meta.datas){
          this.display.data.push(JSON.parse(JSON.stringify(this.meta.datas[i])));
            
        }

        
      },
     
    
      tahun_list:function(){
        var tahun_list=[];
          for(var i=(new Date()).getFullYear()+5;i>=(new Date()).getFullYear()-5;i--){
            tahun_list.push({val:i-(new Date()).getFullYear(),tahun:i});
          }
          return tahun_list;
      },
      tw_list:function(){
        var step_list=[];
          for(var i=4;i>0;i--){
            step_list.push({val:i,tag:'TW'+i});
          }
          return step_list;

      },
      tf_form_add_data:function(i){
        if(this.sumber.datas.data[i]!=undefined){

          this.sumber.datas.form_add=JSON.parse(JSON.stringify(this.sumber.datas.data[i]));
          this.sumber.datas.form_add.tahun_data=0;
          this.sumber.datas.form_add.tw_data=4;
          this.sumber.datas.form_add.id_recorded=null;      }
        $('#modal-add-datalist').modal();

      },
      sumber_datas_ajax:function(){
       
        window.req_ajax.get('/schema-data/source-data?'+qs.stringify({q: app_{{$SpageId}}.sumber.datas.query})).then(function(res){
          app_{{$SpageId}}.sumber.datas.data=res.data.data;
          app_{{$SpageId}}.sumber.datas.token=null;
        });
      },
      sumber_pemda_ajax:function(){
        window.req_ajax.get('/public/source-pemda/?'+qs.stringify({q: app_{{$SpageId}}.sumber.pemda_list.query})).then(function(res){
          app_{{$SpageId}}.sumber.pemda_list.data=res.data.data;
          app_{{$SpageId}}.sumber.pemda_list.token=null;
        });
      },
      tf_pemda_form_add:function(i){
        if(this.sumber.pemda_list.data[i]!=undefined){
              this.sumber.pemda_list.form_add=JSON.parse(JSON.stringify(this.sumber.pemda_list.data[i]));
              this.sumber.pemda_list.form_add.tahun_data={{$Stahun}};
        }
        $('#modal-add-pemdalist').modal();
      },
      tf_pemda_form_pemda_list:function(){
        this.sumber.pemda_list.form_add.id_list=this.sumber.pemda_list.form_add.kodepemda+'_'+this.sumber.pemda_list.form_add.tahun_data;
        var av=false;
        for(var i in this.meta.pemda_list){
            if(this.meta.pemda_list[i].id_list==this.sumber.pemda_list.form_add.id_list){
              av=true;
            }
        }

        if(!av){
          this.meta.pemda_list.push(JSON.parse(JSON.stringify(this.sumber.pemda_list.form_add)));
        }
            
          $('#modal-add-pemdalist').modal('hide');
      },
      tf_form_add_t_datas:function(){
        this.sumber.datas.form_add.id_list=this.sumber.datas.form_add.id+'_'+this.sumber.datas.form_add.tahun_data+'_'+this.sumber.datas.form_add.tw_data;
        var av=false;
        for(var i in this.meta.datas){
            if(this.meta.datas[i].id_list==this.sumber.datas.form_add.id_list){
              av=true;
            }
        }

        if(!av){
          this.meta.datas.push(JSON.parse(JSON.stringify(this.sumber.datas.form_add)));
          this.display.data.push(JSON.parse(JSON.stringify(this.sumber.datas.form_add)));
        }
        $('#modal-add-datalist').modal('hide');
      }
    }
  });

  app_{{$SpageId}}.init_diaplay();
</script>
@stop