@extends('adminlte::page')

@section('content_header')
    <a href="{{route('dash.si.schema.dataset.tambah_dataset',['tahun'=>$Stahun])}}" class="btn btn-success">Tambah Dataset</a>
@stop
@section('content')
  <div class="card" id="app_{{$SpageId}}">
    <div class="card-body">
      <table id="table-validator-{{$SpageId}}" class="table table-bordered">
        <thead class="bg-primary">
          <tr>
            <th>Nama Dataset</th>
            <th>Jenis Dataset</th>
            <th>Interval Pengambilan</th>
            <th>Distribusi Data</th>
            <th>Penangjung Jawab</th>
            <th>Status</th>
            <th>Tanggal Publish</th>
            <th>Tujuan</th>
            <th>Jumlah Data</th>
            <th>Jumlah Daerah Scope</th>
            <th>Aksi</th>
            

          </tr>
        </thead>
      </table>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="modal-delete-dataset">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Hapus Dataset</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>Apkah anda yakin ingin menhapus dataset "@{{form_delete.name}}"</p>
          </div>
          <div class="modal-footer">
            <form v-bind:action="form_delete.link" method="post">
              @method('DELETE')
              @csrf
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger">Hapus</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop

@section('js')
<script>
  var app_table;
   var A{{$SpageId}}=new Vue({
     el:'#app_{{$SpageId}}',
     
     data:{
       table_validator:null,
       dataset:<?=json_encode($dataset)?>,
       form_delete:{
         name:'',
         link:''
       }
     },
   
     watch:{
      dataset:function(){
        this.redraw_table();
      }
     },
     methods:{
      show_delete_form:function(link,name){
        this.form_delete.link=link;
        this.form_delete.name=name;
        $('#modal-delete-dataset').modal();

      },
       redraw_table:function(){
        
        this.table_validator.clear().draw(); 
        this.table_validator.rows.add(this.dataset).draw();
       },
       init:function(){
          this.table_validator=$('#table-validator-{{$SpageId}}').DataTable( {
            paging: false,
            autoWidth: false,
            process:1,
            data: window.A{{$SpageId}}.dataset,
            createdRow: function (row, data, index) {
              if(data.status==1){
                $(row).addClass('bg-success');
              }      
                    
            },
            columns: [
                { 
                    data: 'name',
                },
                { 
                    data: 'jenis_dataset',
                    render:function(dis,a,item){
                      return item.jenis_dataset==0?'Single-Record':'Multy-Record';
                    }
                },
                { 
                    data: 'interval_pengambilan',
                    render:function(dis,a,item){
                      return item.interval_pengambilan==0?'Triwulanan':'Tahunan';
                    }
                },
                { 
                    data: 'jenis_distribusi',
                    render:function(dis,a,item){
                      return item.jenis_distribusi==0?'External':'Internal';
                    }
                },
                { 
                    data: 'penangung_jawab',
                },
                { 
                    data: 'status',
                    type:'html',
                    render:function(dis,a,item,index){
                      return item.status==0?'Draf':'<span> <div class="spinner-grow text-danger"  style="width:15px; height:15px;" role="status"><span class="sr-only">Loading...</span></div></span> Publish';
                    }
                },{
                  data:'tanggal_publish',
                  render:function(d,a,item){
                    return item.tanggal_publish;
                  }
                },
                {
                  data:'tujuan',
                  render:function(d,a,item){
                    return clean_text(item.tujuan,50);
                  }
                },
                { 
                    data: 'jumlah_data',
                    type:'html',
                    render:(display,a,item)=>{
                      return item.jumlah_data+' Data';
                    }
                },
                {
                  render:function(d,a,item){
                    return (item.jumlah_pemda_scope==0?514:item.jumlah_pemda_scope)+' Pemda Kab/Kota';
                  }
                },
                {
                  data:'',
                  name:'Aksi', 
                  sortable:false,
                  render:(display,a,item)=>{
                    return '<div class="btn-group"><a href="'+item.link_detail+'"  class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i></a><button  onclick=\'A{{$SpageId}}.show_delete_form("'+item.link_delete+'",\"'+item.name+'\")\' class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button><a  href="'+item.link_schema_up+'" class="btn btn-sm btn-warning"><i class="fa fa-compress-alt"></i></a></div>';
                  }
                  
                }
                
              
            ],
          
        });
       }
     }
   });
   A{{$SpageId}}.init();

</script>

@stop