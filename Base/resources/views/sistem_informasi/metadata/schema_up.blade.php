@extends('adminlte::page')

@section('content_header')
<style>
  .btn-xxs{
    font-size: 8px!important;
    width:20px;
    height: 20px;
    padding: 0;
  }
</style>
<h3 id="h_a"><b>DATASET {{$dataset->name}} </b></h3>
<hr>
@stop
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <p><b>Upload Data</b></p>
            <div class="small-box bg-success">
                <div class="inner">
                  <div class="form-group">
                      <label for="">File Excel</label>
                      <input type="file" class="form-control">
                  </div>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <div class="small-box-footer">
                    <button class="btn btn" >Upload <i class="fas fa-arrow-circle-right"></i></button>
                </div>
              </div>
        </div>
    </div>
</div>

@stop