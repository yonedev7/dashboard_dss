@php
   if(Auth::check()){
        $MyUser=Auth::User();
        $f=[
            'regional'=>true,
            'sortlist'=>true,
            'tipe_bantuan'=>true,
        ];  

        if($MyUser->roleCheck('TACT REGIONAL')){
            $f['regional']=false;
            $f['sortlist']=false;
        }else{

        }

   }else{
        $f=[
            'regional'=>true,
            'sortlist'=>true,
            'tipe_bantuan'=>true,
        ]; 

   }
@endphp
<form action="{{url()->current()}}" method="get" id="form-filter-data">

    <div class="row">
       @if($f['regional'])
        <div class="col-3">
            <div class="form-group">
                <label for="">Regional </label>
                <input type="hidden" name="meta" value="{{isset($_GET['meta'])?$_GET['meta']:null}}">

                <select  v-model="filter.filter_regional"  name="filter_regional" class="form-control" id="">
                    <option value=""></option>
                    <option value="I">I</option>
                    <option value="II">II</option>
                    <option value="III">III</option>
                    <option value="IV">IV</option>
                    <option value="V">V</option>

                </select>
            </div>
        </div>

       @endif
       @if($f['sortlist'])
        
        <div class="col-3">
            <div class="form-group">
                <label for="">Tahun Proyek</label>
                <select  v-model="filter.filter_sortlist"  name="filter_sortlist" class="form-control" id="">
                    <option value=""></option>
                    <option value="{{$StahunRoute+2}}">{{$StahunRoute+2}}</option>

                    <option value="{{$StahunRoute+1}}">{{$StahunRoute+1}}</option>
                    <option value="{{$StahunRoute}}">{{$StahunRoute}}</option>
                    <option value="{{$StahunRoute-1}}">{{$StahunRoute-1}}</option>
                    <option value="{{$StahunRoute-2}}">{{$StahunRoute-2}}</option>


                    
                </select>
            </div>
        </div>
        @endif
       @if($f['tipe_bantuan'])

        <div class="col-3">
            <div class="form-group">
                <label for="">Tipe Bantuan</label>
                <select  v-model="filter.filter_tipe_bantuan" name="filter_tipe_bantuan" class="form-control" id="">
                    <option value=""></option>
                    <option value="STIMULAN">STIMULAN</option>
                    <option value="PENDAMPING">PENDAMPING</option>
                    <option value="BASIS KINERJA">BASIS KINERJA</option>
                </select>
            </div>
        </div>
        @endif
        <div class="col-3">
            <div class="form-group">
                <label for="">Jenis Pemda</label>
                <select  v-model="filter.filter_jenis_pemda" name="filter_jenis_pemda" class="form-control" id="">
                    <option value="3">Provinsi & Kab/Kot</option>
                    <option value="2">Kab/Kot</option>
                    <option value="1">Provinsi</option>
                </select>
            </div>
        </div>
    </div>
</form>
<hr>

@push('js_push')
<script>
    var filter_data=new Vue({
        el:"#form-filter-data",
        data:{
            filter:{
                filter_sortlist:"{{$req['filter_sortlist']??''}}",
                filter_tipe_bantuan:"{{$req['filter_tipe_bantuan']??''}}",
                filter_regional:"{{$req['filter_regional']??''}}",
                filter_jenis_pemda:{{$req['filter_jenis_pemda']??3}},

            }
        },
        watch:{
        filter:{
            deep:true,
            handler:function(){
                $('#form-filter-data').submit();
            }
        }
        },
    })
</script>
@endpush