@extends('adminlte::page')

@section('content_header')
    <h3 class=""><b class="text-uppercase">Form Pengisian Dataset</b></h3>

@stop

@section('content')
<div id="app">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card bg-dark">
                    <div class="card-body">
                        <h3><b>Jumlah Dataset</b></h3>
                            @{{dataset.length}} Dataset
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table id="table-dataset" class="table table-bordered" style="max-width:100%;">
                   
                    <thead class="thead-dark">
                        <tr>
                            <th rowspan="2">Aksi</th>
                            <th rowspan="2">Nama Dataset</th>
                            <th rowspan="2">Interval Pengambilan</th>
                            <th rowspan="2">Penangung Jawab</th>
                            <th colspan="12">Status Data</th>
                            
    
                        </tr>
                        <tr>
                            <th>TW 1 - Input</th>
                            <th>TW 1 - Verifikasi</th>
                            <th>TW 1 Updated</th>
    
    
                            <th>TW 2 - Input</th>
                            <th>TW 2 - Verifikasi</th>
                            <th>TW 2 Updated</th>
    
    
                            <th>TW 3 - Input</th>
                            <th>TW 3 - Verifikasi</th>
                            <th>TW 3 Updated</th>
    
                            
    
                            <th>TW 4 / FINAL - Input</th>
                            <th>TW 4 / FINAL - Verifikasi</th>
                            <th>TW 4 Updated</th>
    
    
                           
    
                        </tr>
                       
                       
                    </thead>
                </table>
            </div>
        </div>
    
       
    </div>
    <div class="modal fade" id="modal-dataset-handle" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Download / Update Data</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <h5><b>Dataset: @{{data_handle.name}}</b></h5>
                <div class="form-group">
                    <label for="">Pilih TW</label>
                    <select name="" v-model="tw" id="" class="form-control">
                        <option value="1">TW I</option>
                        <option value="2">TW II</option>
                        <option value="3">TW III</option>
                        <option value="4">TW IV</option>
    
                    </select>
                </div>
              <div class="btn-group mb-2 mt-2">
               
                <a v-bind:href="data_handle.link_download+'?tw='+tw" class="btn btn-success" download="">Download Excel</a>
              </div>
              <hr>
              <form v-bind:action="data_handle.link_upload" method="post" enctype='multipart/form-data'>
                  @csrf
                <div class="from-group">
                    <label for="">Upload Excel</label>
                    <input type="file" name="file" class="form-control" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" required>
                    <button type="submit" class="mb-1 mt-2 btn btn-primary">Upload</button>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
</div>


@stop

@section('js')
<script>
   var app=new Vue({
       el:'#app',
       data:{
           dataset:<?=json_encode($dataset)?>,
           table:null,
           tw:4,
           data_handle:{
               name:'',
               tujuan:'',
               link_upload:'',
               link_download:''
           }

       },
       methods:{
           handle_dataset:function(id){
            key=0;
            while(this.dataset[key].id!=id){
                key++;
            }

            if(this.dataset[key]!==undefined){
                this.data_handle=this.dataset[key];
                $('#modal-dataset-handle').modal();
            }
           },
           table_init:function(){
            app.table=$('#table-dataset').DataTable({
                data:app.dataset,
                width:'100%',
                columns:[
                        {
                        type:'html',
                        render:function(s,a,item){
                            console.log(s,a,item);
                            console.log('------');
                            return '<div class="btn btn-group"><a class="btn btn-primary btn-sm" href="'+item.meta.link_detail+'"><i class="fa fa-arrow-right"></i></a><a class="btn btn-warning btn-sm" href="javascript:void(0)" onclick="app.handle_dataset('+item.id+')"><i class="fa fa-upload" aria-hidden="true"></i></a></div>';
                        }
                    },
                    {data:'name'},

                        {data:'interval_pengambilan',
                        render:function(s,a,item){
                            return item.interval_pengambilan==0?'TRIWULANAN':'TAHUNAN';
                        }},

                    {data:'penangung_jawab'},
                    {data:'meta.count_data_1'},
                    {data:'meta.count_finish_1'},
                    {data:'meta.last_update_data_1'},


                    {data:'meta.count_data_2'},
                    {data:'meta.count_finish_2'},
                    {data:'meta.last_update_data_2'},


                    {data:'meta.count_data_3'},
                    {data:'meta.count_finish_3'},
                    {data:'meta.last_update_data_3'},


                    {data:'meta.count_data_4'},

                    {data:'meta.count_finish_4'},
                    {data:'meta.last_update_data_4'},

                ]
                });
           }
       }
   });
   app.table_init();
</script>

@stop