@extends('adminlte::page')

@section('content_header')
    <h3 class="">Dataset : {{$dataset->name}}</h3>

@stop

@section('content')
<div class="container-fluid">
    <div class="row">
        @if($dataset->interval_pengambilan==0)
        <div class="col-md-3">
            <div class="card bg-dark">
                <div class="card-body">
                    <h3><b>Terdata TW 1</b></h3>
                    <p>{{number_format($rekap['count_data_1'])}} PEMDA</p>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card bg-secondary">
                <div class="card-body">
                    <h3><b>Terverifikasi TW 1</b></h3>
                    <p>{{number_format($rekap['count_finish_1'])}} PEMDA</p>

                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card bg-dark">
                <div class="card-body">
                    <h3><b>Terdata TW 2</b></h3>
                    <p>{{number_format($rekap['count_data_2'])}} PEMDA</p>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card bg-secondary">
                <div class="card-body">
                    <h3><b>Terverifikasi TW 2</b></h3>
                    <p>{{number_format($rekap['count_finish_2'])}} PEMDA</p>

                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card bg-dark">
                <div class="card-body">
                    <h3><b>Terdata TW 3</b></h3>
                    <p>{{number_format($rekap['count_data_3'])}} PEMDA</p>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card bg-secondary">
                <div class="card-body">
                    <h3><b>Terverifikasi TW 3</b></h3>
                    <p>{{number_format($rekap['count_finish_3'])}} PEMDA</p>

                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="card bg-dark">
                <div class="card-body">
                    <h3><b>Terdata TW 4</b></h3>
                    <p>{{number_format($rekap['count_data_4'])}} PEMDA</p>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card bg-secondary">
                <div class="card-body">
                    <h3><b>Terverifikasi TW 4</b></h3>
                    <p>{{number_format($rekap['count_finish_4'])}} PEMDA</p>

                </div>
            </div>
        </div>


        @else
        <div class="col-md-6">
            <div class="card bg-dark">
                <div class="card-body">
                    <h3><b>Terdata</b></h3>
                    <p>{{number_format($rekap['count_data_4'])}} PEMDA</p>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card bg-secondary">
                <div class="card-body">
                    <h3><b>Terverifikasi</b></h3>
                    <p>{{number_format($rekap['count_finish_4'])}} PEMDA</p>

                </div>
            </div>
        </div>

        @endif
        
    </div>
</div>
<div class="card">
    <div class="card-body">
        <div class="table-responsive">
            <table id="table-dataset" class="table table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th {{$dataset->interval_pengambilan==0?'rowspan=2':''}} >Aksi</th>
                        <th {{$dataset->interval_pengambilan==0?'rowspan=2':''}}>Kode Pemda</th>
                        <th {{$dataset->interval_pengambilan==0?'rowspan=2':''}}>Nama Pemda</th>
                        <th {{$dataset->interval_pengambilan==0?'colspan=4':''}}>Status Data</th>

                    </tr>
                    @if($dataset->interval_pengambilan==0)

                    <tr>
                        <th>TW 1</th>
                        <th>TW 2</th>
                        <th>TW 3</th>
                        <th>TW 4 / FINAL</th>
                       
                    </tr>
                    @endif
                   
                   
                </thead>
            </table>
        </div>
    </div>
</div>

@stop


@section('js')
<script>
    $('#table-dataset').DataTable({
       columns:[
            {
               type:'html',
               render:function(s,a,item){
                   return '';
               }
           },
             {data:'kodepemda',
             render:function(a,s,item){
                 return item.kodepemda.replace(/^0/,'');
             }},
             {data:'nama_pemda'},
             @if($dataset->interval_pengambilan==0)
             {
                 render:function(a,s,item){
                     return item.count_finish_1?'Terverifikasi':item.count_data_1?'Terinput':'Belum Terinput';
                 }
             },
             {
                 render:function(a,s,item){
                     return item.count_finish_2?'Terverifikasi':item.count_data_2?'Terinput':'Belum Terinput';
                 }
             },
             {
                 render:function(a,s,item){
                     return item.count_finish_3?'Terverifikasi':item.count_data_3?'Terinput':'Belum Terinput';
                 }
             },
             @endif
             {
                 render:function(a,s,item){
                     return item.count_finish_4?'Terverifikasi':item.count_data_4?'Terinput':'Belum Terinput';
                 }
             }
       ],
       autoWidth:false,
       data:<?=json_encode($pemdas)?>,

    })
</script>

@stop