@extends('adminlte::page')

@section('content_header')
@stop
@section('content')
<div class="container" id="rekap-app">
    <div class="card">
        <div class="card-body">
            <p class="mb-0">Diperbarui</p>
            <p><span class="badge badge-warning">{{$date[$index]}}</span></p>
            <h3>Program Kegiatan Air Minum (RKPD) - 2021</h3>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <div class="card bg-dark">
                        <div class="card-body">
                            <h3><b>Data Terinput</b></h3>
                            <p>205 Pemda</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card bg-secondary">
                        <div class="card-body">
                            <h3><b>Data Terverifikasi</b></h3>
                            <p>0 Pemda</p>

        
                        </div>
                    </div>
                </div>
            </div>
            <div class="">
                <table class="table table-bordered">
                    <tbody>
                    <tr>
                    <td width="87">Kodepemda</td>
                    <td width="87">Nama Pemda</td>
                    <td width="87">Status Data</td>
                    <td width="163">Pagu Indikatif</td>
                    <td width="87">Jumlah Kegiatan</td>
                    <td width="87">Jumlah Sub Kegiatan</td>
                    </tr>
                    <tr>
                    <td>1101</td>
                    <td>KABUPATEN ACEH SELATAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>1900000000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>1102</td>
                    <td>KABUPATEN ACEH TENGGARA</td>
                    <td>Belum Terverifikasi</td>
                    <td>49750000000</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>1104</td>
                    <td>KABUPATEN ACEH TENGAH</td>
                    <td>Belum Terverifikasi</td>
                    <td>5950004206</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>1105</td>
                    <td>KABUPATEN ACEH BARAT</td>
                    <td>Belum Terverifikasi</td>
                    <td>4678000000</td>
                    <td>1</td>
                    <td>22</td>
                    </tr>
                    <tr>
                    <td>1106</td>
                    <td>KABUPATEN ACEH BESAR</td>
                    <td>Belum Terverifikasi</td>
                    <td>6468000000</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>1108</td>
                    <td>KABUPATEN ACEH UTARA</td>
                    <td>Belum Terverifikasi</td>
                    <td>1018077000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>1116</td>
                    <td>KABUPATEN ACEH TAMIANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>14685224981</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>1171</td>
                    <td>KOTA BANDA ACEH</td>
                    <td>Belum Terverifikasi</td>
                    <td>24448126000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>1172</td>
                    <td>KOTA SABANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>894118423</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>1174</td>
                    <td>KOTA LANGSA</td>
                    <td>Belum Terverifikasi</td>
                    <td>3688520000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>12</td>
                    <td>PROVINSI SUMATERA UTARA</td>
                    <td>Belum Terverifikasi</td>
                    <td>16692761484</td>
                    <td>1</td>
                    <td>12</td>
                    </tr>
                    <tr>
                    <td>1202</td>
                    <td>KABUPATEN TAPANULI UTARA</td>
                    <td>Belum Terverifikasi</td>
                    <td>4747260936</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>1204</td>
                    <td>KABUPATEN NIAS</td>
                    <td>Belum Terverifikasi</td>
                    <td>2200000000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>1205</td>
                    <td>KABUPATEN LANGKAT</td>
                    <td>Belum Terverifikasi</td>
                    <td>6970000000</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>1206</td>
                    <td>KABUPATEN KARO</td>
                    <td>Belum Terverifikasi</td>
                    <td>1100000000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>1207</td>
                    <td>KABUPATEN DELI SERDANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>32700480000</td>
                    <td>1</td>
                    <td>8</td>
                    </tr>
                    <tr>
                    <td>1208</td>
                    <td>KABUPATEN SIMALUNGUN</td>
                    <td>Belum Terverifikasi</td>
                    <td>27162700000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>1209</td>
                    <td>KABUPATEN ASAHAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>21942705344</td>
                    <td>1</td>
                    <td>8</td>
                    </tr>
                    <tr>
                    <td>1210</td>
                    <td>KABUPATEN LABUHANBATU</td>
                    <td>Belum Terverifikasi</td>
                    <td>1271900000</td>
                    <td>1</td>
                    <td>8</td>
                    </tr>
                    <tr>
                    <td>1211</td>
                    <td>KABUPATEN DAIRI</td>
                    <td>Belum Terverifikasi</td>
                    <td>18950000000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>1272</td>
                    <td>KOTA PEMATANGSIANTAR</td>
                    <td>Belum Terverifikasi</td>
                    <td>10400000000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>1273</td>
                    <td>KOTA SIBOLGA</td>
                    <td>Belum Terverifikasi</td>
                    <td>378074550</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>1274</td>
                    <td>KOTA TANJUNG BALAI</td>
                    <td>Belum Terverifikasi</td>
                    <td>2200000000</td>
                    <td>1</td>
                    <td>18</td>
                    </tr>
                    <tr>
                    <td>1275</td>
                    <td>KOTA BINJAI</td>
                    <td>Belum Terverifikasi</td>
                    <td>5000000000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>1276</td>
                    <td>KOTA TEBING TINGGI</td>
                    <td>Belum Terverifikasi</td>
                    <td>1000000000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>1277</td>
                    <td>KOTA PADANGSIDIMPUAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>485000000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>1302</td>
                    <td>KABUPATEN SOLOK</td>
                    <td>Belum Terverifikasi</td>
                    <td>10505000000</td>
                    <td>1</td>
                    <td>10</td>
                    </tr>
                    <tr>
                    <td>1303</td>
                    <td>KABUPATEN SIJUNJUNG</td>
                    <td>Belum Terverifikasi</td>
                    <td>2510000000</td>
                    <td>1</td>
                    <td>14</td>
                    </tr>
                    <tr>
                    <td>1304</td>
                    <td>KABUPATEN TANAH DATAR</td>
                    <td>Belum Terverifikasi</td>
                    <td>9186000000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>1306</td>
                    <td>KABUPATEN AGAM</td>
                    <td>Belum Terverifikasi</td>
                    <td>11629830000</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>1372</td>
                    <td>KOTA SOLOK</td>
                    <td>Belum Terverifikasi</td>
                    <td>1473085000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>1373</td>
                    <td>KOTA SAWAHLUNTO</td>
                    <td>Belum Terverifikasi</td>
                    <td>4376757500</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>1375</td>
                    <td>KOTA BUKITTINGGI</td>
                    <td>Belum Terverifikasi</td>
                    <td>170580000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>1376</td>
                    <td>KOTA PAYAKUMBUH</td>
                    <td>Belum Terverifikasi</td>
                    <td>101000000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>1401</td>
                    <td>KABUPATEN KAMPAR</td>
                    <td>Belum Terverifikasi</td>
                    <td>3900000000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>1402</td>
                    <td>KABUPATEN INDRAGIRI HULU</td>
                    <td>Belum Terverifikasi</td>
                    <td>19067873200</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>1403</td>
                    <td>KABUPATEN BENGKALIS</td>
                    <td>Belum Terverifikasi</td>
                    <td>57535850000</td>
                    <td>1</td>
                    <td>10</td>
                    </tr>
                    <tr>
                    <td>1404</td>
                    <td>KABUPATEN INDRAGIRI HILIR</td>
                    <td>Belum Terverifikasi</td>
                    <td>13060364000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>1471</td>
                    <td>KOTA PEKANBARU</td>
                    <td>Belum Terverifikasi</td>
                    <td>28896018716</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>1472</td>
                    <td>KOTA DUMAI</td>
                    <td>Belum Terverifikasi</td>
                    <td>19800000000</td>
                    <td>1</td>
                    <td>8</td>
                    </tr>
                    <tr>
                    <td>1501</td>
                    <td>KABUPATEN KERINCI</td>
                    <td>Belum Terverifikasi</td>
                    <td>5300000000</td>
                    <td>1</td>
                    <td>8</td>
                    </tr>
                    <tr>
                    <td>1502</td>
                    <td>KABUPATEN MERANGIN</td>
                    <td>Belum Terverifikasi</td>
                    <td>11500000000</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>1504</td>
                    <td>KABUPATEN BATANGHARI</td>
                    <td>Belum Terverifikasi</td>
                    <td>9307426000</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>1508</td>
                    <td>KABUPATEN BUNGO</td>
                    <td>Belum Terverifikasi</td>
                    <td>2558735000</td>
                    <td>1</td>
                    <td>14</td>
                    </tr>
                    <tr>
                    <td>1571</td>
                    <td>KOTA JAMBI</td>
                    <td>Belum Terverifikasi</td>
                    <td>103226710000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>1601</td>
                    <td>KABUPATEN OGAN KOMERING ULU</td>
                    <td>Belum Terverifikasi</td>
                    <td>5000000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>1603</td>
                    <td>KABUPATEN MUARA ENIM</td>
                    <td>Belum Terverifikasi</td>
                    <td>6109345000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>1604</td>
                    <td>KABUPATEN LAHAT</td>
                    <td>Belum Terverifikasi</td>
                    <td>77175000000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>1606</td>
                    <td>KABUPATEN MUSI BANYUASIN</td>
                    <td>Belum Terverifikasi</td>
                    <td>3020000000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>1610</td>
                    <td>KABUPATEN OGAN ILIR</td>
                    <td>Belum Terverifikasi</td>
                    <td>4750000000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>1671</td>
                    <td>KOTA PALEMBANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>35000000000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>1673</td>
                    <td>KOTA LUBUK LINGGAU</td>
                    <td>Belum Terverifikasi</td>
                    <td>1293750000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>1701</td>
                    <td>KABUPATEN BENGKULU SELATAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>46880000000</td>
                    <td>1</td>
                    <td>16</td>
                    </tr>
                    <tr>
                    <td>1702</td>
                    <td>KABUPATEN REJANG LEBONG</td>
                    <td>Belum Terverifikasi</td>
                    <td>13480120000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>1709</td>
                    <td>KABUPATEN BENGKULU TENGAH</td>
                    <td>Belum Terverifikasi</td>
                    <td>4383903183</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>1771</td>
                    <td>KOTA BENGKULU</td>
                    <td>Belum Terverifikasi</td>
                    <td>4104340000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>1801</td>
                    <td>KABUPATEN LAMPUNG SELATAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>6786727170</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>1802</td>
                    <td>KABUPATEN LAMPUNG TENGAH</td>
                    <td>Belum Terverifikasi</td>
                    <td>22084980133</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>1803</td>
                    <td>KABUPATEN LAMPUNG UTARA</td>
                    <td>Belum Terverifikasi</td>
                    <td>674336744</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>1806</td>
                    <td>KABUPATEN TANGGAMUS</td>
                    <td>Belum Terverifikasi</td>
                    <td>16078144600</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>1871</td>
                    <td>KOTA BANDAR LAMPUNG</td>
                    <td>Belum Terverifikasi</td>
                    <td>0</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>1901</td>
                    <td>KABUPATEN BANGKA</td>
                    <td>Belum Terverifikasi</td>
                    <td>1800000000</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>1902</td>
                    <td>KABUPATEN BELITUNG</td>
                    <td>Belum Terverifikasi</td>
                    <td>10996608250</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>1971</td>
                    <td>KOTA PANGKAL PINANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>5617769500</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>2172</td>
                    <td>KOTA TANJUNG PINANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>29558195200</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>3201</td>
                    <td>KABUPATEN BOGOR</td>
                    <td>Belum Terverifikasi</td>
                    <td>20501000000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>3202</td>
                    <td>KABUPATEN SUKABUMI</td>
                    <td>Belum Terverifikasi</td>
                    <td>162630369000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>3204</td>
                    <td>KABUPATEN BANDUNG</td>
                    <td>Belum Terverifikasi</td>
                    <td>11272849650</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>3207</td>
                    <td>KABUPATEN CIAMIS</td>
                    <td>Belum Terverifikasi</td>
                    <td>27555000000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>3208</td>
                    <td>KABUPATEN KUNINGAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>154257069492</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>3209</td>
                    <td>KABUPATEN CIREBON</td>
                    <td>Belum Terverifikasi</td>
                    <td>8439046003</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>3210</td>
                    <td>KABUPATEN MAJALENGKA</td>
                    <td>Belum Terverifikasi</td>
                    <td>40200917000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>3211</td>
                    <td>KABUPATEN SUMEDANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>32971994000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>3212</td>
                    <td>KABUPATEN INDRAMAYU</td>
                    <td>Belum Terverifikasi</td>
                    <td>649999932</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>3213</td>
                    <td>KABUPATEN SUBANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>10558000000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>3217</td>
                    <td>KABUPATEN BANDUNG BARAT</td>
                    <td>Belum Terverifikasi</td>
                    <td>6506452945</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>3273</td>
                    <td>KOTA BANDUNG</td>
                    <td>Belum Terverifikasi</td>
                    <td>18736768375</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>3274</td>
                    <td>KOTA CIREBON</td>
                    <td>Belum Terverifikasi</td>
                    <td>1250493500</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>3276</td>
                    <td>KOTA DEPOK</td>
                    <td>Belum Terverifikasi</td>
                    <td>2663160000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>3277</td>
                    <td>KOTA CIMAHI</td>
                    <td>Belum Terverifikasi</td>
                    <td>6666976300</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>3279</td>
                    <td>KOTA BANJAR</td>
                    <td>Belum Terverifikasi</td>
                    <td>4100000000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>3301</td>
                    <td>KABUPATEN CILACAP</td>
                    <td>Belum Terverifikasi</td>
                    <td>7186625000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>3302</td>
                    <td>KABUPATEN BANYUMAS</td>
                    <td>Belum Terverifikasi</td>
                    <td>9180000000</td>
                    <td>1</td>
                    <td>10</td>
                    </tr>
                    <tr>
                    <td>3305</td>
                    <td>KABUPATEN KEBUMEN</td>
                    <td>Belum Terverifikasi</td>
                    <td>13503685000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>3306</td>
                    <td>KABUPATEN PURWOREJO</td>
                    <td>Belum Terverifikasi</td>
                    <td>17178000000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>3308</td>
                    <td>KABUPATEN MAGELANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>390510000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>3309</td>
                    <td>KABUPATEN BOYOLALI</td>
                    <td>Belum Terverifikasi</td>
                    <td>5725793000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>3310</td>
                    <td>KABUPATEN KLATEN</td>
                    <td>Belum Terverifikasi</td>
                    <td>1668237254</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>3311</td>
                    <td>KABUPATEN SUKOHARJO</td>
                    <td>Belum Terverifikasi</td>
                    <td>1290000000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>3314</td>
                    <td>KABUPATEN SRAGEN</td>
                    <td>Belum Terverifikasi</td>
                    <td>4884999500</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>3318</td>
                    <td>KABUPATEN PATI</td>
                    <td>Belum Terverifikasi</td>
                    <td>25725000000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>3322</td>
                    <td>KABUPATEN SEMARANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>27010549000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>3325</td>
                    <td>KABUPATEN BATANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>3239879739</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>3326</td>
                    <td>KABUPATEN PEKALONGAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>18374776462</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>3329</td>
                    <td>KABUPATEN BREBES</td>
                    <td>Belum Terverifikasi</td>
                    <td>0</td>
                    <td>1</td>
                    <td>12</td>
                    </tr>
                    <tr>
                    <td>3373</td>
                    <td>KOTA SALATIGA</td>
                    <td>Belum Terverifikasi</td>
                    <td>2416792000</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>3375</td>
                    <td>KOTA PEKALONGAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>2030999000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>3376</td>
                    <td>KOTA TEGAL</td>
                    <td>Belum Terverifikasi</td>
                    <td>816000000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>3402</td>
                    <td>KABUPATEN BANTUL</td>
                    <td>Belum Terverifikasi</td>
                    <td>8333089440</td>
                    <td>1</td>
                    <td>16</td>
                    </tr>
                    <tr>
                    <td>3404</td>
                    <td>KABUPATEN SLEMAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>37410744000</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>3501</td>
                    <td>KABUPATEN PACITAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>5624000000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>3502</td>
                    <td>KABUPATEN PONOROGO</td>
                    <td>Belum Terverifikasi</td>
                    <td>1275000000</td>
                    <td>1</td>
                    <td>7</td>
                    </tr>
                    <tr>
                    <td>3504</td>
                    <td>KABUPATEN TULUNGAGUNG</td>
                    <td>Belum Terverifikasi</td>
                    <td>126022000000</td>
                    <td>1</td>
                    <td>21</td>
                    </tr>
                    <tr>
                    <td>3505</td>
                    <td>KABUPATEN BLITAR</td>
                    <td>Belum Terverifikasi</td>
                    <td>12209491000</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>3506</td>
                    <td>KABUPATEN KEDIRI</td>
                    <td>Belum Terverifikasi</td>
                    <td>11581734950</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>3507</td>
                    <td>KABUPATEN MALANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>12744281000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>3508</td>
                    <td>KABUPATEN LUMAJANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>6229046400</td>
                    <td>1</td>
                    <td>18</td>
                    </tr>
                    <tr>
                    <td>3509</td>
                    <td>KABUPATEN JEMBER</td>
                    <td>Belum Terverifikasi</td>
                    <td>19390347537</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>3513</td>
                    <td>KABUPATEN PROBOLINGGO</td>
                    <td>Belum Terverifikasi</td>
                    <td>22782116000</td>
                    <td>1</td>
                    <td>7</td>
                    </tr>
                    <tr>
                    <td>3514</td>
                    <td>KABUPATEN PASURUAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>7195231000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>3515</td>
                    <td>KABUPATEN SIDOARJO</td>
                    <td>Belum Terverifikasi</td>
                    <td>180117314300</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>3521</td>
                    <td>KABUPATEN NGAWI</td>
                    <td>Belum Terverifikasi</td>
                    <td>3975000000</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>3524</td>
                    <td>KABUPATEN LAMONGAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>38777000000</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>3525</td>
                    <td>KABUPATEN GRESIK</td>
                    <td>Belum Terverifikasi</td>
                    <td>33028001880</td>
                    <td>1</td>
                    <td>7</td>
                    </tr>
                    <tr>
                    <td>3526</td>
                    <td>KABUPATEN BANGKALAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>27812006665</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>3527</td>
                    <td>KABUPATEN SAMPANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>23758791255</td>
                    <td>1</td>
                    <td>11</td>
                    </tr>
                    <tr>
                    <td>3573</td>
                    <td>KOTA MALANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>6003830000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>3574</td>
                    <td>KOTA PROBOLINGGO</td>
                    <td>Belum Terverifikasi</td>
                    <td>10000000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>3575</td>
                    <td>KOTA PASURUAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>3521161000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>3578</td>
                    <td>KOTA SURABAYA</td>
                    <td>Belum Terverifikasi</td>
                    <td>293924659</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>3579</td>
                    <td>KOTA BATU</td>
                    <td>Belum Terverifikasi</td>
                    <td>11527517167</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>3601</td>
                    <td>KABUPATEN PANDEGLANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>1247900000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>3602</td>
                    <td>KABUPATEN LEBAK</td>
                    <td>Belum Terverifikasi</td>
                    <td>14696068250</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>3603</td>
                    <td>KABUPATEN TANGERANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>48903200000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>3671</td>
                    <td>KOTA TANGERANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>2829768317</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>5101</td>
                    <td>KABUPATEN JEMBRANA</td>
                    <td>Belum Terverifikasi</td>
                    <td>0</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>5102</td>
                    <td>KABUPATEN TABANAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>32706083200</td>
                    <td>1</td>
                    <td>14</td>
                    </tr>
                    <tr>
                    <td>5103</td>
                    <td>KABUPATEN BADUNG</td>
                    <td>Belum Terverifikasi</td>
                    <td>7413898982</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>5104</td>
                    <td>KABUPATEN GIANYAR</td>
                    <td>Belum Terverifikasi</td>
                    <td>3508000000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>5105</td>
                    <td>KABUPATEN KLUNGKUNG</td>
                    <td>Belum Terverifikasi</td>
                    <td>3303362712</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>5106</td>
                    <td>KABUPATEN BANGLI</td>
                    <td>Belum Terverifikasi</td>
                    <td>7997000000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>5107</td>
                    <td>KABUPATEN KARANGASEM</td>
                    <td>Belum Terverifikasi</td>
                    <td>15924376000</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>5108</td>
                    <td>KABUPATEN BULELENG</td>
                    <td>Belum Terverifikasi</td>
                    <td>3415163500</td>
                    <td>1</td>
                    <td>8</td>
                    </tr>
                    <tr>
                    <td>5171</td>
                    <td>KOTA DENPASAR</td>
                    <td>Belum Terverifikasi</td>
                    <td>4510173400</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>5201</td>
                    <td>KABUPATEN LOMBOK BARAT</td>
                    <td>Belum Terverifikasi</td>
                    <td>12518802400</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>5202</td>
                    <td>KABUPATEN LOMBOK TENGAH</td>
                    <td>Belum Terverifikasi</td>
                    <td>22766640000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>5203</td>
                    <td>KABUPATEN LOMBOK TIMUR</td>
                    <td>Belum Terverifikasi</td>
                    <td>17291313459</td>
                    <td>1</td>
                    <td>8</td>
                    </tr>
                    <tr>
                    <td>5208</td>
                    <td>KABUPATEN LOMBOK UTARA</td>
                    <td>Belum Terverifikasi</td>
                    <td>6933289529</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>5271</td>
                    <td>KOTA MATARAM</td>
                    <td>Belum Terverifikasi</td>
                    <td>70460074</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>5307</td>
                    <td>KABUPATEN SIKKA</td>
                    <td>Belum Terverifikasi</td>
                    <td>48634553120</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>5308</td>
                    <td>KABUPATEN ENDE</td>
                    <td>Belum Terverifikasi</td>
                    <td>8060000000</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>5309</td>
                    <td>KABUPATEN NGADA</td>
                    <td>Belum Terverifikasi</td>
                    <td>47698910600</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>5310</td>
                    <td>KABUPATEN MANGGARAI</td>
                    <td>Belum Terverifikasi</td>
                    <td>83438179300</td>
                    <td>1</td>
                    <td>16</td>
                    </tr>
                    <tr>
                    <td>5315</td>
                    <td>KABUPATEN MANGGARAI BARAT</td>
                    <td>Belum Terverifikasi</td>
                    <td>7519565000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>6110</td>
                    <td>KABUPATEN MELAWI</td>
                    <td>Belum Terverifikasi</td>
                    <td>33753500000</td>
                    <td>1</td>
                    <td>9</td>
                    </tr>
                    <tr>
                    <td>6112</td>
                    <td>KABUPATEN KUBU RAYA</td>
                    <td>Belum Terverifikasi</td>
                    <td>12805415710</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>6171</td>
                    <td>KOTA PONTIANAK</td>
                    <td>Belum Terverifikasi</td>
                    <td>5510722590</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>6172</td>
                    <td>KOTA SINGKAWANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>5356740000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>6203</td>
                    <td>KABUPATEN KAPUAS</td>
                    <td>Belum Terverifikasi</td>
                    <td>19644823000</td>
                    <td>1</td>
                    <td>13</td>
                    </tr>
                    <tr>
                    <td>6204</td>
                    <td>KABUPATEN BARITO SELATAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>13927986000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>6205</td>
                    <td>KABUPATEN BARITO UTARA</td>
                    <td>Belum Terverifikasi</td>
                    <td>4282694325</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>6271</td>
                    <td>KOTA PALANGKARAYA</td>
                    <td>Belum Terverifikasi</td>
                    <td>933873988</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>6301</td>
                    <td>KABUPATEN TANAH LAUT</td>
                    <td>Belum Terverifikasi</td>
                    <td>6424081600</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>6303</td>
                    <td>KABUPATEN BANJAR</td>
                    <td>Belum Terverifikasi</td>
                    <td>8045205150</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>6305</td>
                    <td>KABUPATEN TAPIN</td>
                    <td>Belum Terverifikasi</td>
                    <td>1839000000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>6308</td>
                    <td>KABUPATEN HULU SUNGAI UTARA</td>
                    <td>Belum Terverifikasi</td>
                    <td>4482435000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>6372</td>
                    <td>KOTA BANJARBARU</td>
                    <td>Belum Terverifikasi</td>
                    <td>5970440000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>6409</td>
                    <td>KABUPATEN PENAJAM PASER UTARA</td>
                    <td>Belum Terverifikasi</td>
                    <td>14004000000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>6471</td>
                    <td>KOTA BALIKPAPAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>15500000000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>6472</td>
                    <td>KOTA SAMARINDA</td>
                    <td>Belum Terverifikasi</td>
                    <td>28250000000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>6474</td>
                    <td>KOTA BONTANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>29100000000</td>
                    <td>1</td>
                    <td>8</td>
                    </tr>
                    <tr>
                    <td>6501</td>
                    <td>KABUPATEN BULUNGAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>13700000000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>6571</td>
                    <td>KOTA TARAKAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>10500000000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>7101</td>
                    <td>KABUPATEN BOLAANG MONGONDOW</td>
                    <td>Belum Terverifikasi</td>
                    <td>13645000000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>7102</td>
                    <td>KABUPATEN MINAHASA</td>
                    <td>Belum Terverifikasi</td>
                    <td>13035338022</td>
                    <td>1</td>
                    <td>7</td>
                    </tr>
                    <tr>
                    <td>7103</td>
                    <td>KABUPATEN KEPULAUAN SANGIHE</td>
                    <td>Belum Terverifikasi</td>
                    <td>1500000000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>7106</td>
                    <td>KABUPATEN MINAHASA UTARA</td>
                    <td>Belum Terverifikasi</td>
                    <td>6900000000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>7172</td>
                    <td>KOTA BITUNG</td>
                    <td>Belum Terverifikasi</td>
                    <td>5700000000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>7173</td>
                    <td>KOTA TOMOHON</td>
                    <td>Belum Terverifikasi</td>
                    <td>8010055617</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>7201</td>
                    <td>KABUPATEN BANGGAI</td>
                    <td>Belum Terverifikasi</td>
                    <td>4963500000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>7202</td>
                    <td>KABUPATEN POSO</td>
                    <td>Belum Terverifikasi</td>
                    <td>7674097450</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>7203</td>
                    <td>KABUPATEN DONGGALA</td>
                    <td>Belum Terverifikasi</td>
                    <td>15504800000</td>
                    <td>1</td>
                    <td>10</td>
                    </tr>
                    <tr>
                    <td>7271</td>
                    <td>KOTA PALU</td>
                    <td>Belum Terverifikasi</td>
                    <td>11333593500</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>7302</td>
                    <td>KABUPATEN BULUKUMBA</td>
                    <td>Belum Terverifikasi</td>
                    <td>11196512110</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>7303</td>
                    <td>KABUPATEN BANTAENG</td>
                    <td>Belum Terverifikasi</td>
                    <td>3957933300</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>7304</td>
                    <td>KABUPATEN JENEPONTO</td>
                    <td>Belum Terverifikasi</td>
                    <td>5937000000</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>7305</td>
                    <td>KABUPATEN TAKALAR</td>
                    <td>Belum Terverifikasi</td>
                    <td>7426815000</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>7306</td>
                    <td>KABUPATEN GOWA</td>
                    <td>Belum Terverifikasi</td>
                    <td>19965772400</td>
                    <td>1</td>
                    <td>10</td>
                    </tr>
                    <tr>
                    <td>7307</td>
                    <td>KABUPATEN SINJAI</td>
                    <td>Belum Terverifikasi</td>
                    <td>50084000000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>7308</td>
                    <td>KABUPATEN BONE</td>
                    <td>Belum Terverifikasi</td>
                    <td>16117544000</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>7309</td>
                    <td>KABUPATEN MAROS</td>
                    <td>Belum Terverifikasi</td>
                    <td>5560247935</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>7310</td>
                    <td>KABUPATEN PANGKAJENE KEPULAUAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>2729200000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>7311</td>
                    <td>KABUPATEN BARRU</td>
                    <td>Belum Terverifikasi</td>
                    <td>4266000000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>7312</td>
                    <td>KABUPATEN SOPPENG</td>
                    <td>Belum Terverifikasi</td>
                    <td>547200000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>7313</td>
                    <td>KABUPATEN WAJO</td>
                    <td>Belum Terverifikasi</td>
                    <td>2637000000</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>7314</td>
                    <td>KABUPATEN SIDENRENG RAPPANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>16505000000</td>
                    <td>1</td>
                    <td>13</td>
                    </tr>
                    <tr>
                    <td>7315</td>
                    <td>KABUPATEN PINRANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>9870821548</td>
                    <td>1</td>
                    <td>8</td>
                    </tr>
                    <tr>
                    <td>7316</td>
                    <td>KABUPATEN ENREKANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>10858635650</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>7318</td>
                    <td>KABUPATEN TANA TORAJA</td>
                    <td>Belum Terverifikasi</td>
                    <td>42200000000</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>7371</td>
                    <td>KOTA MAKASSAR</td>
                    <td>Belum Terverifikasi</td>
                    <td>5483747800</td>
                    <td>1</td>
                    <td>8</td>
                    </tr>
                    <tr>
                    <td>7372</td>
                    <td>KOTA PARE PARE</td>
                    <td>Belum Terverifikasi</td>
                    <td>12000000000</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>7373</td>
                    <td>KOTA PALOPO</td>
                    <td>Belum Terverifikasi</td>
                    <td>5527109700</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>7403</td>
                    <td>KABUPATEN MUNA</td>
                    <td>Belum Terverifikasi</td>
                    <td>20200000000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>7471</td>
                    <td>KOTA KENDARI</td>
                    <td>Belum Terverifikasi</td>
                    <td>14688357000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>7472</td>
                    <td>KOTA BAU BAU</td>
                    <td>Belum Terverifikasi</td>
                    <td>5698026256</td>
                    <td>1</td>
                    <td>8</td>
                    </tr>
                    <tr>
                    <td>7501</td>
                    <td>KABUPATEN GORONTALO</td>
                    <td>Belum Terverifikasi</td>
                    <td>3382053000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>7571</td>
                    <td>KOTA GORONTALO</td>
                    <td>Belum Terverifikasi</td>
                    <td>24049714000</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>7602</td>
                    <td>KABUPATEN MAMUJU</td>
                    <td>Belum Terverifikasi</td>
                    <td>10741736065</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>7604</td>
                    <td>KABUPATEN POLEWALI MANDAR</td>
                    <td>Belum Terverifikasi</td>
                    <td>42635000000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>7605</td>
                    <td>KABUPATEN MAJENE</td>
                    <td>Belum Terverifikasi</td>
                    <td>73172164940</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>8101</td>
                    <td>KABUPATEN MALUKU TENGAH</td>
                    <td>Belum Terverifikasi</td>
                    <td>29777825000</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>8271</td>
                    <td>KOTA TERNATE</td>
                    <td>Belum Terverifikasi</td>
                    <td>3777165000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>9103</td>
                    <td>KABUPATEN JAYAPURA</td>
                    <td>Belum Terverifikasi</td>
                    <td>6275147600</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>9171</td>
                    <td>KOTA JAYAPURA</td>
                    <td>Belum Terverifikasi</td>
                    <td>5843646800</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>9202</td>
                    <td>KABUPATEN MANOKWARI</td>
                    <td>Belum Terverifikasi</td>
                    <td>1884193000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    </tbody>
                    </table>

             </div>
        </div>
    </div>
</div> 

@stop
@section('js')

@endsection