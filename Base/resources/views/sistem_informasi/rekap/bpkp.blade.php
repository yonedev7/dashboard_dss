@extends('adminlte::page')

@section('content_header')
@stop
@section('content')
<div class="container" id="rekap-app">
    <div class="card">
        <div class="card-body">
            <p class="mb-0">Diperbarui</p>
            <p><span class="badge badge-warning">{{$date[$index]}}</span></p>
            <h3>Evaluasi Kinerja Air minum (BPKP) - 2020</h3>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <div class="card bg-dark">
                        <div class="card-body">
                            <h3><b>Data Terinput</b></h3>
                            <p>211 Pemda</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card bg-secondary">
                        <div class="card-body">
                            <h3><b>Data Terverifikasi</b></h3>
                            <p>0 Pemda</p>

        
                        </div>
                    </div>
                </div>
            </div>
            <div class="">
                <table class="table table-bordered">
                    <tbody>
                    <tr>
                    <td width="87" height="20">Kodepemda</td>
                    <td width="87">Nama Pemda</td>
                    <td width="87">Status Data</td>
                    </tr>
                    <tr>
                    <td height="20">1101</td>
                    <td>KABUPATEN ACEH SELATAN</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1102</td>
                    <td>KABUPATEN ACEH TENGGARA</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1104</td>
                    <td>KABUPATEN ACEH TENGAH</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1105</td>
                    <td>KABUPATEN ACEH BARAT</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1106</td>
                    <td>KABUPATEN ACEH BESAR</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1108</td>
                    <td>KABUPATEN ACEH UTARA</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1116</td>
                    <td>KABUPATEN ACEH TAMIANG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1171</td>
                    <td>KOTA BANDA ACEH</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1172</td>
                    <td>KOTA SABANG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1174</td>
                    <td>KOTA LANGSA</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">12</td>
                    <td>PROVINSI SUMATERA UTARA</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1202</td>
                    <td>KABUPATEN TAPANULI UTARA</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1204</td>
                    <td>KABUPATEN NIAS</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1205</td>
                    <td>KABUPATEN LANGKAT</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1206</td>
                    <td>KABUPATEN KARO</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1207</td>
                    <td>KABUPATEN DELI SERDANG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1208</td>
                    <td>KABUPATEN SIMALUNGUN</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1209</td>
                    <td>KABUPATEN ASAHAN</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1210</td>
                    <td>KABUPATEN LABUHANBATU</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1211</td>
                    <td>KABUPATEN DAIRI</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1272</td>
                    <td>KOTA PEMATANGSIANTAR</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1273</td>
                    <td>KOTA SIBOLGA</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1274</td>
                    <td>KOTA TANJUNG BALAI</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1275</td>
                    <td>KOTA BINJAI</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1276</td>
                    <td>KOTA TEBING TINGGI</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1277</td>
                    <td>KOTA PADANGSIDIMPUAN</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1302</td>
                    <td>KABUPATEN SOLOK</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1303</td>
                    <td>KABUPATEN SIJUNJUNG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1304</td>
                    <td>KABUPATEN TANAH DATAR</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1305</td>
                    <td>KABUPATEN PADANG PARIAMAN</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1306</td>
                    <td>KABUPATEN AGAM</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1371</td>
                    <td>KOTA PADANG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1372</td>
                    <td>KOTA SOLOK</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1373</td>
                    <td>KOTA SAWAHLUNTO</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1374</td>
                    <td>KOTA PADANG PANJANG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1375</td>
                    <td>KOTA BUKITTINGGI</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1376</td>
                    <td>KOTA PAYAKUMBUH</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1401</td>
                    <td>KABUPATEN KAMPAR</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1402</td>
                    <td>KABUPATEN INDRAGIRI HULU</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1403</td>
                    <td>KABUPATEN BENGKALIS</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1404</td>
                    <td>KABUPATEN INDRAGIRI HILIR</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1471</td>
                    <td>KOTA PEKANBARU</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1472</td>
                    <td>KOTA DUMAI</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1501</td>
                    <td>KABUPATEN KERINCI</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1502</td>
                    <td>KABUPATEN MERANGIN</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1504</td>
                    <td>KABUPATEN BATANGHARI</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1508</td>
                    <td>KABUPATEN BUNGO</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1571</td>
                    <td>KOTA JAMBI</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1601</td>
                    <td>KABUPATEN OGAN KOMERING ULU</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1603</td>
                    <td>KABUPATEN MUARA ENIM</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1604</td>
                    <td>KABUPATEN LAHAT</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1606</td>
                    <td>KABUPATEN MUSI BANYUASIN</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1610</td>
                    <td>KABUPATEN OGAN ILIR</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1671</td>
                    <td>KOTA PALEMBANG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1673</td>
                    <td>KOTA LUBUK LINGGAU</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1674</td>
                    <td>KOTA PRABUMULIH</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1701</td>
                    <td>KABUPATEN BENGKULU SELATAN</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1702</td>
                    <td>KABUPATEN REJANG LEBONG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1709</td>
                    <td>KABUPATEN BENGKULU TENGAH</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1771</td>
                    <td>KOTA BENGKULU</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1801</td>
                    <td>KABUPATEN LAMPUNG SELATAN</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1806</td>
                    <td>KABUPATEN TANGGAMUS</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1809</td>
                    <td>KABUPATEN PESAWARAN</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1871</td>
                    <td>KOTA BANDAR LAMPUNG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1901</td>
                    <td>KABUPATEN BANGKA</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1902</td>
                    <td>KABUPATEN BELITUNG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">1971</td>
                    <td>KOTA PANGKAL PINANG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3201</td>
                    <td>KABUPATEN BOGOR</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3202</td>
                    <td>KABUPATEN SUKABUMI</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3204</td>
                    <td>KABUPATEN BANDUNG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3207</td>
                    <td>KABUPATEN CIAMIS</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3208</td>
                    <td>KABUPATEN KUNINGAN</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3209</td>
                    <td>KABUPATEN CIREBON</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3210</td>
                    <td>KABUPATEN MAJALENGKA</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3211</td>
                    <td>KABUPATEN SUMEDANG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3212</td>
                    <td>KABUPATEN INDRAMAYU</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3213</td>
                    <td>KABUPATEN SUBANG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3215</td>
                    <td>KABUPATEN KARAWANG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3216</td>
                    <td>KABUPATEN BEKASI</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3271</td>
                    <td>KOTA BOGOR</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3272</td>
                    <td>KOTA SUKABUMI</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3273</td>
                    <td>KOTA BANDUNG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3274</td>
                    <td>KOTA CIREBON</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3276</td>
                    <td>KOTA DEPOK</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3279</td>
                    <td>KOTA BANJAR</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3301</td>
                    <td>KABUPATEN CILACAP</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3302</td>
                    <td>KABUPATEN BANYUMAS</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3305</td>
                    <td>KABUPATEN KEBUMEN</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3306</td>
                    <td>KABUPATEN PURWOREJO</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3308</td>
                    <td>KABUPATEN MAGELANG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3309</td>
                    <td>KABUPATEN BOYOLALI</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3310</td>
                    <td>KABUPATEN KLATEN</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3311</td>
                    <td>KABUPATEN SUKOHARJO</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3314</td>
                    <td>KABUPATEN SRAGEN</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3318</td>
                    <td>KABUPATEN PATI</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3322</td>
                    <td>KABUPATEN SEMARANG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3325</td>
                    <td>KABUPATEN BATANG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3326</td>
                    <td>KABUPATEN PEKALONGAN</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3329</td>
                    <td>KABUPATEN BREBES</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3371</td>
                    <td>KOTA MAGELANG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3372</td>
                    <td>KOTA SURAKARTA</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3373</td>
                    <td>KOTA SALATIGA</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3374</td>
                    <td>KOTA SEMARANG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3375</td>
                    <td>KOTA PEKALONGAN</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3376</td>
                    <td>KOTA TEGAL</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3402</td>
                    <td>KABUPATEN BANTUL</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3404</td>
                    <td>KABUPATEN SLEMAN</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3471</td>
                    <td>KOTA YOGYAKARTA</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3501</td>
                    <td>KABUPATEN PACITAN</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3502</td>
                    <td>KABUPATEN PONOROGO</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3504</td>
                    <td>KABUPATEN TULUNGAGUNG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3505</td>
                    <td>KABUPATEN BLITAR</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3506</td>
                    <td>KABUPATEN KEDIRI</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3507</td>
                    <td>KABUPATEN MALANG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3508</td>
                    <td>KABUPATEN LUMAJANG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3509</td>
                    <td>KABUPATEN JEMBER</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3513</td>
                    <td>KABUPATEN PROBOLINGGO</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3514</td>
                    <td>KABUPATEN PASURUAN</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3515</td>
                    <td>KABUPATEN SIDOARJO</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3521</td>
                    <td>KABUPATEN NGAWI</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3524</td>
                    <td>KABUPATEN LAMONGAN</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3525</td>
                    <td>KABUPATEN GRESIK</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3526</td>
                    <td>KABUPATEN BANGKALAN</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3527</td>
                    <td>KABUPATEN SAMPANG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3573</td>
                    <td>KOTA MALANG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3574</td>
                    <td>KOTA PROBOLINGGO</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3575</td>
                    <td>KOTA PASURUAN</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3578</td>
                    <td>KOTA SURABAYA</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3579</td>
                    <td>KOTA BATU</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3601</td>
                    <td>KABUPATEN PANDEGLANG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3602</td>
                    <td>KABUPATEN LEBAK</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3603</td>
                    <td>KABUPATEN TANGERANG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">3671</td>
                    <td>KOTA TANGERANG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">5101</td>
                    <td>KABUPATEN JEMBRANA</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">5102</td>
                    <td>KABUPATEN TABANAN</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">5103</td>
                    <td>KABUPATEN BADUNG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">5104</td>
                    <td>KABUPATEN GIANYAR</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">5105</td>
                    <td>KABUPATEN KLUNGKUNG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">5106</td>
                    <td>KABUPATEN BANGLI</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">5107</td>
                    <td>KABUPATEN KARANGASEM</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">5108</td>
                    <td>KABUPATEN BULELENG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">5171</td>
                    <td>KOTA DENPASAR</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">5202</td>
                    <td>KABUPATEN LOMBOK TENGAH</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">5203</td>
                    <td>KABUPATEN LOMBOK TIMUR</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">5208</td>
                    <td>KABUPATEN LOMBOK UTARA</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">5271</td>
                    <td>KOTA MATARAM</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">5307</td>
                    <td>KABUPATEN SIKKA</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">5308</td>
                    <td>KABUPATEN ENDE</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">5309</td>
                    <td>KABUPATEN NGADA</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">5310</td>
                    <td>KABUPATEN MANGGARAI</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">5315</td>
                    <td>KABUPATEN MANGGARAI BARAT</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">5371</td>
                    <td>KOTA KUPANG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">6110</td>
                    <td>KABUPATEN MELAWI</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">6112</td>
                    <td>KABUPATEN KUBU RAYA</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">6171</td>
                    <td>KOTA PONTIANAK</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">6172</td>
                    <td>KOTA SINGKAWANG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">6203</td>
                    <td>KABUPATEN KAPUAS</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">6204</td>
                    <td>KABUPATEN BARITO SELATAN</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">6205</td>
                    <td>KABUPATEN BARITO UTARA</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">6271</td>
                    <td>KOTA PALANGKARAYA</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">6301</td>
                    <td>KABUPATEN TANAH LAUT</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">6303</td>
                    <td>KABUPATEN BANJAR</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">6305</td>
                    <td>KABUPATEN TAPIN</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">6308</td>
                    <td>KABUPATEN HULU SUNGAI UTARA</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">6371</td>
                    <td>KOTA BANJARMASIN</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">6409</td>
                    <td>KABUPATEN PENAJAM PASER UTARA</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">6471</td>
                    <td>KOTA BALIKPAPAN</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">6472</td>
                    <td>KOTA SAMARINDA</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">6474</td>
                    <td>KOTA BONTANG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">6501</td>
                    <td>KABUPATEN BULUNGAN</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">6571</td>
                    <td>KOTA TARAKAN</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7101</td>
                    <td>KABUPATEN BOLAANG MONGONDOW</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7102</td>
                    <td>KABUPATEN MINAHASA</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7103</td>
                    <td>KABUPATEN KEPULAUAN SANGIHE</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7106</td>
                    <td>KABUPATEN MINAHASA UTARA</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7172</td>
                    <td>KOTA BITUNG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7173</td>
                    <td>KOTA TOMOHON</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7201</td>
                    <td>KABUPATEN BANGGAI</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7202</td>
                    <td>KABUPATEN POSO</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7203</td>
                    <td>KABUPATEN DONGGALA</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7271</td>
                    <td>KOTA PALU</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7302</td>
                    <td>KABUPATEN BULUKUMBA</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7303</td>
                    <td>KABUPATEN BANTAENG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7305</td>
                    <td>KABUPATEN TAKALAR</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7306</td>
                    <td>KABUPATEN GOWA</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7307</td>
                    <td>KABUPATEN SINJAI</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7308</td>
                    <td>KABUPATEN BONE</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7309</td>
                    <td>KABUPATEN MAROS</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7310</td>
                    <td>KABUPATEN PANGKAJENE KEPULAUAN</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7311</td>
                    <td>KABUPATEN BARRU</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7312</td>
                    <td>KABUPATEN SOPPENG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7313</td>
                    <td>KABUPATEN WAJO</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7314</td>
                    <td>KABUPATEN SIDENRENG RAPPANG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7316</td>
                    <td>KABUPATEN ENREKANG</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7318</td>
                    <td>KABUPATEN TANA TORAJA</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7371</td>
                    <td>KOTA MAKASSAR</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7372</td>
                    <td>KOTA PARE PARE</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7373</td>
                    <td>KOTA PALOPO</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7403</td>
                    <td>KABUPATEN MUNA</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7471</td>
                    <td>KOTA KENDARI</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7472</td>
                    <td>KOTA BAU BAU</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7501</td>
                    <td>KABUPATEN GORONTALO</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7571</td>
                    <td>KOTA GORONTALO</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7602</td>
                    <td>KABUPATEN MAMUJU</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7604</td>
                    <td>KABUPATEN POLEWALI MANDAR</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">7605</td>
                    <td>KABUPATEN MAJENE</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">8101</td>
                    <td>KABUPATEN MALUKU TENGAH</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">8171</td>
                    <td>KOTA AMBON</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">8271</td>
                    <td>KOTA TERNATE</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">9103</td>
                    <td>KABUPATEN JAYAPURA</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    <tr>
                    <td height="20">9202</td>
                    <td>KABUPATEN MANOKWARI</td>
                    <td>Belum Terverifikasi</td>
                    </tr>
                    </tbody>
                    </table>
             </div>
        </div>
    </div>
</div> 

@stop
@section('js')

@endsection