@extends('adminlte::page')

@section('content_header')
<h5><b>Rekap Data {{$Stahun}}</b></h5>

@endsection

@section('content')
<div id="app">
    <div class="row no-gutters" style="z-index: 0;">
        <div class="col-4 {{$basis=='nasional'?'bg-secondary':' bg-primary'}} p-3 pt-5" >
            <h4><b>NASIONAL</b></h4>
            <a href="{{url()->current().'?basis=nasional'}}" class="btn btn-success"><i class="fa fa-arrow-down"></i></a>
        </div>
        <div class="col-4  {{$basis=='longlist'?'bg-secondary efault':' bg-success'}} p-3 pt-5">
            <h4><b>LONGLIST</b></h4>
            <a  href="{{url()->current().'?basis=longlist'}}" class="btn btn-warning text-dark"><i class="fa fa-arrow-down text-dark"></i></a>
    
        </div>
        <div class="col-4  {{$basis=='sortlist'?'bg-secondary':' bg-warning'}} p-3 pt-5">
            <h4><b>SORTLIST</b></h4>
            <a  href="{{url()->current().'?basis=sortlist'}}" class="btn btn-primary text-white"><i class="fa fa-arrow-down text-white"></i></a>
    
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="form-group " >
                <div class="col-4">
                  <label for="">Filter</label>
                  <select name="" class="form-control" id="" v-model="filter">
                      <option value="provinsi dan kota / kab">Semua</option>
                      <option value="provinsi">Provinsi</option>
                      <option value="kota">Kota</option>
                      <option value="kab">Kab</option>
                  </select>
                </div>
              </div>
        </div>
        <div class="col-12" id="table-rekap">
            <div v-if="load_rekap">
                <div class="text-center"><div role="status" class="spinner-grow text-primary"><span class="sr-only">Loading...</span></div> <div role="status" class="spinner-grow text-secondary"><span class="sr-only">Loading...</span></div> <div role="status" class="spinner-grow text-success"><span class="sr-only">Loading...</span></div> <div role="status" class="spinner-grow text-danger"><span class="sr-only">Loading...</span></div> <div role="status" class="spinner-grow text-warning"><span class="sr-only">Loading...</span></div> <div role="status" class="spinner-grow text-info"><span class="sr-only">Loading...</span></div></div>
            </div>
            <div class="card" v-if="rekap.length">
               <div class="card-header">
                <p class="text-center"><b>Rekap Data Basis {{strtoupper($basis)}} - @{{filter.toUpperCase()}}</b></p>
               </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Aksi</th>
                                    <th>Nama Data</th>
                                    <th>Jenis Dataset</th>
                                    <th>Jumlah Row Data Terecord</th>
                                    <th>Jumlah Pemda</th>
                                    <th>Jumlah Pemda Terverifikasi</th>
                                    <th>Jumlah Pemda Belum Terverifikasi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="item,key in rekap">
                                    <td>
                                        <button class="btn btn-sm btn-success" @click="detail(key)">Detail</button>
                                    </td>
                                    <td>@{{item.name}}</td>
                                    <td>@{{item.jenis_dataset?'DATA PUSAT':'DATA KEDAERAHAN'}}</td>
                                    <td>@{{formatAngka(item.data.count_row)}} Row Data</td>
                                    <td>@{{formatAngka(item.data.jumlah_pemda)}}</td>
                                    <td>@{{formatAngka(item.data.jumlah_pemda_ver)}}</td>
                                    <td>@{{formatAngka(item.data.jumlah_pemda_not_ver)}}</td>

                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12" id="table-data">
            <div class="parallax" v-if="data.data.length" style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
                <div class="container">
                    <section>
                        <h3><b>@{{data.title}} {{$Stahun}}</b></h3>
                        <p><b>@{{data.tujuan}}</b></p>
                    </section>
                </div>
            </div>
            <div class="container" v-if="data.data.length">
                <div class="row">
                    <div class="col-6">
                        <div class="card bg-dark">
                            <div class="card-body">
                                <h3><b>Jumlah Pemda Terverifikasi</b></h3>
                                 @{{formatAngka(data.rekap.jumlah_pemda_ver)}}
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="card bg-dark">
                            <div class="card-body">
                                <h3><b>Jumlah Pemda Belum Terverifikasi</b></h3>
                                 @{{formatAngka(data.rekap.jumlah_pemda_not_ver)}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div v-if="load_data">
                <div class="text-center"><div role="status" class="spinner-grow text-primary"><span class="sr-only">Loading...</span></div> <div role="status" class="spinner-grow text-secondary"><span class="sr-only">Loading...</span></div> <div role="status" class="spinner-grow text-success"><span class="sr-only">Loading...</span></div> <div role="status" class="spinner-grow text-danger"><span class="sr-only">Loading...</span></div> <div role="status" class="spinner-grow text-warning"><span class="sr-only">Loading...</span></div> <div role="status" class="spinner-grow text-info"><span class="sr-only">Loading...</span></div></div>
            </div>

            <div class="card" v-if="data.data.length">
               
               <div class="card-header">
                <p class="text-center"><b>Rekap Data Basis {{strtoupper($basis)}} - @{{filter.toUpperCase()}}</b></p>
                <p class="text-center"><small>@{{data.date}}</small></p>
               </div>
                <div class="card-body">

                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Kode Pemda</th>
                                    <th>Nama Pemda</th>
                                    <th>Status Data</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="item,key in data.data">
                                    
                                    <td>@{{parseInt(item.kodepemda)}}</td>
                                    <td>@{{item.namapemda}}</td>
                                    <td>@{{item.status?'Terverifikasi':'Belum Terverifikasi'}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@section('js')
<script>
var app=new Vue({
    el:'#app',
    data:{
        filter:'provinsi dan kota / kab',
        basis:'<?=$_GET['basis']??''?>',
        rekap:[],
        load_rekap:false,
        load_data:false,
        data:{
            title:'',
            tujuan:'',
            rekap:{
                'jumlah_pemda':0,
                'jumlah_pemda_ver':0,
                'jumlah_pemda_not_ver':0,
            },
            data:[]
        }
    },
    watch:{
        filter:function(){
            this.loadData();
        }
    },
    created(){
        this.loadData();
    },methods:{
        detail:function(key){
            this.data.data=[];
            this.data.title='';
            this.data.tujuan='';
            this.load_data=true;
            if(this.rekap[key]!=undefined){
                
            var c=JSON.parse(JSON.stringify(this.rekap[key]));
            this.data.title=c.name;
            this.data.tujuan=c.tujuan;
             req_ajax.post('{{route('api-web.rekap.load-data-detail',['tahun'=>$Stahun])}}',{
                filter:this.filter,
                basis:this.basis,
                table:c.table,
                }).then(function(res){
                   app.data.data=res.data.data;
                   app.data.date=res.data.date;
                   app.data.rekap=res.data.rekap;

                    app.load_data=false;

                    window.scrollToDom('#table-data');

                });
            
            }


        },
        formatAngka:function(angka){
                var number_string = ((angka||'0')+'').replace(/[^,\d]/g, '').toString(),
                split   		= number_string.split(','),
                sisa     		= split[0].length % 3,
                rupiah     		= split[0].substr(0, sisa),
                ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
    
                // tambahkan titik jika yang di input sudah menjadi angka ribuan
                if(ribuan){
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                }
    
                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                return rupiah;
            },
        loadData:function(){
            this.load_rekap=true;
            this.rekap=[];
            this.data.data=[];
            this.data.title='';
            this.data.tujuan='';

            req_ajax.post('{{route('api-web.rekap.load-data',['tahun'=>$Stahun])}}',{
                filter:this.filter,
                basis:this.basis,
                }).then(function(res){
                   app.rekap=res.data;
                    app.load_rekap=false;

                    window.scrollToDom('#table-rekap');

                });
        }
    }
});
</script>    
@endsection