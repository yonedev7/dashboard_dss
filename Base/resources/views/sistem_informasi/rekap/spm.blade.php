@extends('adminlte::page')

@section('content_header')
@stop
@section('content')
<div class="container" id="rekap-app">
    <div class="card">
        <div class="card-body">
            <p class="mb-0">Diperbarui</p>
            <p><span class="badge badge-warning">{{$date[$index]}}</span></p>
            <h3>DATA RKP PROGRAM KEGIATAN 2022</h3>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <div class="card bg-dark">
                        <div class="card-body">
                            <h3><b>Data Terinput</b></h3>
                            <p>211 Pemda</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card bg-secondary">
                        <div class="card-body">
                            <h3><b>Data Terverifikasi</b></h3>
                            <p>0 Pemda</p>

        
                        </div>
                    </div>
                </div>
            </div>
            <div class="">
                <table  class="table table-bordered">
                    <tbody>
                    <tr>
                    <td width="87">Kodepemda</td>
                    <td width="87">Nama Pemda</td>
                    <td width="180">Status Data</td>
                    <td width="163">Pagu Indikatif</td>
                    <td width="87">Jumlah Kegiatan</td>
                    <td width="87">Jumlah Sub Kegiatan</td>
                    </tr>
                    <tr>
                    <td>1101</td>
                    <td>KABUPATEN ACEH SELATAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>82631000000</td>
                    <td>1</td>
                    <td>21</td>
                    </tr>
                    <tr>
                    <td>1102</td>
                    <td>KABUPATEN ACEH TENGGARA</td>
                    <td>Belum Terverifikasi</td>
                    <td>18700000000</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>1104</td>
                    <td>KABUPATEN ACEH TENGAH</td>
                    <td>Belum Terverifikasi</td>
                    <td>16450850000</td>
                    <td>1</td>
                    <td>21</td>
                    </tr>
                    <tr>
                    <td>1105</td>
                    <td>KABUPATEN ACEH BARAT</td>
                    <td>Belum Terverifikasi</td>
                    <td>6895000000</td>
                    <td>1</td>
                    <td>22</td>
                    </tr>
                    <tr>
                    <td>1106</td>
                    <td>KABUPATEN ACEH BESAR</td>
                    <td>Belum Terverifikasi</td>
                    <td>8038824000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>1108</td>
                    <td>KABUPATEN ACEH UTARA</td>
                    <td>Belum Terverifikasi</td>
                    <td>15436710500</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>1116</td>
                    <td>KABUPATEN ACEH TAMIANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>6021199970</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>1171</td>
                    <td>KOTA BANDA ACEH</td>
                    <td>Belum Terverifikasi</td>
                    <td>23434386000</td>
                    <td>1</td>
                    <td>7</td>
                    </tr>
                    <tr>
                    <td>1172</td>
                    <td>KOTA SABANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>680000000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>1174</td>
                    <td>KOTA LANGSA</td>
                    <td>Belum Terverifikasi</td>
                    <td>3150000000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>12</td>
                    <td>PROVINSI SUMATERA UTARA</td>
                    <td>Belum Terverifikasi</td>
                    <td>169000000000</td>
                    <td>1</td>
                    <td>7</td>
                    </tr>
                    <tr>
                    <td>1202</td>
                    <td>KABUPATEN TAPANULI UTARA</td>
                    <td>Belum Terverifikasi</td>
                    <td>785833870</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>1204</td>
                    <td>KABUPATEN NIAS</td>
                    <td>Belum Terverifikasi</td>
                    <td>500000000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>1205</td>
                    <td>KABUPATEN LANGKAT</td>
                    <td>Belum Terverifikasi</td>
                    <td>5800000000</td>
                    <td>1</td>
                    <td>7</td>
                    </tr>
                    <tr>
                    <td>1206</td>
                    <td>KABUPATEN KARO</td>
                    <td>Belum Terverifikasi</td>
                    <td>450000000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>1207</td>
                    <td>KABUPATEN DELI SERDANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>16519098052</td>
                    <td>1</td>
                    <td>7</td>
                    </tr>
                    <tr>
                    <td>1208</td>
                    <td>KABUPATEN SIMALUNGUN</td>
                    <td>Belum Terverifikasi</td>
                    <td>36453990358</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>1209</td>
                    <td>KABUPATEN ASAHAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>9962140000</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>1210</td>
                    <td>KABUPATEN LABUHANBATU</td>
                    <td>Belum Terverifikasi</td>
                    <td>1704200000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>1211</td>
                    <td>KABUPATEN DAIRI</td>
                    <td>Belum Terverifikasi</td>
                    <td>1250000000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>1272</td>
                    <td>KOTA PEMATANGSIANTAR</td>
                    <td>Belum Terverifikasi</td>
                    <td>5720000000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>1273</td>
                    <td>KOTA SIBOLGA</td>
                    <td>Belum Terverifikasi</td>
                    <td>1425000000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>1274</td>
                    <td>KOTA TANJUNG BALAI</td>
                    <td>Belum Terverifikasi</td>
                    <td>4610000000</td>
                    <td>1</td>
                    <td>11</td>
                    </tr>
                    <tr>
                    <td>1275</td>
                    <td>KOTA BINJAI</td>
                    <td>Belum Terverifikasi</td>
                    <td>1500000000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>1276</td>
                    <td>KOTA TEBING TINGGI</td>
                    <td>Belum Terverifikasi</td>
                    <td>4383074096</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>1277</td>
                    <td>KOTA PADANGSIDIMPUAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>399000000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>1302</td>
                    <td>KABUPATEN SOLOK</td>
                    <td>Belum Terverifikasi</td>
                    <td>7689887000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>1303</td>
                    <td>KABUPATEN SIJUNJUNG</td>
                    <td>Belum Terverifikasi</td>
                    <td>674035676</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>1304</td>
                    <td>KABUPATEN TANAH DATAR</td>
                    <td>Belum Terverifikasi</td>
                    <td>12892609600</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>1305</td>
                    <td>KABUPATEN PADANG PARIAMAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>6600000000</td>
                    <td>1</td>
                    <td>7</td>
                    </tr>
                    <tr>
                    <td>1306</td>
                    <td>KABUPATEN AGAM</td>
                    <td>Belum Terverifikasi</td>
                    <td>32720000000</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>1371</td>
                    <td>KOTA PADANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>900000000</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>1372</td>
                    <td>KOTA SOLOK</td>
                    <td>Belum Terverifikasi</td>
                    <td>2590999000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>1373</td>
                    <td>KOTA SAWAHLUNTO</td>
                    <td>Belum Terverifikasi</td>
                    <td>4030000000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>1375</td>
                    <td>KOTA BUKITTINGGI</td>
                    <td>Belum Terverifikasi</td>
                    <td>13784000000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>1376</td>
                    <td>KOTA PAYAKUMBUH</td>
                    <td>Belum Terverifikasi</td>
                    <td>258500000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>1401</td>
                    <td>KABUPATEN KAMPAR</td>
                    <td>Belum Terverifikasi</td>
                    <td>1650000000</td>
                    <td>1</td>
                    <td>8</td>
                    </tr>
                    <tr>
                    <td>1402</td>
                    <td>KABUPATEN INDRAGIRI HULU</td>
                    <td>Belum Terverifikasi</td>
                    <td>3726924600</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>1403</td>
                    <td>KABUPATEN BENGKALIS</td>
                    <td>Belum Terverifikasi</td>
                    <td>43256786764</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>1404</td>
                    <td>KABUPATEN INDRAGIRI HILIR</td>
                    <td>Belum Terverifikasi</td>
                    <td>10587870000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>1471</td>
                    <td>KOTA PEKANBARU</td>
                    <td>Belum Terverifikasi</td>
                    <td>17863339600</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>1472</td>
                    <td>KOTA DUMAI</td>
                    <td>Belum Terverifikasi</td>
                    <td>7150000000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>1501</td>
                    <td>KABUPATEN KERINCI</td>
                    <td>Belum Terverifikasi</td>
                    <td>7000000000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>1502</td>
                    <td>KABUPATEN MERANGIN</td>
                    <td>Belum Terverifikasi</td>
                    <td>11932827500</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>1504</td>
                    <td>KABUPATEN BATANGHARI</td>
                    <td>Belum Terverifikasi</td>
                    <td>4237437000</td>
                    <td>1</td>
                    <td>8</td>
                    </tr>
                    <tr>
                    <td>1508</td>
                    <td>KABUPATEN BUNGO</td>
                    <td>Belum Terverifikasi</td>
                    <td>800000000</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>1571</td>
                    <td>KOTA JAMBI</td>
                    <td>Belum Terverifikasi</td>
                    <td>4959426420</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>1601</td>
                    <td>KABUPATEN OGAN KOMERING ULU</td>
                    <td>Belum Terverifikasi</td>
                    <td>2980460800</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>1603</td>
                    <td>KABUPATEN MUARA ENIM</td>
                    <td>Belum Terverifikasi</td>
                    <td>6575397249</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>1604</td>
                    <td>KABUPATEN LAHAT</td>
                    <td>Belum Terverifikasi</td>
                    <td>9930181000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>1606</td>
                    <td>KABUPATEN MUSI BANYUASIN</td>
                    <td>Belum Terverifikasi</td>
                    <td>17137819400</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>1610</td>
                    <td>KABUPATEN OGAN ILIR</td>
                    <td>Belum Terverifikasi</td>
                    <td>19144351045</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>1671</td>
                    <td>KOTA PALEMBANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>50000000000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>1673</td>
                    <td>KOTA LUBUK LINGGAU</td>
                    <td>Belum Terverifikasi</td>
                    <td>1237300000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>1674</td>
                    <td>KOTA PRABUMULIH</td>
                    <td>Belum Terverifikasi</td>
                    <td>&nbsp;</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>1701</td>
                    <td>KABUPATEN BENGKULU SELATAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>8097374000</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>1702</td>
                    <td>KABUPATEN REJANG LEBONG</td>
                    <td>Belum Terverifikasi</td>
                    <td>3104621521</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>1709</td>
                    <td>KABUPATEN BENGKULU TENGAH</td>
                    <td>Belum Terverifikasi</td>
                    <td>9096500000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>1771</td>
                    <td>KOTA BENGKULU</td>
                    <td>Belum Terverifikasi</td>
                    <td>5333310275</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>1801</td>
                    <td>KABUPATEN LAMPUNG SELATAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>1061968700</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>1803</td>
                    <td>KABUPATEN LAMPUNG UTARA</td>
                    <td>Belum Terverifikasi</td>
                    <td>7922312800</td>
                    <td>1</td>
                    <td>21</td>
                    </tr>
                    <tr>
                    <td>1806</td>
                    <td>KABUPATEN TANGGAMUS</td>
                    <td>Belum Terverifikasi</td>
                    <td>15503722300</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>1809</td>
                    <td>KABUPATEN PESAWARAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>15818728000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>1871</td>
                    <td>KOTA BANDAR LAMPUNG</td>
                    <td>Belum Terverifikasi</td>
                    <td>3120076050</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>1901</td>
                    <td>KABUPATEN BANGKA</td>
                    <td>Belum Terverifikasi</td>
                    <td>16957385670</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>1902</td>
                    <td>KABUPATEN BELITUNG</td>
                    <td>Belum Terverifikasi</td>
                    <td>6316566683</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>1971</td>
                    <td>KOTA PANGKAL PINANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>500000000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>2172</td>
                    <td>KOTA TANJUNG PINANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>45721450000</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>3201</td>
                    <td>KABUPATEN BOGOR</td>
                    <td>Belum Terverifikasi</td>
                    <td>16360000000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>3202</td>
                    <td>KABUPATEN SUKABUMI</td>
                    <td>Belum Terverifikasi</td>
                    <td>160235526900</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>3204</td>
                    <td>KABUPATEN BANDUNG</td>
                    <td>Belum Terverifikasi</td>
                    <td>28378643000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>3207</td>
                    <td>KABUPATEN CIAMIS</td>
                    <td>Belum Terverifikasi</td>
                    <td>39142580000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>3208</td>
                    <td>KABUPATEN KUNINGAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>79402517700</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>3209</td>
                    <td>KABUPATEN CIREBON</td>
                    <td>Belum Terverifikasi</td>
                    <td>1850000000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>3210</td>
                    <td>KABUPATEN MAJALENGKA</td>
                    <td>Belum Terverifikasi</td>
                    <td>6990419800</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>3211</td>
                    <td>KABUPATEN SUMEDANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>106564468005</td>
                    <td>1</td>
                    <td>7</td>
                    </tr>
                    <tr>
                    <td>3212</td>
                    <td>KABUPATEN INDRAMAYU</td>
                    <td>Belum Terverifikasi</td>
                    <td>1287954000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>3213</td>
                    <td>KABUPATEN SUBANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>1893000000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>3215</td>
                    <td>KABUPATEN KARAWANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>0</td>
                    <td>0</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>3216</td>
                    <td>KABUPATEN BEKASI</td>
                    <td>Belum Terverifikasi</td>
                    <td>32000000000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>3217</td>
                    <td>KABUPATEN BANDUNG BARAT</td>
                    <td>Belum Terverifikasi</td>
                    <td>7023797069</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>3271</td>
                    <td>KOTA BOGOR</td>
                    <td>Belum Terverifikasi</td>
                    <td>385030000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>3272</td>
                    <td>KOTA SUKABUMI</td>
                    <td>Belum Terverifikasi</td>
                    <td>5004436325</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>3273</td>
                    <td>KOTA BANDUNG</td>
                    <td>Belum Terverifikasi</td>
                    <td>&nbsp;</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>3274</td>
                    <td>KOTA CIREBON</td>
                    <td>Belum Terverifikasi</td>
                    <td>&nbsp;</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>3276</td>
                    <td>KOTA DEPOK</td>
                    <td>Belum Terverifikasi</td>
                    <td>2000000000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>3277</td>
                    <td>KOTA CIMAHI</td>
                    <td>Belum Terverifikasi</td>
                    <td>8634749300</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>3279</td>
                    <td>KOTA BANJAR</td>
                    <td>Belum Terverifikasi</td>
                    <td>2600000000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>3301</td>
                    <td>KABUPATEN CILACAP</td>
                    <td>Belum Terverifikasi</td>
                    <td>17052881900</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>3302</td>
                    <td>KABUPATEN BANYUMAS</td>
                    <td>Belum Terverifikasi</td>
                    <td>11082972000</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>3305</td>
                    <td>KABUPATEN KEBUMEN</td>
                    <td>Belum Terverifikasi</td>
                    <td>9239754000</td>
                    <td>1</td>
                    <td>8</td>
                    </tr>
                    <tr>
                    <td>3306</td>
                    <td>KABUPATEN PURWOREJO</td>
                    <td>Belum Terverifikasi</td>
                    <td>17253744000</td>
                    <td>1</td>
                    <td>9</td>
                    </tr>
                    <tr>
                    <td>3308</td>
                    <td>KABUPATEN MAGELANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>14002323605</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>3309</td>
                    <td>KABUPATEN BOYOLALI</td>
                    <td>Belum Terverifikasi</td>
                    <td>17157091000</td>
                    <td>1</td>
                    <td>7</td>
                    </tr>
                    <tr>
                    <td>3311</td>
                    <td>KABUPATEN SUKOHARJO</td>
                    <td>Belum Terverifikasi</td>
                    <td>&nbsp;</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>3314</td>
                    <td>KABUPATEN SRAGEN</td>
                    <td>Belum Terverifikasi</td>
                    <td>24095083700</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>3318</td>
                    <td>KABUPATEN PATI</td>
                    <td>Belum Terverifikasi</td>
                    <td>75000000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>3322</td>
                    <td>KABUPATEN SEMARANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>23771525000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>3325</td>
                    <td>KABUPATEN BATANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>8550862900</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>3326</td>
                    <td>KABUPATEN PEKALONGAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>15232861500</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>3329</td>
                    <td>KABUPATEN BREBES</td>
                    <td>Belum Terverifikasi</td>
                    <td>19820620000</td>
                    <td>1</td>
                    <td>16</td>
                    </tr>
                    <tr>
                    <td>3371</td>
                    <td>KOTA MAGELANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>5000000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>3372</td>
                    <td>KOTA SURAKARTA</td>
                    <td>Belum Terverifikasi</td>
                    <td>7603470000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>3373</td>
                    <td>KOTA SALATIGA</td>
                    <td>Belum Terverifikasi</td>
                    <td>1072017000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>3375</td>
                    <td>KOTA PEKALONGAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>4180531000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>3376</td>
                    <td>KOTA TEGAL</td>
                    <td>Belum Terverifikasi</td>
                    <td>100000000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>3402</td>
                    <td>KABUPATEN BANTUL</td>
                    <td>Belum Terverifikasi</td>
                    <td>1684064441</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>3404</td>
                    <td>KABUPATEN SLEMAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>24447798650</td>
                    <td>1</td>
                    <td>7</td>
                    </tr>
                    <tr>
                    <td>3501</td>
                    <td>KABUPATEN PACITAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>9890000000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>3502</td>
                    <td>KABUPATEN PONOROGO</td>
                    <td>Belum Terverifikasi</td>
                    <td>18580000000</td>
                    <td>1</td>
                    <td>7</td>
                    </tr>
                    <tr>
                    <td>3504</td>
                    <td>KABUPATEN TULUNGAGUNG</td>
                    <td>Belum Terverifikasi</td>
                    <td>16949900000</td>
                    <td>1</td>
                    <td>8</td>
                    </tr>
                    <tr>
                    <td>3505</td>
                    <td>KABUPATEN BLITAR</td>
                    <td>Belum Terverifikasi</td>
                    <td>6663336706</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>3506</td>
                    <td>KABUPATEN KEDIRI</td>
                    <td>Belum Terverifikasi</td>
                    <td>8900000000</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>3507</td>
                    <td>KABUPATEN MALANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>12100000000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>3508</td>
                    <td>KABUPATEN LUMAJANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>38500750000</td>
                    <td>1</td>
                    <td>10</td>
                    </tr>
                    <tr>
                    <td>3509</td>
                    <td>KABUPATEN JEMBER</td>
                    <td>Belum Terverifikasi</td>
                    <td>15500000000</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>3513</td>
                    <td>KABUPATEN PROBOLINGGO</td>
                    <td>Belum Terverifikasi</td>
                    <td>9950000000</td>
                    <td>1</td>
                    <td>7</td>
                    </tr>
                    <tr>
                    <td>3514</td>
                    <td>KABUPATEN PASURUAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>5211963000</td>
                    <td>1</td>
                    <td>12</td>
                    </tr>
                    <tr>
                    <td>3515</td>
                    <td>KABUPATEN SIDOARJO</td>
                    <td>Belum Terverifikasi</td>
                    <td>9331475000</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>3521</td>
                    <td>KABUPATEN NGAWI</td>
                    <td>Belum Terverifikasi</td>
                    <td>11006711650</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>3525</td>
                    <td>KABUPATEN GRESIK</td>
                    <td>Belum Terverifikasi</td>
                    <td>28959986858</td>
                    <td>1</td>
                    <td>7</td>
                    </tr>
                    <tr>
                    <td>3526</td>
                    <td>KABUPATEN BANGKALAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>24297500000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>3527</td>
                    <td>KABUPATEN SAMPANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>18833945996</td>
                    <td>1</td>
                    <td>7</td>
                    </tr>
                    <tr>
                    <td>3573</td>
                    <td>KOTA MALANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>1600000000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>3574</td>
                    <td>KOTA PROBOLINGGO</td>
                    <td>Belum Terverifikasi</td>
                    <td>4051012300</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>3575</td>
                    <td>KOTA PASURUAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>2400000000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>3578</td>
                    <td>KOTA SURABAYA</td>
                    <td>Belum Terverifikasi</td>
                    <td>0</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>3579</td>
                    <td>KOTA BATU</td>
                    <td>Belum Terverifikasi</td>
                    <td>13273342549</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>3601</td>
                    <td>KABUPATEN PANDEGLANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>3695000000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>3602</td>
                    <td>KABUPATEN LEBAK</td>
                    <td>Belum Terverifikasi</td>
                    <td>25903300280</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>3603</td>
                    <td>KABUPATEN TANGERANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>26150000000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>3671</td>
                    <td>KOTA TANGERANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>2695182862</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>5101</td>
                    <td>KABUPATEN JEMBRANA</td>
                    <td>Belum Terverifikasi</td>
                    <td>6170982800</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>5102</td>
                    <td>KABUPATEN TABANAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>5027514000</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>5103</td>
                    <td>KABUPATEN BADUNG</td>
                    <td>Belum Terverifikasi</td>
                    <td>4303380885</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>5104</td>
                    <td>KABUPATEN GIANYAR</td>
                    <td>Belum Terverifikasi</td>
                    <td>7686745500</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>5105</td>
                    <td>KABUPATEN KLUNGKUNG</td>
                    <td>Belum Terverifikasi</td>
                    <td>1587563720</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>5106</td>
                    <td>KABUPATEN BANGLI</td>
                    <td>Belum Terverifikasi</td>
                    <td>6495000000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>5107</td>
                    <td>KABUPATEN KARANGASEM</td>
                    <td>Belum Terverifikasi</td>
                    <td>16318395900</td>
                    <td>1</td>
                    <td>9</td>
                    </tr>
                    <tr>
                    <td>5108</td>
                    <td>KABUPATEN BULELENG</td>
                    <td>Belum Terverifikasi</td>
                    <td>10849992640</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>5171</td>
                    <td>KOTA DENPASAR</td>
                    <td>Belum Terverifikasi</td>
                    <td>2298513500</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>5201</td>
                    <td>KABUPATEN LOMBOK BARAT</td>
                    <td>Belum Terverifikasi</td>
                    <td>6033685000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>5202</td>
                    <td>KABUPATEN LOMBOK TENGAH</td>
                    <td>Belum Terverifikasi</td>
                    <td>9490612000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>5203</td>
                    <td>KABUPATEN LOMBOK TIMUR</td>
                    <td>Belum Terverifikasi</td>
                    <td>75807929000</td>
                    <td>1</td>
                    <td>8</td>
                    </tr>
                    <tr>
                    <td>5208</td>
                    <td>KABUPATEN LOMBOK UTARA</td>
                    <td>Belum Terverifikasi</td>
                    <td>10054260911</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>5271</td>
                    <td>KOTA MATARAM</td>
                    <td>Belum Terverifikasi</td>
                    <td>5400000000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>5307</td>
                    <td>KABUPATEN SIKKA</td>
                    <td>Belum Terverifikasi</td>
                    <td>16000000</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>5308</td>
                    <td>KABUPATEN ENDE</td>
                    <td>Belum Terverifikasi</td>
                    <td>21935000000</td>
                    <td>1</td>
                    <td>7</td>
                    </tr>
                    <tr>
                    <td>5309</td>
                    <td>KABUPATEN NGADA</td>
                    <td>Belum Terverifikasi</td>
                    <td>5881896400</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>5310</td>
                    <td>KABUPATEN MANGGARAI</td>
                    <td>Belum Terverifikasi</td>
                    <td>44048640534</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>5315</td>
                    <td>KABUPATEN MANGGARAI BARAT</td>
                    <td>Belum Terverifikasi</td>
                    <td>22144201100</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>5371</td>
                    <td>KOTA KUPANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>5093099130</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>6110</td>
                    <td>KABUPATEN MELAWI</td>
                    <td>Belum Terverifikasi</td>
                    <td>713700000000</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>6112</td>
                    <td>KABUPATEN KUBU RAYA</td>
                    <td>Belum Terverifikasi</td>
                    <td>7466506606</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>6171</td>
                    <td>KOTA PONTIANAK</td>
                    <td>Belum Terverifikasi</td>
                    <td>5376541000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>6172</td>
                    <td>KOTA SINGKAWANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>8896828226</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>6204</td>
                    <td>KABUPATEN BARITO SELATAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>8535362000</td>
                    <td>1</td>
                    <td>9</td>
                    </tr>
                    <tr>
                    <td>6205</td>
                    <td>KABUPATEN BARITO UTARA</td>
                    <td>Belum Terverifikasi</td>
                    <td>2052333474</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>6271</td>
                    <td>KOTA PALANGKARAYA</td>
                    <td>Belum Terverifikasi</td>
                    <td>6882165000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>6301</td>
                    <td>KABUPATEN TANAH LAUT</td>
                    <td>Belum Terverifikasi</td>
                    <td>15424415991</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>6303</td>
                    <td>KABUPATEN BANJAR</td>
                    <td>Belum Terverifikasi</td>
                    <td>9014620306</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>6305</td>
                    <td>KABUPATEN TAPIN</td>
                    <td>Belum Terverifikasi</td>
                    <td>3541000000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>6308</td>
                    <td>KABUPATEN HULU SUNGAI UTARA</td>
                    <td>Belum Terverifikasi</td>
                    <td>2461211250</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>6371</td>
                    <td>KOTA BANJARMASIN</td>
                    <td>Belum Terverifikasi</td>
                    <td>500000000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>6372</td>
                    <td>KOTA BANJARBARU</td>
                    <td>Belum Terverifikasi</td>
                    <td>11550000000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>6409</td>
                    <td>KABUPATEN PENAJAM PASER UTARA</td>
                    <td>Belum Terverifikasi</td>
                    <td>0</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>6471</td>
                    <td>KOTA BALIKPAPAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>10000000000</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>6472</td>
                    <td>KOTA SAMARINDA</td>
                    <td>Belum Terverifikasi</td>
                    <td>3800000000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>6474</td>
                    <td>KOTA BONTANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>8848502536</td>
                    <td>1</td>
                    <td>9</td>
                    </tr>
                    <tr>
                    <td>6501</td>
                    <td>KABUPATEN BULUNGAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>14535000000</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>6571</td>
                    <td>KOTA TARAKAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>16919657530</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>7101</td>
                    <td>KABUPATEN BOLAANG MONGONDOW</td>
                    <td>Belum Terverifikasi</td>
                    <td>11524268884</td>
                    <td>1</td>
                    <td>7</td>
                    </tr>
                    <tr>
                    <td>7102</td>
                    <td>KABUPATEN MINAHASA</td>
                    <td>Belum Terverifikasi</td>
                    <td>6455523400</td>
                    <td>1</td>
                    <td>10</td>
                    </tr>
                    <tr>
                    <td>7103</td>
                    <td>KABUPATEN KEPULAUAN SANGIHE</td>
                    <td>Belum Terverifikasi</td>
                    <td>12660741024</td>
                    <td>1</td>
                    <td>8</td>
                    </tr>
                    <tr>
                    <td>7106</td>
                    <td>KABUPATEN MINAHASA UTARA</td>
                    <td>Belum Terverifikasi</td>
                    <td>6534494000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>7172</td>
                    <td>KOTA BITUNG</td>
                    <td>Belum Terverifikasi</td>
                    <td>4020000000</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>7173</td>
                    <td>KOTA TOMOHON</td>
                    <td>Belum Terverifikasi</td>
                    <td>5860331425</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>7201</td>
                    <td>KABUPATEN BANGGAI</td>
                    <td>Belum Terverifikasi</td>
                    <td>41983194650</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>7202</td>
                    <td>KABUPATEN POSO</td>
                    <td>Belum Terverifikasi</td>
                    <td>1840774776</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>7203</td>
                    <td>KABUPATEN DONGGALA</td>
                    <td>Belum Terverifikasi</td>
                    <td>8812285448</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>7271</td>
                    <td>KOTA PALU</td>
                    <td>Belum Terverifikasi</td>
                    <td>5097420000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>7302</td>
                    <td>KABUPATEN BULUKUMBA</td>
                    <td>Belum Terverifikasi</td>
                    <td>29938539010</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>7303</td>
                    <td>KABUPATEN BANTAENG</td>
                    <td>Belum Terverifikasi</td>
                    <td>14299006469</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>7304</td>
                    <td>KABUPATEN JENEPONTO</td>
                    <td>Belum Terverifikasi</td>
                    <td>11667653000</td>
                    <td>1</td>
                    <td>9</td>
                    </tr>
                    <tr>
                    <td>7305</td>
                    <td>KABUPATEN TAKALAR</td>
                    <td>Belum Terverifikasi</td>
                    <td>4687123600</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>7306</td>
                    <td>KABUPATEN GOWA</td>
                    <td>Belum Terverifikasi</td>
                    <td>9662179230</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>7307</td>
                    <td>KABUPATEN SINJAI</td>
                    <td>Belum Terverifikasi</td>
                    <td>7595000000</td>
                    <td>1</td>
                    <td>10</td>
                    </tr>
                    <tr>
                    <td>7308</td>
                    <td>KABUPATEN BONE</td>
                    <td>Belum Terverifikasi</td>
                    <td>17145657765</td>
                    <td>1</td>
                    <td>13</td>
                    </tr>
                    <tr>
                    <td>7309</td>
                    <td>KABUPATEN MAROS</td>
                    <td>Belum Terverifikasi</td>
                    <td>26165000000</td>
                    <td>1</td>
                    <td>7</td>
                    </tr>
                    <tr>
                    <td>7310</td>
                    <td>KABUPATEN PANGKAJENE KEPULAUAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>9113584000</td>
                    <td>1</td>
                    <td>15</td>
                    </tr>
                    <tr>
                    <td>7312</td>
                    <td>KABUPATEN SOPPENG</td>
                    <td>Belum Terverifikasi</td>
                    <td>4329374000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>7313</td>
                    <td>KABUPATEN WAJO</td>
                    <td>Belum Terverifikasi</td>
                    <td>3009294820</td>
                    <td>1</td>
                    <td>8</td>
                    </tr>
                    <tr>
                    <td>7315</td>
                    <td>KABUPATEN PINRANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>7244397987</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>7316</td>
                    <td>KABUPATEN ENREKANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>8843950000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>7318</td>
                    <td>KABUPATEN TANA TORAJA</td>
                    <td>Belum Terverifikasi</td>
                    <td>&nbsp;</td>
                    <td>1</td>
                    <td>7</td>
                    </tr>
                    <tr>
                    <td>7371</td>
                    <td>KOTA MAKASSAR</td>
                    <td>Belum Terverifikasi</td>
                    <td>28873812800</td>
                    <td>1</td>
                    <td>7</td>
                    </tr>
                    <tr>
                    <td>7372</td>
                    <td>KOTA PARE PARE</td>
                    <td>Belum Terverifikasi</td>
                    <td>20950000000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>7373</td>
                    <td>KOTA PALOPO</td>
                    <td>Belum Terverifikasi</td>
                    <td>6500000000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>7403</td>
                    <td>KABUPATEN MUNA</td>
                    <td>Belum Terverifikasi</td>
                    <td>30250000000</td>
                    <td>1</td>
                    <td>2</td>
                    </tr>
                    <tr>
                    <td>7471</td>
                    <td>KOTA KENDARI</td>
                    <td>Belum Terverifikasi</td>
                    <td>6661000000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>7472</td>
                    <td>KOTA BAU BAU</td>
                    <td>Belum Terverifikasi</td>
                    <td>8485814000</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>7501</td>
                    <td>KABUPATEN GORONTALO</td>
                    <td>Belum Terverifikasi</td>
                    <td>28877000000</td>
                    <td>1</td>
                    <td>6</td>
                    </tr>
                    <tr>
                    <td>7571</td>
                    <td>KOTA GORONTALO</td>
                    <td>Belum Terverifikasi</td>
                    <td>39046094270</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    <tr>
                    <td>7602</td>
                    <td>KABUPATEN MAMUJU</td>
                    <td>Belum Terverifikasi</td>
                    <td>8977520000</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>7604</td>
                    <td>KABUPATEN POLEWALI MANDAR</td>
                    <td>Belum Terverifikasi</td>
                    <td>20076332400</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>7605</td>
                    <td>KABUPATEN MAJENE</td>
                    <td>Belum Terverifikasi</td>
                    <td>127262049202.4</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>8101</td>
                    <td>KABUPATEN MALUKU TENGAH</td>
                    <td>Belum Terverifikasi</td>
                    <td>40646000000</td>
                    <td>1</td>
                    <td>5</td>
                    </tr>
                    <tr>
                    <td>8171</td>
                    <td>KOTA AMBON</td>
                    <td>Belum Terverifikasi</td>
                    <td>0</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>8271</td>
                    <td>KOTA TERNATE</td>
                    <td>Belum Terverifikasi</td>
                    <td>18357358250</td>
                    <td>1</td>
                    <td>4</td>
                    </tr>
                    <tr>
                    <td>9103</td>
                    <td>KABUPATEN JAYAPURA</td>
                    <td>Belum Terverifikasi</td>
                    <td>6238808000</td>
                    <td>1</td>
                    <td>1</td>
                    </tr>
                    <tr>
                    <td>9171</td>
                    <td>KOTA JAYAPURA</td>
                    <td>Belum Terverifikasi</td>
                    <td>2300000000</td>
                    <td>1</td>
                    <td>3</td>
                    </tr>
                    </tbody>
                    </table> 
            
            
            </div>
        </div>
    </div>
</div> 

@stop
@section('js')

@endsection