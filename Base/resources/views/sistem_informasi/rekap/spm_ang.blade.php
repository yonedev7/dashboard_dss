@extends('adminlte::page')

@section('content_header')
@stop
@section('content')
<div class="container" id="rekap-app">
    <div class="card">
        <div class="card-body">
            <p class="mb-0">Diperbarui</p>
            <p><span class="badge badge-warning">{{$date[$index]}}</span></p>
            <h3>Anggaran SPM - 2022</h3>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <div class="card bg-dark">
                        <div class="card-body">
                            <h3><b>Data Terinput</b></h3>
                            <p>40 Pemda</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card bg-secondary">
                        <div class="card-body">
                            <h3><b>Data Terverifikasi</b></h3>
                            <p>0 Pemda</p>

        
                        </div>
                    </div>
                </div>
            </div>
            <div class="">
                <table class="table table-bordered">
                    <tbody>
                    <tr>
                    <td width="87">Kodepemda</td>
                    <td width="87">Nama Pemda</td>
                    <td width="87">Status Data</td>
                    <td width="217">Anggaran SPM Pembangunan Baru</td>
                    <td width="169">Anggaran SPM Penambahan</td>
                    <td width="125">Anggaran SMP Rehabilitasi</td>
                    </tr>
                    <tr>
                    <td>7308</td>
                    <td>KABUPATEN BONE</td>
                    <td>Belum Terverifikasi</td>
                    <td>00.00</td>
                    <td>9329969500</td>
                    <td>2408860600</td>
                    </tr>
                    <tr>
                    <td>1571</td>
                    <td>KOTA JAMBI</td>
                    <td>Belum Terverifikasi</td>
                    <td>3600000000</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>7304</td>
                    <td>KABUPATEN JENEPONTO</td>
                    <td>Belum Terverifikasi</td>
                    <td>00.00</td>
                    <td>2000000000</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>7101</td>
                    <td>KABUPATEN BOLAANG MONGONDOW</td>
                    <td>Belum Terverifikasi</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>3207</td>
                    <td>KABUPATEN CIAMIS</td>
                    <td>Belum Terverifikasi</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>3309</td>
                    <td>KABUPATEN BOYOLALI</td>
                    <td>Belum Terverifikasi</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>3305</td>
                    <td>KABUPATEN KEBUMEN</td>
                    <td>Belum Terverifikasi</td>
                    <td>00.00</td>
                    <td>6154250000</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>3509</td>
                    <td>KABUPATEN JEMBER</td>
                    <td>Belum Terverifikasi</td>
                    <td>19298096036</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>7306</td>
                    <td>KABUPATEN GOWA</td>
                    <td>Belum Terverifikasi</td>
                    <td>00.00</td>
                    <td>22395000000</td>
                    <td>1220670000</td>
                    </tr>
                    <tr>
                    <td>1501</td>
                    <td>KABUPATEN KERINCI</td>
                    <td>Belum Terverifikasi</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>1116</td>
                    <td>KABUPATEN ACEH TAMIANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>14300700000</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>3209</td>
                    <td>KABUPATEN CIREBON</td>
                    <td>Belum Terverifikasi</td>
                    <td>721858300</td>
                    <td>8000000000</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>9202</td>
                    <td>KABUPATEN MANOKWARI</td>
                    <td>Belum Terverifikasi</td>
                    <td>00.00</td>
                    <td>16500000000</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>3310</td>
                    <td>KABUPATEN KLATEN</td>
                    <td>Belum Terverifikasi</td>
                    <td>1424500000</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>3217</td>
                    <td>KABUPATEN BANDUNG BARAT</td>
                    <td>Belum Terverifikasi</td>
                    <td>8466119500</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>7303</td>
                    <td>KABUPATEN BANTAENG</td>
                    <td>Belum Terverifikasi</td>
                    <td>53573474517</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>7501</td>
                    <td>KABUPATEN GORONTALO</td>
                    <td>Belum Terverifikasi</td>
                    <td>3370000000</td>
                    <td>1150000000</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>7312</td>
                    <td>KABUPATEN SOPPENG</td>
                    <td>Belum Terverifikasi</td>
                    <td>378752000</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>3579</td>
                    <td>KOTA BATU</td>
                    <td>Belum Terverifikasi</td>
                    <td>5540000000</td>
                    <td>12650000000</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>1209</td>
                    <td>KABUPATEN ASAHAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>20162789998</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>7571</td>
                    <td>KOTA GORONTALO</td>
                    <td>Belum Terverifikasi</td>
                    <td>1689999790</td>
                    <td>9378999720</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>7316</td>
                    <td>KABUPATEN ENREKANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>1000000000</td>
                    <td>2820700000</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>5271</td>
                    <td>KOTA MATARAM</td>
                    <td>Belum Terverifikasi</td>
                    <td>00.00</td>
                    <td>2159268078</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>1771</td>
                    <td>KOTA BENGKULU</td>
                    <td>Belum Terverifikasi</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>6303</td>
                    <td>KABUPATEN BANJAR</td>
                    <td>Belum Terverifikasi</td>
                    <td>30357364000</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>7311</td>
                    <td>KABUPATEN BARRU</td>
                    <td>Belum Terverifikasi</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>3325</td>
                    <td>KABUPATEN BATANG</td>
                    <td>Belum Terverifikasi</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>9103</td>
                    <td>KABUPATEN JAYAPURA</td>
                    <td>Belum Terverifikasi</td>
                    <td>700000000</td>
                    <td>4200000000</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>5171</td>
                    <td>KOTA DENPASAR</td>
                    <td>Belum Terverifikasi</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>8171</td>
                    <td>KOTA AMBON</td>
                    <td>Belum Terverifikasi</td>
                    <td>41550000000</td>
                    <td>45503242000</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>5108</td>
                    <td>KABUPATEN BULELENG</td>
                    <td>Belum Terverifikasi</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>5106</td>
                    <td>KABUPATEN BANGLI</td>
                    <td>Belum Terverifikasi</td>
                    <td>300000000</td>
                    <td>300000000</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>3505</td>
                    <td>KABUPATEN BLITAR</td>
                    <td>Belum Terverifikasi</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>6501</td>
                    <td>KABUPATEN BULUNGAN</td>
                    <td>Belum Terverifikasi</td>
                    <td>3,79E+11</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>7201</td>
                    <td>KABUPATEN BANGGAI</td>
                    <td>Belum Terverifikasi</td>
                    <td>8000000000</td>
                    <td>4000000000</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>9171</td>
                    <td>KOTA JAYAPURA</td>
                    <td>Belum Terverifikasi</td>
                    <td>500000000</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>6271</td>
                    <td>KOTA PALANGKARAYA</td>
                    <td>Belum Terverifikasi</td>
                    <td>1000000000</td>
                    <td>1000000000</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>7103</td>
                    <td>KABUPATEN KEPULAUAN SANGIHE</td>
                    <td>Belum Terverifikasi</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>7309</td>
                    <td>KABUPATEN MAROS</td>
                    <td>Belum Terverifikasi</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    <td>00.00</td>
                    </tr>
                    <tr>
                    <td>1106</td>
                    <td>KABUPATEN ACEH BESAR</td>
                    <td>Belum Terverifikasi</td>
                    <td>4085000000</td>
                    <td>4150000000</td>
                    <td>00.00</td>
                    </tr>
                    </tbody>
                    </table>
            </div>
        </div>
    </div>
</div> 

@stop
@section('js')

@endsection