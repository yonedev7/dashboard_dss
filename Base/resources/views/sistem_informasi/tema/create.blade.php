@extends('adminlte::page')

@section('content_header')
<h3 class="">Tambah Menu </b></h3>
    
@stop

@section('content')
<form action="{{route('dash.si.menu.store',['tahun'=>$Stahun])}}" method="post">
    @csrf
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Nama Menu</label>
                        <input type="text" class="form-control" name="name" required> 
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Group Menu</label>
                        <select name="menu_group" class="form-control" id="">
                            <option value="UTAMA">UTAMA</option>
                            <option value="KPI-BANGDA">KPI-BANGDA</option>
                            <option value="LAINYA">LAINYA</option>
    
                        </select>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <textarea name="keterangan" class="form-control" id="" cols="30" rows="10"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <button class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>
@stop