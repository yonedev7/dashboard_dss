@extends('adminlte::page')

@section('content_header')
    
@stop

@section('content')
<form action="{{route('dash.si.menu.update',['tahun'=>$Stahun,'id_data'=>$data->id])}}" method="post">
    @csrf
    @method('PUT')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Nama Menu</label>
                        <input type="text" class="form-control" name="name" value="{{$data->name}}" required> 
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Group Menu</label>
                        <select name="menu_group" class="form-control" id="">
                            <option value="UTAMA" {{$data->menu_group=='UTAMA'?'selected':""}}>UTAMA</option>
                            <option value="KPI-BANGDA" {{$data->menu_group=='KPI-BANGDA'?'selected':""}}>KPI-BANGDA</option>
                            <option value="LAINYA" {{$data->menu_group=='LAINYA'?'selected':""}}>LAINYA</option>
    
                        </select>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <textarea name="keterangan" class="form-control" id="" cols="30" rows="10"><?=$data->keterangan?></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <button class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>
@stop