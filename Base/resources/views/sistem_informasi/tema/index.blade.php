@extends('adminlte::page')

@section('content_header')
<h3 class="">Menu </b></h3>
    
@stop

@section('content')
<div id="app_{{$SpageId}}" class="content">
    <div class="btn-group mb-2 ml-4">
    <a href="{{route('dash.si.menu.create',['tahun'=>$Stahun])}}" class="btn btn-success">Tambah</a>

    </div>
    <div class="card" data-app>
        <div class="card-body">
              <ul class="list-group">
                  <li  v-for="item in data" class="list-group-item" >@{{item.name}} | @{{item.menu_group}}  <div class="btn btn-group">
                    <a v-bind:href="('{{route('dash.si.menu.detail',['tahun'=>$Stahun,'id_data'=>'xxx'])}}').replace('xxx',item.id)" class="btn  btn-sm btn-primary"><i class="fa fa-arrow-right"></i></a>  
                    <button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>  
                </div></li>
              </ul>
        </div>
    </div>
   
</div>

@stop


@section('js')
<script>
    var app_{{$SpageId}}=new Vue({
        el:'#app_{{$SpageId}}',
        data:{
            data:<?=json_encode($data)?>,
            table:null
        }
    });
</script>
@stop