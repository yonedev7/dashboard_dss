@extends('adminlte::page')

@section('content_header')
    <h3><b>Timeline DSS</b> <small>{{$date->format('d F Y')}}</small></h3>

@stop


@section('content')
<div id="timeline">
    <div class="card" >
        <div class="card-body">
            <button class="btn btn-primary" @click="show_modal_add()"><i class="fa fa-plus"></i> Series</button>
            <hr>
    
          
            <template v-if="loader==false">
                <charts :constructor-type="'ganttChart'" :options="chartOptions"></charts>
            </template>
            <template v-if="loader==true">
                <div class="text-center">
                    <div class="spinner-grow text-primary" role="status">
                        <span class="sr-only">Loading...</span>
                      </div>
                      <div class="spinner-grow text-secondary" role="status">
                        <span class="sr-only">Loading...</span>
                      </div>
                      <div class="spinner-grow text-success" role="status">
                        <span class="sr-only">Loading...</span>
                      </div>
                      <div class="spinner-grow text-danger" role="status">
                        <span class="sr-only">Loading...</span>
                      </div>
                      <div class="spinner-grow text-warning" role="status">
                        <span class="sr-only">Loading...</span>
                      </div>
                      <div class="spinner-grow text-info" role="status">
                        <span class="sr-only">Loading...</span>
                      </div>
                </div>
            </template>
                
        </div>
    </div>
    
    <div class="modal" tabindex="-1" role="dialog" id="modal-add-series">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Tambah Series</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="">Judul Seris</label>
                    <input type="text" class="form-control" v-model="modal_series.kegiatan">
                </div>
                <div class="form-group">
                    <label for="">Mulai</label>
                    <input type="date" class="form-control" v-model="modal_series.start">
                </div>
                <div class="form-group">
                    <label for="">Selesai</label>
                    <input type="date" class="form-control" v-model="modal_series.end">
                </div>
                <div class="form-group">
                    <label for="">Keterangan</label>
                   <textarea name="" v-model="modal_series.keterangan_tugas" class="form-control" id="" cols="10" rows="10"></textarea>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" v-if="(modal_series.kegiatan&&modal_series.start)&&modal_series.end" class="btn btn-primary" @click="save_seris()">Tambah</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

      <div class="modal modal-primary fade" tabindex="-1" role="dialog" id="modal-detail-data">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title"><b>@{{modal_data.name_series}}</b></h5>
              <button type="button" v-if="(modal_data.name&&modal_data.start)&&modal_data.end" class="btn btn-success" @click="add_data(modal_data.link_add_child)">Tambah Turunan</button>

              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="">Tugas</label>
                    <input type="text" class="form-control" v-model="modal_data.name">
                </div>
                <div class="form-group">
                    <label for="">Mulai</label>
                    <input type="date" class="form-control" v-model="modal_data.start">
                </div>
                <div class="form-group">
                    <label for="">Selesai</label>
                    <input type="date" class="form-control" v-model="modal_data.end">
                </div>
             
                
                <div class="form-group">
                    <label for="">Keterangan Tugas</label>
                   <textarea name="" v-model="modal_data.keterangan_tugas" class="form-control" id="" cols="10" rows="10"></textarea>
                </div>
                <hr>
                <div class="form-group">
                    <label for="">Persentase Pelaksanaan %</label>
                    <input type="number" class="form-control" min="0" max="100" v-model="modal_data.completed">
                </div>
                <div class="form-group">
                    <label for="">Catatan  Pelaksanaan</label>
                   <textarea name="" v-model="modal_data.catatan_pelaksanaan" class="form-control" id="" cols="10" rows="10"></textarea>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" v-if="(modal_data.name&&modal_data.start)&&modal_data.end" class="btn btn-success" @click="add_data(modal_data.link_add_child)">Tambah Turunan</button>
              <button type="button" v-if="(modal_data.name&&modal_data.start)&&modal_data.end" class="btn btn-primary" @click="update_data(modal_data.link_update)">Update</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

      <div class="modal" tabindex="-1" role="dialog" id="modal-data-add">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">@{{modal_data_add.parent_name}} - @{{modal_data_add.name_series}}</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="">Tugas</label>
                    <input type="text" class="form-control" v-model="modal_data_add.name">
                </div>
                <div class="form-group">
                    <label for="">Mulai</label>
                    <input type="date" class="form-control" v-model="modal_data_add.start">
                </div>
                <div class="form-group">
                    <label for="">Selesai</label>
                    <input type="date" class="form-control" v-model="modal_data_add.end">
                </div>
                <div class="form-group">
                    <label for="">Keterangan</label>
                   <textarea name="" v-model="modal_data_add.keterangan_tugas" class="form-control" id="" cols="10" rows="10"></textarea>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" v-if="(modal_data_add.name&&modal_data_add.start)&&modal_data_add.end" class="btn btn-primary" @click="store()">Tambah</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
</div>
@stop

@section('js')
<script>
    var app=new Vue({
        el:'#timeline',
        watch:{
            'modal_data.completed':function(val){
                if(val){
                    this.modal_data.completed_parse=this.modal_data.completed>0?(this.modal_data.completed/100):0;
                }
            }
        },
        methods:{
            formatDate:function(date) {
                var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();

                if (month.length < 2) 
                    month = '0' + month;
                if (day.length < 2) 
                    day = '0' + day;

                return [year, month, day].join('-');
            },
            add_data:function(path){
                this.modal_data_add.parent_name=this.modal_data.name;
                this.modal_data_add.name_series=this.modal_data.name_series;
                this.modal_data_add.id_parent=this.modal_data.id;
                this.modal_data_add.path=path;
                $('.modal').modal('hide');
                setTimeout(() => {
                     $('#modal-data-add').modal();
                }, 500);
            },
            store:function(){
                req_ajax.post(this.modal_data_add.path,{
                    kegiatan:this.modal_data_add.name,
                    id_parent:this.modal_data_add.id_parent,
                    start:this.modal_data_add.start,
                    end:this.modal_data_add.end,
                    keterangan_tugas:this.modal_data_add.keterangan_tugas,
                }).then((res)=>{
                    $('.modal').modal('hide');
                    app.load();
                });
            },
            update_data:function(path){
                var data={
                    kegiatan:this.modal_data.name,
                    start:this.modal_data.start,
                    end:this.modal_data.end,
                    keterangan_tugas:this.modal_data.keterangan_tugas,
                    catatan_pelaksanaan:this.modal_data.catatan_pelaksanaan,
                    persentase_pelaksanaan:this.modal_data.completed_parse
                }
                req_ajax.post(path,data).then((res)=>{
                    $('.modal').modal('hide');
                    app.load();
                });
            },
            show_modal_add_data:function(){
                $('#modal-add-data').modal();
            },
            show_modal_add:function(){
                $('#modal-add-series').modal();
            },
            show_modal_detail_data:function(){
                $('#modal-detail-data').modal();
            },
            load:function(){
                this.loader=true;
                req_ajax.post('{{route('api-web.public.timeline.load',['tahun'=>$Stahun])}}').then((res)=>{
                   if(res.data){
                       var series=res.data;
                       for(var i in series){
                           for(var d in series[i].data){
                           series[i].data[d].completed=  parseFloat(series[i].data[d].completed);
                            series[i].data[d].start= new Date(series[i].data[d].start).getTime();
                            series[i].data[d].end=  new Date(series[i].data[d].end).getTime();


                           }

                       }
                       app.chartOptions.series=series;
                       this.loader=false
                   }
                });
            },
            save_seris:function(){
                req_ajax.post('{{route('api-web.public.timeline.store',['tahun'=>$Stahun])}}',this.modal_series).then((res)=>{
                    console.log(res);
                    app.load();

                });
                $('.modal').modal('hide');
            }
        },
        data:{
            loader:true,
            modal_series:{
                kegiatan:'',
                start:null,
                end:null,
                keterangan_tugas:null
            },
            modal_data:{
                'name_series':'',
                'name':'',
                'id':0,
                'catatan_pelaksanaan':'',
                'kode':'',
                'id_patent':0,
                'completed':0,
                'completed_parse':0,
                'start':null,
                'end':null,
                'keterangan_tugas':'',
                'link_update':'',
                'link_delete':'',
                'link_add_child':'',
            },
            modal_data_add:{
                'id_parent':0,
                'parent_name':'',
                'name_series':'',

                'start':null,
                'end':null,
                'keterangan_tugas':'',
                'link_add_child':'',
            },
            chartOptions:{
                chart: {
                    events: {
                    load() {
                        let chart = this;
                        chart.xAxis[0].setExtremes( new Date('{{$date->startOfWeek()}}').getTime(),new Date('{{$date->endOfWeek()}}').getTime())
                    }
                    }
                },
                title: {
                        text: 'Timeline DSS {{$Stahun}}'
                    },

                    yAxis: {
                        // uniqueNames: true
                    },

                    navigator: {
                        enabled: true,
                        liveRedraw: true,
                        series: {
                            type: 'gantt',
                            pointPlacement: 0.5,
                            pointPadding: 0.25
                        },
                        yAxis: {
                            min: 0,
                            max: 3,
                            reversed: true,
                            categories: []
                        }
                    },
                    scrollbar: {
                        enabled: true
                    },
                    rangeSelector: {
                        enabled: true,
                        selected: 0
                    },
                    plotOptions:{
                        series:{
                            point:{
                                events:{
                                    click:function(e,d){
                                        console.log(e,d,this);
                                        data={
                                            'name_series':this.series.name,
                                            'name':this.name,
                                            'id':this.id_data,
                                            'id_parent':this.id_parent,
                                            'start':app.formatDate(this.start),
                                            'end':app.formatDate(this.end),
                                            'catatan_pelaksanaan':this.catatan_pelaksanaan,
                                            'completed':((this.completed||0)*100),
                                            'completed_parse':this.completed||0,
                                            'kode':this.kode,
                                            'keterangan_tugas':this.keterangan_tugas,
                                            'link_update':this.link_update,
                                            'link_delete':this.link_delete,
                                            'link_add_child':this.link_add_child,
                                        }


                                        app.modal_data=data;
                                        app.show_modal_detail_data();

                                    }
                                }
                            }
                        }
                    },
                    series: []
                }


            }
    });
    app.load();
</script>

@stop