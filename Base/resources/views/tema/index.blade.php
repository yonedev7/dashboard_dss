@extends('adminlte::page')


@section('content')
<div id="app_{{$SpageId}}">
<div class="parallax"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
    <div class="container">
        <section >
            <h3><b>@{{tema.name}} - {{$Stahun}}</b></h3>
            <p><b>@{{com_keterangan}}</b></p>
           <div class="row ">
            <div class=" col-md-12 " >
                <div class="input-group mb-3 input-group-lg" style="z-index:99;position: absolute; width:50%; bottom:-50px; right:0px;">
                    <input type="text" v-model="q" class="form-control input-xl" placeholder="Cari Dataset" aria-label="Cari Dataset" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                    <button @click="get_data" class="btn btn-primary btn-xl" type="button"><i class="fa fa-search"></i></button>
                    </div>
                </div>
        </div>
           </div>
        </section>
      
    </div>
   

    
</div>


<div class="container mt-3" >
    <nav aria-label="breadcrumb"  v-if="s_stack.length>1">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">History</li>
            <li v-for="(item,key) in s_stack" @click="q=item;get_data();"  v-if="item!=q" v-bind:class="'breadcrumb-item '+((key+1)==s_stack.length?'active':'')"><a href="javascript::void(0)" ><i class="fa fa-search"></i> @{{item}}</a></li>
        </ol>
      </nav>
    <div class="text-center p-2" v-if="datas.length==0 && load_process==false">
        Tidak Terdapat Data
        <hr>
    </div>
   

    <section v-if="datas.length>0">
        <div class="mb-1" style="width: 100%; min-height:30px;">
            <p class="float-sm-right"> @{{to+'/'+total}} Dataset Termuat</p>
        </div> 
        <div class="mb-3" v-for="(item,key) in datas"  style="background-image: url('{{url('assets/img/bgcard.png')}}'); position:relative; padding:15px; height:200px;  border-radius:10px; box-shadow:2px 3px 5px #ddd">
             
            <p><span class="badge" v-for="kat in kat_array(item.kategori)">@{{kat}}</span></p>
            
            <h4>@{{item.name}} </h4>
            <p >
                @{{r_tujuan(item.tujuan)}}
            <div class="btn-group" style="position: absolute; bottom:15px; left:15px;    ">
                <a v-bind:href="item.link_detail" class="btn btn-primary"><i class="fa fa-arrow-right"></i> Detail</a>
            </div>
        </div> 
        <div class="text-center p-2">
            <button class="btn btn-primary"  @click="load_more" v-if="load_process==false && current_page!=last_page"><i class="fa fa-arrow-down"></i> Load More</button>
            <div class="text-center" v-if="load_process==true">
                <div class="spinner-border text-navy" role="status">
                    <span class="sr-only">Loading...</span>
                  </div>
                <div class="spinner-border text-primary" role="status">
                    <span class="sr-only">Loading...</span>
                  </div>
                  <div class="spinner-border text-warning" role="status">
                    <span class="sr-only">Loading...</span>
                  </div>
                 
                  <div class="spinner-border text-danger" role="status">
                    <span class="sr-only">Loading...</span>
                  </div>
            </div>
        </div> 
        
    </section>
</div>

</div>
@stop

@section('js')
    <script>
        var app_{{$SpageId}}=new Vue({
            el:'#app_{{$SpageId}}',
            data:{
                tema:<?=json_encode($tema)?>,
                load_process:false,
                datas:[],
                q:'',
                current_page:1,
                last_page:1,
                to:0,
                total:0,
                s_stack:[

                ],
            },
            methods:{
                get_data:function(){
                    if(this.q){
                        if(this.s_stack.length>3){
                            this.s_stack.splice(0,1);
                        }
                        this.s_stack.push(this.q.replace(/\s+/g,' '));
                    }

                    this.load_process=true;
                    $.get('{{route('api-web.public.tema.ajax_dataset',['tahun'=>$Stahun,'id_tema'=>$tema->id])}}',{q:this.q,page:1},function(res){
                        app_{{$SpageId}}.datas=res.data;
                        app_{{$SpageId}}.load_process=false;
                        app_{{$SpageId}}.current_page=res.current_page;
                        app_{{$SpageId}}.last_page=res.last_page;
                        app_{{$SpageId}}.to=res.to;
                        app_{{$SpageId}}.total=res.total;


                    });
                },
                load_more:function(){
                    this.load_process=true;
                    if(this.load_process==false){
                        $.get('{{route('api-web.public.tema.ajax_dataset',['tahun'=>$Stahun,'id_tema'=>$tema->id])}}',{q:this.q,page:this.current_page+1},function(res){
                        app_{{$SpageId}}.load_process=false;
                        app_{{$SpageId}}.current_page=res.current_page;
                        app_{{$SpageId}}.last_page=res.last_page;
                        app_{{$SpageId}}.to=res.to;
                        app_{{$SpageId}}.total=res.total;
                        for(var i=0;i<res.data.length;i++){
                            app_{{$SpageId}}.datas.push(res.data[i]);
                        }
                       
                     });
                    }
                },
                r_tujuan:function(t){
                    return t.replace(/<br>/g,' ');
                },
                kat_array:function(kategori){
                    return JSON.parse(kategori||'[]');
                },
            },
            computed:{
               
                q_stack:function(){
                    this.s_stack=this.filter(function(value, index, self) {
                        return self.indexOf(value) === index;
                    });
                    console.log(this.s_stack,'ss');

                    return this.s_tack.reverse().slice(0,2);
                },
                com_keterangan:function(){
                    return (this.tema.keterangan||'').replace(/<br>/g,' ');
                   
                },
            
            }
        });
        app_{{$SpageId}}.get_data();

    </script>
@stop