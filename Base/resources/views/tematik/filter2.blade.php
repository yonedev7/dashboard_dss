<div class="row no-gutters d-flex align-items-stretch" style="z-index: 0;">
    
    <div class="col-4  {{strtoupper($basis)=='LONGLIST'?'bg-secondary efault':' bg-success'}} p-3 pt-5">
        <h4><b>LONGLIST</b></h4>
        <a  href="{{url()->current().'?basis=LONGLIST&meta='.(isset($_GET['meta'])?$_GET['meta']:'')}}" class="btn btn-warning text-dark"><i class="fa fa-arrow-down text-dark"></i></a>

    </div>
    <div class="col-4  {{strtoupper($basis)=='SORTLIST'?'bg-secondary':' bg-warning'}} p-3 pt-5">
        <h4><b>SORTLIST</b></h4>
        <a  href="{{url()->current().'?basis=SORTLIST&meta='.(isset($_GET['meta'])?$_GET['meta']:'')}}" class="btn btn-primary text-white"><i class="fa fa-arrow-down text-white"></i></a>
    </div>
    <div class="col-4  bg-primary p-3 pt-5" >

    </div>
 
</div>