@extends('adminlte::page')


@section('content')
<div class="parallax"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
    <div class="container">
        <section>
            <h3><b>{{$menus['title']}} - {{$Stahun}}</b></h3>
            <p>
                <b></b>
            </p>
        </section>
    
    </div>
</div>
<div class="container">
    @foreach($menus['submenu'] as $key=>$item)
    <div class="mb-3" style="background-image: url({{url('assets/img/bgcard.png')}}); position: relative; padding: 15px; height: 200px; border-radius: 10px; box-shadow: rgb(221, 221, 221) 2px 3px 5px;">
     <h4>{{$item['title']}} </h4> 
        
    </p><div class="btn-group" style="position: absolute; bottom: 15px; left: 15px;">
        <a href="{{$item['link']?route($item['link'],['tahun'=>$Stahun,"meta"=>base64_encode('tematik-'.$temaindex.'-'.$key)]):null}}" class="btn btn-primary">
            <i class="fa fa-arrow-right"></i> Detail</a></div></div>

    @endforeach
</div>
{{-- <div class="row  mt-3 justify-content-md-center">
    @foreach ($menus['sub'] as $key=>$item)
    <div class="col-4">
        <div class="info-box">
            <span class="info-box-icon bg-success"><i><b>{{$key+1}}</b> </i></span>
    
            <div class="info-box-content">
              <span class="info-box-text"><b>{{$item['name']}}</b></span>
              <span class="info-box-number"></span>
            </div>
            <!-- /.info-box-content -->
          </div>
    </div>
    @endforeach
</div> --}}
{{-- <div class="row no-gutters" v-if="dataset.pusat!=1" style="z-index: 0;">
    <div class="col-4 bg-primary p-3 pt-5" >
        <h4><b>Nasional</b></h4>
        <a href="" class="btn btn-success"><i class="fa fa-arrow-down"></i></a>
    </div>
    <div class="col-4 bg-success p-3 pt-5">
        <h4><b>Long List NUWSP</b></h4>
        <a  href=""class="btn btn-warning text-dark"><i class="fa fa-arrow-down text-dark"></i></a>
    </div>
    <div class="col-4 bg-warning p-3 pt-5   ">
        <h4><b>Sort List NUWSP {{$Stahun}}</b></h4>
        <a href="" class="btn btn-primary "><i class="fa fa-arrow-down text-white"></i></a>
    </div>
</div> --}}



@stop

@section('js')
@stop