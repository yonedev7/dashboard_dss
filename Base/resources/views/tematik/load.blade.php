@extends('adminlte::page')


@section('content')
<div id="listing-data">
    <div class="parallax"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
        <div class="container">
            <section>
                <h3><b>@{{data.title}} - {{$Stahun}}</b></h3>
                <p>
                    <b>@{{data.keterangan}}</b>
                </p>
            </section>
        
        </div>
    </div>
    <div class="container pt-3">
        <div class="mb-3" v-for="item in data.submenu" style="background-image: url({{url('assets/img/bgcard.png')}}); position: relative; padding: 15px; height: 200px; border-radius: 10px; box-shadow: rgb(221, 221, 221) 2px 3px 5px;">
         <h4><b>@{{item.title}} @{{item.rekap.tahun>0?item.rekap.tahun:''}}</b></h4> 
            
        <p>@{{item.keterangan}}</p>
        <div class="" style="position: absolute; bottom: 15px; left: 15px;">
            <div class="spinner-border text-primary" v-if="!item.rekap.tahun" role="status">
                <span class="sr-only">Loading...</span>
            </div>
           <div class="d-flex">
            <a v-bind:href="item.link_render.replace('xxx',item.rekap.tahun)"  v-if="item.rekap.tahun"  class="btn btn-primary self-align-center text-white bg-primary">
                <i class="fa fa-arrow-right"></i> <br>Detail
            </a>
            <div class="d-flex bg-white p-1" v-if="item.rekap.tahun">
               
                <div class="p-1">
                    <p class="mb-0"><b>Nasional</b></p>
                    <p class="mb-0">Provinsi: @{{item.rekap.nasional.pemda_pro}} PEMDA, Kota/Kab: @{{item.rekap.nasional.pemda_kabkot}} PEMDA</p>
                </div>
                <div class="p-1">
                    <p class="mb-0"><b>Longlist</b></p>
                    <p class="mb-0">Provinsi: @{{item.rekap.longlist.pemda_pro}} PEMDA, Kota/Kab: @{{item.rekap.longlist.pemda_kabkot}} PEMDA</p>
                </div>
                <div class="p-1">
                    <p class="mb-0"><b>Sortlist {{$Stahun}}</b></p>
                    <p class="mb-0">Provinsi: @{{item.rekap.sortlist.pemda_pro}} PEMDA, Kota/Kab: @{{item.rekap.sortlist.pemda_kabkot}} PEMDA</p>
                </div>
            </div>

           </div>
                
           

        </div>
    
    </div>
</div>

@stop


@section('js')
<script>
    var app=new Vue({
        el:'#listing-data',
        data:{
            data:<?=json_encode($page_meta)?>,
           
            index_data:0,
            index_meta:<?=$temaindex?>,

        },
        created(){
                this.next_index_meta();
            },
        methods:{
            next_index_meta:function(){
              if(this.data.submenu[this.index_data]!=undefined){
                  this.loadRekapMeta(this.index_meta,this.index_data);
                  this.index_data+=1;
              }

            },
            loadRekapMeta:async function(index,indexsub){
                this.data['submenu'][indexsub]['rekap']['load']=true;
                req_ajax.post('{{route('api-web.public.data.rekap.load_rekap',['tahun'=>$Stahun])}}',{
                'table': this.data['submenu'][indexsub].table
                }).then((res)=>{
                app.data['submenu'][indexsub]['rekap']['load']=false;
                app.data['submenu'][indexsub]['rekap']=res.data;
                app.next_index_meta();
                });
            },
        }
    })
</script>

@stop