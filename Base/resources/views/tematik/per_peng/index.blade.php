@extends('adminlte::page')


@section('content')
<div class="" id="l1">
    <div class="row no-gutters">
        <div class="col-3">
            <div class="card">
                <div class="card-body" style="height: 505px;">
                    <h3><b>Indikator Kinerja</b></h3>
                    <hr>
                    <ul class="list-group">
                        <li class="list-group-item" v-for="item,key in indikator"><a @click="indikator_selected=indikator[key]" href="javascript:void(0)">@{{item.name}}</a></li>
                      
                      </ul>
                </div>
            </div>
        </div>
        <div class="col-9">
            <div class="card">
                <div class="card-header with-border">
                    <p>@{{indikator_selected.name}}</p>
                </div>
                <div class="card-body">
                    <div class="row">
                        
                        <div class="col-12">
                                <charts :options="chartOptions"></charts>

                        </div>
                    </div>
                </div>
             </div>
        </div>
    </div>

    
</div>

<div id="l2">
    <div class="row" v-if="show_box" >
        <div class="col-4"></div>
        <div class="col-4"></div>
        <div class="col-4"></div>
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">

                    <div class="col-9">
                        <charts :options="chartOptions"></charts>

                    </div>
                    <div class="col-3">
                          <charts :options="chartOptions2"></charts>
                    </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div id="post-2"></div>

@stop


@section('js')
<script>
    var app_l1=new Vue({
        el:'#l1',
        mounted:function(){
            if(true){
                this.indikator_selected=this.indikator[0];

            }
        },
        watch:{
            indikator_selected:function(val){
                series={
                    name:val.name,
                    type:'line',
                    data:[]
                },
                val.target.forEach(t1 => {
                        series.data.push({
                            name:t1.tahun,
                            y:t1.val,
                            yAxis:0,
                        });
                });

                this.chartOptions.series=[];
                this.chartOptions.yAxis=[{
                    title:{
                        text:val.satuan
                    }
                }];

                this.chartOptions.series.push(series);
                app_l2.show_box=false;
            }
        },
        data:{
            indikator:[
                {
                    'name':'Jumlah Perluasan SPAM SR yang terlayani SPAM (SR) - Pemerintah Daerah',
                    'satuan':'SR',
                    'target':[
                        {
                            tahun:2020,
                            val:300,
                        },
                        {
                            tahun:2021,
                            
                            val:400,
                        },
                        {
                            tahun:2022,

                            val:500,
                        },
                        {
                            tahun:2023,

                            val:600,
                        },
                        {
                            tahun:2024,

                            val:700,
                        }
                    ]
                },
                {
                    'name':'Jumlah Perluasan SPAM SR yang terlayani SPAM (SR) - Pemerintah Daerah melalui hibah',
                    'satuan':'SR',

                    'target':[
                        {
                            tahun:2020,
                            val:30,
                        },
                        {
                            tahun:2021,
                            
                            val:50,
                        },
                        {
                            tahun:2022,

                            val:70,
                        },
                        {
                            tahun:2023,

                            val:80,
                        },
                        {
                            tahun:2024,

                            val:100,
                        }
                    ]
                },{
                    name:'Jumlah rumah tangga dengan akses air minum Bukan Jaringan Perpipaan (RT)',
                    satuan:'RT',
                    'target':[
                        {
                            tahun:2020,
                            val:300,
                        },
                        {
                            tahun:2021,
                            
                            val:520,
                        },
                        {
                            tahun:2022,

                            val:710,
                        },
                        {
                            tahun:2023,

                            val:820,
                        },
                        {
                            tahun:2024,

                            val:180,
                        }
                    ]
                }
            ],
            indikator_selected:{},
            chartOptions:{
                title:{
                    text:'Indikator Kinerja Air Minum RPJMN 2020-2024',
                },
                subtitle:{
                    text:''
                },
                xAxis:{
                    type:'category'
                },
                
                plotOptions:{
                    series:{
                        point:{
                        events:{
                            click:function(){
                                app_l2.show_box=false;
                                app_l2.show_box=true;
                                app_l2.indikator_selected=app_l1.indikator_selected;
                                app_l2.tahun=parseInt(this.name);

                                app_l2.load();

                                setTimeout(() => {
                                    // console.log(document.getElementById('post-2'));
                                    document.getElementById('post-2').scrollIntoView(false);;
                                }, 500);

                            }
                        }
                    }
                    }
                },
                series:[]
            }
        }
    });

    var app_l2=new Vue({
        el:'#l2',
        watch:{
            show_box:function(val){
                if(val==true){
                   this.load()
                }
            }
        },
        methods:{
            load:function(){
                this.chartOptions.title.text='Distribusi Target '+this.indikator_selected.name;
                this.chartOptions.subtitle.text='Tahun '+this.tahun;
                for(var i in this.chartOptions.series[0].data){
                    this.chartOptions.series[0].data[i].y=parseInt(Math.random()*this.tahun);
                    
                };
                for(var i in this.chartOptions.series[1].data){
                    this.chartOptions.series[1].data[i].y=parseInt(Math.random()*this.tahun);
                    
                };
            }
        },
        data:{
            tahun:0,
            show_box:false,
            indikator_selected:{},
            chartOptions:{
                title:{
                    text:''
                },
                subtitle:{
                    text:''
                },
                xAxis:{
                    type:'category'
                },
                series:[
                    {
                        type:'column',
                        name:'Satuan SR',
                        data:[
                            {
                                y:40,
                                name:'Provins Aceh'
                            },
                            {
                                y:30,
                                name:'Kab. Aceh Selatan'
                            }
                        ]
                    },
                    {
                        type:'column',
                        name:'Satuan RT',
                        data:[
                            {
                                y:4,
                                name:'Provins Aceh'
                            },
                            {
                                y:9,
                                name:'Kab. Aceh Selatan'
                            },
                            {
                                y:30,
                                name:'Kab. Aceh Barat'
                            }
                        ]
                    }

                ]

            },
            chartOptions2:{
                title:{
                    text:''
                },
                subtitle:{
                    text:''
                },
                xAxis:{
                    type:'category'
                },
                series:[
                    {
                        type:'pie',
                        name:'Satuan SR',
                        data:[
                            {
                                y:40,
                                name:'SR'
                            },
                            {
                                y:30,
                                name:'L/D'
                            }
                        ]
                    },
                   

                ]

            }
        }
    });
</script>
@stop


