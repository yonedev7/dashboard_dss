@extends('adminlte::page')


@section('content')
<div class="parallax"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
    <div class="container">
        <section >
            <h3><b>PERENCANAAN DAERAH</b></h3>
            <p><b>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quaerat vitae, ea assumenda officiis quod repudiandae nostrum quos, inventore porro non illo vel obcaecati voluptatum, consequatur amet. Voluptates qui a alias.</b></p>
           
        </section>
      
    </div>
   
</div>
<div class="row no-gutters" style="z-index: 0;">
    <div class="col-4 {{$basis=='nasional'?'bg-secondary':' bg-primary'}} p-3 pt-5" >
        <h4><b>NASIONAL</b></h4>
        <a href="{{url()->current().'?basis=nasional'}}" class="btn btn-success"><i class="fa fa-arrow-down"></i></a>
    </div>
    <div class="col-4  {{$basis=='longlist'?'bg-secondary efault':' bg-success'}} p-3 pt-5">
        <h4><b>LONGLIST</b></h4>
        <a  href="{{url()->current().'?basis=longlist'}}" class="btn btn-warning text-dark"><i class="fa fa-arrow-down text-dark"></i></a>

    </div>
    <div class="col-4  {{$basis=='sortlist'?'bg-secondary':' bg-warning'}} p-3 pt-5">
        <h4><b>SORTLIST</b></h4>
        <a  href="{{url()->current().'?basis=sortlist'}}" class="btn btn-primary text-white"><i class="fa fa-arrow-down text-white"></i></a>

    </div>
</div>
<div id="filter">
<div>
    <div class="form-group p-3" >
      <div class="col-4">
        <label for="">Filter</label>
        <select name="" class="form-control" id="" v-model="filter">
            <option value="provinsi dan kota / kab">Semua</option>
            <option value="provinsi">Provinsi</option>
            <option value="kota">Kota</option>
            <option value="kab">Kab</option>
        </select>
      </div>
    </div>
</div>
<div style="min-height:20px;" class="bg-dark text-center p-1"><b>BASIS DATA {{strtoupper($basis)}} - (PEMDA @{{filter.toUpperCase()}}) TAHUN {{$StahunRoute}}</b></div>
</div>
<div class="" id="l1">
    <h5 class="text-center mt-2"><b>Rekapitulasi Data RKPD Air Minum</b></h5>
    <div class="row mt-1 d-flex align-items-stretch" >
        <div class="col-3 d-flex">
            <div class="info-box mb-3 flex-grow-1 ">
                <span class="info-box-icon bg-primary elevation-1"><i class="fas  fa-university"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Jumlah Pemda Provinsi</span>
                    <span class="info-box-number">@{{formatAngka(rekap_data.jumlah_pemda_provinsi)}} Pemda</span>
                </div>
                
            </div>
            
        </div>
        
        <div class="col-3  d-flex">
            <div class="info-box mb-3 flex-grow-1">
                <span class="info-box-icon bg-success elevation-1"><i class="fas  fa-university"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Jumlah Pemda Kabupaten Kota</span>
                    <span class="info-box-number">@{{formatAngka(rekap_data.jumlah_pemda_kabkota)}} Pemda</span>
                </div>
                
            </div>
            
        </div>
        <div class="col-3 d-flex">
            <div class="info-box mb-3 flex-grow-1">
                <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-credit-card"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Jumlah Pagu Indikatif</span>
                    <span class="info-box-number">Rp. @{{formatAngka(rekap_data.pagu)}}</span>
                </div>
                
            </div>
            
        </div>
        <div class="col-3 d-flex">
            <div class="info-box mb-3 flex-grow-1">
                <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-star"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Jumlah Kegiatan</span>
                    <span class="info-box-number">@{{formatAngka(rekap_data.jumlah_kegiatan)}} Kegiatan / @{{formatAngka(rekap_data.jumlah_subkegiatan)}} Sub Kegiatan</span>
                </div>
                
            </div>
            
        </div>
    </div>
    <div class="row no-gutters">
        <div class="col-12">
            <div class="card parallax mb-0"  style=" background-image: url('{{url('assets/img/back-t.png')}}');">
                <div class="card-body" >
                    <div class="row no-gutter">
                        <div class="col-6">
                            <charts :options="chart_per_urusan"></charts>
                        </div>
                        <div class="col-6">
                            <charts :options="chart_per_program"></charts>
                        </div>
                        <div class="col-12">
                            <charts  :options="chart_per_kegiatan"></charts>
                            <p class="mt-1 pt-1"><small><b>*Click Chart @{{chart_per_kegiatan.title.text}} Untuk melihat detail data</b></small></p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="height:20px;" class="bg-dark"></div>

<div id="l2">
    <div class="row">
        <div class="col-12" v-if="table.load==true">
            <p class="text-center mt-2"><b>Loading..</b></p>
        </div>
        <div class="col-12" v-if="table.name && table.load!=true">
          <div class="card">
              <div class="card-header">
                  <h5><b>@{{table.name}} (@{{table.kode}})</b></h5>
                  <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Sort By</label>
                            <select name="" class="form-control" id="" v-model="table.sorted">
                                <option value="pemda">PEMDA</option>
                                <option value="pagu">Pagu</option>
      
                            </select>
                        </div>
                      </div>
                  </div>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                 
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Kode</th>
                                <th>Nama Daerah</th>
                                <th>Nama SKPD</th>
                                <th>Pagu Indikatif Air Minum</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="item in table.data">
                                <td>@{{item.kodepemda}}</td>
                                <td>@{{item.namapemda}}</td>
                                <td>@{{(item.skpd)}}</td>
                                <td>Rp. @{{formatAngka(item.pagu)}}</td>
                                <td><button class="btn  btn-xs btn-success">Indikator</button></td>



                            </tr>
                        </tbody>
                    </table>
                </div>
              </div>
          </div>
        </div>
        <div class="col-12">
        </div>
    </div>
</div>
@stop

@section('js')

<script>

    var filter =new Vue({
        el:'#filter',
        data:{
            filter:'provinsi dan kota / kab'
        },
        watch:{
            filter:function(val){
                window.scrollToDom('#l1');

                l1.filter=val;
                l2.filter=val;
                l1.init();
                l1.chart_per_urusan.subtitle.text=l1.filter.toUpperCase()+' - Tahun {{$StahunRoute}}';
                l1.chart_per_kegiatan.subtitle.text=l1.filter.toUpperCase()+' - Tahun {{$StahunRoute}}';
                l1.chart_per_program.subtitle.text=l1.filter.toUpperCase()+' - Tahun {{$StahunRoute}}';
            }
        }
    })
    var l2=new Vue({
        el:'#l2',
        data:{
           table:{
               name:'',
               sorted:'pemda',
               kode:'',
               load:false,
               data:[]
           },
           filter:'',
            basis:'<?=$_GET['basis']??''?>',
        },
        watch:{
            "table.sorted":function(){
                this.detailDataGiat(this.table.kode,this.table.name);
            }
        },
        methods:{
            formatAngka(angka){
                return l1.formatAngka(angka);
            },
            detailDataGiat:function(kode,namasubgiat){
                this.table.load=true;
                window.scrollToDom('#l2');
                req_ajax.post('{{route('api-web.public.tema1.detail_subgiat_perpemda',['tahun'=>$StahunRoute])}}',{
                filter:this.filter,
                basis:this.basis,
                kode:kode,
                sort:this.table.sorted=='pagu'?['sum(pagu)','desc']:['mp.kodepemda','asc']
                }).then(function(res){
                    l2.table.name=namasubgiat;
                    l2.table.data=res.data;
                    l2.table.kode=kode;
                    l2.table.load=false;
                });
            },
        }
    })
    var l1=new Vue({
        el:'#l1',
        created:function(){
            this.init();
        },
        methods:{
          
            formatAngka:function(angka){
                var number_string = (angka+'').replace(/[^,\d]/g, '').toString(),
                split   		= number_string.split(','),
                sisa     		= split[0].length % 3,
                rupiah     		= split[0].substr(0, sisa),
                ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
    
                // tambahkan titik jika yang di input sudah menjadi angka ribuan
                if(ribuan){
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                }
    
                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                return rupiah;
            },
            init:function(){
                req_ajax.post('{{route('api-web.public.tema1.pagu_urusan',['tahun'=>$StahunRoute])}}',{
                filter:this.filter,
                basis:this.basis
                }).then(function(res){
                    l1.chart_per_urusan.series[0].data=res.data;
                });

                req_ajax.post('{{route('api-web.public.tema1.rekap',['tahun'=>$StahunRoute])}}',{
                filter:this.filter,
                basis:this.basis
                }).then(function(res){
                    l1.rekap_data=res.data;
                });

                req_ajax.post('{{route('api-web.public.tema1.pagu_program',['tahun'=>$StahunRoute])}}',{
                filter:this.filter,
                basis:this.basis
                }).then(function(res){
                    l1.chart_per_program.series[0].data=res.data;
                });
                req_ajax.post('{{route('api-web.public.tema1.pagu_giat_air',['tahun'=>$StahunRoute])}}',{
                filter:this.filter,
                basis:this.basis
                }).then(function(res){
                    l1.chart_per_kegiatan.series[0].data=res.data;
                });

                l2.table={
                    name:'',
                    data:[],
                    load:false,
                    kode:'',
                    sorted:'pemda'
                }
            }
        },
        data:{
            filter:'',
            basis:'<?=$_GET['basis']??''?>',
            table:{
                title:'',
                data:[]
            },
            rekap_data:{
                pagu:0,
                jumlah_pemda_provinsi:0,
                jumlah_pemda_kabkota:0,
                jumlah_kegiatan:0,
                jumlah_subkegiatan:0,
            },
            chart_per_program:{
                title:{
                    text:'Distribusi Pagu Indikatif Per-Program PU'
                },
                subtitle:{
                    text:'Tahun {{$StahunRoute}}'
                },
                chart:{
                    type:'pie',
                    backgroundColor:'#ffffffb8',
                    height:300,

                },
                xAxis:{
                    type:'category'
                },
                plotOptions: {
                        pie: {
                            dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>:<br>{point.percentage:.1f} %',
                            }
                        }
                 },
                series:[
                    {
   
                        name:'Pagu Indikatif',
                        data:[
                           
                        ]
                    }
                ]
            },
            chart_per_urusan:{
                title:{
                    text:'Distribusi Pagu Indikatif Per-Urusan'
                },
                subtitle:{
                    text:(window.filter.filter.toUpperCase())+' Tahun {{$StahunRoute}}'
                },
                chart:{
                    type:'pie',
                    backgroundColor:'#ffffffb8',
                    height:300,


                },plotOptions: {
                        pie: {
                            dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>:<br>{point.percentage:.1f} %',
                            }
                        }
                 },
                xAxis:{
                    type:'category'
                },
                series:[
                    {
                        name:'Pagu Indikatif',
                        data:[]
                    }
                ]
            },
            chart_per_kegiatan:{
                title:{
                    text:'Distribusi Pagu Indikatif Air Minum'
                },
                subtitle:{
                    text:'Nomenklatur Sub Kegiatan  {{$StahunRoute}}'
                },
                chart:{
                    type:'pie',
                    backgroundColor:'#ffffffb8',
                    minHight:400,

                },
                plotOptions: {
                        pie: {

                            dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>:<br>{point.percentage:.1f} %',
                            }
                        },
                    series:{
                        point:{
                            events:{
                                click:function(){
                                    l2.detailDataGiat(this.kodesubkegiatan,this.name);
                                }
                            },
                        },
                    },
                },
                xAxis:{
                    type:'category'
                },
                series:[
                    {
                        name:'Pagu Indikatif',
                        data:[
                            
                        ]
                    }
                ]
            }
        }
    });
</script>
@stop