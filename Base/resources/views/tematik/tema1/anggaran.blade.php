@extends('adminlte::page')
@push('css_push')
<style>
   /* *:fullscreen, *:-webkit-full-screen, *:-moz-full-screen {
    background-color:red!important;
    } */
   
</style>
@endpush

@section('content')
<div class="parallax"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
    <div class="container">
        <section >
            <h3><b>PENGANGARAN AIR MINUM DAERAH</b></h3>
            <p><b>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quaerat vitae, ea assumenda officiis quod repudiandae nostrum quos, inventore porro non illo vel obcaecati voluptatum, consequatur amet. Voluptates qui a alias.</b></p>
           
        </section>
      
    </div>
   
</div>
<div class="row no-gutters" style="z-index: 0;">
    <div class="col-4 {{$basis=='nasional'?'bg-secondary':' bg-primary'}} p-3 pt-5" >
        <h4><b>NASIONAL</b></h4>
        <a href="{{url()->current().'?basis=nasional'}}" class="btn btn-success"><i class="fa fa-arrow-down"></i></a>
    </div>
    <div class="col-4  {{$basis=='longlist'?'bg-secondary efault':' bg-success'}} p-3 pt-5">
        <h4><b>LONGLIST</b></h4>
        <a  href="{{url()->current().'?basis=longlist'}}" class="btn btn-warning text-dark"><i class="fa fa-arrow-down text-dark"></i></a>

    </div>
    <div class="col-4  {{$basis=='sortlist'?'bg-secondary':' bg-warning'}} p-3 pt-5">
        <h4><b>SORTLIST</b></h4>
        <a  href="{{url()->current().'?basis=sortlist'}}" class="btn btn-primary text-white"><i class="fa fa-arrow-down text-white"></i></a>

    </div>
</div>
<div id="filter">
<div>
    <div class="form-group p-3" >
      <div class="col-4">
        <label for="">Filter</label>
        <select name="" class="form-control" id="" v-model="filter">
            <option value="provinsi dan kota / kab">Semua</option>
            <option value="provinsi">Provinsi</option>
            <option value="kota">Kota</option>
            <option value="kab">Kab</option>
        </select>
      </div>
    </div>
</div>
<div style="min-height:20px;" class="bg-dark text-center p-1"><b>BASIS DATA {{strtoupper($basis)}} - (PEMDA @{{filter.toUpperCase()}}) TAHUN {{$StahunRoute}}</b></div>
</div>
<div class="" id="l1">
    <h5 class="text-center mt-2"><b>Rekapitulasi Data Anggaran Air Minum</b></h5>
    <div class="row mt-1 d-flex align-items-stretch" >
        <div class="col-3 d-flex">
            <div class="info-box mb-3 flex-grow-1 ">
                <span class="info-box-icon bg-primary elevation-1"><i class="fas  fa-university"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Jumlah Pemda Provinsi</span>
                    <span class="info-box-number">@{{formatAngka(rekap_data.jumlah_pemda_provinsi)}} Pemda</span>
                </div>
                
            </div>
            
        </div>
        
        <div class="col-3  d-flex">
            <div class="info-box mb-3 flex-grow-1">
                <span class="info-box-icon bg-success elevation-1"><i class="fas  fa-university"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Jumlah Pemda Kabupaten Kota</span>
                    <span class="info-box-number">@{{formatAngka(rekap_data.jumlah_pemda_kabkota)}} Pemda</span>
                </div>
                
            </div>
            
        </div>
        <div class="col-3 d-flex">
            <div class="info-box mb-3 flex-grow-1">
                <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-credit-card"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Jumlah Anggaran APBD Total</span>
                    <span class="info-box-number">Rp. @{{formatAngka(rekap_data.pagu)}}</span>
                </div>
                
            </div>
            
        </div>
        <div class="col-3 d-flex">
            <div class="info-box mb-3 flex-grow-1">
                <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-star"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Jumlah Kegiatan</span>
                    <span class="info-box-number">@{{formatAngka(rekap_data.jumlah_kegiatan)}} Kegiatan / @{{formatAngka(rekap_data.jumlah_subkegiatan)}} Sub Kegiatan</span>
                </div>
                
            </div>
            
        </div>
    </div>
    <div class="row no-gutters">
        <div class="col-12">
            <div class="card parallax mb-0"  style=" background-image: url('{{url('assets/img/back-t.png')}}');">
                <div class="card-body" >
                    <div class="row no-gutter">
                        
                        <div class="col-6">
                            <p class="p-2 mb-0 bg-dark"><b>Jumlah Program : @{{chart_per_program.series[0].data.length}}</b> </p>
                            <charts :options="chart_per_program"></charts>
                        </div>
                        <div class="col-6">
                            <p class="p-2 mb-0 bg-dark"><b>Jumlah Kegiatan : @{{chart_per_kegiatan.series[0].data.length}}</b> </p>
                            <charts :options="chart_per_kegiatan"></charts>
                        </div>
                        <div class="col-12">
                           <div class="row no-gutters d-flex align-items-stretch">
                            <div class="col-8 flex-column">
                                <p class="p-2 mb-0 bg-dark"><b>Jumlah Sub Kegiatan : @{{chart_per_subkegiatan.series[0].data.length}}</b> </p>
                                <charts  :options="chart_per_subkegiatan"></charts>
                            </div>
                            <div class="col-4 flex-column">
                                <p class="p-2 mb-0 bg-dark"><b>List Sub kegiatan</b> </p>
                                <div class="bg-white flex-column p-3" style="height:700px; overflow-y:scroll">
                                    
                                    <p class="text-dark font-weight-bold" v-for="item,k in chart_per_subkegiatan.series[0].data" ><a @click="load_from_list(item.kodesubkegiatan,item.name)" class="nav-link d-flex text-dark  align-items-stretch" href="javascript:void(0)" >
                                    <span class="mr-2">
                                        <span class=" rounded-circle p-2">@{{k+1}}.</span>
                                    </span>  @{{item.name}} (Rp. @{{formatAngka(item.y)}}) - @{{persentaseAngka(chart_per_subkegiatan.series[0].data.map(el=>{return el.y}),item.y)}}%</a></p>
                                </div>
                            </div>
                            <p class="mt-1 pt-1"><small><b>*Click Chart @{{chart_per_subkegiatan.title.text}} Untuk melihat detail data</b></small></p>


                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="height:20px;" class="bg-dark"></div>

<div id="l2">
    <div class="row">
        <div class="col-12" v-if="table.load==true">
            <p class="text-center mt-2"><b>Loading..</b></p>
        </div>
        <div class="col-12" v-if="table.name && table.load!=true">
          <div class="card">
              <div class="card-header">
                  <h5><b>@{{table.name}} (@{{table.kode}})</b></h5>
                  <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Sort By</label>
                            <select name="" class="form-control" id="" v-model="table.sorted">
                                <option value="pemda">PEMDA</option>
                                <option value="pagu">Total Anggaran</option>
      
                            </select>
                        </div>
                      </div>
                  </div>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                 
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Kode</th>
                                <th>Nama Daerah</th>
                                <th>Nama SKPD</th>
                                <th>Anggaran APBD Air Minum</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="item in table.data">
                                <td>@{{item.kodepemda}}</td>
                                <td>@{{item.namapemda}}</td>
                                <td>@{{(item.skpd)}}</td>
                                <td>Rp. @{{formatAngka(item.pagu)}}</td>
                                <td><button @click="detail_anggaran_daerah_l1(item.tahun,item.kodepemda,item.kode,{name:table.name,kode:table.kode})" class="btn  btn-sm btn-success">Detail</button></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
              </div>
          </div>
        </div>
        <div class="col-12">
        </div>
    </div>
    <div id="modal-detail" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog  modal-lg" role="document">
         
            <div class="modal-content">
                <div class="modal-header">
                    <h5><b>(@{{modal_detail.kodepemda}}) @{{modal_detail.namapemda}} {{$StahunRoute}}</b></h5>
                </div>
                <div class="modal-body">
                   <div class="row">
                    <div v-bind:class="modal_detail.l2.title.text?'col-6':'col-12'">
                        <charts :options="modal_detail.l1"></charts>
                       </div>
                       <div v-if="modal_detail.l2.title.text" v-bind:class="modal_detail.l2.title.text?'col-6':'col-12'">
                        <charts :options="modal_detail.l2"></charts>
    
                       </div>
                       <div  v-if="modal_detail.l3.title.text"  v-bind:class="modal_detail.l4.title.text?'col-6':'col-12'">
                        <charts :options="modal_detail.l3"></charts>
    
                       </div>
                       
                       <div  v-if="modal_detail.l4.title.text"   v-bind:class="modal_detail.l4.title.text?'col-6':'col-12'">
                        <charts :options="modal_detail.l4"></charts>
    
                       </div>
                   </div>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('js')

<script>

    var filter =new Vue({
        el:'#filter',
        data:{
            filter:'provinsi dan kota / kab'
        },
        watch:{
            filter:function(val){
                window.scrollToDom('#l1');

                l1.filter=val;
                l2.filter=val;
                l1.init();
                l1.chart_per_kegiatan.subtitle.text=l1.filter.toUpperCase()+' - Tahun {{$StahunRoute}}';
                l1.chart_per_subkegiatan.subtitle.text=l1.filter.toUpperCase()+' - Tahun {{$StahunRoute}}';
                l1.chart_per_program.subtitle.text=l1.filter.toUpperCase()+' - Tahun {{$StahunRoute}}';
            }
        }
    })
    var l2=new Vue({
        el:'#l2',
        data:{
           table:{
               name:'',
               sorted:'pemda',
               kode:'',
               load:false,
               data:[],
              
           },
           modal_detail:{
                namapemda:'',
                kodepemda:'',
                subkegiatan:{
                    kode:'',
                    nama:''
                },
                tahun:'',
                data:[],
                l1:{
                    title:{
                        text:''
                    },
                    subtitle:{
                        text:'Tahun {{$StahunRoute}}'
                    },
                    chart:{
                        type:'pie',
                        backgroundColor:'#ffffffb8',
                        height:300,

                    },
                    xAxis:{
                        type:'category'
                    },
                    plotOptions: {
                            pie: {
                                dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>:<br>{point.percentage:.2f} %',
                                }
                            },
                            series:{
                                point:{
                                    events:{
                                        click:function(){
                                            console.log(this);
                                            l2.detail_anggaran_daerah_l2(this.tahun,this.kodepemda,this.kodesub,this.kodeakun,this.name);
                                        }
                                    }    
                                },
                             },
                    },
                    series:[
                        {
                            name:'Anggaran',
                            data:[]
                        }
                        
                    ]
                },
                l2:{
                    kodeakun:'',

                    title:{
                        text:''
                    },
                    subtitle:{
                        text:'Tahun {{$StahunRoute}}'
                    },
                    chart:{
                        type:'pie',
                        backgroundColor:'#ffffffb8',
                        height:300,

                    },
                    xAxis:{
                        type:'category'
                    },
                    plotOptions: {
                            pie: {
                                dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>:<br>{point.percentage:.2f} %',
                                }
                            },
                            series:{
                                point:{
                                    events:{
                                        click:function(){
                                            l2.detail_anggaran_daerah_l3(this.tahun,this.kodepemda,this.kodesub,this.kodeakun,this.name);
                                        }
                                    },
                                },
                            }
                    },
                    series:[
                        {
                            name:'Anggaran',
                            data:[]
                        },
                        
                    ]
                },
                l3:{
                    kodeakun:'',

                    title:{
                        text:''
                    },
                    subtitle:{
                        text:'Tahun {{$StahunRoute}}'
                    },
                    chart:{
                        type:'pie',
                        backgroundColor:'#ffffffb8',
                        height:300,

                    },
                    xAxis:{
                        type:'category'
                    },
                    plotOptions: {
                            pie: {
                                dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>:<br>{point.percentage:.2f} %',
                                }
                            },
                            // series:{
                            //     point:{
                            //         events:{
                            //             click:function(){
                            //                 console.log(this);
                            //                 l2.detail_anggaran_daerah_l4(this.tahun,this.kodepemda,this.kodesub,this.kodeakun,this.name);
                            //             }
                            //         },
                            //      },
                            // }
                            
                    },
                    series:[
                        {
                            name:'Anggaran',
                            data:[]
                        }
                        
                    ]
                },
                l4:{
                    kodeakun:'',

                    title:{
                        text:''
                    },
                    subtitle:{
                        text:'Tahun {{$StahunRoute}}'
                    },
                    chart:{
                        type:'pie',
                        backgroundColor:'#ffffffb8',
                        height:700,

                    },
                    xAxis:{
                        type:'category'
                    },
                    plotOptions: {
                            pie: {
                                dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>:<br>{point.percentage:.2f} %',
                                }
                            }
                    },
                    series:[
                        {
                            name:'Anggaran',
                            data:[]
                        }
                        
                    ]
                }
            },
           filter:'',
            basis:'<?=$_GET['basis']??''?>',
        },
        watch:{
            "table.sorted":function(){
                this.detailDataGiat(this.table.kode,this.table.name);
            }
        },
        methods:{
            initmodal:function(){
                $('#modal-detail').modal();
            },
            detail_anggaran_daerah_l1:function(tahun,kodepemda,kode,parentdata){
                
                req_ajax.post('{{route('api-web.public.tema1.anggaran.ang_subgiat_pemda_l1',['tahun'=>$StahunRoute])}}',{
                filter:this.filter,
                basis:this.basis,
                kodepemda:kodepemda,
                kode:kode,
                }).then(function(res){
                    l2.modal_detail.namapemda=res.data.pemda.namapemda;
                    l2.modal_detail.subkegiatan.name=parentdata.name;
                    l2.modal_detail.subkegiatan.kode=parentdata.kode;
                    l2.modal_detail.kodepemda=res.data.pemda.kode||kode;
                    l2.modal_detail.l1.series[0].data=res.data.data.map((el)=>{
                        el.y=parseFloat(el.y);
                        return el;
                    });
                    l2.modal_detail.l1.title.text='('+l2.modal_detail.kodepemda+') '+l2.modal_detail.namapemda;
                    l2.modal_detail.l1.subtitle.text='('+l2.modal_detail.subkegiatan.kode+') '+l2.modal_detail.subkegiatan.name;
                    l2.modal_detail.l2.title.text='';
                    l2.modal_detail.l3.title.text='';
                    l2.modal_detail.l4.title.text='';
                    l2.modal_detail.l1.chart.height=300;
                    l2.modal_detail.l2.chart.height=300;
                    l2.modal_detail.l3.chart.height=300;



                    l2.initmodal();
                });
            },
            detail_anggaran_daerah_l2:function(tahun,kodepemda,kode,kodeakun,namaakun){
                
                req_ajax.post('{{route('api-web.public.tema1.anggaran.ang_subgiat_pemda_l2',['tahun'=>$StahunRoute])}}',{
                filter:this.filter,
                basis:this.basis,
                kodepemda:kodepemda,
                kode:kode,
                kodeakun:kodeakun
                }).then(function(res){
                    l2.modal_detail.l2.title.text='('+l2.modal_detail.kodepemda+') '+l2.modal_detail.namapemda+' ('+kodeakun+') '+namaakun;
                    l2.modal_detail.l2.subtitle.text='('+l2.modal_detail.subkegiatan.kode+') '+l2.modal_detail.subkegiatan.name;
                    l2.modal_detail.l2.series[0].data=res.data.data.map((el)=>{
                        el.y=parseFloat(el.y);
                        return el;
                    });

                    l2.modal_detail.l3.title.text='';
                    l2.modal_detail.l1.chart.height=400;
                    l2.modal_detail.l2.chart.height=400;
                    l2.modal_detail.l4.title.text='';



                });
            },
            detail_anggaran_daerah_l3:function(tahun,kodepemda,kode,kodeakun,namaakun){
                
                req_ajax.post('{{route('api-web.public.tema1.anggaran.ang_subgiat_pemda_l3',['tahun'=>$StahunRoute])}}',{
                filter:this.filter,
                basis:this.basis,
                kodepemda:kodepemda,
                kode:kode,
                kodeakun:kodeakun
                }).then(function(res){
                    l2.modal_detail.l3.title.text='('+l2.modal_detail.kodepemda+') '+l2.modal_detail.namapemda+' ('+kodeakun+') '+namaakun;
                    l2.modal_detail.l3.subtitle.text='('+l2.modal_detail.subkegiatan.kode+') '+l2.modal_detail.subkegiatan.name;
                    l2.modal_detail.l3.series[0].data=res.data.data.map((el)=>{
                        el.y=parseFloat(el.y);
                        return el;
                    });
                    l2.modal_detail.l3.chart.height=300;




                    l2.modal_detail.l4.title.text='';

                });
            },
            detail_anggaran_daerah_l4:function(tahun,kodepemda,kode,kodeakun,namaakun){

                req_ajax.post('{{route('api-web.public.tema1.anggaran.ang_subgiat_pemda_l4',['tahun'=>$StahunRoute])}}',{
                filter:this.filter,
                basis:this.basis,
                kodepemda:kodepemda,
                kode:kode,
                kodeakun:kodeakun

                }).then(function(res){
                    l2.modal_detail.l4.title.text='('+l2.modal_detail.kodepemda+') '+l2.modal_detail.namapemda+' ('+kodeakun+') '+namaakun;
                    l2.modal_detail.l4.subtitle.text='('+l2.modal_detail.subkegiatan.kode+') '+l2.modal_detail.subkegiatan.name;
                    l2.modal_detail.l4.series[0].data=res.data.data.map((el)=>{
                        el.y=parseFloat(el.y);
                        return el;
                    });
                    l2.modal_detail.l3.chart.height=400;
                    l2.modal_detail.l4.chart.height=400;


                    
                });
            },
            formatAngka(angka){
                return l1.formatAngka(angka);
            },
            detailDataGiat:function(kode,namasubgiat){
                console.log(kode,namasubgiat);
                this.table.load=true;
                window.scrollToDom('#l2');
                req_ajax.post('{{route('api-web.public.tema1.anggaran.ang_per_pemda_subgiat',['tahun'=>$StahunRoute])}}',{
                filter:this.filter,
                basis:this.basis,
                kode:kode,
                sort:this.table.sorted=='pagu'?['sum(d.total_harga)','desc']:['mp.kodepemda','asc']
                }).then(function(res){
                    l2.table.name=namasubgiat;
                    l2.table.data=res.data;
                    l2.table.kode=kode;
                    l2.table.load=false;
                });
            },
        }
    })
    var l1=new Vue({
        el:'#l1',
        created:function(){
            this.init();
        },
        methods:{
            persentaseAngka:function(arr,nilai){
                var total=0;
                arr.forEach(el=>{
                    total+=el
                });
                return ((nilai/total)*100).toFixed(2);
            },
            load_from_list:function(kode,name){
                window.l2.detailDataGiat(kode,name);
            },
            formatAngka:function(angka){
                var number_string = (angka+'').replace(/[^,\d]/g, '').toString(),
                split   		= number_string.split(','),
                sisa     		= split[0].length % 3,
                rupiah     		= split[0].substr(0, sisa),
                ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
    
                // tambahkan titik jika yang di input sudah menjadi angka ribuan
                if(ribuan){
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                }
    
                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                return rupiah;
            },
            init:function(){
                req_ajax.post('{{route('api-web.public.tema1.anggaran.ang_keg',['tahun'=>$StahunRoute])}}',{
                filter:this.filter,
                basis:this.basis
                }).then(function(res){
                    l1.chart_per_kegiatan.series[0].data=res.data;
                });

                req_ajax.post('{{route('api-web.public.tema1.anggaran.ang_rekap',['tahun'=>$StahunRoute])}}',{
                filter:this.filter,
                basis:this.basis
                }).then(function(res){
                    l1.rekap_data=res.data;
                });

                req_ajax.post('{{route('api-web.public.tema1.anggaran.ang_pro',['tahun'=>$StahunRoute])}}',{
                filter:this.filter,
                basis:this.basis
                }).then(function(res){
                    l1.chart_per_program.series[0].data=res.data;
                });
                req_ajax.post('{{route('api-web.public.tema1.anggaran.ang_subkeg',['tahun'=>$StahunRoute])}}',{
                filter:this.filter,
                basis:this.basis
                }).then(function(res){
                    l1.chart_per_subkegiatan.series[0].data=res.data;
                });

                l2.table={
                    name:'',
                    data:[],
                    load:false,
                    kode:'',
                    sorted:'pemda'
                }
            }
        },
        data:{
            filter:'',
            basis:'<?=$_GET['basis']??''?>',
            table:{
                title:'',
                data:[]
            },
            rekap_data:{
                pagu:0,
                jumlah_pemda_provinsi:0,
                jumlah_pemda_kabkota:0,
                jumlah_kegiatan:0,
                jumlah_subkegiatan:0,
            },
            chart_per_program:{
                title:{
                    text:'Distribusi Anggaran APBD Per-Program PU'
                },
                subtitle:{
                    text:'Tahun {{$StahunRoute}}'
                },
                chart:{
                    type:'pie',
                    backgroundColor:'#ffffffb8',
                    height:500,

                },
                xAxis:{
                    type:'category'
                },
                plotOptions: {
                        pie: {
                            // allowPointSelect: true,
                            // cursor: 'pointer',
                            showInLegend: true,
                            dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>:<br>{point.percentage:.2f} %',
                            }
                        }
                 },
                series:[
                    {
   
                        name:'Anggaran APBD',
                        data:[
                           
                        ]
                    }
                ]
            },
            chart_per_kegiatan:{
                title:{
                    text:'Distribusi Anggaran APBD Per Kegiatan RKPD Air Minum'
                },
                subtitle:{
                    text:(window.filter.filter.toUpperCase())+' Tahun {{$StahunRoute}}'
                },
                chart:{
                    type:'pie',
                    backgroundColor:'#ffffffb8',
                    height:500,


                },plotOptions: {
                        pie: {
                            // allowPointSelect: true,
                            // cursor: 'pointer',
                            showInLegend: true,
                            dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>:<br>{point.percentage:.2f} %',
                            }
                        }
                 },
                xAxis:{
                    type:'category'
                },
                series:[
                    {
                        name:'Anggaran APBD',
                        data:[]
                    }
                ]
            },
            chart_per_subkegiatan:{
                title:{
                    text:'Distribusi Anggaran APBD Air Minum'
                },
                subtitle:{
                    text:'Nomenklatur Sub Kegiatan  {{$StahunRoute}}'
                },
                chart:{
                    type:'pie',
                    backgroundColor:'#ffffffb8',
                    height:700,

                },
                plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            
                            dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>:<br>{point.percentage:.2f} %',
                            }
                        },
                    series:{
                        point:{
                            events:{
                                click:function(){
                                    l2.detailDataGiat(this.kodesubkegiatan,this.name);
                                }
                            },
                        },
                    },
                },
                xAxis:{
                    type:'category'
                },
                series:[
                    {
                        name:'Anggaran APBD',
                        data:[
                            
                        ]
                    }
                ]
            }
        }
    });
</script>
@stop