@extends('adminlte::page')
<script>
    var page_meta={
        title:'TIPOLOGI HUBUNGAN PEMDA TERHADAP BUMDAM',
        keterangan:'',
        tahun:{{$StahunRoute}},
        tw:4
    }
</script>

@section('content')
<div class="parallax"  style=" background-image: url('{{url('assets/img/back-t.png')}}'); border-bottom:5px solid #fcda1a;">
    <div class="container">
        <section >
            <h3><b>Hubungan PEMDA dan BUMDAM</b></h3>
            <p><b>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quaerat vitae, ea assumenda officiis quod repudiandae nostrum quos, inventore porro non illo vel obcaecati voluptatum, consequatur amet. Voluptates qui a alias.</b></p>
           
        </section>
      
    </div>
   
</div>
@include('tematik.filter')

<div id="filter">
    <div>
        <div class="form-group p-3" >
        <div class="col-4">
            <label for="">Filter</label>
            <select name="" class="form-control" id="" v-model="filter">
                <option value="provinsi dan kota / kab">Semua</option>
                <option value="provinsi">Provinsi</option>
                <option value="kota">Kota</option>
                <option value="kab">Kab</option>
            </select>
        </div>
        </div>
    </div>

    <div style="min-height:20px;" class="bg-dark text-center p-1"><b>BASIS DATA {{strtoupper($basis)}} - (PEMDA @{{filter.toUpperCase()}}) TAHUN {{$StahunRoute}}</b></div>
</div>

<div class="" id="l1">
    <h5 class="text-center mt-2"><b>REKAP @{{getTitle}}  </b></h5>
    <div class="row mt-1 d-flex align-items-stretch" >
        <div class="col-3 d-flex mb-0 mt-0 p-0">
            <div class="info-box mb-2 flex-grow-1 rounded-0">
                <span class="info-box-icon bg-primary elevation-1"><i class="fas  fa-university"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">PEMDA PROVINSI</span>
                    <span class="info-box-number">@{{formatAngka(rekap_data.jumlah_pemda_pro)}} PEMDA</span>
                </div>
                
            </div>
            
        </div>
        
        <div class="col-3 d-flex mb-0 mt-0 p-0" >
            <div class="info-box mb-2 flex-grow-1 rounded-0">
                <span class="info-box-icon bg-success elevation-1"><i class="fas  fa-university"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">PEMDA KAB/KOTA</span>
                    <span class="info-box-number">@{{formatAngka(rekap_data.jumlah_pemda_kab)}} PEMDA</span>
                </div>
                
            </div>
            
        </div> 
        <div class="col-3 d-flex mb-0 mt-0 p-0">
            <div class="info-box mb-2 flex-grow-1 rounded-0 bg-lime bg-">
                <div class="info-box-content">
                    <span class="info-box-text">SEHAT DAN MANDIRI</span>
                    <span class="info-box-number">@{{formatAngka(rekap_data.k6)}} PEMDA</span>
                </div>
                
            </div>
            
        </div>
        <div class="col-3 d-flex mb-0 mt-0 p-0">
            <div class="info-box mb-2 flex-grow-1 rounded-0 bg-teal">
                <div class="info-box-content">
                    <span class="info-box-text">SEHAT TAPI TERGANTUNG PADA PEMDA</span>
                    <span class="info-box-number">@{{formatAngka(rekap_data.k5)}} PEMDA</span>
                </div>
                
            </div>
            
        </div>
        <div class="col-3 d-flex mb-0 mt-0 p-0">
            <div class="info-box mb-2 flex-grow-1 rounded-0 bg-olive">
                <div class="info-box-content">
                    <span class="info-box-text">SEHAT TAPI SEMI TERGANTUNG</span>
                    <span class="info-box-number">@{{formatAngka(rekap_data.k4)}} PEMDA</span>
                </div>
                
            </div>
            
        </div>
        <div class="col-3 d-flex mb-0 mt-0 p-0">
            <div class="info-box mb-2 flex-grow-1 rounded-0 bg-orange">
                <div class="info-box-content">
                    <span class="info-box-text">KURANG SEHAT DAN SEMI TERGANTUNG</span>
                    <span class="info-box-number">@{{formatAngka(rekap_data.k3)}} PEMDA</span>
                </div>
                
            </div>
            
        </div>
        <div class="col-3 d-flex mb-0 mt-0 p-0">
            <div class="info-box mb-2 flex-grow-1 rounded-0 bg-yellow">
                <div class="info-box-content">
                    <span class="info-box-text">KURANG SEHAT DAN TERGANTUNG</span>
                    <span class="info-box-number">@{{formatAngka(rekap_data.k2)}} PEMDA</span>
                </div>
                
            </div>
            
        </div>
        <div class="col-3 d-flex mb-0 mt-0 p-0">
            <div class="info-box mb-2 flex-grow-1 rounded-0 bg-danger">
                <div class="info-box-content">
                    <span class="info-box-text">TIDAK SEHAT DAN TERGANTUNG</span>
                    <span class="info-box-number">@{{formatAngka(rekap_data.k1)}} PEMDA</span>
                </div>
                
            </div>
            
        </div>
       
    </div>
    <div class="row no-gutters">
        <div class="col-12">
            <div class="card parallax mb-0"  style=" background-image: url('{{url('assets/img/back-t.png')}}');">
                <div class="card-body" >
                    <div class="row no-gutter">
                        <div v-bind:class="chart_kota.title.text?'col-6':'col-12'">
                            <charts :constructor-type="'mapChart'" :options="chart_provinsi" :callback="chartCallback"></charts>
                            <div class="card rounded-0" >
                                <div class="card-body">
                                    <ul class="list-inline">
                                        @foreach ($color as $key=> $c)
                                        <li class="list-inline-item">
                                            <i class="fa fa-circle" style="color:{{$c}}"></i> {{$key}}
                                        </li>
                                        @endforeach
        
                                    </ul>
                                </div>
                            </div>
                            
                           
                        </div>
                        <div v-if="chart_kota.title.text" class="col-6">
                            <charts  :constructor-type="'mapChart'" :options="chart_kota"></charts>
                            <div class="card rounded-0" >
                                <div class="card-body">
                                    <ul class="list-inline">
                                        <li class="list-inline-item" style="font-size:11px;" v-for="item,key in chart_kota.colors ">
                                            <i class="fa fa-circle" v-bind:style="'color:'+item.color+'; '"></i> @{{item.text}}
                                        </li>
        
                                    </ul>
                                </div>
                           
                                    
                            </div>
                        </div>
                        
                      
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="height:20px;" class="bg-dark"></div>

<div id="l2">
    <div class="row">
        <div class="col-12" v-if="table.load==true">
            <p class="text-center mt-2"><b>Loading..</b></p>
        </div>
        <div class="col-12" v-if="table.name && table.load!=true">
          <div class="card">
              <div class="card-header">
                  <h5><b>@{{table.name}} (@{{table.kode}})</b></h5>
                  <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Sort By</label>
                            <select name="" class="form-control" id="" v-model="table.sorted">
                                <option value="PEMDA">PEMDA</option>
                                <option value="pagu">Pagu</option>
      
                            </select>
                        </div>
                      </div>
                  </div>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                 
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Kode</th>
                                <th>Nama Daerah</th>
                                <th>Tipologi Hubungan</th>
                                <th>Keterangan</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="item in table.data" >
                                <td>@{{item.kodepemda}}</td>
                                <td>@{{item.name}}</td>
                                <td v-bind:style="'background-color:'+item.color"><p class="bg-white p-1"><b>@{{item.text}}</b></p></td>
                                <td>@{{item.jenis_hubungan}}</td>




                            </tr>
                        </tbody>
                    </table>
                </div>
              </div>
          </div>
        </div>
        <div class="col-12">
        </div>
    </div>
</div>
@stop


@section('js')
<script>


    var filter =new Vue({
        el:'#filter',
        data:{
            filter:'provinsi dan kota / kab'
        },
        watch:{
            "filter":function(val){
                console.log(val);
                setTimeout(function(){
                window.l1.filter=val;
                l1.chart_provinsi.subtitle.text=val.toUpperCase()+' - TAHUN {{$StahunRoute}}';
                l1.chart_kota.subtitle.text=val.toUpperCase()+' - TAHUN {{$StahunRoute}}';
                window.l2.filter=val;
                l1.init();
                l1.ref_one.reflow();
                window.scrollToDom('#l1');
                },500);

            }
        }
    })
    var l2=new Vue({
        el:'#l2',
        data:{
           table:{
               name:'',
               sorted:'PEMDA',
               kode:'',
               load:false,
               data:[]
           },
           filter:'',
            basis:'<?=$_GET['basis']??''?>',
        },
        watch:{
            "table.sorted":function(){
                // this.detailDataGiat(this.table.kode,this.table.name);
            }
        },
        methods:{
            formatAngka(angka){
                return l1.formatAngka(angka);
            },
            detailDataGiat:function(kode,namasubgiat){
                // this.table.load=true;
                // window.scrollToDom('#l2');
                // req_ajax.post('{{route('api-web.public.tema1.detail_subgiat_perpemda',['tahun'=>$StahunRoute])}}',{
                // filter:this.filter,
                // basis:this.basis,
                // kode:kode,
                // sort:this.table.sorted=='pagu'?['sum(pagu)','desc']:['mp.kodePEMDA','asc']
                // }).then(function(res){
                //     l2.table.name=namasubgiat;
                //     l2.table.data=res.data;
                //     l2.table.kode=kode;
                //     l2.table.load=false;
                // });
            },
        }
    });


    var l1=new Vue({
        el:'#l1',
        created:function(){
            this.init();
        },
        computed:{
            getTitle:function(){
                return window.page_meta.title.toUpperCase();
            }
        },
        methods:{
            detail_per_kota:function(kodepemda,name){
                req_ajax.post('{{route('api-web.public.tema2.tipologi_hub_pemda.load_hubungan_kota',['tahun'=>$StahunRoute])}}',{
                    basis:this.basis,
                    filter:this.filter,
                    kodepemda:kodepemda
                }).then((res)=>{
                    l1.chart_kota.title.text='KETERISIAN '+page_meta.title+' '+name+' BASIS '+l1.basis.toUpperCase();

                    l2.table.name=page_meta.title+' '+name+' BASIS '+l1.basis.toUpperCase();
                    l1.chart_kota.series[0].data=[{
                        id:parseInt(kodepemda),
                        value:1,
                        color:'#fff'
                        
                    }];
                    l1.chart_kota.series[1].data=(res.data.data);
                    l1.chart_kota.colors=(res.data.colors);

                    l2.table.data=res.data.data;
                    l2.table.kode=kodepemda;
                    // l1.chart_provinsi.chart.height=500;
                    setTimeout(function(){
                      l1.ref_one.reflow();

                    },600);

                });

            },
            chartCallback:function(event){
                this.ref_one=event;
                this.ref_one.reflow();
            },
            formatAngka:function(angka){
                var number_string = (angka+'').replace(/[^,\d]/g, '').toString(),
                split   		= number_string.split(','),
                sisa     		= split[0].length % 3,
                rupiah     		= split[0].substr(0, sisa),
                ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
    
                // tambahkan titik jika yang di input sudah menjadi angka ribuan
                if(ribuan){
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                }
    
                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                return rupiah;
            },
            init:function(){
                this.chart_kota.title.text='';
                this.chart_provinsi.subtitle.text=this.filter.toUpperCase()+' - TAHUN {{$StahunRoute}}';
                this.chart_kota.subtitle.text=this.filter.toUpperCase()+' - TAHUN {{$StahunRoute}}';

                req_ajax.post('{{route('api-web.public.tema2.tipologi_hub_pemda.load_rekap_hubungan',['tahun'=>$StahunRoute])}}',{
                    basis:this.basis,
                    filter:this.filter
                }).then((res)=>{
                    l1.rekap_data=res.data;
                });
                req_ajax.post('{{route('api-web.public.tema2.tipologi_hub_pemda.load_hubungan_pro',['tahun'=>$StahunRoute])}}',{
                    basis:this.basis,
                    filter:this.filter
                }).then((res)=>{
            
                    l1.chart_provinsi.series[0].data=res.data;
                    // setTimeout(() => {
                    //     // l1.chart_provinsi.chart.height=500;
                    // }, 500);

                    
                });

                this.chart_provinsi.title.text='KETERISIAN '+page_meta.title+' BASIS '+this.basis.toUpperCase();

                l2.table={
                    name:'',
                    data:[],
                    load:false,
                    kode:'',
                    sorted:'PEMDA'
                }
            }
        },
        data:{
            ref_one:{},
            filter:'provinsi dan kota / kab',
            basis:'<?=$_GET['basis']??'nasional'?>',
            table:{
                title:'',
                data:[]
            },
            rekap_data:{
                jumla_pemda_pro:0,
                jumla_pemda_kab:0,
                k1:0,
                k2:0,
                k3:0,
                k4:0,
                k5:0,
                k6:0,
                k0:0,
            },
               
            chart_kota:{
                colors:[],
                title:{
                    text:'KETERISIAN PER PROVINSI'
                },
                subtitle:{
                    text:'provinsi dan kota / kab Tahun {{$StahunRoute}}'
                },
                chart:{
                    height:500,
                },
                tooltip: {
                    formatter:function(){
                        return this.point.text?'<b>'+this.point.name+'</b><br>'+this.point.text:'<b>'+this.point.name+'</b>';
                    }
                },
                series:[
                    {
                        name:'KETERISIAN PER PROVINSI',
                        data:[],
                        type:'map',
                        joinBy:['id','id'],
                        mapData:Highcharts.maps['ind'],
                        allAreas:false,
                        dataLabels: {
                            enabled: false,
                            format: '{point.text} '
                        },
                        tooltip: {
                           enabled:false
                        },
                        point:{
                            events:{
                               
                            }
                        },
                        showInLegend:false,
                        data:[]
                    },
                    {
                        name:'KETERISIAN PER KOTA/KAB',
                        data:[],
                        type:'map',
                        joinBy:['id','id'],
                        mapData:Highcharts.maps['ind_kab'],
                        allAreas:false,
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}'
                        },
                        
                        point:{
                            events:{
                               
                            }
                        },
                        showInLegend:false,
                        data:[]
                    },
                ]
            },
            chart_provinsi:{
                ref:null,
                title:{
                    text:'KETERISIAN PER PROVINSI'
                },
                subtitle:{
                    text:'provinsi dan kota / kab Tahun {{$StahunRoute}}'
                },
                chart:{
                    height:500,
                },
                tooltip: {
                    formatter:function(){
                        return this.point.name+': <b>'+this.point.value+'</b> PEMDA';
                    }
                },
                series:[
                    {
                        name:'KETERISIAN PER PROVINSI',
                        data:[],
                        type:'map',
                        joinBy:['id','id'],
                        mapData:Highcharts.maps['ind'],
                        dataLabels: {
                            enabled: true,
                            format: '{point.name} : <br>{point.value} Pemda'
                        },
                        point:{
                            events:{
                                click:function(){
                                    l1.detail_per_kota(this.kodepemda,this.name);
                                }
                            }
                        },
                        showInLegend:false,
                        data:[]
                    }
                ]
            }
         
         
        }
    });

</script>

@stop