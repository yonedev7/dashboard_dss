@extends('adminlte::page')


@section('content')
<div id="app">
   <div class="p-3">
    <div class="d-flex">
        <div class="d-flex flex-column ">
            <img style="background:#ddd;"  :src="data.adminlte_image" width="300" height="300" class="img-responsive p-3" alt="">
            <input type="file" accept="image/*" @change="loadNewFile($event)" style="display: none" ref="file_ava">
            <button class="btn btn-success btn-sm" @click="newSourceAvA">Rubah Avatar</button>
            <div id="modal-crop" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <v-img-ava-croper :img-data="file_load" :result="UploadImage"></v-img-ava-croper>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="flex-grow-1 ml-2 ">
            <h3>@{{data.name}}</h3>
            <div class="bg-shadow p-3 shadow bg-white mb-3 rounded">
                <h5>Prifile</h5>
                <hr>
               <form action="{{route('dash.si.account.update',['tahun'=>$StahunRoute])}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="">Nama</label>
                    <input type="text" class="form-control" name="name" required v-model="data.name">
                </div>
                <div class="form-group">
                    <label for="">Email</label>
                    <input type="text" disabled class="form-control" v-model="data.email">
                </div>
                <div class="form-group">
                    <label for="">Nomer Telpon</label>
                    <input class="form-control" type="hidden" name="phone_number" v-model="com_phone_number">
                    <vue-phone-number-input required @update="phoneProcessfunction" ref="phone_number_ref" :default-country-code="data.phone_number[0]||'ID'" v-model="data.phone_number[1]" ></vue-phone-number-input   >
                </div>
                <button type="submit" class="mt-2 btn btn-success btn-sm">Simpan</button>

               </form>
            </div>
            <div class="bg-shadow p-3 bg-white shadow mb-3 rounded">
                <h5>Password</h5>
                <hr>
                <form action="{{route('dash.si.account.update_password',['tahun'=>$StahunRoute])}}" method="post">
                    @csrf
                <div class="form-group">
                    <label for="">Password lama</label>
                    <input type="password" class="form-control" required name="old" v-model="password.old" >
                </div>
                <p class="m-0"><span class=" badge bg-danger">@{{password.text}}</span></p>
                <div class="form-group">
                    <label for="">Password Baru</label>
                    <input type="password" class="form-control" required name="new" v-model="password.new" >
                </div>
                <div class="form-group">
                    <label for="">Konfirmasi Password Baru</label>
                    <input type="password" class="form-control"  required name="new_conf" v-model="password.new_conf">
                </div>

                <button type="submit" v-if="com_password_valid" class="mt-2 btn btn-success btn-sm">Simpan</button>
                </form>
            </div>
            
            

            
        </div>

    </div>
   </div>
</div>


@stop


@section('js')
<script>

    var app=new Vue({
        el:'#app',
        data:{
            data:<?=json_encode($my)?>,
            file:null,
            file_load:null,
            password:{
                old:'',
                new:'',
                mew_conf:'',
                valid:false,
                text:''
            }

        },
        created:function(){
            this.data.phone_number=JSON.parse(this.data.phone_number||"[null,null,null]");
        },
        watch:{
            
        },
        computed:{
            com_phone_number:function(){
                return JSON.stringify(this.data.phone_number||[null,null,null]);
            },
            com_password_valid:function(){
                if(this.password.old){
                    if(this.password.old.length>=8){
                        if(this.password.new.length>=8){
                            if(this.password.new==this.password.new_conf){
                                this.password.valid=true;
                                this.password.text=null;

                            }else{
                                this.password.valid=false;
                                this.password.text='Password Not Match';

                            }
                        }else{
                            this.password.valid=false;
                            this.password.text='Password Baru Min 8 Charakter';

                        }
                    }else{
                        this.password.valid=false;
                        this.password.text='Password Lama Min 8 Charakter';

                    }
                }else{
                    this.password.valid=false;
                    this.password.text=null;

                }

                return this.password.valid;
            }
        },
        methods:{
            phoneProcessfunction:function(){
                this.data.phone_number[0]=this.$refs.phone_number_ref.results.countryCode;
                this.data.phone_number[2]=this.$refs.phone_number_ref.results.formattedNumber;
            },
            newSourceAvA:function(){
                $(this.$refs.file_ava).click();
            },
            loadNewFile:async function(ev){
                if(ev.target.files){
                    self=this;
                    let reader = new FileReader();
                    reader.onload=function(e){
                       self.file=e.target.result;
                       self.file_load=self.file;
                       $('#modal-crop').modal({backdrop:'static',keyboard:false});
                       ev.target.value=null;
                    };
                    reader.readAsDataURL(ev.target.files[0]);
                }else{
                    this.file=null;
                }
            },
            UploadImage:function(data){
                var self=this;
                this.$isLoading(true);
                this.data.adminlte_image=data;
                req_ajax.post('{{route('api-web.account.update_ava')}}',{
                    image:data
                }).then(res=>{
                    console.log(res);
                }).finally(function(){
                    self.$isLoading(false);

                })
                console.log(data,'new img');
            }
        }
    })
</script>

@stop