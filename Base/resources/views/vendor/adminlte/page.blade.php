
@extends('adminlte::master')


@inject('layoutHelper', 'JeroenNoten\LaravelAdminLte\Helpers\LayoutHelper')



@php

config(['adminlte.logo_img'=>url('/assets/img/brand/logo-1.png')]);
        

if($Sadmin){
    

    config([
        'adminlte.layout_topnav'=>false,
        'adminlte.layout_fixed_navbar' => true,
        'adminlte.layout_fixed_sidebar' => false,
        'adminlte.sidebar_collapse' => true,
        'adminlte.right_sidebar'=>true,
        'adminlte.layout_dark_mode'=>false,
        'adminlte.classes_topnav'=>'navbar-primary bg-primary',
        'adminlte.classes_content_header_a'=>'bg-primary container-fluid h-ppp'


    ]);

}else{
    config([
        'adminlte.layout_topnav'=>true,
        'adminlte.layout_fixed_navbar' => false,
        'adminlte.layout_fixed_sidebar' => false,
        'adminlte.sidebar_collapse' => false,
        'adminlte.logo_img'=>false,
        'adminlte.logo_img_class'=>null,
        'adminlte.logo_img_alt'=>null,
        'adminlte.logo_img_xl_class'=>null,
        'adminlte.logo_img_xl'=>null,
        'adminlte.nav_logo'=>false,
        'adminlte.right_sidebar'=>false,
        'adminlte.classes_topnav'=>'navbar-dark text-white',

    ]);

    // if((config('adminlte.page_type'))){
    //     dd('iset',config('adminlte.page_type'));
    // }
}    

@endphp

@section('adminlte_css')
    <style>
         :not(:root):fullscreen::backdrop{
        background-image: url('{{asset('assets/img/back-fs.png')}}')!important;
    }
    </style>
    @stack('css')
    @yield('css')

    @yield('css_push')
    @stack('css_push')

    @if($Sadmin)
    <style>
        /* .layout-navbar-fixed .wrapper .main-sidebar:hover .brand-link{
            width: 30vw;
            max-width: 600px;
            min-width: 300px;

        }
        .main-sidebar, .main-sidebar::before{
            width: 30vw;
            max-width: 600px;
            min-width: 300px;
        }
        body:not(.sidebar-mini-md):not(.sidebar-mini-xs):not(.layout-top-nav) .content-wrapper, body:not(.sidebar-mini-md):not(.sidebar-mini-xs):not(.layout-top-nav) .main-footer, body:not(.sidebar-mini-md):not(.sidebar-mini-xs):not(.layout-top-nav) .main-header{
            margin-left: 30vw;
            flex-grow: inherit
        } */

        .h-ppp{
            padding-bottom: 70px;
            margin-bottom: -50px;
        }
        .h-ppp hr{
            background-color: #fff;
        }
        .main-header{
            border-bottom:1px solid #007bff;
        }
        .navbar-primary.bg-primary ul a.nav-link{
            color:#fff!important;
        }
        .main-sidebar .nav  {
            max-width: 100%;
        }
        .main-sidebar nav > ul> li{
            word-wrap: break-word!important;
            white-space: unset;

        }
       
         .nav-sidebar>.nav-header{
            /* background-color:orange; */
            /* border-bottom: 2px solid #000; */
            color:#000;
            white-space: unset;

        }

        .sidebar-mini .nav-sidebar>li{
            word-wrap: break-word!important;
            
                white-space: unset;

        }
        


        @media (min-width: 992px){
            .sidebar-mini .nav-sidebar>.nav-header{
                word-wrap: break-word!important;
                white-space: initial;
                
            }
            .sidebar-mini .nav-sidebar>.nav-header{
                padding-left: 0px;
            }
        }
        

    </style>
    @endif
@stop



@section('classes_body', $layoutHelper->makeBodyClasses())

@section('body_data', $layoutHelper->makeBodyData())

@section('body')
    <script>
        const dss{{$SpageId}}={
            'base':'{{url('api')}}',
            'token':'{{Auth::check()?Auth::User()->_web_api_token():''}}',
            'user':<?=json_encode(Auth::check()?(Auth::User()):'{}')?>   
        }
    </script>
    <div class="wrapper loading-wrapper" style="display: none;">
        <div style="display: none;">
            <img id="dev-s" src="{{url('assets/img/brand/logo.png')}}" alt="">
        </div>

        {{-- Top Navbar --}}
        @if($layoutHelper->isLayoutTopnavEnabled())
        <style>
        .table th,.table td{
            font-size:14px;
        }
        .navbar-nav .nav-item a{
            font-weight: 800;
            color:#fff;
        }
        .navbar-nav .dropdown-menu .dropdown-item {
            color:#000;
        }
        </style>
        @if(!in_array(config('adminlte.page_type'),['desk_dash']))

        @if(!$Sadmin)
        <div style="height: 70px; "></div>
        @endif

        @include('adminlte::partials.navbar.navbar-layout-topnav')
        @endif

        @if(!$Sadmin)
        <button id="btn-screenshoot" class="btn btn-primary">
            <i class="fa fa-camera"></i>
        </button>
        <style>
            #btn-screenshoot{
                position: fixed;
                top:5px;
                right:0px;
                width:40px;
                height:40px;
                border-top-left-radius:20px;
                border-bottom-left-radius:20px;
                z-index:99999;
                transition: width 1s;
            }    
            #btn-screenshoot:hover{
                width:70px;
            }
        </style>
        @if(!in_array(config('adminlte.page_type'),['desk_dash']))
        <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top" style="z-index: 99; border-bottom:1px solid #000">
            <a class="navbar-brand" href="{{url('/')}}">
                <img src="{{url('assets/img/brand/logo.png')}}" width="100" height="40" alt="">
            </a>
            <div class="collapse navbar-collapse" id="navbar-menu-tema">
                <ul class="navbar-nav ">
                    @php
                    $menus=DB::table('master.menus')->where('type',1)->orderBy('index','asc')->get();
                    @endphp
                    @foreach($menus as $menu)
                    <li class="nav-item nav-link ">
                      <a href="{{route('mod.tematik.index',['tahun'=>date('Y'),'id'=>$menu->id])}}" class="text-dark">
                        <img src="{{asset($menu->icon)}}" width="40" style="border-radius:50%; border:1px solid rgb(76, 76, 76)" alt="">
                        <span>{{strtoupper($menu->title)}}</span></a>
                    </li>
                    @endforeach
                </ul>
            </div>
            <style>
                #navbar-menu-tema .nav-item{
                    border-left:1px solid #000;
                }
                
            </style>
           
            <ul class="navbar-nav ml-auto">
              
              @foreach($Smenus as $menu)
                
                  @endforeach
                  @if(!Auth::check())
                  <li class="nav-item  nav-link">
                      <a class="btn btn-primary " href="{{url('login')}}">Login</a>
                  </li>
              @endif
          </ul>
        </nav>
        @endif

        <div id="btn-up" style="position:fixed; z-index:99; bottom:50px; right:15px;  width:50px; height:50px;">
            <button onclick="window.scrollTo({top: 0, behavior: 'smooth'})" style="height:50px; width:50px; border-radius:39%; background-color:#ddd" class="btn"><i class="fa fa-arrow-up"></i></button>    
        </div>
       
        
        @endif
       
        @else
             @if(!in_array(config('adminlte.page_type'),['desk_dash']))
                @include('adminlte::partials.navbar.navbar')
            @endif
        @endif

        {{-- Left Main Sidebar --}}
        @if(!$layoutHelper->isLayoutTopnavEnabled())
            @include('adminlte::partials.sidebar.left-sidebar')
        @endif

        {{-- Content Wrapper --}}
        @empty($iFrameEnabled)
            @include('adminlte::partials.cwrapper.cwrapper-default')
        @else
            @include('adminlte::partials.cwrapper.cwrapper-iframe')
        @endempty

        {{-- Footer --}}
        @hasSection('footer')
            @include('adminlte::partials.footer.footer')
        @endif

        {{-- Right Control Sidebar --}}
        @if(config('adminlte.right_sidebar'))
            @include('adminlte::partials.sidebar.right-sidebar')
        @endif

    </div>
@stop
       

@section('adminlte_js')
<script>
    Array.prototype.unique = function() {
                var a = this.concat();
                for(var i=0; i<a.length; ++i) {
                    for(var j=i+1; j<a.length; ++j) {
                        if(a[i] === a[j])
                            a.splice(j--, 1);
                    }
                }

                return a;
            };

            function ScrollTo(el,time=1000,offset=0){
                    $([document.documentElement, document.body]).animate({
                    scrollTop: $(el).offset().top+offset
                }, time);
            }
           

</script>
  
    <script>

        initial_ajax(dss{{$SpageId}});
        window.xdss{{md5(url()->current())}}=dss{{$SpageId}};
    </script>
    @yield('js')
    <script>
          
            function check_view(){
                var data={
                    w_width:window.innerWidth,
                    mobile:false,
                    y_view:window.scrollY,
                    x_view:window.scrollX
                } 
                
                if(data.mobile==false && data.y_view>500){
                    $('#btn-up').css('display','block');
                }else{
                    $('#btn-up').css('display','none');
                }
                return data;
            };
            window.addEventListener('resize', check_view());
            window.addEventListener(
                    "scroll",
                    () => check_view(),
                    { passive: true }
                );

                @if(env('DEBUG')==false)
                   
                @endif
        </script>

    
    @stack('js')

    @stack('js_push')


@stop


