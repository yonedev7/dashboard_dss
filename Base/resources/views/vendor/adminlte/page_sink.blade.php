<html>
    <head>
        @php
        if(!isset($title_page)){

        }
        @endphp
        <title>{{(!isset($title_page)?'DSS-NUWSP':(strtoupper($title_page)))}}</title>
        <link rel="stylesheet" href="{{ asset(config('adminlte.laravel_mix_css_path', 'css/app.css').'?v='.date('is')) }}">
        @yield('css')
        <style>
            :not(:root):fullscreen::backdrop{
                background-image: url('{{asset('assets/img/back-fs.png')}}')!important;
            }
       </style>
        <script>
            const dss{{$SpageId}}={
                'base':'{{url('api')}}',
                'token':'{{Auth::check()?Auth::User()->_web_api_token():''}}',
                'user':<?=json_encode(Auth::check()?(Auth::User()):'{}')?>   
            }
        </script>
    </head>

    <body>
        @yield('content')
    </body>
    <script src="{{ asset(config('adminlte.laravel_mix_js_path', 'js/app.js').'?version='.date('is').rand(0,10)) }}"></script>

    <script>
        initial_ajax(dss{{$SpageId}});
    </script>
    
    @yield('js')
    @stack('js_push')
    <button id="btn-screenshoot" class="btn btn-primary">
        <i class="fa fa-camera"></i>
    </button>
    <style>
        #btn-screenshoot{
            position: fixed;
            top:5px;
            right:0px;
            width:40px;
            height:40px;
            border-top-left-radius:20px;
            border-bottom-left-radius:20px;
            z-index:99999;
            transition: width 1s;
        }    
        #btn-screenshoot:hover{
            width:70px;
        }
    </style>

    <script>

        $('#btn-screenshoot').on('click',function(){
            const screenshotTarget = document.body;
                html2canvas(screenshotTarget).then((canvas) => {
                    canvas.toBlob(function(blob){
                       var url= window.URL.createObjectURL(blob);
                       console.log(url);
                       window.open(url);
                       
                    }, 'image/jpeg', 0.95);
                  
                });
        });
    </script>


</html>