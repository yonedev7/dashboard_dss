@inject('layoutHelper', 'JeroenNoten\LaravelAdminLte\Helpers\LayoutHelper')

@if($layoutHelper->isLayoutTopnavEnabled())
    @php( $def_container_class = 'container' )
@else
    @php( $def_container_class = 'container-fluid' )
@endif

{{-- Default Content Wrapper --}}
<div class="content-wrapper {{ config('adminlte.classes_content_wrapper', '') }}">
    @if(Auth::check() AND ($SenvDash=='admin'))
    @if(count(Auth::User()->getRoleNames()))
    {{-- <div class="ribbon-wrapper">
        <div class="ribbon bg-danger px-3">
            Dashboard {{Auth::check()?implode(', ',Auth::User()->getRoleNames()->toArray()):''}}
        </div>
      </div> --}}
        {{-- <div class="bg-secondary p-1 text-uppercase"><b></b></div> --}}

    @endif
    @endif
    {{-- Content Header --}}
    @hasSection('content_header')
        <div class="content-header {{ config('adminlte.classes_content_header_a') ?: '' }}">
            <div class="{{ config('adminlte.classes_content_header') ?: $def_container_class }}">
                
                @if(isset($breadcrums_page))
                @if (is_array($breadcrums_page))
                 
                    {{call_user_func_array('Breadcrumbs::render',$breadcrums_page)}}
                @else
                    {{Breadcrumbs::render($breadcrums_page)}}
                @endif
                <hr>
                @endif
                @yield('content_header')
            </div>
        </div>
    @endif

    {{-- Main Content --}}
    <div class="content" style="{{config('adminlte.full_content')?'padding-right:0;padding-left:0':''}}">
        <div class="{{ config('adminlte.classes_content') ?: $def_container_class }}">
            @yield('content')
        </div>
    </div>

</div>
