<aside class="control-sidebar control-sidebar-{{ config('adminlte.right_sidebar_theme') }}">
    <a class="nav-link btn btn-sm " data-widget="control-sidebar" href="#"><i class="fa fa-arrow-right"></i> Close</a>
    @yield('right-sidebar')
</aside>
