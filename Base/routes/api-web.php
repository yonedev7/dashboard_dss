<?php

Route::name('api-web')->group(function(){

        Route::prefix('mode-dashboard/{tahun}')->middleware('auth:api')->name('.mod.dashboard')->group(function(){
            Route::post('/schema/{context}',[Modules\TematikModul\Http\Controllers\SkemaHalamanController::class, 'save'])->name('.save_schema_tampilan');
        });


        Route::get('test',function(){
            return 'yyy';
        });

        Route::prefix('account')->middleware('auth:api')->name('.account')->group(function(){
            Route::post('/update-ava',[App\Http\Controllers\UserController::class,'update_ava'])->name('.update_ava');
            
    
        });

        Route::prefix('schema-data')->middleware('auth:api')->name('.schema_data')->group(function(){
            Route::get('/source-data',[App\Http\Controllers\SistemInformasi\MasterDataCtrl::class, 'json_get_data'])->name('.get-data');

        });

        Route::prefix('{tahun}/rekap')->middleware('auth:api')->name('.rekap')->group(function(){
            Route::post('/get-data',[App\Http\Controllers\SistemInformasi\RekapCtrl::class, 'load'])->name('.load-data');
            Route::post('/get-data-detail',[App\Http\Controllers\SistemInformasi\RekapCtrl::class, 'load_detail'])->name('.load-data-detail');
            
        });

        Route::prefix('{tahun}/lokus')->middleware('auth:api')->name('.lokus')->group(function(){
            Route::post('/get-data',[App\Http\Controllers\SistemInformasi\DaerahCtrl::class, 'load'])->name('.load-data');
            
        });
        Route::prefix('public')->name('.public')->group(function(){
                 Route::prefix('analisa')->middleware('auth:api')->name('.analisa')->group(function(){
                         Route::post('listen-list-data/{tahun}',[App\Http\Controllers\SistemInformasi\Analish\WidgetCtrl::class,'listing_schema'])->name('.listing_schema');
                         Route::post('chart/saving/{tahun}/{kode_analisa}',[App\Http\Controllers\SistemInformasi\Analish\WidgetCtrl::class,'save_dashbord'])->name('.save_dashbord');

                });


                Route::prefix('{tahun}/data')->middleware('api_public')->name('.data')->group(function(){
                    Route::prefix('profile-pemda')->name('.profile_pemda')->group(function(){
                        Route::post('get-content',[App\Http\Controllers\ProfileDaerahCtrl::class,'render_content'])->name('.render_content');

                    
                    });


                    Route::prefix('tema-data-rekap')->name('.rekap')->group(function(){
                        Route::post('list',[App\Http\Controllers\HomeController::class,'load_rekap_meta'])->name('.load_rekap_meta');
                        Route::post('load',[App\Http\Controllers\HomeController::class,'load_rekap'])->name('.load_rekap');

                    });

                    Route::prefix('rencana-bisnis')->name('.renbis')->group(function(){
                        Route::post('per-provinsi',[App\Http\Controllers\DataNUWSP\BisnisPlanCtrl::class,'load_provinsi'])->name('.load_provinsi');
                        Route::post('per-kota-kab',[App\Http\Controllers\DataNUWSP\BisnisPlanCtrl::class,'load_kota'])->name('.load_kota');


                    });

                    Route::prefix('ikfd')->name('.ikfd')->group(function(){
                        Route::post('ikfd-per-provinsi',[App\Http\Controllers\DataNUWSP\IKFDCtrl::class,'load_provinsi'])->name('.per_provinsi');
                        Route::post('ikfd-per-kota-kab',[App\Http\Controllers\DataNUWSP\IKFDCtrl::class,'load_kota'])->name('.per_kota');
                        Route::post('ikfd-rekap',[App\Http\Controllers\DataNUWSP\IKFDCtrl::class,'load_rekap'])->name('.rekap');

                    });

                    Route::prefix('fcr')->name('.fcr')->group(function(){
                        Route::post('fcr-per-provinsi',[App\Http\Controllers\DataNUWSP\KinerjaBPKPCtrl::class,'load_frc_provinsi'])->name('.per_provinsi');
                        Route::post('fcr-per-kota-kab',[App\Http\Controllers\DataNUWSP\KinerjaBPKPCtrl::class,'load_fcr_kota'])->name('.per_kota');
                        Route::post('fcr-rekap',[App\Http\Controllers\DataNUWSP\KinerjaBPKPCtrl::class,'load_fcr_rekap'])->name('.rekap');

                    });

                    Route::prefix('pelatihan')->name('.pelatihan')->group(function(){
                        Route::post('pelatihan-per-provinsi',[App\Http\Controllers\DataNUWSP\PelatihanCtrl::class,'load_provinsi'])->name('.per_provinsi');
                        Route::post('pelatihan-per-kota-kab',[App\Http\Controllers\DataNUWSP\PelatihanCtrl::class,'load_kota'])->name('.per_kota');
                        Route::post('pelatihan-rekap',[App\Http\Controllers\DataNUWSP\PelatihanCtrl::class,'load_rekap'])->name('.rekap');

                    });

                    Route::prefix('tema-pelatihan')->name('.tema-pelatihan')->group(function(){
                        Route::post('tema-pelatihan-per-provinsi',[App\Http\Controllers\DataNUWSP\TemaPelatihanCtrl::class,'load_provinsi'])->name('.load_provinsi');
                        Route::post('tema-pelatihan-per-kota-kab',[App\Http\Controllers\DataNUWSP\TemaPelatihanCtrl::class,'load_kota'])->name('.load_kota');
                        Route::post('tema-pelatihan-rekap',[App\Http\Controllers\DataNUWSP\TemaPelatihanCtrl::class,'load_rekap'])->name('.rekap');

                    });

                    Route::prefix('luas-wilayah')->name('.luas-wilayah')->group(function(){
                        Route::post('per-provinsi',[App\Http\Controllers\DataNUWSP\LuasWilCtrl::class,'load_provinsi'])->name('.per_provinsi');
                        Route::post('per-kota-kab',[App\Http\Controllers\DataNUWSP\LuasWilCtrl::class,'load_kota'])->name('.per_kota');
                        Route::post('rekap',[App\Http\Controllers\DataNUWSP\LuasWilCtrl::class,'load_rekap'])->name('.rekap');

                    });

                    Route::prefix('jumlah-penduduk')->name('.jumlah-penduduk')->group(function(){
                        Route::post('per-provinsi',[App\Http\Controllers\DataNUWSP\JumlahPendudukCtrl::class,'load_provinsi'])->name('.load_provinsi');
                        Route::post('per-kota-kab',[App\Http\Controllers\DataNUWSP\JumlahPendudukCtrl::class,'load_kota'])->name('.load_kota');
                        Route::post('rekap',[App\Http\Controllers\DataNUWSP\JumlahPendudukCtrl::class,'load_rekap'])->name('.rekap');

                    });

                    Route::prefix('jumlah-penduduk-miskin')->name('.jumlah-penduduk-miskin')->group(function(){
                        Route::post('per-provinsi',[App\Http\Controllers\DataNUWSP\JumlahPendudukMiskinCtrl::class,'load_provinsi'])->name('.load_provinsi');
                        Route::post('per-kota-kab',[App\Http\Controllers\DataNUWSP\JumlahPendudukMiskinCtrl::class,'load_kota'])->name('.load_kota');
                        Route::post('rekap',[App\Http\Controllers\DataNUWSP\JumlahPendudukMiskinCtrl::class,'load_rekap'])->name('.rekap');

                    });

                    Route::prefix('tipologi-urusan')->name('.tipologi-urusan')->group(function(){
                        Route::post('per-provinsi',[App\Http\Controllers\DataNUWSP\TipologiUrusanCtrl::class,'load_provinsi'])->name('.load_provinsi');
                        Route::post('per-kota-kab',[App\Http\Controllers\DataNUWSP\TipologiUrusanCtrl::class,'load_kota'])->name('.load_kota');
                        Route::post('rekap',[App\Http\Controllers\DataNUWSP\TipologiUrusanCtrl::class,'load_rekap'])->name('.rekap');

                    });

                    Route::prefix('penilaian-bpkp')->name('.bpkp')->group(function(){
                        Route::post('penilaian-per-provinsi',[App\Http\Controllers\DataNUWSP\KinerjaBPKPCtrl::class,'load_provinsi'])->name('.per_provinsi');
                        Route::post('penilaian-per-kota-kab',[App\Http\Controllers\DataNUWSP\KinerjaBPKPCtrl::class,'load_kota'])->name('.per_kota');
                        Route::post('penilaian-rekap',[App\Http\Controllers\DataNUWSP\KinerjaBPKPCtrl::class,'load_rekap'])->name('.rekap');
                    });

                    Route::prefix('penyertaan-modal')->name('.penyertaan-modal')->group(function(){
                        Route::post('per-provinsi',[App\Http\Controllers\DataNUWSP\PenyertaanModalCtrl::class,'load_provinsi'])->name('.load_provinsi');
                        Route::post('per-kota-kab',[App\Http\Controllers\DataNUWSP\PenyertaanModalCtrl::class,'load_kota'])->name('.load_kota');
                        Route::post('rekap',[App\Http\Controllers\DataNUWSP\PenyertaanModalCtrl::class,'load_rekap'])->name('.rekap');
                    });

                    Route::prefix('spm')->name('.spm')->group(function(){
                        Route::post('spm-per-provinsi',[App\Http\Controllers\DataNUWSP\CapaianSPMCtrl::class,'load_provinsi'])->name('.per_provinsi');
                        Route::post('spm-per-kota',[App\Http\Controllers\DataNUWSP\CapaianSPMCtrl::class,'load_kota'])->name('.per_kota');
                        Route::post('spm-rekap',[App\Http\Controllers\DataNUWSP\CapaianSPMCtrl::class,'load_rekap'])->name('.rekap');
                    });
                });

                Route::prefix('/{tahun}/tema1')->name('.tema1')->group(function(){
                    Route::post('pagu_urusan',[App\Http\Controllers\Tema\Tema1Ctrl::class,'pagu_urusan'])->name('.pagu_urusan');
                    
                    Route::post('pagu_program',[App\Http\Controllers\Tema\Tema1Ctrl::class,'pagu_program'])->name('.pagu_program');
                    Route::post('pagu_giat_air',[App\Http\Controllers\Tema\Tema1Ctrl::class,'pagu_giat_air'])->name('.pagu_giat_air');
                    Route::post('rekap',[App\Http\Controllers\Tema\Tema1Ctrl::class,'rekap_daerah'])->name('.rekap'); 
                    Route::post('data-rkpd-subkegiatan-perdaerah',[App\Http\Controllers\Tema\Tema1Ctrl::class,'detail_subgiat_perpemda'])->name('.detail_subgiat_perpemda'); 
                

                    Route::prefix('anggaran')->name('.anggaran')->group(function(){
                        Route::post('anggaran-program',[App\Http\Controllers\Tema\Tema1Ctrl::class,'anggaran_program'])->name('.ang_pro');
                        Route::post('anggaran-giat',[App\Http\Controllers\Tema\Tema1Ctrl::class,'anggaran_kegiatan'])->name('.ang_keg');
                        Route::post('anggaran-rekap',[App\Http\Controllers\Tema\Tema1Ctrl::class,'rekap_anggaran'])->name('.ang_rekap');
                        Route::post('anggran-apbd-pemda-sub-giat/level-1',[App\Http\Controllers\Tema\Tema1Ctrl::class,'anggaran_subgiat_pemda_1'])->name('.ang_subgiat_pemda_l1');
                        Route::post('anggran-apbd-pemda-sub-giat/level-2',[App\Http\Controllers\Tema\Tema1Ctrl::class,'anggaran_subgiat_pemda_2'])->name('.ang_subgiat_pemda_l2');
                        Route::post('anggran-apbd-pemda-sub-giat/level-3',[App\Http\Controllers\Tema\Tema1Ctrl::class,'anggaran_subgiat_pemda_3'])->name('.ang_subgiat_pemda_l3');
                        Route::post('anggran-apbd-pemda-sub-giat/level-4',[App\Http\Controllers\Tema\Tema1Ctrl::class,'anggaran_subgiat_pemda_4'])->name('.ang_subgiat_pemda_l4');
                        Route::post('anggran-apbd-perpemda-sub-giat',[App\Http\Controllers\Tema\Tema1Ctrl::class,'anggaran_subkegiatan_perdaerah'])->name('.ang_per_pemda_subgiat');
                        Route::post('anggaran-sub-giat',[App\Http\Controllers\Tema\Tema1Ctrl::class,'anggaran_subkegiatan'])->name('.ang_subkeg');

                    });

                });

                Route::prefix('{tahun}/tema2')->name('.tema2')->group(function(){
                    Route::prefix('DDUB')->name('.ddub')->group(function(){
                        Route::post('ddub-keterisian-provinsi',[App\Http\Controllers\Tema\Tema2Ctrl::class,'ddub_ket_pro'])->name('.ddub_ket_pro');
                        Route::post('ddub-keterisian-kota',[App\Http\Controllers\Tema\Tema2Ctrl::class,'ddub_ket_kota'])->name('.ddub_ket_kota');
                
                    });

                    Route::prefix('tipologi-hubungan-pemda-bumdam')->name('.tipologi_hub_pemda')->group(function(){
                        Route::post('tipologi-hub-keterisian-provinsi',[App\Http\Controllers\Tema\Tema2Ctrl::class,'load_hubungan_pro'])->name('.load_hubungan_pro');
                        Route::post('tipologi-hub-keterisian-kota',[App\Http\Controllers\Tema\Tema2Ctrl::class,'load_hubungan_kota'])->name('.load_hubungan_kota');
                        Route::post('tipologi-hub-keterisian-rekap',[App\Http\Controllers\Tema\Tema2Ctrl::class,'load_rekap_hubungan'])->name('.load_rekap_hubungan');

                    });
                    
                });


                Route::prefix('timeline/{tahun}')->middleware('auth:api')->name('.timeline')->group(function(){
                    Route::post('/load',[App\Http\Controllers\SistemInformasi\TimelineCtrl::class,'load_data'])->name('.load');
                    Route::post('/add/{id}/child',[App\Http\Controllers\SistemInformasi\TimelineCtrl::class,'store_child'])->name('.add_child');
                    Route::post('/store',[App\Http\Controllers\SistemInformasi\TimelineCtrl::class,'store'])->name('.store');
                    Route::post('/update/{id}',[App\Http\Controllers\SistemInformasi\TimelineCtrl::class,'update'])->name('.update');
                    Route::delete('/delete/{id}',[App\Http\Controllers\SistemInformasi\TimelineCtrl::class,'delete'])->name('.delete');
                });

            
            Route::get('json_schema/{tahun}/{id_dataset}/',[App\Http\Controllers\DatasetCtrl::class,'ajax_data'])->name('.ajax_dataset');
            Route::post('wget-chart/{tahun}/{kode_analisa}',[App\Http\Controllers\SistemInformasi\Analish\WidgetCtrl::class,'draw'])->name('.analisa.draw');

            Route::post('json_data/{tahun}/{id_dataset}/',[App\Http\Controllers\DatasetCtrl::class,'ajax_data'])->name('.ajax_data');
            Route::post('json_data-1/{tahun}/{id_dataset}/',[App\Http\Controllers\DatasetCtrl::class,'ajax_table'])->name('.ajax_table');
            Route::get('json_dataset/{tahun}/{id_tema}',[App\Http\Controllers\TemaCtrl::class,'ajax_dataset'])->name('.tema.ajax_dataset');
            Route::get('source-pemda',[App\Http\Controllers\Public\MasterPemdaCtrl::class,'get_pemda'])->name('.pemda');
            Route::get('get-dataset-detail/{tahun}/{tw}/{id_dataset}/',[App\Http\Controllers\DatasetCtrl::class,'get_dataset'])->name('.get_dataset_detail_level');
            Route::get('build-widget-dataset/{tahun}/{tw}',[App\Http\Controllers\DatasetCtrl::class,'build_widget'])->name('.get_widget_dataset');

        });

       
});


