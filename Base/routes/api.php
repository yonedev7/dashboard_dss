<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
include  __DIR__.'/api-web.php';


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/app-rkpd_apbd-distribute',function(){
    $d= require public_path('rkpd_bot/rkpd_wabot.php');
    $d['link']=url('api/app-rkpd_apbd-distribute/'.$d['file_path']);
    $d['host']=url('');

    return $d;
});

Route::get('/app-rkpd_apbd-distribute/downloads/{file_name}',function($fileName){
  if (!$fileName || !Storage::disk('rkpd_apbd_app_distribute')->exists($fileName)) {
       abort(404);
   }
   return response()->download(public_path('rkpd_bot/downloads/'.$fileName), $fileName, [
       'Content-Type'  => Storage::disk('rkpd_apbd_app_distribute')->mimeType($fileName),
   ]);
});


Route::middleware(['auth:sanctum','can:getApiData'])->group(function(){
    Route::get('/dataset/{kode}/{tahun}',[App\Http\Controllers\API\dataset::class, 'get_data']);
});

Route::get('v/{tahun}/rkpd-keterisian',[App\Http\Controllers\DataRKPD\keterisianCTRL::class, 'keterisian']);
