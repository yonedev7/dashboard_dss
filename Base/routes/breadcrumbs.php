<?php

$Stahun=session('access_tahun')??date('Y');

Breadcrumbs::for('dash.validator', function ($trail) use ($Stahun) {
    $trail->push('Admin', route('dash.si.index',['tahun'=>$Stahun]));
    $trail->push('Validator Data');

});

Breadcrumbs::for('dash.validator.tambah', function ($trail) use ($Stahun) {
    $trail->push('Admin', route('dash.si.index',['tahun'=>$Stahun]));
    $trail->push('Validato Data',route('dash.si.schema.data.index',['tahun'=>$Stahun]));
    $trail->push('Tambah Validator Data');
});

Breadcrumbs::for('dash.validator.detail_sumber', function ($trail) use ($Stahun) {
    $trail->push('Admin', route('dash.si.index',['tahun'=>$Stahun]));
    $trail->push('Validator Data',route('dash.si.schema.data.index',['tahun'=>$Stahun]));
    $trail->push('Detail Validator Data');
});

Breadcrumbs::for('dash.validator.tambah_data', function ($trail,$sumber) use ($Stahun) {
    $trail->push('Admin', route('dash.si.index',['tahun'=>$Stahun]));
    $trail->push('Sumber data',route('dash.si.schema.data.index',['tahun'=>$Stahun]));
    $trail->push($sumber->name,route('dash.si.schema.data.detail_sumber',['tahun'=>$Stahun,'id'=>$sumber->id]));
    $trail->push('Tambah data');
});

Breadcrumbs::for('dash.validator.edit_data', function ($trail,$sumber) use ($Stahun) {
    $trail->push('Admin', route('dash.si.index',['tahun'=>$Stahun]));
    $trail->push('Validator Data',route('dash.si.schema.data.index',['tahun'=>$Stahun]));
    $trail->push($sumber->name,route('dash.si.schema.data.detail_sumber',['tahun'=>$Stahun,'id'=>$sumber->id]));
    $trail->push('Ubah Data');
});


Breadcrumbs::for('dash.metadata', function ($trail) use ($Stahun) {
    $trail->push('Admin', route('dash.si.index',['tahun'=>$Stahun]));
    $trail->push('Datasets');

});

Breadcrumbs::for('dash.metadata.tambah', function ($trail) use ($Stahun) {
    $trail->push('Admin', route('dash.si.index',['tahun'=>$Stahun]));
    $trail->push('Datasets',route('dash.si.schema.metadata.index',['tahun'=>$Stahun]));
    $trail->push('Tambah');
});

