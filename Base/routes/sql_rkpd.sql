select hsk.tahun,REGEXP_REPLACE(dj.nama_sub_tahap,'\s\s+',' ','g') as tahap,hsk.id_daerah,rd.kode_ddn_2 as kodepemda,rd.nama_daerah,
bd.kode_bidang_urusan ,bd.nama_bidang_urusan,
skpd.kode_skpd ,skpd.nama_skpd,
p.kode_program ,p.nama_program,
k.kode_giat,k.nama_giat,
sk.kode_sub_giat ,sk.nama_sub_giat,
hsk.pagu
from anggaran.d_jadwal dj 
join hist_sub_bl hsk on hsk.id_jadwal =dj.id_jadwal and hsk.tahun=dj.tahun and hsk.id_daerah=dj.id_daerah 
join public.r_daerah rd on rd.id_daerah =hsk.id_daerah
join public.r_bidang_urusan bd on bd.id_urusan =hsk.id_urusan and bd.tahun =hsk.tahun and bd.id_daerah =hsk.id_daerah 
join public.r_skpd skpd on skpd.id_skpd =hsk.id_skpd and skpd.tahun =hsk.tahun and skpd.id_daerah =hsk.id_daerah 
join public.r_program p on p.id_program =hsk.id_program  and p.tahun=hsk.tahun and p.id_daerah =hsk.id_daerah 
join public.r_giat k on k.id_giat =hsk.id_giat and k.tahun =hsk.tahun and k.id_daerah =hsk.id_daerah 
join public.r_sub_giat sk on sk.id_sub_giat =hsk.id_sub_giat and sk.tahun =hsk.tahun and sk.id_daerah =hsk.id_daerah
where bd.kode_bidang_urusan ='1.03' and p.kode_program ilike '1.03.02%' and dj.id_tahap=1 and dj.id_jadwal in ( select max(dd.id_jadwal) from anggaran.d_jadwal as dd where dd.id_tahap =1 group by dd.id_daerah order by max(dd.waktu_selesai) desc ) 