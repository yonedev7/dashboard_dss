<?php

Route::prefix('session/{tahun}/slug/{tm}')->name('dash')->middleware('bindTahun')->group(function(){
    Route::get('/','Dashboard/MenusCtrl@index')->name('tm.index');
    Route::get('/sort-view/{id}','Dashboard/MenusCtrl@rekap')->name('tm.sort-view');
    Route::prefix('dataset/')->group(function(){
        Route::get('rkpd','Dashbord/RKPDCtrl@index')->name('.rkpd.index');
        Route::get('ddub','Dashbord/DDUBCtrl@index')->name('.ddub.index');
        Route::get('apbd','Dashbord/APBDCtrl@index')->name('.apbd.index');
        Route::get('bpkp','Dashbord/BPKPCtrl@index')->name('.bpkp.index');
        Route::get('sat','Dashbord/SATCtrl@index')->name('.sat.index');
    });
    Route::get('/detail/{id}','Dashboard/MenusCtrl@rekap')->name('tm.detail');
});