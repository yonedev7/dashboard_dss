<?php
Route::get('test-view',function(){
    return view('test');
});
// dashboard
Route::prefix('dash/{tahun}/sistem-informasi')->name('dash.si')->middleware(['auth:web','bindTahun'])->group(function(){
    Route::get('/',[App\Http\Controllers\SistemInformasi\HomeCtrl::class,'index'])->name('.index');
    Route::get('/my-dash',[App\Http\Controllers\SistemInformasi\Analish\WidgetCtrl::class,'dashbord'])->name('.my_dash');

   

    Route::prefix('account')->name('.account')->group(function(){
        Route::get('/',[App\Http\Controllers\UserController::class,'index'])->name('.index');
        Route::post('/update-profile',[App\Http\Controllers\UserController::class,'update_profile'])->name('.update');
        Route::post('/update-password',[App\Http\Controllers\UserController::class,'update_password'])->name('.update_password');

    });


    Route::prefix('schema')->name('.schema')->group(function(){
        Route::prefix('data')->name('.data')->group(function(){
            Route::get('/',[App\Http\Controllers\SistemInformasi\MasterDataCtrl::class,'index'])->name('.index');

            Route::get('/tambah-validator',[App\Http\Controllers\SistemInformasi\MasterDataCtrl::class,'tambah_validator'])->name('.tambah_validator');
            Route::post('/store-validator',[App\Http\Controllers\SistemInformasi\MasterDataCtrl::class,'store_validator'])->name('.store_validator');
            Route::delete('/delete-validator/{id_validator}',[App\Http\Controllers\SistemInformasi\MasterDataCtrl::class,'delete_validator'])->name('.delete_validator');

            Route::get('/detail-validator/{id_validator}',[App\Http\Controllers\SistemInformasi\MasterDataCtrl::class,'detail_validator'])->name('.detail_validator');
            Route::get('/detail-validator/{id_validator}/tambah-data',[App\Http\Controllers\SistemInformasi\MasterDataCtrl::class,'tambah_data'])->name('.tambah_data');
            Route::post('/detail-validator/{id_validator}/tambah-data',[App\Http\Controllers\SistemInformasi\MasterDataCtrl::class,'store_data'])->name('.store_data');
            Route::get('/detail-validator/{id_validator}/edit-data/{id_data}',[App\Http\Controllers\SistemInformasi\MasterDataCtrl::class,'edit_data'])->name('.edit_data');
            Route::post('/detail-validator/{id_validator}/update-data/{id_data}',[App\Http\Controllers\SistemInformasi\MasterDataCtrl::class,'update_data'])->name('.update_data');
            Route::delete('/detail-validator/{id_validator}/delete-data/{id_data}',[App\Http\Controllers\SistemInformasi\MasterDataCtrl::class,'delete_data'])->name('.delete_data');
        });

        Route::prefix('dataset')->name('.dataset')->group(function(){
            Route::get('/',[App\Http\Controllers\SistemInformasi\MasterDatasetCtrl::class,'index'])->name('.index');
            Route::get('/tambah',[App\Http\Controllers\SistemInformasi\MasterDatasetCtrl::class,'tambah'])->name('.tambah_dataset');
            Route::post('/store',[App\Http\Controllers\SistemInformasi\MasterDatasetCtrl::class,'store'])->name('.store_dataset');
            Route::get('/detail/{id_dataset}',[App\Http\Controllers\SistemInformasi\MasterDatasetCtrl::class,'detail'])->name('.detail_dataset');
            Route::get('/schema-up/{id_dataset}',[App\Http\Controllers\SistemInformasi\MasterDatasetCtrl::class,'schema_up'])->name('.schema_up');
            Route::post('/schema-up/{id_dataset}',[App\Http\Controllers\SistemInformasi\MasterDatasetCtrl::class,'schema_upload'])->name('.schema_upload');

            Route::put('/update/{id_dataset}',[App\Http\Controllers\SistemInformasi\MasterDatasetCtrl::class,'update'])->name('.update_dataset');
            Route::delete('/delete/{id_dataset}',[App\Http\Controllers\SistemInformasi\MasterDatasetCtrl::class,'delete'])->name('.delete_dataset');
            // Route::post('/detail-sumber/{id}/tambah-data',[App\Http\Controllers\SistemInformasi\MasterDatasetCtrl::class,'store_data'])->name('.store_data_dataset');
            // Route::get('/detail-sumber/{id}/edit-data/{id_data}',[App\Http\Controllers\SistemInformasi\MasterDatasetCtrl::class,'edit_data'])->name('.edit_data_dataset');
            // Route::post('/detail-sumber/{id}/update-data/{id_data}',[App\Http\Controllers\SistemInformasi\MasterDatasetCtrl::class,'update_data'])->name('.update_data_dataset');
            // Route::delete('/detail-sumber/{id}/delete-data/{id_data}',[App\Http\Controllers\SistemInformasi\MasterDatasetCtrl::class,'delete_data'])->name('.delete_data_dataset');
        });

    });

    Route::prefix('dataset-pusat')->name('.dataset_pusat')->group(function(){
        Route::get('/',[App\Http\Controllers\SistemInformasi\MasterDatasetCtrl::class,'index_pusat'])->name('.index');
        Route::get('/{id_dataset}',[App\Http\Controllers\SistemInformasi\MasterDatasetCtrl::class,'pilihan_pemda_dataset'])->name('.pilihan_pemda');
        Route::get('/list/{kodepemda}',[App\Http\Controllers\SistemInformasi\MasterDatasetCtrl::class,'form_dataset_daerah'])->name('.list');


    });

    Route::prefix('rekap')->name('.rekap')->group(function(){
        Route::get('/',[App\Http\Controllers\SistemInformasi\RekapCtrl::class,'index'])->name('.index');
       

        
    });

  


    Route::prefix('dataset')->name('.dataset')->group(function(){

        Route::get('/download-excel/{id_dataset}',[App\Http\Controllers\SistemInformasi\MasterDatasetCtrl::class,'download_excel'])->name('.download_excel');
        Route::post('/upload-excel/{id_dataset}',[App\Http\Controllers\SistemInformasi\MasterDatasetCtrl::class,'upload_excel'])->name('.upload_excel');

    });


    Route::prefix('timeline')->name('.timeline')->group(function(){
        Route::get('/index',[App\Http\Controllers\SistemInformasi\TimelineCtrl::class,'index'])->name('.index');
       
    });

    Route::prefix('lokus')->name('.lokus')->group(function(){
        Route::get('/index',[App\Http\Controllers\SistemInformasi\DaerahCtrl::class,'index'])->name('.index');
        Route::put('/update/{kodepemda}',[App\Http\Controllers\SistemInformasi\DaerahCtrl::class,'update'])->name('.update');

       
    });

    Route::prefix('analisa')->name('.analisa')->group(function(){
        Route::get('/build-widget/{kode_analisa?}',[App\Http\Controllers\SistemInformasi\Analish\WidgetCtrl::class,'new_widget'])->name('.wg_build');
        Route::post('/check-schema/{kode_analisa?}',[App\Http\Controllers\SistemInformasi\Analish\WidgetCtrl::class,'check_schema'])->name('.check_schema');
        Route::get('/canvas/{kode_analisa}',[App\Http\Controllers\SistemInformasi\Analish\WidgetCtrl::class,'canvas_chart'])->name('.canvas');



    });
    Route::prefix('menu')->name('.menu')->group(function(){
        Route::get('/',[App\Http\Controllers\SistemInformasi\TemaCtrl::class,'index'])->name('.index');
        Route::post('/',[App\Http\Controllers\SistemInformasi\TemaCtrl::class,'restructure'])->name('.restructure');
        Route::get('/detail/{id_data}',[App\Http\Controllers\SistemInformasi\TemaCtrl::class,'detail'])->name('.detail');
        Route::put('/update/{id_data}',[App\Http\Controllers\SistemInformasi\TemaCtrl::class,'update'])->name('.update');
        Route::get('/tambah',[App\Http\Controllers\SistemInformasi\TemaCtrl::class,'create'])->name('.create');
        Route::post('/tambah',[App\Http\Controllers\SistemInformasi\TemaCtrl::class,'store'])->name('.store');
        Route::delete('/delete/{id_data}',[App\Http\Controllers\SistemInformasi\TemaCtrl::class,'delete'])->name('.delete');
    });

    Route::prefix('dataset')->name('.dataset_daerah')->group(function(){
        Route::get('/pm/{kodepemda}',[App\Http\Controllers\SistemInformasi\MasterDatasetCtrl::class,'form_dataset_daerah'])->name('.index');
        Route::get('/pm/{kodepemda}/dataset/{id_dataset}',[App\Http\Controllers\SistemInformasi\MasterDatasetCtrl::class,'form_dataset_pemda_detail'])->name('.list');
        Route::get('/pm/{kodepemda}/dataset/{id_dataset}/form-input/{tw}',[App\Http\Controllers\SistemInformasi\MasterDatasetCtrl::class,'form_input_dataset'])->name('.form_input');
        Route::get('/pm/{kodepemda}/dataset/{id_dataset}/form-input-multy/{tw}',[App\Http\Controllers\SistemInformasi\MasterDatasetCtrl::class,'form_input_dataset_multy'])->name('.form_input_multy');
        Route::get('/pm/{kodepemda}/dataset/{id_dataset}/result/{tw}',[App\Http\Controllers\SistemInformasi\MasterDatasetCtrl::class,'form_input_dataset'])->name('.result_input');
        Route::put('/pm/{kodepemda}/dataset/{id_dataset}/tw/{tw}',[App\Http\Controllers\SistemInformasi\MasterDatasetCtrl::class,'form_dataset_submit'])->name('.submit_data');
        Route::get('/pm/{kodepemda}/dataset/{id_dataset}/tw/{tw}/verify',[App\Http\Controllers\SistemInformasi\MasterDatasetCtrl::class,'form_dataset_verifikasi'])->name('.verifikasi_data');
        Route::put('/pm/{kodepemda}/dataset-multy-row/{id_dataset}/tw/{tw}',[App\Http\Controllers\SistemInformasi\MasterDatasetCtrl::class,'form_dataset_submit_multy_row'])->name('.submit_data_multy_row');


    });


    


    Route::prefix('sumber-data')->name('.sumberdata')->group(function(){
        Route::get('/',[App\Http\Controllers\DataNUWSP\DashSumberDataCtrl::class, 'index'])->name('.index');
        Route::get('/view/{id}',[App\Http\Controllers\DataNUWSP\DashSumberDataCtrl::class, 'view'])->name('.view');
        Route::get('/form',[App\Http\Controllers\DataNUWSP\DashSumberDataCtrl::class, 'form'])->name('.form');
        Route::post('/store',[App\Http\Controllers\DataNUWSP\DashSumberDataCtrl::class, 'store'])->name('.store');
        Route::put('/update/{id}',[App\Http\Controllers\DataNUWSP\DashSumberDataCtrl::class, 'update'])->name('.update');
        Route::delete('/delete/{id}',[App\Http\Controllers\DataNUWSP\DashSumberDataCtrl::class, 'delete'])->name('.delete');
    });

    Route::prefix('kategori-data')->name('.kategoridata')->group(function(){
        Route::get('/',[App\Http\Controllers\DataNUWSP\DashKategoriDataCtrl::class, 'index'])->name('.index');
        Route::get('/view/{id}',[App\Http\Controllers\DataNUWSP\DashKategoriDataCtrl::class, 'view'])->name('.view');
        Route::get('/form',[App\Http\Controllers\DataNUWSP\DashKategoriDataCtrl::class, 'form'])->name('.form');
        Route::post('/store',[App\Http\Controllers\DataNUWSP\DashKategoriDataCtrl::class, 'store'])->name('.store');
        Route::put('/update/{id}',[App\Http\Controllers\DataNUWSP\DashKategoriDataCtrl::class, 'update'])->name('.update');
        Route::delete('/delete/{id}',[App\Http\Controllers\DataNUWSP\DashKategoriDataCtrl::class, 'delete'])->name('.delete');
    });

    Route::prefix('data')->name('.data')->group(function(){
        Route::get('/',[App\Http\Controllers\DataNUWSP\DashDataCtrl::class, 'index'])->name('.index');
        Route::get('/view/{id}',[App\Http\Controllers\DataNUWSP\DashDataCtrl::class, 'view'])->name('.view');
        Route::get('/form',[App\Http\Controllers\DataNUWSP\DashDataCtrl::class, 'form'])->name('.form');
        Route::post('/store/{id_sumber}',[App\Http\Controllers\DataNUWSP\DashDataCtrl::class, 'store'])->name('.store');
        Route::put('/update/{id}',[App\Http\Controllers\DataNUWSP\DashDataCtrl::class, 'update'])->name('.update');
        Route::delete('/delete/{id}',[App\Http\Controllers\DataNUWSP\DashDataCtrl::class, 'delete'])->name('.delete');
    });

    Route::prefix('value')->name('.value')->group(function(){
        Route::get('/',[App\Http\Controllers\DataNUWSP\DashValueCtrl::class, 'index'])->name('.index');
        Route::get('/view/{id}',[App\Http\Controllers\DataNUWSP\DashValueCtrl::class, 'view'])->name('.view');
        Route::get('/form',[App\Http\Controllers\DataNUWSP\DashValueCtrl::class, 'form'])->name('.form');
        Route::post('/store',[App\Http\Controllers\DataNUWSP\DashValueCtrl::class, 'store'])->name('.store');
        Route::put('/update/{id}',[App\Http\Controllers\DataNUWSP\DashValueCtrl::class, 'update'])->name('.update');
        Route::delete('/delete/{id}',[App\Http\Controllers\DataNUWSP\DashValueCtrl::class, 'delete'])->name('.delete');
    });
});


