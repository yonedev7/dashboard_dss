<?php

use Illuminate\Support\Facades\Route;

Route::get('/', [App\Http\Controllers\HelperController::class,'direct_home'])->name('direct_home');

Route::get('/queue-clear', function () {
    
	Artisan::call('queue:clear',[
       '--force' => true
    ]);

});

Route::get('phpinfo',function(){
	return phpinfo();
});

Route::get('/list-rekap',function(){
	$dd=DB::table('master.master_pemda')->where('kodepemda','!=','0')->get();
	$list=[];
	foreach($dd as $k=>$d){
		$list[]=[
			'data_name'=>$d->nama_pemda." Urusan PU",
			'data'=>[
				'kodeurusan'=>'1.03',
				'kodepemda'=>((int)$d->kodepemda).""
			],
		];
	}

	return $list;
});

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/home', [App\Http\Controllers\HelperController::class,'direct_home'])->name('home');

use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use App\Models\User;
Route::get('test-data/{tahun}/{tw}/{id_dataset}', function(DataTables $dataTables,$tahun,$tw,$id_dataset,Request $request) {
	$dataset=DB::table('master.dataset')->find($id_dataset);
	$name_table='dataset.'.str_replace('[TAHUN]',$tahun,$dataset->table);
	$tb=explode('.',$name_table);
	$exist_table=false;
	$data=[];
	$paginate=200;
	if(count($tb)>1){
		$exist_table=(int)DB::table('pg_tables')->where('tablename','=',$tb[1])->where('schemaname','=',$tb[0])->count();
	}
	if($exist_table){
		$lokussql=DB::table('master.pemda_lokus as lkg')
		->selectRaw("lkg.kodepemda,(string_agg(distinct(concat(lkg.tipe_bantuan,' - ',lkg.tahun_bantuan)),'@')) as tipe_bantuan")
		->groupBy('lkg.kodepemda')->toSql();

		if(strtoupper($dataset->klasifikasi_1)=='DATA PEMDA'){
			$data=DB::table($name_table.' as d')
			->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
			->leftJoin(DB::raw("(".$lokussql.") as lk"),'lk.kodepemda','=','d.kodepemda')
			->selectRaw('mp.regional_1,mp.regional_2,lk.tipe_bantuan,mp.tahun_proyek,mp.nama_pemda as name,d.*')
			->where('tw',$tw)
			->where('tahun',$tahun);

		}else{
			$data=DB::table($name_table.' as d')
			->join('master.master_pemda as mp','mp.kodepemda','=','d.kodepemda')
			->leftJoin(DB::raw("(".$lokussql.") as lk"),'lk.kodepemda','=','d.kodepemda')
			->join('master.master_bu_pemda as bup',[['d.kodepemda','=','bup.kodepemda'],['d.kodebu','=','bup.id_bu']])
			->join('master.master_bu as bu','bu.id','=','bup.id_bu')
			->selectRaw('bu.nama_bu,mp.regional_1,mp.regional_2,lk.tipe_bantuan,mp.tahun_proyek,mp.nama_pemda as name,d.*')
			->orderBy('d.index','asc')
			->where('tw',$tw)
			->where('tahun',$tahun);
		}

		if($request->kodepemda){
		   $data=$data->where('d.kodepemda','=',$request->kodepemda);
		}
		
		$data= $data->whereIn('d.status',$request->status??[0,1]);

		$data=DB::table(DB::raw("({$data->toSql()}) as sub"))
		->mergeBindings($data);


		// dd(Str::replaceArray('?',$data->getBindings(),$data->toSql()));
		try{

			return $dataTables->queryBuilder($data)->toJson();
			// return ['data'=>($data->items()),
			// 	'last_page'=>$data->lastPage(),
			// 	'curent_page'=>$data->currentPage(),
			// 	'paginate_count'=>$paginate,
			// 	'total'=>$data->total()
			// ];
			
		}catch(Exception $e){

		}

		 
	}

    return $dataTables->queryBuilder($query)->toJson();
});


Route::get('/splace-screen/{tahun}', function ($tahun) {
    return view('sistem_informasi.splace')->with('tahun',$tahun);
})->name('splice');

Route::get('/sss', function () {
	dd(\Slack::to('#general')->send('Hey, finance channel! A new order was created just now!'));
	dd('s');
});


Route::get('test',[App\Http\Controllers\TestController::class,'index']);

Route::get('view-test',function(){
	return view('test');
});


Route::get('/change-tahun/{tahun}',[App\Http\Controllers\HelperController::class,'change_tahun']);

Auth::routes();




Route::get('route-list',function(){
	$routeCollection = Illuminate\Support\Facades\Route::getRoutes();
	return view('route-list')->with('routes',$routeCollection);
			
});

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
	Route::get('upgrade', function () {return view('pages.upgrade');})->name('upgrade'); 
	Route::get('map', function () {return view('pages.maps');})->name('map');
	Route::get('icons', function () {return view('pages.icons');})->name('icons'); 
	Route::get('table-list', function () {return view('pages.tables');})->name('table');
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);
});

Route::get('/rubah-tahun/{tahun}',[App\Http\Controllers\HomeController::class,'rubah_tahun'])->name('rubah_tahun');
Route::get('/rubah-tahun-lokus/{tahun}',[App\Http\Controllers\HomeController::class,'rubah_tahun_lokus'])->name('rubah_tahun_lokus');


Route::prefix('session/{tahun}')->middleware('bindTahun')->group(function(){
	
	// Route::prefix('/tematik')->group(function(){
    //     Route::get('/rkpd-d',[App\Http\Controllers\Tematik\RKPDCtrl::class,'index']);
    // });

	Route::prefix('dataset')->name('dataset')->group(function(){
		Route::get('/detail/{id_dataset}',[App\Http\Controllers\DatasetCtrl::class, 'detail'])->name('.detail');
		Route::get('/{id_dataset}',[App\Http\Controllers\DatasetCtrl::class, 'index'])->name('.index');

	});

	Route::get('home', [App\Http\Controllers\HomeController::class, 'index'])->name('home_index');


	Route::prefix('profile-daerah')->name('pd')->group(function(){
		Route::get('/',[App\Http\Controllers\ProfileDaerahCtrl::class, 'index'])->name('.index');
	});

	Route::prefix('catalog')->name('cat')->group(function(){
		Route::get('/',[App\Http\Controllers\CatalogCtrl::class, 'index'])->name('.index');
	});

	Route::prefix('tema')->name('tema')->group(function(){
		Route::get('/{id_tema}',[App\Http\Controllers\TemaCtrl::class, 'index'])->name('.index');
	});

	Route::prefix('tematik')->name('tematik')->group(function(){
		
		Route::get('/',[App\Http\Controllers\TemaCtrl::class, 'home'])->name('.home');
		Route::get('/load',[App\Http\Controllers\HomeController::class, 'load_tematik'])->name('.load_tematik');
		Route::get('/load-pendukung',[App\Http\Controllers\HomeController::class, 'load_pendukung'])->name('.load_pendukung');

		Route::get('/tema1',[App\Http\Controllers\Tema\Tema1Ctrl::class, 'index'])->name('.tm1.index');
		Route::get('/tema1/perencanaan-penganggaran',[App\Http\Controllers\Tema\Tema1Ctrl::class, 'program_kegiatan'])->name('.tm1.program_kegiatan');
		Route::get('/tema1/anggaran',[App\Http\Controllers\Tema\Tema1Ctrl::class, 'anggaran'])->name('.tm1.anggaran');
		Route::get('/tema1/sinkronisasi/perencanaan-penganggaran',[App\Http\Controllers\Tema\Tema1Ctrl::class, 'rpjmn_rpjmd_index'])->name('.tm1.per_peng');


		Route::get('/tema2',[App\Http\Controllers\Tema\Tema2Ctrl::class, 'index'])->name('.tm2.index');
		Route::get('/tema2/DDUB',[App\Http\Controllers\Tema\Tema2Ctrl::class, 'ddub'])->name('.tm2.ddub');
		Route::get('/tema2/tipologi-pemda',[App\Http\Controllers\Tema\Tema2Ctrl::class, 'tipologi'])->name('.tm2.tipologi');
		Route::get('/tema2/tipologi-hubungan-pemda-bumdam',[App\Http\Controllers\Tema\Tema2Ctrl::class, 'hub_pemda'])->name('.tm2.hub_pemda');

		Route::get('/tema3',[App\Http\Controllers\Tema\Tema3Ctrl::class, 'index'])->name('.tm3.index');
		Route::get('/tema4',[App\Http\Controllers\Tema\Tema4Ctrl::class, 'index'])->name('.tm4.index');
		Route::get('/tema5',[App\Http\Controllers\Tema\Tema5Ctrl::class, 'index'])->name('.tm5.index');





	});

	Route::prefix('data/')->name('data')->group(function(){

		Route::get('ikfd',[App\Http\Controllers\DataNUWSP\IKFDCtrl::class, 'index'])->name('.ikfd');
		Route::get('profile-pemda',[App\Http\Controllers\ProfileDaerahCtrl::class, 'index'])->name('.profile-pemda');
		Route::get('profile-pemda-detail/{kodepemda}',[App\Http\Controllers\ProfileDaerahCtrl::class, 'profile'])->name('.profile-pemda-detail');

		Route::get('bpkp-penilaian',[App\Http\Controllers\DataNUWSP\KinerjaBPKPCtrl::class, 'index'])->name('.bpkp-penilain');
		Route::get('jumlah-penduduk',[App\Http\Controllers\DataNUWSP\JumlahPendudukCtrl::class, 'index'])->name('.jumlah-penduduk');
		Route::get('jumlah-penduduk-miskin',[App\Http\Controllers\DataNUWSP\JumlahPendudukMiskinCtrl::class, 'index'])->name('.penduduk-miskin');
		Route::get('luas-wilayah',[App\Http\Controllers\DataNUWSP\LuasWilCtrl::class, 'index'])->name('.luas-wilayah');
		Route::get('tipologi-urusan',[App\Http\Controllers\DataNUWSP\TipologiUrusanCtrl::class, 'index'])->name('.tipologi-urusan');
		Route::get('penilaian-sat',[App\Http\Controllers\DataNUWSP\SatCtrl::class, 'index'])->name('.sat');
		Route::get('bisplan',[App\Http\Controllers\DataNUWSP\BisnisPlanCtrl::class, 'index'])->name('.bisplan');
		Route::get('pelatihan',[App\Http\Controllers\DataNUWSP\PelatihanCtrl::class, 'index'])->name('.pelatihan');
		Route::get('tema-pelatihan',[App\Http\Controllers\DataNUWSP\TemaPelatihanCtrl::class, 'index'])->name('.tema-pelatihan');
		Route::get('fcr',[App\Http\Controllers\DataNUWSP\KinerjaBPKPCtrl::class, 'index_fcr'])->name('.fcr');
		Route::get('penyertaan-modal-pemda',[App\Http\Controllers\DataNUWSP\PenyertaanModalCtrl::class, 'index'])->name('.penyertaan-modal');


		
		Route::get('spm',[App\Http\Controllers\DataNUWSP\CapaianSPMCtrl::class, 'index'])->name('.spm');


	});

	Route::prefix('data-sektoral')->name('ds')->group(function(){
		Route::get('/',function(){
			return view('pages.data-sektoral.index');
		})->name('.index');
	});
	Route::prefix('perencanaan-pusat')->name('pre-p')->group(function(){
		Route::get('/',[App\Http\Controllers\ProfileDaerahCtrl::class, 'index'])->name('.index');
	});

	Route::prefix('refrensi')->name('ref')->group(function(){
		Route::get('/',[App\Http\Controllers\ProfileDaerahCtrl::class, 'index'])->name('.index');
	});
	Route::prefix('publikasi')->name('pub')->group(function(){
		Route::get('/',[App\Http\Controllers\ProfileDaerahCtrl::class, 'index'])->name('.index');
	});

	Route::prefix('analisa')->name('analis')->group(function(){
		Route::get('/',[App\Http\Controllers\ProfileDaerahCtrl::class, 'index'])->name('.index');
	});
});


Route::prefix('v-1/api/')->name('d-api')->group(function(){

	Route::prefix('sdgs')->name('.sdgs')->group(function(){

	});

	Route::prefix('rpjmn')->name('.rpjmn')->group(function(){

	});
	Route::prefix('rpjmd')->name('.rpjmd')->group(function(){
		
	});

	Route::prefix('bisplan')->name('.bisplan')->group(function(){
		
	});

	Route::prefix('rkpd')->name('.rkpd')->group(function(){
		
	});

	Route::prefix('sat')->name('.sat')->group(function(){
		
	});

	Route::prefix('bppspam')->name('.bppspam')->group(function(){
		
	});

	Route::prefix('modul')->name('.modul')->group(function(){
		
	});

	Route::prefix('analisa')->name('.analisa')->group(function(){
		
	});
});



include  __DIR__.'/web-data.php';
include  __DIR__.'/web-dashboard.php';



