CREATE TABLE IF NOT EXISTS data.data_4 (
                id bigserial NOT NULL,
                kodepemda varchar NULL,
                tahun int4 NULL,
                tw int2 NULL,
                id_dataset bigint,
                val float8 NULL,
                CONSTRAINT data_4_pk PRIMARY KEY (id),
                CONSTRAINT data_4_un UNIQUE (kodepemda, tahun, tw, id_dataset)
            );
            
CREATE TABLE IF NOT EXISTS data_konsensus.data_4 (
                id bigserial NOT NULL,
                kodepemda varchar NULL,
                tahun int4 NULL,
                tw int2 NULL,
                val float8 NULL,
                CONSTRAINT data_4_pk PRIMARY KEY (id),
                CONSTRAINT data_4_un UNIQUE (kodepemda, tahun, tw)
            );
            
            
CREATE INDEX IF NOT EXISTS data_4_kodepemda_idx ON data.data_4 USING btree (kodepemda);
CREATE INDEX IF NOT EXISTS  data_4_tahun_idx ON data.data_4 USING btree (tahun);
CREATE INDEX IF NOT EXISTS  data_4_tw_idx ON data.data_4 USING btree (tw);
CREATE INDEX IF NOT EXISTS  data_4_dataset_idx ON data.data_4 USING btree (id_dataset);
CREATE INDEX IF NOT EXISTS data_4_kodepemda_idx ON data.data_4 USING btree (kodepemda);
CREATE INDEX IF NOT EXISTS  data_4_tahun_idx ON data.data_4 USING btree (tahun);
CREATE INDEX IF NOT EXISTS  data_4_tw_idx ON data.data_4 USING btree (tw);
