CREATE TABLE IF NOT EXISTS data.data_5 (
                id bigserial NOT NULL,
                kodepemda varchar NULL,
                tahun int4 NULL,
                tw int2 NULL,
                id_dataset bigint,
                val varchar(195) NULL,
                CONSTRAINT data_5_pk PRIMARY KEY (id),
                CONSTRAINT data_5_un UNIQUE (kodepemda, tahun, tw, id_dataset)
            );
            
CREATE TABLE IF NOT EXISTS data_konsensus.data_5 (
                id bigserial NOT NULL,
                kodepemda varchar NULL,
                tahun int4 NULL,
                tw int2 NULL,
                val varchar(195) NULL,
                CONSTRAINT data_5_pk PRIMARY KEY (id),
                CONSTRAINT data_5_un UNIQUE (kodepemda, tahun, tw)
            );
            
            
CREATE INDEX IF NOT EXISTS data_5_kodepemda_idx ON data.data_5 USING btree (kodepemda);
CREATE INDEX IF NOT EXISTS  data_5_tahun_idx ON data.data_5 USING btree (tahun);
CREATE INDEX IF NOT EXISTS  data_5_tw_idx ON data.data_5 USING btree (tw);
CREATE INDEX IF NOT EXISTS  data_5_dataset_idx ON data.data_5 USING btree (id_dataset);
CREATE INDEX IF NOT EXISTS data_5_kodepemda_idx ON data.data_5 USING btree (kodepemda);
CREATE INDEX IF NOT EXISTS  data_5_tahun_idx ON data.data_5 USING btree (tahun);
CREATE INDEX IF NOT EXISTS  data_5_tw_idx ON data.data_5 USING btree (tw);
