CREATE TABLE IF NOT EXISTS data.data_6 (
                id bigserial NOT NULL,
                kodepemda varchar NULL,
                tahun int4 NULL,
                tw int2 NULL,
                id_dataset bigint,
                val float8 NULL,
                CONSTRAINT data_6_pk PRIMARY KEY (id),
                CONSTRAINT data_6_un UNIQUE (kodepemda, tahun, tw, id_dataset)
            );
            
CREATE TABLE IF NOT EXISTS data_konsensus.data_6 (
                id bigserial NOT NULL,
                kodepemda varchar NULL,
                tahun int4 NULL,
                tw int2 NULL,
                val float8 NULL,
                CONSTRAINT data_6_pk PRIMARY KEY (id),
                CONSTRAINT data_6_un UNIQUE (kodepemda, tahun, tw)
            );
            
            
CREATE INDEX IF NOT EXISTS data_6_kodepemda_idx ON data.data_6 USING btree (kodepemda);
CREATE INDEX IF NOT EXISTS  data_6_tahun_idx ON data.data_6 USING btree (tahun);
CREATE INDEX IF NOT EXISTS  data_6_tw_idx ON data.data_6 USING btree (tw);
CREATE INDEX IF NOT EXISTS  data_6_dataset_idx ON data.data_6 USING btree (id_dataset);
CREATE INDEX IF NOT EXISTS data_6_kodepemda_idx ON data.data_6 USING btree (kodepemda);
CREATE INDEX IF NOT EXISTS  data_6_tahun_idx ON data.data_6 USING btree (tahun);
CREATE INDEX IF NOT EXISTS  data_6_tw_idx ON data.data_6 USING btree (tw);
