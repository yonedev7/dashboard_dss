CREATE TABLE IF NOT EXISTS data.data_7 (
                id bigserial NOT NULL,
                kodepemda varchar NULL,
                tahun int4 NULL,
                tw int2 NULL,
                id_dataset bigint,
                val float8 NULL,
                CONSTRAINT data_7_pk PRIMARY KEY (id),
                CONSTRAINT data_7_un UNIQUE (kodepemda, tahun, tw, id_dataset)
            );
            
CREATE TABLE IF NOT EXISTS data_konsensus.data_7 (
                id bigserial NOT NULL,
                kodepemda varchar NULL,
                tahun int4 NULL,
                tw int2 NULL,
                val float8 NULL,
                CONSTRAINT data_7_pk PRIMARY KEY (id),
                CONSTRAINT data_7_un UNIQUE (kodepemda, tahun, tw)
            );
            
            
CREATE INDEX IF NOT EXISTS data_7_kodepemda_idx ON data.data_7 USING btree (kodepemda);
CREATE INDEX IF NOT EXISTS  data_7_tahun_idx ON data.data_7 USING btree (tahun);
CREATE INDEX IF NOT EXISTS  data_7_tw_idx ON data.data_7 USING btree (tw);
CREATE INDEX IF NOT EXISTS  data_7_dataset_idx ON data.data_7 USING btree (id_dataset);
CREATE INDEX IF NOT EXISTS data_7_kodepemda_idx ON data.data_7 USING btree (kodepemda);
CREATE INDEX IF NOT EXISTS  data_7_tahun_idx ON data.data_7 USING btree (tahun);
CREATE INDEX IF NOT EXISTS  data_7_tw_idx ON data.data_7 USING btree (tw);
