CREATE TABLE IF NOT EXISTS data.data_202 (
                id bigserial NOT NULL,
                kodepemda varchar NULL,
                tahun int4 NULL,
                tw int2 NULL,
                id_dataset bigint,
                val float8 NULL,
                CONSTRAINT data_202_pk PRIMARY KEY (id),
                CONSTRAINT data_202_un UNIQUE (kodepemda, tahun, tw, id_dataset)
            );
            
CREATE TABLE IF NOT EXISTS data_konsensus.data_202 (
                id bigserial NOT NULL,
                kodepemda varchar NULL,
                tahun int4 NULL,
                tw int2 NULL,
                val float8 NULL,
                CONSTRAINT data_202_pk PRIMARY KEY (id),
                CONSTRAINT data_202_un UNIQUE (kodepemda, tahun, tw)
            );
            
            
CREATE INDEX IF NOT EXISTS data_202_kodepemda_idx ON data.data_202 USING btree (kodepemda);
CREATE INDEX IF NOT EXISTS  data_202_tahun_idx ON data.data_202 USING btree (tahun);
CREATE INDEX IF NOT EXISTS  data_202_tw_idx ON data.data_202 USING btree (tw);
CREATE INDEX IF NOT EXISTS  data_202_dataset_idx ON data.data_202 USING btree (id_dataset);
CREATE INDEX IF NOT EXISTS data_202_kodepemda_idx ON data.data_202 USING btree (kodepemda);
CREATE INDEX IF NOT EXISTS  data_202_tahun_idx ON data.data_202 USING btree (tahun);
CREATE INDEX IF NOT EXISTS  data_202_tw_idx ON data.data_202 USING btree (tw);
