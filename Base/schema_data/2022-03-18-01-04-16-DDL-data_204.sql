CREATE TABLE IF NOT EXISTS data.data_204 (
                id bigserial NOT NULL,
                kodepemda varchar NULL,
                tahun int4 NULL,
                tw int2 NULL,
                id_dataset bigint,
                val float8 NULL,
                CONSTRAINT data_204_pk PRIMARY KEY (id),
                CONSTRAINT data_204_un UNIQUE (kodepemda, tahun, tw, id_dataset)
            );
            
CREATE TABLE IF NOT EXISTS data_konsensus.data_204 (
                id bigserial NOT NULL,
                kodepemda varchar NULL,
                tahun int4 NULL,
                tw int2 NULL,
                val float8 NULL,
                CONSTRAINT data_204_pk PRIMARY KEY (id),
                CONSTRAINT data_204_un UNIQUE (kodepemda, tahun, tw)
            );
            
            
CREATE INDEX IF NOT EXISTS data_204_kodepemda_idx ON data.data_204 USING btree (kodepemda);
CREATE INDEX IF NOT EXISTS  data_204_tahun_idx ON data.data_204 USING btree (tahun);
CREATE INDEX IF NOT EXISTS  data_204_tw_idx ON data.data_204 USING btree (tw);
CREATE INDEX IF NOT EXISTS  data_204_dataset_idx ON data.data_204 USING btree (id_dataset);
CREATE INDEX IF NOT EXISTS data_204_kodepemda_idx ON data.data_204 USING btree (kodepemda);
CREATE INDEX IF NOT EXISTS  data_204_tahun_idx ON data.data_204 USING btree (tahun);
CREATE INDEX IF NOT EXISTS  data_204_tw_idx ON data.data_204 USING btree (tw);
