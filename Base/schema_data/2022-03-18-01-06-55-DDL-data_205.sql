CREATE TABLE IF NOT EXISTS data.data_205 (
                id bigserial NOT NULL,
                kodepemda varchar NULL,
                tahun int4 NULL,
                tw int2 NULL,
                id_dataset bigint,
                val text NULL,
                CONSTRAINT data_205_pk PRIMARY KEY (id),
                CONSTRAINT data_205_un UNIQUE (kodepemda, tahun, tw, id_dataset)
            );
            
CREATE TABLE IF NOT EXISTS data_konsensus.data_205 (
                id bigserial NOT NULL,
                kodepemda varchar NULL,
                tahun int4 NULL,
                tw int2 NULL,
                val text NULL,
                CONSTRAINT data_205_pk PRIMARY KEY (id),
                CONSTRAINT data_205_un UNIQUE (kodepemda, tahun, tw)
            );
            
            
CREATE INDEX IF NOT EXISTS data_205_kodepemda_idx ON data.data_205 USING btree (kodepemda);
CREATE INDEX IF NOT EXISTS  data_205_tahun_idx ON data.data_205 USING btree (tahun);
CREATE INDEX IF NOT EXISTS  data_205_tw_idx ON data.data_205 USING btree (tw);
CREATE INDEX IF NOT EXISTS  data_205_dataset_idx ON data.data_205 USING btree (id_dataset);
CREATE INDEX IF NOT EXISTS data_205_kodepemda_idx ON data.data_205 USING btree (kodepemda);
CREATE INDEX IF NOT EXISTS  data_205_tahun_idx ON data.data_205 USING btree (tahun);
CREATE INDEX IF NOT EXISTS  data_205_tw_idx ON data.data_205 USING btree (tw);
