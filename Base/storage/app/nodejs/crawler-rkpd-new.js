const puppeteer = require('puppeteer');
const { PendingXHR } = require('pending-xhr-puppeteer');
const { functionsIn } = require('lodash');
var md5 = require('md5');
const fs = require('fs');
const { Console } = require('console');
const { Knex } = require('knex');

// xxx

var dataset_rkpd={};
var access_key=null;
var status_rkpd;
var account_user={
    username:"subpkp@bangda.kemendagri.go.id",
    password:"bangdapkp"
};

var access_data={
    tahun_data:2022,
    kodedaerah:"32",
    tahap:5
};



async function exist_name(ind){
    if(ind!='-' && ind!='' && ind && ind!='0'){
        return true;
    }else{
        return false;
    }
} 

const knex = require('knex')({
    client: 'pg',
    connection: {
      host : 'localhost',
      port : 5432,
      user : 'postgres',
      password : 'uba2013',
      database : 'NUWSP'
    },
    userParams:{
        tahun:access_data.tahun_data
    }
});

var schema='rkpd_'+access_data.tahun_data;



function delay(time) {
    return new Promise(function(resolve) { 
        setTimeout(resolve, time)
    });
} 

var browser;
var page;  
var pendingXHR;  


 async function init(){
        browser = await puppeteer.launch(
      { 
          headless: false,
          timeout:6000000
        }
      
      ); // default is true


    page =await  browser.newPage();
    pendingXHR = new PendingXHR(page);
 }

async function get_data(){    
   if(access_data.kodedaerah.length>2){
    access_data['kode_wil']={
        prov:access_data.kodedaerah.substring(0,2),
        kabkot:access_data.kodedaerah.substring(2,4),
        levelAnggaran:'kabkota'

    };
   

    }else{
        access_data['kode_wil']={
            prov:access_data.kodedaerah.substring(0,2),
            kabkot:"00",
            levelAnggaran:'provinsi'
        };
       
    }

 // get-sesion-key   
  await page.goto('https://sipd.kemendagri.go.id/edb_gis/?m=login', {
    waitUntil: 'networkidle2',
  });
  access_key= await page.evaluate(function(){
       var url_action= $('form').attr('action');
        if(url_action){
            url_action=url_action.split('/');
            if(url_action.length>2){
                return url_action[2];
            }
        }
        return url_action;
    });
  
    console.log('access-key :'+access_key);
    
//   check login
  await page.goto('https://sipd.kemendagri.go.id/edb_gis/'+access_key+'/?m=dashboard', {
    waitUntil: 'networkidle2',
  });

  logined=  await page.evaluate(function(){
        const urlSearchParams = new URLSearchParams(window.location.search);
        const params = Object.fromEntries(urlSearchParams.entries());
        if(params.m!=undefined){
            if(params.m=='login'){
                return false;
            }else{
                return true;
            }
        }
        
        return false;
    });

    // login
    if(!logined){
        await page.type('#username', account_user.username);
        await    page.type('#password', account_user.password);
        await Promise.all([
            
            page.click('[type="submit"]'),
            page.waitForNavigation({
                    waitUntil: 'networkidle0',
            })
        ]);
       
    }


    delay(3000);

    // dta={access_data:access_data,access_key:access_key};
    // console.log({
    //         m:"dashboard",
    //         tahun:dta.access_data.tahun_data,
    //         f: "get_jadwal_tahapan",
    //         prov:dta.access_data.kode_wil.prov,
    //         kabkot:dta.access_data.kode_wil.kabkot
    //     });
    // status RKPD

    await page.evaluate(function(dta){
        status_rkpd=null;
        $.get("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/",{
            m:"dashboard",
            tahun:dta.access_data.tahun_data,
            f: "get_jadwal_tahapan",
            prov:dta.access_data.kode_wil.prov,
            kabkot:dta.access_data.kode_wil.kabkot
        },function(res){
            status_rkpd=res;
        });

    },{access_data:access_data,access_key:access_key});
    await pendingXHR.waitForAllXhrFinished();

    status_rkpd=await page.evaluate(function(){
        return status_rkpd;
    },{access_data:access_data,access_key:access_key});
   


    // dataset_rkpd['semua_status']=status_rkpd;
    dataset_rkpd={
        'kodedaerah':access_data.kodedaerah,
        'tahun':access_data.tahun_data,
        'updated_at':new Date(),
        
    };
    // console.log(status_rkpd,'status---------------');

    if(status_rkpd[access_data.tahap-1]!=undefined){
        access_data['status_data']=status_rkpd[access_data.tahap-1];
        access_data['tahap']=access_data.tahap;

    }else{
        access_data['status_data']=status_rkpd[status_rkpd.length-1];
        access_data['tahap']=status_rkpd.length;
    }


    dataset_rkpd['status_data']=access_data['status_data'];
    dataset_rkpd['tahap']=access_data['tahap'];
    dataset_rkpd['dataset']=[];

    // console.log(dataset_rkpd);
    // await browser.close();
    // ambil urusan
    // masukan status
    db_req={};

    db_req['id_meta'] = await knex.select('id').from(schema+'.meta_rkpd').where(
        {
            kodepemda: dataset_rkpd.kodedaerah,
            tahap:dataset_rkpd.tahap,
            tahun:dataset_rkpd.tahun
        }
    ).then(async (idn)=>{
        if(!(idn.length)){
            newid=await knex(schema+'.meta_rkpd').insert(
                {
                    kodepemda: dataset_rkpd.kodedaerah,
                    tahap:dataset_rkpd.tahap,
                    tahun:dataset_rkpd.tahun,
                    kode_daerah_sipd:dataset_rkpd.status_data.id_daerah,
                    log_ang_sipd:dataset_rkpd.status_data.ang_log,
                    log_sipd:dataset_rkpd.status_data.log,
                    updated_at:dataset_rkpd.updated_at
                 }
            ).returning('id');
            return newid[0];
        }else{
            return (idn[0].id);
        }
    });


   

    await page.evaluate(function(dta){
        dss_buffer=null;
        $.get("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/",{
            m:"dashboard",
            tahun:dta.access_data.tahun_data,
            f: "perbidangUrusan",
            prov:dta.access_data.kode_wil.prov,
            levelAnggaran:dta.access_data.levelAnggaran,
            kabkot:dta.access_data.kode_wil.kabkot,
            tahap:dta.access_data.status_data.max,
            log:1
           
        },function(res){
            dss_buffer=res;
        });

    },{access_data:access_data,access_key:access_key});
    await pendingXHR.waitForAllXhrFinished();

    dataset_rkpd['_dataset_per_urusan']=await page.evaluate(function(){
        return dss_buffer;
    },{access_data:access_data,access_key:access_key});
    delay(100);

    // urusan



 

    index_urusan=0;
    while(dataset_rkpd['_dataset_per_urusan'].length>index_urusan){
        
        console.log('-- RKPD '+access_data.kodedaerah+' Tahun '+access_data.tahun_data+' Tahap '+access_data.tahap+' -- '+(index_urusan/dataset_rkpd['_dataset_per_urusan'].length*100)+'%');
        dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd']=[];
        var ko_b=dataset_rkpd['_dataset_per_urusan'][index_urusan]['kodebidang'];
        if(ko_b.includes("1.03")||ko_b.includes("1.04")||ko_b.includes("2.15")||ko_b.includes("2.16")||ko_b.includes("2.20")||ko_b.includes("2.21")||ko_b.includes("3.25")){
        
        var parent_data={
            tahun:access_data.tahun_data,
            prov:access_data.kode_wil.prov,
            kabkot:access_data.kode_wil.kabkot,
            kodebidang:dataset_rkpd['_dataset_per_urusan'][index_urusan].kodebidang,
            log:1,
            jadwal:access_data.status_data.max,
            tab:"bidang"
        };
        //  db_req['id_urusan']=await knex(schema+'.urusan').upsert(
        //     [
        //         {
        //             kodepemda: dataset_rkpd.kodedaerah,
        //             tahap:dataset_rkpd.tahap,
        //             tahun:dataset_rkpd.tahun,
        //             kodebidang:dataset_rkpd['_dataset_per_urusan'][index_urusan].kodebidang
        //         },
        //          {
        //             id_meta:db_req['id_meta'], 
        //             kodepemda: dataset_rkpd.kodedaerah,
        //             tahap:dataset_rkpd.tahap,
        //             tahun:dataset_rkpd.tahun,
        //             kodebidang:parent_data['kodebidang'],
        //             updated_at:dataset_rkpd.updated_at
        //          }],
        //  ['id']
        //  );


        db_req['id_urusan'] = await knex.select('id').from(schema+'.urusan').where(
            {
                kodepemda: dataset_rkpd.kodedaerah,
                tahap:dataset_rkpd.tahap,
                tahun:dataset_rkpd.tahun,
                kodebidang:dataset_rkpd['_dataset_per_urusan'][index_urusan].kodebidang
            }
        ).then(async (idn)=>{
            if(!(idn.length)){
                newid=await knex(schema+'.urusan').insert(
                    {
                        id_meta:db_req['id_meta'], 
                        kodepemda: dataset_rkpd.kodedaerah,
                        tahap:dataset_rkpd.tahap,
                        tahun:dataset_rkpd.tahun,
                        kodebidang:parent_data['kodebidang'],
                        updated_at:dataset_rkpd.updated_at
                     }
                ).returning('id');
                return newid[0];
            }else{
                return (idn[0].id);
            }
        });

        await page.evaluate(function(dta){
            dss_buffer=null;
            $.post("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/?m=dashboard&f=perbidang_skpd",{
                tahun:dta.tahun,
                prov:dta.prov,
                kodekot:dta.kabkot,
                kodebidang:dta.kodebidang,
                log:dta.log,
                jadwal:dta.jadwal,
                tab:dta.tab,
            },function(res){
                dss_buffer=res;
            });

        },parent_data);
        await pendingXHR.waitForAllXhrFinished();


        dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd']=await page.evaluate(function(){
            return dss_buffer;
        },{access_data:access_data,access_key:access_key});
        delay(100);

            index_skpd=0;
            while(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'].length>index_skpd){
                // ambil program skpd
                
                parent_data['id_skpd']=dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd].id_unit;
                parent_data['kode_skpd']=dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd].kode_skpd||'null';

                dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program']=[];

                // db_req['id_skpd']=await knex(schema+'.skpd').upsert(
                //     [
                //         {
                //             kodepemda: dataset_rkpd.kodedaerah,
                //             tahap:dataset_rkpd.tahap,
                //             tahun:dataset_rkpd.tahun,
                //             kodebidang:parent_data['kodebidang'],
                //             kodeskpd:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['kode_skpd'],


                //         },
                //          {
                //             parent:db_req['id_urusan'], 
                //             kodepemda: dataset_rkpd.kodedaerah,
                //             tahap:dataset_rkpd.tahap,
                //             tahun:dataset_rkpd.tahun,
                //             kodebidang:parent_data['kodebidang'],
                //             kodeskpd:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['kode_skpd'],
                //            
                //             updated_at:dataset_rkpd.updated_at
                //          }],
                //  ['id']
                //  );

                db_req['id_skpd'] = await knex.select('id').from(schema+'.skpd').where(
                    {
                        kodepemda: dataset_rkpd.kodedaerah,
                        tahap:dataset_rkpd.tahap,
                        tahun:dataset_rkpd.tahun,
                        kodebidang:parent_data['kodebidang'],
                        kodeskpd:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['kode_skpd']||'null',


                    }
                ).then(async (idn)=>{
                    if(!(idn.length)){
                        newid=await knex(schema+'.skpd').insert(
                            {
                                parent:db_req['id_urusan'], 
                                kodepemda: dataset_rkpd.kodedaerah,
                                tahap:dataset_rkpd.tahap,
                                tahun:dataset_rkpd.tahun,
                                kodebidang:parent_data['kodebidang'],
                                kodeskpd:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['kode_skpd']||'null',
                                nama_skpd:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['nama_skpd']||'null',
                                updated_at:dataset_rkpd.updated_at
                            }
                        ).returning('id');
                        return newid[0];
                    }else{
                        return (idn[0].id);
                    }
                });
        
        
                await page.evaluate(function(dta){
                    dss_buffer=null;
                    $.post("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/?m=dashboard&f=program",{
                        tahun:dta.tahun,
                        prov:dta.prov,
                        kodekot:dta.kabkot,
                        kodebidang:dta.kodebidang,
                        skpd:dta.id_skpd,
                        log:dta.log,
                        jadwal:dta.jadwal,
                        tab:dta.tab,
                    },function(res){
                        dss_buffer=res;
                    });

                },parent_data);
                await pendingXHR.waitForAllXhrFinished();

                dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program']=await page.evaluate(function(){
                    return dss_buffer;
                },{access_data:access_data,access_key:access_key});
                delay(100);


                index_program=0;

                while( dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'].length>index_program){
                    // ambil kegiatan
                    parent_data['kode_program']=dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program].kode_program;

                    dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan']=[];
                    // db_req['id_program']=await knex(schema+'.program').upsert(
                    //     [
                    //         {
                    //             kodepemda: dataset_rkpd.kodedaerah,
                    //             tahap:dataset_rkpd.tahap,
                    //             tahun:dataset_rkpd.tahun,
                    //             kodebidang:parent_data['kodebidang'],
                    //             kodeskpd:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['kode_skpd'],
                    //             kodeprogram:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program].kode_program,

        
        
                    //         },
                    //          {
                    //             parent:db_req['id_skpd'], 
                    //             kodepemda: dataset_rkpd.kodedaerah,
                    //             tahap:dataset_rkpd.tahap,
                    //             tahun:dataset_rkpd.tahun,
                    //             kodebidang:parent_data['kodebidang'],
                    //             kodeskpd:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['kode_skpd'],
                    //            
                    //             kodeprogram:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program].kode_program,
                    //             updated_at:dataset_rkpd.updated_at
                    //          }],
                    //  ['id']
                    //  );

                    db_req['id_program'] = await knex.select('id').from(schema+'.program').where(
                        {
                            kodepemda: dataset_rkpd.kodedaerah,
                            tahap:dataset_rkpd.tahap,
                            tahun:dataset_rkpd.tahun,
                            kodebidang:parent_data['kodebidang'],
                            kodeskpd:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['kode_skpd'],
                            kodeprogram:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program].kode_program,

        
        
                        }
                    ).then(async (idn)=>{
                        if(!(idn.length)){
                            newid=await knex(schema+'.program').insert(
                                {
                                    parent:db_req['id_skpd'], 
                                    kodepemda: dataset_rkpd.kodedaerah,
                                    tahap:dataset_rkpd.tahap,
                                    tahun:dataset_rkpd.tahun,
                                    kodebidang:parent_data['kodebidang'],
                                    kodeskpd:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['kode_skpd'],
                                    kodeprogram:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program].kode_program,
                                    updated_at:dataset_rkpd.updated_at
                                }
                            ).returning('id');
                            return newid[0];
                        }else{
                            return (idn[0].id);
                        }
                    });
            
                    await page.evaluate(function(dta){
                        dss_buffer=null;
                        $.post("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/?m=dashboard&f=kegiatan",{
                            tahun:dta.tahun,
                            prov:dta.prov,
                            kodekot:dta.kabkot,
                            kodebidang:dta.kodebidang,
                            skpd:dta.id_skpd,
                            log:dta.log,
                            program:dta.kode_program,
                            jadwal:dta.jadwal,
                            tab:dta.tab,
                        },function(res){
                            dss_buffer=res;
                        });
        
                    },parent_data);
                    await pendingXHR.waitForAllXhrFinished();
        
                    dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan']=await page.evaluate(function(){
                        return dss_buffer;
                    },{access_data:access_data,access_key:access_key});
                    
                    delay(100);

                    // ambil outcome
                    dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator']=[];
                    
                    await page.evaluate(function(dta){
                        dss_buffer=null;
                        $.post("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/?m=dashboard&f=indikator_prog",{
                            tahun:dta.tahun,
                            prov:dta.prov,
                            kodekot:dta.kabkot,
                            kodebidang:dta.kodebidang,
                            skpd:dta.kode_skpd,
                            log:dta.log,
                            program:dta.kode_program,
                            jadwal:dta.jadwal,
                            tab:dta.tab,
                        },function(res){
                            dss_buffer=res;
                        });
        
                    },parent_data);

                    await pendingXHR.waitForAllXhrFinished();
        
                    dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator']=await page.evaluate(function(){
                        return dss_buffer;
                    },{access_data:access_data,access_key:access_key});
                    delay(100);
                    index_program_ind=0;

                    while(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'].length>index_program_ind){
                        if(await exist_name(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'][index_program_ind].namaoutcome)){
                            var kodeindp=md5(dataset_rkpd.kodedaerah+'|'+dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'][index_program_ind].tahun_rpjmd_awal+'-'+dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'][index_program_ind].tahun_rpjmd_akhir+'-'+dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'][index_program_ind].namaoutcome);
                            var rpjmd_awal=dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'][index_program_ind].tahun_rpjmd_awal;
                            var rpjmd_ahir=dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'][index_program_ind].tahun_rpjmd_akhir;
                            
                            await knex.select('id').from(schema+'.indikator').where(
                                {
                                    kodepemda: dataset_rkpd.kodedaerah,
                                    rpjmd_awal:rpjmd_awal,
                                    rpjmd_ahir:rpjmd_ahir,
                                    kodebidang:dataset_rkpd['_dataset_per_urusan'][index_urusan].kodebidang,
                                    kodeskpd:parent_data['kode_skpd'],
                                    kodeprogram:parent_data['kode_program'],
                                    kodeindikator:kodeindp,
                                    jenis:3
        
                                }
                            ).then(async (idn)=>{
                                if(!(idn.length)){
                                    newid=await knex(schema+'.indikator').insert(
                                        {
                                            kodepemda: dataset_rkpd.kodedaerah,
                                            rpjmd_awal:rpjmd_awal,
                                            rpjmd_ahir:rpjmd_ahir,
                                            kodebidang:parent_data['kodebidang'],
                                            kodeskpd:parent_data['kode_skpd'],
                                            kodeprogram:parent_data['kode_program'],
                                            kodeindikator:kodeindp,
                                            jenis:3,
                                            target_1:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'][index_program_ind].target_1,
                                            target_2:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'][index_program_ind].target_2,
                                            target_3:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'][index_program_ind].target_3,
                                            target_4:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'][index_program_ind].target_4,
                                            target_5:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'][index_program_ind].target_5,
                                            namaindikator:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'][index_program_ind].namaoutcome,
                                            satuan:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'][index_program_ind].satuanoutcome||'',
                                            updated_at:dataset_rkpd.updated_at
                                        }
                                    ).returning('id');
                                    return newid[0];
                                }else{
                                    var idxx=(idn[0].id);
                                    var target_f=(rpjmd_awal-access_data.tahun_data)+1;
                                    if(target_f>5){
                                        target_f=5;
                                    }else if(target_f<1){
                                        target_f=1;
                                    }
                                    if(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'][index_program_ind]['target_'+target_f]){
                                        await knex.select('target_'+target_f).from(schema+'.indikator').where({
                                            id:idxx
                                        }).then(async (target)=>{
                                            if(target==null){
                                                var upin={};
                                                upin['target_'+target_f]=dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'][index_program_ind]['target_'+target_f];
                                                await knex(schema+'.indikator').update(upin).where({
                                                    id:idxx
                                                });
                                            }
                                        });
                                    }
                                    return idxx;
                                }
                            });
                        }

                        index_program_ind++;
                    }

                    index_kegiatan=0;

                    while(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'].length>index_kegiatan){
                        // ambil sub kegiatan
                        dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan']=[];
                        parent_data['kode_kegiatan']=dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan].kode_giat;
                    
                        // db_req['id_kegiatan']=await knex(schema+'.kegiatan').upsert(
                        //     [
                        //         {
                        //             kodepemda: dataset_rkpd.kodedaerah,
                        //             tahap:dataset_rkpd.tahap,
                        //             tahun:dataset_rkpd.tahun,
                        //             kodebidang:parent_data['kodebidang'],
                        //             kodeskpd:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['kode_skpd'],
                        //             kodeprogram:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program].kode_program,
                        //             kodekegiatan:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan].kode_giat,
            
                        //         },
                        //          {
                        //             parent:db_req['id_program'], 
                        //             kodepemda: dataset_rkpd.kodedaerah,
                        //             tahap:dataset_rkpd.tahap,
                        //             tahun:dataset_rkpd.tahun,
                        //             kodebidang:parent_data['kodebidang'],
                        //             kodeskpd:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['kode_skpd'],
                        //            
                        //             kodeprogram:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program].kode_program,
                        //             kodekegiatan:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan].kode_giat,

                        //             updated_at:dataset_rkpd.updated_at
                        //          }],
                        //  ['id']
                        //  );

                    
                        db_req['id_kegiatan'] = await knex.select('id').from(schema+'.kegiatan').where(
                            {
                                kodepemda: dataset_rkpd.kodedaerah,
                                tahap:dataset_rkpd.tahap,
                                tahun:dataset_rkpd.tahun,
                                kodebidang:parent_data['kodebidang'],
                                kodeskpd:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['kode_skpd'],
                                kodeprogram:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program].kode_program,
                                kodekegiatan:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan].kode_giat,
        
            
                            }
                        ).then(async (idn)=>{
                            if(!(idn.length)){
                                newid=await knex(schema+'.kegiatan').insert(
                                    {
                                        parent:db_req['id_program'], 
                                        kodepemda: dataset_rkpd.kodedaerah,
                                        tahap:dataset_rkpd.tahap,
                                        tahun:dataset_rkpd.tahun,
                                        kodebidang:parent_data['kodebidang'],
                                        kodeskpd:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['kode_skpd'],
                                    
                                        kodeprogram:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program].kode_program,
                                        kodekegiatan:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan].kode_giat,

                                        updated_at:dataset_rkpd.updated_at
                                    }
                                ).returning('id');
                                return newid[0];
                            }else{
                                return (idn[0].id);
                            }
                        });
                        
                    
                        await page.evaluate(function(dta){
                            dss_buffer=null;
                            
                            $.post("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/?m=dashboard&f=subkegiatan",{
                                tahun:dta.tahun,
                                prov:dta.prov,
                                kodekot:dta.kabkot,
                                kodebidang:dta.kodebidang,
                                skpd:dta.id_skpd,
                                log:dta.log,
                                program:dta.kode_program,
                                jadwal:dta.jadwal,
                                tab:dta.tab,
                                kegiatan:dta.kode_kegiatan
                            },function(res){
                                dss_buffer=res;
                            });
            
                        },parent_data);
                        await pendingXHR.waitForAllXhrFinished();
            
                        dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan']=await page.evaluate(function(){
                            return dss_buffer;
                        },{access_data:access_data,access_key:access_key});
                        delay(100);



                        // ambil indikator kegiatan
                        dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator']=[];

                        await page.evaluate(function(dta){
                            dss_buffer=null;
                            $.post("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/?m=dashboard&f=indikator_keg",{
                                tahun:dta.tahun,
                                prov:dta.prov,
                                kodekot:dta.kabkot,
                                kodebidang:dta.kodebidang,
                                skpd:dta.kode_skpd,
                                log:dta.log,
                                program:dta.kode_program,
                                jadwal:dta.jadwal,
                                tab:dta.tab,
                                kegiatan:dta.kode_kegiatan
                            },function(res){
                                dss_buffer=res;
                            });
            
                        },parent_data);
                        await pendingXHR.waitForAllXhrFinished();
            
                        dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator']=await page.evaluate(function(){
                            return dss_buffer;
                        },{access_data:access_data,access_key:access_key});
                        delay(100);

                        // xxxx

                        index_kegiatan_ind=0;
                        while(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator'].length>index_kegiatan_ind){
                            if(await exist_name(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator'][index_kegiatan_ind].namaoutput)){
                                var kodeindp=md5(dataset_rkpd.kodedaerah+'|'+dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator'][index_kegiatan_ind].tahun_rpjmd_awal+'-'+dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator'][index_kegiatan_ind].tahun_rpjmd_akhir+'-'+dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator'][index_kegiatan_ind].namaoutput);
                                var rpjmd_awal=dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator'][index_kegiatan_ind].tahun_rpjmd_awal;
                                var rpjmd_ahir=dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator'][index_kegiatan_ind].tahun_rpjmd_akhir;
                               
                                await knex.select('id').from(schema+'.indikator').where(
                                    {
                                        kodepemda: dataset_rkpd.kodedaerah,
                                        rpjmd_awal:rpjmd_awal,
                                        rpjmd_ahir:rpjmd_ahir,
                                        kodebidang:dataset_rkpd['_dataset_per_urusan'][index_urusan].kodebidang,
                                        kodeskpd:parent_data['kode_skpd'],
                                        kodeprogram:parent_data['kode_program'],
                                        kodekegiatan:parent_data['kode_kegiatan'],
                                        kodeindikator:kodeindp,
                                        jenis:4,
                                    }
                                ).then(async (idn)=>{
                                    if(!(idn.length)){
                                        newid=await knex(schema+'.indikator').insert(
                                            {
                                                kodepemda: dataset_rkpd.kodedaerah,
                                                rpjmd_awal:rpjmd_awal,
                                                rpjmd_ahir:rpjmd_ahir,
                                                kodeskpd:parent_data['kode_skpd'],
                                                jenis:4,
                                                kodebidang:parent_data['kodebidang'],
                                                kodeprogram:parent_data['kode_program'],
                                                kodekegiatan:parent_data['kode_kegiatan'],
                                                kodeindikator:kodeindp,
                                                target_1:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator'][index_kegiatan_ind].target_1,
                                                target_2:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator'][index_kegiatan_ind].target_2,
                                                target_3:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator'][index_kegiatan_ind].target_3,
                                                target_4:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator'][index_kegiatan_ind].target_4,
                                                target_5:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator'][index_kegiatan_ind].target_5,
                                                namaindikator:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator'][index_kegiatan_ind].namaoutput,
                                                satuan:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator'][index_kegiatan_ind].satuanoutput||'',
                                                updated_at:dataset_rkpd.updated_at
                                            }
                                        ).returning('id');


                                        return newid[0];
                                    }else{
                                        var idxx=(idn[0].id);
                                        var target_f=(rpjmd_awal-access_data.tahun_data)+1;
                                        if(target_f>5){
                                            target_f=5;
                                        }else if(target_f<1){
                                            target_f=1;
                                        }
                                        if(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator'][index_kegiatan_ind]['target_'+target_f]){
                                            await knex.select('target_'+target_f).from(schema+'.indikator').where({
                                                id:idxx
                                            }).then(async (target)=>{
                                                if(target==null){
                                                    var upin={};
                                                    upin['target_'+target_f]=dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_indikator'][index_kegiatan_ind]['target_'+target_f];
                                                    await knex(schema+'.indikator').update(upin).where({
                                                        id:idxx
                                                    });
                                                }
                                            });
                                        }
                                        return idxx;
                                    }
                                });
                            }

                            index_kegiatan_ind++;
                        }

                        index_sub_kegiatan=0;

                        while(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'].length>index_sub_kegiatan){
                            // ambil indikator sub kegiatan
                            parent_data['kode_sub_kegiatan']=dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'][index_sub_kegiatan].kode_sub_giat;
                            dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'][index_sub_kegiatan]['_indikator']=[];
                            // db_req['id_sub_kegiatan']=await knex(schema+'.kegiatan').upsert(
                            //     [
                            //         {
                            //             kodepemda: dataset_rkpd.kodedaerah,
                            //             tahap:dataset_rkpd.tahap,
                            //             tahun:dataset_rkpd.tahun,
                            //             kodebidang:parent_data['kodebidang'],
                            //             kodeskpd:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['kode_skpd'],
                            //             kodeprogram:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program].kode_program,
                            //             kodekegiatan:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan].kode_giat,
                            //             kodesubkegiatan:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'][index_sub_kegiatan].kode_sub_kegiatan,

                
                            //         },
                            //          {
                            //             parent:db_req['id_kegiatan'], 
                            //             kodepemda: dataset_rkpd.kodedaerah,
                            //             tahap:dataset_rkpd.tahap,
                            //             tahun:dataset_rkpd.tahun,
                            //             kodebidang:parent_data['kodebidang'],
                            //             kodeskpd:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['kode_skpd'],
                            //            
                            //             kodeprogram:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program].kode_program,
                            //             kodekegiatan:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan].kode_giat,
                            //             kodesubkegiatan:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'][index_sub_kegiatan].kode_sub_kegiatan,
                                        
                            //             updated_at:dataset_rkpd.updated_at
                            //          }],
                            //  ['id']
                            //  );

                            db_req['id_sub_kegiatan'] = await knex.select('id').from(schema+'.sub_kegiatan').where(
                                {
                                    kodepemda: dataset_rkpd.kodedaerah,
                                    tahap:dataset_rkpd.tahap,
                                    tahun:dataset_rkpd.tahun,
                                    kodebidang:parent_data['kodebidang'],
                                    kodeskpd:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['kode_skpd'],
                                    kodeprogram:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program].kode_program,
                                    kodekegiatan:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan].kode_giat,
                                    kodesubkegiatan:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'][index_sub_kegiatan].kode_sub_giat,

                
                                }
                            ).then(async (idn)=>{
                                if(!(idn.length)){
                                    newid=await knex(schema+'.sub_kegiatan').insert(
                                        {
                                            parent:db_req['id_kegiatan'], 
                                            kodepemda: dataset_rkpd.kodedaerah,
                                            tahap:dataset_rkpd.tahap,
                                            tahun:dataset_rkpd.tahun,
                                            kodebidang:parent_data['kodebidang'],
                                            kodeskpd:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['kode_skpd'],
                                            anggaran:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'][index_sub_kegiatan].sum,
                                            kodeprogram:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program].kode_program,
                                            kodekegiatan:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan].kode_giat,
                                            kodesubkegiatan:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'][index_sub_kegiatan].kode_sub_giat,
                                            updated_at:dataset_rkpd.updated_at
                                        }
                                    ).returning('id');
                                    return newid[0];
                                }else{
                                    return (idn[0].id);
                                }
                            });


                            await page.evaluate(function(dta){
                                dss_buffer=null;
                                $.post("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/?m=dashboard&f=indikator_sub_keg",{
                                    tahun:dta.tahun,
                                    prov:dta.prov,
                                    kodekot:dta.kabkot,
                                    kodebidang:dta.kodebidang,
                                    skpd:dta.kode_skpd,
                                    log:dta.log,
                                    program:dta.kode_program,
                                    jadwal:dta.jadwal,
                                    tab:dta.tab,
                                    kegiatan:dta.kode_kegiatan,
                                    subkegiatan:dta.kode_sub_kegiatan
                                },function(res){
                                    dss_buffer=res;
                                });
                
                            },parent_data);
                            await pendingXHR.waitForAllXhrFinished();
                
                            dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'][index_sub_kegiatan]['_indikator']=await page.evaluate(function(){
                                return dss_buffer;
                            },{access_data:access_data,access_key:access_key});

                            delay(100);

                            index_sub_kegiatan_ind=0;
                            while(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'][index_sub_kegiatan]['_indikator'].length>index_sub_kegiatan_ind){
                                if(await exist_name(dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'][index_sub_kegiatan]['_indikator'][index_sub_kegiatan_ind].tolokukur_indikator )){
                                    var kodeindp=md5(dataset_rkpd.kodedaerah+'|'+access_data.tahun_data+'-'+dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'][index_sub_kegiatan]['_indikator'][index_sub_kegiatan_ind].tolokukur_indikator);
                                    var rpjmd_awal=dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'][index_sub_kegiatan]['_indikator'][index_sub_kegiatan_ind].tahun;
                                    var rpjmd_ahir=dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'][index_sub_kegiatan]['_indikator'][index_sub_kegiatan_ind].tahun;
                                
                                    await knex.select('id').from(schema+'.indikator_sub').where(
                                        {
                                            parent:db_req['id_sub_kegiatan'],
                                            kodepemda: dataset_rkpd.kodedaerah,
                                            tahap:dataset_rkpd.tahap,
                                            tahun:dataset_rkpd.tahun,
                                            kodeskpd:parent_data['kode_skpd'],
                                            kodebidang:dataset_rkpd['_dataset_per_urusan'][index_urusan].kodebidang,
                                            kodeprogram:parent_data['kode_program'],
                                            kodekegiatan:parent_data['kode_kegiatan'],
                                            kodesubkegiatan:parent_data['kode_sub_kegiatan'],
                                            kodeindikator:kodeindp,
                                            jenis:5,

                                        }
                                    ).then(async (idn)=>{
                                        if(!(idn.length)){
                                            newid=await knex(schema+'.indikator_sub').insert(
                                                {
                                                    parent:db_req['id_sub_kegiatan'],
                                                    kodepemda: dataset_rkpd.kodedaerah,
                                                    tahap:dataset_rkpd.tahap,
                                                    tahun:dataset_rkpd.tahun,
                                                    kodeskpd:parent_data['kode_skpd'],
                                                    kodebidang:parent_data['kodebidang'],
                                                    kodeprogram:parent_data['kode_program'],
                                                    kodekegiatan:parent_data['kode_kegiatan'],
                                                    kodesubkegiatan:parent_data['kode_sub_kegiatan'],
                                                    kodeindikator:kodeindp,
                                                    jenis:5,
                                                    pagu:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'][index_sub_kegiatan]['_indikator'][index_sub_kegiatan_ind].pagu_indikator,
                                                    namaindikator:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'][index_sub_kegiatan]['_indikator'][index_sub_kegiatan_ind].tolokukur_indikator,
                                                    satuan:dataset_rkpd['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_kegiatan'][index_kegiatan]['_sub_kegiatan'][index_sub_kegiatan]['_indikator'][index_sub_kegiatan_ind].satuan_indikator||'',
                                                    updated_at:dataset_rkpd.updated_at
                                                }
                                            ).returning('id');
                                            return newid[0];
                                        }else{
                                            var idxx=(idn[0].id);
                                            
                                            return idxx;
                                        }
                                    });
                                }

                                index_sub_kegiatan_ind++;
                            }


                            index_sub_kegiatan++;

                        }


                        index_kegiatan++;
                    }


                    index_program++;
                }

                index_skpd++;

            }
        }

        index_urusan++;

    }
    // WHILE URUSAN

//  await client.connect();
// var dbo= await client.db('RKPD');
// dbo.collection(access_data.tahun_data+'').insertOne(dataset_rkpd, function(err, res) {
//     if (err) throw err;
//         console.log("1 document inserted");
// });

fs.writeFileSync(__dirname+"/RKPD-JSON/rkpd-"+access_data.tahun_data+"-"+access_data.kodedaerah+"-"+dataset_rkpd.tahap+".json", JSON.stringify(dataset_rkpd));

  delay(1000);
  await page.reload({ waitUntil: ["networkidle0", "domcontentloaded"] });
   







//   await browser.close();
    };





// var last_d=10;

(async ()=>{

    await init();
    var egine=process.argv[2];
    

    var last_d=await knex('schema_data.test').select('name').where({
        id:egine
    });
    
    if(!last_d){
        last_d=[{name:10}];
        await knex().table('schema_data.test').insert({id:egine,name:10});
    }

    last_d=parseInt(last_d[0].name);


    console.log('initialisasi--- data tahun '+access_data.tahun_data);
    var interasi=1;
    var daerah=['11','12','13','14','15','16','17','18','19','21','31','32',
    '33','34','35','36','51','52','53','61','62','63','64','65','71','72',
    '73','74','75','76','81','82','91','92','1101','1102','1103','1104','1105',
    '1106','1107','1108','1109','1110','1111','1112','1113','1114','1115','1116','1117','1118','1171','1172',
    '1173','1174','1175','1201','1202','1203','1204','1205','1206','1207','1208','1209','1210','1211',
    '1212','1213','1214','1215','1216','1217','1218','1219','1220','1221','1222','1223','1224','1225',
    '1271','1272','1273','1274','1275','1276','1277','1278','1301','1302','1303','1304','1305','1306',
    '1307','1308','1309','1310','1311','1312','1371','1372','1373','1374','1375','1376','1377','1401',
    '1402','1403','1404','1405','1406','1407','1408','1409','1410','1471','1472','1501','1502','1503',
    '1504','1505','1506','1507','1508','1509','1571','1572','1601','1602','1603','1604','1605','1606',
    '1607','1608','1609','1610','1611','1612','1613','1671','1672','1673','1674','1701','1702','1703',
    '1704','1705','1706','1707','1708','1709','1771','1801','1802','1803','1804','1805','1806','1807',
    '1808','1809','1810','1811','1812','1813','1871','1872','1901','1902','1903','1904','1905','1906',
    '1971','2101','2102','2103','2104','2105','2171','2172','3101','3171','3172','3173','3174','3175',
    '3201','3202','3203','3204','3205','3206','3207','3208','3209','3210','3211','3212','3213','3214',
    '3215','3216','3217','3218','3271','3272','3273','3274','3275','3276','3277','3278','3279','3301',
    '3302','3303','3304','3305','3306','3307','3308','3309','3310','3311','3312','3313','3314','3315',
    '3316','3317','3318','3319','3320','3321','3322','3323','3324','3325','3326','3327','3328','3329',
    '3371','3372','3373','3374','3375','3376','3401','3402','3403','3404','3471','3501','3502','3503',
    '3504','3505','3506','3507','3508','3509','3510','3511','3512','3513','3514','3515','3516','3517',
    '3518','3519','3520','3521','3522','3523','3524','3525','3526','3527','3528','3529','3571','3572',
    '3573','3574','3575','3576','3577','3578','3579','3601','3602','3603','3604','3671','3672','3673',
    '3674','5101','5102','5103','5104','5105','5106','5107','5108','5171','5201','5202','5203','5204',
    '5205','5206','5207','5208','5271','5272','5301','5302','5303','5304','5305','5306','5307','5308',
    '5309','5310','5311','5312','5313','5314','5315','5316','5317','5318','5319','5320','5321','5371',
    '6101','6102','6103','6104','6105','6106','6107','6108','6109','6110','6111','6112','6171','6172',
    '6201','6202','6203','6204','6205','6206','6207','6208','6209','6210','6211','6212','6213','6271',
    '6301','6302','6303','6304','6305','6306','6307','6308','6309','6310','6311','6371','6372','6401',
    '6402','6403','6407','6408','6409','6411','6471','6472','6474','6501','6502','6503','6504','6571',
    '7101','7102','7103','7104','7105','7106','7107','7108','7109','7110','7111','7171','7172','7173',
    '7174','7201','7202','7203','7204','7205','7206','7207','7208','7209','7210','7211','7212','7271',
    '7301','7302','7303','7304','7305','7306','7307','7308','7309','7310','7311','7312','7313','7314',
    '7315','7316','7317','7318','7322','7324','7326','7371','7372','7373','7401','7402','7403','7404',
    '7405','7406','7407','7408','7409','7410','7411','7412','7413','7414','7415','7471','7472','7501',
    '7502','7503','7504','7505','7571','7601','7602','7603','7604','7605','7606','8101','8102','8103',
    '8104','8105','8106','8107','8108','8109','8171','8172','8201','8202','8203','8204','8205','8206',
    '8207','8208','8271','8272','9101','9102','9103','9104','9105','9106','9107','9108','9109','9110',
    '9111','9112','9113','9114','9115','9116','9117','9118','9119','9120','9121','9122','9123','9124',
    '9125','9126','9127','9128','9171','9201','9202','9203','9204','9205','9206','9207','9208','9209',
    '9210','9211','9212','9271'];

    index_daerah=0;
    while(daerah.length>=index_daerah){
        if(last_d<parseInt(daerah[index_daerah])){
            if(index_daerah==daerah.length){
                index_daerah=0;
                interasi++;
            }
            if(index_daerah==0){
                console.log('////////////---- iterasi ke '+interasi+' ------/////////');
            }
            access_data['kodedaerah']=daerah[index_daerah];
            console.log('egine---runing ---',egine);
            console.log("---------- session change for kode daerah "+daerah[index_daerah]+'--------');
            console.log("");

            await get_data();

            await knex('schema_data.test').update({
                name:daerah[index_daerah]
            }).where({
                id:1
            });

        }else{
            console.log('skip -- kodedaerah '+daerah[index_daerah]);
        }
        index_daerah++;

    }
})();