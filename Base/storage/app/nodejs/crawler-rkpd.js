const puppeteer = require('puppeteer');
const { PendingXHR } = require('pending-xhr-puppeteer');
// database connection
const { MongoClient } = require("mongodb");
const client = new MongoClient("mongodb://127.0.0.1:27017");
async ()=>{

 try {
    await client.connect();
    await client.db("admin").command({ ping: 1 });
} finally {
    // await client.close();
    }
}
var dbo = client.db("RKPD");

var dataset_rkpd={};
var access_key=null;
var status_rkpd;
var account_user={
    username:"subpkp@bangda.kemendagri.go.id",
    password:"bangdapkp"
};
var access_data={
    tahun_data:2022,
    kodedaerah:"32",
    tahap:5
};

if(access_data.kodedaerah.length>2){
    access_data['kode_wil']={
        prov:access_data.kodedaerah.substring(0,2),
        kabkot:access_data.kodedaerah,
        levelAnggaran:'kabkota'

    };
   

}else{
    access_data['kode_wil']={
        prov:access_data.kodedaerah.substring(0,2),
        kabkot:"00",
        levelAnggaran:'provinsi'
    };
   
}

(async () => {
    function delay(time) {
        return new Promise(function(resolve) { 
            setTimeout(resolve, time)
        });
        }
  
  const browser = await puppeteer.launch(
      { 
        //   headless: false,
          timeout:6000000
        }
      
      ); // default is true

  const page = await browser.newPage();
  const pendingXHR = new PendingXHR(page);

 // get-sesion-key   
  await page.goto('https://sipd.kemendagri.go.id/edb_gis/?m=login', {
    waitUntil: 'networkidle2',
  });
  access_key= await page.evaluate(function(){
       var url_action= $('form').attr('action');
        if(url_action){
            url_action=url_action.split('/');
            if(url_action.length>2){
                return url_action[2];
            }
        }
        return url_action;
    });
  
    console.log('access-key :'+access_key);
    
//   check login
  await page.goto('https://sipd.kemendagri.go.id/edb_gis/'+access_key+'/?m=dashboard', {
    waitUntil: 'networkidle2',
  });

  logined=  await page.evaluate(function(){
        const urlSearchParams = new URLSearchParams(window.location.search);
        const params = Object.fromEntries(urlSearchParams.entries());
        if(params.m!=undefined){
            if(params.m=='login'){
                return false;
            }else{
                return true;
            }
        }
        
        return false;
    });

    // login
    if(!logined){
        await page.type('#username', account_user.username);
        await page.type('#password', account_user.password);
        await page.click('[type="submit"]');
        
    }
    await page.waitForNavigation();
    


    
    // status RKPD
    await page.evaluate(function(dta){
        status_rkpd=null;
        $.get("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/",{
            m:"dashboard",
            tahun:dta.access_data.tahun_data,
            f: "get_jadwal_tahapan",
            prov:dta.access_data.kode_wil.prov,
            kabkot:dta.access_data.kode_wil.kabkot
        },function(res){
            status_rkpd=res;
        });

    },{access_data:access_data,access_key:access_key});
    await pendingXHR.waitForAllXhrFinished();

    status_rkpd=await page.evaluate(function(){
        return status_rkpd;
    },{access_data:access_data,access_key:access_key});
   




    // dataset_rkpd['semua_status']=status_rkpd;
    dataset_rkpd={
        'kodedaerah':access_data.kodedaerah,
        'tahun':access_data.tahun,
        'updated_at':null,
        
    };

    if(status_rkpd[access_data.tahap-1]!=undefined){
        access_data['status_data']=status_rkpd[access_data.tahap-1];
        access_data['tahap']=access_data.tahap;

    }else{
        access_data['status_data']=status_rkpd[status_rkpd.length-1];
        access_data['tahap']=status_rkpd.length;
    }
    

    dataset_rkpd['status_data']=access_data['status_data'];
    dataset_rkpd['tahap']=access_data['tahap'];
    dataset_rkpd['dataset']=[];
    // ambil urusan
    await page.evaluate(function(dta){
        dss_buffer=null;
        $.get("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/",{
            m:"dashboard",
            tahun:dta.access_data.tahun_data,
            f: "perbidangUrusan",
            prov:dta.access_data.kode_wil.prov,
            levelAnggaran:dta.access_data.levelAnggaran,
            kabkot:dta.access_data.kode_wil.kabkot,
            tahap:dta.access_data.status_data.max,
            log:1
           
        },function(res){
            dss_buffer=res;
        });

    },{access_data:access_data,access_key:access_key});
    await pendingXHR.waitForAllXhrFinished();

    dataset_rkpd['dataset_per_urusan']=await page.evaluate(function(){
        return dss_buffer;
    },{access_data:access_data,access_key:access_key});
    delay(500);


    index_urusan=0;
    while(dataset_rkpd['dataset_per_urusan'].length>index_urusan){
        console.log('-- RKPD '+access_data.kodedaerah+' Tahun '+access_data.tahun_data+' Tahap '+access_data.tahap+' -- '+(index_urusan/dataset_rkpd['dataset_per_urusan'].length*100)+'%');
        dataset_rkpd['dataset_per_urusan'][index_urusan]['skpd']=[];
        var parent_data={
            tahun:access_data.tahun_data,
            prov:access_data.kode_wil.prov,
            kabkot:access_data.kode_wil.kabkot,
            kodebidang:dataset_rkpd['dataset_per_urusan'][index_urusan].kodebidang,
            log:1,
            jadwal:access_data.status_data.max,
            tab:"bidang"
        };
        await page.evaluate(function(dta){
            dss_buffer=null;
            $.post("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/?m=dashboard&f=perbidang_skpd",{
                tahun:dta.tahun,
                prov:dta.prov,
                kodekot:dta.kabkot,
                kodebidang:dta.kodebidang,
                log:dta.log,
                jadwal:dta.jadwal,
                tab:dta.tab,
            },function(res){
                dss_buffer=res;
            });

        },parent_data);
        await pendingXHR.waitForAllXhrFinished();


        dataset_rkpd['dataset_per_urusan'][index_urusan]['skpd']=await page.evaluate(function(){
            return dss_buffer;
        },{access_data:access_data,access_key:access_key});
        delay(500);

        index_skpd=0;
        while(dataset_rkpd['dataset_per_urusan'][index_urusan]['skpd'].length>index_skpd){
            // ambil program skpd
            parent_data['id_skpd']=dataset_rkpd['dataset_per_urusan'][index_urusan]['skpd'][index_skpd].id_unit;
            parent_data['kode_skpd']=dataset_rkpd['dataset_per_urusan'][index_urusan]['skpd'][index_skpd].kode_skpd;

            dataset_rkpd['dataset_per_urusan'][index_urusan]['skpd'][index_skpd]['program']=[];
            await page.evaluate(function(dta){
                dss_buffer=null;
                $.post("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/?m=dashboard&f=program",{
                    tahun:dta.tahun,
                    prov:dta.prov,
                    kodekot:dta.kabkot,
                    kodebidang:dta.kodebidang,
                    skpd:dta.id_skpd,
                    log:dta.log,
                    jadwal:dta.jadwal,
                    tab:dta.tab,
                },function(res){
                    dss_buffer=res;
                });

            },parent_data);
            await pendingXHR.waitForAllXhrFinished();

            dataset_rkpd['dataset_per_urusan'][index_urusan]['skpd'][index_skpd]['program']=await page.evaluate(function(){
                return dss_buffer;
            },{access_data:access_data,access_key:access_key});
            delay(500);


            index_program=0;

            while( dataset_rkpd['dataset_per_urusan'][index_urusan]['skpd'][index_skpd]['program'].length>index_program){
                // ambil kegiatan
                parent_data['kode_program']=dataset_rkpd['dataset_per_urusan'][index_urusan]['skpd'][index_skpd]['program'][index_program].kode_program;

                dataset_rkpd['dataset_per_urusan'][index_urusan]['skpd'][index_skpd]['program'][index_program]['kegiatan']=[];
                await page.evaluate(function(dta){
                    dss_buffer=null;
                    $.post("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/?m=dashboard&f=kegiatan",{
                        tahun:dta.tahun,
                        prov:dta.prov,
                        kodekot:dta.kabkot,
                        kodebidang:dta.kodebidang,
                        skpd:dta.id_skpd,
                        log:dta.log,
                        program:dta.kode_program,
                        jadwal:dta.jadwal,
                        tab:dta.tab,
                    },function(res){
                        dss_buffer=res;
                    });
    
                },parent_data);
                await pendingXHR.waitForAllXhrFinished();
    
                dataset_rkpd['dataset_per_urusan'][index_urusan]['skpd'][index_skpd]['program'][index_program]['kegiatan']=await page.evaluate(function(){
                    return dss_buffer;
                },{access_data:access_data,access_key:access_key});
                delay(500);

                // ambil outcome
                dataset_rkpd['dataset_per_urusan'][index_urusan]['skpd'][index_skpd]['program'][index_program]['indikator']=[];
                
                await page.evaluate(function(dta){
                    dss_buffer=null;
                    $.post("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/?m=dashboard&f=indikator_prog",{
                        tahun:dta.tahun,
                        prov:dta.prov,
                        kodekot:dta.kabkot,
                        kodebidang:dta.kodebidang,
                        skpd:dta.kode_skpd,
                        log:dta.log,
                        program:dta.kode_program,
                        jadwal:dta.jadwal,
                        tab:dta.tab,
                    },function(res){
                        dss_buffer=res;
                    });
    
                },parent_data);
                await pendingXHR.waitForAllXhrFinished();
    
                dataset_rkpd['dataset_per_urusan'][index_urusan]['skpd'][index_skpd]['program'][index_program]['indikator']=await page.evaluate(function(){
                    return dss_buffer;
                },{access_data:access_data,access_key:access_key});
                delay(500);
                
                index_kegiatan=0;
                while(dataset_rkpd['dataset_per_urusan'][index_urusan]['skpd'][index_skpd]['program'][index_program]['kegiatan'].length<index_kegiatan){
                    // ambil sub kegiatan
                    dataset_rkpd['dataset_per_urusan'][index_urusan]['skpd'][index_skpd]['program'][index_program]['kegiatan'][index_kegiatan]['sub_kegiatan']=[];
                    parent_data['kode_kegiatan']=dataset_rkpd['dataset_per_urusan'][index_urusan]['skpd'][index_skpd]['program'][index_program]['kegiatan'][index_kegiatan].kode_kegiatan;

                    await page.evaluate(function(dta){
                        dss_buffer=null;
                        $.post("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/?m=dashboard&f=subkegiatan",{
                            tahun:dta.tahun,
                            prov:dta.prov,
                            kodekot:dta.kabkot,
                            kodebidang:dta.kodebidang,
                            skpd:dta.id_skpd,
                            log:dta.log,
                            program:dta.kode_program,
                            jadwal:dta.jadwal,
                            tab:dta.tab,
                            kegiatan:dta.kode_kegiatan
                        },function(res){
                            dss_buffer=res;
                        });
        
                    },parent_data);
                    await pendingXHR.waitForAllXhrFinished();
        
                    dataset_rkpd['dataset_per_urusan'][index_urusan]['skpd'][index_skpd]['program'][index_program]['kegiatan'][index_kegiatan]['sub_kegiatan']=await page.evaluate(function(){
                        return dss_buffer;
                    },{access_data:access_data,access_key:access_key});
                    delay(500);


                    // ambil indikator kegiatan
                    dataset_rkpd['dataset_per_urusan'][index_urusan]['skpd'][index_skpd]['program'][index_program]['kegiatan'][index_kegiatan]['indikator']=[];

                    await page.evaluate(function(dta){
                        dss_buffer=null;
                        $.post("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/?m=dashboard&f=indikator_keg",{
                            tahun:dta.tahun,
                            prov:dta.prov,
                            kodekot:dta.kabkot,
                            kodebidang:dta.kodebidang,
                            skpd:dta.kode_skpd,
                            log:dta.log,
                            program:dta.kode_program,
                            jadwal:dta.jadwal,
                            tab:dta.tab,
                            kegiatan:dta.kode_kegiatan
                        },function(res){
                            dss_buffer=res;
                        });
        
                    },parent_data);
                    await pendingXHR.waitForAllXhrFinished();
        
                    dataset_rkpd['dataset_per_urusan'][index_urusan]['skpd'][index_skpd]['program'][index_program]['kegiatan'][index_kegiatan]['indikator']=await page.evaluate(function(){
                        return dss_buffer;
                    },{access_data:access_data,access_key:access_key});
                    delay(500);
                    index_sub_kegiatan=0;

                    while(dataset_rkpd['dataset_per_urusan'][index_urusan]['skpd'][index_skpd]['program'][index_program]['kegiatan'][index_kegiatan]['sub_kegiatan'].length<index_sub_kegiatan){
                        // ambil indikator sub kegiatan
                        parent_data['kode_sub_kegiatan']=dataset_rkpd['dataset_per_urusan'][index_urusan]['skpd'][index_skpd]['program'][index_program]['kegiatan'][index_kegiatan]['sub_kegiatan'][index_sub_kegiatan].kode_sub_kegiatan;
                        dataset_rkpd['dataset_per_urusan'][index_urusan]['skpd'][index_skpd]['program'][index_program]['kegiatan'][index_kegiatan]['sub_kegiatan'][index_sub_kegiatan]['indikator']=[];

                        await page.evaluate(function(dta){
                            dss_buffer=null;
                            $.post("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/?m=dashboard&f=indikator_sub_keg",{
                                tahun:dta.tahun,
                                prov:dta.prov,
                                kodekot:dta.kabkot,
                                kodebidang:dta.kodebidang,
                                skpd:dta.kode_skpd,
                                log:dta.log,
                                program:dta.kode_program,
                                jadwal:dta.jadwal,
                                tab:dta.tab,
                                kegiatan:dta.kode_kegiatan,
                                subkegiatan:dta.kode_sub_kegiatan
                            },function(res){
                                dss_buffer=res;
                            });
            
                        },parent_data);
                        await pendingXHR.waitForAllXhrFinished();
            
                        dataset_rkpd['dataset_per_urusan'][index_urusan]['skpd'][index_skpd]['program'][index_program]['kegiatan'][index_kegiatan]['sub_kegiatan'][index_sub_kegiatan]['indikator']=await page.evaluate(function(){
                            return dss_buffer;
                        },{access_data:access_data,access_key:access_key});

                        delay(500);

                        index_sub_kegiatan++;

                    }


                    index_kegiatan++;
                }


                index_program++;
            }

            index_skpd++;

        }

        index_urusan++;

 }

 await client.connect();
var dbo= await client.db('RKPD');
dbo.collection(access_data.tahun_data+'').insertOne(dataset_rkpd, function(err, res) {
    if (err) throw err;
        console.log("1 document inserted");
});

  delay(1000);
  await page.reload({ waitUntil: ["networkidle0", "domcontentloaded"] });
   







//   await browser.close();
})();