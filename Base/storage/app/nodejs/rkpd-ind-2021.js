const puppeteer = require('puppeteer');
const { PendingXHR } = require('pending-xhr-puppeteer');
const { functionsIn } = require('lodash');
var md5 = require('md5');
const fs = require('fs');
const { Console } = require('console');
const { Knex } = require('knex');

var indikator_program=[];
var indikator_kegiatan=[];
var parent={};

var pemda=[];
var browser;
var page;  
var pendingXHR;  
var access_data={
    tahun_data:2022,
    kodedaerah:"32",
    tahap:5
};
var account_user={
    username:'subpkp@bangda.kemendagri.go.id',
    password:'bangdapkp'
}
var access_key=null;

var parent_data={};

function delay(time) {
    return new Promise(function(resolve) { 
        setTimeout(resolve, time)
    });
} 
 async function init(){
        browser = await puppeteer.launch(
      { 
          headless: true,
          timeout:6000000
        }
      
      );


    page =await  browser.newPage();
    pendingXHR = new PendingXHR(page);
 }


async function get_data(){    
    if(access_data.kodedaerah.length>2){
     access_data['kode_wil']={
         prov:access_data.kodedaerah.substring(0,2),
         kabkot:access_data.kodedaerah.substring(2,4),
         levelAnggaran:'kabkota'
 
     };
    
 
     }else{
         access_data['kode_wil']={
             prov:access_data.kodedaerah.substring(0,2),
             kabkot:"00",
             levelAnggaran:'provinsi'
         };
        
     }
 
    await page.goto('https://sipd.kemendagri.go.id/edb_gis/?m=login', {
     waitUntil: 'networkidle2',
   });
   access_key= await page.evaluate(function(){
        var url_action= $('form').attr('action');
         if(url_action){
             url_action=url_action.split('/');
             if(url_action.length>2){
                 return url_action[2];
             }
         }
         return url_action;
     });
   
     console.log('access-key :'+access_key);
     
 //   check login
   await page.goto('https://sipd.kemendagri.go.id/edb_gis/'+access_key+'/?m=dashboard', {
     waitUntil: 'networkidle2',
   });
 
   logined=  await page.evaluate(function(){
         const urlSearchParams = new URLSearchParams(window.location.search);
         const params = Object.fromEntries(urlSearchParams.entries());
         if(params.m!=undefined){
             if(params.m=='login'){
                 return false;
             }else{
                 return true;
             }
         }
         
         return false;
     });
 
     // login
     if(!logined){
         await page.type('#username', account_user.username);
         await    page.type('#password', account_user.password);
         await Promise.all([
             
             page.click('[type="submit"]'),
             page.waitForNavigation({
                     waitUntil: 'networkidle0',
             })
         ]);
        
     }
 
 
     delay(3000);
 
     
 
     await page.evaluate(function(dta){
         status_rkpd=null;
         $.get("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/",{
             m:"dashboard",
             tahun:dta.access_data.tahun_data,
             f: "get_jadwal_tahapan",
             prov:dta.access_data.kode_wil.prov,
             kabkot:dta.access_data.kode_wil.kabkot
         },function(res){
             status_rkpd=res;
         });
 
     },{access_data:access_data,access_key:access_key});
     await pendingXHR.waitForAllXhrFinished();
 
     status_rkpd=await page.evaluate(function(){
         return status_rkpd;
     },{access_data:access_data,access_key:access_key});
     await delay(2000);
     console.log('info load');
    
     await page.evaluate(function(dta){
        info_pemda_dss=null;
        $.get('https://sipd.kemendagri.go.id/edb_gis/'+dta.access_key+'/?m=dashboard&f=get_info_pemda&prov='+dta.access_data.kode_wil.prov+'&kabkot='+dta.access_data.kode_wil.kabkot,
        function(res){
            info_pemda_dss=res;
            console.log(res);
        });
     },{access_data:access_data,access_key:access_key});
     await pendingXHR.waitForAllXhrFinished();

     pemda_info=await page.evaluate(function(){
         
        return JSON.parse(info_pemda_dss);
    },{access_data:access_data,access_key:access_key});
  
    await delay(100);

     parent={
         'kodedaerah':access_data.kodedaerah,
         'tahun':access_data.tahun_data,
         'updated_at':new Date(),   
     };

 
     if(status_rkpd[access_data.tahap-1]!=undefined){
         access_data['status_data']=status_rkpd[access_data.tahap-1];
         access_data['tahap']=access_data.tahap;
 
     }else{
         access_data['status_data']=status_rkpd[status_rkpd.length-1];
         access_data['tahap']=status_rkpd.length;
     }
 
 
     parent['status_data']=access_data['status_data'];
     parent['tahap']=access_data['tahap'];
     parent['dataset']=[];

    

 
     await page.evaluate(function(dta){
         dss_buffer=null;
         $.get("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/",{
             m:"dashboard",
             tahun:dta.access_data.tahun_data,
             f: "perbidangUrusan",
             prov:dta.access_data.kode_wil.prov,
             levelAnggaran:dta.access_data.levelAnggaran,
             kabkot:dta.access_data.kode_wil.kabkot,
             tahap:dta.access_data.status_data.max,
             log:1
            
         },function(res){
             dss_buffer=res;
         });
 
     },{access_data:access_data,access_key:access_key});
     await pendingXHR.waitForAllXhrFinished();
 
     parent['_dataset_per_urusan']=await page.evaluate(function(){
         return dss_buffer;
     },{access_data:access_data,access_key:access_key});


     delay(100);
     var index_urusan=0;
     while(parent['_dataset_per_urusan'].length>index_urusan ){

        // parent
        parent_data={
            tahun:access_data.tahun_data,
            prov:access_data.kode_wil.prov,
            kodepemda:access_data.kodedaerah,
            namapemda:pemda_info.namapemda,
            nomenklatur:pemda_info.nomenklatur,
            tahap_rkpd:parent['status_data'].nama_sub_tahap.replace(/\s+/g,' '),
            kabkot:access_data.kode_wil.kabkot,
            kodebidang:parent['_dataset_per_urusan'][index_urusan].kodebidang,
            namabidang:parent['_dataset_per_urusan'][index_urusan].uraibidang,
            log:1,
            jadwal:access_data.status_data.max,
            tab:"bidang"
        }

      

        //  build_si_parent=
        if(parent_data.kodebidang=='1.04'){
            await page.evaluate(function(dta){
                dss_buffer=null;
                $.post("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/?m=dashboard&f=perbidang_skpd",{
                    tahun:dta.tahun,
                    prov:dta.prov,
                    kodekot:dta.kabkot,
                    kodebidang:dta.kodebidang,
                    log:dta.log,
                    jadwal:dta.jadwal,
                    tab:dta.tab,
                },function(res){
                    dss_buffer=res;
                });

            },parent_data);
            await pendingXHR.waitForAllXhrFinished();


            parent['_dataset_per_urusan'][index_urusan]['_skpd']=await page.evaluate(function(){
                return dss_buffer;
            },{access_data:access_data,access_key:access_key});
            delay(100);
            var index_skpd=0;
            while( parent['_dataset_per_urusan'][index_urusan]['_skpd'].length>index_skpd){

            
                parent_data['id_skpd']=parent['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd].id_unit;
                parent_data['namaskpd']=parent['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd].nama_skpd;
                parent_data['kode_skpd']=parent['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd].kode_skpd;



                await page.evaluate(function(dta){
                    dss_buffer=null;
                    $.post("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/?m=dashboard&f=program",{
                        tahun:dta.tahun,
                        prov:dta.prov,
                        kodekot:dta.kabkot,
                        kodebidang:dta.kodebidang,
                        skpd:dta.id_skpd,
                        log:dta.log,
                        jadwal:dta.jadwal,
                        tab:dta.tab,
                    },function(res){
                        dss_buffer=res;
                    });

                },parent_data);
                await pendingXHR.waitForAllXhrFinished();

                parent['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program']=await page.evaluate(function(){
                    return dss_buffer;
                },{access_data:access_data,access_key:access_key});
                delay(100);
                var index_program=0;
                while(parent['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'].length>index_program){
                
                    parent_data['nama_program']=parent['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program].nama_program;
                    parent_data['kode_program']=parent['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program].kode_program;
                    await page.evaluate(function(dta){
                        dss_buffer=null;
                        $.post("https://sipd.kemendagri.go.id/edb_gis/"+dta.access_key+"/?m=dashboard&f=indikator_prog",{
                            tahun:dta.tahun,
                            prov:dta.prov,
                            kodekot:dta.kabkot,
                            kodebidang:dta.kodebidang,
                            skpd:dta.kode_skpd,
                            log:dta.log,
                            program:dta.kode_program,
                            jadwal:dta.jadwal,
                            tab:dta.tab,
                        },function(res){
                            dss_buffer=res;
                        });
        
                    },parent_data);

                    await pendingXHR.waitForAllXhrFinished();
        
                    parent['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator']=await page.evaluate(function(){
                        return dss_buffer;
                    },{access_data:access_data,access_key:access_key});
                    delay(100);
                    var index_indikator_program=0;
                    while( parent['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'].length>index_indikator_program){
                        
                        if(parent['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'][index_indikator_program].namaoutcome){
                            var out=parent['_dataset_per_urusan'][index_urusan]['_skpd'][index_skpd]['_program'][index_program]['_indikator'][index_indikator_program];
                            console.log(out);
                            indikator_program.push({
                                tahun:parent_data.tahun,
                                kodepemda:parent_data.kodepemda,
                                namapemda:parent_data.namapemda,
                                tahaprkpd:parent_data.tahap_rkpd,
                                nomenklatur:parent_data.nomenklatur,
                                kodebidang:parent_data.kodebidang,
                                namabidang:parent_data.namabidang,
                                kodeskpd:parent_data.kode_skpd,
                                namaskpd:parent_data.namaskpd||'',
                                kodeprogram:parent_data.kode_program,
                                nama_program:parent_data.nama_program,
                                namaoutcome:out.namaoutcome,
                                tahun_rpjmd_awal:out.tahun_rpjmd_awal|'',
                                tahun_rpjmd_akhir:out.tahun_rpjmd_akhir||'',
                                target_1:out.target_1,
                                target_2:out.target_2,
                                target_3:out.target_3,
                                target_4:out.target_4,
                                target_5:out.target_5,
                                satuanoutcome:out.satuanoutcome
                            });
                        }
                        index_indikator_program++;
                    }

                    index_program++;
                }
                index_skpd++;
            }


        }
        // end select urusan

         index_urusan++;
     }

     fs.writeFileSync(__dirname+"/RKPD-IND-PROGRAM/rkpd-"+access_data.tahun_data+"-"+access_data.kodedaerah+"-"+parent.tahap+".json", JSON.stringify(indikator_program).replace(/},/g,'},'+
     
     ''));

    delay(1000);
    await page.reload({ waitUntil: ["networkidle0", "domcontentloaded"] });

}; 
var tahun_rkpd=2021;
var pemda=[12,13,14,15,16,17,18,19,21,31,32,33,34,35,36,51,52,53,61,62,63,64,65,71,72,73,74,75,76,81,82,91,92,11];
access_data.tahun_data=tahun_rkpd;
(async ()=>{
    await init();
    var index_pemda=0
    while(pemda.length>index_pemda){

        console.log('pemda--------'+pemda[index_pemda]+'--------tahun--'+access_data.tahun_data);
        access_data['kodedaerah']=pemda[index_pemda]+'';
        // xxxx
        await get_data();
        index_pemda++;
    }
})();