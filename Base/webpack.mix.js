const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


 mix.webpackConfig({
    stats: {
        children: true,
    },
 }).js('resources/js/dss_sock.js', 'public/js')
 .js('resources/js/data_tematik.js', 'public/js')
 .js('Modules/DatasetModul/Resources/assets/js/form_dataset.js', 'public/js')
 .js('resources/js/app.js', 'public/js')
 .js('resources/js/dss-final.js', 'public/js')
 .js('Modules/FGDM/Resources/views/com_vue/app_builder.js', 'public/js')
 .vue({version:2})
 .sass('resources/sass/app.scss', 'public/css')
 .sass('resources/sass/app_hdark.scss', 'public/css')
 .js('resources/js/page_builder.js','public/js/page_builder').vue({version:2}).sass('resources/sass/page_builder.scss','public/css/page_builder')
 .options({
    processCssUrls: false,
});
