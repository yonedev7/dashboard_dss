const socket = io({
  auth: (cb) => {
    cb({
      token:localStorage.getItem('YuToken'),
      fingerprint:null
    });
  }
});
  

  
var app =new Vue({
    el:'#app',
    mounted:function(){
        this.sock_id=socket.id;
        this.conect_socket=socket.connected;
    },
    created:function(){
        this.actived_provider_live();
    },
    watch:{
        interval_live:function(val){
            if(val<=0){
                clearInterval(this.provider_check_live);
                this.sock_id=null;
                socket.close();

            }else if(val==this.interval_live_def){
                clearInterval(this.provider_check_live);
                this.actived_provider_live();

            }
        },
        
    },
    data:{
        interval_live:300000,
        interval_live_def:300000,
        conect_socket:0,
        approve_socket:0,
        sock_id:'',
        form_message:{
            text:'',
            file:null,
            file_local:null,
            type:0,
            user:{

            },
            is_me:true,
            time:null
        },
        chat_room:{
            id:0,
            data:[
                
            ]
        },
        list_user:[

        ]
    },
    methods:{
        sendMessage:function(){
            this.interval_live=this.interval_live_def;
            var message=JSON.parse(JSON.stringify(this.form_message));
            message.is_me=true;
            message.time=new Date().toISOString();
           if(message.text){
            socket.emit('on-message-send',message);
            this.chat_room.data.push(message);
            this.form_message.text='';

           }

        },
        actived_provider_live:function(){
            this.provider_check_live=setInterval(function(){
                app.interval_live-=1;
            },1000);
        },
        checkOnSend:function(e){
            if (e.keyCode === 13) {
                this.interval_live=this.interval_live_def;
                this.sendMessage();
              } else if (e.keyCode === 50) {
                alert('@ was pressed');
              }      
        },
        onMessage:function(payload){
            if(payload){
                var message={
                    text:payload.text,
                    file:payload.file_path_server,
                    file_local:payload.file_path_local,
                    type:0,
                    user:{

                    },
                    is_me:false,
                    time:null
                }  
            }

            this.chat_room.data.push(message);

        }
    }
});



socket.io.on("reconnect_attempt", () => {
    // ...
  });
  
socket.io.on("reconnect", () => {
    // ...
 });



 socket.on('on-message',(payload)=>{
     console.log(payload);
    app.onMessage(payload);
 });

 socket.on("connect_error", () => {
  
        app.sock_id=null;
        app.conect_socket=false;

  });

socket.on("connect", () => {
    const engine = socket.io.engine;
    app.sock_id=socket.id;
    app.conect_socket=true;
    console.log('client connected');

  
    engine.once("upgrade", () => {
      // called when the transport is upgraded (i.e. from HTTP long-polling to WebSocket)
      console.log(engine.transport.name); // in most cases, prints "websocket"
    });
  
    engine.on("packet", ({type,data})=> {
      // called for each packet received
      console.log('package receives',type
      );
      console.log('datanya',data);
        

    });
  
    engine.on("packetCreate", ({ type, data }) => {
      // called for each packet sent
      console.log('package create',type,data);
    });
  
    engine.on("drain", () => {
      console.log('package drain');

      // called when the write buffer is drained
    });
  
    engine.on("close", (reason) => {
        console.log('close with ',reason);
        app.sock_id=null;
        app.conect_socket=false;


      // called when the underlying connection is closed
    });
  });
