window.io = require("socket.io-client");
window.Vue=require('vue/dist/vue.esm.js').default;
import VueTimeago from 'vue-timeago';
Vue.use(VueTimeago, {
    name: 'Timeago', // Component name, `Timeago` by default
    locale: 'en', // Default locale
   
    locales: {
      'zh-CN': require('date-fns/locale/zh_cn'),
      ja: require('date-fns/locale/ja')
    }
  });


