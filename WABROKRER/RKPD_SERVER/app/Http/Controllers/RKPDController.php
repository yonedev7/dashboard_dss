<?php

namespace App\Http\Controllers;
use App\Models\Nomen;
use Illuminate\Http\Request;
class RKPDController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function nomen(Request $request){
        $data=(new Nomen());
        if($request->kode){
            $data=$data->where('kode',$request->kode['operator'],$request->kode['value']);
        }
        if($request->tipe){
            $data=$data->where('tipe',$request->kode['operator'],$request->kode['value']);
        }

        if($request->nama_kegiatan){
            $data=$data->where('nama_kegiatan',$request->kode['operator'],$request->kode['value']);
        }

        return [
            'code'=>200,
            'data'=>$data->get()
        ];
    }



    //
}
