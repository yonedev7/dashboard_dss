<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('master_akun',function(Blueprint $table){
            $table->uuid('uid')->default(DB::raw('(UUID())'))->primary();
            $table->string('kode',20);
            $table->string('tipe',20);
            $table->string('nama_akun',200);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
