<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        //
       if(!Schema::hasTable('apbd_'.date('Y'))){
            Schema::create('apbd_'.date('Y'),function(Blueprint $table){
                $table->uuid('uid')->default(DB::raw('(UUID())'))->primary();

                $table->string('kodepemda',5);
                $table->string('kodesubkegiatan',20);
                $table->string('kodeakun',20);
                $table->float('realisasi')->nullable();

            });
       }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('apbd_'.date('Y'));

    }
};
