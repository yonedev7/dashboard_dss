<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        //
       if(!Schema::hasTable('rkpd_'.date('Y'))){
            Schema::create('rkpd_'.date('Y'),function(Blueprint $table){
                $table->uuid('uid')->default(DB::raw('(UUID())'))->primary();

                $table->string('kodepemda',5);
                $table->string('kodesubkegiatan',20);
                $table->float('pagu')->nullable();
                $table->unique(['kodesubkegiatan','kodepemda'],'rkpd_'.date('Y').'_un');

            });
       }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('rkpd_'.date('Y'));

    }
};
