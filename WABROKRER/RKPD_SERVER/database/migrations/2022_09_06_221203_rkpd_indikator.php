<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if(!Schema::hasTable('rkpd_indikator_'.date('Y'))){
            Schema::create('rkpd_indikator_'.date('Y'),function(Blueprint $table){
                $table->uuid('uid')->default(DB::raw('(uuid())'))->primary();
                $table->string('kode',36);
                $table->text('nama');
                $table->string('satuan')->nullable();
                $table->text('value')->nullable();
                $table->foreign('kode')
                ->references('uid')
                ->on('rkpd_'.date('Y'))
                ->onDelete('cascade')
                ->onUpdate('cascade');
            });
       }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('rkpd_indikator_'.date('Y'));
    }
};
