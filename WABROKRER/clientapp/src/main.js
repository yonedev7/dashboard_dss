import { createApp } from 'vue'


import BootstrapVue from "bootstrap-vue-3"
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueSocketIO from 'vue-3-socket.io'
import SocketIO from 'socket.io-client'
import QrcodeVue from 'qrcode.vue'
import PulseLoader from "vue-spinner/src/PulseLoader.vue"

import App from './App.vue';
import ListSub from './components/subMenu.vue';

import router from './router'
import store from "./store";
import VueNestedMenu from 'vue-nested-menu';

import { library } from '@fortawesome/fontawesome-svg-core'

import { faHatWizard,faArrowCircleLeft,faArrowRight } from '@fortawesome/free-solid-svg-icons'
library.add(faHatWizard);
library.add(faArrowCircleLeft);
library.add(faArrowRight);

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'







import "./assets/main.scss"

 const app = createApp(App)
var options={ } //Optional options

app.use(new VueSocketIO({
    debug: true,
    connection: SocketIO('https://sock.sinkronisasi.id', options),
    vuex: {
        store,
        actionPrefix: 'SOCKET_',
        mutationPrefix: 'SOCKET_'
    },

}));
app.use(VueNestedMenu);
app.use()
app.component('font-awesome-icon', FontAwesomeIcon)
app.component('qrcode-vue',QrcodeVue)
app.component('pulser',PulseLoader)
app.component('listsub',ListSub)
app.use(store)
app.use(FontAwesomeIcon)
app.use(VueAxios, axios)
app.use(BootstrapVue)
app.use(router)

app.mount('#app')

 import 'sweetalert2/src/sweetalert2.scss'
window.yoneBM=app;
