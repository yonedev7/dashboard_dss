import { createRouter, createWebHistory } from 'vue-router'
import Egine from '../views/Egine.vue';
import AdminEgine from '../components/App.vue';
import ViewData from "../views/ViewData.vue";



const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      name: 'list-data',
      component: ViewData
    },
    {
      path: '/app/admin-egine',
      name: 'admin-egine',
      component: AdminEgine
    },
    {
      path: '/app/egine/:egine_id',
      name: 'egine',
      component: Egine
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue')
    }
  ]
})

export default router
