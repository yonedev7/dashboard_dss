/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
    return knex.schema.createTable('users', function (t) {
        t.uuid('id',{
            primaryKey:true,
            useBinaryUuid:true
        });
        t.string('email').unique().collate('utf8_unicode_ci');
        t.string('name').nullable();
        t.string('number_phone').unique();
      });
  
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
    return knex.schema.dropTableIfExists('users')
  
};
