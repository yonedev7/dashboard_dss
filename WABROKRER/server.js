
"use strict";
const Buffer = require('buffer/').Buffer
const port=3100;
const host= '127.0.0.1';

const path =require('path');
const fs =require('fs');
var wget = require('node-wget');
 const express = require("express");
 const { createServer } = require("http");
 const { Server } = require("socket.io");
 const WA=require('./src/egine_wa/egine').WA
 var os = require('os');
 var mysql = require('mysql');
 const axios=require('axios');
 const http=require('https');
 var cors = require('cors')
 const bodyParser = require("body-parser");
let netwok=null;
 var allNetworkInterfaces = os.networkInterfaces();
    if(allNetworkInterfaces['en0']!=undefined){
      console.log(allNetworkInterfaces);
      var netwokallNetworkInterfaces=allNetworkInterfaces.en0.filter(el=>el.family=='IPv4');
      if(netwok.length){
          netwok='http://'+netwok[0].address;
      }else{
          netwok=null;
      }

    }else{
      netwok=null
    }

netwok='https://sock.sinkronisasi.id';
 const onChange = (objToWatch, onChangeFunction) => {
    const handler = {
      get(target, property, receiver) {
        onChangeFunction(); // Calling our function
        return target[property];
      }
    };
  return new Proxy(objToWatch, handler);
  };
  const dbconfig={
    host     : 'localhost',
    user     : 'root',
    password : 'Uba2013?',
    database : 'yoneMB'
  };


class serverHttp{

    constructor(){

        this.router=require('./src/router');
        const app=express();
        this.httpServer=createServer(app);
        this.app=app;
        this.app.use(cors())
        // this.app.use(express.json());       // to support JSON-encoded bodies
        // this.app.use(express.urlencoded()); // to support URL-encoded bodies
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(bodyParser.json());

        this.io=new  Server(this.httpServer, {
            cors:{
                host:"*"
            }
        });

        // this.db= mysql.createConnection(dbconfig);
        // this.knex=require('knex')({
        //     client: 'mysql',
        //     connection: dbconfig
        //   });
          this.egine={
            wa:[
                {
                    egine:null,
                    sub:null,
                    name:'client_1',
                    status:true,
                    isRunning:false
                }
            ]

        }
        // this.db.connect();
        this.io.on('connect',(socket)=>{
            socket.on('list_egine',(mess,callback)=>{
                var datax=[];
                Object.entries(this.egine).forEach(el=>{
                        el[1].forEach(x=>{
                        datax.push({
                            egine_type:el[0],
                            id:el[0]+'->'+x.name,
                            name:x.name,
                            status:x.status,
                            isRunning:x.isRunning,
                        });
                    });

                });

                callback(datax);
            });

            socket.on('egine_logout',(mes,callback)=>{
                console.log('try logout');
                var meta=mes.id.split('->');
                var egine=this.egine[meta[0]]!=undefined?this.egine[meta[0]]:[];

                var exist=egine.filter(el=>{
                    return el.name==meta[1];
                });
                if(exist){
                    exist[0].egine.state.auth.logined=false;
                    exist[0].egine.state.message.from=null;
                    exist[0].egine.client.logout();
                    exist[0].egine.client.initialize();
                }

            });

            socket.on('egine_state',(mes,callback)=>{
                var meta=mes.id.split('->');
                var egine=this.egine[meta[0]]!=undefined?this.egine[meta[0]]:[];

                var exist=egine.filter(el=>{
                    return el.name==meta[1];
                });
                if(exist){
                    console.log(exist[0].egine.state);
                    callback(exist[0].egine.state);
                }else{
                    callback({state:null,
                        status:false,
                        auth:{
                            logined:false,
                            qr_data:null,
                            client:{
                                number:''
                            }
                        },
                        message:{

                    }});
                }
            });

        });



        this.http_port=3000;
    }


    listen=(port,host)=>{
        const self=this;
        this.httpServer.listen(port,host,function(){
            // netwok+=':'+port;

            self.initEgine();
            console.log('listen http server',netwok);
            console.log('listen http server',host+':'+port);


        });


    }

   async initEgine(){
        const self=this;
        Object.entries(this.egine).forEach(async (el)=> {
            el[1].forEach(async (eg,k)=>{
                if(eg.isRunning){
                    this.egine[el[0]][k].egine.destroy();
                }
                if(eg.status){
                    this.egine[el[0]][k].egine=el[0]=='wa'?new WA(el[1].name,this.io,netwok):null;
                    this.egine[el[0]][k].isRunning=this.egine[el[0]][k].egine.run()?true:false;
                }
            });
        });
    }

    regisRouter(egine_name,egine,router){
        console.log('register router egine ',egine_name);
    }

    sendTo(egine,data){
        console.log('send to io',data);
        this.io.of(egine).broadcast('message',data);
    }

   async rkpd(req){
        var url="https://dss-nuwas.sinkronisasi.id/v2/api/app-rkpd_apbd-distribute?tahun="+(req.tahun||2022)+"&&kodeurusan="+(req.kodeurusan||'1')+"&kodedaerah="+(req.kodepemda||'NAS')+"";
        // await wget(url,function(err,response,body){
        //     if(!err){
        //         console.log(body);
        //     }
        // });

        var self=this;


        let res=await  axios.post(url).then( async res=>{
            console.log(res);
            console.log('------------');
            try {
                var internal_path=path.resolve(__dirname,'clientapp/dist/files','rkpd','downloads',res.data.filename);
                if (fs.existsSync(internal_path)) {
                    console.log('file_exist');
                    Object.entries(server.egine).forEach(el=>{
                        if(el[0]=='wa'){
                            el[1].forEach(x=>{
                                if(x.name=='client_1'){
                                    if(x.egine.state.auth.logined){
                                        console.log('send file ',internal_path,'to',req.envNumber);
                                    x.egine.sendMediaClient(internal_path,req.envNumber);

                                    }else{
                                        console.log('egine',x.name,'not ready');

                                    }
                                }
                            });
                        }
                    });
                  //file exists
                }else{
                    console.log('file_not exist');

                    await self.downloadFile(res.data.link,res.data.filename,req.envNumber,'rkpd',function(path){
                        console.log('download done ',path);
                        Object.entries(server.egine).forEach(el=>{
                            if(el[0]=='wa'){
                                el[1].forEach(x=>{
                                    if(x.name=='client_1'){
                                    if(x.egine.state.auth.logined){
                                        console.log('send file ',internal_path,'to',req.envNumber);

                                        x.egine.sendMediaClient(path,req.envNumber);
                                    }else{
                                        console.log('egine',x.name,'not ready');
                                    }
                                    }
                                });
                            }
                        })

                    });

                }
              } catch(err) {
                console.log('eeee');
            }


        });
    }

   async downloadFile(url,filename,sendTo,app_name,callback){
        var internal_path=path.resolve(__dirname,'clientapp/dist/files',app_name,'downloads',filename);
        const file = fs.createWriteStream(internal_path);
        const request = await http.get(url, function(response) {
             response.pipe(file);
                file.on("finish", () => {
                    file.close();
                    callback(internal_path);
                });
            });


    }

}






var server=new serverHttp();


server.app.post('/api/get-rekap/:app_egine',function(req,res){
    console.log(req.params.app_egine)
    if(req.params.app_egine=="rkpd_egine"){
           if(req.body.envNumber){
            server.rkpd(req.body);
           }
    }
    res.send(req.body);

});

server.app.get('/set-env/:phone',function(req,res){
    let buff = new Buffer(req.params.phone, 'base64');
    let text = buff.toString('ascii');
    res.send(`
    <h5>Redirect To ${netwok}</h5>
    <script>

    localStorage.setItem('yoneMB_phone','${text}');
        location.href='${netwok}';
    </script>
    `);
});


// server.app.get('/reload-all-egine',function(req,res){
//     server.initEgine()
//     res.send('reload...');

//     res.send('aaa');
// });



// server.app.get('/egine/:type/:egine_name',function(req,res){
//     res.send(req.params);
// });

server.app.get('/testx',function(req,res){
    res.send(req);
});

// server.app.get('/state-change',function(req,res){

    // server.egine.wa.egine.state.state=Math.random(10);
    // res.send(server.egine.wa.egine.state);
// });


server.app.use(express.static(path.resolve(__dirname,'clientapp/dist')));
server.app.get('/',function(req,res){
    console.log('load egine');
    res.sendFile(path.join(__dirname, 'clientapp/dist/index.html'));
});

server.app.get('/app/*',function(req,res){
    console.log('load egine');
    res.sendFile(path.join(__dirname, 'clientapp/dist/index.html'));
});

 server.listen(port,host);
