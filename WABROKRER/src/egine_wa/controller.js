
const axios = require('axios').default;
const { cli } = require('webpack');
const {List,Message,MessageMedia,Buttons} =require('whatsapp-web.js')
const fs =require('fs');

class Controller{
    constructor(client){
        this.client=client;
        this.axios=axios.create({
            method: 'post',
            baseURL     :'http://localhost:8000'
        });
        this.number=null;
        this.prefix="";
        this.list=null;
    }

    setNumber(number){
        this.number=number;
    }

    setPrefix(prefix){
        this.prefix=prefix;
    }

    setPrefix(surfix){
        this.surfix=surfix;
    }

    buildInit(){
        this.list=new List(
            "Pilih Lingkup",
            "Tampilkan Pilihan",
            [
                {
                    title:"Level Data",
                    rows:[
                        {
                            title:'NASIONAL',
                            id:'LINGKUP_NASIONAL'

                        },
                        {
                            title:'PER DAERAH',
                            id:'LINGKUP_PEMDA'
                        }
                    ]
                }
            ],
            "Lingkup"
        );
    }

    get(path,data,callback){
        var d=this.axios.post(path,data).then(res=>{
            return res;
        });
        callback(d);
    }

    buildListScope(data){
        var urusan=data.data.map(el=>{
            return {
                title:el.nama_kegiatan,
                id:this.prefix+'->'+el.kode
            }
        });
        var list=new List(
            "Pilih Urusan",
            "List Urusan",
            [
                
                {
                    title: "..",
                    rows: urusan
                }
                
            ]
        
        );

        this.list=list;
    }

    buildFile(data){

    }

   async  sendMessage(type){
        switch(type){
            case 'text':

            break;
            case 'file':

            break;
            case 'list':
                console.log(this.number,this.list);
               await this.client.reply(this.list);
               await this.client.reply(MessageMedia.fromFilePath(__dirname+'/../../clientapp/public/logo.png'));
            //    await this.client.reply(MessageMedia.fromFilePath('https://peraturan.go.id/common/dokumen/bn/2016/bn863-2016.pdf'));
            var btn=new Buttons(
                "a",
                [{id:'customId',body:'button1'},{body:'button2'},{body:'button3'},{body:'button4'}],
                "",
                "LIST"
            );

            await this.client.reply(btn);

            var btn=new Buttons([{id:'customId',body:'button1'},{body:'button2'},{body:'button3'},{body:'button4'}]);

            await this.client.reply(btn);
               


            break;
        }
    }
}


module.exports={
    RKPDController:Controller
};