const fs = require('fs');
const { Client, LegacySessionAuth,List,MessageMedia } = require('whatsapp-web.js');
const HandlerMessage=require('./controller');
const Buffer = require('buffer/').Buffer 
console.log('ssss',HandlerMessage);

const onChange = (objToWatch, onChangeFunction,op,route) => { 
    const handler = {
      get(target, property, receiver) {
        onChangeFunction(target,op,route); // Calling our function
        return target[property];
      }
    };
  return new Proxy(objToWatch, handler);
  };


class WA{
    constructor(client_id=null,io,network){
       try{
        this.defMessage=network;
        
        this.egine_name='wa';
        this.router=[];
        this.client_id=client_id||'client_1';
        this.io=io;
        this.config = new (require('./config').defalut);
        this.client=new Client(this.config.get());
        this.state=onChange({
            state:null,
            status:false,
            auth:{
                logined:false,
                qr_data:null,
                client:{
                    pushname: null,
                    wid: {
                      server: null,
                      user: null,
                      _serialized: null
                    },
                    me: {
                      server: null,
                      user: null,
                      _serialized: null
                    },
                    phone: null,
                    platform: null
                }
            },
            message:{
                from:null,
                body:null,
                type:null
            }
        },function(a,io,route){
            console.log('send to client',a);
            io.emit(route,a);
       },this.io,this.egine_name+'->'+this.client_id);
       
       
     
       }catch(e){
        console.log(e);
       }
       
    
    }

    setPortDefNetwork(port){
        this.defMessage+':'+port;
    }

    addHandle(add_route,write=false){
        var exist=this.router.forEach(el=>{
            el[0]==add_route.id
        });

       if(write==false){
            if(exist.length){
                console.log('router id ',add_route.id,' is exist in egine whatsapp');
            }else{
                this.router.push(add_route);
            }
       }else{
            if(exist.length){
                var index=this.router.indexOf(exist[0]);
                if(index!=-1){
                    this.router.push(add_route);
                }   
            }else{
                this.router.push(add_route);

            }
       }
    }

    async sendMediaClient(path,to){
            this.client.sendMessage(to, MessageMedia.fromFilePath(path));
    }

    async destroy(){
        this.client.destroy();
        this.client.pupBrowser.close();

    }


    async run(){
        console.log('running egine whatsapp');
        this.client.on('qr', (qr) => {
            // Generate and scan this code with your phone
            console.log('QR RECEIVED', qr);
            this.state.auth.qr_data=qr;
            this.state.auth.logined=false;
            this.state.auth.status=false;


        });
        
        this.client.on('ready', () => {
            this.state.status=true;
            console.log('Client is ready!');
            this.state.auth.client.pushname=this.client.info.pushname;
            this.state.auth.client.me=this.client.info.me;
            this.state.auth.client.wid=this.client.info.wid;
            this.state.auth.client.phone=this.client.info.phone;
            this.state.auth.client.platform=this.client.info.platform;





        });

        this.client.on('authenticated',()=>{
            this.state.auth.logined=true;

            
        });

        this.client.on('state', (state) => {

            this.state.state=state;
            console.log('Client state! ',state);

        });


        this.client.on('message', async (mes) => {
            var data={
                from:mes.from,
                body:mes.body,
                type:mes.type
            }
            this.state.message.from=data.from;
            this.state.message.body=data.body;
            this.state.message.type=data.type;
            if((mes.body||'').toUpperCase()=='!DSS'){
                let buff = new Buffer(mes.from);
                mes.reply(this.defMessage+'/set-env/'+(buff.toString('base64')));
            }
            if(mes.body=='/DATA-RKPD'){
                console.log('init response');
                let HM= new HandlerMessage.RKPDController(mes);
                HM.setNumber(mes.from);
                HM.setPrefix("EXCEL");
                HM.buildInit();
                HM.sendMessage('list');
                this.client.sendMessage(mes.from,'sss');

                this.client.sendMessage(mes.from,new List(
                    "Pilih Lingkup",
                    "",
                    [
                        {
                            title:"Level Data",
                            rows:[
                                    {
                                        id:'LINGKUP_NASIONAL',
                                        title:'NASIONAL',
            
                                    },
                                    {
                                        id:'LINGKUP_PEMDA',
                                        title:'PER DAERAH',
                                    }
                            ]
                        },
                    ],
                    ""
                ));

            }


            this.router.forEach(async (el) => {
                if(el.isResponse){
                    await mes.reply('type response body',el.body);

                }else if(el.type=='stright'){
                    if(el.body==mes.body){
                        await mes.reply('type stright body',el.body);
                    }
                }else if(el.type=='includes'){
                    if((mes.body||'').toUpperCase().includes((el.body.toUpperCase()))){
                        await mes.reply('type includes body',el.body);
                    }
                }
                
            });
        });

      try{
        this.client.initialize();
        return true;
      }catch(e){
        return false;
      }



    }
}

module.exports={
    WA
};