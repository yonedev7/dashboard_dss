const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
module.exports = {
  watch: true,
  mode:'development',
  watchOptions: {
    aggregateTimeout: 200,
    poll: 1000,
    followSymlinks: true,
    stdin: true,
  },
  entry:{
    appjs:{
      import:path.resolve(__dirname,'client/app.js'),
      filename:'app.js'
    },
    appstyle:{
      import:path.resolve(__dirname,'client/app.scss'),
      filename:'app.css'
    }
  },
  output:{
    chunkLoadTimeout: 30000,
    path:path.resolve(__dirname,'public/asset')
   
  },  
  plugins: [new MiniCssExtractPlugin()],

};